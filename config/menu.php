<?php

return [
    [
        'name'=>'account.label',
        'route'=>'admin.accounts.index',
        'glyphicon' => 'far fa-handshake fa-fw',
        'permissions'   => [],
        'hide' =>false
    ],
    [
        'name'=>'sale-cost-items.label',
        'route'=>'admin.sale-cost-items.index',
        'glyphicon' => 'far fa-handshake fa-fw',
        'permissions'   => [],
        'hide' =>false
    ],
    [
        'name'=>'fixed-assets.label',
        'glyphicon' => 'fa fa-barcode fa-fw',
        'hide' => false,
        'permissions'   => [],
        'child'=> [
            [
                'name' => 'list',
                'route'=> 'admin.fixed-assets.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
            [
                'name' => 'fixed-asset-depreciation.label',
                'route'=> 'admin.fixed-asset-depreciation.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
            [
                'name' => 'fixed-asset-disposal.label',
                'route'=> 'admin.fixed-asset-disposal.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
            [
                'name' => 'report.label',
                'route'=> 'admin.fixed-asset-reports.index',
                'glyphicon' => 'ion ion-md-list-box',
                'permissions'   => [],
                'hide' => false
            ],
        ]
        
    ],
    [
        'name'=>'expenses-prepaid.label',
        'glyphicon' => 'fa fa-barcode fa-fw',
        'hide' => false,
        'permissions'   => [],
        'child'=> [
            [
                'name' => 'list',
                'route'=> 'admin.expenses-prepaid.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
            [
                'name' => 'expenses-prepaid-allocation.label',
                'route'=> 'admin.expenses-prepaid-allocation.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
        ]
    ],
    [
        'name'=>'instrument-tools.label',
        'glyphicon' => 'fa fa-barcode fa-fw',
        'hide' => false,
        'permissions'   => [],
        'child'=> [
            [
                'name' => 'list',
                'route'=> 'admin.instrument-tools.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
            [
                'name' => 'instrument-tool-allocation.label',
                'route'=> 'admin.instrument-tool-allocation.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
            [
                'name' => 'instrument-tool-disposal.label',
                'route'=> 'admin.instrument-tool-disposal.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => false
            ],
        ]
    ],
    [
        'name'=>'instrument-tool-types.label',
        'route'=>'admin.instrument-tool-types.index',
        'glyphicon' => 'fa fa-barcode fa-fw',
        'permissions'   => [],
        'hide' =>false
    ],
    [
        'name'=>'cost-items.label',
        'route'=>'admin.cost-items.index',
        'glyphicon' => 'fa fa-barcode fa-fw',
        'permissions'   => [],
        'hide' =>false
    ],
    [
        'name'=>'input-invoices.label',
        'route'=>'admin.accounting.invoices',
        'glyphicon' => 'fas fa-file-invoice-dollar fa-fw',
        'permissions'   => [],
        'hide' =>false
    ],
    [
        'name' => 'jobs.label',
        'route' => 'admin.jobs.index',
        'glyphicon' => 'far fa-handshake fa-fw',
        'permissions' => [],
        'hide' => false,
    ],
    [
        'name' => 'partners.label',
        'route' => 'admin.partners.index',
        'glyphicon' => 'far fa-handshake fa-fw',
        'permissions' => [],
        'hide' => false,
    ],
    [
        'name' => 'payment-receipt-vouchers.label',
        'glyphicon' => 'fas fa-file-invoice-dollar fa-fw',
        'hide' => false,
        'permissions'   => [],
        'child'=> [
            [
                'name' => 'list',
                'route'=> 'admin.payment-receipt-vouchers.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => true
            ],
            [
                'name' => 'payment-receipt-vouchers.label_1',
                'route'=> 'admin.payment-receipt-vouchers.create-payment-vendor',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => true
            ],
            [
                'name' => 'payment-receipt-vouchers.label_2',
                'route'=> 'admin.payment-receipt-vouchers.create-receipt-bank',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => true
            ],
            [
                'name' => 'payment-receipt-vouchers.label_3',
                'route'=> 'admin.payment-receipt-vouchers.create-payment-order',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => true
            ],
        ]
    ],
	[
		'name' => 'accounting-invoices.label',
		'route' => 'admin.accounting-invoices.index',
		'glyphicon' => 'fas fa-file-contract fa-fw',
		'permissions' => [],
		'hide' => false,
	],
    [
        'name' => 'service-purchase.label',
        'route' => 'admin.service-purchases.index',
        'glyphicon' => 'fas fa-file-invoice-dollar fa-fw',
        'permissions' => [],
        'hide' => true,
    ],
    [
        'name' => 'banks.label',
        'glyphicon' => 'glyphicon glyphicon-piggy-bank',
        'permissions' => [],
        'hide' => true,
        'child'=> [
            [
                'name' => 'banks.label_2',
                'route' => 'admin.banks.index',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => true
            ],
            [
                'name' => 'banks.label_1',
                'route'=> 'admin.banks.index-accounts',
                'glyphicon' => 'fa fa-list fa-fw',
                'permissions'   => [],
                'hide' => true
            ],
         
        ]
    ],
    

    // [
    //     'name' => 'products.label',
    //     'glyphicon' => 'fa fa-barcode fa-fw',
    //     'hide' => false,
    //     'permissions'   => ['products.read', 'brands.read', 'brand_categories.read', 'product_suppliers.read', 'supplier_categories.read', 'product_categories.read', 'reviews.read', 'attributes.read', 'attribute_values.read', 'attribute_categories.read'],
    //     'child'=> [
    //         [
    //             'name' => 'products.list',
    //             'route'=> 'admin.products.index',
    //             'glyphicon' => 'fa fa-list fa-fw',
    //             'permissions'   => ['products.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'products.bulk_size',
    //             'route'=> 'admin.sizes.create-bulk',
    //             'glyphicon' => 'fas fa-exchange-alt fa-fw',
    //             'permissions'   => ['products.update'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'products.review',
    //             'route'=> 'admin.reviews.index',
    //             'glyphicon' => 'fa fa-star fa-fw',
    //             'permissions'   => ['reviews.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'products.brand',
    //             'route'=> 'admin.brands.index',
    //             'glyphicon' => 'fa fa-list fa-fw',
    //             'permissions'   => ['brands.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'products.brand_category',
    //             'route'=> 'admin.brand-categories.index',
    //             'glyphicon' => 'fa fa-th fa-fw',
    //             'permissions'   => ['brand_categories.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'products.supplier',
    //             'route'=> 'admin.product-suppliers.index',
    //             'glyphicon' => 'fa fa-table fa-fw',
    //             'permissions'   => ['product_suppliers.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'products.supplier_category',
    //             'route'=> 'admin.supplier-categories.index',
    //             'glyphicon' => 'fa fa-list-alt fa-fw',
    //             'permissions'   => ['supplier_categories.read'],
    //             'hide' => false
    //         ],

    //         [
    //             'name' => 'products.category',
    //             'route'=> 'admin.product-categories.index',
    //             'glyphicon' => 'fa fa-folder fa-fw',
    //             'permissions'   => ['product_categories.read'],
    //             'hide' => false
    //         ],
    //         // [
    //         //     'name' => 'products.category_sort',
    //         //     'route'=> 'admin.product-categories.sort',
    //         //     'permissions'   => ['product_categories.update'],
    //         //     'glyphicon' => 'fa fa-sort-amount-asc fa-fw',
    //         //     'hide' => false
    //         // ],
    //         [
    //             'name' => 'attributes.label',
    //             'route'=> 'admin.attributes.index',
    //             'glyphicon' => 'fa fa-adjust fa-fw',
    //             'permissions'   => ['attributes.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'attributes.value',
    //             'route'=> 'admin.attribute-values.index',
    //             'glyphicon' => 'fa fa-flask fa-fw',
    //             'permissions'   => ['attribute_values.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'attributes.category',
    //             'route'=> 'admin.attribute-categories.index',
    //             'glyphicon' => 'fa fa-folder fa-fw',
    //             'permissions'   => ['attribute_categories.read'],
    //             'hide' => false
    //         ],
    //     ]
    // ],
    
//     [
//         'name' => 'products.label',
//         'glyphicon' => 'fa fa-barcode fa-fw',
//         'hide' => false,
//         'permissions'   => ['products.read', 'brands.read', 'brand_categories.read', 'product_suppliers.read', 'supplier_categories.read', 'product_categories.read', 'reviews.read', 'attributes.read', 'attribute_values.read', 'attribute_categories.read'],
//         'child'=> [
//             [
//                 'name' => 'products.list',
//                 'route'=> 'admin.products.index',
//                 'glyphicon' => 'fa fa-list fa-fw',
//                 'permissions'   => ['products.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'products.bulk_size',
//                 'route'=> 'admin.sizes.create-bulk',
//                 'glyphicon' => 'fas fa-exchange-alt fa-fw',
//                 'permissions'   => ['products.update'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'products.review',
//                 'route'=> 'admin.reviews.index',
//                 'glyphicon' => 'fa fa-star fa-fw',
//                 'permissions'   => ['reviews.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'products.brand',
//                 'route'=> 'admin.brands.index',
//                 'glyphicon' => 'fa fa-list fa-fw',
//                 'permissions'   => ['brands.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'products.brand_category',
//                 'route'=> 'admin.brand-categories.index',
//                 'glyphicon' => 'fa fa-th fa-fw',
//                 'permissions'   => ['brand_categories.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'products.supplier',
//                 'route'=> 'admin.product-suppliers.index',
//                 'glyphicon' => 'fa fa-table fa-fw',
//                 'permissions'   => ['product_suppliers.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'products.supplier_category',
//                 'route'=> 'admin.supplier-categories.index',
//                 'glyphicon' => 'fa fa-list-alt fa-fw',
//                 'permissions'   => ['supplier_categories.read'],
//                 'hide' => false
//             ],

//             [
//                 'name' => 'products.category',
//                 'route'=> 'admin.product-categories.index',
//                 'glyphicon' => 'fa fa-folder fa-fw',
//                 'permissions'   => ['product_categories.read'],
//                 'hide' => false
//             ],
//             // [
//             //     'name' => 'products.category_sort',
//             //     'route'=> 'admin.product-categories.sort',
//             //     'permissions'   => ['product_categories.update'],
//             //     'glyphicon' => 'fa fa-sort-amount-asc fa-fw',
//             //     'hide' => false
//             // ],
//             [
//                 'name' => 'attributes.label',
//                 'route'=> 'admin.attributes.index',
//                 'glyphicon' => 'fa fa-adjust fa-fw',
//                 'permissions'   => ['attributes.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'attributes.value',
//                 'route'=> 'admin.attribute-values.index',
//                 'glyphicon' => 'fa fa-flask fa-fw',
//                 'permissions'   => ['attribute_values.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'attributes.category',
//                 'route'=> 'admin.attribute-categories.index',
//                 'glyphicon' => 'fa fa-folder fa-fw',
//                 'permissions'   => ['attribute_categories.read'],
//                 'hide' => false
//             ],
//         ]
//     ],
    // [
    //     'name' => 'warehouses.label',
    //     'glyphicon' => 'fas fa-boxes fa-fw',
    //     'hide' => false,
    //     'permissions'   => ['inventory_categories.read', 'inventories.read'],
    //     'child'=> [
    //         [
    //             'name' => 'list',
    //             'route'=> 'admin.inventory-categories.index',
    //             'glyphicon' => 'fa fa-list fa-fw',
    //             'permissions'   => ['inventory_categories.read'],
    //             'hide' => false
    //         ],
    //         [
    //             'name' => 'warehouses.inventory',
    //             'route'=> 'admin.inventories.index',
    //             'glyphicon' => 'fa fa-cube fa-fw',
    //             'permissions'   => ['inventories.read'],
    //             'hide' => false
    //         ],
    //     ]
    // ],
//     [
//         'name' => 'prices.label',
//         'glyphicon' => 'fas fa-hand-holding-usd fa-fw',
//         'hide' => false,
//         'permissions'   => ['prices.read', 'agency_prices.read'],
//         'child'=> [
//             [
//                 'name' => 'prices.ctv',
//                 'route'=> 'admin.prices.index',
//                 'glyphicon' => 'fas fa-list fa-fw',
//                 'permissions'   => ['prices.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'prices.agency',
//                 'route'=> 'admin.agency-prices.index',
//                 'glyphicon' => 'fas fa-cubes fa-fw',
//                 'permissions'   => ['agency_prices.read'],
//                 'hide' => false
//             ],
//         ]
//     ],
//     [
//         'name' => 'flash-sales.label',
//         'glyphicon' => 'fa fa-gifts fa-fw',
//         'hide' => false,
//         'permissions'   => ['flash_sales.read', 'sales.read'],
//         'child'=> [
//             [
//                 'name' => 'list',
//                 'route'=> 'admin.flash-sales.index',
//                 'glyphicon' => 'fa fa-list fa-fw',
//                 'permissions'   => ['flash_sales.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'flash-sales.sale',
//                 'route'=> 'admin.sales.index',
//                 'glyphicon' => 'fa fa-gift fa-fw',
//                 'permissions'   => ['sales.read'],
//                 'hide' => false
//             ],
//         ]
//     ],
//     [
//         'name' => 'banner',
//         'glyphicon' => 'far fa-images fa-fw',
//         'permissions'   => ['sliders.read', 'banner_advertisements.read'],
//         'hide' => false,
//         'child'=> [
//             [
//                 'name' => 'slider_popups.slider',
//                 'route'=> 'admin.sliders.index',
//                 'glyphicon' => 'fas fa-sliders-h fa-fw',
//                 'permissions'   => ['sliders.read'],
//                 'hide' => false
//             ],
//             [
//                 'name' => 'banners.advertisement',
//                 'route'=> 'admin.banner-advertisements.index',
//                 'glyphicon' => 'fa fa-folder fa-fw',
//                 'permissions'   => ['banner_advertisements.read'],
//                 'hide' => false
//             ],
//         ]
//     ],
//     [
//         'name' => 'utility',
//         'glyphicon' => 'fa fa-wrench fa-fw',
//         'permissions'   => ['mailboxes.read'],
//         'hide' => false,
//         'child'=> [
//             [
//                 'name' => 'marketings.mailbox',
//                 'route'=> 'admin.mailboxes.index',
//                 'glyphicon' => 'fas fa-mail-bulk fa-fw',
//                 'permissions'   => ['mailboxes.read'],
//                 'hide' => false
//             ],
//         ]
//     ],
//     [
//         'name' => 'users.label',
//         'glyphicon' => 'fas fa-user-shield fa-fw',
//         'hide' => false,
//         'role'   => 'system',
//         'child'=> [
//             [
//                 'name' => 'list',
//                 'route'=> 'admin.users.index',
//                 'glyphicon' => 'fa fa-list fa-fw',
//                 'hide' => false
//             ],
//         ]
//     ],
//     [
//         'name' => 'reports.label',
//         'route'=> 'admin.reports.index',
//         'glyphicon' => 'far fa-file-excel fa-fw',
//         'permissions'   => ['reports.read'],
//         'hide' => false
//     ],
//     [
//         'name' => 'settings.label',
//         'glyphicon' => 'fa fa-cog fa-fw',
//         'hide' => false,
//         'role'   => 'system',
//         'child'=> [
//             [
//                 'name' => 'settings.location',
//                 'route'=> 'admin.locations.index',
//                 'glyphicon' => 'fa fa-map fa-fw',
//                 'hide' => false,
//                 'role'   => 'system',
//             ],
//             [
//                 'name' => 'settings.redis',
//                 'route'=> 'admin.caches.redis',
//                 'glyphicon' => 'fas fa-sync fa-fw',
//                 'role'   => 'system',
//                 'hide' => false
//             ],
//             [
//                 'name' => 'settings.static_page',
//                 'route'=> 'admin.static-pages.index',
//                 'glyphicon' => 'fab fa-buffer fa-fw',
//                 'hide' => false
//             ],
//         ]
//     ],
// ];
];
