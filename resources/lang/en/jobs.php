<?php

return [
    'label'         => 'Jobs',
    'job_type'  => [
        'label' => 'Type',
        App\Defines\Job::TYPE_0  => 'AE - Air export',
        App\Defines\Job::TYPE_1  => 'AI - Air Import ',
        App\Defines\Job::TYPE_2  => 'MS - Miscellaneous',
        App\Defines\Job::TYPE_3  => 'SE - Sea Export',
        App\Defines\Job::TYPE_4  => 'SI - Sea Import',
        App\Defines\Job::TYPE_5  => 'TR - Transport',
        App\Defines\Job::TYPE_6  => 'WH - Warehouse',
    ],
    'job_group' => [
        'label' => 'Group',
        App\Defines\Job::GROUP_0  => 'NORMAL',
        App\Defines\Job::GROUP_1  => 'HOUSE ',
    ],
    'job_month' => 'Month',
    'job_year'  => 'Year',
    'branch_code' => 'Branch code',
    'code'  => 'Code',
];
