<?php 
    return [
        'label' => 'Tax',
        'taxation' => 'Taxation',
        'unit_price' => 'Unit price',
        'exchange' => 'Exchange',
        'exchange_taxation' => 'Exchange taxation',
        'group_unit' => 'Group unit',
        'invoice_no' => 'Invoice no',
        'date_invoice' => 'Invoice date',
        'invoice_date' => 'Invoice date',
        'object_name'  => 'Object name',
        'object_code'  => 'Object code',
        'partner_id'  => 'Object code',
        'tax_code'     => 'Tax code',
        'account_tax'  => 'Account tax',
        'job_code' => 'Job code',
        'job_code_id' => 'Job code',
        'branch_code' => 'Branch code',
        'bank_account' => 'Bank Account',
        'taxation_rate' => 'Taxation rate',
        'description' => 'Description',
        'explain' => 'Explain',
        'tax_rate'            => [
            'label' => 'Tax rate',
            App\Defines\Tax::TAX_VALUE_0   => '0%',
            App\Defines\Tax::TAX_VALUE_10  => '10%',
        ],
		'exchange_unit' => 'Exchange Unit',
		'exchange_tax' => 'Exchange Tax',
    ]


?>