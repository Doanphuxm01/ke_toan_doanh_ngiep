<?php
    return [
        'description'          => 'Description',
        'branch'               => 'Branch',
        'payment_account_bank' => 'Payment account bank',
        'receipt_account_bank' => 'Receipt account bank',
        'list_bank'            => 'List Bank',
        'abbreviations'        => 'Abbreviations',
        'desc'                 => 'Description',
        'label'                => 'Banks',
        'name'                 => 'name bank',
        'account_number'       => 'Account number',
        'province_city'        => 'Province/City',
        'account_holder'       => 'Account holder',
        'type'                 => 'Loại ngân hàng/thẻ',
        'user'                 => 'User',
        'fee_fixed'            => 'Fee fixed',
        'fee_percent'          => 'Fee percent',
        'raw_fee_fixed'        => 'Raw fee fixed',
        'raw_fee_percent'      => 'Raw fee percent',
        'code'                 => 'Code',
        'gateway'              => 'Gateway',
        'logo'                 => 'Logo',
        'online'               => 'Online',
        'qr_code'              => 'QR Code',
        'is_partner'           => 'Pay partner',
        'address'              => 'Address',
        'full_name'            => 'Full name',
        'types'           => [
            App\Define\Bank::TYPE_INTERNAL      => 'Thẻ nội địa',
            App\Define\Bank::TYPE_EXTERNAL      => 'Thẻ quốc tế',
        ],
        'users'           => [
            App\Define\Bank::USER_CUSTOMER      => 'Khách hàng thường',
            App\Define\Bank::USER_MERCHANT      => 'Đại lý',
            App\Define\Bank::USER_PARTNER       => 'Đối tác',
        ],
    ];
    

?>