<?php
    return [
        'types' => [
            App\Define\Workflow::TYPE_AIR    => 'Air',
            App\Define\Workflow::TYPE_SEA    => 'Sea',
            App\Define\Workflow::TYPE_DOM    => 'Domestic',
            App\Define\Workflow::TYPE_OTHER    => 'OTHER',
        ],
    ];