<?php

return [
    'label' => 'Dashboard',
    'role'  => 'Role',
    'fullname'  => 'Fullname',
    'email'     => 'Email',
    'last_login'    => 'Last login',
    'password'  => 'Password',
    'new_password'  => 'New password',
    're_password'   => 'Retype password',
    'change_password'   => 'Change password',
    'update_password'   => 'Update password',
    'current_password'  => 'Current password',
    'menu_is_collapse'  => 'Colapse menu bar',
    'roles'             => 'Roles',
    'changepwd'         => 'Change password',
];
