<?php

return [
    'label'         => 'Sale cost item',
    'code'      	=> 'Item code',
    'description' 	=> 'Description',
    'branch_code' 	=> 'Branch Code',
    'charge_type'	=> 'Charge type',
    'tax_value' 	=> 'Tax value',
    'sale_account_code' 	=> 'Sale account code',
    'cost_account_code' 	=> 'Cost account cost',
    'prov_account_code' 	=> 'Prov Account code',
    'tax_values'            => [
        'label' => 'Property',
        App\Define\SaleCostItem::TAX_VALUE_0   => '0%',
        App\Define\SaleCostItem::TAX_VALUE_10  => '10%',
    ],
];