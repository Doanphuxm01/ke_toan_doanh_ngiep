<?php 

return [
    'disposal_reason' => "Disposal reason",
    'voucher_date' => "Voucher date",
    'voucher_number' => "Voucher number",   
    'disposal_account' => "Disposal account",   
    'disposal_fixed_assets' => "Disposal fixed assets",   
];
