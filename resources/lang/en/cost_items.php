<?php 

return [
    'cost_item' => "Cost item",
    'description' => "Description",
    'interpretation' => "Interpretation",   
    'cannot_delete' => "Cannot delete because it is already in use",   
];
