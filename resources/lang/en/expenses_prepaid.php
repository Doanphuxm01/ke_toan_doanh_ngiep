<?php 

return [
    'code' => "Expenses prepaid code",
    'name' => "Expenses prepaid name",
    'voucher_date' => "Voucher date",   
    'voucher_no' => "Voucher no", 
    'desc' => "Description", 
    'amount' => "Amount",   
    'period_number' => "Period number",   
    'periodical_allocation_value' => "Periodical allocation value",   
    'allocation_account' => "Allocation account", 
    'stop' => "Stop allocate", 
    'voucher_collection' => "Voucher collection",
    'duplicate' => "Duplicated code",
    'confirm' => "Confirm",
    'save_message' => "The voucher money and the allocation money are not equal.",
    'confirm_save' => "Do you want to save?",
    'no_data' => "No data",
    'allocated_period_number' => "Allocated period number",
    'select_period' => "Select the period for the allocation of expenses prepaid",

];
