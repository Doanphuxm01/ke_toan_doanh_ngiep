<?php

return [
    'login' 	=> 'Login',
    'start_session' => 'Login to start session',
    'register' 	=> 'Register',
    'password'  => 'Password',
    'remember'  => 'Remember me?',
    'email'     => 'Email',
    'fullname'  => 'Fullname',
    'role'      => 'Role',
    'account'   => 'My account',
    'logout'    => 'Logout',
    'register_social'   => 'Login with ',
    'confirm_password' 	=> 'Confirm password',
    'forgot_password' 	=> 'Forgot password?',
    'new_password'      => 'New password',
    're_password'       => 'Retype password',
];