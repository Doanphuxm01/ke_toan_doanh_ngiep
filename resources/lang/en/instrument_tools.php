<?php

    return [
        'label' => 'Instrument & Tool',
        'name'  => 'Instrument & Tool name',
        'type'  => 'Instrument & Tool type',
        'code'  => 'Instrument & Tool code',
        'currency'  => 'Currency',
        'quantity'  => 'Quantity',
        'value'  => 'Value',
        'period_number'  => 'Period number',
        'periodical_allocation_value'  => 'Periodial allocation value',
        'unit_price'  => 'Unit price',
        'total_amount'  => 'Total amount',
        'reason'  => 'Reason',
        'allocation_account'  => 'Allocation account',
        'status' => 'Stop monitor',
        'equal_quantity' => 'Allocation quantity have to equal to total quantity',
        'select_period' => "Select the period for the allocation of instrument tools",
        'total_allocation_amount' => "Total allocation amount",
        'residual_value' => "Residual value"

    ];