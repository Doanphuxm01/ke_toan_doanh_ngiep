<?php 

return [
    'department_id' => "Department ID",
    'general_information' => "General Information",
    'depreciation_information' => "Depreciation information",
    'allocation_information' => "Allocation information",
    'source' => "Source",
    'voucher_number' => "Voucher number",
    'voucher_creation_date' => "Voucher creation date",
    'asset_code' => "Asset code",
    'asset_name' => "Asset name",
    'original_price_account' => "Original price account",
    'depreciation_account' => "Depreciation account",
    'original_price' => "Original price",
    'depreciation_value' => "Depreciation value",
    'begin_use_date' => "Begin use date",
    'use_time' => "Use time (month)",
    'residual_time' => "Residual time (month)",
    'monthly_depreciation_rate' => "Monthly depreciation rate",
    'yearly_depreciation_rate' => "Yearly depreciation rate",
    'monthly_depreciation_value' => "Monthly depreciation value",
    'yearly_depreciation_value' => "Yearly depreciation value",
    'accumulated_depreciation' => "Accumulated depreciation",
    'residual_value' => "Residual value",
    'use_department' => "Use department",
    'allocation_rate' => "Allocation rate",
    'expense_account' => "Expense account",
    'cost_item' => "Cost item",
    'supply_unit' => "Supply unit",
    'invoice_code' => "Invoice code",
    'purchase_date' => "Purchase date",   
    'create_new_row' => "Create new row",  
    'create_new_cost_item' => "Create new cost item",  
    'remove_this_row' => "Remove this row",
    'already_100' => "The allocation rate has reached 100%",
    'must_100' => "The allocation rate must reach 100%",
    'should_100' => "The allocation rate should not exceed 100%",
    'save_message' => "The voucher money and the depreciation money are not equal.",
    'date_range' => "Date range",
    'line_number' => "Line number",
    'periodical_depreciation_value' => "Period depreciation value",
];
