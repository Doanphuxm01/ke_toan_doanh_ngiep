<?php

    return [
        'label' => 'Accounting Invoice',
        'voucher_no'  => 'Voucher no',
		'voucher_date'  => 'Voucher Date',
        'payment_term'  => 'Payment term',
        'exchange_rate'  => 'Exchange rate',
        'desc'=> 'Description',
        'currency'    => 'Currency',
        'status'     => 'Status',
    ];