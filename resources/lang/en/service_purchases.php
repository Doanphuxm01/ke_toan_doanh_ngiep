<?php
return [
    'label' => 'Voucher of service purchase',
    'vendor' => 'Vendor',
    'type' => 'Type',
    'tax_code' => 'Tax code',
    'description' => 'Description',
    'invoice_no' => 'Invoice no',
    'invoice_date' => 'Invoice date',
    'time_for_payment' => 'Due date',
    'settlement_date' => 'Settlement date',
    'voucher_no' => 'Voucher no',
    'voucher_date' => 'Voucher date',
    'desc' => 'Description',
    'exchange_rate'  => 'Exchange rate',
    'currency'  => 'Currency',
    'amount_service' => 'Amount service',
    'total_payment' => 'Total payment',
    'vat_amount' => 'VAT amount',
    'total_amount' => 'Total amount',
    'day_voucher' => 'Day Voucher',
    'export_excel' => 'Export excel',
    'status' => [
        App\Defines\ServicePurchase::STATUS_DRAFT    	=> 'DRAFT',
        App\Defines\ServicePurchase::STATUS_REJECTED    => 'REJECTED',
        App\Defines\ServicePurchase::STATUS_APPROVED    => 'APPROVED',
        App\Defines\ServicePurchase::STATUS_SAVE    	=> 'SAVE',
    ],
    'number_of_days_owed' => 'Number of days owed',
    'buy_service_of' => 'Buy service of ',
]

?>