<?php 
    return [
        'label' => 'Accounting',
        'sale_code_item' => 'Sale code item',
        'description' => 'Description',
        'quantity'  => 'Quantity',
        'credit_account' => 'Credit account',
        'debit_account' => 'Debit account',
        'price_unit' => 'Price Unit',
        'amount' => 'Amount',
        'exchange' => 'Exchange',
        'branch_code' => 'Branch code',
        'bank_account' => 'Bank account',
        'job_code' => 'Job code',
        'job_code_id' => 'Job code',
        'vat' => "Vat",
        'with_amount' => 'With amount',
        'object_name' => 'Object name',
        'object_code' => 'Object code',
		'debit_object' => 'Debit object',
		'credit_object' => 'Credit object',
		'cost_item' => 'Cost item'
    ]


?>