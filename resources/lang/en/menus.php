<?php

return [
    'logout'    => 'Logout',
    'home'      => 'Home',
    'list'      => 'List',
    'reports'   => [
        'label' => 'Report',
    ],
    'fixed_assets'      => [
        'label'         => 'Fixed asset',
    ],

    'account' => [
        'label' => 'Accounts',
    ],
    'sale-cost-items'   => [
        'label'         => 'Sale cost items',
    ],
    'fixed-assets'  => [
        'label'     => 'Fixed assets',
    ],
    'fixed-asset-depreciation'  => [
        'label'     => 'Fixed asset depreciation',
    ],
    'fixed-asset-disposal'  => [
        'label'     => 'Fixed asset disposal',
    ],
    'fixed-asset-reports'  => [
        'label'     => 'Fixed asset report',
    ],
    'expenses-prepaid'  => [
        'label'     => 'Expenses prepaid',
    ],
    'expenses-prepaid-allocation'  => [
        'label'     => 'Expenses prepaid allocation',
    ],
    'instrument-tools'  => [
        'label'     => 'Instrument & Tool',
    ],
    'instrument-tool-allocation'  => [
        'label'     => 'Instrument & Tool allocation',
    ],
    'instrument-tool-disposal'  => [
        'label'     => 'Instrument & Tool disposal',
    ],
    'instrument-tool-types'  => [
        'label'     => 'Instrument & Tool type',
    ],
    'cost-items'  => [
        'label'     => 'Cost item',
    ],
    'jobs' => [
        'label' => 'Jobs'
    ],
    'report' => [
        'label' => 'Report'
    ],
    'partners' => [
        'label' => 'Partners'
    ],
    'input-invoices'    => [
        'label'         => 'Input Invoices',
    ],
    'payment-receipt-vouchers' => [
        'label'   => 'Payment Receipt Vouchers',
        'label_1' => 'Payment Vendor',
        'label_2' => 'Receipt Bank',
        'label_3' => 'Payment Order',
    ],
    'banks' => [
        'label'   => 'Banks',
        'label_1' => 'BankAccounts',
        'label_2' => 'List Banks'
    ],
	'accounting-invoices' => [
		'label' => 'Accounting Invoice'
	],
    'service-purchase' => [
        "label" => 'Voucher of service purchase'
    ],
    
];
