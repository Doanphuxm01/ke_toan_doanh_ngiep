<?php 
    return [
        'label'            => 'Dept Voucher',
        'voucher_no'       => 'Voucher no',
        'voucher_date'     => "Voucher date",
        'invoice_date'     => 'Invoice date',
        'invoice_no'       => 'Invoice no',
        'explain'          => 'Explain',
        'time_for_payment' => 'Due date',
        'amount'           => 'Amount',
        'exchange'         => 'Exchange',
        'account_receipt'  => 'Account Receipt',
        'choose_voucher'   => 'Choose Voucher',
        'account_payment'  => 'Account Payment',
        
    ];

?>