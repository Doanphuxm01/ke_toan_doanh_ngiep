<?php
    return [
        'label'     => 'Mailbox',
        'compose'   => 'Soạn mail',
        'contact'   => 'Danh bạ',
        'address'   => 'Người nhận',
        'content'   => 'Nội dung',
        'subject'   => 'Tiêu đề',
        'template'  => 'Mẫu email',
        'description'   => 'Mô tả',
    ];
?>