<?php

return [
    'label' 	=> 'Đánh giá/Nhận xét',
    'rating'	=> 'Số sao',
    'comment'	=> 'Nội dung',
    'product' 	=> 'Sản phẩm',
    'image'		=> 'Ảnh',
    'reply'		=> 'Trả lời',
];
