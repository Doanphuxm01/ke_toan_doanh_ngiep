<?php
    return [
        'label'     => 'Kho hàng',
        'province'  => 'Khu vực',
        'address'   => 'Địa chỉ',
        'code'      => 'Mã kho',
        'note'		=> 'Ghi chú',
        'phone' => 'ĐT liên hệ',
        'name' => 'Tên kho',
    ];
