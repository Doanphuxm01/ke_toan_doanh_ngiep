<?php

return [
    'label'         => 'Hãng tàu',
    'name'      	=> 'Tên hãng',
    'description'	=> 'Mô tả',
    'phone'         => 'Điện thoại',
    'email'         => 'Email',
    'address'       => 'Địa chỉ',
];
