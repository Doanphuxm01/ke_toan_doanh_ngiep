<?php

return array(
    'label'                         => 'Danh mục tin',
    'name'                          => 'Tên danh mục',
    'parent_category'               => '-- Chọn một danh mục làm danh mục cha --',
    'parent'                        => 'Danh mục cha',
    'root'                          => 'Là danh mục gốc',
    'is_single'                     => 'Danh mục 1 bài viết',
    'is_system'                     => 'Danh mục hệ thống',
    'summary'                       => 'Tóm tắt',
);
