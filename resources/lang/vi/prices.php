<?php
    return [
        'label'     => 'Giá sản phẩm',
        'category'  => 'Danh mục',
        'product'   => 'Sản phẩm',
        'partner_percent'  => 'Giá cho CTV',
        'dropship_percent' => 'Giá Dropship',
        'customer_percent' => 'Giá cho Khách',
        'price_ipo' => 'Giá niêm yết',
        'partner_amount'  => 'Giá cho CTV',
        'customer_amount' => 'Giá cho Khách',
        'dropship_amount' => 'Giá Dropship',
        'customer_percent_default'  => 'Giá cho Khách',
        'customer_amount_default'   => 'Giá cho Khách',
        'ipo'   => 'Giá niêm yết',
        'note'  => 'Ghi chú',
        'apply' => 'Áp dụng',
        'type'      => 'Phân loại',
        "types"     => [
            App\Define\Price::TYPE_CATEGORY => 'C.K mặc định/Danh mục',
            App\Define\Price::TYPE_PRODUCT  => 'C.K chi tiết/Sản phẩm',
        ],
        'sources'   => [
            App\Define\Price::SOURCE_ADMIN    => 'Quản trị',
            App\Define\Price::SOURCE_PARTNER  => 'Cộng tác viên',
        ],
    ];
