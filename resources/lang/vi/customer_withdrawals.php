<?php

return [
    'label' => 'Rút tiền',
    'amount'=> 'Số tiền',
    'note'  => 'Ghi chú',
    'files' => 'File đính kèm',
    'status'           => [
        'label' => 'Trạng thái',
        App\Define\Customer::WITHDRAWAL_STATUS_REQUESTED  => 'Gửi yêu cầu',
        App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING => 'Đang xử lý',
        App\Define\Customer::WITHDRAWAL_STATUS_REJECTED   => 'Từ chối',
        App\Define\Customer::WITHDRAWAL_STATUS_SUCCESS    => 'Thành công',
    ],
];