<?php

return [
    'label' => 'Tài khoản ví',
    'amount'=> 'Giá trị',
    'note'  => 'Ghi chú',
    'files' => 'File đính kèm',
    'sources' 		=> [
        'label' 	=> 'Tạo bởi',
        App\Define\CustomerTransaction::SOURCE_ADMIN 	=> 'Quản trị',
        App\Define\CustomerTransaction::SOURCE_PARTNER 	=> 'Cộng tác viên',
        App\Define\CustomerTransaction::SOURCE_SYSTEM 	=> 'Hệ thống',
    ],
    'types' 		=> [
        'label' 	=> 'Phân loại',
        App\Define\CustomerTransaction::COIN_TYPE_REFILL 		=> 'Nạp tiền',
        App\Define\CustomerTransaction::COIN_TYPE_TRANSACTION 	=> 'Mua đơn hàng',
        App\Define\CustomerTransaction::COIN_TYPE_COMMISSION   	=> 'Hoa hồng',
    ],
];