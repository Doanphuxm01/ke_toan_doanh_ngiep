<?php

return [
    'label' 			=> 'Banner',
    'href'				=> 'Liên kết',
    'name'				=> 'Tiêu đề',
    'image'				=> 'Ảnh banner',
    'type'				=> 'Kiểu banner',
    'types'				=> [
        App\Define\Constant::IMAGE_TYPE_BANNER_TOP         => 'Banner trên cùng menu',
        App\Define\Constant::IMAGE_TYPE_BANNER_RIGHT_SLIDE => 'Banner bên phải slide trang chủ',
        App\Define\Constant::IMAGE_TYPE_BANNER_MIDDLE      => 'Banner dưới flash sale trang chủ',
        App\Define\Constant::IMAGE_TYPE_BANNER_BOTTOM      => 'Banner dưới cùng trang chủ/trên thương hiệu',
        App\Define\Constant::IMAGE_TYPE_BANNER_EMPTY_CART  => 'Banner khi giỏ hàng trống',
    ],
];
