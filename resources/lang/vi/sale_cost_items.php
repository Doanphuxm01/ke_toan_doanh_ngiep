<?php

return [
    'label'         => 'Sale cost item',
    'fullname'      => 'Họ và tên',
    'phone'         => 'Điện thoại',
    'code'          => 'Mã khách hàng',
    'code_placeholder' => 'Chỉ bao gồm chữ,số, dấu _-.',
    'email'         => 'Email',
    'gender'        => 'Giới tính',
    'dob'           => 'Năm sinh',
];
