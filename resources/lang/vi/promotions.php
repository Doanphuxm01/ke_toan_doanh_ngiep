<?php

return array(
    'label' 		=> 'Khuyến mãi',
    'name'			=> 'Tên chương trình',
    'note'		    => 'Ghi chú',
    'from_date'     => 'Từ ngày',
    'to_date'       => 'Tới ngày',
    'date'          => 'Thời gian',
    'products'      => 'Sản phẩm',
    'image'         => 'Ảnh chương trình',
    'product_ids'	=> 'Sản phẩm',
    'number_of_product'	=> 'Số sản phẩm',
    'percent'       => 'Phần trăm',
    'category'      => 'Danh mục KM',
    'auto_loop'     => 'Tự lặp lại',
);
