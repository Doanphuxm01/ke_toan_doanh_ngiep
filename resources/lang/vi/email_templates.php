<?php
    return [
        'label'     => 'Mẫu email',
        'name'      => 'Tên mẫu email',
        'content'   => 'Nội dung',
        'status'           => [
            App\Define\Email::TEMPLATE_STATUS_PUBLISHED     => 'Xuất bản',
            App\Define\Email::TEMPLATE_STATUS_DRAFT         => 'Bản nháp',
        ],
    ];
?>