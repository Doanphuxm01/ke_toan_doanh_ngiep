<?php

return [
    'logout'    => 'Đăng xuất',
    'home'      => 'Quản trị',
    'list'      => 'Danh sách',
    'product'           => 'Sản phẩm',
    // 'order'             => 'Đơn hàng',
    'marketing'         => 'Marketing',
    'customers'    => [
        'label'         => 'Khách hàng',
        'transaction'   => 'Tài khoản ví',
        'withdrawal'    => 'Rút tiền',
    ],
    'flash-sales'   => [
        'label'     => 'Khuyến mãi',
        'sale'      => 'Chi tiết',
    ],
    'show-account-child' =>[
        'label' => 'Danh sách tài khoản',
    ],
    'utility'   => 'Tiện ích',
    'system'    => 'Hệ thống',
    'warehouses' => [
        'label'     => 'Kho hàng',
        'inventory' => 'Tồn kho',
    ],
    'users'     => [
        'label' => 'Quản trị',
        'role'  => 'Vai trò',
    ],
    'settings'  => [
        'label' => 'Cài đặt',
        'redis' => 'Cache',
        'static_page'   => 'Trang tĩnh',
        'location'      => 'Tỉnh/Thành phố',
    ],
    'reports'   => [
        'label' => 'Báo cáo',
    ],

    'attributes'            => [
        'label'         => 'Thuộc tính Sản phẩm',
        'value'         => 'Giá trị Thuộc tính',
        'list'          => 'Danh sách Thuộc tính',
        'category'      => 'Gán Thuộc tính cho Danh mục',
    ],
    'prices'   => [
        'label'         => 'Quản lý giá',
        'ctv'           => 'Giá ctv + dropship',
        'agency'        => 'Giá sỉ'
    ],
    'partners'  => [
        'request'   => 'Yêu cầu nâng cấp TK',
        'condition' => 'Điều kiện nâng cấp TK',
    ],
    'products'          => [
        'label'         => 'Sản phẩm',
        'price'         => 'Giá sản phẩm',
        'list'          => 'Danh sách',
        'category'      => 'Quản lý Danh mục',
        'category_sort' => 'Sắp xếp Danh mục',
        'brand'         => 'Quản lý Thương hiệu',
        'supplier'      => 'Quản lý Nhà cung cấp',
        'brand_category'    => 'Gán Thương hiệu cho Danh mục',
        'supplier_category' => 'Gán Nhà cung cấp cho Danh mục',
        'promotion'     => 'Chương trình khuyến mãi',
        'wrap'          => 'Gom nhóm sản phẩm',
        'review'        => 'Đánh giá/Nhận xét',
        'agency-price'  => 'Giá sỉ',
        'bulk_size'     => 'Kích thước',
    ],
    'orders'            => [
        'label'         => 'Đơn hàng',
        'ship_location' => 'Phí ship Sube',
        'bank'          => 'Ngân hàng/Thẻ',
        'gateway'       => 'Cổng thanh toán',
        'delivery'      => 'Vận chuyển',
        'customer'      => 'Khách hàng',
        'coupon'        => 'Mã giảm giá',
    ],
    'marketings'            => [
        'label'         => 'Marketing',
        'contact'       => 'Danh bạ',
        'email'         => "Email",
        'affiliate'     => "Affiliate Icon",
        'newsletter'    => 'Newsletter',
        'feedback'      => 'Liên hệ/Phản hồi KH',
        'vmg'           => 'VMG - Thử vận may',
        'campaign'      => 'Chiến dịch',
        'email_template'    => 'Mẫu email',
        'email_tag'     => 'Email Tag',
        'email_validation'  => 'Kiểm tra email',
        'mailbox'       => 'Mailbox',
    ],
    'news'              => [
        'label'         => 'Tin tức',
        'category'      => 'Danh mục tin',
        'image'         => 'Tin ảnh',
        'static_page'   => 'Trang tĩnh',
    ],


    'banners'   => [
        'advertisement' => 'Banner',
    ],
    'slider_popups'   => [
        'label'         => 'Popup & Slider',
        'slider'        => 'Slider',
        'popup'         => 'Popup',
    ],
    'news'              => [
        'label'         => 'Tin tức',
        'category'      => 'Danh mục tin',
        'image'         => 'Tin ảnh',
        'static_page'   => 'Trang tĩnh',
    ],
    'banner'    => 'Banner',
    'sale'  => 'Khuyến mãi',

    'fixed_assets'   => [
        'label'         => 'Tài sản cố định',
    ],
   
];
