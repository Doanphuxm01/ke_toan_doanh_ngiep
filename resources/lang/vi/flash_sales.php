<?php
return [
    'label' 		=> 'Khuyến mãi',
    'name'			=> 'Tên chương trình',
    'note'		    => 'Ghi chú',
    'from_date'     => 'Từ ngày',
    'to_date'       => 'Tới ngày',
    'auto_loop'     => 'Tự lặp lại',
    'image'	=> 'Ảnh chương trình',
	'time_range'	=> 'Khoảng thời gian',
	'is_flashsale'	=> 'Flash sale',
];