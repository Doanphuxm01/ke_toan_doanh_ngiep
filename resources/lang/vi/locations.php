<?php

return [
    'label' 		=> 'Tỉnh/Thành phố',
    'province'		=> 'Tỉnh/Thành phố',
    'district'		=> 'Quận/Huyện',
    'location'      => 'Địa điểm',
    // 'province'  => 'Thành phố',
    'district'  => 'Quận',
];
