<?php
    return [
        'label'     => 'Tồn kho',
        'quantity'  => 'Số lượng',
        'province'  => 'Khu vực',
        'category'  => 'Kho hàng',
        'address'   => 'Địa chỉ',
        'note'		=> 'Ghi chú',
        'type'      => 'Phân loại',
        "types"     => [
            App\Define\Inventory::TYPE_IN_STOCK     => 'Nhập kho',
            App\Define\Inventory::TYPE_OUT_STOCK    => 'Xuất kho',
        ],
    ];
