<?php
return [
    'label'             => 'Báo cáo',
    'types'         => [
        'label'         => '--------- Chọn loại báo cáo ---------',
        \App\Define\Report::PRODUCT_ALL 	=> 'Tất cả sản phẩm',
        \App\Define\Report::PRODUCT_BEST_SELLER => 'Sản phẩm bán chạy',
        \App\Define\Report::ORDER_PROFIT	=> 'Lợi nhuận',
        \App\Define\Report::ORDER_ALL		=> 'Đơn hàng',
        \App\Define\Report::ORDER_CPN		=> 'Chuyển phát nhanh',
        \App\Define\Report::ORDER_BANK_NL 	=> 'Ngân lượng',
    ],
];