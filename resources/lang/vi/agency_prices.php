<?php
return [
    'label'     => 'Giá sỉ',
    'product'   => 'Sản phẩm',
    'price_ipo' => 'Giá sỉ',
    'greater_than_equal' => 'Lớn hơn hoặc bằng',
    'less_than' => 'Nhỏ hơn',
    'price_list'	=> 'Bảng giá',
];
