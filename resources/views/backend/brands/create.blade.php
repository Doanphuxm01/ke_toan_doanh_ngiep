@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('brands.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('brands.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.brands.index') !!}">{!! trans('brands.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.brands.store'), 'role' => 'form', 'files' => true]) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle">
                    {!! trans('brands.name') !!}
                </th>
                <td>
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('brands.href') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('href', old('href'), ['class' => 'form-control', 'maxlength' => 255]) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">{!! trans("brands.logo") !!}<br/>
                    ({!! \App\Define\Constant::IMAGE_BRAND_WIDTH !!}x{!! \App\Define\Constant::IMAGE_BRAND_HEIGHT !!})</th>
                <td>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="height: {!! \App\Define\Constant::IMAGE_BRAND_HEIGHT !!}px; width: {!! \App\Define\Constant::IMAGE_BRAND_WIDTH !!}px;">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">
                                    {!! trans('system.action.select_image') !!}
                                </span>
                                {!! Form::file('logo') !!}
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                {!! trans('system.action.remove') !!}
                            </a>
                        </div>
                    </div>
                </td>
                <th class="text-right">
                    {!! trans('brands.summary') !!}
                </th>
                <td>
                    {!! Form::textarea('summary', old('summary'), ['class' => 'form-control', 'rows' => 5, 'maxlength' => 255]) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">{!! trans("brands.image1") !!}<br/>
                    ({!! \App\Define\Constant::IMAGE_BRAND_IMG1_WIDTH !!}x{!! \App\Define\Constant::IMAGE_BRAND_IMG1_HEIGHT !!})</th>
                <td colspan="3">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="height: {!! \App\Define\Constant::IMAGE_BRAND_IMG1_HEIGHT/\App\Define\Constant::IMAGE_BRAND_RATIO !!}px; width: {!! \App\Define\Constant::IMAGE_BRAND_IMG1_WIDTH/\App\Define\Constant::IMAGE_BRAND_RATIO !!}px;">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">
                                    {!! trans('system.action.select_image') !!}
                                </span>
                                {!! Form::file('image1') !!}
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                {!! trans('system.action.remove') !!}
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">{!! trans("brands.image2") !!}<br/>
                    ({!! \App\Define\Constant::IMAGE_BRAND_IMG2_WIDTH !!}x{!! \App\Define\Constant::IMAGE_BRAND_IMG2_HEIGHT !!})</th>
                <td colspan="3">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="height: {!! \App\Define\Constant::IMAGE_BRAND_IMG2_HEIGHT/\App\Define\Constant::IMAGE_BRAND_RATIO !!}px; width: {!! \App\Define\Constant::IMAGE_BRAND_IMG2_WIDTH/\App\Define\Constant::IMAGE_BRAND_RATIO !!}px;">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">
                                    {!! trans('system.action.select_image') !!}
                                </span>
                                {!! Form::file('image2') !!}
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                {!! trans('system.action.remove') !!}
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">{!! trans("brands.image3") !!}<br/>
                    ({!! \App\Define\Constant::IMAGE_BRAND_IMG3_WIDTH !!}x{!! \App\Define\Constant::IMAGE_BRAND_IMG3_HEIGHT !!})</th>
                <td colspan="3">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="height: {!! \App\Define\Constant::IMAGE_BRAND_IMG3_HEIGHT/\App\Define\Constant::IMAGE_BRAND_RATIO !!}px; width: {!! \App\Define\Constant::IMAGE_BRAND_IMG3_WIDTH/\App\Define\Constant::IMAGE_BRAND_RATIO !!}px;">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">
                                    {!! trans('system.action.select_image') !!}
                                </span>
                                {!! Form::file('image3') !!}
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                {!! trans('system.action.remove') !!}
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('system.seo_keywords') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('seo_keywords', old('seo_keywords'), ['class' => 'form-control', 'maxlength' => 50]) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('system.seo_description') !!}
                </th>
                <td colspan="3">
                    {!! Form::textarea('seo_description', old('seo_description'), ['class' => 'form-control', 'rows' => 2, 'maxlength' => 255]) !!}
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="4">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', 1), ['class' => 'minimal' ]) !!}
                        {!! trans('system.status.active') !!}
                    </label>
                    &nbsp;&nbsp;
                    <label>
                        {!! Form::checkbox('is_top', 1, old('is_top', 0), [ 'class' => 'minimal-red' ]) !!}
                        {!! trans('brands.is_top') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    {!! HTML::link(route( 'admin.brands.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>

    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $('input[type="checkbox"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red'
                });
            });
        }(window.jQuery);
    </script>
@stop