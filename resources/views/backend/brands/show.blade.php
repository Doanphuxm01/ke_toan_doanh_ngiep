@extends('backend.master')
@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('brands.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('brands.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($brand->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.brands.index') !!}">{!! trans('brands.label') !!}</a></li>
        </ol>
    </section>
    <table class='table borderless'>
        <tr>
            <th class="table_right_middle">
                {!! trans('brands.name') !!}
            </th>
            <td>
                {!! $brand->name !!}
            </td>
            <th class="table_right_middle">
                {!! trans('brands.href') !!}
            </th>
            <td colspan="3">
                {!! $brand->href !!}
            </td>
        </tr>
        <tr>
            <th class="text-right">{!! trans("brands.logo") !!}<br/>
                ({!! \App\Define\Constant::IMAGE_BRAND_WIDTH !!}x{!! \App\Define\Constant::IMAGE_BRAND_HEIGHT !!})</th>
            <td>
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-preview thumbnail" style="height: {!! \App\Define\Constant::IMAGE_BRAND_HEIGHT !!}px; width: {!! \App\Define\Constant::IMAGE_BRAND_WIDTH !!}px;">
                        <img src="{!! asset($brand->logo) !!}">
                    </div>
                </div>
            </td>
            <th class="text-right">
                {!! trans('brands.summary') !!}
            </th>
            <td>
                {!! $brand->summary !!}
            </td>
        </tr>
        <tr>
            <th class="text-right">
                {!! trans('system.seo_keywords') !!}
            </th>
            <td colspan="3">
                {!! $brand->seo_keywords !!}
            </td>
        </tr>
        <tr>
            <th class="text-right">
                {!! trans('system.seo_description') !!}
            </th>
            <td colspan="3">
                {!! $brand->seo_description !!}
            </td>
        </tr>
    </table>
@stop