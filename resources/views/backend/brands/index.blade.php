@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('brands.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('brands.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.brands.index') !!}">{!! trans('brands.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.brands.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('name', trans('brands.name')) !!}
                        {!! Form::text('name', Request::input('name'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('is_top', trans('brands.is_top')) !!}
                        {!! Form::select('is_top', [ '' => trans('system.dropdown_all'), 0 => trans('system.no'), 1 => trans('system.yes') ], Request::input('new'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label')) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            @permission('brands.create')
            <a href="{!! route('admin.brands.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp;{!! trans('system.action.create') !!}
            </a>
            @endpermission
        </div>
        <div class="col-md-10 text-right">
            {!! $brands->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($brands) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($brands->currentPage() - 1) * $brands->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $brands->count()) . ' ( ' . trans('system.total') . ' ' . $brands->total() . ' )' !!}
                    | <i>Chú giải: </i>
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </span>&nbsp;&nbsp;
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class='table table-striped table-bordered tree'>
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;"></th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('brands.logo') !!} </th>
                        <th style="text-align: center1; vertical-align: middle;"> {!! trans('brands.name') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('brands.is_top') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('brands.number_of_product') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                    </tr>
                </thead>
                <tbody class="borderless">
                    @foreach ($brands as $item)
                        <tr class="treegrid-{!! $item->id !!}">
                            <td style="text-align: center; width: 3%; vertical-align: middle;"></td>
                            <td  style="text-align: center; vertical-align: middle;">
                                @if($item->logo)
                                    <img src="{!! asset($item->logo) !!}" height="30px">
                                @endif
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{!! route('admin.brands.show', $item->id) !!}" title="{!! trans('system.action.detail') !!}">
                                    {!! $item->name !!}
                                </a>
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->is_top == 0)
                                <span class="label label-danger"><span class='glyphicon glyphicon-remove'></span></span>
                                @elseif($item->is_top == 1)
                                <span class="label label-success"><span class='glyphicon glyphicon-ok'></span></span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <?php $productCount = $item->products()->count(); ?>
                                <span class="label label-{!! $labels[$productCount%4] !!}"> {!! $productCount !!} </span>
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                <span class="label label-danger"><span class='glyphicon glyphicon-remove'></span></span>
                                @elseif($item->status == 1)
                                <span class="label label-success"><span class='glyphicon glyphicon-ok'></span></span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @permission('brands.update')
                                    <a href="{!! route('admin.brands.edit', $item->id) !!}" class="btn btn-xs btn-default">
                                        <i class="text-warning glyphicon glyphicon-edit"></i>
                                    </a>
                                @endpermission
                                @permission('brands.delete')
                                    <a href="javascript:void(0)" link="{!! route('admin.brands.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                        <i class="text-danger glyphicon glyphicon-remove"></i>
                                    </a>
                                @endpermission
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function(){
                $(".select2").select2({'width': '100%'});
            });
        }(window.jQuery);
    </script>
@stop