@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('customer_withdrawals.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('customer_withdrawals.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.customer-withdrawals.index') !!}">{!! trans('customer_withdrawals.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bconsumer">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.customer-withdrawals.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('customer', trans('customers.label')) !!}
                        {!! Form::select('customer', empty($customer) ? ['' => trans('system.dropdown_all')] : $customer, Request::input('customer'), ["class" => "form-control"]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('date_range', trans('system.update_range')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('date_range', Request::input('date_range'), ['class' => 'form-control pull-right date_range']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('customer_withdrawals.status.label')) !!}
                        {!! Form::select('status', [-1 => trans('system.dropdown_all')] + App\Define\Customer::getWithdrawalStatusForOption(), Request::input('status'), ["class" => "form-control select2"]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('page_num', trans('system.page_num')) !!}
                        {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10 text-right">
            {!! $withdrawals->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($withdrawals) > 0)
    <div class="box">
        <div class="box-header">
            <?php $i = (($withdrawals->currentPage() - 1) * $withdrawals->perPage()) + 1; ?>
            <div class="form-inline row">
                <div class="pull-right1 form-group col-md-81">
                    <span id="counterSelected" class="badge">0</span>
                        <b>{!! trans('system.itemSelected') !!}</b> | {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $withdrawals->count()) . ' ( ' . trans('system.total') . ' ' . $withdrawals->total() . ' )' !!}
                    | <i>Chú giải: </i>&nbsp;&nbsp;
                    <span class="text-success"><i class="far fa-eye"></i> {!! trans('system.action.detail') !!}</span>
                </div>
                <div class="pull-right form-group col-md-41">
                    {!! Form::select('action', ['noaction' => trans('system.action.label'), 'excel' => 'Tải file excel', \App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING => trans('customer_withdrawals.status.' . \App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING), \App\Define\Customer::WITHDRAWAL_STATUS_REJECTED => trans('customer_withdrawals.status.' . \App\Define\Customer::WITHDRAWAL_STATUS_REJECTED), \App\Define\Customer::WITHDRAWAL_STATUS_SUCCESS => trans('customer_withdrawals.status.' . \App\Define\Customer::WITHDRAWAL_STATUS_SUCCESS)], old('action'), ['class' => 'form-control select2', 'id' => "action"]) !!}
                    {!! Form::text('note', old('note'), ["class" => "form-control", "style" => 'display: none;', 'placeholder' => 'Ghi chú...']) !!}
                    <button type="button" class="btn btn-info btn-flat" onclick="return save()">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp; Thực hiện
                    </button>
                </div>

            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! Form::checkbox('check_all', 1, 0, []) !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customers.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customer_withdrawals.amount') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customer_withdrawals.status.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customer_withdrawals.note') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_at') !!}</th>
                        {{-- <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($withdrawals as $item)
                        <?php $customer = App\Customer::find($item->customer_id); if (is_null($customer)) continue; ?>
                        <tr>
                            <td style="text-align: center; width: 3%; vertical-align: middle;">
                                @if ($item->status == \App\Define\Customer::WITHDRAWAL_STATUS_REQUESTED || $item->status == \App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING)
                                {!! Form::checkbox('withdrawalId', $item->id, null, ['class' => 'withdrawalId']) !!}
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">{!! $i++ !!}</td>
                            <td style="vertical-align: middle;">
                                <a href="{!! route('admin.customers.show', $customer->id) !!}">{!! $customer->code !!} - {!! $customer->fullname !!}</a><br/>
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! App\Helper\HString::currencyFormat($item->amount) !!}đ
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! trans('customer_withdrawals.status.' . $item->status) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->note !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <span class="label label-default">{!! date('d/m/Y H:i', strtotime($item->created_at)) !!}</span>
                            </td>
                            {{-- <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                <a href="{!! route('admin.customer-withdrawals.show', $item->id) !!}" class="btn btn-default btn-xs"><i class="text-success far fa-eye"></i></a>
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width1: '100%' });
                $('.date_range').daterangepicker({
                    autoUpdateInput: false,
                    "locale": {
                        "format": "DD/MM/YYYY HH:mm",
                        "separator": " - ",
                        "applyLabel": "Áp dụng",
                        "cancelLabel": "Huỷ bỏ",
                        "fromLabel": "Từ ngày",
                        "toLabel": "Tới ngày",
                        "customRangeLabel": "Tuỳ chọn",
                        "weekLabel": "W",
                        "daysOfWeek": [
                            "CN",
                            "T2",
                            "T3",
                            "T4",
                            "T5",
                            "T6",
                            "T7"
                        ],
                        "monthNames": [
                            "Thg 1",
                            "Thg 2",
                            "Thg 3",
                            "Thg 4",
                            "Thg 5",
                            "Thg 6",
                            "Thg 7",
                            "Thg 8",
                            "Thg 9",
                            "Thg 10",
                            "Thg 11",
                            "Thg 12"
                        ],
                        "firstDay": 1
                    },
                    ranges: {
                       'Hôm nay': [moment(), moment()],
                       'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                       '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                       'Tháng này': [moment().startOf('month'), moment()],
                       'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    },
                    "alwaysShowCalendars": true,
                    maxDate: moment(),
                    minDate: moment().subtract(1, "years"),
                }, function(start, end, label) {
                    $('.date_range').val(start.format('DD/MM/YYYY HH:mm') + " - " + end.format('DD/MM/YYYY HH:mm'));
                });
                $("select[name='customer']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    width: '100%',
                    allowClear: true,
                    minimumResultsForSearch: 10,
                    ajax: {
                        url: "{!! route('admin.customers.search') !!}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    return {
                                        text: item.code + " - " + item.fullname,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    }
                }).on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    $(this).closest('form').submit();
                });

                var countChecked = function() {
                    var length = $("input[name='withdrawalId']:checked" ).length;
                    $("#counterSelected").text( length );
                    if (length == 0) $("input[name='check_all']").attr('checked', false);
                };
                countChecked();
                $("input[type=checkbox][name='withdrawalId']").on( "click", countChecked );
                $("input[name='check_all']").change(function() {
                    if($(this).is(':checked')) {
                        $('.withdrawalId').each(function() {
                            this.checked = true;
                        });
                    } else {
                        $('.withdrawalId').each(function() {
                            this.checked = false;
                        });
                    }
                    countChecked();
                });
                $("#action").change(function(event) {
                    if($(this).val() == '{!! \App\Define\Customer::WITHDRAWAL_STATUS_REJECTED !!}' || $(this).val() == '{!! \App\Define\Customer::WITHDRAWAL_STATUS_SUCCESS !!}') {
                        $("input[name='note']").show();
                    } else {
                        $("input[name='note']").hide();
                    }
                });
            });
        }(window.jQuery);
        function save() {
            if($( "input[name='withdrawalId']:checked" ).length == 0) {
                alert("{!! trans('system.no_item_selected') !!}");
                return false;
            }
            if($('#action').val() == 'noaction') {
                alert("{!! trans('system.no_action_selected') !!}");
                return false;
            }
            NProgress.start();
            var values = new Array();
            $.each($("input[name='withdrawalId']:checked"),
                function () {
                    values.push($(this).val());
                }
            );
            $.ajax({
                url: "{!! route('admin.customer-withdrawals.update-bulk') !!}",
                data: { action: $('#action').val(), ids: JSON.stringify(values), note: $("input[name='note']").val() },
                type: 'POST',
                datatype: 'json',
                headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                success: function(res) {
                    if($('#action').val() == 'excel') {
                        window.open(res.path, "_blank");
                    } else {
                        window.location.reload(true);
                    }
                },
                error: function(obj, status, err) {
                    var error = $.parseJSON(obj.responseText);
                    toastr.error(error.message, '{!! trans('system.info') !!}');
                }
            }).always(function() {
                NProgress.done();
            });
        };
    </script>
@stop
