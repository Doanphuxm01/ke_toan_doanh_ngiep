<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
    <table>
        <tr>
            <th style="text-align: center;">STT</th>
            <th style="text-align: center;" width="20">Mã CTV</th>
            <th style="text-align: center;" width="20">Số tiền</th>
            <th style="text-align: center;" width="80">Ngân hàng</th>
            <th style="text-align: center;" width="30">Chủ tài khoản</th>
            <th style="text-align: center;" width="30">Số tài khoản</th>
            <th style="text-align: center;" width="50">Chi nhánh</th>
            <th style="text-align: center;" width="80">Ghi chú</th>
        </tr>
        <?php $i = 1; ?>
        @foreach($data as $d)
            <tr>
                <th style="text-align: center;">{!! $i++ !!}</th>
                <th style="text-align: center;">{!! $d['code'] !!}</th>
                <th style="text-align: center;">{!! $d['amount'] !!}</th>
                <th style="text-align: center;">{!! $d['bank'] !!}</th>
                <th style="text-align: center;">{!! $d['bank_name'] !!}</th>
                <th style="text-align: center;">{!! $d['bank_no'] !!}</th>
                <th style="text-align: center;">{!! $d['bank_branch'] !!}</th>
                <th style="text-align: center;">{!! $d['note'] !!}</th>
            </tr>
        @endforeach
    </table>
</body>
</html>
