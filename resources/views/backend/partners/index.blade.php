@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('partners.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('partners.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.partners.index') !!}">{!! trans('partners.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bconsumer">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
     
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.partners.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('tax_code', trans('partners.tax_code')) !!}
                        {!! Form::text('tax_code', Request::input('tax_code'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('name', trans('partners.name')) !!}
                        {!! Form::text('name', Request::input('name'), ['class' => 'form-control']) !!}
                    </div>
                </div>
              
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.partners.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-1" style="margin-right: 3px;">
            <button type="button"  class="btn btn-success" id='btnExport'>
                <span class="far fa-file-excel fa-fw"></span>&nbsp; {!! trans('system.export') !!}
            </button>
        </div>
        &nbsp;&nbsp;
        <div class="col-sm-1" style="margin-right: 3px;">
                <a href="{!! route('admin.partners.create-bulk') !!}" class='btn btn-success btn-flat'>
                    <span class="far fa-file-excel fa-fw"></span>&nbsp;{!! trans('system.action.import') !!}
                </a>
        </div>
        <div class="col-sm-9 pull-right">
            <span class="pull-right">
                {!! $partners->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($partners) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($partners->currentPage() - 1) * $partners->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $partners->count()) . ' ( ' . trans('system.total') . ' ' . $partners->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('partners.code') !!}</th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('partners.name') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('partners.address_1') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('partners.tax_code') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('partners.phone_no') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>                        
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($partners as $item)
                        <tr>
                            <td style="vertical-align: middle; text-align:center">
                                {!! $item->code !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->name !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->address_1 !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->tax_code !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->phone_no !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                    <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                @elseif($item->status == 1)
                                    <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap; text-align:center">
                                <a href="{!! route('admin.partners.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.partners.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script>
        $(document).ready(function(){
            $("#btnExport").click(function() {
                let table = document.getElementsByTagName("table");
                if(table.length == 0) {
                    toastr.error('No record can not export excel');
                    return false;
                }
                TableToExcel.convert(table[0], { 
                    name: `export.xlsx`, 
                    sheet: {
                        name: 'Sheet 1' 
                    }
                });
            });
        });
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
