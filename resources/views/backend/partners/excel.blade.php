@for ($i = 1; $i < count($data); $i++)
    <?php if (!isset($data[$i][1]) || trim($data[$i][1]) == "") continue; ?>
    <tr>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            {!! $i !!}
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class="tax_code">{!! $data[$i][0] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class="nullable">{!! $data[$i][1] !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class="nullable">{!! $data[$i][2] !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
           <span class='nullable'>{!! $data[$i][3] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][4] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][5] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][6] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][7] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][8] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][9] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][10] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][11] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][12] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle; min-width: 120px;">
            <span class='nullable'>{!! $data[$i][13] ?? '' !!}</span>
        </td>
        <td style="vertical-align: middle; white-space: nowrap; min-width: 120px;">
            <span class='nullable'>{!! trim($data[$i][14] ?? '') !!}</span>
        </td>
        <td style="vertical-align: middle; white-space: nowrap; min-width: 120px;">
            <span class='nullable'>{!! trim($data[$i][15] ?? '') !!}</span>
        </td>
        <td style="vertical-align: middle; white-space: nowrap; min-width: 120px;">
            <span class='nullable'>{!! trim($data[$i][16] ?? '') !!}</span>
        </td>
    </tr>
@endfor
<script>
    $(function(){
        $.fn.editable.defaults.mode = 'inline';
        $('.tax_code').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
            }
        });
        $('.nullable').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                var value = $.trim(value);
                if (value.length > 255) return "Chuỗi không được vượt quá 255 kí tự";
            }
        });
    });
</script>
