@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} -{!! trans('partners.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('partners.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.partners.index') !!}">{!! trans('partners.label') !!}</a></li>
        </ol>
        
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.partners.update', $partner->id), 'method' => 'PUT', 'role' => 'form']) !!}
    <table class='table borderless' style="width: 70%; margin:auto">
        <tr>
            <th class="table_right_middle" style="width: 15%;">
                {!! trans('partners.tax_code') !!}
            </th>
            <td class='col-md-1'>
                {!! Form::text('tax_code', old('tax_code', $partner->tax_code), ['class' => 'form-control', 'maxlength' => 15]) !!}
            </td>
            <td style="width: 35%;">
                <button name="search" class="btn btn-primary"> <span class="glyphicon glyphicon-search"></span>&nbsp;search</button>
            </td>
        </tr>
        <tr>
            <th class="table_right_middle" style="width: 15%;" >
                {!! trans('partners.code') !!}
            </th>
            <td colspan="3">
                {!! Form::text('code', old('code', $partner->code), ['class' => 'form-control', 'maxlength' => 15, 'required']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('partners.name') !!}
            </th>
            <td colspan="3">
                {!! Form::text('name', old('name', $partner->name), ['class' => 'form-control', 'maxlength' => 255, 'required']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('partners.address_1') !!}
            </th>
            <td colspan="3">
                {!! Form::text('address_1', old('address_1',  $partner->address_1), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('partners.address_2') !!}
            </th>
            <td colspan="3">
                {!! Form::text('address_2', old('address_2', $partner->address_2), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>             
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('partners.address_3') !!}
            </th>
            <td colspan="3">
                {!! Form::text('address_3', old('address_3', $partner->address_3), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('partners.address_4') !!}
            </th>
            <td colspan="3">
                {!! Form::text('address_4', old('address_4', $partner->address_4), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle" >
                {!! trans('partners.city_code') !!}
            </th>
            <td>
                {!! Form::text('city_code', old('city_code', $partner->city_code), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
            <th class="table_right_middle" style="float:left; min-width:32%">            
                {!! trans('partners.country_code') !!}
            </th>
            <td  style="float:left; min-width:65%">
                {!! Form::text('country_code', old('country_code', $partner->country_code), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle" >
                {!! trans('partners.phone_no') !!}
            </th>
            <td >
               {!! Form::text('phone_no', old('phone_no', $partner->phone_no), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
            <th class="table_right_middle" style="float:left; min-width:32%">
                {!! trans('partners.fax_no') !!}
            </th>       
            <td  style="float:left; min-width:65%">
                {!! Form::text('fax_no', old('fax_no', $partner->fax_no), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('partners.contact') !!}
            </th>
            <td style="width: 30% " class="table_right_middle">
                {!! Form::text('contact', old('contact', $partner->contact), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
            <th class="table_right_middle" style="float:left; min-width:32%">
                {!! trans('partners.email') !!}
            </th>
            <td style="float:left; min-width:65%">
                {!! Form::text('email', old('email', $partner->email), ['class' => 'form-control', 'maxlength' => 255]) !!}
            </td>
        </tr>
        <tr>
            <td class="text-center table_right_middle1" colspan="4">
                <div class ='checkbox_'>
                    {!! Form::checkbox('status', 1,old('status', $partner->status), [ 'class' => 'minimal' ]) !!}
                    {!! trans('system.status.active') !!}
                </div>
                <div class ='checkbox_'>
                    {!! Form::checkbox('is_vendor', 1,old('is_vendor', $partner->is_vendor), [ 'class' => 'minimal' ]) !!}
                    {!! trans('partners.vendor') !!}
                </div>
                <div class ='checkbox_'>
                    {!! Form::checkbox('is_customer', 1, old('is_customer', $partner->is_customer), [ 'class' => 'minimal' ]) !!}
                    {!! trans('partners.customer') !!} 
                </div>
                <div class ='checkbox_'>
                    {!! Form::checkbox('is_contra', 1, old('is_contra', $partner->is_contra), [ 'class' => 'minimal' ]) !!}
                    {!! trans('partners.contra.label') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="text-center">
                {!! HTML::link(route( 'admin.partners.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
            </td>
        </tr>
    </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <style>
        .checkbox_{
            display: inline-block;
        }
        .checkbox_ + .checkbox_ {
            margin-left: 16px
        }
    </style>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                // $('input[type="checkbox"].minimal').iCheck({
                //     checkboxClass: 'icheckbox_minimal-blue'
                // });
                $('button[name="search"]').click(function (e) { 
                    e.preventDefault();
                    var msdn = $('input[name="tax_code"]').val();
                    console.log(msdn);
                    $.ajax({
                        type: "GET",
                        url: "{{ route('admin.partners.get-info-customer') }}",
                        data: {msdn:msdn},
                        dataType: "json",
                        success: function (response) {
                            $('input[name="tax_code"]').val(response.data.msdn);
                            $('input[name="name"]').val(response.data.name);
                            $('input[name="address_1"]').val(response.data.address);
                            $('input[name="city_code"]').val(response.data.city_code);
                            $('input[name="contact"]').val(response.data.contact);
                        },
                        error: function(obj, status, err) {
                            var error = $.parseJSON(obj.responseText);
                            toastr.error(error.message, '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                        }
                    });
                });
                $(document).on('change', 'input:checkbox[name="is_vendor"], input:checkbox[name="is_customer"]', function(e) {
                    if(!$('input:checkbox[name="is_vendor"]').is(":checked") && !$('input:checkbox[name="is_customer"]').is(":checked")){
                        toastr.error('{!! trans('partners.error_check') !!}')
                        $('input:submit').prop('disabled', true);
                    } else {
                        $('input:submit').prop('disabled', false);
                    }
                })
            });
        }(window.jQuery);
    </script>
@stop