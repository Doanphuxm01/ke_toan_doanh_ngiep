@extends('backend.master')
@section('title')
{!! trans('system.action.import') !!} - {!! trans('partners.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('partners.label') !!}
            <small>{!! trans('system.action.import') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.prices.index') !!}">{!! trans('partners.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="excel_file">File excel data</label>
                            {!! Form::file('excel_file', [ 'id' => 'excel_file', "accept" => ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel", 'class'=>"custom-file-input" ]) !!}
                            <p class="help-block">download file example <a href="{!! route('admin.partners.download') !!}"> in here</a></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                            <button class="btn btn-success upload btn-flat" onclick="return upload()">
                                <span class="fa fa-upload"></span> {!! trans('system.action.upload') !!}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="table-responsive" style="overflow-y: scroll; max-height: 400px;">
                    <table class="table table-condensed table-bordered" id="data" style="overflow-x: scroll;">
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;"> {!! trans('system.no.') !!} </th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.tax_code') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.code') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.name') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.address_1') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.address_2') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.address_3') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.address_4') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.city_code') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.country_code') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.phone_no') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.fax_no') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.contact') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.email') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.vendor') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.customer') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;  min-width: 120px;">{!! trans('partners.contra.IS_CONTRA') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('system.status.label') !!}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">{!! trans('partners.error_no_data') !!}  &nbsp;&nbsp;{!! trans('partners.action_create_multi') !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-flat" onclick="save()">{!! trans('system.action.save') !!}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script>
    !function ($) {
        $(function() {
            $(".select2").select2();
        });
    }(window.jQuery);

    function upload() {
        if ( document.getElementById("excel_file").files.length == 0 ) {
            toastr.error("you need choose a file", '{!! trans('system.info') !!}');
            return false;
        }
        var myfile = new FormData();
        myfile.append('file', $('#excel_file')[0].files[0]);
        NProgress.start();
        $.ajax({
            url: "{!! route('admin.partners.read-bulk') !!}",
            data: myfile,
            type: 'POST',
            contentType: false,
            processData: false,
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(res) {
              
                $("#data tbody").html('').append(res.message);
            },
            error: function(obj, status, err) {
                NProgress.done();
                var error = $.parseJSON(obj.responseText);
                toastr.error(error.message, '{!! trans('system.have_an_error') !!}');
            }
        }).always(function() {
            NProgress.done();
        });
    }

    function save() {
        var table = $("#data tbody");
        var data = [], have_error = false, categories = {};
        table.find('tr').each(function (row) {
            var $tds = $(this).find('td');
            var dRow = [];
            var stt = $.trim($tds.eq(0).html());
            var tax_code = $.trim($tds.eq(1).find('span').html());
                code = $.trim($tds.eq(2).find('span').html());
                name = $.trim($tds.eq(3).find('span').html());
                address_1 = $.trim($tds.find('span').eq(4).html());
                address_2 = $.trim($tds.find('span').eq(5).html());
                address_3 = $.trim($tds.find('span').eq(6).html());
                address_4 = $.trim($tds.find('span').eq(7).html());
                city_code = $.trim($tds.find('span').eq(8).html());
                country_code = $.trim($tds.eq(9).find('span').html());
                phone_no = $.trim($tds.eq(10).find('span').html());
                fax_no = $.trim($tds.eq(11).find('span').html());
                contact = $.trim($tds.eq(12).find('span').html());
                email = $.trim($tds.eq(13).find('span').html());
                vendor = $.trim($tds.eq(14).find('span').html());
                customer = $.trim($tds.eq(15).find('span').html());
                contra = $.trim($tds.eq(16).find('span').html());
                status = $.trim($tds.eq(17).find('span').html());
            if (/^[0-9]{10}(-[0-9]{3}$)?$/.test(tax_code) == false) {
                if(tax_code=='-'){

                    if(code == '-'){
                        toastr.error("{!! trans('partners.error_code') !!}"+" "+"{!! trans('partners.number_line') !!}"  + stt);
                        have_error = true;
                        return false;
                    } 
                    if(name == '-'){
                        toastr.error("{!! trans('partners.error_name') !!}"+" "+"{!! trans('partners.number_line') !!}"  + stt);
                        have_error = true;
                        return false;
                    } 
                    if(vendor =='-' && customer=='-'){
                        toastr.error("{!! trans('partners.error_check') !!}"+" "+"{!! trans('partners.number_line') !!}"  + stt);
                        have_error = true;
                        return false;
                    }
                    dRow.push(stt);
                    dRow.push(tax_code);
                    dRow.push(code);
                    dRow.push(name);
                    dRow.push(address_1);
                    dRow.push(address_2);
                    dRow.push(address_3);
                    dRow.push(address_4);
                    dRow.push(city_code);
                    dRow.push(country_code);
                    dRow.push(phone_no);
                    dRow.push(fax_no);
                    dRow.push(contact);
                    dRow.push(email);
                    dRow.push(vendor);
                    dRow.push(customer);
                    dRow.push(contra);
                    dRow.push(status);
                    data.push(dRow);
                    return true;
                } else {
                    toastr.error("{!! trans('partners.error_tax_code') !!}"+" "+"{!! trans('partners.number_line') !!}" + stt);
                    return false
                }
            }
          
            if(code == '-'){
                toastr.error("{!! trans('partners.error_code') !!}"+" "+"{!! trans('partners.number_line') !!}"  + stt);
                have_error = true;
                return false;
            } 
            if(name == '-'){
                toastr.error("{!! trans('partners.error_name') !!}"+" "+"{!! trans('partners.number_line') !!}"  + stt);
                have_error = true;
                return false;
            } 
            if(vendor =='-' && customer=='-'){
                toastr.error("{!! trans('partners.error_check') !!}"+" "+"{!! trans('partners.number_line') !!}"  + stt);
                have_error = true;
                return false;
            }
            dRow.push(stt);
            dRow.push(tax_code);
            dRow.push(code);
            dRow.push(name);
            dRow.push(address_1);
            dRow.push(address_2);
            dRow.push(address_3);
            dRow.push(address_4);
            dRow.push(city_code);
            dRow.push(country_code);
            dRow.push(phone_no);
            dRow.push(fax_no);
            dRow.push(contact);
            dRow.push(email);
            dRow.push(vendor);
            dRow.push(customer);
            dRow.push(contra);
            dRow.push(status);
            data.push(dRow);
        });
        console.log(data);
        if (have_error) return false;
        if(data.length === 0) {
            toastr.error("{!! trans('partners.error_no_data') !!}");
            return false;
        }
        NProgress.start();
        $.ajax({
            url: "{!! route('admin.partners.save-bulk') !!}",
            data: {_token: '{{ csrf_token() }}', data: data },
            type: 'POST',
            datatype: 'json',
          
            success: function(res) {
                window.location.href = "{!! route('admin.partners.index') !!}";
            },
            error: function(obj, status, err) {
                var error = $.parseJSON(obj.responseText);
                toastr.error(error.message, '{!! trans('system.have_an_error') !!}');
            }
        }).always(function() {
            NProgress.done();
        });
    }
</script>
@stop
