<?php $oldSKU = ""; ?>
@for ($i = 1; $i < count($data); $i++)
    <?php if ((!isset($data[$i][1]) || trim($data[$i][1]) == "") && $oldSKU == "") continue; ?>
    <tr>
        <td style="text-align: center; vertical-align: middle;">
            {!! $data[$i][0] ?? '' !!}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="text_require">{!! $data[$i][1] ?? $oldSKU !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="quantity">{!! $data[$i][2] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="quantity">{!! $data[$i][3] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="integer">{!! $data[$i][4] ?? '' !!}</span>
        </td>
    </tr>
    <?php if ($data[$i][1]) $oldSKU = $data[$i][1]; ?>
@endfor
<script>
    $(function(){
        $.fn.editable.defaults.mode = 'inline';
        $('.text_require').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                var value = $.trim(value);
                if(value == '') return "{!! trans('system.data_not_empty') !!}";
                if (value.length > 100) return "Chuỗi không được vượt quá 100 kí tự";
            }
        });
        $('.quantity').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                if($.trim(value) == '') return "{!! trans('system.data_not_empty') !!}";
                if (isNaN(value))  return 'Dữ liệu phải là một số nguyên dương';
                if (value%1 != 0)  return 'Dữ liệu phải là một số nguyên dương';
                if(value < 1 || value > 99999999) return 'Dữ liệu nằm trong khoảng từ 1-999';
            }
        });
        $('.integer').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                if($.trim(value) == '') return "{!! trans('system.data_not_empty') !!}";
                if (isNaN(value))  return 'Dữ liệu phải là một số nguyên dương';
                if (value%1 != 0)  return 'Dữ liệu phải là một số nguyên dương';
                if (value%100 != 0)  return 'Dữ liệu phải có đơn vị là trăm';
                if(value < 1 || value > 99999999) return 'Dữ liệu nằm trong khoảng từ 1-99,999,999';
            }
        });
    });
</script>
