@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('agency_prices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('agency_prices.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.agency-prices.index') !!}">{!! trans('agency_prices.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.agency-prices.update', $price->id), 'method' => 'PUT']) !!}
        <div class="box box-default">
            <div class="box-header">
                <table class="table">
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('agency_prices.product') !!}
                        </th>
                        <td style="width: 55%;">
                            {!! Form::select('product', $product, old('product'), ["class" => "form-control", "disabled"]) !!}
                        </td>
                        <th class="table_right_middle" style="width: 15%;">
                        </th>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="text-align: center; vertical-align: middle; width: 30%;">{!! trans('agency_prices.greater_than_equal') !!}</th>
                            <th style="text-align: center; vertical-align: middle; width: 30%;">{!! trans('agency_prices.less_than') !!}</th>
                            <th style="text-align: center; vertical-align: middle; width: 30%;">{!! trans('agency_prices.price_ipo') !!}</th>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                        </tr>
                    </thead>
                    <tbody id="agency-prices">
                        <?php
                            $list = $price->list()->get()->toArray();
                            $greater_than_equal = old('greater_than_equal', array_column($list, 'greater_than_equal'));
                            $less_than = old('less_than', array_column($list, 'less_than'));
                            $price_ipo = old('price_ipo', array_column($list, 'price_ipo'));
                        ?>
                        @if (count($greater_than_equal))
                            @for ($i = 0; $i < count($greater_than_equal); $i++)
                                <tr>
                                    <td style="vertical-align: middle;">
                                        {!! Form::text('greater_than_equal[]', old('greater_than_equal[]', $greater_than_equal[$i]), ['class' => 'form-control quantity', 'required']) !!}
                                    </td>
                                    <td style="vertical-align: middle;">
                                        {!! Form::text('less_than[]', old('less_than[]', $less_than[$i]), ['class' => 'form-control quantity', 'required']) !!}
                                    </td>
                                    <td style="vertical-align: middle;">
                                        {!! Form::text('price_ipo[]', old('price_ipo[]', $price_ipo[$i]), ['class' => 'form-control decimal', 'required']) !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="javascript:void(0)" class="del-row btn btn-default btn-xs"><i class="text-danger glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            @endfor
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <a href="javascript:void(0)" id="addrow"><span class="fa fa-plus"></span> Thêm khoảng</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="4">
                                <label>
                                    {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                                    {!! trans('system.status.active') !!}
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-center">
                                <a class="btn btn-default btn-flat" href="{!! route('admin.inventories.index') !!}">{!! trans('system.action.cancel') !!}</a>
                                {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $(".select2").select2({ width: '100%' });
                $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                $(".quantity").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '1000', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                $("select[name='product']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    minimumResultsForSearch: 10,
                    width: '100%',
                    templateResult: formatState,
                    ajax: {
                        url: "{{ route('admin.products.search-for-price-by-type') }}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term,
                                type: '{!! App\Define\Price::TYPE_PRODUCT !!}',
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    if (typeof(item.children) == 'undefined') {
                                        return {
                                            text: item.name + " - " + parseInt(item.price_ipo).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "đ",
                                            id: item.id,
                                            image: item.image,
                                        }
                                    } else {
                                        return {
                                            text: item.text,
                                            children: $.map(item.children, function(child){
                                                return {
                                                    id: child.id,
                                                    text: child.name + " - " + parseInt(child.price_ipo + "").toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "đ",
                                                    image: child.image,
                                                }
                                            })
                                        }
                                    }
                                })
                            };
                        }
                    }
                });

                $(document).on('click','#addrow',function(){
                    var newRow = $("<tr>");
                    var cols = "";
                    cols += '<td><input type="text" class="form-control quantity" required name="greater_than_equal[]"/></td>';
                    cols += '<td><input type="text" class="form-control quantity" required name="less_than[]"/></td>';
                    cols += '<td><input type="text" class="form-control decimal" required name="price_ipo[]"/></td>';
                    cols += '<td style="text-align: center; vertical-align: middle;"><a href="javascript:void(0)" class="del-row btn btn-default btn-xs"><i class="text-danger glyphicon glyphicon-remove"></i></a></td>';
                    newRow.append(cols);
                    $("#agency-prices").append(newRow);
                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                    $(".quantity").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                });

                $(document).on("click", ".del-row", function (event) {
                    $(this).closest("tr").remove();
                });
            });
        }(window.jQuery);
        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img height="30px" src="' + state.image + '" class="img-flag" /> ' + state.text + '</span>'
            );
            return $state;
        };
    </script>
@stop
