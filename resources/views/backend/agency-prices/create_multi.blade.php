@extends('backend.master')
@section('title')
{!! trans('system.action.import') !!} - {!! trans('agency_prices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('agency_prices.label') !!}
            <small>{!! trans('system.action.import') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.agency-prices.index') !!}">{!! trans('prices.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="excel_file">File excel dữ liệu</label>
                            {!! Form::file('excel_file', [ 'id' => 'excel_file', "accept" => ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" ]) !!}
                            <p class="help-block">Tải file theo định dạng sau <a href="{!! route('admin.agency-prices.download') !!}">tại đây</a></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                            <button class="btn btn-success upload btn-flat" onclick="return upload()">
                                <span class="fa fa-upload"></span> {!! trans('system.action.upload') !!}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-body">
                <div class="table-responsive" style="overflow-y: scroll; max-height: 400px;">
                    <table class="table table-condensed table-bordered" id="data" style="overflow-x: scroll;">
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap; width: 5%;"> {!! trans('system.no.') !!} </th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap; width: 20%;">{!! trans('products.sku') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap; width: 15%;">{!! trans('agency_prices.greater_than_equal') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap; width: 15%;">{!! trans('agency_prices.less_than') !!}</th>
                                <th style="text-align: center; vertical-align: middle; white-space: nowrap; width: 15%;">{!! trans('agency_prices.price_ipo') !!}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">Chưa có dữ liệu, kéo trên dưới và sang hai bên (nếu cần) để xem thêm các cột khác...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-flat" onclick="save()">{!! trans('system.action.save') !!}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script>
    !function ($) {
        $(function() {
            $(".select2").select2();
        });
    }(window.jQuery);

    function upload() {
        if (document.getElementById("excel_file").files.length == 0) {
            toastr.error("Bạn chưa chọn file", '{!! trans('system.info') !!}');
            return false;
        }
        var myfile = new FormData();
        myfile.append('file', $('#excel_file')[0].files[0]);
        NProgress.start();
        $.ajax({
            url: "{!! route('admin.agency-prices.read-bulk') !!}",
            data: myfile,
            type: 'POST',
            contentType: false,
            processData: false,
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(res) {
                $("#data tbody").html('').append(res.message);
            },
            error: function(obj, status, err) {
                NProgress.done();
                var error = $.parseJSON(obj.responseText);
                toastr.error(error.message, '{!! trans('system.have_an_error') !!}');
            }
        }).always(function() {
            NProgress.done();
        });
    }

    function save() {
        var table = $("#data tbody");
        var data = [], have_error = false, no_data = true;
        table.find('tr').each(function (row) {
            var $tds = $(this).find('td');
            var dRow = [];
            var stt = $.trim($tds.eq(0).html()),
                sku = $.trim($tds.eq(1).find("span").text()),
                greater_than_equal = parseInt($tds.eq(2).find("span").text()),
                less_than = parseInt($tds.eq(3).find("span").text()),
                price_ipo = $tds.eq(4).find("span").text();
            if (sku == "" || sku.length > 100) {
                toastr.error("Vui lòng kiểm tra lại `SKU` dòng số: " + stt);
                have_error = true;
                return false;
            }
            if (isNaN(greater_than_equal) || greater_than_equal < 1 || greater_than_equal > 9999999) {
                toastr.error("Vui lòng kiểm tra lại `Lớn hơn hoặc bằng` dòng số: " + stt);
                have_error = true;
                return false;
            }
            if (isNaN(less_than) || less_than < 1 || less_than > 9999999) {
                toastr.error("Vui lòng kiểm tra lại `Nhỏ hơn` dòng số: " + stt);
                have_error = true;
                return false;
            }
            if (greater_than_equal > less_than) {
                toastr.error("Vui lòng kiểm tra lại `Lớn hơn hoặc bằng` lớn hơn `Nhỏ hơn` dòng số: " + stt);
                have_error = true;
                return false;
            }
            if (isNaN(price_ipo) || price_ipo < 1 || price_ipo > 999999999) {
                toastr.error("Vui lòng kiểm tra lại `Giá sỉ` dòng số: " + stt);
                have_error = true;
                return false;
            }
            if (price_ipo%100 != 0) {
                toastr.error("`Giá sỉ` dòng số: " + stt + " phải là tròn trăm");
                have_error = true;
                return false;
            }
            // dRow.push(stt + "." + greater_than_equal + "." + less_than + "." + price_ipo);
            if (data[sku] === undefined) {
                data[sku] = [];
            }
            data[sku].push(stt + "." + greater_than_equal + "." + less_than + "." + price_ipo);
            no_data = false;
        });
        if (have_error) return false;
        if (no_data) {
            toastr.error("Chưa có dữ liệu để thêm mới");
            return false;
        }
        var final = [];
        for (var row in data) {
            final.push({'sku': row, 'data': data[row]});
        }
        NProgress.start();
        $.ajax({
            url: "{!! route('admin.agency-prices.save-bulk') !!}",
            data: { data: JSON.stringify(final) },
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(res) {
                window.location.href = "{!! route('admin.agency-prices.index') !!}";
            },
            error: function(obj, status, err) {
                var error = $.parseJSON(obj.responseText);
                toastr.error(error.message, '{!! trans('system.have_an_error') !!}');
            }
        }).always(function() {
            NProgress.done();
        });
    }
</script>
@stop
