@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('suppliers.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('suppliers.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.suppliers.index') !!}">{!! trans('suppliers.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.suppliers.store')]) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('suppliers.name') !!}
                </th>
                <td style="width: 35%;">
                    {!! Form::text('name', old('name'), ["class" => "form-control", "required"]) !!}
                </td>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('suppliers.description') !!}
                </th>
                <td>
                    {!! Form::text('description', old('description'), ["class" => "form-control"]) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('suppliers.phone') !!}
                </th>
                <td>
                    {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('suppliers.email') !!}
                </th>
                <td>
                    {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('suppliers.address') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                        {!! trans('system.status.active') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    <a class="btn btn-default btn-flat" href="{!! route('admin.suppliers.index') !!}">{!! trans('system.action.cancel') !!}</a>
                    {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
            });
        }(window.jQuery);
    </script>
@stop
