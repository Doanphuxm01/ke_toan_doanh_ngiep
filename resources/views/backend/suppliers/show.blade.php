@extends('backend.master')
@section('title')
{!! trans('system.action.detail') !!} - {!! trans('suppliers.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('suppliers.label') !!}
            <small>
                {!! trans('system.action.detail') !!}
                @if($supplier->status)
                    <label class="label label-success">{!! trans('system.status.active') !!}</label>
                @else
                    <label class="label label-danger">{!! trans('system.status.deactive') !!}</label>
                @endif
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.suppliers.index') !!}">{!! trans('suppliers.label') !!}</a></li>
        </ol>
    </section>
    <div class="box">
        <div class="box-body">
            <table class='table table-bordered1'>
                <tr>
                    <th class="table_right_middle" style="width: 15%;">
                        {!! trans('suppliers.name') !!}
                    </th>
                    <td style="width: 35%;">
                        {!! Form::text('name', old('name', $supplier->name), ["class" => "form-control", "disabled"]) !!}
                    </td>
                    <th class="table_right_middle" style="width: 15%;">
                        {!! trans('suppliers.description') !!}
                    </th>
                    <td>
                        {!! Form::text('description', old('description', $supplier->description), ['class' => 'form-control', "disabled"]) !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('suppliers.phone') !!}
                    </th>
                    <td>
                        {!! Form::text('phone', old('phone', $supplier->phone), ["class" => "form-control", "disabled"]) !!}
                    </td>
                    <th class="table_right_middle">
                        {!! trans('suppliers.email') !!}
                    </th>
                    <td>
                        {!! Form::text('email', old('email', $supplier->email), ['class' => 'form-control', "disabled"]) !!}
                    </td>
                </tr>
                <tr>
                    <th class="text-right">
                        {!! trans('suppliers.address') !!}
                    </th>
                    <td colspan="3">
                        {!! Form::textarea('address', old('address', $supplier->address), ['class' => 'form-control', 'rows' => 1, "disabled"]) !!}
                    </td>
                </tr>
            </table>
        </div>
    </div>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
            });
        }(window.jQuery);
    </script>
@stop
