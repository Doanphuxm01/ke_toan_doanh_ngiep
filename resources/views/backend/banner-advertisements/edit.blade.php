@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('banner_advertisements.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('banner_advertisements.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.banner-advertisements.index') !!}">{!! trans('banner_advertisements.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.banner-advertisements.update', $category->id), 'method' => 'PUT', 'role' => 'form', 'files' => true]) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle">
                    {!! trans('banner_advertisements.name') !!}
                </th>
                <td>
                    {!! Form::text('name', old('name', $category->name), ['class' => 'form-control', 'maxlength' => 100, 'required']) !!}
                </td>
                <th class="text-right">
                    {!! trans('banner_advertisements.href') !!}
                </th>
                <td>
                    {!! Form::text('href', old('href', $category->href), ['class' => 'form-control', 'maxlength' => 255]) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('banner_advertisements.type') !!}
                </th>
                <td>
                    {!! Form::select('type', \App\Define\Constant::getBannerTypesForOption(), old('type', $category->type), ['class' => 'form-control']) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans("banner_advertisements.image") !!}<br/>
                    <span class="label_size">
                        <?php
                            $type = old('type', $category->type);
                            $reflector = new ReflectionClass('App\Define\Constant');
                            $width = $reflector->getConstant($type . '_WIDTH');
                            $height = $reflector->getConstant($type . '_HEIGHT');
                        ?>
                        @if(in_array($type, \App\Define\Constant::getBannerTypes()))
                            ({!! $width !!} x {!! $height !!})
                        @endif
                    </span>
                </th>
                <td colspan="3">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail" style="width: {!! $width/2 !!}px; height: {!! $height/2 !!}px;">
                            <img src="{!! asset($category->image) !!}">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">
                                    {!! trans('system.action.select_image') !!}
                                </span>
                                {!! Form::file('image') !!}
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                {!! trans('system.action.remove') !!}
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="4">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', $category->status), [ 'class' => 'minimal' ]) !!}
                        {!! trans('system.status.active') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    {!! HTML::link(route( 'admin.banner-advertisements.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat'])!!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });

                $('input[type="checkbox"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red'
                });

                $("select[name='type']").change(function(event) {
                    var type = $(this).val();
                    if (type == '{!! \App\Define\Constant::IMAGE_TYPE_BANNER_TOP !!}') {
                        $(".label_size").html("({!! \App\Define\Constant::IMAGE_TYPE_BANNER_TOP_WIDTH !!}x{!! \App\Define\Constant::IMAGE_TYPE_BANNER_TOP_HEIGHT !!})");
                        $(".thumbnail").css({
                            width: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_TOP_WIDTH/2 !!}px",
                            height: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_TOP_HEIGHT/2 !!}px"
                        });
                    } else if (type == '{!! \App\Define\Constant::IMAGE_TYPE_BANNER_RIGHT_SLIDE !!}') {
                        $(".label_size").html("({!! \App\Define\Constant::IMAGE_TYPE_BANNER_RIGHT_SLIDE_WIDTH !!}x{!! \App\Define\Constant::IMAGE_TYPE_BANNER_RIGHT_SLIDE_HEIGHT !!})");
                        $(".thumbnail").css({
                            width: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_RIGHT_SLIDE_WIDTH/2 !!}px",
                            height: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_RIGHT_SLIDE_HEIGHT/2 !!}px"
                        });
                    } else if (type == '{!! \App\Define\Constant::IMAGE_TYPE_BANNER_MIDDLE !!}') {
                        $(".label_size").html("({!! \App\Define\Constant::IMAGE_TYPE_BANNER_MIDDLE_WIDTH !!}x{!! \App\Define\Constant::IMAGE_TYPE_BANNER_MIDDLE_HEIGHT !!})");
                        $(".thumbnail").css({
                            width: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_MIDDLE_WIDTH/2 !!}px",
                            height: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_MIDDLE_HEIGHT/2 !!}px"
                        });
                    } else if (type == '{!! \App\Define\Constant::IMAGE_TYPE_BANNER_BOTTOM !!}') {
                        $(".label_size").html("({!! \App\Define\Constant::IMAGE_TYPE_BANNER_BOTTOM_WIDTH !!}x{!! \App\Define\Constant::IMAGE_TYPE_BANNER_BOTTOM_HEIGHT !!})");
                        $(".thumbnail").css({
                            width: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_BOTTOM_WIDTH/2 !!}px",
                            height: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_BOTTOM_HEIGHT/2 !!}px"
                        });
                    } else if (type == '{!! \App\Define\Constant::IMAGE_TYPE_BANNER_EMPTY_CART !!}') {
                        $(".label_size").html("({!! \App\Define\Constant::IMAGE_TYPE_BANNER_EMPTY_CART_WIDTH !!}x{!! \App\Define\Constant::IMAGE_TYPE_BANNER_EMPTY_CART_HEIGHT !!})");
                        $(".thumbnail").css({
                            width: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_EMPTY_CART_WIDTH !!}px",
                            height: "{!! \App\Define\Constant::IMAGE_TYPE_BANNER_EMPTY_CART_HEIGHT !!}px"
                        });
                    } else {
                        $(".label_size").html("");
                        $(".thumbnail").css({
                            width: "0px",
                            height: "0px"
                        });
                    }
                });
            });
        }(window.jQuery);
    </script>
@stop