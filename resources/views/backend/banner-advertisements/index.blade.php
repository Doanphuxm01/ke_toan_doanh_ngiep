@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('banner_advertisements.label') !!}
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('banner_advertisements.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.banner-advertisements.index') !!}">{!! trans('banner_advertisements.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.banner-advertisements.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('type', trans('banner_advertisements.type')) !!}
                        {!! Form::select('type', ['' => trans('system.dropdown_all')] + \App\Define\Constant::getBannerTypesForOption(), Request::input('type'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label')) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            @permission('banner_advertisements.create')
            <a href="{!! route('admin.banner-advertisements.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp;{!! trans('system.action.create') !!}
            </a>
            @endpermission
        </div>
        <div class="col-md-10 text-right">
            {!! $categories->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($categories) > 0)
    <div class="box">
        <div class="box-header">
            <?php $i = (($categories->currentPage() - 1) * $categories->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $categories->count()) . ' ( ' . trans('system.total') . ' ' . $categories->total() . ' )' !!}
                    | <i>Chú giải: </i>&nbsp;&nbsp;
                    <span class="text-info"><i class="fa fa-eye"></i> {!! trans('system.action.detail') !!} </span>&nbsp;&nbsp;
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!}</span>
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class='table table-striped table-bordered tree'>
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('banner_advertisements.image') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('banner_advertisements.name') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('banner_advertisements.type') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!} </th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!}</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                    </tr>
                </thead>
                <tbody class="borderless">
                    @foreach ($categories as $item)
                        <tr>
                            <td  style="text-align: center; vertical-align: middle;">
                                <img src="{!! asset($item->image) !!}" style="max-height: 50px; max-width: 250px;">
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{!! route('admin.banner-advertisements.show', $item->id) !!}" title="{!! trans('system.action.detail') !!}">
                                    {!! $item->name !!}
                                </a>
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! trans('banner_advertisements.types.' . $item->type) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                <span class="label label-danger"><span class='glyphicon glyphicon-remove'></span></span>
                                @elseif($item->status == 1)
                                <span class="label label-success"><span class='glyphicon glyphicon-ok'></span></span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @permission('banner_advertisements.update')
                                    <a href="{!! route('admin.banner-advertisements.edit', $item->id) !!}" class="btn btn-xs btn-default">
                                        <i class="text-warning glyphicon glyphicon-edit"></i>
                                    </a>
                                    &nbsp;&nbsp;
                                @endpermission
                                @permission('banner_advertisements.delete')
                                    <a href="javascript:void(0)" link="{!! route('admin.banner-advertisements.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                        <i class="text-danger glyphicon glyphicon-remove"></i>
                                    </a>
                                @endpermission
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop