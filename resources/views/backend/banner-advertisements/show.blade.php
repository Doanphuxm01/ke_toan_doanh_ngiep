@extends('backend.master')

@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('banner_advertisements.label') !!}
@stop

@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
@stop

@section('content')
    <section class="content-header">
        <h1>
            {!! trans('banner_advertisements.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($category->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.banner-advertisements.index') !!}">{!! trans('banner_advertisements.label') !!}</a></li>
        </ol>
    </section>
    <table class='table borderless'>
        <tr>
            <th class="table_right_middle">
                {!! trans('banner_advertisements.name') !!}
            </th>
            <td>
                {!! $category->name !!}
            </td>
            <th class="text-right">
                {!! trans('banner_advertisements.href') !!}
            </th>
            <td>
                {!! $category->href !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('banner_advertisements.type') !!}
            </th>
            <td>
                {!! trans('banner_advertisements.types.' . $category->type) !!}
            </td>
        </tr>
        <tr>
            <th class="text-right">
                {!! trans("banner_advertisements.image") !!}<br/>
                <span class="label_size">
                    <?php
                        $reflector = new ReflectionClass('App\Define\Constant');
                        $width = $reflector->getConstant($category->type . '_WIDTH');
                        $height = $reflector->getConstant($category->type . '_HEIGHT');
                    ?>
                    @if(in_array($type, \App\Define\Constant::getBannerTypes()))
                        ({!! $width !!} x {!! $height !!})
                    @endif
                </span>
            </th>
            <td colspan="3">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-preview thumbnail" style="width: {!! $width/2 !!}px; height: {!! $height/2 !!}px;">
                        <img src="{!! asset($category->image) !!}">
                    </div>
                </div>
            </td>
        </tr>
    </table>
@stop