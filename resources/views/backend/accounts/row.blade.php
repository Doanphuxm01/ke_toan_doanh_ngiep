<?php $found = $code && (strpos(strtoupper($account->code), $code) !== false); ?>
<tr class="treegrid-{!! $account->code !!} @if ($found) need-expand @endif @if($account->parent_id) treegrid-parent-{!! $account->parent_id !!}@endif" @if ($found) style="background: orange;" @endif>
    <td style="vertical-align: middle; white-space: nowrap;" >
        {!! $account->code !!}
    </td>
    <td style="vertical-align: middle; text-align:center1; white-space: nowrap;">
        {!! $account->name !!}
    </td>
    <td style="vertical-align: middle;">
    {!! $account->description !!}
    </td>
    <td style="vertical-align: middle; text-align:center;">
        {!! $account->property !!}
    </td>
    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
        @if($account->status == 0 && $account->parent_id != null)
            <span class="label label-danger"><span class='glyphicon glyphicon-remove'></span></span>
        @elseif($account->status == 1 && $account->parent_id != null)
            <span class="label label-success"><span class='glyphicon glyphicon-ok'></span></span>
        @endif
    </td>
    <td style="text-align: right; vertical-align: middle; white-space: nowrap;">
        @if($account->parent_id)
            @if ($account->status)
            <a style="cursor: pointer" data-group="{!! $account->group_code !!}" data-code="{!! $account->code !!}" class="add-account" class="btn btn-xs btn-default" data-dismiss={!! $account->code !!} data-toggle="tooltip" title="{!! trans('system.action.create') !!} {!! trans('accounts.label') !!}">  <span class="glyphicon glyphicon-plus"></span></a>
            &nbsp;&nbsp;
            @endif
            <a style="cursor: pointer" data-group="{!! $account->group_code !!}" data-id={!! $account->id !!} data-code={!! $account->code !!} class="edit-acocunt" class="btn btn-xs btn-default"  data-toggle="tooltip" title="{!! trans('system.action.update') !!} {!! trans('accounts.label') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
            &nbsp;&nbsp;
            <a href="javascript:void(0)" link="{!! route('admin.accounts.destroy', $account->id) !!}" class="btn-confirm-del btn btn-default btn-xs" data-toggle="tooltip" title="{!! trans('system.action.remove') !!} {!! trans('accounts.label') !!}">
                <i class="text-danger glyphicon glyphicon-remove"></i>
        @endif
    </td>
</tr>