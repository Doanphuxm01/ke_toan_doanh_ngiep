@foreach ($accounts as $account)
<?php
    $dots = "";
    for($i = 0; $i < $level; $i++) $dots .= "__.";
?>
{!! $account->code !!}|{!! $dots !!}{!! $account->code !!}
<?php
    $children = \App\Models\Account::whereRaw("(status=1 OR (id IN (" . implode(',', $parentIds) . "))) AND company_id=" . $account->company_id . " AND parent_id='" . $account->code . "'")->get();
?>
@include('backend.accounts.row-edit-tree', ['accounts' => $children, 'level' => $level+1])
@endforeach