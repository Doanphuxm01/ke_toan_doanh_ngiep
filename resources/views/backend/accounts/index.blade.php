@extends('backend.master')
@section('title')
    {!! trans('system.action.list') !!} {!! trans('accounts.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" href="{!! asset('assets/backend/plugins/treegrid/css/jquery.treegrid.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('accounts.label') !!}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.accounts.index') !!}">{!! trans('accounts.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-header with-bconsumer">
                <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <div class="box-body">
                {!! Form::open(['url' => route('admin.accounts.index'), 'method' => 'GET', 'role' => 'search']) !!}
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('code', trans('accounts.code')) !!}
                            {!! Form::text('code', Request::input('code'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('show_view', trans('accounts.show_view')) !!}
                            {!! Form::select('show_view', [0 => trans('system.no'), 1 => trans('system.yes')], Request::input('show_view'), ['class' => 'form-control select2'])!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                            <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row" style="display: flex;">
            <div class="col-sm-1" style="margin-right: 3px;">
                <a id='create' class='btn btn-primary btn-flat'>
                    <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
                </a>
            </div>
        </div>
        @if (count($accounts) > 0)
            <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
            <div class="box">
                <div class="box-header">
                    <?php $i = $accounts->count(); ?>
                </div>
                <div class="box-body no-padding">
                    <div class="table-responsive">
                        <table  class="table table-striped table-bordered table-tree">
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle; width: 10%;">{!! trans('accounts.code') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; width: 15%;">{!! trans('accounts.name') !!}</th>
                                    <th style="text-align: center; vertical-align: middle;">{!! trans('accounts.description') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; width: 15%;">{!! trans('accounts.property') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; width: 10%;">{!! trans('system.status.label') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; width: 10%;">{!! trans('system.action.label') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $code = strtoupper(trim(Request::input('code'))); ?>
                                @foreach ($accounts as $item)
                                    <?php $item->children($code); ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
        @endif
        <div id="edit-account" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{ trans('system.action.edit') }}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::hidden('edit_account_id') !!}
                        <table class='table borderless'>
                            <tr>
                                <th class="table_right_middle" style="width: 15%;">
                                    {!! trans('accounts.group') !!}
                                </th>
                                <td style="width: 30%;">
                                    {!! Form::select('edit_group', $groups, old('edit_group'), ['class' => 'form-control select2']) !!}
                                </td>
                                <th class="table_right_middle" style="width: 15%;">
                                    {!! trans('accounts.parent_id') !!}
                                </th>
                                <td>
                                    {!! Form::select('edit_parent_id', ['' => trans('system.dropdown_choice')], old('edit_parent_id'), ['class' => 'form-control select2']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th class="table_right_middle">
                                    {!! trans('accounts.code') !!}
                                </th>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon edit_parent_code"></span>
                                        {!! Form::text('edit_code', old('edit_code'), ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                                    </div>
                                </td>
                                <th class="table_right_middle">
                                    {!! trans('accounts.name') !!}
                                </th>
                                <td>
                                    {!! Form::text('edit_name', old('edit_name'), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th class="table_right_middle">
                                    {!! trans('accounts.description') !!}
                                </th>
                                <td>
                                    {!! Form::text('edit_description', old('edit_description'), ['class' => 'form-control', 'maxlength' => 255]) !!}
                                </td>
                                <th class="table_right_middle" style="width: 15%;">
                                    {!! trans('accounts.property') !!}
                                </th>
                                <td>
                                    {!! Form::select('edit_property', App\Defines\Account::getPropertiesForOption(), old('edit_property'), ['class' => 'form-control select2']) !!}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" colspan="4">
                                    {!! Form::checkbox('edit_active', 1, old('edit_active', 1), ['class' => 'minimal']) !!} &nbsp;{!! trans('system.status.active') !!}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" colspan="4">
                                    <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat', 'id' => 'edit_save']) !!}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="add-account" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{ trans('system.action.create') }}</h4>
                    </div>
                    <div class="modal-body">
                        <table class='table borderless'>
                            <tr>
                                <th class="table_right_middle" style="width: 15%;">
                                    {!! trans('accounts.group') !!}
                                </th>
                                <td style="width: 30%;">
                                    {!! Form::select('add_group', ['' =>  trans('system.dropdown_choice')] + $groups, old('add_group'), ['class' => 'form-control select2']) !!}
                                </td>
                                <th class="table_right_middle" style="width: 15%;">
                                    {!! trans('accounts.parent_id') !!}
                                </th>
                                <td>
                                    {!! Form::select('add_parent_id', ['' => trans('system.dropdown_choice')], old('add_parent_id'), ['class' => 'form-control select2']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th class="table_right_middle">
                                    {!! trans('accounts.code') !!}
                                </th>
                                <td>
                                    <div class="input-group">
                                        <span class="input-group-addon add_parent_code"></span>
                                        {!! Form::text('add_code', old('add_code'), ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                                    </div>
                                </td>
                                <th class="table_right_middle">
                                    {!! trans('accounts.name') !!}
                                </th>
                                <td>
                                    {!! Form::text('add_name', old('add_name'), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th class="table_right_middle">
                                    {!! trans('accounts.description') !!}
                                </th>
                                <td>
                                    {!! Form::text('add_description', old('add_description'), ['class' => 'form-control', 'maxlength' => 255]) !!}
                                </td>
                                <th class="table_right_middle" style="width: 15%;">
                                    {!! trans('accounts.property') !!}
                                </th>
                                <td>
                                    {!! Form::select('add_property', ['' =>  trans('system.dropdown_choice')] + App\Defines\Account::getPropertiesForOption(), old('add_property'), ['class' => 'form-control select2']) !!}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" colspan="4">
                                    {!! Form::checkbox('add_active', 1, old('add_active', 1), ['class' => 'minimal']) !!} &nbsp;{!! trans('system.status.active') !!}
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" colspan="4">
                                    <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat', 'id' => 'add_save']) !!}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/treegrid/js/jquery.treegrid.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/jquery.cookie.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $(".select2").select2({ width: '100%' });
                $("#create").click(function (e) {
                    e.preventDefault();
                    $("input[name='add_code']").val("");
                    $("input[name='add_description']").val("");
                    $('.add_parent_code').html("");
                    $("select[name='add_group']").val("").trigger('change');
                    $("input[name='add_active']").iCheck('check');
                    $('#add-account').modal('show');
                });
                var code = "", editCode = "";
                $("select[name='add_group']").change(function (e) {
                    e.preventDefault();
                    if ($(this).val() == "") {
                        $("select[name='add_parent_id']").empty().append($("<option></option>").attr("value", '').text('{!! trans('system.dropdown_choice') !!}'));
                        $("input[name='add_code']").val("");
                        $('.add_parent_code').html("");
                        return false;
                    }
                    NProgress.start();
                    $.getJSON("{!! route('admin.accounts.get-by-group') !!}?id=" + $(this).val() + "&time=" + {!! time() !!}).done(function (data) {
                        $("select[name='add_parent_id']").empty().append($("<option></option>").attr("value", '').text('{!! trans('accounts.level1') !!}'));
                        $.each(data.accounts, function(key, value) {
                            if (key == code) {
                                $('.add_parent_code').html(code.substring(0, 5));
                                $("input[name='add_code']").val(code.substring(5, code.length));
                                $("select[name='add_parent_id']").append($("<option selected></option>").attr("value", key).text(value));
                            } else {
                                $("select[name='add_parent_id']").append($("<option></option>").attr("value", key).text(value));
                            }
                        });
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                });
                $("select[name='edit_group']").change(function (e) {
                    if (editCode == "") return false;
                    e.preventDefault();
                    if ($(this).val() == "") {
                        $("select[name='edit_parent_id']").empty().append($("<option></option>").attr("value", '').text('{!! trans('system.dropdown_choice') !!}'));
                        $("input[name='edit_code']").val("");
                        $('.edit_parent_code').html("");
                        return false;
                    }
                    NProgress.start();
                    $.getJSON("{!! route('admin.accounts.get-by-group') !!}?id=" + $(this).val() + "&time=" + {!! time() !!}).done(function (data) {
                        $("select[name='edit_parent_id']").empty().append($("<option></option>").attr("value", '').text('{!! trans('accounts.level1') !!}'));
                        $.each(data.accounts, function(key, value) {
                            if (key == code) {
                                $('.edit_parent_code').html(code.substring(0, 5));
                                $("input[name='edit_code']").val(code.substring(5, code.length));
                                $("select[name='edit_parent_id']").append($("<option selected></option>").attr("value", key).text(value));
                            } else {
                                $("select[name='edit_parent_id']").append($("<option></option>").attr("value", key).text(value));
                            }
                        });
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                });
                $(document).on("change", "select[name='add_parent_id']", function(e) {
                    var tmpCode = $(this).val();
                    $('.add_parent_code').html(tmpCode.substring(0, 5));
                    $("input[name='add_code']").val(tmpCode.substring(5, tmpCode.length));
                });
                $(document).on("change", "select[name='edit_parent_id']", function(e) {
                    var tmpCode = $(this).val();
                    $('.edit_parent_code').html(tmpCode.substring(0, 5));
                    $("input[name='edit_code']").val(tmpCode.substring(5, tmpCode.length));
                });
                $("#add_save").click(function (e) {
                    e.preventDefault();
                    var group = $("select[name='add_group']").val(),
                        parent = $("select[name='add_parent_id']").val(),
                        code = $("input[name='add_code']").val(),
                        name = $("input[name='add_name']").val(),
                        description = $("input[name='add_description']").val(),
                        property = $("select[name='add_property']").val(),
                        status = $("input[name='add_active']").is(":checked") ? 1 : 0;
                    NProgress.start();
                    $.ajax({
                        url: "{!! route('admin.accounts.store') !!}",
                        data: { group: group, parent: parent, code: code, name: name, description: description, property: property, status: status },
                        type: 'POST',
                        datatype: 'json',
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        success: function(res) {
                            onReload('code', res.code);
                        },
                        error: function(obj, status, err) {
                            var error = $.parseJSON(obj.responseText);
                            toastr.error(error.message, '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                        }
                    }).always(function() {
                        NProgress.done();
                    });
                });
                $("#edit_save").click(function (e) {
                    e.preventDefault();
                    var group = $("select[name='edit_group']").val(),
                        parent = $("select[name='edit_parent_id']").val(),
                        code = $("input[name='edit_code']").val(),
                        name = $("input[name='edit_name']").val(),
                        description = $("input[name='edit_description']").val(),
                        property = $("select[name='edit_property']").val(),
                        status = $("input[name='edit_active']").is(":checked") ? 1 : 0;
                    NProgress.start();
                    var url = "{!! route('admin.accounts.update', ':id') !!}";
                    url = url.replace(':id', parseInt($("input[name='edit_account_id']").val()) || 0);
                    $.ajax({
                        url: url,
                        data: { group: group, parent: parent, code: code, name: name, description: description, property: property, status: status },
                        type: 'PUT',
                        datatype: 'json',
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        success: function(res) {
                            onReload('code', res.code);
                        },
                        error: function(obj, status, err) {
                            var error = $.parseJSON(obj.responseText);
                            toastr.error(error.message, '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                        }
                    }).always(function() {
                        NProgress.done();
                    });
                });
                $(document).on("click", ".add-account", function(e) {
                    code = $(this).attr('data-code');
                    $("select[name='add_group']").val($(this).attr('data-group')).trigger('change');
                    $('#add-account').modal('show');
                });
                $(document).on("click", ".edit-acocunt", function(e) {
                    editCode = "";
                    NProgress.start();
                    $("select[name='edit_group']").val($(this).attr('data-group')).trigger('change');
                    $.getJSON("{!! route('admin.accounts.get-by-id') !!}?id=" + $(this).attr('data-id') + "&time=" + {!! time() !!}).done(function (data) {
                        var account = data.account;
                        $("input[name='edit_code']").val(data.code);
                        editCode = data.code;
                        $("input[name='edit_description']").val(account.description);
                        $("select[name='edit_property']").val(account.property).trigger('change');
                        $("select[name='edit_parent_id']").empty().append($("<option></option>").attr("value", '').text('{!! trans('accounts.level1') !!}'));
                        $.each(data.data, function(key, value) {
                            if (key == account.parent_id) {
                                $("select[name='edit_parent_id']").append($("<option selected></option>").attr("value", key).text(value));
                            } else {
                                $("select[name='edit_parent_id']").append($("<option></option>").attr("value", key).text(value));
                            }
                        });
                        $("input[name='edit_name']").val(account.name);
                        $("input[name='edit_account_id']").val(account.id);
                        $('.edit_parent_code').html(data.parent_code);
                        if (account.status == 1) {
                            $("input[name='edit_active']").iCheck('check');
                        } else {
                            $("input[name='edit_active']").iCheck('uncheck');
                        }
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                    $('#edit-account').modal('show');
                });
            });
        }(window.jQuery);
        @if(Request::input('show_view'))
            $('.table-tree').treegrid({'saveState1': true});
        @else
            $('.table-tree').treegrid({'initialState': 'collapsed', 'saveState1': true});
        @endif
        var codes = "";
        $('.table-tree').find('tr').each(function() {
            if ($(this).hasClass("need-expand")) {
                codes += $.trim($(this).find('td').eq(0).text()) + ".";
            }
        });
        if (codes)  {
            NProgress.start();
            $.getJSON("{!! route('admin.accounts.get-parents') !!}?codes=" + codes).done(function (data) {
                $.each(data.parents, function(index, code) {
                    $(".treegrid-" + code).treegrid("expand");
                });
            }).fail(function(jqxhr, textStatus, error) {
                toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
            }).always(function() {
                NProgress.done();
            });
        }
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key] = $value;
            // queryParameters['trang'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
