@foreach ($accounts as $account)
<?php
    $dots = "";
    for($i = 0; $i < $level; $i++) $dots .= "__.";
?>
{!! $account->code !!}|{!! $dots !!}{!! $account->code !!}
<?php
    $children = \App\Models\Account::where('status', 1)->where('company_id', $account->company_id)->where('parent_id', $account->code)->get();
?>
@include('backend.accounts.row-tree', ['accounts' => $children, 'level' => $level+1])
@endforeach