@extends('backend.master')
@section('title')
{!! trans('system.action.detail') !!} - {!! trans('menus.expenses-prepaid-allocation.label') !!}
@stop
@section('head')
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.expenses-prepaid-allocation.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.expenses-prepaid-allocation.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.expenses-prepaid-allocation.update', $voucher->id), 'method' => 'PUT', 'role' => 'form', 'files' => true]) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
            <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_depreciation.description') !!}</b>
                    </div>    
                    <div class="col-md-4">
                        {!! Form::text('voucher_description', old('voucher_description', $voucher->description), ['class' => 'form-control', 'disabled']) !!}
                    </div>     
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_depreciation.voucher_date') !!}</b>
                    </div>    
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('voucher_date', old('voucher_date', \Carbon\Carbon::createFromTimestamp(strtotime($voucher->voucher_date))->format('d-m-Y')), ['class' => 'form-control voucher_date', 'disabled']) !!}       
                        </div>
                    </div>         
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <b> {!! trans('fixed_asset_depreciation.month') !!}</b>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('month', old('month', $voucher->month), ['class' => 'form-control', 'disabled']) !!}
                            </div>
                            <div class="col-md-2">
                                <b>{!! trans('fixed_asset_depreciation.year') !!}</b>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('year', old('year', $voucher->year), ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_depreciation.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('voucher_no', old('voucher_no', $voucher->voucher_no), ['class' => 'form-control', 'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.determine_cost_of_allocation') !!}</h3>
                <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div style="margin-right:20px"> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 100px;">
                                            {!! trans('instrument_tools.code') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 300px;">
                                            {!! trans('instrument_tools.name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('expenses_prepaid.periodical_allocation_value') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.value_in_cost') !!}
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 200px;">
                                            {!! trans('fixed_assets.use_department') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 80px;">
                                            {!! trans('fixed_assets.allocation_rate') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.money_amount') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.expense_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.cost_item') !!}
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    @foreach($instrument_tool as $item)
                    <div class='cost' style="display: flex;">
                        <div style="margin-right:20px"> 
                            <table>
                                <tr>
                                    <td style="white-space: nowrap; min-width: 100px;">
                                        {!! Form::text('asset_code[]', old('asset_code', $item->code), ['class' => 'form-control', 'disabled' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 300px;">
                                        {!! Form::text('asset_name[]', old('asset_name', $item->name), ['class' => 'form-control', 'disabled' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::text('monthly_depreciation_value[]', old('monthly_depreciation_value',  App\Models\InstrumentToolAllocationCost::getValue($item->id, $voucher->id)), ['class' => 'form-control money', 'disabled']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::text('value_in_cost[]', old('value_in_cost', App\Models\InstrumentToolAllocationCost::getValue($item->id, $voucher->id)), ['class' => 'form-control money', 'disabled']) !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <?php $allocation_info = App\Models\InstrumentToolAllocation::getAllocationCost($item->id, $voucher->id) ?>     
                            <table>
                                @foreach($allocation_info as $allo_info)
                                <tr>
                                    {!! Form::hidden('allo_info_id[]', old('allo_info_id', $allo_info->id), ['class' => 'form-control', 'disabled' ]) !!}
                                    {!! Form::hidden('a_c[]', old('a_c', $allo_info->asset_code), ['class' => 'form-control']) !!}
                                    {!! Form::hidden('department_id[]', old('department_id', $allo_info->department_id), ['class' => 'form-control', 'disabled']) !!}
                                    <td style="white-space: nowrap; min-width: 200px;"> 
                                        {!! Form::text('use_department[]', old('use_department', App\Models\Department::getDepartment($allo_info->department_id)), ['class' => 'form-control', 'disabled' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 80px;">
                                        {!! Form::text('allocation_rate[]', old('allocation_rate', $allo_info->allocation_rate), ['class' => 'form-control', 'disabled' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::text('money_amount[]', old('money_amount', $allo_info->money_amount), ['class' => 'form-control money', 'disabled']) !!}
                                    </td>   
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::select('expense_account[]', ['' => trans('system.dropdown_choice')] + App\Models\Account::getExpenseAccounts(session('current_company')), old('expense_account', $allo_info->expense_account), ['class' => 'form-control select2 expense_account', 'disabled']) !!}
                                        {!! Form::hidden('allocation_account[]', old('allocation_account', $item->allocation_account), ['class' => 'form-control']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::select('cost_item[]',['' => trans('system.dropdown_choice')] + App\Models\CostItem::getActiveCostItem(session('current_company')), old('cost_item', $allo_info->cost_item), ['class' => 'form-control select2', 'disabled' ]) !!}
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        <br>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.accounting') !!}</h3>
            </div>
            <div class="box-body">
               <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead style="background: #3C8DBC; color: white;">
                            <tr>
                                <th style="white-space: nowrap; min-width: 300px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.description') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.debit_account') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.credit_account') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.money_amount') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.debit_object') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 250px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.object_name') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.credit_object') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 250px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.object_name') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('cost_items.cost_item') !!}
                                </th>
                            </tr>
                        </thead>
                        <tbody class='accounting'>
                        @foreach($accounting as $item)
                            <tr>
                            {!! Form::hidden('accounting_id[]', old('accounting_id', $item->id), ['class' => 'form-control']) !!}
                                <td style="width:20%">
                                    {!! Form::text('description[]', old('description', $item->description), ['class' => 'form-control','disabled']) !!}
                                </td>
                                <td style="width:10%"> 
                                    {!! Form::text('debit_account[]', old('debit_account', $item->debit_account), ['class' => 'form-control', 'disabled']) !!}
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('credit_account[]', old('credit_account', $item->credit_account), ['class' => 'form-control', 'disabled']) !!}
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('amount[]', old('amount', $item->amount), ['class' => 'form-control money', 'disabled']) !!}
                                </td>   
                                <td style="width:10%">
                                    {!! Form::text('debit_object_type[]', old('debit_object_type', $item->debit_object_type), ['class' => 'form-control', 'disabled']) !!}
                                </td>
                                <td style="width:10%">
                                @if($item->debit_object_type == "DEPARTMENTS")
                                    {!! Form::text('debit_object_id[]', old('debit_object_id[]', App\Models\Accounting::getDepartmentObject($item->debit_object_id)), ['class' => 'form-control', 'disabled']) !!}
                                @elseif($item->debit_object_type == "USERS")
                                    {!! Form::text('debit_object_id[]', old('debit_object_id[]', App\Models\Accounting::getUserObject($item->debit_object_id)), ['class' => 'form-control', 'disabled']) !!}
                                @elseif($item->debit_object_type == "PARTNERS")
                                    {!! Form::text('debit_object_id[]', old('debit_object_id[]', App\Models\Accounting::getPartnerObject($item->debit_object_id)), ['class' => 'form-control', 'disabled']) !!}
                                @else
                                    {!! Form::text('debit_object_id[]', old('debit_object_id[]'), ['class' => 'form-control', 'disabled']) !!}
                                @endif
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('credit_object_type[]', old('credit_object_type', $item->credit_object_type), ['class' => 'form-control', 'disabled']) !!}
                                </td>
                                <td style="width:10%">
                                @if($item->credit_object_type == "DEPARTMENTS")
                                    {!! Form::text('credit_object_id[]', old('credit_object_id[]', App\Models\Accounting::getDepartmentObject($item->credit_object_id)), ['class' => 'form-control', 'disabled']) !!}
                                @elseif($item->credit_object_type == "USERS")
                                    {!! Form::text('credit_object_id[]', old('credit_object_id[]', App\Models\Accounting::getUserObject($item->credit_object_id)), ['class' => 'form-control', 'disabled']) !!}
                                @elseif($item->credit_object_type == "PARTNERS")
                                    {!! Form::text('credit_object_id[]', old('credit_object_id[]', App\Models\Accounting::getPartnerObject($item->credit_object_id)), ['class' => 'form-control', 'disabled']) !!}
                                @else
                                    {!! Form::text('credit_object_id[]', old('credit_object[]'), ['class' => 'form-control', 'disabled']) !!}
                                @endif
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('c_a[]', old('c_a', $item->cost_item), ['class' => 'form-control', 'disabled']) !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" style="font-size:18px" >
                                    {!! trans('fixed_asset_depreciation.total') !!}:
                                </th>
                                <th class="text-right" style="font-size:18px" id="total" >
                                </th> 
                                <th colspan="5" >
                                </th>
                            </tr>
                        </tfoot>
                    </table>   
                </div>
            </div>
        </div>
        
        {!! Form::close() !!}
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $(".select2").select2({
        dropdownAutoWidth: true,
    });
    $('.voucher_date').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'removeMaskOnSubmit': true});
</script>
<script>
    var d = new Date();
    var y = d.getFullYear();
    $('select[name="year"]').yearselect({start:2010}).select2();
    
    
    $(document).ready(function(){
        var total_amount = 0;
        $("input[name='money_amount[]']").each(function() {
            var money_amount = $(this).val().replace(/,/g, "");
            total_amount += Number(money_amount);
        }); 
        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        $("input[name='value_in_cost[]']").each(function() {
            var value_in_cost = $(this).val().replace(/,/g, "");
            var a = $(this).closest('.cost').find(("input[name='allocation_rate[]']"))
            a.each(function() {
                var allocation_rate = $(this).val() 
                b = $(this).closest('tr').find(("input[name='money_amount[]']"))
                b.val(value_in_cost*allocation_rate/100)
            });
        });            
    });    

@stop
