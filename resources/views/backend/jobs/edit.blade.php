@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('jobs.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.jobs.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.jobs.index') !!}">{!! trans('jobs.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.jobs.update', $job->id), 'method' => 'PUT', 'role' => 'form']) !!}
    <table class='table borderless'>
        <tr>
            <th class="table_right_middle" style="width: 15%;">
                {!! trans('jobs.code') !!}
            </th>
            <td style="width: 35%;">
                {!! Form::text('code', old('code', $job->code), ['class' => 'form-control', 'maxlength' => 20]) !!}
            </td>
            <th class="table_right_middle" style="width: 15%;">
                {!! trans('jobs.branch_code') !!}
            </th>
            
            <td>
                {!! Form::select('branch_code', ['' => '--'] + $branchCodes, old('branch_code', $job->branch_code), ['class' => 'form-control select2', 'required']) !!}
            </td>
        </tr>
        <tr>
            
            <th class="table_right_middle">
                {!! trans('jobs.job_type.label') !!}
            </th>
            <td>
                {!! Form::select('type', ['' => '--'] + App\Defines\Job::getJobTypeForOption(), old('type', $job->type), ['class' => 'form-control select2']) !!}
            </td>
            <th class="table_right_middle">
                {!! trans('jobs.job_group.label') !!}
            </th>
            <td>
                {!! Form::select('group', ['' => '--'] + App\Defines\Job::getGroupForOption(), old('group', $job->group), ['class' => 'form-control select2']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle"> 
                {!! trans('jobs.job_month') !!}
            </th>
            <td>
                <div class="month_year" style="width: 20%">
                {!! Form::select('month', ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5' ,'6' => '6' , '7' => '7' , '8' => '8' , '9' => '9', '10' => '10' ,'11' => '11' ,'12' => '12'], old('month', $job->month), ['class' => 'form-control select2', 'required', 'style'=>'width:15%']) !!}
                </div>
                <div class="month_year" style="width: 10%">
                    <label for="">{!! trans('jobs.job_year') !!}</label> 
                </div>
                <div class="month_year" style="width: 20%">
                    {!! Form::select('year',['' => '--'], old('year', $job->year), ['class' => 'form-control select2', 'maxlength' => 50]) !!}
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-center table_right_middle1" colspan="4">
                <label>
                    {!! Form::checkbox('status', 1, old('status', $job->status), [ 'class' => 'minimal' ]) !!}
                    {!! trans('system.status.active') !!}
                </label>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="text-center">
                {!! HTML::link(route( 'admin.jobs.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
            </td>
        </tr>
    </table>

    {!! Form::close() !!}
@stop
@section('footer')
    <style>
        .month_year {
            display: inline-block;
        }
        .month_year + .month_year {
            margin-left: 10px
        }
    </style>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
            });
        }(window.jQuery);
        var d = new Date();
        var y = d.getFullYear();
        $('select[name="year"]').yearselect({start:2010}).select2({ width: '100%' });
    </script>
@stop