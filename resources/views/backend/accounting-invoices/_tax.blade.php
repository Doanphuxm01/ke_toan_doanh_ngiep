<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered tax">
                <thead style="background: #3C8DBC;color: white;">
                    <tr>
                        <th style="vertical-align: middle; text-align: center;">{!! trans('system.action.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.description') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.tax_rate.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans("taxes.taxation") !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans('taxes.exchange_tax') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans("taxes.unit_price") !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans('taxes.exchange_unit') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.account_tax') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.group_unit') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.invoice_no') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.date_invoice') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('taxes.object_code') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 300px;">{!! trans('taxes.object_name') !!}</th>
                        <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans('taxes.tax_code') !!}</th>
                    </tr>
                </thead>
                <tbody>
				<?php $tax_rate = old('tax_rate', []); ?>
                @if (count($tax_rate))
					<?php
					$desc_tax = old('desc', []);
					$tax_rate = old('tax_rate');
					$taxation = old('taxation');
					$exchange_tax = old('exchange_tax');
					$unit_price = old('unit_price');
					$exchange_unit = old('exchange_service');
					$account_tax = old('account_tax');
					$invoice_no = old('invoice_no');
					$date_invoice = old('date_invoice');
					$group_unit = old('group_unit');
					$object_code_tax = old('object_code_tax');
					$object_name_tax = old('object_name_tax');
					$account_tax = old('account_tax');
					$tax_code = old('tax_code');
					$object_name_tax = old('object_name_tax');
					$tax_code = old('tax_code');
					$total_taxation = array_sum($taxation);
					$total_exchange_tax = array_sum($exchange_tax);
					$total_unit_price = array_sum($unit_price);
					$total_exchange_unit = array_sum($exchange_unit);
					?>
                    @for ($i = 0; $i < count($tax_rate); $i++)
                    <tr>
                        {!! Form::hidden("tax_id[$i]", '') !!}
                        <td style="text-align: center; vertical-align: middle;">
                            <a tabindex="-1" href="javascript:void(0);"
                               title="{!! trans('input_invoices.copy_current_row') !!}"
                               class="btn btn-xs btn-default clone-row-tax">
                                <i class="fas fa-copy text-success"></i>
                            </a>
                            @if ($i > 0)
                                <a tabindex="-1" href="javascript:void(0);" class="btn btn-xs btn-default remove-tax">
                                    <i class="text-danger fa fa-minus"></i>
                                </a>
                            @endif
                        </td>
                        <td>
                            {!! Form::text("desc_tax[$i]", old("desc_tax[$i]", $desc_tax[$i]), ['class' => 'form-control']) !!}
                        </td>
                        <td>
                            {!! Form::select("tax_rate[$i]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old("tax_rate[$i]", $tax_rate[$i]), ['class' => 'form-control select2 tax-rate']) !!}
                        </td>
                        <td>
                            {!! Form::text("taxation[$i]", old("taxation[$i]", $taxation[$i]), ['class' => 'form-control currency taxation']) !!}
                        </td>
                        <td>
                            {!! Form::text("exchange_tax[$i]", old("exchange_tax[$i]", $exchange_tax[$i]), ['class' => 'form-control currency exchange-tax']) !!}
                        </td>
                        <td>
                            {!! Form::text("unit_price[$i]", old("unit_price[$i]", $unit_price[$i]), ['class' => 'form-control currency unit-price']) !!}
                        </td>
                        <td>
                            {!! Form::text("exchange_unit[$i]", old("exchange_unit[$i]", $exchange_unit[$i]), ['class' => 'form-control currency exchange-unit'  ]) !!}
                        </td>
                        <td>
                            {!! Form::select("account_tax[$i]", [' '=> trans('system.dropdown_choice')]  + \App\Models\AccountingInvoice::getPropertyAccountForOption('CREDIT'), old("account_tax[$i]", $account_tax[$i]), ['class' => 'form-control select2']) !!}
                        </td>
                        <td>
                            {!! Form::select("group_unit[$i]", [' '=> trans('system.dropdown_choice')] + \App\Models\GroupUnit::getGroupUnit(), old("group_unit[$i]", $group_unit[$i]), ['class' => 'form-control select2']) !!}
                        </td>
                        <td>
                            {!! Form::text("invoice_no[$i]", old("invoice_no[$i]", $invoice_no[$i]), ['class' => 'form-control']) !!}
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text("date_invoice[$i]",  old("date_invoice[$i]", $date_invoice[$i]), ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </td>
                        <td>
                            {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + \App\Models\Partner::getAllPartner() , old('object_code_tax[]', $object_code_tax[$i]), ['class' => 'form-control select2 object-code-tax']) !!}
                        </td>
                        <td>
                            {!! Form::text("object_name_tax[$i]", old("object_name_tax[$i]", $object_name_tax[$i]), ['class' => 'form-control object-name-tax', 'readonly']) !!}
                        </td>
                        <td>
                            {!! Form::text("tax_code[$i]", old("tax_code[$i]", $tax_code[$i]), ['class' => 'form-control tax-code', 'readonly']) !!}
                        </td>
                    </tr>
                    @endfor
                @else
                    <tr>
                        {!! Form::hidden("tax_id[]", '') !!}
                        <td style="text-align: center; vertical-align: middle;">
                            <a tabindex="-1" href="javascript:void(0);"
                               title="{!! trans('input_invoices.copy_current_row') !!}"
                               class="btn btn-xs btn-default clone-row-tax">
                                <i class="fas fa-copy text-success"></i>
                            </a>
                        </td>
                        <td>
                            {!! Form::text("desc_tax[]", old('desc_tax[]'), ['class' => 'form-control']) !!}
                        </td>
                        <td>
                            {!! Form::select("tax_rate[]", [''=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2 tax-rate']) !!}
                        </td>
                        <td>
                            {!! Form::text("taxation[]", old('taxation[]'), ['class' => 'form-control currency taxation']) !!}
                        </td>
                        <td>
                            {!! Form::text("exchange_tax[]", old('exchange_tax[]'), ['class' => 'form-control currency exchange-tax', 'readonly' => true]) !!}
                        </td>
                        <td>
                            {!! Form::text("unit_price[]", old('unit_price[]'), ['class' => 'form-control currency unit-price']) !!}
                        </td>
                        <td>
                            {!! Form::text("exchange_unit[]", old('exchange_unit[]'), ['class' => 'form-control currency exchange-unit', 'readonly' => true ]) !!}
                        </td>
                        <td>
                            {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getPropertyAccountForOption('CREDIT'), old('account_tax[]'), ['class' => 'form-control select2']) !!}
                        </td>
                        <td>
                            {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + \App\Models\GroupUnit::getGroupUnit(), old('group_unit[]'), ['class' => 'form-control select2']) !!}
                        </td>
                        <td>
                            {!! Form::text("invoice_no[]", old('invoice_no[]'), ['class' => 'form-control']) !!}
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text("date_invoice[]",  old('date_invoice[]'), ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </td>
                        <td>
                            {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + \App\Models\Partner::getAllPartner(), old('object_code[]'), ['class' => 'form-control select2 object-code-tax']) !!}
                        </td>
                        <td>
                            {!! Form::text("object_name_tax[]", old('object_name_tax[]'), ['class' => 'form-control object-name-tax', 'readonly']) !!}
                        </td>
                        <td>
                            {!! Form::text("tax_code[]", old('tax_code[]'), ['class' => 'form-control tax-code', 'readonly']) !!}
                        </td>
                    </tr>
                @endif
                    <tr>
                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                            <a href="javascript:void(0);" class="add-tax text-aqua" title="Add new row">
                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                            </a>
                        </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <th style="text-align: right; vertical-align: middle;">
                            {!! trans('input_invoices.total') !!}
                        </th>
                        <td style="white-space: nowrap;">
                            {!! Form::text('total_taxation', old('exchange_tax', App\Helper\HString::currencyFormat($exchange_tax)), ['class' => 'form-control currency', 'id'=>"total-taxation", 'readonly' => 'true']) !!}
                        </td>
                        <td style="white-space: nowrap;">
                            {!! Form::text('total_exchange_tax', old('total_exchange_tax', App\Helper\HString::currencyFormat($total_exchange_tax)), ['class' => 'form-control currency', 'id'=>"total-exchange-tax", 'readonly' => 'true']) !!}
                        </td>
                        <td style="white-space: nowrap;">
                            {!! Form::text('total_unit_price', old('total_unit_price', App\Helper\HString::currencyFormat($total_unit_price)), ['class' => 'form-control currency', 'id'=>"total-unit-price", 'readonly' => 'true']) !!}
                        </td>
                        <td style="white-space: nowrap;">
                            {!! Form::text('total_exchange_unit', old('total_exchange_unit', App\Helper\HString::currencyFormat($total_exchange_unit)), ['class' => 'form-control currency', 'id'=>"total-exchange-unit", 'readonly' => 'true']) !!}
                        </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <td style="vertical-align: middle;"> -- </td>
                        <td style="vertical-align: middle;"> -- </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    !function ($) {
        $(function () {
            $(document).on('click', '.add-tax', function(event) {
                let newRow = '{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.accounting-invoices._row_tax", compact('sci'))->render()) !!}';
                $('table.tax tr').last().before(newRow)
                callSelect2()
                callDatePicker()
                callInputMaskDecimal()
                setPriceUnitFromTaxRate($('.tax-rate'))
                setTotal($('#total-taxation'), $('#total-exchange-tax'), $('input.taxation'), '.exchange-tax')
                setTotal($('#total-unit-price'), $('#total-exchange-unit'), $('input.unit-price'), '.exchange-unit')
            })
            $(document).on("click", ".remove-tax", function (event) {
                $(this).closest("tr").remove();
                setPriceUnitFromTaxRate($('.tax-rate'))
                setTotal($('#total-taxation'), $('#total-exchange-tax'), $('input.taxation'), '.exchange-tax')
                setTotal($('#total-unit-price'), $('#total-exchange-unit'), $('input.unit-price'), '.exchange-unit')
            });
            $(document).on("keyup", 'input[name="taxation[]"], input.exchange-rate', function() {
              setTotal($('#total-taxation'), $('#total-exchange-tax'), $('input.taxation'), '.exchange-tax')
            })
            $(document).on("keyup", 'input[name="unit_price[]"], input.exchange-rate', function() {
                setTotal($('#total-unit-price'), $('#total-exchange-unit'), $('input.unit-price'), '.exchange-unit')
            })
            setTotal($('#total-taxation'), $('#total-exchange-tax'), $('input.taxation'), '.exchange-tax')
            setTotal($('#total-unit-price'), $('#total-exchange-unit'), $('input.unit-price'), '.exchange-unit')

            setPriceUnitFromTaxRate($('.tax-rate'))
            $(document).on("keyup change", '.tax-rate, input[name="taxation[]"]', function() {
                console.log('o')
                setPriceUnitFromTaxRate($('.tax-rate'))
                setTotal($('#total-unit-price'), $('#total-exchange-unit'), $('input.unit-price'), '.exchange-unit')
            })
            $(document).on('click', '.clone-row-tax', function(event) {
                let title = "{!! trans('input_invoices.remove_current_row') !!}";
                cloneRow($(this), 'remove-tax', title)
                setPriceUnitFromTaxRate($('.tax-rate'))
                setTotal($('#total-taxation'), $('#total-exchange-tax'), $('input.taxation'), '.exchange-tax')
                setTotal($('#total-unit-price'), $('#total-exchange-unit'), $('input.unit-price'), '.exchange-unit')
            });

            function getInfoObject($tr, id)
            {
                $.ajax({
                    type: "GET",
                    url: "{!! route('admin.payment-receipt-vouchers.get-info-object') !!}",
                    data: {id: id},
                    dataType: "json",
                    success: function (response) {
                        $tr.find('.object-name-tax').val(response.data.name);
                        $tr.find('.tax-code').val(response.data.tax_code);
                    },
                    error:function (response) {
                        let error = $.parseJSON(response.responseText);
                        toastr.error(error.message, 'Error');
                    }
                });
            }
            $(document).on("change", 'select[name^="object_code_tax"]', function() {
                let id = $(this).val()
                let tr = $(this).closest('tr')
                getInfoObject(tr, id)
            })

        });
    }(window.jQuery);
</script>
