<tr>
    {!! Form::hidden("accounting_id[]", '') !!}
    <td style="text-align: center; vertical-align: middle;">
        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}"
           class="btn btn-xs btn-default clone-row-accounting">
            <i class="fas fa-copy text-success"></i>
        </a>
        <a tabindex="-1" href="javascript:void(0);" class="btn btn-xs btn-default remove-row remove-accounting">
            <i class="text-danger fa fa-minus"></i>
        </a>
    </td>
    <td style="text-align: center; vertical-align: middle;">
        {!! Form::text("desc_accounting[]", old("desc_accounting[]"), ['class' => 'form-control' ]) !!}
    </td>
    <td style="text-align: center; vertical-align: middle;">
        {!! Form::select("job_code[]", ['' => trans('system.dropdown_choice')] + \App\Models\Job::getJobsForOption(), old("job_code[]"), ['class' => 'form-control select2']) !!}
    </td>
    <td style="vertical-align: middle;">
        {!! Form::select("debit_account[]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getPropertyAccountForOption('DEBT') , old("debit_account[]"), ['class' => 'form-control select2 ', ]) !!}
    </td>
    <td style="vertical-align: middle;">
        {!! Form::select("credit_account[]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getPropertyAccountForOption('CREDIT') , old("credit_account[]"), ['class' => 'form-control select2 ', ]) !!}
    </td>
    <td style="text-align: center; vertical-align: middle;">
        {!! Form::text("amount[]", old("amount[]"), ['class' => 'form-control currency amount']) !!}
    </td>
    <td style="text-align: center; vertical-align: middle;">
        {!! Form::text("exchange[]", old("exchange[]"), ['class' => 'form-control currency exchange', 'readonly' => true]) !!}
    </td>
    <td style="vertical-align: middle;">
        {!! Form::select("branch_code[]", ['' => trans('system.dropdown_choice')] + \App\Models\BranchCode::getBranchCode() , old("branch_code[]"), ['class' => 'form-control select2 ', ]) !!}
    </td>
    <td style="vertical-align: middle;">
        {!! Form::select("credit_object[]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('DEBT') , old("credit_object[]"), ['class' => 'form-control select2 ', ]) !!}
    </td>
    <td style="vertical-align: middle;">
        {!! Form::select("debit_object[]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('CREDIT') , old("debit_object[]"), ['class' => 'form-control select2 ', ]) !!}
    </td>
    <td style="vertical-align: middle;">
        {!! Form::select("cost_item[]", ['' => trans('system.dropdown_choice')] + \App\Models\CostItem::getActiveCostItem(session('current_company')) , old("cost_item[]"), ['class' => 'form-control select2 ', ]) !!}
    </td>
</tr>