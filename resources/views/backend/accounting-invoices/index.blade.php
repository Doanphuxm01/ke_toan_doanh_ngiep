@extends('backend.master')
@section('title')
    {!! trans('system.action.list') !!} {!! trans('accounting_invoices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css"
          href="{!! asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}"/>
    <style>

    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('accounting_invoices.label') !!}
            <small>{!! trans('system.action.list') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li>
                <a href="{!! route('admin.accounting-invoices.index') !!}">{!! trans('accounting_invoices.label') !!}</a>
            </li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <div class="box-body">
                {!! Form::open(['url' => route('admin.accounting-invoices.index'), 'method' => 'GET', 'role' => 'search']) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('from_date', trans('payment_receipt_vouchers.from_date')) !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('to_date', trans('payment_receipt_vouchers.to_date')) !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('desc', trans('accounting_invoices.desc')) !!}
                            {!! Form::text('desc', Request::input('desc'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('amount', trans('accounting.amount')) !!}
                            {!! Form::text('amount', Request::input('amount'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('status', trans('system.status.label')) !!}
                            {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                            <button type="submit" class="btn btn-primary btn-flat" style="display: block;">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row" style="display: flex;">
            <div class="col-sm-2" style="margin-right: 3px;">
                <a href="{!! route('admin.accounting-invoices.create') !!}" class='btn btn-primary btn-flat'>
                    <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
                </a>
            </div>
            <div class="col-sm-10 pull-right">
                <span class="pull-right">
                    {!! $accountingInvoices->appends( Request::except('page') )->render() !!}
                </span>
                <span class="pull-right">
                    {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
                </span>
            </div>
        </div>
        @if (count($accountingInvoices) > 0)
			<?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
            <div class="box">
                <div class="box-header">
					<?php $i = (($accountingInvoices->currentPage() - 1) * $accountingInvoices->perPage()) + 1; ?>
                    <div class="form-inline">
                        <div class="form-group">
                            {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $accountingInvoices->count()) . ' ( ' . trans('system.total') . ' ' . $accountingInvoices->total() . ' )' !!}
                        </div>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" style="border-collapse: collapse;">
                            <thead style="background: #3C8DBC; color: white;">
                                <tr>
                                    <th style="text-align: center; vertical-align: middle; min-width: 100px"> {!! trans('accounting_invoices.voucher_date') !!} </th>
                                    <th style="text-align: center; vertical-align: middle; min-width: 100px">{!! trans('accounting_invoices.voucher_no') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; min-width: 300px">{!! trans('accounting_invoices.desc') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; min-width: 100px">{!! trans('accounting.amount') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; min-width: 100px">{!! trans('system.status.active') !!}</th>
                                    <th style="text-align: center; vertical-align: middle; min-width: 100px">{!! trans('system.action.label') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($accountingInvoices as $item)
                                <tr class="treegrid-{!! $item->id !!}">
                                    <td style="text-align: center; vertical-align: middle;">
                                        <label class="label label-default">{!! $item->voucher_date->format('d/m/Y') !!}</label>
                                        {{-- {!! date('d/m/Y', strtotime($item->voucher_date)) !!}--}}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {!! $item->voucher_no !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {!! $item->desc !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {!! $item->total_amount !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if($item->status == 0)
                                            <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                        @elseif($item->status == 1)
                                            <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <a href="{!! route('admin.accounting-invoices.show', $item->id) !!}"
                                           class="btn btn-xs btn-default"
                                           title="{!! trans('system.action.edit') !!}">
                                            <i class="text-success glyphicon glyphicon-eye-open"></i>
                                        </a>
                                        <a href="{!! route('admin.accounting-invoices.edit', $item->id) !!}"
                                           class="btn btn-xs btn-default"
                                           title="{!! trans('system.action.edit') !!}">
                                            <i class="text-warning glyphicon glyphicon-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)"
                                           link="{!! route('admin.accounting-invoices.destroy', $item->id) !!}"
                                           class="btn-confirm-del btn btn-default btn-xs"
                                           title="{!! trans('system.action.delete') !!}">
                                            <i class="text-danger glyphicon glyphicon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr style="background-color: #ccc">
                                <th style="text-align: center; vertical-align: middle;">Total</th>
                                <td style="text-align: center; vertical-align: middle;"> -- </td>
                                <td style="text-align: center; vertical-align: middle;"> -- </td>
                                <th style="text-align: center; vertical-align: middle;">{!! $total !!}</th>
                                <td style="text-align: center; vertical-align: middle;"> -- </td>
                                <td style="text-align: center; vertical-align: middle;"> -- </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
            @endif
            </div>
    </section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/call-plugins.js') !!}"></script>

    <script>
        !function ($) {
            $(function () {
                callDatePicker()
                callSelect2()
            });
        }(window.jQuery);
    </script>
@stop