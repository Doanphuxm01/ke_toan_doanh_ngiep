@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('menus.accounting-invoices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}"/>
    <style>
        .content-global {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .content-global .row {
            margin: 8px 0;
        }

        .container-fluid .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            text-align: center;
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('accounting_invoices.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.accounting-invoices.index') !!}">{!! trans('accounting_invoices.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.accounting-invoices.storeOrUpdate'), 'role' => 'form']) !!}
    <section class="content-global overlay">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default with-border" >
                    <div class="box-body">
                        <div class="row">
                            <div class="row">
                                {!! Form::hidden('accounting_invoice_id', $accountingInvoice->id) !!}
                                <div class="col-md-1 text-right">
                                    {!! Form::label('desc', trans('accounting_invoices.desc')); !!}
                                </div>
                                <div class="col-md-5">
                                    {!! Form::text('desc', old('desc', $accountingInvoice->desc), ['class' => 'form-control desc' ]) !!}
                                </div>
                                <div class="col-md-offset-1 col-md-2 text-right">
                                    <label>{!! trans('accounting_invoices.voucher_no') !!}</label>
                                </div>
                                <div class="col-md-3">
                                    {!! Form::text('voucher_no', old('voucher_no', $accountingInvoice->voucher_no), ['class' => 'form-control voucher_no', 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-1 text-right">
                                    {!! Form::label('payment_term', trans('accounting_invoices.payment_term'), ['class' => 'payment_term']); !!}
                                </div>
                                <div class="col col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('payment_term', old('payment_term', date('d/m/Y', strtotime($accountingInvoice->payment_term)) ), ['class' => 'form-control datepicker', 'required', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="col col-md-offset-3 col-md-2 text-right">
                                    <label >{!! trans('accounting_invoices.voucher_date') !!}</label>
                                </div>
                                <div class="col col-md-3 text-left">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('voucher_date' ,old('voucher_date',  $accountingInvoice->voucher_date->format('d/m/Y') ), ['class' => 'form-control datepicker voucher-date', 'required', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-1 text-right">
                                    <label>{!! trans('accounting_invoices.currency') !!}</label>
                                </div>
                                <div class="col-md-2">
                                    {!! Form::select('currency', ['' => trans('system.dropdown_choice')] + \App\Models\Currency::getCurrency() ,old('currency', $accountingInvoice->currency), ['class' => 'form-control select2', 'required']) !!}
                                </div>
                                <div class="col-md-1 text-right">
                                    <label>{!! trans('accounting_invoices.exchange_rate') !!}</label>
                                </div>
                                <div class="col-md-2">
                                    {!! Form::text('exchange_rate' ,old('exchange_rate', $accountingInvoice->exchange_rate), ['class' => 'form-control currency exchange-rate', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row container-fluid">
        <div class="col-md-12" style="margin-top: 25px">
            <div class="box nav-tabs-custom active">
                <ul class="nav nav-tabs" style="font-weight: 700;">
                    <li class="nav-item active">
                        <a class="nav-link active" data-toggle="tab" href="#accounting-tab">Accounting</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tax-tab">Tax</a>
                    </li>
                </ul>
                <div class="box-body tab-content">
                    <div id="accounting-tab" class="tab-pane active">
                        @include('backend.accounting-invoices._accounting_edit')
                    </div>
                    <div id="tax-tab" class="tab-pane">
                        @include('backend.accounting-invoices._tax_edit')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-body row with-border">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        {!! HTML::link(route( 'admin.accounting-invoices.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                        {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat', 'style' => 'padding: 6px 16px;']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/fix-tabindex.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/accounting-invoice.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/call-plugins.js') !!}"></script>

    <script>
        !function ($) {
            $(function() {
                callDatePicker()
                enterTab()
                select2Tab()
                callInputMaskDecimal()
                callSelect2()
                autoDesc()
            });
        }(window.jQuery);
    </script>
    @include('backend.plugins.tinymce')
@stop