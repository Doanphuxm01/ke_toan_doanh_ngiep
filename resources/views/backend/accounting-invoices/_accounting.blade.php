<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered accounting">
                <thead style="background: #3C8DBC;color: white;">
                <tr>
                    <th style="text-align: center; vertical-align: middle; min-width: 50px;">{!! trans('system.action.label') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.description') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.job_code') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.debit_account') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.credit_account') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans('accounting.amount') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 130px;">{!! trans('accounting.exchange') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.branch_code') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.debit_object') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.credit_object') !!}</th>
                    <th style="text-align: center; vertical-align: middle; min-width: 150px;">{!! trans('accounting.cost_item') !!}</th>
                </tr>
                </thead>
                <tbody>
				<?php $credit_account = old('credit_account', []); ?>
                @if (count($credit_account))
					<?php
					$accounting_id = [];
					$desc_accounting = old('desc_accounting', []);
					$debit_account = old('debit_account', []);
					$job_code = old('job_code');
					$amount = old('amount');
					$exchange = old('exchange');
					$branch_code = old('branch_code');
					$object_code = old('object_code');
					$debit_object = old('debit_object');
					$credit_object = old('credit_object');
					$total_amount = array_sum($amount);
					$total_exchange = array_sum($exchange);
					?>
                    @for ($i = 0; $i < count($credit_account); $i++)
                        <tr>
                            {!! Form::hidden("accounting_id[$i]", '') !!}
                            <td style="text-align: center; vertical-align: middle;">
                                <a tabindex="-1" href="javascript:void(0);"
                                   title="{!! trans('input_invoices.copy_current_row') !!}"
                                   class="btn btn-xs btn-default clone-row-accounting">
                                    <i class="fas fa-copy text-success"></i>
                                </a>
                                @if ($i > 0)
                                    <a tabindex="-1" href="javascript:void(0);" class="btn btn-xs btn-default remove-row remove-accounting">
                                        <i class="text-danger fa fa-minus"></i>
                                    </a>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! Form::text("desc_accounting[$i]", old("desc_accounting[$i]"), ['class' => 'form-control desc-accounting' ]) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! Form::select("job_code[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\Job::getJobsForOption(), old("job_code[$i]", $job_code[$i]), ['class' => 'form-control select2']) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::select("debit_account[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getPropertyAccountForOption('DEBT'), old("debit_account[$i]", $debit_account[$i]), ['class' => 'form-control select2 ', ]) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::select("credit_account[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getPropertyAccountForOption('CREDIT') , old("credit_account[$i]", $credit_account[$i]), ['class' => 'form-control select2 ', ]) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! Form::text("amount[$i]", old("amount[$i]", $amount[$i]), ['class' => 'form-control currency amount']) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! Form::text("exchange[$i]", old("exchange[$i]", $exchange[$i]), ['class' => 'form-control currency exchange']) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::select("branch_code[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\BranchCode::getBranchCode() , old("branch_code[$i]", $branch_code[$i]), ['class' => 'form-control select2 ', ]) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::select("debit_object[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('DEBT') , old("debit_object[$i]", $debit_object[$i]), ['class' => 'form-control select2 ', ]) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::select("credit_object[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('CREDIT') , old("credit_object[$i]", $credit_object[$i]), ['class' => 'form-control select2 ', ]) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::select("cost_item[$i]", ['' => trans('system.dropdown_choice')] + \App\Models\CostItem::getActiveCostItem(session('current_company')) , old("cost_item[$i]", $cost_item[$i]), ['class' => 'form-control select2 ', ]) !!}
                            </td>
                        </tr>
                    @endfor
                @else
                    <tr>
                        {!! Form::hidden("accounting_id[]", '') !!}
                        <td style="text-align: center; vertical-align: middle;">
                            <a tabindex="-1" href="javascript:void(0);"
                               title="{!! trans('input_invoices.copy_current_row') !!}"
                               class="btn btn-xs btn-default clone-row-accounting">
                                <i class="fas fa-copy text-success"></i>
                            </a>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            {!! Form::text("desc_accounting[]", old("desc_accounting[]"), ['class' => 'form-control desc-accounting' ]) !!}
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            {!! Form::select("job_code[]", ['' => trans('system.dropdown_choice')] + \App\Models\Job::getJobsForOption(), old("job_code[]"), ['class' => 'form-control select2 job-code']) !!}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! Form::select("debit_account[]", ['' => trans('system.dropdown_choice')] + \App\Models\Account::getActiveLowestLevel(session('current_company')) , old("debit_account[]"), ['class' => 'form-control select2 debit-account', ]) !!}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! Form::select("credit_account[]", ['' => trans('system.dropdown_choice')] + \App\Models\Account::getActiveLowestLevel(session('current_company')) , old("credit_account[]"), ['class' => 'form-control select2 ', ]) !!}
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            {!! Form::text("amount[]", old("amount[]"), ['class' => 'form-control currency amount']) !!}
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            {!! Form::text("exchange[]", old("exchange[]"), ['class' => 'form-control currency exchange', 'readonly' => true]) !!}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! Form::select("branch_code[]", ['' => trans('system.dropdown_choice')] + \App\Models\BranchCode::getBranchCode() , old("branch_code[]"), ['class' => 'form-control select2 ', ]) !!}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! Form::select("debit_object[]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('DEBT'), old("debit_object[]"), ['class' => 'form-control select2 debit-object ', ]) !!}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! Form::select("credit_object[]", ['' => trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('CREDIT') , old("credit_object[]"), ['class' => 'form-control select2 ', ]) !!}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! Form::select("cost_item[]", ['' => trans('system.dropdown_choice')] + \App\Models\CostItem::getActiveCostItem(session('current_company')) , old("cost_item[]"), ['class' => 'form-control select2 ', ]) !!}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                        <a href="javascript:void(0);" class="add-accounting text-aqua" title="Add new row">
                            <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                        </a>
                    </td>
                    <td style="vertical-align: middle;"> -- </td>
                    <td style="vertical-align: middle;"> -- </td>
                    <td style="vertical-align: middle;"> -- </td>
                    <th style="vertical-align: middle; text-align: right">
                        {!! trans('input_invoices.total') !!}
                    </th>
                    <td style="white-space: nowrap;">
                        {!! Form::text('total_amount', old('total_amount', App\Helper\HString::currencyFormat($total_amount)), ['class' => 'form-control currency', 'id'=>"total-amount", 'readonly' => 'true']) !!}
                    </td>
                    <td style="white-space: nowrap;">
                        {!! Form::text('total_exchange', old('total_exchange', App\Helper\HString::currencyFormat($total_exchange)), ['class' => 'form-control currency', 'id'=>"total-exchange", 'readonly' => 'true']) !!}
                    </td>
                    <td style="vertical-align: middle;"> -- </td>
                    <td style="vertical-align: middle;"> -- </td>
                    <td style="vertical-align: middle;"> -- </td>
                    <td style="vertical-align: middle;"> -- </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    !function ($) {
        $(function () {
            $(document).on('click', '.add-accounting', function(event) {
                let newRow = '{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.accounting-invoices._row_accounting")->render()) !!}';
                $('table.accounting tr').last().before(newRow)
                callSelect2()
                callInputMaskDecimal()
                autoDesc()
                setTotal($('#total-amount'), $('#total-exchange'), $('input.amount'), '.exchange')
            })
            $(document).on("click", ".remove-accounting", function (event) {
                $(this).closest("tr").remove();
                setTotal($('#total-amount'), $('#total-exchange'), $('input.amount'), '.exchange')
            });
            $(document).on("keyup", "input.amount, input.exchange-rate", function() {
                setTotal($('#total-amount'), $('#total-exchange'), $('input.amount'), '.exchange')
            })
            setTotal($('#total-amount'), $('#total-exchange'), $('input.amount'), '.exchange')
            $(".debit-object").on("select2:select", function(e) {
                console.log(e.params.data.text);
            });
            $(document).on('click', '.clone-row-accounting', function(event) {
                let title = "{!! trans('input_invoices.remove_current_row') !!}";
                cloneRow($(this), 'remove-accounting', title)
                setTotal($('#total-amount'), $('#total-exchange'), $('input.amount'), '.exchange')
            });
        });
    }(window.jQuery);
</script>