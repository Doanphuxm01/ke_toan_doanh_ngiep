<tr>
    {!! Form::hidden("tax_id[]", '') !!}
    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
        <a tabindex="-1" href="javascript:void(0);"
           title="{!! trans('input_invoices.copy_current_row') !!}"
           class="btn btn-xs btn-default clone-row-tax">
            <i class="fas fa-copy text-success"></i>
        </a>
        <a tabindex="-1" href="javascript:void(0);" class="btn btn-xs btn-default remove-tax">
            <i class="text-success fa fa-minus"></i>
        </a>
    </td>
    <td>
        {!! Form::text("desc_tax[]", old('desc_tax[]'), ['class' => 'form-control']) !!}
    </td>
    <td>
        {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2 tax-rate']) !!}
    </td>
    <td>
        {!! Form::text("taxation[]", old('taxation[]'), ['class' => 'form-control currency taxation']) !!}
    </td>
    <td>
        {!! Form::text("exchange_tax[]", old('exchange_tax[]'), ['class' => 'form-control currency exchange-tax', 'readonly' => true]) !!}
    </td>
    <td>
        {!! Form::text("unit_price[]", old('unit_price[]'), ['class' => 'form-control currency unit-price']) !!}
    </td>
    <td>
        {!! Form::text("exchange_unit[]", old('exchange_unit[]'), ['class' => 'form-control currency exchange-unit', 'readonly' => true  ]) !!}
    </td>
    <td>
        {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + \App\Models\AccountingInvoice::getObjectForOption('CREDIT'), old('account_tax[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td>
        {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + \App\Models\GroupUnit::getGroupUnit(), old('group_unit[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td>
        {!! Form::text("invoice_no[]", old('invoice_no[]'), ['class' => 'form-control']) !!}
    </td>
    <td>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text("date_invoice[]",  old('date_invoice[]'), ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}
        </div>
    </td>
    <td>
        {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + \App\Models\Partner::getAllPartner(), old('object_code[]'), ['class' => 'form-control select2 object-code-tax']) !!}
    </td>
    <td>
        {!! Form::text("object_name_tax[]", old('object_name_tax[]'), ['class' => 'form-control object-name-tax', 'readonly']) !!}
    </td>
    <td>
        {!! Form::text("tax_code[]", old('tax_code[]'), ['class' => 'form-control tax-code', 'readonly']) !!}
    </td>
</tr>

