<!DOCTYPE html>
<html>
<head>
    <title>W2UI Demo: combo-9</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.4/xlsx.core.min.js"></script>
    <script src="{!! asset('assets/backend/js/FileSaver.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/jhxlsx.js') !!}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
</head>
<body>
<button class="export-me btn btn-info btn-block" data-jsonfile="single-sheet.json">Export</button>
<div id="main" style="width: 100%; height: 400px;"></div>
<br>
<label>
    <input type="checkbox" id="autoLoad" onclick="refreshGrid(this.checked)" checked>
    Auto Load
</label>

<script type="text/javascript">
// widget configuration
var config = {
    grid: {
        name: 'grid',
        url : '{{route('admin.demo.get-data')}}',
        show: {
            footer: true,
            toolbar: true,
            lineNumbers: true
        },
        limit: {{count($banks)}},
        columns: [
           
            { field: 'id', caption: 'ID', size: '200px', searchable: 'text' },
            { field: 'name', caption: 'Name', size: '200px', searchable: 'text' },
            
        ],
        onLoad: function(event) {
            let data = JSON.parse(event.xhr.responseText)
            data.records.forEach((rec, ind) => {
                rec.recid = 'recid-' + (this.records.length + ind)
            })
            event.xhr.responseText = data
        }
    }
};

function refreshGrid(auto) {
    w2ui.grid.autoLoad = auto;
    w2ui.grid.skip(0);
}

$(function () {
    $('#main').w2grid(config.grid);
});
</script>

<script>
            $(function () {
                $('.export-me').on('click', function () {
                    var url = '{{route('admin.demo.data-export')}}';
                    $.get(url, {}, function (data) {
                        Jhxlsx.export(data.tableData, data.options);
                    }).fail(function (jqXHR) {
                        alert("error: " + jqXHR.status + " / " + jqXHR.statusText);
                    });
                });
            });

            var tableData = [
                {
                    "sheetName": "Sheet1",
                    "data": [[{"text":"ID"},{"text":"Name"}]]
                }
            ];
            function save() {
                var options = {
                    fileName: "Export xlsx Sipmle Sheet"
                };
                Jhxlsx.export(tableData, options);
            }


        </script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
