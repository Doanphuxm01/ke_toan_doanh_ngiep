<table style="border-collapse: collapse;">
    <thead>
        <tr>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">No</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Date</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Type of Doc</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Document No</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Trans Description</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Beginning</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Year</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Cost</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Usefull life</th>
            <th style="background-color:lightblue;border: 1px solid #000;text-align:center;font-weight:bold">Monthly dep</th>
            <th rowspan="2" style="background-color:yellow;border: 1px solid #000;text-align:center;font-weight:bold">Increasing in FY 2017 <br> Tăng ST 2017</th>
        </tr>
        <tr>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">STT</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Ngày</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Loại CT</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Số CT</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Diễn giải</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Thời gian bắt đầu đưa vào sd</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Năm tính KH</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Nguyên giá</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Thời gian khấu hao</td>
            <td style="background-color:lightblue;border: 1px solid #000;text-align:center">Khấu hao tháng</td>      
        </tr> 
    </thead>
    <tbody>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center">I</td>
            <td colspan="2" style="border: 1px solid #000;font-weight:bold;font-size: 16px">CONTRUCTURE</td>
            <td style="border: 1px solid #000;"></td>
            <td style="border: 1px solid #000;;text-align:left"></td>
            <td style="border: 1px solid #000;;text-align:right"></td>
            <td style="border: 1px solid #000;;text-align:right"></td>
            <td style="border: 1px solid #000;;text-align:right"></td>
            <td style="border: 1px solid #000;;text-align:right"></td>
            <td style="border: 1px solid #000;;text-align:right"></td>
            <td style="border: 1px solid #000;;text-align:right"></td>
        </tr>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center;"></td>
            <td colspan="2" style="border: 1px solid #000">12/06/2002 B12-12</td>
            <td style="border: 1px solid #000;"></td>
            <td style="border: 1px solid #000;;text-align:left">NB office renovation</td>
            <td style="border: 1px solid #000;;text-align:right">02/12/2002</td>
            <td style="border: 1px solid #000;;text-align:right">2002</td>
            <td style="border: 1px solid #000;;text-align:right">$169,01</td>
            <td style="border: 1px solid #000;;text-align:right;font-style: italic;">60</td>
            <td style="border: 1px solid #000;;text-align:right">$2,82</td>
            <td style="border: 1px solid #000;;text-align:right"></td>
        </tr>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center;"></td>
            <td colspan="2" style="border: 1px solid #000">14/12/2002</td> 
            <td style="border: 1px solid #000;"></td>
            <td style="border: 1px solid #000;;text-align:left">NB office renovation 9</td>
            <td style="border: 1px solid #000;;text-align:right">02/12/2002</td>
            <td style="border: 1px solid #000;;text-align:right">2002</td>
            <td style="border: 1px solid #000;;text-align:right">$169,01</td>
            <td style="border: 1px solid #000;;text-align:right;font-style: italic;">60</td>
            <td style="border: 1px solid #000;;text-align:right">$2,82</td>
            <td style="border: 1px solid #000;;text-align:right"></td>
        </tr>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center;"></td>
            <td style="border: 1px solid #000">14/12/2002</td>
            <td style="border: 1px solid #000;text-align:left">35412</td>
            <td style="border: 1px solid #000;">CREPRC</td>
            <td style="border: 1px solid #000;;text-align:left">NB office renovation 5</td>
            <td style="border: 1px solid #000;;text-align:right">02/12/2002</td>
            <td style="border: 1px solid #000;;text-align:right">2002</td>
            <td style="border: 1px solid #000;;text-align:right">$169,01</td>
            <td style="border: 1px solid #000;;text-align:right;font-style: italic;">60</td>
            <td style="border: 1px solid #000;;text-align:right">$2,82</td>
            <td style="border: 1px solid #000;;text-align:right;background-color:yellow">21.132,12</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center;"></td>
            <td style="border: 1px solid #000">14/12/2002</td>
            <td style="border: 1px solid #000;text-align:left">4912</td>
            <td style="border: 1px solid #000;">CREPRC</td>
            <td style="border: 1px solid #000;;text-align:left">NB office renovation 1</td>
            <td style="border: 1px solid #000;;text-align:right">02/12/2002</td>
            <td style="border: 1px solid #000;;text-align:right">2002</td>
            <td style="border: 1px solid #000;;text-align:right">$169,01</td>
            <td style="border: 1px solid #000;;text-align:right;font-style: italic;">60</td>
            <td style="border: 1px solid #000;;text-align:right">$2,82</td>
            <td style="border: 1px solid #000;;text-align:right;background-color:yellow">1.541,32</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center;"></td>
            <td style="border: 1px solid #000">14/12/2002</td>
            <td style="border: 1px solid #000;text-align:left">2312</td>
            <td style="border: 1px solid #000;">CREPRC</td>
            <td style="border: 1px solid #000;;text-align:left">Renovation of Mr Matsubara's office in HD</td>
            <td style="border: 1px solid #000;;text-align:right">02/12/2002</td>
            <td style="border: 1px solid #000;;text-align:right">2002</td>
            <td style="border: 1px solid #000;;text-align:right">$169,01</td>
            <td style="border: 1px solid #000;;text-align:right;font-style: italic;">60</td>
            <td style="border: 1px solid #000;;text-align:right">$2,82</td>
            <td style="border: 1px solid #000;;text-align:right;background-color:yellow"></td>
        </tr>
        <tr>
            <td style="border: 1px solid #000;font-weight:bold;text-align:center;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;font-size:16px;font-weight:bold;text-align:center;background-color: #ffc299">Sub Total</td>
            <td style="border: 1px solid #000;text-align:right;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;text-align:right;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;text-align:right;font-weight:bold;background-color: #ffc299">$43.123,54</td>
            <td style="border: 1px solid #000;text-align:right;font-style: italic;background-color: #ffc299"></td>
            <td style="border: 1px solid #000;text-align:right;font-weight:bold;background-color: #ffc299">$721,90</td>
            <td style="border: 1px solid #000;text-align:right;font-weight:bold;background-color: #ffc299">$23.293,54</td>
        </tr>
    </tbody>
</table>
