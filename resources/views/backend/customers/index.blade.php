@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('customers.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('customers.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.customers.index') !!}">{!! trans('customers.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bconsumer">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.customers.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('fullname_email', trans('customers.fullname_email')) !!}
                        {!! Form::text('fullname_email', Request::input('fullname_email'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('level', trans('customers.level')) !!}
                        {!! Form::select('level', [-1 => trans('system.dropdown_all')] + App\Define\Customer::getLevelsForOption(), Request::input('level'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('page_num', trans('system.page_num')) !!}
                        {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <a href="{!! route('admin.customers.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp;{!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-md-10 text-right">
            {!! $customers->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($customers) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($customers->currentPage() - 1) * $customers->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $customers->count()) . ' ( ' . trans('system.total') . ' ' . $customers->total() . ' )' !!}
                    | <i>Chú giải: </i>&nbsp;&nbsp;
                    <span class="text-info"><i class="fa fa-key"></i>{!! trans('users.changepwd') !!}</span>&nbsp;&nbsp;
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i>{!! trans('system.action.update') !!}</span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i>{!! trans('system.action.delete') !!}</span>
                    <br/>
                    <small class="label bg-green">{!! trans('customers.coin_available') !!}</small>&nbsp;&nbsp;
                    <small class="label bg-red">{!! trans('customers.coin_holding') !!}</small>&nbsp;&nbsp;
                    <small class="label bg-yellow">{!! trans('customers.coin_threshold') !!}</small>
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customers.code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customers.fullname') !!}<br/>{!! trans('customers.phone') !!}</th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('customers.level') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customers.email') !!}<br/>{!! trans('customers.address') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('customers.last_login') !!}<br/>{!! trans('system.created_at') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $item)
                        <tr>
                            <td style="text-align: center; vertical-align: middle;">{!! $i++ !!}</td>
                            <td style="vertical-align: middle;">
                                <a href="{!! route('admin.customers.show', $item->id) !!}">{!! $item->code !!}</a><br/>
                                <small class="label bg-green">{!! App\Helper\HString::currencyFormat($item->coin_available) !!}đ</small>&nbsp;
                                <small class="label bg-red">{!! App\Helper\HString::currencyFormat($item->coin_holding) !!}đ</small>&nbsp;
                                <small class="label bg-yellow">{!! App\Helper\HString::currencyFormat($item->coin_threshold) !!}đ</small>
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->fullname !!}<br/>
                                {!! $item->phone !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                {!! App\Define\Customer::getColorLevels($item->level) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->email !!}<br/>
                                {!! $item->address !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                    <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                @elseif($item->status == 1)
                                    <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                    <span class="label label-default">{!! is_null($item->last_login) ? '-' : date('d/m/Y H:i', strtotime($item->last_login)) !!}</span><br/>
                                <span class="label label-default">{!! date('d/m/Y H:i', strtotime($item->created_at)) !!}</span>
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                <a href="{!! route('admin.customers.update_password', $item->id) !!}" class="btn btn-default btn-xs"><i class="text-info fa fa-key"></i> </a>
                                &nbsp;&nbsp;
                                <a href="{!! route('admin.customers.edit', $item->id) !!}" class="btn btn-xs btn-default"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.customers.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
            });
        }(window.jQuery);
    </script>
@stop
