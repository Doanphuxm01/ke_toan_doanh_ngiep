@extends('backend.master')
@section('title')
    {!! trans('customers.change_password') !!} - {!! trans('customers.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('customers.label') !!}
            <small>{!! trans('customers.change_password') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.customers.index') !!}">{!! trans('customers.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-body">
                @if($errors->count())
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
                        <ul>
                            @foreach($errors->all() as $message)
                            <li>{!! $message !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['role' => 'form', 'method' => 'PUT', 'url' => route('admin.customers.update_password_put', $customer->id)]) !!}
                    <table class='table'>
                        <tr>
                            <th class="table_right_middle">
                                {!! trans('customers.fullname') !!}
                            </th>
                            <td>
                                {!! $customer->fullname !!}
                            </td>
                            <th class="table_right_middle">
                                {!! trans('customers.email') !!}
                            </th>
                            <td>
                                {!! $customer->email !!}
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                {!! trans('customers.new_password') !!}
                            </th>
                            <td>
                                {!! Form::input('password', 'new_password', '', array('class' => 'form-control', 'maxlength' => 30, 'required')) !!}
                            </td>
                            <th class="text-right">
                                {!! trans('customers.re_password') !!}
                            </th>
                            <td>
                                {!! Form::input('password', 're_password', '', array('class' => 'form-control', 'maxlength' => 30, 'required')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                {!! HTML::link(route('admin.customers.index'), trans('system.action.cancel'), array('class' => 'btn btn-default btn-flat'))!!}
                                {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                            </td>
                        </tr>
                    </table>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@stop
