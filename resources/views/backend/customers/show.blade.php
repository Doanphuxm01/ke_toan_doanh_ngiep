@extends('backend.master')
@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('customers.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('customers.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($customer->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.customers.index') !!}">{!! trans('customers.label') !!}</a></li>
        </ol>
    </section>
    <table class='table borderless'>
        <tr>
            <th class="table_right_middle">
                {!! trans('customers.fullname') !!}
            </th>
            <td>
                {!! Form::text('fullname', old('fullname', $customer->fullname), ['class' => 'form-control', 'disabled']) !!}
            </td>
            <th class="table_right_middle">
                {!! trans('customers.code') !!}
            </th>
            <td>
                {!! Form::text('code', old('code', $customer->code), ['class' => 'form-control', 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('customers.phone') !!}
            </th>
            <td>
                {!! Form::text('phone', old('phone', $customer->phone), ['class' => 'form-control', 'disabled']) !!}
            </td>
            <th class="table_right_middle">
                {!! trans('customers.email') !!}
            </th>
            <td>
                {!! Form::text('email', old('email', $customer->email), ['class' => 'form-control', 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('customers.address') !!}
            </th>
            <td colspan="3">
                {!! Form::text('address', old('address', $customer->address), ['class' => 'form-control', 'disabled']) !!}
            </td>
        </tr>
    </table>
@stop
