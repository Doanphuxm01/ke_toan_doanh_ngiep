@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('customers.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('customers.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.customers.index') !!}">{!! trans('customers.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.customers.store'), 'role' => 'form']) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.code') !!}
                </th>
                <td>
                    {!! Form::text('code', old('code'), ['class' => 'form-control', 'maxlength' => 50, 'required', 'placeholder' => trans('customers.code_placeholder')]) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('customers.coin_threshold') !!}
                </th>
                <td>
                    <div class="input-group">
                        {!! Form::text('coin_threshold', old('coin_threshold'), ['class' => 'form-control', 'required']) !!}
                        <div class="input-group-addon">
                            VNĐ
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('customers.fullname') !!}
                </th>
            <td style="width: 35%;">
                    {!! Form::text('fullname', old('fullname'), ['class' => 'form-control', 'maxlength' => 100, 'required']) !!}
                </td>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('customers.phone') !!}
                </th>
                <td>
                    {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'maxlength' => 15, 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.email') !!}
                </th>
                <td>
                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'maxlength' => 100, 'required']) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('customers.level') !!}
                </th>
                <td>
                    {!! Form::select('level', ['' => trans('system.dropdown_choice')] + App\Define\Customer::getLevelsForOption(), old('tax_code'), ['class' => 'form-control select2', 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.password') !!}
                </th>
                <td>
                    {!! Form::password('password', ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('customers.re_password') !!}
                </th>
                <td>
                    {!! Form::password('re_password', ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.address') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-center table_right_middle1" colspan="4">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                        {!! trans('system.status.active') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    {!! HTML::link(route( 'admin.customers.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $("input[name='coin_threshold']").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'digits': 0, 'removeMaskOnSubmit': true});
              
            });
         
        }(window.jQuery);
       
    </script>
@stop