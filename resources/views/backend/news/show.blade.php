@extends('backend.master')

@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('news.label') !!}
@stop

@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
@stop

@section('content')
    <section class="content-header">
        <h1>
            {!! trans('news.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($news->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.news.index') !!}">{!! trans('news.label') !!}</a></li>
        </ol>
    </section>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <table class='table table-striped table-bordered'>
                        <tr>
                            <th class="text-right">
                                {!! trans('news.title') !!}
                            </th>
                            <td>
                                {!! $news->title !!}
                            </td>
                            <th class="text-right">
                                {!! trans('news.category') !!}
                            </th>
                            <td>
                                {!! Form::select('category', $categories, old('category', $news->category_id), ["class" => "form-control", 'disabled']) !!}
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right" style="width: 15%;">{!! trans("news.image") !!}</th>
                            <td style="width: 35%;">
                                <img src="{!! asset($news->image) !!}" width="250px">
                            </td>
                            <th class="text-right" style="width: 15%;">
                                {!! trans('news.summary') !!}
                            </th>
                            <td>
                                {!! $news->summary !!}
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                {!! trans('news.content') !!}
                            </th>
                            <td colspan="3">
                                {!! $news->content !!}
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                {!! trans('news.seo_keywords') !!}
                            </th>
                            <td colspan="3">
                                {!! $news->seo_keywords !!}
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                {!! trans('news.seo_description') !!}
                            </th>
                            <td colspan="3">
                                {!! $news->seo_description !!}
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                {!! trans('news.featured') !!}
                            </th>
                            <td>
                                {!! Form::checkbox('featured', 1, old('featured', $news->featured), [ 'disabled' ]) !!}
                            </td>
                            <th class="text-right">
                                {!! trans('system.status.active') !!}
                            </th>
                            <td>
                                {!! Form::checkbox('status', 1, old('status', $news->status), [ 'disabled' ]) !!}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop