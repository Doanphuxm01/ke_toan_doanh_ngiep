@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('payment_receipt_vouchers.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('payment_receipt_vouchers.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.payment-receipt-vouchers.index') !!}">{!! trans('payment_receipt_vouchers.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bconsumer">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.payment-receipt-vouchers.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('from_date', trans('payment_receipt_vouchers.from_date')) !!}
                        {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control from_date', 'placeholder' => 'MM/DD/YYYY']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('to_date', trans('payment_receipt_vouchers.to_date')) !!}
                        {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control to_date', 'placeholder' => 'MM/DD/YYYY']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('type', trans('payment_receipt_vouchers.type.label')) !!}
                        {!! Form::select('type', [ -1 => trans('system.dropdown_all')] + App\Defines\PaymentReceiptVoucher::getTypeValueForOption(), Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('object_name', trans('payment_receipt_vouchers.object_name')) !!}
                        {!! Form::text('object_name', Request::input('object_name'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </button>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $paymentReceiptVoucher->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div> 
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><b>You want create</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => route('admin.payment-receipt-vouchers.create'), 'method' => 'get','role' => 'form']) !!}
                {!! Form::checkbox('type', 1, old('type', 0), [ 'class' => 'minimal' ]) !!} {!! trans('payment_receipt_vouchers.label2') !!}
                &nbsp;
                {!! Form::checkbox('type', 0, old('type', 1), [ 'class' => 'minimal' ]) !!} {!! trans('payment_receipt_vouchers.label1') !!}
            </div>
            <div class="modal-footer">
                <button type="button"  data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-flat">{!! trans('system.action.cancel') !!}</button>
                {!! Form::submit(trans('system.action.confirm'), ['class' => 'btn btn-primary btn-flat']) !!}
                {!! Form::close() !!}
              <script>
                  $('input[type="checkbox"]').on('change', function() {
                     $(this).siblings('input[type="checkbox"]').not(this).prop('checked', false);
                  });
              </script>
            </div>
          </div>
        </div>
    </div>
    @if (count($paymentReceiptVoucher) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($paymentReceiptVoucher ->currentPage() - 1) *  $paymentReceiptVoucher ->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    <span id="counterSelected" class="badge">0</span>
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $paymentReceiptVoucher ->count()) . ' ( ' . trans('system.total') . ' ' .$paymentReceiptVoucher ->total() . ' )' !!}
                </div>
            </div>
            <div class="pull-right form-group col-md-41">
                <div style="display: inline-block;">
                    <select class="form-control select2" id="action" style="width: 25% !important;">
                        <option value="delete"> {{ trans('system.action.delete_all') }} </option>
                    </select>
                </div>
                <div style="display: inline-block;">
                    <button type="button" class="btn btn-info btn-flat" onclick="return save()">
                        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp; {!! trans('system.action.save') !!}
                    </button>
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! Form::checkbox('check_all', 1, 0, [  ]) !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_at') !!}</th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('payment_receipt_vouchers.voucher_no') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('payment_receipt_vouchers.description') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('payment_receipt_vouchers.with_amount') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('payment_receipt_vouchers.object_name') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('payment_receipt_vouchers.type.label') !!}</th>                        
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($paymentReceiptVoucher as $item)
                        <tr>
                            <td style="text-align: center; width: 3%; vertical-align: middle;">
                                {!! Form::checkbox('paymentReceiptVoucherId', $item->id, null, array('class' => 'paymentReceiptVoucherId')) !!}
                            </td>
                            <td style="vertical-align: middle; text-align:center">
                                {!!  date("d/m/Y", strtotime(str_replace('/', '-', $item->created_at))); !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->voucher_no !!}
                             </td>
                            <td style="vertical-align: middle; text-align:left">
                               {!! $item->desc !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! App\Helper\HString::currencyFormat($item->total_amount_accounting) !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! App\Models\Partner::getInfoPartner($item->partner_id)->name !!}
                            </td>
                            <td style="vertical-align: middle; text-align:center">
                               {!! App\Defines\PaymentReceiptVoucher::getColorTypeVouchers($item->type) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap; text-align:center">
                                @if($item->object_type == 0 || $item->object_type == NULL)
                                <a href="{!! route('admin.payment-receipt-vouchers.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                @elseif($item->object_type == 2)
                                <a href="{!! route('admin.payment-receipt-vouchers.edit-payment-vendor', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                @elseif($item->object_type == 3)
                                <a href="{!! route('admin.payment-receipt-vouchers.edit-receipt-payment-bank', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                @endif
                                <a href="javascript:void(0)" link="{!! route('admin.payment-receipt-vouchers.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script>
          var countChecked = function() {
            var length = $("input[name='paymentReceiptVoucherId']:checked" ).length;
            $("#counterSelected").text( length );
            if (length == 0) $("input[name='check_all']").attr('checked', false);
        };
        countChecked();
        $("input[type=checkbox][name='paymentReceiptVoucherId']").on( "click", countChecked );
        $("input[name='check_all']").change(function() {
            if($(this).is(':checked')) {
                $('.paymentReceiptVoucherId').each(function() {
                    this.checked = true;
                });
            } else {
                $('.paymentReceiptVoucherId').each(function() {
                    this.checked = false;
                });
            }
            countChecked();
        });
        function save() {
            if($( "input[name='paymentReceiptVoucherId']:checked" ).length == 0) {
                alert("{!! trans('system.no_item_selected') !!}");
                return false;
            }
            if($('#action').val() == 'noaction') {
                alert("{!! trans('system.no_action_selected') !!}");
                return false;
            }

            NProgress.start();

            var values = new Array();

            $.each($("input[name='paymentReceiptVoucherId']:checked"),
                function () {
                    values.push($(this).val());
                });

            $.ajax({
                url: "{!! route('admin.payment-receipt-vouchers.delete_all') !!}",
                data: { action: $('#action').val(), ids: JSON.stringify(values) },
                type: 'POST',
                datatype: 'json',
                headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                success: function(res) {
                    if(res.error)
                        alert(res.message);
                    else
                        window.location.reload(true);
                },
                error: function(obj, status, err) {
                    alert(err);
                }
            }).always(function() {
                NProgress.done();
            });

        }
    </script>
    <script>
        var from_date=$('.from_date'); 
        from_date.datepicker({
            format: 'dd/mm/yyyy',
            
            autoclose: true,
        })
    
        var to_date=$('.to_date'); 
        to_date.datepicker({
            format: 'dd/mm/yyyy',
            
            autoclose: true,
        })
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
