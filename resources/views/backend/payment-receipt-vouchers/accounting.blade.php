<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class='table table-bordered'>
                                <thead style="background: #3C80BC; color:white">
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('system.action.label') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans('accounting.description') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 180px; text-align: center;">
                                            <b>{!! trans('accounting.job_code') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.debit_account') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans("accounting.credit_account") !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.amount') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.exchange') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('accounting.branch_code') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('accounting.object_code') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('accounting.object_name') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('accounting.bank_account') !!}</b>
                                        </td>
                                    </tr>
                                </thead>
                                <?php $description = old('description', []); $totalAmount = 0;?>
                                @if(count($description))
                                    <?php
                                        $job_code = old('job_code');
                                        $debit_account = old('debit_account');
                                        $credit_account = old('credit_account');
                                        $amount = old('amount');
                                        $exchange = old('exchange');
                                        $branchCode = old('branch_code');
                                        $objectCode = old('object_code_accounting');
                                        $objectNameAccounting = old('object_name_accounting');
                                        $bankAccount = old('bank_account');
                                        $totalAmount = array_sum($amount);
                                        $totalexchange = array_sum($exchange);
                                    ?>
                                    @for ($i = 0; $i < count($description); $i++)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                @if ($i > 0)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>                                                &nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("description[]", old('description[]'), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("job_code[]", [' '=> trans('system.dropdown_choice')] + $jobs, old("job_code[$i]", $job_code[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("debit_account[]", [' '=> trans('system.dropdown_choice')] + $accounts , old("debit_account[$i]", $debit_account[$i] ), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("credit_account[]", [' '=> trans('system.dropdown_choice')] + $accounts, old("credit_account[$i]", $credit_account[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("amount[]", old("amount[]", $amount[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange[]", old("exchange[]", $exchange[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select("branch_code[]", [' '=> trans('system.dropdown_choice')] + $branchCodes, old("branch_code[$i]",  $branchCode[$i]), ['class' => 'form-control select2', 'data-container'=>'__"body"']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select("object_code_accounting[]", [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old("object_code_accounting[$i]", $objectCode[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::text("object_name_accounting[]", old("object_name_accounting[$i]", $objectNameAccounting[$i] ), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select("bank_account[]", [' '=> trans('system.dropdown_choice')], old("bank_account[$i]", $bankAccount[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                        </tr>
                                    @endfor
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-accounting btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            {!! Form::text('total_with_amount', old('total_with_amount', App\Helper\HString::decimalFormat($totalAmount)), ['class' => 'form-control currency', 'id' => 'total_with_amount']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right" >
                                            {!! Form::text('total_exchange', old('total_exchange', App\Helper\HString::decimalFormat($totalexchange)), ['class' => 'form-control currency', 'id' => 'total_exchange']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                            
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @elseif (count($paymentReceiptVoucher->accounting))
                                    @foreach ($paymentReceiptVoucher->accounting as $item)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;" id={{$i++}}>
                                                @if ($i > 1)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>                                                &nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('description[]', old('description[]', $item->description), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]', $item->job_code_id), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("debit_account[]", [' '=> trans('system.dropdown_choice')] + $accounts , old("debit_account[]", $item->debit_account ), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('credit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]', $item->credit_account), ['class' => 'form-control select2']) !!}                                            
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('amount[]', old('amount[]', $item->amount), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('exchange[]', old('exchange[]', $item->exchange), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]', $item->branch_code_id), ['class' => 'form-control select2']) !!} 
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                @if ($paymentReceiptVoucher->type == 1) 
                                                    {!! Form::select('object_code_accounting[]', [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old('object_code_tax[]',$item->debit_object_id), ['class' => 'form-control select2', 'style'=>"overflow-x: auto:"]) !!}                                  
                                                @elseif($paymentReceiptVoucher->type == 0)
                                                    {!! Form::select('object_code_accounting[]', [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old('object_code_tax[]',$item->credit_object_id), ['class' => 'form-control select2', 'style'=>"overflow-x: auto:"]) !!}                                  
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                @if ($paymentReceiptVoucher->type == 1) 
                                                    {!! Form::text('object_name_accounting[]', old('object_name[]', App\Models\Partner::getInfoPartner($item->debit_object_id)->name), ['class' => 'form-control']) !!}                                  
                                                @elseif($paymentReceiptVoucher->type == 0)
                                                    {!! Form::text('object_name_accounting[]', old('object_name[]', App\Models\Partner::getInfoPartner($item->credit_object_id)->name), ['class' => 'form-control']) !!}                                  
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select("bank_account[]", [' '=> trans('system.dropdown_choice')], old('bank_account[]', $item->bank_account), ['class' => 'form-control select2']) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-accounting btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                            {!! Form::text('total_with_amount', old('total_with_amount', $paymentReceiptVoucher->total_amount_accounting), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center" >
                                            {!! Form::text('total_exchange', old('total_exchange', $paymentReceiptVoucher->total_exchange_accounting), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        </td>

                                        <td style="white-space: nowrap;">

                                        </td>
                                        <td style="white-space: nowrap;">

                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <p style="vertical-align: middle; text-align: center;">
                                                <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-accounting text-success"><i class="fas fa-copy"></i></a>
                                            </p>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('description[]', old('description[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select('debit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts , old('debit_account[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select('credit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('amount[]', old('amount[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('exchange[]', old('exchange[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]'), ['class' => 'form-control select2']) !!} 
                                        </td>
                                        <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select('object_code_accounting[]', [' '=> trans('system.dropdown_choice')], old('object_code_tax[]'), ['class' => 'form-control select2', 'style'=>"overflow-x: auto:"]) !!}                                  
                                        </td>
                                        <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::text('object_name_accounting[]', old('object_name[]'), ['class' => 'form-control']) !!}                                  
                                        </td>
                                        <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select("bank_account[]", [' '=> trans('system.dropdown_choice')], old('bank_account[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-accounting btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            {!! Form::text('total_with_amount', old('total_with_amount', App\Helper\HString::decimalFormat($totalAmount)), ['class' => 'form-control currency', 'id' => 'total_with_amount']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right" >
                                            {!! Form::text('total_exchange', old('total_exchange', App\Helper\HString::decimalFormat($totalexchange)), ['class' => 'form-control currency', 'id' => 'total_exchange']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                            
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @endif
                                
                            </table>
                        </div >
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>
<script>
    !function ($) {
        $(function() {
            $(document).on('click', '.add-row-accounting', function(event) {
                $(this).closest('tr').prev().after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.payment-receipt-vouchers.row-accounting", compact("jobs", "branchCodes", "accounts", "partnerVendorCodes"))->render()) !!}');
                td_object_code = $(this).closest('tr').prev().find('select[name="object_code"]');
                td_object_code.val($('select[name="object"]').val()).change();
                select2Tab();
                enterTab();
                caculatorExchange();
                renderTableS2('select[name="object"]', "{!! route('admin.partners.get-all-partner') !!}", 'vendor-dropdown', s2FormatResultPartner, setInfoObjectOriginal);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
                renderTableS2('select[name="object_code_accounting[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, setInfoObjectAccounting);
                renderTableS2('select[name="bank_account[]"]', "{!! route('admin.banks.get-info-bank-account') !!}", 'custom-dropdown', s2FormatResultAccount);
                callInputMaskDecimal();
            });
           
            $(document).on('click', '.remove-row-accounting', function(event) {
                var length = $(this).closest('tbody').find('tr').length;
                if (length <= 3) {
                    $(this).closest('.box').fadeOut(300, function(){ $(this).remove();});
                } else {
                    $(this).closest('tr').fadeOut(150, function() { 
                        $(this).remove();
                        caculatorExchange()
                        callInputMaskDecimal();
                    });
                }
            });

            $(document).on('click', '.clone-row-accounting', function(event) {
                var tr = $(this).closest('tr');
                tr.find("select").select2('destroy');
                var tr1 = tr.clone();
                tr.find("select").each(function() {
                    var name = $(this).attr('name');
                    tr1.find("select[name='"+ name + "']").val($(this).select2().val());
                });
                            
                if (!tr1.find('a').hasClass('remove-row-accounting')) {
                    tr1.find('.clone-row-accounting').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>');
                }
                $(this).closest('tr').after(tr1);
                select2Tab();
                enterTab();
                renderTableS2('select[name="object"]', "{!! route('admin.partners.get-all-partner') !!}", 'vendor-dropdown', s2FormatResultPartner, setInfoObjectOriginal);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
                renderTableS2('select[name="object_code_accounting[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, setInfoObjectAccounting);
                renderTableS2('select[name="bank_account[]"]', "{!! route('admin.banks.get-info-bank-account') !!}", 'custom-dropdown', s2FormatResultAccount);
                caculatorExchange();
                callInputMaskDecimal();
            });
        });
    }(window.jQuery);


    function caculatorExchange() {
        var sum = 0;
        var total_exchange = 0;
        var exchange_rate = parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, ''));
        $('input[name="amount[]"]').each(function() {
            amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            sum += amount;
        });
        $("#total_with_amount").val(sum)
        $("input[name='exchange[]']").each(function() {
            var exchange =  (parseFloat($(this).val().replace(/,/g, '')) || 0);
            total_exchange += exchange;
            $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
        })
    }
</script>
