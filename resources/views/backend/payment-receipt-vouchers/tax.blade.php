<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class='table table-bordered'>
                                <thead style="background: #3C80BC; color:white">
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('system.action.label') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans('taxes.description') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 180px; text-align: center;">
                                            <b>{!! trans('taxes.tax_rate.label') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans("taxes.taxation") !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('taxes.exchange') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('taxes.unit_price') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('taxes.exchange') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('taxes.account_tax') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('taxes.group_unit') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('taxes.invoice_no') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('taxes.date_invoice') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('taxes.object_code') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align: center;">
                                            <b>{!! trans('taxes.object_name') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap;  text-align: center;">
                                            <b> {!! trans('taxes.tax_code') !!}</b>
                                        </td>
                                    </tr>
                                </thead>
                                <?php $description_tax = old('description_tax', []); $total_taxation = 0; $total_exchange_tax = 0; $total_service_value = 0; $total_exchange_service = 0;?>
                                @if(count($description_tax))
                                    <?php
                                    $tax_rate = old('tax_rate');
                                    $taxation = old('taxation');
                                    $exchange_tax = old('exchange_tax');
                                    $service_value = old('unit_price');
                                    $exchange_service = old('exchange_unit_price');
                                    $account_tax = old('account_tax');
                                    $invoice_no = old('invoice_no');
                                    $date_invoice = old('date_invoice');
                                    $group_unit = old('group_unit');
                                    $object_code_tax = old('object_code_tax');
                                    $object_name_tax = old('object_name_tax');
                                    $account_tax = old('account_tax');
                                    $tax_code = old('tax_code');
                                    $total_taxation = array_sum($taxation);
                                    $total_exchange_tax = array_sum($exchange_tax);
                                    $total_service_value = array_sum($service_value);
                                    $total_exchange_service = array_sum($exchange_service);
                                    ?>
                                    @for($i = 0; $i< count($description_tax); $i++)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                @if ($i > 0)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-tax"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-tax"><i class="fas fa-copy"></i></a>                                                &nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200x;">
                                                {!! Form::text("description_tax[]", old('description_tax[]', $description_tax[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("taxation[]", old('taxation[]', $taxation[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_tax[]", old('exchange_tax[]',  $exchange_tax[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("unit_price[]", old('unit_price[]', $service_value[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]', $exchange_service[$i]), ['class' => 'form-control currency'  ]) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]', $account_tax[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("group_unit[]",[' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]', $group_unit[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("invoice_no[]", old('invoice_no[]', $invoice_no[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;"">
                                                {!! Form::text("date_invoice[]",  old('date_invoice[]', $date_invoice[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                        
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old("object_code_tax[$i]", $object_code_tax[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("object_name_tax[]",  old("object_name[$i]", $object_name_tax[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("tax_code[]", old('tax_code[]', $tax_code[$i]), ['class' => 'form-control ']) !!}
                                            </td>
                                        </tr>

                                    @endfor
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-tax btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                            {!! Form::text("total_taxation", old('total_taxation', App\Helper\HString::decimalFormat($total_taxation)), ['class' => 'form-control currency', 'id'=>"total_taxation"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_tax", old('total_exchange_tax', App\Helper\HString::decimalFormat($total_exchange_tax)), ['class' => 'form-control currency', 'id'=>"total_exchange_tax"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_service_value", old('total_service_value', App\Helper\HString::decimalFormat($total_service_value)), ['class' => 'form-control currency', 'id'=>"total_service_value"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_service", old('total_exchange_service', App\Helper\HString::decimalFormat($total_exchange_service)), ['class' => 'form-control currency', 'id'=>"total_exchange_service"]) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <th style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>    
                                @elseif ($paymentReceiptVoucher->taxes)
                                    @foreach ($paymentReceiptVoucher->taxes as $item)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;" id={{$i++}}>
                                                @if ($i > 1)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-tax"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>                                                &nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("description_tax[]", old('description_tax[]', $item->description), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]', $item->tax_rate), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("taxation[]", old('taxation[]', $item->taxation), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap;min-width: 200px;">
                                                {!! Form::text("exchange_tax[]", old('exchange_tax[]', $item->exchange_tax), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("unit_price[]", old('unit_price[]', $item->unit_price), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]', $item->exchange_unit), ['class' => 'form-control currency'  ]) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]', $item->account_tax), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]', $item->group_unit), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("invoice_no[]", old('invoice_no[]', $item->invoice_no), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("date_invoice[]",  old('date_invoice[]',date("d/m/Y", strtotime(str_replace('-', '/', $item->invoice_date)))), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old('object_code[]', $item->partner_id), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("object_name_tax[]", old('object_name_tax[]', App\Models\Partner::getInfoPartner($item->partner_id)->name), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("tax_code[]", old('tax_code[]'), ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-tax btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                            {!! Form::text("total_taxation", old('total_taxation', App\Helper\HString::decimalFormat($paymentReceiptVoucher->total_taxation )), ['class' => 'form-control currency', 'id'=>"total_taxation"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_tax", old('total_exchange_tax', App\Helper\HString::decimalFormat($paymentReceiptVoucher->total_exchange_taxation)), ['class' => 'form-control currency', 'id'=>"total_exchange_tax"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_service_value", old('total_service_value', App\Helper\HString::decimalFormat($paymentReceiptVoucher->total_unit_price_tax)), ['class' => 'form-control currency', 'id'=>"total_service_value"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_service", old('total_exchange_service', App\Helper\HString::decimalFormat($paymentReceiptVoucher->total_exchange_unit_price_tax)), ['class' => 'form-control currency', 'id'=>"total_exchange_service"]) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <p style="vertical-align: middle; text-align: center;">
                                                <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-tax text-success"><i class="fas fa-copy"></i></a>
                                            </p>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("description_tax[]", old('description_tax[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("taxation[]", old('taxation[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap;min-width: 200px;">
                                            {!! Form::text("exchange_tax[]", old('exchange_tax[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("unit_price[]", old('unit_price[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]'), ['class' => 'form-control currency'  ]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("invoice_no[]", old('invoice_no[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("date_invoice[]",  old('date_invoice[]'), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old('object_code[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("object_name_tax[]", old('object_name_tax[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("tax_code[]", old('tax_code[]'), ['class' => 'form-control']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-tax btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                            {!! Form::text("total_taxation", old('total_taxation', App\Helper\HString::decimalFormat($total_taxation )), ['class' => 'form-control currency', 'id'=>"total_taxation"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_tax", old('total_exchange_tax', App\Helper\HString::decimalFormat($total_exchange_tax)), ['class' => 'form-control currency', 'id'=>"total_exchange_tax"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_service_value", old('total_service_value', App\Helper\HString::decimalFormat($total_service_value)), ['class' => 'form-control currency', 'id'=>"total_service_value"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_service", old('total_exchange_service', App\Helper\HString::decimalFormat($total_exchange_service)), ['class' => 'form-control currency', 'id'=>"total_exchange_service"]) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    !function ($) {
        $(function() {
            $(document).on('click', '.add-row-tax', function(event) {
                $(this).closest('tr').prev().after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.payment-receipt-vouchers.row-tax", compact("jobs", "branchCodes", "accounts", "partnerVendorCodes", "group_unit"))->render()) !!}');
                dateInvoice();
                select2Tab();
                enterTab();
                renderTableS2('select[name="object"]', "{!! route('admin.partners.get-all-partner') !!}", 'vendor-dropdown', s2FormatResultPartner, setInfoObjectOriginal);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
                renderTableS2('select[name="object_code_accounting[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, setInfoObjectAccounting);
                renderTableS2('select[name="bank_account[]"]', "{!! route('admin.banks.get-info-bank-account') !!}", 'custom-dropdown', s2FormatResultAccount);
                caculateRestartMoney("input[name='taxation[]']", "input[name='exchange_tax[]']", '#total_taxation', '#total_exchange_tax');
                caculateRestartMoney("input[name='unit_price[]']", "input[name='exchange_unit_price[]']", '#total_service_value', '#total_exchange_service');
                callInputMaskDecimal();
            });
       
            $(document).on('click', '.remove-row-tax', function(event) {
                var length = $(this).closest('tbody').find('tr').length;
                if (length <= 3) {
                    $(this).closest('.box').fadeOut(300, function(){ $(this).remove();});
                } else {
                    $(this).closest('tr').fadeOut(150, function(){ 
                        $(this).remove();
                        caculateRestartMoney("input[name='taxation[]']", "input[name='exchange_tax[]']", '#total_taxation', '#total_exchange_tax');
                        caculateRestartMoney("input[name='service_value[]']", "input[name='exchange_service[]']", '#total_service_value', '#total_exchange_service');
                        callInputMaskDecimal();
                        enterTab();
                    });
                }
            });

            $(document).on('click', '.clone-row-tax', function(event) {
                var tr = $(this).closest('tr');
                tr.find("select").select2('destroy');
                var tr1 = tr.clone();
                tr.find("select").each(function() {
                    var name = $(this).attr('name');
                    tr1.find("select[name='"+ name + "']").val($(this).select2().val());
                });
                console.log('asasdas');
                if (!tr1.find('a').hasClass('remove-row-tax')) {
                    tr1.find('.clone-row-tax').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>');
                }
                $(this).closest('tr').after(tr1);
                dateInvoice()
                select2Tab();
                enterTab();
                renderTableS2('select[name="object"]', "{!! route('admin.partners.get-all-partner') !!}", 'vendor-dropdown', s2FormatResultPartner, setInfoObjectOriginal);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
                renderTableS2('select[name="object_code_accounting[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, setInfoObjectAccounting);
                renderTableS2('select[name="bank_account[]"]', "{!! route('admin.banks.get-info-bank-account') !!}", 'custom-dropdown', s2FormatResultAccount);
                callInputMaskDecimal();
                caculateRestartMoney("input[name='taxation[]']", "input[name='exchange_tax[]']", '#total_taxation', '#total_exchange_tax');
                caculateRestartMoney("input[name='unit_price[]']", "input[name='exchange_unit_price[]']", '#total_service_value', '#total_exchange_service')
            });
            
        });
    }(window.jQuery);

    function caculateRestartMoney(col_1, col_2, total_1, total_2)
    {   
        var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')).toFixed(2) || 0);
        var total_col_1 = 0;
        var total_col_2= 0;
        $(col_1).each(function(){
            var value_col_1 = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            var tr = $(this).closest('tr').find(col_2);
            tr.val((value_col_1*exchange_rate)).inputmask();
            total_col_1 += value_col_1;
        })
        $(total_1).val(total_col_1.toFixed(2)).inputmask();
        $(col_2).each(function() {
            var value_col_2 =  (parseFloat($(this).val().replace(/,/g, '')) || 0);
            total_col_2 += value_col_2;
        })
        $(total_2).val(total_col_2.toFixed(2)).inputmask();
    }  

    function dateInvoice(){
        $('input[name="date_invoice[]"]').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            autoclose: true,
        })
    }
 
</script>   
