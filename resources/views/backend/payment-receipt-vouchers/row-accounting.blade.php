<tr>
    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
        <p style="vertical-align: middle; text-align: center;">
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>
            &nbsp;&nbsp;
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>
        </p>
    </td>
    <td style="white-space: nowrap; min-width: 120px;">
        {!! Form::text('description[]', old('description[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::select('debit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('debit_account[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::select('credit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text('amount[]', old('amount[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text('exchange[]', old('exchange[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]'), ['class' => 'form-control select2']) !!}
    </td>   
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::select('object_code[]', [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old('object_code[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::text('object_name[]', old('object_name[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 150px;">
        {!! Form::select('bank_account[]', [' '=> trans('system.dropdown_choice')], old('bank_account[]'), ['class' => 'form-control select2']) !!}
    </td>
</tr>

