
@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!}-{!! App\Defines\PaymentReceiptVoucher::getTypeVouchers($type) !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/css/table.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}"/>
@section('content')
    <section class="content-header">
        <h1>
            {!! App\Defines\PaymentReceiptVoucher::getTypeVouchers($type) !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            {{-- <li><a href="{!! route('admin.payment-receipt-vourchers.index') !!}">{!! trans('payment-vourchers.label') !!}</a></li> --}}
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.payment-receipt-vouchers.store-payment-vendor'), 'method'=>'POST','role' => 'form']) !!}
            {!! Form::hidden('type', 1) !!}
            {!! Form::hidden('object_type', 2) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-1">
                                           <b>{!! trans('payment_receipt_vouchers.object') !!}</b> 
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::hidden('object_id', $data_start['idVendor']) !!}
                                            {!! Form::select('object', [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old('object', $data_start['idVendor']), ['class' => 'form-control select2', 'required', 'disabled']) !!}
                                        </div>                                       
                                        <div class="col-md-5">
                                            {!! Form::text('object_name', old('object_name', $data_start['vendorName']), ['class' => 'form-control', 'maxlength' => 20, 'required', 'readonly']) !!}                                    
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.voucher_date') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::text('date_vouchers', old('date_vouchers', date('d/m/Y')), ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'required', 'readonly']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-1">
                                          <b>{!! trans('payment_receipt_vouchers.beneficiary') !!}</b> 
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('beneficiary', old('beneficiary'), ['class' => 'form-control ']) !!}
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.voucher_no') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('voucher_no', old('voucher_no', $trx_no), ['class' => 'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('payment_receipt_vouchers.address') !!}</b>
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.currency') !!}</b>
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::hidden('currency_id', $data_start['currency']) !!}
                                            {!! Form::select('currency', [' '=> trans('system.dropdown_choice')] + $currency, old('currency', $data_start['currency']), ['class' => 'form-control select2', 'required', 'disabled']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.exchange_rate') !!}</b>
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::text('exchange_rate',  old('exchange_rate', $data_start['exchange_rate']), ['class' => 'form-control currency', 'readonly']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('payment_receipt_vouchers.details') !!}</b>
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('details', old('details'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><b>{!! trans('accounting.label') !!}</b></a></li>
                  <li><a href="#tab_2" data-toggle="tab"><b>{!! trans('dept_voucher.label') !!}</b></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    @include('backend.payment-receipt-vouchers._accounting')
                  </div>
                  <div class="tab-pane" id="tab_2">
                    @include('backend.payment-receipt-vouchers.dept-voucher-check')
                  </div>
                </div>
            </div>   
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <table class='table borderless'>
                                <tr>
                                    <td style="text-align:center" colspan="2">
                                        {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                                        {!! trans('system.status.active') !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        {!! HTML::link(route( 'admin.payment-receipt-vouchers.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                                        {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/fix-tabindex.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/call-plugins.js') !!}"></script>
    <script>
        $('.select2').select2({
            width: '100%'
        });
    </script>
    <script>
         callICheck();
    </script>
    <script>
        $(function() {
            $('.date').datepicker({
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true,
            });

            $(document).ready(function(){
                 var date_input=$('input[name="date_invoice[]"]'); 
                date_input.datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    autoclose: true,
                })
            })
        });
        callInputMaskDecimal()
    </script>
    <script>
        $(document).ready(function () {
            $('select[name="object"]').attr('readonly', true);
            $('select[name="object"]').change(function (e) { 
                var id =  $(this).val();
                $('select[name="object_code[]"]').val(id).change();
                $.ajax({
                    type: "Get",
                    url: "{!! route('admin.payment-receipt-vouchers.get-info-object') !!}",
                    data: {id:id},
                    dataType: "json",
                    success: function (response) {
                        $('input[name="object_name"]').val(response.data.name);
                        $('input[name="beneficiary"]').val(response.data.name);
                    },
                    error:function (response) {
                        toastr.error(response.message);
                    }
                });
            });
            getInfoObject('select[name="object_code[]"]', 'input[name="object_name[]"]');
            getInfoObject('select[name="object_code_tax[]"]', 'input[name="object_name_tax[]"]') 
        });

        function getInfoObject(object_code, object_name) 
        {   
             $(document).on('change',object_code, function(){
                var id =  $(this).val()
                var tr = $(this).closest('tr');
                tr = tr.find(object_name);
                tr_tax_code = tr.closest('tr').find('input[name="tax_code[]"]');
                $.ajax({
                    type: "Get",
                    url: "{!! route('admin.payment-receipt-vouchers.get-info-object') !!}",
                    data: {id:id},
                    dataType: "json",
                    success: function (response) {
                        tr.val(response.data.name);
                        tr_tax_code.val(response.data.tax_code);
                    },
                    error:function (response) {
                        toastr.error(response.message);
                    }
                });
            })
        }
        $('input[name="details"]').keyup(function (e) { 
            var details = $('input[name="details"]').val();
            $('input[name="description[]"]').val(details);
            
        });
    </script>
    <script>
        !function ($) {
            $(function() {
                enterTab();
                select2Tab();
            });
        }(window.jQuery);
    </script>
  
   
@stop