@extends('backend.master')
@section('title')
    {{-- {!! trans('system.action.create') !!}-{!! App\Defines\PaymentReceiptVoucher::getTypeVouchers($type) !!} --}}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/css/table.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}"/>
@section('content')
    <section class="content-header">
        <h1>
            {{-- {!! App\Defines\PaymentReceiptVoucher::getTypeVouchers($type) !!} --}}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            {{-- <li><a href="{!! route('admin.payment-receipt-vourchers.index') !!}">{!! trans('payment-vourchers.label') !!}</a></li> --}}
        </ol>
    </section>

    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.payment-receipt-vouchers.create-payment-vendor-step2'), 'method'=> 'GET']) !!}
        {!! Form::hidden('type', 1) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-1 text-right vcenter">
                                           <b>{!! trans('payment_receipt_vouchers.vendor') !!}</b> 
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::select ('vendor', [' '=> trans('system.dropdown_choice')] + $vendor, old('vendor'), ['class' => 'form-control select2', 'required']) !!} 
                                        </div>                                       
                                        <div class="col-md-8">
                                            {!! Form::text('vendor_name', old('vendor_name'), ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}                                    
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.date_payment') !!}</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::text('date_payment', old('date_payment', date('d/m/Y')), ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                          <b>{!! trans('payment_receipt_vouchers.with_amount') !!}</b> 
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::text('with_amount', old('with_amount'), ['class' => 'form-control currency']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.currency') !!}</b>
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::select('currency', [' '=> trans('system.dropdown_choice')] + $currency, old('currency'), ['class' => 'form-control select2', 'required']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.exchange_rate') !!}</b>
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::text('exchange_rate',  old('exchange_rate'), ['class' => 'form-control currency', 'required']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-10">
                                        </div>
                                        <div class="col-md-2 text-right vcenter">
                                            <button type='button' name="search" class="btn btn-primary"> <span class="glyphicon glyphicon-search"></span>&nbsp;search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><b>{!! trans('dept_voucher.label') !!}</b></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active table-responsive" id="tab_1">
                    <table class="table table-bordered plane">
                        
                    </table>
                  </div>
                </div>
            </div>   
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <table class='table borderless'>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        {!! HTML::link(route( 'admin.payment-receipt-vouchers.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                                        {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </section>
        @include('backend.payment-receipt-vouchers.vendor.modal_voucher');
@stop
@section('footer')
    <style>
         .vendor-dropdown {
            width: 800px !important;
        }
    </style>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/fix-tabindex.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/call-plugins.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/select2-table-partner.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/payment-vendor.js') !!}"></script>
    <script>
         callICheck();
         callInputMaskDecimal()
    </script>
    <script>
        function formatNumber (num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
         }
        $("button[name='search']").click(function (e) { 
                var id =  $('select[name="vendor"]').val();
                var exchange_rate = Number($('input[name="exchange_rate"]').val().replace(/,/g, ""));
                $.ajax({
                    type: "post",
                    url: "{!! route('admin.payment-receipt-vouchers.get-voucher') !!}",
                    data: {id:id, exchange_rate:exchange_rate},
                    dataType: "json",
                    headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                    success: function (response) {
                        $('#voucher').modal().show();
                        $('.voucher').html(response.data.html);
                        check_id = $('table.plane').find('input[name="id[]"]');
                        check_id.each(function () {
                            invoice_no_plane      = $(this).closest('tr').find('input[name="invoice_no[]"]').val();
                            invoice_date_plane    = $(this).closest('tr').find('input[name="invoice_date[]"]').val();
                            account_payment_plane = $(this).closest('tr').find('input[name="account_payment[]"]').val();
                            check_id_modal = $('#table-dept').find('input[name="id[]"]');
                            check_id_modal.each(function(){
                                invoice_no_modal       = $(this).closest('tr').find('input[name="invoice_no[]"]').val();
                                invoice_date_modal     = $(this).closest('tr').find('input[name="invoice_date[]"]').val();
                                account_payment_modal  = $(this).closest('tr').find('input[name="account_payment[]"]').val();
                                if(invoice_no_plane == invoice_no_modal && invoice_date_plane ==invoice_date_modal && account_payment_plane == account_payment_modal)
                                {
                                    $(this).closest('tr').find('input[name="checkboxindex[]"]').iCheck('check');  
                                }
                            })
                        })
                    },
                    error: function(obj, status, err) {
                        var error = $.parseJSON(obj.responseText);
                        toastr.error(error.message, '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }
                });
                
                $('#btn_ok').click(function(e){
                    var html  = '<thead style="background: #3C80BC; color:white">'
                        html +=    '<td style="text-align: center; white-space: nowrap; min-width: 120px;">'
                        html +=    '    <b>{!! trans('dept_voucher.voucher_date') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap; min-width: 180px;">'
                        html +=    '    <b>{!! trans('dept_voucher.voucher_no') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap; min-width: 90px;">'
                        html +=    '    <b>{!! trans('dept_voucher.invoice_date') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap; min-width: 120px;">'
                        html +=    '    <b>{!! trans("dept_voucher.invoice_no") !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap; min-width: 90px;">'
                        html +=    '    <b>{!! trans('dept_voucher.explain') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap; min-width: 90px;">'
                        html +=    '    <b>{!! trans('dept_voucher.time_for_payment') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap;">'
                        html +=    '    <b>{!! trans('dept_voucher.amount') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap;">'
                        html +=    '    <b>{!! trans('dept_voucher.exchange') !!}</b> '
                        html +=    '</td>'
                        html +=    '<td style="text-align: center; white-space: nowrap;">'
                        html +=    '    <b>{!! trans('dept_voucher.account_payment') !!}</b>'
                        html +=    '</td>'
                        html +='</thead>';
                    var total_amount = 0;
                    var total_exchange = 0;
                          e.preventDefault();
                    $('input:checkbox[name="checkboxindex[]"]').each(function () {
                        if($(this).is(':checked')){
                            id = $(this).closest('tr').find('input[name="id[]"]').val();
                            voucher_date = $(this).closest('tr').find('input[name="day_voucher[]"]').val();
                            voucher_no = $(this).closest('tr').find('input[name="trx_no[]"]').val();
                            invoice_date = $(this).closest('tr').find('input[name="invoice_date[]"]').val();
                            invoice_no = $(this).closest('tr').find('input[name="invoice_no[]"]').val();
                            explain = $(this).closest('tr').find('input[name="explain[]"]').val();
                            due_date = $(this).closest('tr').find('input[name="due_date[]"]').val();
                            amount = (parseFloat($(this).closest('tr').find('input[name="amount[]"]').val().replace(/,/g, '')) || 0);
                            exchange =  (parseFloat($(this).closest('tr').find('input[name="exchange[]"]').val().replace(/,/g, '')) || 0);$(this).closest('tr').find('input[name="exchange[]"]').val();
                            account_payment = $(this).closest('tr').find('input[name="account_payment[]"]').val();
                            total_amount += Number(amount);
                            total_exchange += Number(exchange);
                                html += '<tr>'
                                html += '<td>'
                                html += '<input name="id[]" class="form-control" type="hidden" value="'+id+'">'
                                html += '<input name="voucher_date[]" class="form-control" type="text" value="'+voucher_date+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="voucher_no[]" class="form-control" type="text" value="'+voucher_no+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="invoice_date[]" class="form-control" type="text" value="'+invoice_date+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="invoice_no[]" class="form-control" type="text" value="'+invoice_no+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="explain[]" class="form-control" type="text" value="'+explain+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="due_date[]" class="form-control" type="text" value="'+due_date+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="amount[]" class="form-control currency" type="text" value="'+amount+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="exchange[]" class="form-control currency" type="text" value="'+exchange+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="account_payment[]" class="form-control" type="text" value="'+account_payment+'">'
                                html += '</td>'
                                html += '</tr>'
                        }
                    })
                                html += '<tr>'
                                html += '<td>'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '{!! trans('input_invoices.total') !!}'
                                html += '<td>'
                                html += '<input name="total_amount" id="total_amount" class="form-control currency" type="text" value="'+total_amount+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '<input name="total_exchange" id="total_exchange" class="form-control currency" type="text" value="'+total_exchange+'">'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '<td>'
                                html += '</td>'
                                html += '</tr>'
                                $('input[name="with_amount"]').val(total_amount);
                    $('table.plane').empty().append(html);
                    callInputMaskDecimal()
                    $('#voucher').hide();
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                });
                    
         
        });
        
    </script>
    <script>
        !function ($) {
            $(function() {
                enterTab();
                select2Tab();
                renderTableS2('select[name="vendor"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllVendor);
            });
        }(window.jQuery);
    </script>
@stop