<div id="voucher" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{ trans('system.action.create') }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class='voucher'>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id='btn_ok' class="btn btn-danger">OK</button>
                     </div>
                </div>
            </div>
        </div>
        <style>
            .select2 {
                display: block;
            }
            .vcenter {
                /*display: inline-block;*/
                vertical-align: middle;
                /*float: none;*/
            }
            
            .select2-container {
                width: 100% !important;
                padding: 0;
            }
            .container {
                margin-top: 15px;
            }
            button, .btn {
                outline: none !important;
            }
            .no-gutters {
                margin-right: 0;
                margin-left: 0;
            }
            .container label {
                margin-top: 5px;
            }
            .box {
                border-top: none!important;
            }
            table {
                font-size: 13px;
            }
            table tbody tr td {
                vertical-align: middle;
                padding: 0 !important;
            }
            .voucher_modal tr td {
                vertical-align: middle;
                padding: 10px !important;
            }
            .voucher tr td {
                vertical-align: middle;
                padding: 10px !important;
            }
        </style>