<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class='table table-bordered'>
                                <thead style="background: #3C80BC; color:white">
                                    <tr>
                                        @if($action == 1)
                                            <td style="white-space: nowrap; min-width: 120px; text-align:center">
                                                <b>{!! trans('system.action.label') !!}</b>
                                            </td>
                                        @endif
                                        <td style="white-space: nowrap; min-width: 120px; text-align:center">
                                            <b>{!! trans('accounting.description') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 180px; text-align:center">
                                            <b>{!! trans('accounting.job_code') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                            <b>{!! trans('accounting.debit_account') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px; text-align:center">
                                            <b>{!! trans("accounting.credit_account") !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                            <b>{!! trans('accounting.amount') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                            <b>{!! trans('accounting.exchange') !!}</b>
                                        </td>
                                        <td style="white-space: nowrap; text-align:center">
                                            <b>{!! trans('accounting.branch_code') !!}</b>
                                        </td>
                                    </tr>
                                </thead>
                                <?php $description = old('description', []); $totalAmount = 0;?>
                                @if(count($description))
                                    <?php
                                        $job_code = old('job_code');
                                        $debit_account = old('debit_account');
                                        $credit_account = old('credit_account');
                                        $amount = old('amount');
                                        $exchange = old('exchange');
                                        $branchCode = old('branch_code');
                                        $totalAmount = array_sum($amount);
                                        $totalexchange = array_sum($exchange);
                                    ?>
                                    @for ($i = 0; $i < count($description); $i++)
                                        <tr>
                                            @if($action == 1)
                                                <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-accounting text-success"><i class="fas fa-copy"></i></a>
                                                    </p>
                                                </td>
                                            @endif
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("description[]", old('description[]'), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("job_code[]", [' '=> trans('system.dropdown_choice')] + $jobs, old("job_code[$i]", $job_code[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("debit_account[]",  old("debit_account[]", $debit_account[$i] ), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("credit_account[]", [' '=> trans('system.dropdown_choice')] + $accounts, old("credit_account[$i]", $credit_account[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("amount_accounting[]", old("amount_accounting[]", $amount[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_accounting[]", old("exchange_accounting[]", $exchange[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select("branch_code[]", [' '=> trans('system.dropdown_choice')] + $branchCodes, old("branch_code[$i]",  $branchCode[$i]), ['class' => 'form-control select2', 'data-container'=>'__"body"']) !!}
                                            </td>
                                        </tr>
                                    @endfor
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                            {!! trans('input_invoices.total') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            {!! Form::text('total_amount_accounting', old('total_amount_accounting', App\Helper\HString::decimalFormat($totalAmount)), ['class' => 'form-control currency', 'id' => 'total_amount_accounting']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right" >
                                            {!! Form::text('total_exchange_accounting', old('total_exchange_accounting', App\Helper\HString::decimalFormat($totalexchange)), ['class' => 'form-control currency', 'id' => 'total_exchange_accounting']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @elseif (count($newAccounting))
                                    <?php $total_amount =0; $total_exchange = 0;?>
                                    @foreach ($newAccounting as $item)
                                        <?php $total_amount   += $item['amount'];
                                              $total_exchange += $item['exchange'];
                                        ?>
                                        <tr>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('description[]', old('description[]'), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]', $item->job_code_id), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("debit_account[]",  old("debit_account[]", $item['account_payment'] ), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('credit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]', $item->credit_account), ['class' => 'form-control select2']) !!}                                            
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('amount_accounting[]', old('amount_accounting[]', $item['amount']), ['class' => 'form-control currency', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('exchange_accounting[]', old('exchange_accounting[]', $item['exchange']), ['class' => 'form-control currency', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]', $item->branch_code_id), ['class' => 'form-control select2']) !!} 
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            <b>{!! trans('input_invoices.total') !!}</b>&nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                            {!! Form::text('total_amount_accounting', old('total_amount_accounting', $total_amount), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center" >
                                            {!! Form::text('total_exchange_accounting', old('total_exchange_accounting', $total_exchange), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        </td>
                                    </tr>
                                @elseif(count($paymentReceiptVoucher))
                                    @foreach ($paymentReceiptVoucher->accounting as $item)
                                        <tr>
                                            @if($action == 1)
                                                <td style="white-space: nowrap; min-width: 120px; text-align:center">
                                                </td>
                                            @endif
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::hidden('id_accounting[]', $item->id) !!}
                                                {!! Form::text('description[]', old('description[]', $item->description), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]', $item->job_code_id), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("debit_account[]",  old("debit_account[]", $item->debit_account ), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('credit_account[]', old('credit_account[]', $item->credit_account), ['class' => 'form-control', 'readonly']) !!}                                            
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('amount_accounting[]', old('amount_accounting[]', $item->amount), ['class' => 'form-control currency', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('exchange_accounting[]', old('exchange_accounting[]', $item->exchange), ['class' => 'form-control currency', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]', $item->branch_code_id), ['class' => 'form-control select2']) !!} 
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            <b>{!! trans('input_invoices.total') !!}</b>&nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                            {!! Form::text('total_amount_accounting', old('total_amount_accounting', $paymentReceiptVoucher->total_amount_accounting), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center" >
                                            {!! Form::text('total_exchange_accounting', old('total_exchange_accounting', $paymentReceiptVoucher->total_exchange_accounting), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        </td>
                                    </tr>
                                @else
                                <tr>
                                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                        <p style="vertical-align: middle; text-align: center;">
                                            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-accounting text-success"><i class="fas fa-copy"></i></a>
                                        </p>
                                    </td>
                                    <td style="white-space: nowrap; min-width: 200px;">
                                        {!! Form::hidden('id_accounting[]', $item->id) !!}
                                        {!! Form::text('description[]', old('description[]'), ['class' => 'form-control']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 200px;">
                                        {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]', $item->job_code_id), ['class' => 'form-control select2']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 200px;">
                                        {!! Form::select("debit_account[]",  [' '=> trans('system.dropdown_choice')] + $accounts, old("debit_account[]" ), ['class' => 'form-control select2']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 200px;">
                                        {!! Form::select('credit_account[]',  [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]'), ['class' => 'form-control select2']) !!}                                            
                                    </td>
                                    <td style="white-space: nowrap; min-width: 200px;">
                                        {!! Form::text('amount_accounting[]', old('amount_accounting[]'), ['class' => 'form-control currency']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 200px;">
                                        {!! Form::text('exchange_accounting[]', old('exchange_accounting[]'), ['class' => 'form-control currency']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]'), ['class' => 'form-control select2']) !!} 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                    </td>
                                    <td style="white-space: nowrap; min-width: 120px;">
                                    
                                    </td>
                                    <td>

                                    </td>
                                    <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                        <b>{!! trans('input_invoices.total') !!}</b>&nbsp;&nbsp;
                                    </td>
                                    <td style="white-space: nowrap; min-width: 90px; text-align:center">
                                        {!! Form::text('total_amount_accounting', old('total_amount_accounting'), ['class' => 'form-control currency', 'id'=>"total_amount_accounting", 'readonly' => 'true']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 90px; text-align:center" >
                                        {!! Form::text('total_exchange_accounting', old('total_exchange_accounting'), ['class' => 'form-control currency', 'id'=>"total_exchange_accounting", 'readonly' => 'true']) !!}
                                    </td>
                                    <td style="white-space: nowrap;">
                                    </td>
                                </tr>
                                @endif

                            </table>
                        </div >
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>
<script>
     !function ($) {
        $(function() {
            $(document).on('click', '.remove-row-accounting', function(event) {
                var length = $(this).closest('tbody').find('tr').length;
                if (length <= 3) {
                    $(this).closest('.box').fadeOut(300, function(){ $(this).remove();});
                } else {
                    $(this).closest('tr').fadeOut(150, function() { 
                        $(this).remove();
                        caculatorExchange()
                        $(".currency").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'max':99999999999999999999.99, 'digits':2, 'removeMaskOnSubmit': true});
                    });
                }
            });

            $(document).on('click', '.clone-row-accounting', function(event) {
                var tr = $(this).closest('tr');
                tr.find("select").select2('destroy');
                var tr1 = tr.clone();
                tr.find("select").each(function() {
                    var name = $(this).attr('name');
                    tr1.find("select[name='"+ name + "']").val($(this).select2().val());
                });
                            
                if (!tr1.find('a').hasClass('remove-row-accounting')) {
                    tr1.find('.clone-row-accounting').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>');
                }
                $(this).closest('tr').after(tr1);
                select2Tab();
                enterTab();
                renderTableS2('select[name="object"]', "{!! route('admin.partners.get-all-partner') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllPartner);
                renderTableS2('select[name="payment_account"]', "{!! route('admin.banks.get-info-bank-account') !!}", 'payment-dropdown', s2FormatResultAccount, getInfoAccount);
                caculatorExchange();
                $(".currency").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'max':99999999999999999999.99, 'digits':2, 'removeMaskOnSubmit': true});
            });
        });
    }(window.jQuery);

    function caculatorExchange() {
        var sum = 0;
        var total_exchange = 0;
        var exchange_rate = parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, ''));
        $('input[name="amount_accounting[]"]').each(function() {
            amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            sum += amount;
        });
        $("#total_amount_accounting").val(sum)
        $("input[name='exchange_accounting[]']").each(function() {
            var exchange =  (parseFloat($(this).val().replace(/,/g, '')) || 0);
            total_exchange += exchange;
            $("#total_exchange_accounting").val(total_exchange.toFixed(2)).inputmask();
        })
    }
</script>
