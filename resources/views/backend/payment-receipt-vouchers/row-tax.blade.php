<tr>
    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
        <p style="vertical-align: middle; text-align: center;">
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-tax text-success"><i class="fas fa-copy"></i></a>
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>
        </p>
    </td>
    <td style="white-space: nowrap; min-width: 120px;">
        {!! Form::text('description_tax[]', old('description_tax[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 180px;">
        {!! Form::select('tax_rate[]', [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 120px;">
        {!! Form::text('taxation[]', old('taxation[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 90px;">
        {!! Form::text('exchange_tax[]', old('exchange_tax[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("unit_price[]", old('unit_price[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]'), ['class' => 'form-control currency'  ]) !!}
    </td>
    <td style="white-space: nowrap; min-width: 90px;">
        {!! Form::select('account_tax[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 90px;">
        {!! Form::text('group_unit[]', old('group_unit[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::text('invoice_no[]', old('invoice_no[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 120px;">
        {!! Form::text('date_invoice[]',  old('date_invoice[]'), ['class' => 'form-control' , 'placeholder' => 'MM/DD/YYYY']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::text('object_code_tax[]', old('object_code[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::text('object_name_tax[]', old('object_name_tax[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::text('tax_code[]', old('tax_code[]'), ['class' => 'form-control']) !!}
    </td>
</tr>
<tr>
  
</tr>