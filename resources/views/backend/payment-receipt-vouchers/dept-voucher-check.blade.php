<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class='table table-bordered' id='table-dept'>
                               
                                @if(count($paymentReceiptVoucher->dept_vouchers))
                                    <thead style="background: #3C80BC; color:white">
                                        <td style="text-align: center; white-space: nowrap; min-width: 120px;">
                                            <b>{!! trans('dept_voucher.voucher_date') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap; min-width: 180px;">
                                            <b>{!! trans('dept_voucher.voucher_no') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                            <b>{!! trans('dept_voucher.invoice_date') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap; min-width: 120px;">
                                            <b>{!! trans("dept_voucher.invoice_no") !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                            <b>{!! trans('dept_voucher.explain') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                            <b>{!! trans('dept_voucher.time_for_payment') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap;">
                                            <b>{!! trans('dept_voucher.amount') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap;">
                                            <b>{!! trans('dept_voucher.exchange') !!}</b> 
                                        </td>
                                        <td style="text-align: center; white-space: nowrap;">
                                            <b>{!! trans('dept_voucher.account_payment') !!}</b>
                                        </td>
                                    </thead>
                                    @foreach ($paymentReceiptVoucher->dept_vouchers as $item)
                                    <tr> 
                                        {{ $item[0] }}
                                        <td style="white-space: nowrap; min-width: 120px;">
                                            {!! Form::text("day_voucher[]", old('day_voucher[]', date("d/m/Y", strtotime(str_replace('/', '-', $item->voucher_date)))), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("trx_no[]", old('trx_no[]', $item->voucher_no), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("invoice_date[]", old('invoice_date[]', date("d/m/Y", strtotime(str_replace('/', '-', $item->invoice_date)))), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("invoice_no[]", old('invoice_no[]', $item->invoice_no), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("explain[]", old('explain[]', $item->desc), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("due_date[]", old('due_date[]', date("d/m/Y", strtotime(str_replace('/', '-', $item->due_date)))), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("amount[]", old('amount[]', $item->amount), ['class' => 'form-control currency', 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("exchange[]", old('exchange[]', $item->exchange), ['class' => 'form-control currency', 'readonly'  ]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("account_payment[]", old('account_payment[]', $item->account_payment), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle;" class="text-right">
                                            <b>{!! trans('input_invoices.total') !!}</b>&nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("total_amount", old('total_amount', $paymentReceiptVoucher->total_amount_deptv), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("total_exchange", old('total_exchange', $paymentReceiptVoucher->total_exchange_deptv), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr> 
                                @elseif($dataStep_1)
                                <thead style="background: #3C80BC; color:white">
                                    <td style="text-align: center; white-space: nowrap; min-width: 120px;">
                                         <b>{!! trans('dept_voucher.voucher_date') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 180px;">
                                         <b>{!! trans('dept_voucher.voucher_no') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                         <b>{!! trans('dept_voucher.invoice_date') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 120px;">
                                         <b>{!! trans("dept_voucher.invoice_no") !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                         <b>{!! trans('dept_voucher.explain') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                         <b>{!! trans('dept_voucher.time_for_payment') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap;">
                                         <b>{!! trans('dept_voucher.amount') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap;">
                                         <b>{!! trans('dept_voucher.exchange') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap;">
                                        <b>{!! trans('dept_voucher.account_payment') !!}</b>
                                    </td>
                                </thead>
                                  <?php $j= 0?>
                                    @foreach ($dataStep_1 as $value)
                                        <tr> 
                                            <td style="white-space: nowrap; min-width: 120px;">
                                                {!! Form::hidden('id[]', $value['id']) !!}
                                                {!! Form::text("day_voucher[]", old('day_voucher[]', $value['voucher_date']), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("trx_no[]", old('trx_no[]', $value['voucher_no']), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("invoice_date[]", old('invoice_date[]', $value['invoice_date']), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("invoice_no[]", old('invoice_no[]', $value['invoice_no']), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("explain[]", old('explain[]', $value['explain']), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("due_date[]", old('due_date[]', date("d/m/Y", strtotime(str_replace('/', '-', $value['due_date'])))), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("amount[]", old('amount[]', $value['amount']), ['class' => 'form-control currency', 'readonly']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange[]", old('exchange[]', $value['exchange']), ['class' => 'form-control currency', 'readonly'  ]) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("account_payment[]", old('account_payment[]', $value['account_payment']), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                        </tr>
                                        <?php $j++?>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle;" class="text-right">
                                            <b>{!! trans('input_invoices.total') !!}</b>&nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("total_amount", old('total_amount', $data_start['total_amount']), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("total_exchange", old('total_exchange', $data_start['total_exchange']), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr> 
                                @elseif(count($data))
                                <thead style="background: #3C80BC; color:white">
                                    <td style="text-align: center; vertical-align: middle; min-width: 120px;">
                                       <b>{!! trans('dept_voucher.choose_voucher') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 120px;">
                                         <b>{!! trans('dept_voucher.voucher_date') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 180px;">
                                         <b>{!! trans('dept_voucher.voucher_no') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                         <b>{!! trans('dept_voucher.invoice_date') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 120px;">
                                         <b>{!! trans("dept_voucher.invoice_no") !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                         <b>{!! trans('dept_voucher.explain') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap; min-width: 90px;">
                                         <b>{!! trans('dept_voucher.time_for_payment') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap;">
                                         <b>{!! trans('dept_voucher.amount') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap;">
                                         <b>{!! trans('dept_voucher.exchange') !!}</b> 
                                    </td>
                                    <td style="text-align: center; white-space: nowrap;">
                                        <b>{!! trans('dept_voucher.account_payment') !!}</b>
                                    </td>
                                </thead>
                                  <?php $j= 0?>
                                    @foreach ($data as $key => $value)
                                        @foreach($value as $i => $s)
                                            <tr> 
                                                {!! Form::hidden('stt[]', $j) !!}
                                                {!! Form::hidden('id[]', $voucher[$key]['id']) !!}
                                                <td style="white-space: nowrap; min-width: 120px; vertical-align: middle; text-align:center; padding:0 !important">
                                                    {!! Form::checkbox('checkboxindex[]', $j, null, array("class"=> 'minimal')) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 120px; padding:0 !important">
                                                    {!! Form::text("day_voucher[]", old('day_voucher[]', date("d/m/Y", strtotime(str_replace('/', '-', $voucher[$key]['voucher_date'])))), ['class' => 'form-control date']) !!}
                                                </td> 
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("trx_no[]", old('trx_no[]', $voucher[$key]['voucher_no']), ['class' => 'form-control']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("invoice_date[]", old('invoice_date[]', date("d/m/Y", strtotime(str_replace('/', '-', $data[$key][$i]['invoice_date'])))), ['class' => 'form-control date']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("invoice_no[]", old('invoice_no[]', $data[$key][$i]['invoice_no']), ['class' => 'form-control']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("explain[]", old('explain[]', $voucher[$key]['desc']), ['class' => 'form-control']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("due_date[]", old('due_date[]', date("d/m/Y", strtotime(str_replace('/', '-', $voucher[$key]['time_for_payment'])))), ['class' => 'form-control']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("amount[]", old('amount[]', $data[$key][$i]['amount']), ['class' => 'form-control currency']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("exchange[]", old('exchange[]', $data[$key][$i]['exchange']), ['class' => 'form-control currency']) !!}
                                                </td>
                                                <td style="white-space: nowrap; min-width: 200px; padding:0 !important">
                                                    {!! Form::text("account_payment[]", old('account_payment[]', $data[$key][$i]['credit_account']), ['class' => 'form-control'  ]) !!}
                                                </td>
                                            </tr>
                                            <?php $j++?>
                                        @endforeach 
                                    @endforeach
                                @else
                                    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
                                @endif
                            </table>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue'
    });
    $(".currency").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'max':99999999999999999999.99, 'digits':2,'removeMaskOnSubmit': true});
</script>

