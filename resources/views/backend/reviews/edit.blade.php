@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('reviews.label') !!}
@stop
@section('head')
    {!! HTML::style('assets/backend/plugins/iCheck/all.css') !!}
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/jquery-fullsizable/css/jquery-fullsizable.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/jquery-fullsizable/css/jquery-fullsizable-theme.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('reviews.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.reviews.index') !!}">{!! trans('reviews.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open([ 'url' => route('admin.reviews.update', $review->id), 'method' => 'PUT', 'role' => 'form']) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('customers.label') !!}
                </th>
                <td style="width: 35%;">
                    <a href="{!! route('admin.customers.show', $customer->id) !!}" target="_bank">{!! $customer->fullname !!} <i class="fas fa-external-link-alt"></i></a>
                </td>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('products.label') !!}
                </th>
                <td>
                    <a href="{!! route('admin.products.show', $product->id) !!}" target="_bank">{!! $product->name !!} <i class="fas fa-external-link-alt"></i></a>
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('reviews.rating') !!}
                </th>
                <td>
                    {!! Form::selectRange('rating', 1, 5, old('rating', $review->rating), ['class' => 'form-control', 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('reviews.comment') !!}
                </th>
                <td colspan="3">
                    {!! Form::textarea('comment', old('comment', $review->comment), ['class' => 'form-control', 'rows' => 4, 'required', 'maxlength' => 1024]) !!}
                </td>
            </tr>
            <?php $images = $review->images()->get(); ?>
            @if ($images->count())
                <tr>
                    <th class="table_right_middle">
                        {!! trans('system.image') !!}
                    </th>
                    <td colspan="3">
                        <div class="row">
                            @foreach ($images as $img)
                                <div class="col-md-1">
                                    <a href="{!! asset(config('upload.review') .  str_pad($review->product_id, 5, "0", STR_PAD_LEFT) . '/' . $img->title) !!}" class="fullsizable"><img style=" border: 1px solid #c2c2c2; padding: 2px;" height="40" src="{!! asset(config('upload.review') .  str_pad($review->product_id, 5, "0", STR_PAD_LEFT) . '/' . $img->title) !!}"></a>
                                    {{-- <img src="{!! asset(config('upload.review') .  str_pad($review->product_id, 5, "0", STR_PAD_LEFT) . '/' . $img->title) !!}" height="80"> --}}
                                </div>
                            @endforeach
                        </div>
                    </td>
                </tr>
            @endif
            <tr>
                <th class="table_right_middle">
                    {!! trans('reviews.reply') !!}
                </th>
                <td colspan="3">
                    {!! Form::textarea('reply', old('reply', $review->reply), ['class' => 'form-control', 'rows' => 4, 'maxlength' => 1024]) !!}
                </td>
            </tr>
            <tr>
                <th class="text-center" colspan="4">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', $review->status), [ 'class' => 'minimal' ]) !!}
                        Duyệt
                    </label>
                </th>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    {!! HTML::link(route( 'admin.reviews.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                    <span class="label label-danger message"></span>
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/jquery-fullsizable/js/jquery.fullsizable.2.0.2.min.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
            $('a.fullsizable').fullsizable();
        });
    }(window.jQuery);
</script>
@stop