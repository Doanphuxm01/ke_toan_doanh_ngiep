@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('reviews.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/switch-button/bootstrap-switch.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/jquery-fullsizable/css/jquery-fullsizable.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/jquery-fullsizable/css/jquery-fullsizable-theme.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('reviews.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.reviews.index') !!}">{!! trans('reviews.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bprovider">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.reviews.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('name', trans('products.label')) !!}
                        {!! Form::text('name', Request::input('name'), ['class' => 'form-control'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [-1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active')], Request::input('status'), ['class' => 'form-control'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('page_num', trans('system.page_num')) !!}
                        {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2',  "style" => "width: 100%;"]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label')) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            {!! $reviews->appends(Request::except('page'))->render() !!}
        </div>
    </div>
    @if (count($reviews) > 0)
    <div class="box">
        <div class="box-header">
            <?php $i = (($reviews->currentPage() - 1) * $reviews->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $reviews->count()) . ' ( ' . trans('system.total') . ' ' . $reviews->total() . ' )' !!}
                </div>
                | <i>Chú giải: </i>&nbsp;&nbsp;
                <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </span>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('reviews.product') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('customers.label') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('reviews.rating') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('reviews.comment') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('reviews.image') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('reviews.reply') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!}</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.visible') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reviews as $item)
                        <tr>
                            <td style="text-align: center; vertical-align: middle; width: 3%;">{!! $i++ !!}</td>
                            <td style="vertical-align: middle; width: 8%;">
                                <?php $p = App\Product::find($item->product_id); ?>
                                @unless (is_null($p))
                                    <a href="{!! route('home.product', ['id' => $p->id, 'slug' => str_slug($p->name) ]) !!}" target="_blank">{!! $p->name !!}</a>
                                @endunless
                            </td>
                            <td style="vertical-align: middle; width: 8%;">
                                <?php $u = App\Customer::find($item->customer_id); ?>
                                @unless (is_null($u))
                                    <a href="{!! route('admin.customers.show', $u->id) !!}" target="_blank">{!! $u->fullname !!}</a>
                                @endunless
                            </td>
                            <td style="text-align: center; vertical-align: middle; width: 3%;">
                                {!! $item->rating !!}/5
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->comment !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <?php $images = $item->images()->get(); $j = 0; ?>
                                @foreach($images as $img)
                                    <?php $imageClasses[$item->id] = $item->id; ?>
                                    <a style="{!! $j++ ? "display: none;" : "" !!}" href="{!! asset(config('upload.review') .  str_pad($item->product_id, 5, "0", STR_PAD_LEFT) . '/' . $img->title) !!}" class="fullsizable"><img style=" border: 1px solid #c2c2c2; padding: 2px;" height="40" src="{!! asset(config('upload.review') .  str_pad($item->product_id, 5, "0", STR_PAD_LEFT) . '/' . $img->title) !!}"></a>
                                @endforeach
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->reply !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                <input type="checkbox" data-size="mini" name="my-checkbox" value="{!! $item->id !!}" @if($item->status) checked @endif />
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                <a href="{!! route('admin.reviews.edit', $item->id) !!}" class="btn btn-xs btn-default">
                                    <i class="text-warning glyphicon glyphicon-edit"></i>
                                </a>
                                <a href="javascript:void(0)" link="{!! route('admin.reviews.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/switch-button/bootstrap-switch.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/jquery-fullsizable/js/jquery.fullsizable.2.0.2.min.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('a.fullsizable').fullsizable();
            $("[name='my-checkbox']").bootstrapSwitch();
            $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
                var tmp = $(this), url = '{!! route("admin.reviews.publish", ":review_id" ) !!}';
                url = url.replace(':review_id', $(this).attr('value'));
                NProgress.start();
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: { status: state },
                    headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                    datatype: 'json',
                    success: function(data) {
                        if (data.success) {
                            toastr.success(data.message, "{!! trans('system.info') !!}");
                        } else {
                            toastr.error(data.message, "{!! trans('system.have_an_error') !!}");
                            tmp.bootstrapSwitch('state', false, true);
                        }
                    },
                    error: function(obj, status, err) {
                        toastr.error(err, "{!! trans('system.have_an_error') !!}");
                        tmp.bootstrapSwitch('state', false, true);
                    }
                }).always(function() {
                    NProgress.done();
                });
            });
        });
    }(window.jQuery);
</script>
@stop
