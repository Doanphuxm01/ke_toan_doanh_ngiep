@extends('backend.master')
@section('title')
    {!! trans('system.action.list') !!} {!! trans('inventory_categories.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('inventory_categories.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.inventory-categories.index') !!}">{!! trans('inventory_categories.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.inventory-categories.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('province', trans('inventory_categories.province')) !!}
                        {!! Form::select('province', [-1 => trans('system.dropdown_all')] + $provinces, Request::input('province'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('page_num', trans('system.page_num')) !!}
                        {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2',  "style" => "width: 100%;"]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                        <button type="submit" class="btn btn-default btn-flat">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group">
                <a href="{!! route('admin.inventory-categories.create') !!}" class='btn btn-primary btn-flat'>
                    <span class="ion-plus"></span>&nbsp;{!! trans('system.action.create') !!}
                </a>
            </div>
        </div>
        <div class="col-md-12 text-right">
            {!! $inventoryCategories->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($inventoryCategories) > 0)
        <div class="box">
            <div class="box-header">
                <?php $i = (($inventoryCategories->currentPage() - 1) * $inventoryCategories->perPage()) + 1; ?>
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $inventoryCategories->count()) . ' ( ' . trans('system.total') . ' ' . $inventoryCategories->total() . ' )' !!}
                    | <i>Chú giải: </i>&nbsp;&nbsp;
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </span>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class='table table-striped table-bordered tree'>
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                                <th style="vertical-align: middle;"> {!! trans('inventory_categories.name') !!} </th>
                                <th style="text-align: center; vertical-align: middle;"> {!! trans('inventory_categories.code') !!} </th>
                                <th style="text-align: center; vertical-align: middle;"> {!! trans('inventory_categories.phone') !!} </th>
                                <th style="text-align: center1; vertical-align: middle;"> {!! trans('inventory_categories.address') !!} </th>
                                <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!}<br/>{!! trans('system.created_by') !!}</th>
                                <th style="text-align: center; vertical-align: middle;"> {!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!} </th>
                                <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                            </tr>
                        </thead>
                        <tbody class="borderless">
                            <?php
                                $labels = ['success', 'danger', 'info', 'warning', 'default'];
                            ?>
                            @foreach ($inventoryCategories as $item)
                                <tr>
                                    <td style="text-align: center; width: 3%; vertical-align: middle;">{!! $i++ !!}</td>
                                    <td style="vertical-align: middle;">
                                            {!! $item->name !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {!! $item->code !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {!! $item->phone !!}
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <span class="label label-{!! $labels[$item->province_id%5] !!}">{!! isset($provinces[$item->province_id])? $provinces[$item->province_id] : '' !!}</span>
                                        {!! $item->address !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if($item->status == 0)
                                            <span class="label label-default">
                                                {!! trans('system.status.deactive') !!}
                                            </span>
                                        @elseif($item->status == 1)
                                            <span class="label label-success">
                                                {!! trans('system.status.active') !!}
                                            </span>
                                        @endif
                                        <br/>
                                        <?php $adm = App\User::find($item->created_by); ?>
                                        <a href="{!! is_null($adm) ? '#' : route('admin.users.show', $adm->id) !!}" target="_blank">
                                            <small>{!! is_null($adm) ? "" : $adm->fullname !!} <i class="fas fa-external-link-alt"></i></small>
                                        </a>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                        <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                        &nbsp;
                                        <a href="{!! route('admin.inventory-categories.edit', $item->id) !!}" class="btn btn-xs btn-default">
                                            <i class="text-warning glyphicon glyphicon-edit"></i>
                                        </a>
                                        &nbsp;
                                        <a href="javascript:void(0)" link="{!! route('admin.inventory-categories.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                            <i class="text-danger glyphicon glyphicon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function(){
                $(".select2").select2();
            });
        }(window.jQuery);
    </script>
@stop
