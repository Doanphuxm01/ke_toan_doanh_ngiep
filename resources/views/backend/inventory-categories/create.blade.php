@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('inventory_categories.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('inventory_categories.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.inventory-categories.index') !!}">{!! trans('inventory_categories.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.inventory-categories.store')]) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('inventory_categories.name') !!}
                </th>
                <td style="width: 35%;">
                    {!! Form::text('name', old('name'), ["class" => "form-control", "required", "maxlength" => 255]) !!}
                </td>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('inventory_categories.code') !!}
                </th>
                <td>
                    {!! Form::text('code', old('code'), ["class" => "form-control", "required", "maxlength" => 20]) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('inventory_categories.phone') !!}
                </th>
                <td>
                    {!! Form::text('phone', old('phone'), ["class" => "form-control", "required", "maxlength" => 15]) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('inventory_categories.province') !!}
                </th>
                <td>
                    {!! Form::select('province', ['' => trans('system.dropdown_choice')] + $provinces, old('province'), ["class" => "form-control select2", "required"]) !!}
                </td>
            <tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('inventory_categories.address') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('address', old('address'), ['class' => 'form-control', "maxlength" => 255, "required"]) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('inventory_categories.note') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('note', old('note'), ['class' => 'form-control', "maxlength" => 255]) !!}
                </td>
            </tr>
            <tr>
                <td class="text-center" colspan="4">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                        {!! trans('system.status.active') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    <a class="btn btn-default btn-flat" href="{!! route('admin.inventory-categories.index') !!}">{!! trans('system.action.cancel') !!}</a>
                    {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $(".currency").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'digits': 2, 'max': 9999, 'removeMaskOnSubmit': true});
            });
        }(window.jQuery);
    </script>
@stop
