@extends('backend.master')
@section('title')
{!! trans('system.action.detail') !!} - {!! trans('menus.expenses-prepaid.label') !!}
@stop
@section('head')
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }

    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
        
            {!! trans('menus.expenses-prepaid.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.expenses-prepaid.index') !!}">{!! trans('menus.expenses-prepaid.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('expenses_prepaid.code') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('expenses_prepaid_code', old('expenses_prepaid_code', $expenses_prepaid->expenses_prepaid_code), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('expenses_prepaid.amount') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('money_amount', old('money_amount', $expenses_prepaid->money_amount), ['id' => 'amount', 'class' => 'form-control money', 'readonly']) !!}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('expenses_prepaid.name') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('expenses_prepaid_name', old('expenses_prepaid_name', $expenses_prepaid->expenses_prepaid_name), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('expenses_prepaid.period_number') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('period_number', old('period_number', $expenses_prepaid->period_number), ['class' => 'form-control', 'style' => 'text-align: right', 'readonly']) !!}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('expenses_prepaid.voucher_date') !!}</b>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('voucher_date', old('voucher_date'), ['class' => 'form-control voucher_date', 'readonly']) !!}       
                            </div>
                        </div>
                    <div class="col-md-2">
                        <b>{!! trans('expenses_prepaid.periodical_allocation_value') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('periodical_allocation_value', old('periodical_allocation_value', $expenses_prepaid->periodical_allocation_value), ['class' => 'form-control money', 'readonly']) !!}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <b>{!! trans('expenses_prepaid.allocation_account') !!}</b>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('allocation_account', old('allocation_account', $expenses_prepaid->allocation_account), ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-4" style="text-align:right">
                            @if($expenses_prepaid->status == 1)
                                <b>{!! trans('expenses_prepaid.stop') !!}</b> &nbsp {!! Form::checkbox('status', 0, old('status', 0), [ 'class' => 'minimal' ]) !!} 
                            @else
                                <b>{!! trans('expenses_prepaid.stop') !!}</b> &nbsp {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!} 
                            @endif
                           
                        </div>
                    </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('expenses_prepaid.voucher_collection') !!}</h3>                
            </div>
            <div class="box-body">
                <table class="table table-bordered " id="voucher_table">
                    <thead style="background: #3C8DBC;color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;">{!! trans('expenses_prepaid.voucher_date') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;">{!! trans('expenses_prepaid.voucher_no') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 40%;">{!! trans('expenses_prepaid.desc') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;">{!! trans('fixed_asset_depreciation.money_amount') !!}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($vouchers) == 0)
                            <tr>
                                <td colspan="4" style="font-size: 16px;" class="text-center" ><b>{!! trans('expenses_prepaid.no_data') !!}</b></td>
                            </tr>
                        @else
                        @foreach($vouchers as $voucher)
                            <tr>
                                <td style="text-align: center">
                                    {!! date_format(date_create($voucher->voucher_date), "d/m/Y") !!}
                                </td>
                                <td style="text-align: center">
                                    <a href="{!! route('admin.expenses-prepaid-allocation.show', $voucher->id) !!}">{!! $voucher->voucher_no !!}</a>
                                </td>
                                <td>
                                    {!! $voucher->description !!}  
                                </td>
                                <td style="text-align: right" class="amount">
                                    {!! number_format(App\Models\ExpensesPrepaidAllocation::amount($expenses_prepaid->id, $voucher->id), 2) !!}
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot style="background: #3C8DBC;color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;"> {!! trans('fixed_asset_depreciation.total') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 40%;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;" id="total" class="money"></th>
                        </tr>
                        </tfoot>
                </table>        
            </div>
        </div>
    </section>
    
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
           
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $('.voucher_date').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    

    $('.select2').select2();
    $(".money").inputmask({'alias': 'decimal', 'digits' : 2, 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
</script>
<script>
    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
    var total = 0
    $(document).ready(function(){
        jQuery.each($('.amount'), function(){
            total += Number($(this).text().replace(/,/g, ""))
        })
        $('#total').text(formatNumber(total))
        
    })
</script>
@stop