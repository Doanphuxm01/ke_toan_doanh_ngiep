<div id="voucher" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ trans('system.action.create') }}</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('from', trans('system.from')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('from_date', old('from_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('to', trans('system.to')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('to_date', old('to_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="button" class="btn btn-default btn-flat" id="search" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
                <div>
                    <table class="table table-bordered">
                        <thead style="background: #3C8DBC;color: white;">
                            <tr>
                                <th style=" min-width: 100;">  
                                </th>
                                <td style="display:none"></td>
                                <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('expenses_prepaid.voucher_date') !!}</th>
                                <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('expenses_prepaid.voucher_no') !!}</th>
                                <th style="text-align: center; vertical-align: middle; min-width: 300px;">{!! trans('expenses_prepaid.desc') !!}</th>
                                <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('fixed_asset_depreciation.debit_account') !!}</th>
                                <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('fixed_asset_depreciation.credit_account') !!}</th>
                                <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('fixed_asset_depreciation.money_amount') !!}</th>
                            </tr>
                        </thead>
                        <tbody class="voucher_modal">
                        </tbody>
                    </table>
                    <div style="text-align:center"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('system.confirm_no') !!}</button>
                        <button type="button" class="btn btn-primary" id="confirm_voucher">{!! trans('system.confirm_yes') !!}</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>