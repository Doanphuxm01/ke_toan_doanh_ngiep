<div id="add-cost-item" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{ trans('system.action.create') }}</h4>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                            <div class="col-md-2">
                                <b>{!! trans('cost_items.cost_item') !!}</b>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('new_cost_item', old('new_cost_item'), ['class' => 'form-control', 'required']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <b>{!! trans('cost_items.description') !!}</b>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('description', old('description'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <b>{!! trans('cost_items.interpretation') !!}</b>
                            </div>
                            <div class="col-md-10">
                                {!! Form::textarea('interpretation', old('interpretation'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat', 'id' => 'save-add']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>