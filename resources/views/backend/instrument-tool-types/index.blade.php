@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('menus.instrument-tool-types.label') !!}
@stop
@section('head')
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('menus.instrument-tool-types.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.instrument-tool-types.index') !!}">{!! trans('menus.instrument-tool-types.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
                {!! Form::open(['url' => route('admin.instrument-tool-types.index'), 'method' => 'GET', 'role' => 'search']) !!}
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('code', trans('instrument_tool_types.code')) !!}
                            {!! Form::text('code', Request::input('code'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('status', trans('system.status.label')) !!}
                            {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                            <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.instrument-tool-types.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $types->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($types) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($types->currentPage() - 1) * $types->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $types->count()) . ' ( ' . trans('system.total') . ' ' . $types->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class='table table-striped table-bordered tree'>
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('instrument_tool_types.code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tool_types.name') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tool_types.description') !!} </th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody class="borderless">
                    @foreach ($types as $item)
                        <tr class="treegrid-{!! $item->id !!}">
                            <td style="text-align: center; vertical-align: middle;">
                                <a href="{!! route('admin.instrument-tool-types.show', $item->id) !!}">{!! $item->code !!}</a>
                            </td>
                            <td  style="text-align: center; vertical-align: middle;">
                                {!! $item->name !!}
                            </td>
                            <td  style="text-align: center; vertical-align: middle;">
                                {!! $item->description !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                    <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                @elseif($item->status == 1)
                                    <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <a href="{!! route('admin.instrument-tool-types.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}">
                                    <i class="text-warning glyphicon glyphicon-edit"></i>
                                </a>
                                &nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.instrument-tool-types.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
      </div>
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script>
    $('.select2').select2();
</script>
<script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
