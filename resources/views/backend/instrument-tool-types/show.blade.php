@extends('backend.master')
@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('menus.instrument-tool-types.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.instrument-tool-types.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($type->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.instrument-tool-types.index') !!}">{!! trans('menus.instrument-tool-types.label') !!}</a></li>
        </ol>
    </section>
        <table class='table borderless' style="width: 55%; margin:auto">
            <tr>
                <th>
                    {!! trans('instrument_tool_types.code') !!}
                </th>
                <td>
                    {!! Form::text('type_code', old('type_code', $type->code), ["class" => "form-control", 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('instrument_tool_types.name') !!}
                </th>
                <td>
                    {!! Form::text('type_name', old('type_name', $type->name), ["class" => "form-control", 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('instrument_tool_types.description') !!}
                </th>
                <td>
                    {!! Form::textarea('description', old('description', $type->description), ["class" => "form-control", 'disabled']) !!}
                </td>
            </tr>
        </table>
@stop

@section('footer')
<script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
        });
    }(window.jQuery);
</script>
@include('backend.plugins.tinymce')
@stop