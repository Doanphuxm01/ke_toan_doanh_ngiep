@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('menus.instrument-tool-types.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.instrument-tool-types.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.instrument-tool-types.index') !!}">{!! trans('menus.instrument-tool-types.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.instrument-tool-types.store'), 'role' => 'form']) !!}
        <table class='table borderless' style="width: 55%; margin:auto">
            <tr>
                <th>
                    {!! trans('instrument_tool_types.code') !!}
                </th>
                <td>
                    {!! Form::text('type_code', old('type_code'), ["class" => "form-control", 'required']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('instrument_tool_types.name') !!}
                </th>
                <td>
                    {!! Form::text('type_name', old('type_name'), ["class" => "form-control", 'required']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('instrument_tool_types.description') !!}
                </th>
                <td>
                    {!! Form::textarea('description', old('description'), ["class" => "form-control"]) !!}
                </td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">
                    {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                    {!! trans('system.status.active') !!}
                </th>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2">
                    <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop

@section('footer')
<script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
        });
    }(window.jQuery);
</script>
@include('backend.plugins.tinymce')
@stop