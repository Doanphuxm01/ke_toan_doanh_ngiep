@extends('backend.master')
@section('title')
{!! trans('menus.home') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1 class="title">{!! trans('system.dashboard') !!}</h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}"><i class="fa fa-home"></i> {!! trans('menus.home') !!}</a></li>
    </ol>
</section>
<section class="content">
    <?php $user = auth()->guard('admin')->user(); ?>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-md-sync"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Đợi duyệt</span>
                    <span class="info-box-number">12</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-md-checkmark"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Đã duyệt</span>
                    <span class="info-box-number">23<small>/40</small></span>
                </div>
            </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="ion ion-md-thumbs-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Đang thuê</span>
                    <span class="info-box-number">15</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-md-done-all"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Hoàn thành</span>
                    <span class="info-box-number">2</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Yêu cầu mới</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Cont no</th>
                                    <th style="vertical-align: middle;">Bill no</th>
                                    <th style="text-align: center; vertical-align: middle;">Thời gian gửi</th>
                                    <th style="text-align: center; vertical-align: middle;">Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php $i = 1; ?>
                            	@if (count($newRequest))
	                                @foreach ($newRequest as $container)
	                                    <tr>
	                                        <td style="text-align: center; vertical-align: middle;">{!! $i++ !!}</td>
	                                        <td style="text-align: center; vertical-align: middle;">
	                                            <a href="{!! route('admin.containers.show', $container->id) !!}">{!! $container->cont_no !!}</a>
	                                        </td>
	                                        <td style="vertical-align: middle;">{!! $container->bill_no !!}</td>
	                                        <td style="text-align: center; vertical-align: middle;">
	                                        	<label class="label label-default">{!! $container->requested_time ? date('d/m/Y H:i', $container->requested_time) : "" !!}</label>
	                                        </td>
	                                        <td style="text-align: center; vertical-align: middle;">
	                                        	<a href="{!! route('admin.containers.edit', $container->id) !!}">Cập nhật</a>
	                                        </td>
	                                    </tr>
	                                @endforeach
                                @else
                                	<tr>
                                        <td style="text-align: center; vertical-align: middle;" colspan="5">Chưa có dữ liệu</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Cont chuyển bị giao</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th style="text-align: center; vertical-align: middle;">STT</th>
                                    <th style="text-align: center; vertical-align: middle;">Mã đơn hàng</th>
                                   	<th style="text-align: center; vertical-align: middle;">Cont no</th>
                                    <th style="text-align: center; vertical-align: middle;">Ngày đóng hàng</th>
                                    <th style="text-align: center1; vertical-align: middle;">Nơi đóng hàng</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php $i = 1; ?>
                            	@if (count($booked))
                                @foreach ($booked as $container)
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;">{!! $i++ !!}</td>
                                        <td style="text-align: center; vertical-align: middle;">
                                            <a href="{!! route('admin.containers.show', $container->id) !!}">{!! $container->book_code !!}</a>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle;">{!! $container->cont_no !!}</td>
                                        <td style="text-align: center; vertical-align: middle;">
                                        	<label class="label label-default">{!! $container->closed_time ? date('d/m/Y H:i', strtotime($container->closed_time)) : "" !!}</label>
                                        </td>
                                        <td style="text-align: center1; vertical-align: middle;">
                                        	{!! $container->closed_loc !!}
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                	<tr>
                                        <td style="text-align: center; vertical-align: middle;" colspan="5">Chưa có dữ liệu</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('.time_range').daterangepicker({
                autoUpdateInput: false,
                "locale": {
                    "format": "DD/MM/YYYY HH:mm",
                    "separator": " - ",
                    "applyLabel": "Áp dụng",
                    "cancelLabel": "Huỷ bỏ",
                    "fromLabel": "Từ ngày",
                    "toLabel": "Tới ngày",
                    "customRangeLabel": "Tuỳ chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Thg 1",
                        "Thg 2",
                        "Thg 3",
                        "Thg 4",
                        "Thg 5",
                        "Thg 6",
                        "Thg 7",
                        "Thg 8",
                        "Thg 9",
                        "Thg 10",
                        "Thg 11",
                        "Thg 12"
                    ],
                    "firstDay": 1
                },
                ranges: {
                   'Hôm nay': [moment(), moment()],
                   'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                   '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                   'Tháng này': [moment().startOf('month'), moment()],
                   'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                },
                "alwaysShowCalendars": true,
                maxDate: moment(),
                minDate: moment().subtract(1, "years"),
            }, function(start, end, label) {
                $('.time_range').val(start.format('DD/MM/YYYY HH:mm') + " - " + end.format('DD/MM/YYYY HH:mm'));
            });
            });
    }(window.jQuery);
</script>
@stop