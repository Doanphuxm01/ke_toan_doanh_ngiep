@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('menus.cost-items.label') !!}
@stop
@section('head')
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('menus.cost-items.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.cost-items.index') !!}">{!! trans('menus.cost-items.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
                {!! Form::open(['url' => route('admin.cost-items.index'), 'method' => 'GET', 'role' => 'search']) !!}
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('cost_item', trans('cost_items.cost_item')) !!}
                            {!! Form::text('cost_item', Request::input('cost_item'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('status', trans('system.status.label')) !!}
                            {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                            <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.cost-items.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $cost_items->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($cost_items) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($cost_items->currentPage() - 1) * $cost_items->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $cost_items->count()) . ' ( ' . trans('system.total') . ' ' . $cost_items->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class='table table-striped table-bordered tree'>
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('cost_items.cost_item') !!}</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('cost_items.description') !!} </th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('cost_items.interpretation') !!} </th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody class="borderless">
                    @foreach ($cost_items as $item)
                        <tr class="treegrid-{!! $item->id !!}">
                            <td style="text-align: center; vertical-align: middle;">
                                <a href="{!! route('admin.cost-items.show', $item->id) !!}">{!! $item->cost_item !!}</a>
                            </td>
                            <td  style="text-align: center; vertical-align: middle;">
                                {!! $item->description !!}
                            </td>
                            <td  style="text-align: center; vertical-align: middle;">
                                {!! $item->interpretation !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                    <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                @elseif($item->status == 1)
                                    <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                @endif
                            </td>
                            @if(count(App\Models\CostItem::checkUpdateDelete($item->cost_item)) <= 0)
                            <td style="text-align: center; vertical-align: middle;">
                                <a href="{!! route('admin.cost-items.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}">
                                    <i class="text-warning glyphicon glyphicon-edit"></i>
                                </a>
                                &nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.cost-items.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                            @else
                            <td style="text-align: center; vertical-align: middle;">
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
      </div>
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script>
    $('.select2').select2();
</script>
<script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop