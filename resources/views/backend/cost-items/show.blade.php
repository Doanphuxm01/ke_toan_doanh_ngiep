@extends('backend.master')
@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('menus.cost-items.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.cost-items.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($cost_item->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.cost-items.index') !!}">{!! trans('menus.cost_items.label') !!}</a></li>
        </ol>
    </section>
        <table class='table borderless' style="width: 55%; margin:auto">
            <tr>
                <th>
                    {!! trans('cost_items.cost_item') !!}
                </th>
                <td>
                    {!! Form::text('cost_item', old('cost_item', $cost_item->cost_item), ["class" => "form-control", 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('cost_items.description') !!}
                </th>
                <td>
                    {!! Form::text('description', old('description', $cost_item->description), ["class" => "form-control", 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('cost_items.interpretation') !!}
                </th>
                <td>
                    {!! Form::textarea('interpretation', old('interpretation', $cost_item->interpretation), ["class" => "form-control", 'disabled']) !!}
                </td>
            </tr>
        </table>
@stop

@section('footer')
<script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
        });
    }(window.jQuery);
</script>
@include('backend.plugins.tinymce')
@stop