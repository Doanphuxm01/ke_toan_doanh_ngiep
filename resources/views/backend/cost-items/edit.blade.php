@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('menus.cost-items.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.cost-items.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.cost-items.index') !!}">{!! trans('menus.cost-items.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.cost-items.update', $cost_item->id), 'method' => 'PUT', 'role' => 'form', 'files' => true]) !!}
        <table class='table borderless' style="width: 55%; margin:auto">
            <tr>
                <th>
                    {!! trans('cost_items.cost_item') !!}
                </th>
                <td>
                    {!! Form::text('new_cost_item', old('new_cost_item', $cost_item->cost_item), ["class" => "form-control", 'required']) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('cost_items.description') !!}
                </th>
                <td>
                    {!! Form::text('description', old('description', $cost_item->description), ["class" => "form-control"]) !!}
                </td>
            </tr>
            <tr>
                <th>
                    {!! trans('cost_items.interpretation') !!}
                </th>
                <td>
                    {!! Form::textarea('interpretation', old('interpretation', $cost_item->interpretation), ["class" => "form-control"]) !!}
                </td>
            </tr>
            <tr>
                <th colspan="4" class="text-center">
                    {!! Form::checkbox('status', 1, old('status', $cost_item->status), [ 'class' => 'minimal' ]) !!}
                    {!! trans('system.status.active') !!}
                </th>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="4">
                    <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>

    {!! Form::close() !!}
@stop

@section('footer')
<script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
        });
    }(window.jQuery);
</script>
@include('backend.plugins.tinymce')
@stop