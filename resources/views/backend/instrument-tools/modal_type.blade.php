<div id="add-type" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{ trans('system.action.create') }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <b>{!! trans('instrument_tools.code') !!}</b>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('type_code', old('type_code'), ['class' => 'form-control', 'required']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <b>{!! trans('instrument_tools.name') !!}</b>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('type_name', old('type_name'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <b>{!! trans('instrument_tools.description') !!}</b>
                            </div>
                            <div class="col-md-10">
                                {!! Form::textarea('type_description', old('type_description'), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat', 'id' => 'save-add-type']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>