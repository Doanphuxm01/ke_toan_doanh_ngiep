@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('menus.instrument-tools.label') !!}
@stop
@section('head')
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('menus.instrument-tools.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.instrument-tools.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.instrument-tools.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', trans('instrument_tools.name')) !!}
                        {!! Form::text('name', Request::input('name'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', trans('instrument_tools.type')) !!}
                        {!! Form::select('type_id', ['' => trans('system.dropdown_choice')] + App\Models\InstrumentToolType::getActiveTypes(session('current_company')),  old('type_id', Request::input('type_id')), ['id' => 'type_id', 'class' => 'form-control select2']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('from', trans('system.from')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('from', trans('system.to')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="co-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label')) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.instrument-tools.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $instrument_tools->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($instrument_tools) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($instrument_tools->currentPage() - 1) * $instrument_tools->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $instrument_tools->count()) . ' ( ' . trans('system.total') . ' ' . $instrument_tools->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" style="border-collapse: collapse;">
                    <thead style="background: #3C8DBC; color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('expenses_prepaid.voucher_date') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.voucher_number') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.code') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.name') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.type') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.currency') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.quantity') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.value') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.period_number') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('instrument_tools.periodical_allocation_value') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                        </tr>
                    </thead>
                    <tbody class="borderless">
                        @foreach ($instrument_tools as $item)
                            <tr class="treegrid-{!! $item->id !!}">
                                <td style="text-align: center; vertical-align: middle;">
                                    {!! date_format(date_create($item->voucher_date), "d/m/Y") !!}
                                </td>
                                <td  style="text-align: center; vertical-align: middle;">
                                    {!! $item->voucher_no !!}
                                </td>
                                <td  style="text-align: center; vertical-align: middle;">
                                    {!! $item->code !!}
                                </td>
                                <td  style="vertical-align: middle;">
                                    <a href="{!! route('admin.instrument-tools.show', $item->id) !!}">{!! $item->name !!}</a>
                                </td>
                                <td  style="text-align: center; vertical-align: middle;">
                                    {!! App\Models\InstrumentToolType::getName($item->type_id) !!}
                                </td>
                                <td  style="text-align: center; vertical-align: middle;">
                                    {!! App\Models\Currency::getName($item->currency_id) !!}
                                </td>
                                <td  style="text-align: right; vertical-align: middle;">
                                    {!! number_format($item->quantity) !!}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    {!! number_format(($item->unit_price * $item->quantity), 2) !!}
                                </td>
                                <td style="text-align: right; vertical-align: middle;">
                                {!! number_format($item->period_number) !!}
                                </td>
                                <td style="text-align: right; vertical-align: middle;">
                                {!! number_format($item->periodical_allocation_value, 2) !!}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    @if($item->status == 0)
                                        <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                    @elseif($item->status == 1)
                                        <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                    @endif
                                </td>
                                
                                <td style="text-align: center; vertical-align: middle;width:80px">
                                @if(count(App\Models\InstrumentToolAllocationCost::where('instrument_tool_id', $item->id)->get()) > 0)
                                @else
                                    <a href="{!! route('admin.instrument-tools.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}">
                                        <i class="text-warning glyphicon glyphicon-edit"></i>
                                    </a>
                                    &nbsp;
                                    <a href="javascript:void(0)" link="{!! route('admin.instrument-tools.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                        <i class="text-danger glyphicon glyphicon-remove"></i>
                                    </a>
                                @endif
                                </td>
                                
                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.select2').select2();
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
</script>
<script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
