@extends('backend.master')
@section('title')
{!! trans('system.action.create') !!} - {!! trans('menus.fixed-asset-depreciation.label') !!}
@stop
@section('head')
    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.fixed-asset-depreciation.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.fixed-asset-depreciation.label') !!}</a></li>
        </ol>
    </section>
    
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.fixed-asset-disposal.store'), 'role' => 'form']) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2 text-right">
                        <b>{!! trans('fixed_asset_disposal.disposal_reason') !!}</b>
                    </div>
                    <div class="col-md-5">
                        {!! Form::text('disposal_reason', old('disposal_reason'), ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-2 text-right">
                        <b>{!! trans('fixed_asset_disposal.voucher_date') !!}</b>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('voucher_date', old('voucher_date'), ['class' => 'form-control date_picker',]) !!}       
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2 text-right" >
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2 text-right">
                        <b>{!! trans('fixed_asset_disposal.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('voucher_number', old('voucher_number', $voucher_number), ['class' => 'form-control',]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_disposal.disposal_fixed_assets') !!}</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div style="margin-right:20px"> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 100px;">
                                            {!! trans('fixed_assets.asset_code') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 300px;">
                                            {!! trans('fixed_assets.asset_name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.original_price') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.depreciation_value') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.accumulated_depreciation') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.residual_value') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.original_price_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.depreciation_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_disposal.disposal_account') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>
                                        {!! Form::select('asset_code', ['' => trans('system.dropdown_choice')] + $asset_codes, old('asset_code'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('asset_name', old('asset_name'), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('original_price', old('original_price'), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('depreciation_value', old('depreciation_value'), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('accumulated_depreciation', old('accumulated_depreciation'), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('residual_value', old('residual_value'), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('original_price_account', old('original_price_account'), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('depreciation_account', old('depreciation_account'), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::select('disposal_account', ['' => trans('system.dropdown_choice')] + $disposal_accounts , old('disposal_account'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                                    </td>
                                </tr>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.accounting') !!}</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 300px;">
                                            {!! trans('fixed_asset_depreciation.description') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.debit_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.credit_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.money_amount') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.debit_object') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.object_name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.credit_object') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.object_name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.cost_item') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="accounting">                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3" style="font-size:18px" >
                                            {!! trans('fixed_asset_depreciation.total') !!}:
                                        </th>
                                        <th class="text-right" style="font-size:18px" id="total" >
                                        </th> 
                                        <th colspan="4" >
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
                <div class="text-center">
                    {!! HTML::link(route( 'admin.fixed-asset-depreciation.index' ), trans('system.action.cancel'), [ 'class' => 'btn btn-danger btn-flat ']) !!}
                    {!! Form::submit(trans('system.action.save'), ['id' => 'save-btn', 'class' => 'btn btn-primary btn-flat submit']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
   
    $('.select2').select2({
        dropdownAutoWidth: true,
    });
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
</script>
<script>
    $(document).on('change', "select[name='asset_code']", function () {
        var asset_code = $(this).val()
        var asset_name = $(this).closest('tr').find("input[name='asset_name']")
        var original_price = $(this).closest('tr').find("input[name='original_price']")
        var depreciation_value = $(this).closest('tr').find("input[name='depreciation_value']")
        var accumulated_depreciation = $(this).closest('tr').find("input[name='accumulated_depreciation']")
        var residual_value = $(this).closest('tr').find("input[name='residual_value']")
        var original_price_account = $(this).closest('tr').find("input[name='original_price_account']")
        var depreciation_account = $(this).closest('tr').find("input[name='depreciation_account']")
        var disposal_account = $(this).closest('tr').find("select[name='disposal_account']")
        if(asset_code == ""){
            asset_name.val("")
            original_price.val("")
            depreciation_value.val("")
            accumulated_depreciation.val("")
            residual_value.val("")
            original_price_account.val("")
            depreciation_account.val("")
            disposal_account.val("").change()
            $(".accounting").empty()
            $("#total").html("")
        }
        else{
            $.ajax({
            url: "{!! route('admin.get-asset-info') !!}",
            data: { asset_code: asset_code},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                $(".accounting tr").remove()
                var residual = data.fixed_asset.residual_value - data.accumulated_depreciation
                var accumulated = data.accumulated_depreciation + data.fixed_asset.accumulated_depreciation
                asset_name.val(data.fixed_asset.asset_name)
                original_price.val(data.fixed_asset.original_price)
                depreciation_value.val(data.fixed_asset.depreciation_value)
                accumulated_depreciation.val(accumulated)
                if(residual <= 0){
                    residual_value.removeClass("money").val(residual)
                }
                else{
                    residual_value.val(residual)
                }
                original_price_account.val(data.fixed_asset.original_price_account)
                depreciation_account.val(data.fixed_asset.depreciation_account)
                var html = '<tr>'
                    html += '<td>'
                    html += '{!! Form::text('description[]', old('description[]', 'Giá trị hao mòn lũy kế'), ['class' => 'form-control']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('debit_account[]', ['' => trans('system.dropdown_choice')] + $accounts, old('debit_account[]'), ['class' => 'form-control select2', 'required', 'style' => 'width: 100%']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('credit_account[]', ['' => trans('system.dropdown_choice')] + $accounts, old('credit_account[]'), ['class' => 'form-control select2', 'required', 'style' => 'width: 100%']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '<input type="text" class="form-control money" name="amount[]" value="'+accumulated+'") readonly>'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('debit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('debit_object_type'), ['class' => 'form-control select2 debit_object_type']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('debit_object_id[]', ['' => trans('system.dropdown_choice')], old('debit_object_id'), ['class' => 'form-control select2 debit_object_id']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('credit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('credit_object_type'), ['class' => 'form-control select2 credit_object_type']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('credit_object_id[]', ['' => trans('system.dropdown_choice')], old('credit_object_id'), ['class' => 'form-control select2 credit_object_id']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + $cost_items , old('cost_item[]'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}'
                    html += '</td>'
                    html += '</tr>'
                    if(residual > 0){
                    html += '<tr>'
                    html += '<td>'
                    html += '{!! Form::text('description[]', old('description[]','Giá trị còn lại'), ['class' => 'form-control']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('debit_account[]', ['' => trans('system.dropdown_choice')] + $accounts, old('debit_account[]'), ['class' => 'form-control select2 debit_account', 'required', 'style' => 'width: 100%']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('credit_account[]', ['' => trans('system.dropdown_choice')] + $accounts, old('credit_account[]'), ['class' => 'form-control select2', 'required', 'style' => 'width: 100%']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '<input type="text" class="form-control money" name="amount[]" value="'+residual+'") readonly>'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('debit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('debit_object_type'), ['class' => 'form-control select2 debit_object_type']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('debit_object_id[]', ['' => trans('system.dropdown_choice')], old('debit_object_id'), ['class' => 'form-control select2 debit_object_id']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('credit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('credit_object_type'), ['class' => 'form-control select2 credit_object_type']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('credit_object_id[]', ['' => trans('system.dropdown_choice')], old('credit_object_id'), ['class' => 'form-control select2 credit_object_id']) !!}'
                    html += '</td>'
                    html += '<td>'
                    html += '{!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + $cost_items , old('cost_item[]'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}'
                    html += '</td>'
                    html += '</tr>'
                    }
                $(".accounting").append(html)
                $('select[name="credit_account[]"]').val(data.fixed_asset.original_price_account).change()
                $('select[name="debit_account[]"]').val(data.fixed_asset.depreciation_account).change()
                $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                $('.select2').select2({
                    dropdownAutoWidth: true,
                });
                var total_amount = 0;
                        $("input[name='amount[]']").each(function() {
                            var money_amount = $(this).val().replace(/,/g, "");
                            total_amount += Number(money_amount);
                        }); 
                        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            },
            error: function(obj, status, err) {
            }
        })
        }
        
    });
    
    $(document).on('submit', 'form', function(){
        if($("input[name='voucher_number']").val() == ""){
            toastr.error("Enter voucher number");
            return false;
        }
        else if($("input[name='voucher_date']").val() == ""){
            
            toastr.error("Choose voucher date");
            return false;
        }
        else if($("select[name='asset_code']").val() == ""){
            
            toastr.error("Select asset code");
            return false;
        }
        else if($("select[name='disposal_account']").val() == ""){
            
            toastr.error("Select disposal account");
            return false;
        }
        else{
            $('form').submit();
        }
        
        

    })
    $(document).on('change', 'select[name="disposal_account"]', function(){
        $(".debit_account").val($(this).val()).change()
    })
    $(document).on("change", ".debit_object_type",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find((".debit_object_id"));
        var option = $(this).closest('tr').find((".debit_object_id option"));
        $.ajax({
            url: "{!! route('admin.get-debit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
    $(document).on("change", ".credit_object_type",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find((".credit_object_id"));
        var option = $(this).closest('tr').find((".credit_object_id option"));
        $.ajax({
            url: "{!! route('admin.get-credit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
                
</script>
@stop
