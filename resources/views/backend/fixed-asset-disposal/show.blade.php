@extends('backend.master')
@section('title')
{!! trans('system.action.create') !!} - {!! trans('menus.fixed-asset-depreciation.label') !!}
@stop
@section('head')
    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.fixed-asset-depreciation.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.fixed-asset-depreciation.label') !!}</a></li>
        </ol>
    </section>
    
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2 text-right">
                        <b>{!! trans('fixed_asset_disposal.disposal_reason') !!}</b>
                    </div>
                    <div class="col-md-5">
                        {!! Form::text('disposal_reason', old('disposal_reason', $disposal->disposal_reason), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                    <div class="col-md-2 text-right">
                        <b>{!! trans('fixed_asset_disposal.voucher_date') !!}</b>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('voucher_date', old('voucher_date', \Carbon\Carbon::createFromTimestamp(strtotime($disposal->voucher_date))->format('d-m-Y')), ['class' => 'form-control date_picker', 'readonly']) !!}       
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2 text-right" >
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2 text-right">
                        <b>{!! trans('fixed_asset_disposal.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('voucher_number', old('voucher_number', $disposal->voucher_number), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_disposal.disposal_fixed_assets') !!}</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div style="margin-right:20px"> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 100px;">
                                            {!! trans('fixed_assets.asset_code') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 300px;">
                                            {!! trans('fixed_assets.asset_name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.original_price') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.depreciation_value') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.accumulated_depreciation') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.residual_value') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.original_price_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.depreciation_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_disposal.disposal_account') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>
                                        {!! Form::text('asset_code[]', old('asset_code[]', $disposal->asset_code), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('asset_name', old('asset_name', $asset_info->asset_name), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('original_price', old('original_price', $asset_info->original_price), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('depreciation_value', old('depreciation_value', $asset_info->depreciation_value), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('accumulated_depreciation', old('accumulated_depreciation', $accumulated_depreciation), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('residual_value', old('residual_value', $asset_info->residual_value - $accumulated_depreciation), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('original_price_account', old('original_price_account', $asset_info->original_price_account), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('depreciation_account', old('depreciation_account', $asset_info->depreciation_account), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                    <td>
                                    {!! Form::text('disposal_account[]', old('disposal_account[]', $disposal->disposal_account), ['class' => 'form-control', 'readonly']) !!}
                                    </td>
                                </tr>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.accounting') !!}</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">  
                                    <tr>
                                        <th class="text-center" style=" min-width: 300px;">
                                            {!! trans('fixed_asset_depreciation.description') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.debit_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.credit_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.money_amount') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.debit_object') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.object_name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.credit_object') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.object_name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.cost_item') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="accounting">             
                                @foreach($accounting as $item)
                                    <tr>
                                        <td>
                                            {!! Form::text('description[]', old('description[]', $item->description), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('debit_account[]', old('debit_account[]', $item->debit_account), ['class' => 'form-control', 'readonly']) !!}      
                                        </td>
                                        <td>
                                            {!! Form::text('credit_account[]', old('credit_account[]', $item->credit_account), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('amount[]', old('amount[]', $item->amount), ['class' => 'form-control money', 'readonly']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('debit_object_type[]', old('debit_object_type[]', $item->debit_object_type), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td>
                                        @if($item->debit_object_type == "DEPARTMENTS")
                                            {!! Form::text('debit_object_id[]', old('debit_object_id[]', App\Models\Accounting::getDepartmentObject($item->debit_object_id)), ['class' => 'form-control', 'readonly']) !!}
                                        @elseif($item->debit_object_type == "USERS")
                                            {!! Form::text('debit_object_id[]', old('debit_object_id[]', App\Models\Accounting::getUserObject($item->debit_object_id)), ['class' => 'form-control', 'readonly']) !!}
                                        @elseif($item->debit_object_type == "PARTNERS")
                                            {!! Form::text('debit_object_id[]', old('debit_object_id[]', App\Models\Accounting::getPartnerObject($item->debit_object_id)), ['class' => 'form-control', 'readonly']) !!}
                                        @else
                                            {!! Form::text('debit_object_id[]', old('debit_object_id[]'), ['class' => 'form-control', 'readonly']) !!}
                                        @endif
                                        </td>
                                        <td style="width:10%">
                                            {!! Form::text('credit_object_type[]', old('credit_object_type', $item->credit_object_type), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        <td style="width:10%">
                                        @if($item->credit_object_type == "DEPARTMENTS")
                                            {!! Form::text('credit_object_id[]', old('credit_object_id[]', App\Models\Accounting::getDepartmentObject($item->credit_object_id)), ['class' => 'form-control', 'readonly']) !!}
                                        @elseif($item->credit_object_type == "USERS")
                                            {!! Form::text('credit_object_id[]', old('credit_object_id[]', App\Models\Accounting::getUserObject($item->credit_object_id)), ['class' => 'form-control', 'readonly']) !!}
                                        @elseif($item->credit_object_type == "PARTNERS")
                                            {!! Form::text('credit_object_id[]', old('credit_object_id[]', App\Models\Accounting::getPartnerObject($item->credit_object_id)), ['class' => 'form-control', 'readonly']) !!}
                                        @else
                                            {!! Form::text('credit_object_id[]', old('credit_object[]'), ['class' => 'form-control', 'readonly']) !!}
                                        @endif
                                        </td>
                                        <td>
                                            {!! Form::text('cost_item[]', old('cost_item[]', $item->cost_item), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                        </tr>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="3" style="font-size:18px" >
                                            {!! trans('fixed_asset_depreciation.total') !!}:
                                        </th>
                                        <th class="text-right" style="font-size:18px" id="total" >
                                        </th> 
                                        <th colspan="4" >
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
   
    $('.select2').select2({
        dropdownAutoWidth: true,
    });
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
    
</script>
<script>
    var total_amount = 0;
                        $("input[name='amount[]']").each(function() {
                            var money_amount = $(this).val().replace(/,/g, "");
                            total_amount += Number(money_amount);
                        }); 
                        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
</script>
@stop
