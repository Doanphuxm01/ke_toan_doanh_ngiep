@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('partner_requests.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('partner_requests.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.partner-requests.index') !!}">{!! trans('partner_requests.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.partner-requests.update', $request->id), 'method' => 'PUT', 'role' => 'form']) !!}
        <table class='table bfeedbackless' style="width: 80%;">
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.label') !!}
                </th>
                <td>
                    {!! Form::text('fullname', old('fullname', $customer->fullname), ['class' => 'form-control', 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.level') !!} hiện tại
                </th>
                <td>
                    {!! Form::select('level', App\Define\Customer::getLevelsForOption(), old('level', $customer->level), ['class' => 'form-control select2', 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customers.level') !!} yêu cầu
                </th>
                <td>
                    {!! Form::select('level', App\Define\Customer::getLevelsForOption(), old('level', $request->target), ['class' => 'form-control select2', 'disabled']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                </th>
                <td>
                    {!! Form::select('status',[0 => 'Chưa xử lý', 1 => 'Chấp nhận', -1 => 'Từ chối'], old('status'), ['class' => 'form-control select2']) !!}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="text-center">
                    {!! HTML::link(route( 'admin.partner-requests.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat'])!!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
            });
        }(window.jQuery);
    </script>
@stop