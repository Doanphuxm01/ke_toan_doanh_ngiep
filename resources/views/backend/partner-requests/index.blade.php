@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('partner_requests.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('partner_requests.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.partner-requests.index') !!}">{!! trans('partner_requests.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bfeedback">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.partner-requests.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('customer', trans('customers.label')) !!}
                        {!! Form::select('customer', empty($customer) ? ['' => trans('system.dropdown_all')] : $customer, Request::input('customer'), ["class" => "form-control"]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [-2 => trans('system.dropdown_all'), 0 => 'Chưa cập nhật', 1 => 'Đã duyệt', -1 => 'Từ chối'], Request::input('status'), ['class' => 'form-control select2',  "style" => "width: 100%;"]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('page_num', trans('system.page_num')) !!}
                        {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2',  "style" => "width: 100%;"]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label')) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 text-right">
            {!! $requests->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($requests) > 0)
    <div class="box">
        <div class="box-header">
            <?php $i = (($requests->currentPage() - 1) * $requests->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $requests->count()) . ' ( ' . trans('system.total') . ' ' . $requests->total() . ' )' !!} |
                    <i>Chú giải: </i>&nbsp;&nbsp;
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </span>
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                        <th style="text-align: center1; vertical-align: middle;"> {!! trans('customers.label') !!}</th>
                        <th style="text-align: center1; vertical-align: middle;"> {!! trans('customers.level') !!} hiện tại</th>
                        <th style="text-align: center1; vertical-align: middle;"> {!! trans('customers.level') !!} yêu cầu</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!} </th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!}</th>
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($requests as $item)
                        <?php $customer = App\Customer::find($item->customer_id); if (is_null($customer)) continue; ?>
                        <tr>
                            <td style="text-align: center; vertical-align: middle;">{!! $i++ !!}</td>
                            <td style="text-align: justify; vertical-align: middle;">
                                <a href="{!! route('admin.customers.edit', $customer->id) !!}" target="_blank">{!! $customer->fullname !!}</a>
                            </td>
                            <td style="vertical-align: middle;">
                                @if($item->status == 1)
                                    {!! App\Define\Customer::getColorLevels($item->old) !!}
                                @else
                                    {!! App\Define\Customer::getColorLevels($customer->level) !!}
                                @endif
                            </td>
                            <td style="vertical-align: middle;">
                                {!! App\Define\Customer::getColorLevels($item->target) !!}
                            </td>
                            <td style="width: 10%; text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                    <span class="label label-warning">Chưa cập nhật</span>
                                @elseif($item->status == 1)
                                    <span class="label label-success">Đã duyệt</span>
                                @elseif($item->status == -1)
                                    <span class="label label-danger">Từ chối</span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if ($item->status == 0)
                                <a href="{!! route('admin.partner-requests.edit',$item->id) !!}" class="btn btn-xs btn-default"><i class="text-warning glyphicon glyphicon-edit"></i></a>&nbsp;&nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.partner-requests.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='customer']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    width: '100%',
                    allowClear: true,
                    minimumResultsForSearch: 10,
                    ajax: {
                        url: "{!! route('admin.customers.search') !!}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    return {
                                        text: item.code + " - " + item.fullname,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    }
                }).on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    $(this).closest('form').submit();
                });
            });
        }(window.jQuery);
    </script>
@stop