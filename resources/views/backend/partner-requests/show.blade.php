@extends('backend.master')

@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('partner_requests.label') !!}
@stop

@section('content')
    <section class="content-header">
        <h1>
            {!! trans('partner_requests.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.partner-requests.index') !!}">{!! trans('partner_requests.label') !!}</a></li>
        </ol>
    </section>
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        @if($feedback->type == \App\Define\Constant::FEEDBACK_TYPE_USER)
            <table class='table table-borderless'>
                <tr>
                    <th class="table_right_middle" style="width: 15%;">
                        {!! trans('partner_requests.fullname') !!}
                    </th>
                    <td style="width: 35%;">
                        {!! $feedback->fullname !!}
                    </td>
                    <th class="table_right_middle" style="width: 15%;">
                        {!! trans('partner_requests.email') !!}
                    </th>
                    <td>
                        {!! $feedback->email !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.phone') !!}
                    </th>
                    <td>
                        {!! $feedback->phone !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.types.label') !!}
                    </th>
                    <td colspan="3">
                        {!! $feedback->subject !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.content') !!}
                    </th>
                    <td colspan="3">
                        {!! $feedback->content !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('system.status.label') !!}
                    </th>
                    <td>
                        @if($feedback->status == 0)
                            <span class="label label-danger">Chưa trả lời</span>
                        @elseif($feedback->status == 1)
                            <span class="label label-success">Đã trả lời</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('system.created_at') !!}
                    </th>
                    <td>
                        {!! date("d-m-Y", strtotime($feedback->updated_at)) !!}
                    </td>
                    <th class="table_right_middle">
                        {!! trans('system.updated_at') !!}
                    </th>
                    <td>
                        {!! date("d-m-Y", strtotime($feedback->updated_at)) !!}
                    </td>
                </tr>
            </table>
        @else
            <table class='table table-borderless'>
                <tr>
                    <th class="table_right_middle" style="width: 15%;">
                        {!! trans('partner_requests.com_name') !!}
                    </th>
                    <td style="width: 35%;">
                        {!! $feedback->fullname !!}
                    </td>
                    <th class="table_right_middle" style="width: 15%;">
                        {!! trans('partner_requests.com_phone') !!}
                    </th>
                    <td>
                        {!! $feedback->phone !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.com_contact_name') !!}
                    </th>
                    <td>
                        {!! $feedback->contact_name !!}
                    </td>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.com_contact_phone') !!}
                    </th>
                    <td>
                        {!! $feedback->contact_phone !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.com_address') !!}
                    </th>
                    <td colspan="3">
                        {!! $feedback->com_address !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.com_product') !!}
                    </th>
                    <td colspan="3">
                        {!! $feedback->com_product !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('partner_requests.content') !!}
                    </th>
                    <td colspan="3">
                        {!! $feedback->content !!}
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('system.status.label') !!}
                    </th>
                    <td>
                        @if($feedback->status == 0)
                            <span class="label label-danger">Chưa trả lời</span>
                        @elseif($feedback->status == 1)
                            <span class="label label-success">Đã trả lời</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="table_right_middle">
                        {!! trans('system.created_at') !!}
                    </th>
                    <td>
                        {!! date("d-m-Y", strtotime($feedback->updated_at)) !!}
                    </td>
                    <th class="table_right_middle">
                        {!! trans('system.updated_at') !!}
                    </th>
                    <td>
                        {!! date("d-m-Y", strtotime($feedback->updated_at)) !!}
                    </td>
                </tr>
            </table>
        @endif
    </div>
@stop