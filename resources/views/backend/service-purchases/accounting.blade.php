<div class="row">
    <div class="col-md-12" >
                <div class="box-body">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class='table table-bordered' id="table_accounting">
                                    <tr style="background: #3C80BC; color:white">
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('system.action.label') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans('accounting.sale_code_item') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 180px; text-align: center;">
                                            <b>{!! trans('accounting.description') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans("accounting.job_code") !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans("accounting.debit_account") !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 120px; text-align: center;">
                                            <b>{!! trans("accounting.credit_account") !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.quantity') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.price_unit') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.amount') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 90px; text-align: center;">
                                            <b>{!! trans('accounting.exchange') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap;">
                                            <b>{!! trans('accounting.branch_code') !!}</b>
                                        </th>
                                        <th style="width: 25%; white-space: nowrap; min-width: 100px;">
                                            <span class="for_mawb">{!! trans('input_invoices.mawb') !!}</span>
                                        </th>
                                        <th style="white-space: nowrap; min-width: 100px;">
                                            <span class="for_hawb">{!! trans('input_invoices.hawb') !!}</span>
                                        </th>
                                    </tr>
                                <?php $description = old('description', []); $totalAmount = 0;?>
                                @if(count($description))
                                    <?php
                                        $job_code         = old('job_code');
                                        $debit_account    = old('debit_account');
                                        $credit_account   = old('credit_account');
                                        $price_unit       = old('price_unit');
                                        $exchange         = old('exchange');
                                        $branchCode       = old('branch_code');
                                        $totalAmount      = array_sum($price_unit);
                                        $total_exchange   = array_sum($exchange);
                                        $codeItem         = old('sale_code_item');
                                        $amount           = old('amount');
                                        $quantity         = old('quantity');
                                        $totalQuantity    = array_sum($quantity); 
                                        $total_amount     = array_sum($amount);
                                        $total_price_unit = array_sum($price_unit);
                                        $mawb             = old('mawb');
                                        $hawb             = old('hawb');
                                        session(['total_amount' => $total_amount]);
                                    ?>
                                    @for ($i = 0; $i < count($description); $i++)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                @if ($i > 0)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>                                                &nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("sale_code_item[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item[$i]", $codeItem[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("description[]", old('description[]'), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("job_code[]", [' '=> trans('system.dropdown_choice')] + $jobs, old("job_code[$i]", $job_code[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("debit_account[]", old("debit_account[$i]", $debit_account[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("credit_account[]", [' '=> trans('system.dropdown_choice')] + $accounts , old("credit_account[$i]", $credit_account[$i] ), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('quantity[]', old('quantity[]'), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("price_unit[]", old("price_unit[]", $price_unit[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('amount[]', old('amount[]', $amount[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange[]", old("exchange[]", $exchange[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                                {!! Form::select("branch_code[]", [' '=> trans('system.dropdown_choice')] + $branchCodes, old("branch_code[$i]",  $branchCode[$i]), ['class' => 'form-control select2', 'data-container'=>'__"body"']) !!}
                                            </td>
                                            <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                                                {!! Form::text('mawb[]', old('mawb[]', $mawb[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 100px;">
                                                {!! Form::text('hawb[]', old('hawb[]', $hawb[$i]), ['class' => 'form-control']) !!}
                                            </td>    
                                        </tr>
                                    @endfor
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-accounting text-aqua" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                           
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                           
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            {!! trans('input_invoices.total') !!} &nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center" >
                                            {!! Form::text('total_quantity', old('total_quantity', App\Helper\HString::decimalFormat($totalQuantity)), ['class' => 'form-control currency', 'id'=>"total_quantity", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                            {!! Form::text('total_price_unit', old('total_price_unit', App\Helper\HString::decimalFormat($total_price_unit)), ['class' => 'form-control currency', 'id'=>"total_with_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                            {!! Form::text('total_amount', old('total_amount', App\Helper\HString::decimalFormat($total_amount)), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                            {!! Form::text('total_exchange', old('total_exchange', App\Helper\HString::decimalFormat($total_exchange)), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @elseif (count($editServicePurchase->accounting))
                                    @foreach($editServicePurchase->accounting as $item)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;" id={{$i++}}>
                                                @if ($i > 1)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>                                                &nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("sale_code_item[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item[]", $item->sale_code_item_id), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('description[]', old('description[]', $item->description), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]', $item->job_code_id), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('debit_account[]',  old('debit_account[]', $item->debit_account), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select('credit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]', $item->credit_account), ['class' => 'form-control select2']) !!}                                            
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('quantity[]', old('quantity[]', $item->quantity), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('price_unit[]', old('with_amount[]', $item->price_unit), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('amount[]', old('amount[]', $item->amount), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text('exchange[]', old('exchange[]', $item->exchange), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]', $item->branch_code_id), ['class' => 'form-control select2']) !!} 
                                            </td>
                                            <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                                                {!! Form::text('mawb[]', old('mawb[]'), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 100px;">
                                                {!! Form::text('hawb[]', old('hawb[]'), ['class' => 'form-control']) !!}
                                            </td>   
                                        </tr>
                                    @endforeach
                                    <tr>
                                        {{-- <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            <a href="javascript:void(0);" class="add-row-accounting text-aqua" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a>
                                        </td> --}}
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">

                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            {!! trans('input_invoices.total') !!} &nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right" >
                                            {!! Form::text('total_quantity', old('total_quantity', App\Helper\HString::decimalFormat($editServicePurchase->total_quantity_accounting) ), ['class' => 'form-control currency', 'id'=>"total_quantity", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap; text-align:right"  >
                                            {!! Form::text('total_amount', old('total_amount', App\Helper\HString::decimalFormat($editServicePurchase->total_price_unit_accounting) ), ['class' => 'form-control currency', 'id'=>"total_with_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap; text-align:right">
                                            {!! Form::text('total_with_amount', old('total_with_amount', App\Helper\HString::decimalFormat($editServicePurchase->total_amount_accounting) ), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap; text-align:right">
                                            {!! Form::text('total_exchange', old('total_exchange', App\Helper\HString::decimalFormat($editServicePurchase->total_exchange_accounting) ), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <p style="vertical-align: middle; text-align: center;">
                                                <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-accounting text-success"><i class="fas fa-copy"></i></a>
                                            </p>
                                        </td>
                                        <td style="white-space: nowrap; min-width:200px;padding:0px">
                                            {!! Form::select("sale_code_item[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item[]"), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('description[]', old('description[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select('job_code[]', [' '=> trans('system.dropdown_choice')] + $jobs, old('job_code[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('debit_account[]',  old('debit_account[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select('credit_account[]', [' '=> trans('system.dropdown_choice')] + $accounts, old('credit_account[]'), ['class' => 'form-control select2', "min-width" => "200px", 'display'=>'inline-block']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('quantity[]', old('quantity[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('price_unit[]', old('price_unit[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('amount[]', old('amount[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text('exchange[]', old('exchange[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 150px;">
                                            {!! Form::select('branch_code[]', [' '=> trans('system.dropdown_choice')] + $branchCodes, old('branch_code[]'), ['class' => 'form-control select2']) !!} 
                                        </td>
                                        <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                                            {!! Form::text('mawb', old('mawb'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 100px;">
                                            {!! Form::text('hawb[]', old('hawb[]'), ['class' => 'form-control']) !!}
                                        </td>   
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            {{-- <a href="javascript:void(0);" class="add-row-accounting text-aqua" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a> --}}
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                           
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                           
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                        
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:right">
                                            {!! trans('input_invoices.total') !!} &nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px; text-align:center" >
                                            {!! Form::text('total_quantity', old('total_quantity', App\Helper\HString::decimalFormat($totalQuantity)), ['class' => 'form-control currency', 'id'=>"total_quantity", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;"  id="">
                                            {!! Form::text('total_with_amount', old('total_with_amount', App\Helper\HString::decimalFormat($total_with_amount)), ['class' => 'form-control currency', 'id'=>"total_with_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;"  >
                                            {!! Form::text('total_amount', old('total_amount', App\Helper\HString::decimalFormat($total_amount)), ['class' => 'form-control currency', 'id'=>"total_amount", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                            {!! Form::text('total_exchange', old('total_exchange', App\Helper\HString::decimalFormat($total_exchange)), ['class' => 'form-control currency', 'id'=>"total_exchange", 'readonly' => 'true']) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            <br>
                        </div >
                    </div>
                </div>
        </div>
    </div>
</div>
<script>
    !function ($) {
        $(function() {
            // $(document).on('click', '.add-row-accounting', function(event) {
               
            //     tr =  $(this).closest('tr').prev();
            //     indexTax = tr.index();
            //     console.log('accounting'+indexTax);
            //     tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-accounting", compact("branchCodes", "vendor", "itemCode", "jobs", "accounts", "currency", "financial_paper"))->render()) !!}');
            //     addRowTaxByAccounting(indexTax);
            //     select2Tab();
            //     enterTab();
            //     caculatorExchange();
            //     inputDecima();
            // });
            
            $(document).on('click', '.remove-row-accounting', function(event) {
                var length = $(this).closest('tbody').find('tr').length;
                var indexCurrent = $(this).closest('tr').index();
                if (length <= 3) {
                    $(this).closest('.box').fadeOut(300, function(){ $(this).remove();});
                } else {
                    $(this).closest('tr').fadeOut(150, function() { 
                        $(this).remove();
                        clearRowTaxByAccounting(indexCurrent);
                        caculatorExchange();
                    });
                }
            });
            $(document).on('click', '.clone-row-accounting', function(event) {  
                 
                var tr = $(this).closest('tr');
                tr.find("select").select2('destroy');
                var job_code = tr.find('select[name="job_code[]"]').select2().val();
                var branch_code = tr.find('select[name="branch_code[]"]').select2().val();
                // var tr1 = tr.find()
                // tr.find("select").each(function() {
                //     var name = $(this).attr('name');
                //     tr1.find("select[name='"+ name + "']").val($(this).select2().val());
                // });
                            
                // if (!tr1.find('a').hasClass('remove-row-accounting')) {
                //     tr1.find('.clone-row-accounting').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>');
                // }
                tr_next = tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-accounting", compact("branchCodes", "vendor", "itemCode", "jobs", "accounts", "currency", "financial_paper"))->render()) !!}');
                $('tr:eq('+Number(tr.index()+1)+')').find('select[name="job_code[]"]').val(job_code).change();
                $('tr:eq('+Number(tr.index()+1)+')').find('select[name="branch_code[]"]').val(branch_code);
                cloneRowTaxByAccounting(tr.index())
                caculatorExchange();
                enterTab();
                select2Tab();
                callInputMaskDecimal();
                renderTableS2('select[name="vendor"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllVendor);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
            });
        });
    }(window.jQuery);
    function cloneRowAccountingByTax(index){
        
        var tr = $('#table_accounting').find('tr:eq('+index+')');
        tr.find("select").select2('destroy');
        // var tr1 = tr.clone();
        // tr.find("select").each(function() {
        //     var name = $(this).attr('name');
        //     tr1.find("select[name='"+ name + "']").val($(this).select2().val());
        // });
        // if (!tr1.find('a').hasClass('remove-row-accounting')) {
        //     tr1.find('.clone-row-accounting').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>');
        // }
        // $(tr).after(tr1);
        var job_code = tr.find('select[name="job_code[]"]').select2().val();
        var branch_code = tr.find('select[name="branch_code[]"]').select2().val();
        tr_next = tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-accounting", compact("branchCodes", "vendor", "itemCode", "jobs", "accounts", "currency", "financial_paper"))->render()) !!}');
        $('tr:eq('+Number(tr.index()+1)+')').find('select[name="job_code[]"]').val(job_code).change();
        $('tr:eq('+Number(tr.index()+1)+')').find('select[name="branch_code[]"]').val(branch_code);
        renderTableS2('select[name="vendor"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllVendor);
        renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
        dateInvoice();
        caculatorExchange();
        callInputMaskDecimal();
    }
    function clearRowAccountingByTax(index){
        var tr = $('#table_accounting').find('tr:eq('+index+')');
        $(tr).remove();
        caculatorExchange();
        callInputMaskDecimal();
    }

    // function addRowAccountingByTax(index){
    //     var tr = $('#table_accounting').find('tr:eq('+index+')');
    //     tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-accounting", compact("branchCodes", "vendor", "itemCode", "jobs", "accounts", "currency", "financial_paper"))->render()) !!}');
    // }

    function dateInvoice(){
        $('input[name="date_invoice[]"]').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            autoclose: true,
        })
    }
   
    function caculatorExchange() {
        var total_with_amount = 0;
        var total_amount = 0;
        var total_quantity = 0;
        var total_exchange = 0;
        var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
        $("input[name='quantity[]']").each(function() {
            var tr = $(this).closest('tr');
            var quantity = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            var with_amount = (parseFloat(tr.find('input[name="price_unit[]"]').val().replace(/,/g, ''))|| 0);
            var td_total_amount = tr.find('input[name="amount[]"]');
            var td_exchange = tr.find('input[name="exchange[]"]');
            td_total_amount.val(with_amount*quantity).inputmask();
            total_with_amount += with_amount;
            total_quantity += quantity;
            total_amount += with_amount*quantity;
            td_exchange.val(with_amount*quantity*exchange_rate).inputmask()
            total_exchange += Number(with_amount*quantity*exchange_rate);
            $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $("#total_quantity").val( total_quantity.toFixed(2)).inputmask();
            $("#total_with_amount").val(total_with_amount.toFixed(2)).inputmask();
            $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
            $("#total_amount").val(total_amount.toFixed(2)).inputmask();
        })
    }
</script>