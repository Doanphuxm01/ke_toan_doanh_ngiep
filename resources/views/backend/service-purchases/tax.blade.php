<div class="row">
    <div class="col-md-12">
            <div class="box-body">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="table-responsive" style="z-index: 12 !important">
                            <table  id="table_tax" class='table table-bordered'>
                                    <tr style="background: #3C80BC; color:white">
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('system.action.label') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; min-width: 120px; text-align: center;">
                                            <b>{!! trans('accounting.sale_code_item') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; min-width: 120px; text-align: center;">
                                            <b>{!! trans('taxes.description') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; min-width: 120px; text-align: center;">
                                            <b>{!! trans('taxes.explain') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.account_tax') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; min-width: 180px; text-align: center;">
                                            <b>{!! trans('taxes.tax_rate.label') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; min-width: 120px; text-align: center;">
                                            <b>{!! trans("taxes.taxation") !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; min-width: 90px; text-align: center;">
                                            <b>{!! trans('taxes.exchange_taxation') !!}</b>
                                        </th>
                                        {{-- <td style="white-space: nowrap; min-width: 90px;">
                                            {!! trans('taxes.unit_price') !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            {!! trans('taxes.exchange') !!}
                                        </td> --}}
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.group_unit') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.invoice_no') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.date_invoice') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.object_code') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.object_name') !!}</b>
                                        </th>
                                        <th style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <b>{!! trans('taxes.tax_code') !!}</b>
                                        </th>
                                    </tr>
                              
                                <?php $description_tax = old('description_tax', []); $total_taxation = 0; $total_exchange_tax = 0; $total_service_value = 0; $total_exchange_service = 0;?>
                                @if(count($description_tax))
                                    <?php
                                        $tax_rate = old('tax_rate');
                                        $taxation = old('taxation');
                                        $exchange_tax = old('exchange_tax');
                                        $codeItem = old('sale_code_item_tax');
                                        $explain_tax = old('explain_tax');
                                        $service_value = old('unit_price');
                                        $exchange_service = old('exchange_price_unit');
                                        $account_tax = old('account_tax');
                                        $invoice_no = old('invoice_no');
                                        $date_invoice = old('date_invoice');
                                        $group_unit_tax = old('group_unit');
                                        $object_code_tax = old('object_code_tax');
                                        $object_name_tax = old('object_name_tax');
                                        $account_tax = old('account_tax');
                                        $tax_code = old('tax_code');
                                        $total_taxation = array_sum($taxation);
                                        $total_exchange_tax = array_sum($exchange_tax);
                                        $total_service_value = array_sum($service_value);
                                        $total_exchange_service = array_sum($exchange_service);
                                        session(['total_taxation' => $total_taxation]);
                                    ?>
                                    @for($i = 0; $i< count($description_tax); $i++)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                @if ($i > 0)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-tax"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-tax"><i class="fas fa-copy"></i></a>&nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200x;">
                                                {!! Form::select("sale_code_item_tax[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item_tax[$i]", $codeItem[$i]), ['class' => 'form-control select2 sale_code_item_tax', 'disabled']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200x;">
                                                {!! Form::text("description_tax[]", old('description_tax[]', $description_tax[$i]), ['class' => 'form-control', 'readonly']) !!}
                                            </td>
                                            <td>
                                                {!! Form::text("explain_tax[]", old('explain_tax[]', $explain_tax[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]', $account_tax[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("taxation[]", old('taxation[]', $taxation[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_tax[]", old('exchange_tax[]',  $exchange_tax[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            {{-- <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("unit_price[]", old('unit_price[]', $service_value[$i]), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]', $exchange_service[$i]), ['class' => 'form-control currency'  ]) !!}
                                            </td> --}}
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("group_unit[]",[' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]', $group_unit_tax[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("invoice_no[]", old('invoice_no[]', $invoice_no[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("date_invoice[]",  old('date_invoice[]', $date_invoice[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;" class="object">
                                                {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')] + $partnerVendorCodes, old("object_code_tax[$i]", $object_code_tax[$i]), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("object_name_tax[]",  old("object_name[$i]", $object_name_tax[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("tax_code[]", old('tax_code[]', $tax_code[$i]), ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                    @endfor
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            {{-- <a href="javascript:void(0);" class="add-row-tax text-aqua" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a> --}}
                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td>

                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: right;">
                                            {!! trans('input_invoices.total') !!}  &nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                            {!! Form::text("total_taxation", old('total_taxation', App\Helper\HString::decimalFormat($total_taxation )), ['class' => 'form-control currency', 'id'=>"total_taxation"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_tax", old('total_exchange_tax', App\Helper\HString::decimalFormat($total_exchange_tax)), ['class' => 'form-control currency', 'id'=>"total_exchange_tax"]) !!}
                                        </td>
                                        {{-- <th style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_service_value", old('total_service_value', App\Helper\HString::currencyFormat($total_service_value)), ['class' => 'form-control currency', 'id'=>"total_service_value"]) !!}
                                        </th>
                                        <th style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_service", old('total_exchange_service', App\Helper\HString::currencyFormat($total_exchange_service)), ['class' => 'form-control currency', 'id'=>"total_exchange_service"]) !!}
                                        </th> --}}
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @elseif (count($editServicePurchase->taxes))
                                    @foreach ($editServicePurchase->taxes as $item)
                                        <tr>
                                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;" id={{$i++}}>
                                                @if ($i > 1)
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-tax"><i class="fas fa-copy"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>
                                                    </p>
                                                @else
                                                    <p style="vertical-align: middle; text-align: center;">
                                                        <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>&nbsp;&nbsp;
                                                    </p>
                                                @endif
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200x;">
                                                {!! Form::select("sale_code_item_tax[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item_tax[]", $item->sale_code_item_id), ['class' => 'form-control select2 sale_code_item_tax', 'disabled']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200x;">
                                                {!! Form::text("description_tax[]", old('description_tax[]', $item->description), ['class' => 'form-control', "readonly"]) !!}
                                            </td>
                                            <td>
                                                {!! Form::text("explain_tax[]", old('explain_tax[]', $item->explain_tax), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]', $item->account_tax), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]', $item->tax_rate), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("taxation[]", old('taxation[]', $item->taxation), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap;min-width: 200px;">
                                                {!! Form::text("exchange_tax[]", old('exchange_tax[]', $item->exchange_tax), ['class' => 'form-control currency']) !!}
                                            </td>
                                            {{-- <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("unit_price[]", old('unit_price[]', $item->unit_price), ['class' => 'form-control currency']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]', $item->exchange_unit), ['class' => 'form-control currency'  ]) !!}
                                            </td> --}}
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]', $item->group_unit_buy), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("invoice_no[]", old('invoice_no[]', $item->invoice_no), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("date_invoice[]",  old('date_invoice[]',date("d/m/Y", strtotime(str_replace('-', '/', $item->invoice_date)))), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;" class="object"> 
                                                {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice'), $item->partner_id=>""], old('object_code[]'), ['class' => 'form-control select2']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("object_name_tax[]", old('object_name_tax[]', $item->partner_name), ['class' => 'form-control']) !!}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 200px;">
                                                {!! Form::text("tax_code[]", old('tax_code[]', $item->tax_code), ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>
                                        <script>
                                            console.log($('select[name="object_code_tax[]"]').closest('div').find('.select2-selection__rendered'));
                                               $('select[name="object_code_tax[]"]').closest('div').find('.select2-selection__rendered').html('{!! App\Models\Partner::getInfoPartner($item->partner_id)->code !!}');
                                        </script>
                                    @endforeach
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            {{-- <a href="javascript:void(0);" class="add-row-tax btn btn-info btn-sm btn-flat" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a> --}}
                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="3">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! trans('input_invoices.total') !!} &nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_taxation", old('total_taxation', App\Helper\HString::decimalFormat($editServicePurchase->total_taxation )), ['class' => 'form-control currency', 'id'=>"total_taxation"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_tax", old('total_exchange_tax', App\Helper\HString::decimalFormat($editServicePurchase->total_exchange_taxation)), ['class' => 'form-control currency', 'id'=>"total_exchange_tax"]) !!}
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <p style="vertical-align: middle; text-align: center;">
                                                <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-tax text-success"><i class="fas fa-copy"></i></a>
                                            </p>
                                        </td>
                                        <td>
                                            {!! Form::select("sale_code_item_tax[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item_tax[]"), ['class' => 'form-control select2 sale_code_item_tax', 'disabled']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("description_tax[]", old('description_tax[]'), ['class' => 'form-control', "readonly"]) !!}
                                        </td>
                                        <td style="white-space: nowrap;min-width: 200px;">
                                            {!! Form::text("explain_tax[]", old('explain_tax[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;padding:0px">
                                            {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("taxation[]", old('taxation[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap;min-width: 200px;">
                                            {!! Form::text("exchange_tax[]", old('exchange_tax[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        {{-- <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("unit_price[]", old('unit_price[]'), ['class' => 'form-control currency']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]'), ['class' => 'form-control currency'  ]) !!}
                                        </td> --}}
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]'), ['class' => 'form-control select2']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("invoice_no[]", old('invoice_no[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("date_invoice[]",  old('date_invoice[]', date('d/m/Y')), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;" class="object">
                                            {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')], old("object_code_tax[]"), ['class' => 'form-control select2 object_code_tax']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("object_name_tax[]", old('object_name_tax[]'), ['class' => 'form-control']) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px;">
                                            {!! Form::text("tax_code[]", old('tax_code[]'), ['class' => 'form-control']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                            {{-- <a href="javascript:void(0);" class="add-row-tax text-aqua" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                                <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                            </a> --}}
                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td>

                                        </td>
                                        <td style="white-space: nowrap; vertical-align: middle; text-align: right;">
                                            {!! trans('input_invoices.total') !!}  &nbsp;&nbsp;
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;" >
                                            {!! Form::text("total_taxation", old('total_taxation', App\Helper\HString::decimalFormat($total_taxation )), ['class' => 'form-control currency', 'id'=>"total_taxation"]) !!}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_tax", old('total_exchange_tax', App\Helper\HString::decimalFormat($total_exchange_tax)), ['class' => 'form-control currency', 'id'=>"total_exchange_tax"]) !!}
                                        </td>
                                        {{-- <th style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_service_value", old('total_service_value', App\Helper\HString::currencyFormat($total_service_value)), ['class' => 'form-control currency', 'id'=>"total_service_value"]) !!}
                                        </th>
                                        <th style="white-space: nowrap; min-width: 200px; text-align: right;">
                                            {!! Form::text("total_exchange_service", old('total_exchange_service', App\Helper\HString::currencyFormat($total_exchange_service)), ['class' => 'form-control currency', 'id'=>"total_exchange_service"]) !!}
                                        </th> --}}
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                        <td style="white-space: nowrap;">
                                        
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<script>
    !function ($) {
        $(function() {
            // $(document).on('click', '.add-row-tax', function(event) {
            //     tr =  $(this).closest('tr').prev();
            //     indexAccounting = tr.index();
            //     tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-tax", compact("jobs", "branchCodes", "accounts", "partnerVendorCodes", "group_unit", "itemCode"))->render()) !!}');
            //     addRowAccountingByTax(indexAccounting);
            //     enterTab();
            //     select2Tab();
            //     dateInvoice();
            //     caculateMoney();
            //     inputDecima();
            // });
       
            $(document).on('click', '.remove-row-tax', function(event) {
                var length = $(this).closest('tbody').find('tr').length;
                var indexCurrent = $(this).closest('tr').index();
                if (length <= 3) {
                    $(this).closest('.box').fadeOut(300, function(){ $(this).remove();});
                } else {
                    $(this).closest('tr').fadeOut(150, function(){ 
                        $(this).remove();
                        clearRowAccountingByTax(indexCurrent);
                        caculatorExchange();
                        caculateMoney();
                        inputDecima();
                    });
                }
            });
            $(document).on('click', '.clone-row-tax', function(event) {
                var tr = $(this).closest('tr');
                tr.find("select").select2('destroy');
                var invoice_no = tr.find('input[name="invoice_no[]"]').val();
                var invoice_date = tr.find('input[name="date_invoice[]"]').val();
                var group_unit = tr.find('select[name="group_unit[]"]').select2().val();
                // $('select[name="object_code_tax[]"]').select2();
                //var tr1 = tr.clone();
                // tr.find("select").each(function() {
                //     var name = $(this).attr('name');
                //     tr1.find("select[name='"+ name + "']").val($(this).select2().val());
                // });
                // if (!tr1.find('a').hasClass('remove-row-tax')) {
                //     tr1.find('.clone-row-tax').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>');
                // }
                tr_next = tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-tax", compact("jobs", "branchCodes", "accounts", "partnerVendorCodes", "group_unit", "itemCode"))->render()) !!}');
                $('#table_tax').find('tr:eq('+Number(tr.index()+1)+')').find('input[name="invoice_no[]"]').val(invoice_no);
                $('#table_tax').find('tr:eq('+Number(tr.index()+1)+')').find('input[name="date_invoice[]"]').val(invoice_date);
                $('#table_tax').find('tr:eq('+Number(tr.index()+1)+')').find('select[name="group_unit[]"]').val(group_unit);
                cloneRowAccountingByTax(tr.index());
                enterTab();
                select2Tab();
                renderTableS2('select[name="vendor"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllVendor);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
                dateInvoice();
                caculateMoney();
                callInputMaskDecimal()
            });
            
        });
    }(window.jQuery);
    function cloneRowTaxByAccounting(index){
        var tr = $('#table_tax').find('tr:eq('+index+')');
        tr.find("select").select2('destroy');
        var invoice_no = tr.find('input[name="invoice_no[]"]').val();
        var invoice_date = tr.find('input[name="invoice_date[]"]').val();
        var group_unit = tr.find('select[name="group_unit[]"]').select2().val();
        // tr.find("select").each(function() {
        //     var name = $(this).attr('name');
        //     tr1.find("select[name='"+ name + "']").val($(this).select2().val());
        // });
        // if (!tr1.find('a').hasClass('remove-row-tax')) {
        //     tr1.find('.clone-row-tax').parent('p').append('&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" tabindex="-1" class="text-danger remove-row-tax"><i class="fas fa-minus"></i></a>');
        // }
        tr_next = tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-tax", compact("jobs", "branchCodes", "accounts", "partnerVendorCodes", "group_unit", "itemCode"))->render()) !!}');
        $('tr:eq('+Number(tr.index()+1)+')').find('input[name="invoice_no[]"]').val(invoice_no);
        $('tr:eq('+Number(tr.index()+1)+')').find('input[name="invoice_date[]"]').val(invoice_date);
        $('tr:eq('+Number(tr.index()+1)+')').find('select[name="group_unit[]"]').val(group_unit).change();
        renderTableS2('select[name="vendor"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllVendor);
        renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
        // $(tr).after(tr1);
        dateInvoice();
        caculateMoney();
        callInputMaskDecimal()
    }
    function caculateMoney() {
        var totalTaxation = 0;
        var totalExchangeTax = 0;
        var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
        $('input[name="taxation[]"]').each(function(e){
            tr = $(this).closest('tr');
            taxation = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            totalTaxation += Number(taxation);
            td_exchange_tax = tr.find('input[name="exchange_tax[]"]');
            td_exchange_tax.val(Number(exchange_rate)*Number(taxation)).inputmask()
        });
        $('input[name="exchange_tax[]"]').each(function(e){
            exchange_tax = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            totalExchangeTax += Number(exchange_tax);
        });
        $('#total_taxation').val(totalTaxation.toFixed(2)).inputmask();
        $('#total_exchange_tax').val(totalExchangeTax.toFixed(2)).inputmask();
    }
    function clearRowTaxByAccounting(index){
        var tr = $('#table_tax').find('tr:eq('+index+')');
        $(tr).remove();
        caculateMoney("input[name='taxation[]']", "input[name='exchange_tax[]']", '#total_taxation', '#total_exchange_tax');
        callInputMaskDecimal()
    }
    // function addRowTaxByAccounting(index){
    //     var tr = $('#table_tax').find('tr:eq('+index+')');
    //     tr.after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.service-purchases.row-tax", compact("jobs", "branchCodes", "accounts", "partnerVendorCodes", "group_unit", "itemCode"))->render()) !!}');
    //     dateInvoice();
    // }
</script>   