<tr>
    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
        <p style="vertical-align: middle; text-align: center;">
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row-accounting"><i class="fas fa-copy"></i></a>
            &nbsp;&nbsp;
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row-accounting"><i class="fas fa-minus"></i></a>
        </p>
    </td>
    <td style="white-space: nowrap;min-width: 200px;">
        {!! Form::select("sale_code_item_tax[]", [' '=> trans('system.dropdown_choice')] +  $itemCode, old("sale_code_item_tax[]"), ['class' => 'form-control select2 sale_code_item_tax', 'disabled']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("description_tax[]", old('description_tax[]'), ['class' => 'form-control', "readonly"]) !!}
    </td>
    <td>
        {!! Form::text("explain_tax[]", old('explain_tax[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::select("account_tax[]", [' '=> trans('system.dropdown_choice')] + $accounts, old('account_tax[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;padding:0px">
        {!! Form::select("tax_rate[]", [' '=> trans('system.dropdown_choice')] + \App\Defines\Tax::getTaxValuesForOption(), old('tax_rate[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("taxation[]", old('taxation[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap;min-width: 200px;">
        {!! Form::text("exchange_tax[]", old('exchange_tax[]'), ['class' => 'form-control currency']) !!}
    </td>
    {{-- <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("unit_price[]", old('unit_price[]'), ['class' => 'form-control currency']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("exchange_unit_price[]", old('exchange_unit_price[]'), ['class' => 'form-control currency'  ]) !!}
    </td> --}}
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::select("group_unit[]", [' '=> trans('system.dropdown_choice')] + $group_unit, old('group_unit[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("invoice_no[]", old('invoice_no[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("date_invoice[]",  old('date_invoice[]', date('d/m/Y')), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;" class="object">
        {!! Form::select("object_code_tax[]", [' '=> trans('system.dropdown_choice')], old('object_code_tax[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("object_name_tax[]", old('object_name_tax[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 200px;">
        {!! Form::text("tax_code[]", old('tax_code[]'), ['class' => 'form-control']) !!}
    </td>
</tr>