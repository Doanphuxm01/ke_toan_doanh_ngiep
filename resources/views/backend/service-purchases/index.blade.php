@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('service_purchases.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('service_purchases.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.service-purchases.index') !!}">{!! trans('service_purchases.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bconsumer">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.service-purchases.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('vendor', trans('service_purchases.vendor')) !!}
                        {!! Form::text('vendor', Request::input('vendor'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all')] + App\Defines\ServicePurchase::getAllStatusForOptions(), Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('from_date', trans('payment_receipt_vouchers.from_date')) !!}
                        {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control from_date', 'placeholder' => 'MM/DD/YYYY']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('to_date', trans('payment_receipt_vouchers.to_date')) !!}
                        {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control to_date', 'placeholder' => 'MM/DD/YYYY']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.service-purchases.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-1" style="margin-right: 3px;">
            <button type="button"  class="btn btn-success" id='btnExport'>
                <span class="far fa-file-excel fa-fw"></span>&nbsp; {!! trans('service_purchases.export_excel') !!}
            </button>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $servicePurchase->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
              
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
       
    </div>
    @if (count($servicePurchase) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php  $i = (($servicePurchase->currentPage() - 1) * $servicePurchase->perPage()) + 1; ?>
         
            <div class="form-inline row">
                <div class="form-group col-md-81">
                    <span id="counterSelected" class="badge">0</span>
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $servicePurchase->count()) . ' ( ' . trans('system.total') . ' ' . $servicePurchase->total() . ' )' !!}                    | <i>Chú giải: </i>&nbsp;&nbsp;
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </span>
                </div>
                <div class="pull-right form-group col-md-41">
                    <div style="display: inline-block;">
                        <select class="form-control select2" id="action" style="width: 25% !important;">
                            <option value="delete"> {{ trans('system.action.delete_all') }} </option>
                        </select>
                    </div>
                    <div style="display: inline-block;">
                        <button type="button" class="btn btn-info btn-flat" onclick="return save()">
                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp; {!! trans('system.action.save') !!}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! Form::checkbox('check_all', 1, 0, [  ]) !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('service_purchases.day_voucher') !!}</th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('service_purchases.voucher_no') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('service_purchases.time_for_payment') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('service_purchases.vendor') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('service_purchases.description') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('service_purchases.total_amount') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('service_purchases.vat_amount') !!}</th>
                        <th style="text-align: left; vertical-align: middle;">{!! trans('service_purchases.total_payment') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>                        
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($servicePurchase as $item)
                        <tr>
                            <td style="text-align: center; width: 3%; vertical-align: middle;">
                                {!! Form::checkbox('servicePurchaseId', $item->id, null, array('class' => 'servicePurchaseId')) !!}
                            </td>
                            <td style="vertical-align: middle; text-align:center">
                                {!!  date("d/m/Y", strtotime(str_replace('/', '-', $item->voucher_date))); !!}                           
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->voucher_no !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!!  date("d/m/Y", strtotime(str_replace('/', '-', $item->time_for_payment))); !!}     
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! App\Models\Partner::getInfoPartner($item->partner_id)->name !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! $item->desc !!}
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!!  App\Helper\HString::decimalFormat($item->total_amount) !!} 
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! App\Helper\HString::decimalFormat($item->vat_amount) !!} 
                            </td>
                            <td style="vertical-align: middle; text-align:left">
                                {!! App\Helper\HString::decimalFormat($item->total_payment) !!} 
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 'DRAFT')
                                    {!! App\defines\ServicePurchase::getColorForState($item->status) !!}
                                @elseif ($item->status == 'REJECTED')
                                    {!! App\defines\ServicePurchase::getColorForState($item->status) !!}
                                @elseif ($item->status == 'APPROVED')
                                    {!! App\defines\ServicePurchase::getColorForState($item->status) !!}
                                @elseif ($item->status == 'SAVE')
                                    {!! App\defines\ServicePurchase::getColorForState($item->status) !!}
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap; text-align:center">
                                @if($item->status == "DRAFT")
                                <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#exampleModal">
                                    <i class="text-success glyphicon glyphicon-floppy-save"></i>
                                </button>
                                &nbsp;
                                <a href="{!! route('admin.service-purchases.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.service-purchases.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Save</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                          do you want action save
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                          <a href="{{ route('admin.service-purchases.save', $item->id) }}" class="btn btn-primary" title="{!! trans('system.action.save') !!}">
                                          confirm
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                @elseif ($item->status == "SAVE")
                                    <a href="{!! route('admin.service-purchases.show', $item->id) !!}" class="btn btn-xs btn-default" title="{!! 'browse application' !!}">
                                        <i class="text-success glyphicon glyphicon-floppy-save"></i>
                                    </a>
                                @elseif ($item->status == "APPROVED")
                                    <a href="{!! route('admin.service-purchases.cancel', $item->id) !!}" class="btn btn-xs btn-default" title="{!! 'cancel' !!}">
                                        <i class="text-success glyphicon glyphicon-floppy-remove"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
   
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script>
        var countChecked = function() {
            var length = $("input[name='servicePurchaseId']:checked" ).length;
            $("#counterSelected").text( length );
            if (length == 0) $("input[name='check_all']").attr('checked', false);
        };
        countChecked();
        $("input[type=checkbox][name='servicePurchaseId']").on( "click", countChecked );
        $("input[name='check_all']").change(function() {
            if($(this).is(':checked')) {
                $('.servicePurchaseId').each(function() {
                    this.checked = true;
                });
            } else {
                $('.servicePurchaseId').each(function() {
                    this.checked = false;
                });
            }
            countChecked();
        });
        function save() {
            if($( "input[name='servicePurchaseId']:checked" ).length == 0) {
                alert("{!! trans('system.no_item_selected') !!}");
                return false;
            }
            if($('#action').val() == 'noaction') {
                alert("{!! trans('system.no_action_selected') !!}");
                return false;
            }

            NProgress.start();

            var values = new Array();

            $.each($("input[name='servicePurchaseId']:checked"),
                function () {
                    console.log($(this).val());
                    values.push($(this).val());
                });

            $.ajax({
                url: "{!! route('admin.service-purchases.delete_all') !!}",
                data: { action: $('#action').val(), ids: JSON.stringify(values) },
                type: 'POST',
                datatype: 'json',
                headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                success: function(res) {
                    if(res.error)
                        alert(res.message);
                    else
                        window.location.reload(true);
                },
                error: function(obj, status, err) {
                    alert(err);
                }
            }).always(function() {
                NProgress.done();
            });

        }
            $(document).ready(function(){
                $("#btnExport").click(function() {
                    let table = document.getElementsByTagName("table");
                    TableToExcel.convert(table[0], { 
                        name: `export.xlsx`, 
                        sheet: {
                            name: 'Sheet 1' 
                        }
                    });
                });
            });
            !function ($) {
                var from_date=$('.from_date'); 
                from_date.datepicker({
                    format: 'dd/mm/yyyy',
                    
                    autoclose: true,
                })
            
                var to_date=$('.to_date'); 
                to_date.datepicker({
                    format: 'dd/mm/yyyy',
                    
                    autoclose: true,
                })
                $(function() {
                    $(".select2").select2({ width: '100%' });
                    $("select[name='page_num']").change(function(event) {
                        onReload('page_num', $(this).val());
                    });
                });
            }(window.jQuery);
            function onReload ($key, $value) {
                var queryParameters = {}, queryString = location.search.substring(1),
                    re = /([^&=]+)=([^&]*)/g, m;
                while (m = re.exec(queryString)) {
                    queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
                }
                queryParameters[$key]   = $value;
                queryParameters['page'] = 1;
                location.search = $.param(queryParameters);
            }
    </script>
@stop
