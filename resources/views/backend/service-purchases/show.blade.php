@extends('backend.master')
@section('title')
    {!! trans('system.action.view') !!} -{!! trans('service_purchases.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/css/table.css') !!}"/>
    @section('content')
    <section class="content-header">
        <h1>
            {!! trans('service_purchases.label') !!}
            <small>{!! trans('system.action.view') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            {{-- <li><a href="{!! route('admin.payment-receipt-vourchers.index') !!}">{!! trans('payment-vourchers.label') !!}</a></li> --}}
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content overlay">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-1">
                                        <b>{!! trans('service_purchases.vendor') !!}</b> 
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::select ('vendor', [' '=> trans('system.dropdown_choice')] + $vendor, old('vendor'), ['class' => 'form-control select2', 'required']) !!}
                                        </div>
                                        <div class="col-md-5">
                                            {!! Form::text('vendor_name', old('vendor_name'), ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                        <b>{!! trans('taxes.tax_code') !!}</b> 
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('tax_code_service', old('tax_code_service'), ['class' => 'form-control ']) !!}
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('service_purchases.voucher_date') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::text('voucher_date', old('voucher_date', date('d/m/Y')), ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'required']) !!}                                    
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('service_purchases.description') !!}</b>
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('details', old('details'), ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.financial_paper') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('financial_paper', old('financial_paper', $financial_paper), ['class' => 'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-2">
                                        <b>{!! trans('service_purchases.number_of_days_owed') !!}</b>  
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('number_of_days_owed', old('number_of_days_owed'), ['class' => 'form-control' ,'required']) !!}
                                        </div>
                                        <div class="col-md-1">
                                        <b>{!! trans('service_purchases.time_for_payment') !!}</b> 
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::text('time_for_payment',  old('time_for_payment'), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.currency') !!}</b>
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::select('currency', [' '=> trans('system.dropdown_choice')] + $currency, old('currency'), ['class' => 'form-control select2', 'required']) !!}
                                        </div>                                
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.exchange_rate') !!}</b>
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::text('exchange_rate',  old('exchange_rate'), ['class' => 'form-control currency' ,'required']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Accounting</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Tax</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    @include('backend.service-purchases.accounting')
                  </div>
                  <div class="tab-pane" id="tab_2">
                    @include('backend.service-purchases.tax')
                  </div>
                </div>
              </div>   
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center">
                                    {{-- {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                                    {!! trans('system.status.active') !!} --}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-3 text-right vcenter">
                                    <b>{!! trans('service_purchases.total_amount') !!}:</b>
                                </div>
                                <div class="col-md-3">
                                    <strong  id='amount_service_final'>{!! App\Helper\HString::decimalFormat(session('total_amount')) !!}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-3 text-right vcenter">
                                    <b> {!! trans('service_purchases.vat_amount') !!}:</b>
                                </div>
                                <div class="col-md-3">
                                    <strong  id='amount_vat'>{!! App\Helper\HString::decimalFormat(session('total_taxation')) !!}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-3 text-right vcenter">
                                    <b>{!! trans('service_purchases.total_payment') !!}:</b>
                                </div>
                                <div class="col-md-3">
                                    <strong  id='amount_total'>{!! App\Helper\HString::decimalFormat(session('total_amount') + session('total_taxation')) !!}</strong>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                {!! Form::open(['url'=> route('admin.service-purchases.approve', $servicePurchase->id) , 'method' => 'GET', 'role' => 'form']) !!}
                                {!! HTML::link(route( 'admin.service-purchases.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                                {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@stop
    @section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
              
            });
        }(window.jQuery);
    </script>
    
    <script>
        $('.date').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            autoclose: true,
        });
        $('input[name="voucher_date"]').change(function (e) { 
            e.preventDefault();
            $('input[name="date_vouchers"]').val($('input[name="settlement_date"]').val());
        });
        
        $('input[name="date_invoice[]"]').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            autoclose: true,
        }) 
        $('input[name="number_of_days_owed"]').inputmask({'alias': 'decimal', 'autoGroup': true, 'min': 0, 'max':99999999999999999999.99, 'removeMaskOnSubmit': true});
        $('input[name="number_of_days_owed"], input[name="voucher_date"]').change(function(){
            
            var date = $('input[name="voucher_date"]').datepicker('getDate');
            if (date !== null) { 
                var date = new Date(date);
            }
                days = (parseInt($("input[name='number_of_days_owed']").val(), 10)|| 0);
            if(!isNaN(date.getTime())){
                date.setDate(date.getDate() + days);
                    $("input[name='time_for_payment']").val(date.toInputFormat()).prop('readonly', true);
            } else {
                alert("Invalid Date");  
            }
            
        })
        Date.prototype.toInputFormat = function() {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth()+1).toString(); 
            var dd  = this.getDate().toString();
            return  (dd[1]?dd:"0"+dd[0]) + "/" +(mm[1]?mm:"0"+mm[0])+ "/" +yyyy  ; 
        };
        $(".currency").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'max':99999999999999999999.99, 'digits':2, 'removeMaskOnSubmit': true});
    </script>

    <script>
        $(document).ready(function () {
            $('select[name="vendor"]').change(function (e) { 
                var id =  $(this).val()
                $.ajax({
                    type: "Get",
                    url: "{!! route('admin.payment-receipt-vouchers.get-info-object') !!}",
                    data: {id:id},
                    dataType: "json",
                    success: function (response) {
                        console.log(response.data);
                        $('input[name="vendor_name"]').val(response.data.name);
                        $('input[name="tax_code_service"]').val(response.data.tax_code);
                        $('input[name="details"]').val('{!! trans("service_purchases.buy_service_of") !!} '+response.data.name);
                    },
                    error:function (response) {
                        toastr.error(response.message);
                    }
                });
            });
            getInfoObject('select[name="object_code_tax[]"]', 'input[name="object_name_tax[]"]') 
        });

        function getInfoObject(object_code, object_name) 
        {   
             $(document).on('change',object_code, function(){
                var id =  $(this).val()
                var tr = $(this).closest('tr');
                tr = tr.find(object_name);
                tr_tax_code = tr.closest('tr').find('input[name="tax_code[]"]');
                $.ajax({
                    type: "Get",
                    url: "{!! route('admin.payment-receipt-vouchers.get-info-object') !!}",
                    data: {id:id},
                    dataType: "json",
                    success: function (response) {
                        tr.val(response.data.name);
                        tr_tax_code.val(response.data.tax_code);
                    },
                    error:function (response) {
                        toastr.error(response.message);
                    }
                });
            })
        }
        
        $(document).on('change', 'select[name="sale_code_item[]"]', function () {
            var id =  $(this).val()
            var tr = $(this).closest('tr');
            var index = tr.index();
            var tr_tax = $('#table_tax').find('tr:eq('+index+')');
            var td_sale_code_item_tax = tr_tax.find('select[name="sale_code_item_tax[]"]');
            var td_description_tax = tr_tax.find('input[name="description_tax[]"]');
            var td_explain_tax = tr_tax.find('input[name="explain_tax[]"]');
            td_description = tr.find('input[name="description[]"]');
            td_debit_account = tr.find('input[name="debit_account[]"]');   
            $.ajax({
                type: "get",
                url: "{!! route('admin.service-purchases.get-info-sale-cost-item') !!}",
                data: {id:id},
                dataType: "json",
                success: function (response) {
                    td_description.val(response.data.description).prop('readonly', true);
                    td_debit_account.val(response.data.debit_account).prop('readonly', true);
                    td_sale_code_item_tax.val(response.data.id).change();
                    td_description_tax.val(response.data.description).prop('readonly', true);
                    td_explain_tax.val("VAT " + response.data.description)
                },
                error:function (response) {
                    toastr.error(response.message);
                }
            });
        })
    </script>

    <script>
        
        $(document).on('keyup', 'input[name="quantity[]"] , input[name="price_unit[]"]', function() {
            var total_with_amount = 0;
            var total_amount = 0;
            var total_quantity = 0;
            var total_exchange = 0;

            var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
            $("input[name='quantity[]']").each(function() {
                var tr = $(this).closest('tr');
                var quantity = (parseFloat($(this).val().replace(/,/g, '')) || 0);
                var with_amount = (parseFloat(tr.find('input[name="price_unit[]"]').val().replace(/,/g, ''))|| 0);
                var td_total_amount = tr.find('input[name="amount[]"]');
                var td_exchange = tr.find('input[name="exchange[]"]');
                td_total_amount.val(with_amount*quantity).inputmask();
                total_with_amount += with_amount;
                total_quantity += quantity;
                total_amount += with_amount*quantity;
                td_exchange.val(with_amount*quantity*exchange_rate).inputmask()
                total_exchange += Number(with_amount*quantity*exchange_rate);
            })
               
                $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                $("#total_quantity").val( total_quantity.toFixed(2)).inputmask();
                $("#total_with_amount").val(total_with_amount.toFixed(2)).inputmask();
                $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
                $("#total_amount").val(total_amount.toFixed(2)).inputmask();
        })
        $(document).on('keyup', 'input[name="amount[]"]', function() {
            var total_with_amount = 0;
            var sum_total_amount  = 0;
            var total_quantity    = 0;
            var total_exchange    = 0;
            var exchange_rate     = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
            $('input[name="amount[]"]').each(function() {
                var total_amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
                var tr = $(this).closest('tr');
                var td_quantity = tr.find('input[name="quantity[]"]');
                var td_with_amount = tr.find('input[name="price_unit[]"]');
                var td_exchange = tr.find('input[name="exchange[]"]');
                var quantity = (parseFloat(String(td_quantity.val()).replace(/,/g, ''))|| 0);
                var with_amount = total_amount/quantity;

                if(Object.is(with_amount, NaN) || Object.is(with_amount, Infinity)) {
                    with_amount = 0;
                }
                td_with_amount.val(with_amount.toFixed(2));
                total_with_amount += with_amount;
                total_quantity += quantity;
                sum_total_amount += total_amount;
                td_exchange.val((with_amount*quantity*exchange_rate).toFixed(2)).inputmask();
                total_exchange += with_amount*quantity*exchange_rate;
                
                $('#amount_service_final').html(String(sum_total_amount.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                $("#total_quantity").val( total_quantity.toFixed(2)).inputmask();
                $("#total_with_amount").val(total_with_amount.toFixed(2)).inputmask();
                $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
                $("#total_amount").val(sum_total_amount.toFixed(2)).inputmask();
               
            })
            
        })
        $(document).on('keyup', 'input[name="exchange_rate"]', function () {
            var total_exchange = 0
            var total_amount   = 0
            var exchange_rate  = (parseFloat($(this).val().replace(/,/g, '')) || 0);
            $('input[name="amount[]"]').each(function () {
                var tr = $(this).closest('tr').find('input[name="exchange[]"]');
                var amount =(parseFloat($(this).val().replace(/,/g, '')) || 0);
                tr.val(amount*exchange_rate);
                total_exchange += amount*exchange_rate;
                total_amount += amount;
                $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
            });
            $('input[name="taxation[]"]').each(function () {
                var td_exchange_tax = $(this).closest('tr').find('input[name="exchange_tax[]"]');
                var taxation =(parseFloat($(this).val().replace(/,/g, '')) || 0);
                td_exchange_tax.val(taxation*exchange_rate);
                total_exchange += taxation*exchange_rate;
            });

            $("#total_exchange_tax").val(total_exchange.toFixed(2)).inputmask();
            $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        })
        $(document).on('change', 'select[name="tax_rate[]"]', function() {
            var tax_rate = 0;
            var total_taxation = 0;
            var total_exchange_tax = 0;
            var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
            var total_amount = (parseFloat($("#total_amount").val().replace(/,/g, '')) || 0);
            $("input[name='taxation[]']").each(function(){
                var tr = $(this).closest('tr');
                var indexTrTax = $(this).closest('tr').index();
                var amount_accounting = (parseFloat($('#table_accounting tr:eq('+indexTrTax+')').find('input[name="amount[]"]').val().replace(/,/g, '')) || 0);
                var td_tax_rate = tr.find('select[name="tax_rate[]"]');
                var tax_rate = (parseFloat(td_tax_rate.val().replace(/,/g, '')) || 0);
                $(this).val((amount_accounting*tax_rate)/100);
                var taxation =(parseFloat($(this).val().replace(/,/g, '')) || 0);
                var td_exchange_tax = tr.find('input[name="exchange_tax[]"]');
                td_exchange_tax.val(Number(taxation)*Number(exchange_rate)).inputmask();
                total_taxation+=Number(taxation);
            })

            $("#total_taxation").val(total_taxation.toFixed(2)).inputmask();

            $("input[name='exchange_tax[]']").each(function() {
                var exchange_tax =  (parseFloat($(this).val().replace(/,/g, '')) || 0);
                total_exchange_tax += exchange_tax;
            })

            $("#total_exchange_tax").val(total_exchange_tax.toFixed(2)).inputmask();
            console.log("total_amount"+total_amount);
            var amount_total = total_taxation + total_amount;
           
            $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#amount_vat').html(total_taxation.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#amount_total').html(amount_total.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        })

        $(document).on('keyup', 'input[name="exchange_tax[]"]', function() {
            var total_exchange_tax = 0;
            $("input[name='exchange_tax[]']").each(function() {
                var exchange_tax =  (parseFloat($(this).val().replace(/,/g, '')) || 0);
                total_exchange_tax += exchange_tax;
            })
            $("#total_exchange_tax").val(total_exchange_tax.toFixed(2)).inputmask();
        })

        $(document).on('keyup', 'input[name="exchange[]"]', function() {
            var total_exchange = 0;
            $("input[name='exchange[]']").each(function() {
                var exchange =  (parseFloat($(this).val().replace(/,/g, '')) || 0);
                total_exchange += Number(exchange);
            })
            $("#total_exchange").val(Number(total_exchange).toFixed(2)).inputmask();
        })


       

        $(document).on('keyup', 'input[name="taxation[]"]', function() {
            var totalTaxation = 0;
            var totalExchangeTax = 0;
            var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
            $('input[name="taxation[]"]').each(function(e){
                tr = $(this).closest('tr');
                taxation = (parseFloat($(this).val().replace(/,/g, '')) || 0);
                totalTaxation += Number(taxation);
                td_exchange_tax = tr.find('input[name="exchange_tax[]"]');
                td_exchange_tax.val(Number(exchange_rate)*Number(taxation)).inputmask()
            });
            $('input[name="exchange_tax[]"]').each(function(e){
                exchange_tax = (parseFloat($(this).val().replace(/,/g, '')) || 0);
                totalExchangeTax += Number(exchange_tax);
            });
            $('#total_taxation').val(totalTaxation.toFixed(2)).inputmask();
            $('#total_exchange_tax').val(totalExchangeTax.toFixed(2)).inputmask();
            amount_servive = (parseFloat($("#total_amount").val().replace(/,/g, '')) || 0);
            amount_total = amount_servive+totalTaxation;
            console.log(amount_servive);
            $('#amount_service_final').html(String(amount_servive.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#amount_vat').html(String(totalTaxation.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#amount_total').html(String(amount_total.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        })
    </script>

    <script>
        
        !function ($) {
            $(function() {
                jQuery(document).ready(function(a){var b=a(document.body),c=!1,d=!1;b.on("keydown",function(a){var b=a.keyCode?a.keyCode:a.which;16==b&&(c=!0)}),b.on("keyup",function(a){var b=a.keyCode?a.keyCode:a.which;16==b&&(c=!1)}),b.on("mousedown",function(b){d=!1,1!=a(b.target).is('[class*="select2"]')&&(d=!0)}),b.on("select2:opening",function(b){d=!1,a(b.target).attr("data-s2open",1)}),b.on("select2:closing",function(b){a(b.target).removeAttr("data-s2open")}),b.on("select2:close",function(b){var e=a(b.target);e.removeAttr("data-s2open");var f=e.closest("form-u"),g=f.has("[data-s2open]").length;if(0==g&&0==d){var h=f.find(":input:enabled:not([readonly], input:hidden, button:hidden, textarea:hidden)").not(function(){return a(this).parent().is(":hidden")}),i=null;if(a.each(h,function(b){var d=a(this);if(d.attr("id")==e.attr("id"))return i=c?h.eq(b-1):h.eq(b+1),!1}),null!==i){var j=i.siblings(".select2").length>0;j?i.select2("open"):i.focus()}}}),b.on("focus",".select2",function(b){var c=a(this).siblings("select");0==c.is("[disabled]")&&0==c.is("[data-s2open]")&&a(this).has(".select2-selection--single").length>0&&(c.attr("data-s2open",1),c.select2("open"))})});
                $(document).on('keydown', ':tabbable', function(e) {
                    if (e.key === "Enter") {
                        e.preventDefault();

                        var $canfocus = $(':tabbable:visible');
                        var index = $canfocus.index(document.activeElement) + 1;
                        var isOriginalEvent = e.originalEvent
                        var isSingleSelect = $(this).find(".select2-selection--single").length > 0
                        if (isOriginalEvent && isSingleSelect) {
                            $(this).siblings('select:enabled').select2('open');
                        }
                        if (index >= $canfocus.length) index = 0;
                        $canfocus.eq(index).focus();
                    }   
                });
                
                $(document).on('focus', '.select2', function (e) {
                    if (e.originalEvent) {
                        var s2element = $(this).siblings('select');
                        s2element.select2('open');
                        s2element.on('select2:closing', function (e) {
                            s2element.select2('focus');
                        });
                    }
                }); 

                $(".select2").select2().on("select2:open", function (e) {
                    var position = $(this).closest('td');
                    if(position.index()>4) {
                       $('.table-responsive').scrollLeft($(this).position().left-100);
                    }
                });
             
                $(".select2").select2().on("select2:close", function (e) {
                    var html = $(this).closest('td').next().find('select');
                    if (html.is("select")) {
                        $(this).closest('td').next().find('select').select2('open');
                    } else {
                        $(this).closest('td').next().find('input').focus();
                    }
                });
            });
        }(window.jQuery);
    </script>
@stop

