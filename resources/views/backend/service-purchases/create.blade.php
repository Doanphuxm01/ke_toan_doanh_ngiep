@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} -{!! trans('service_purchases.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/css/table.css') !!}"/>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}"/>
    @section('content')
    <section class="content-header">
        <h1>
            {!! trans('service_purchases.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content overlay">
            {!! Form::open(['url'=> route('admin.service-purchases.store') , 'role' => 'form']) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('service_purchases.vendor') !!}</b> 
                                        </div>
                                        <div class="col-md-2 vendor">
                                            {!! Form::select ('vendor', [' '=> trans('system.dropdown_choice')], old('vendor'), ['class' => 'form-control select2 vendor', 'required']) !!}
                                        </div>
                                        <div class="col-md-5">
                                            {!! Form::text('vendor_name', old('vendor_name'), ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                                        </div>
                                        <div class="col-md-1" style="text-align: right">
                                            <b>{!! trans('service_purchases.type') !!}</b> 
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::select('type', \App\Define\Workflow::getAllReportsForOption(), old('type', \App\Define\Workflow::TYPE_AIR), ['class' => 'type form-control select2']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                        <b>{!! trans('taxes.tax_code') !!}</b> 
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('tax_code_service', old('tax_code_service'), ['class' => 'form-control ']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('service_purchases.voucher_date') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                {!! Form::text('day_voucher', old('day_voucher', date('d/m/Y')), ['class' => 'form-control date', 'placeholder' => 'MM/DD/YYYY', 'required']) !!}                                    
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('service_purchases.desc') !!}</b>
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('details', old('details'), ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('payment_receipt_vouchers.voucher_no') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('financial_paper', old('financial_paper', $financial_paper), ['class' => 'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-tabs-custom">
                <br>
                <br>
                <div>
                    <div class="col-md-2">
                        <b>{!! trans('service_purchases.number_of_days_owed') !!}</b>  
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('number_of_days_owed', old('number_of_days_owed'), ['class' => 'form-control' ,'required']) !!}
                    </div>
                    <div class="col-md-1">
                        <b>{!! trans('service_purchases.time_for_payment') !!}</b> 
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('time_for_payment',  old('time_for_payment'), ['class' => 'form-control', 'placeholder' => 'MM/DD/YYYY']) !!}
                        </div>
                    </div>
                    <div class="col-md-1 text-right vcenter">
                        <b>{!! trans('payment_receipt_vouchers.currency') !!}</b>
                    </div>
                    <div class="col-md-1">
                        {!! Form::select('currency', [' '=> trans('system.dropdown_choice')] + $currency, old('currency'), ['class' => 'form-control select2', 'required']) !!}
                    </div>                                
                    <div class="col-md-1 text-right vcenter">
                        <b>{!! trans('payment_receipt_vouchers.exchange_rate') !!}</b>
                    </div>
                    <div class="col-md-1">
                        {!! Form::text('exchange_rate',  old('exchange_rate'), ['class' => 'form-control currency' ,'required']) !!}
                    </div>
                </div>
                <br>
                <br>
                <br>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab"><b>Accounting</b></a></li>
                    <li><a href="#tab_2" data-toggle="tab"><b>Tax</b></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        @include('backend.service-purchases.accounting')
                    </div>
                    <div class="tab-pane" id="tab_2">
                        @include('backend.service-purchases.tax')
                    </div>
                </div>
              </div>   
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center">
                                    {{-- {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                                    {!! trans('system.status.active') !!} --}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-3 text-right vcenter">
                                    <b>{!! trans('service_purchases.total_amount') !!}:</b>
                                </div>
                                <div class="col-md-2" style="text-align: right">
                                    <strong  id='amount_service_final'>{!! App\Helper\HString::decimalFormat(session('total_amount')) !!}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-3 text-right vcenter">
                                    <b> {!! trans('service_purchases.vat_amount') !!}:</b>
                                </div>
                                <div class="col-md-2" style="text-align: right">
                                    <strong  id='amount_vat'>{!! App\Helper\HString::decimalFormat(session('total_taxation')) !!}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-3 text-right vcenter" style="text-align: right">
                                    <b>{!! trans('service_purchases.total_payment') !!}:</b>
                                </div>
                                <div class="col-md-2" style="text-align: right">
                                    <strong  id='amount_total'>{!! App\Helper\HString::decimalFormat(session('total_amount') + session('total_taxation')) !!}</strong>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12" style="text-align:center">
                                {!! HTML::link(route( 'admin.service-purchases.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                                {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@stop
    @section('footer')
    <style>
        .vendor-dropdown {
             width: 800px !important;
        }

        .custom-dropdown {
             width: 500px !important;
             left: -300px !important;
        }
    </style>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/fix-tabindex.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/call-plugins.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/service-purchase.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/select2-table-partner.js') !!}"></script>
    <script>
        var typeAir   = @json(\App\Define\Workflow::TYPE_AIR);
        var typeSea   = @json(\App\Define\Workflow::TYPE_SEA);
        var typeDom   = @json(\App\Define\Workflow::TYPE_DOM);
        var typeOther = @json(\App\Define\Workflow::TYPE_OTHER);
        var mawb             = @json(trans("input_invoices.mawb"));
        var hawb             = @json(trans("input_invoices.hawb"));
        var hbl              = @json(trans("input_invoices.hbl"));
        var obl              = @json(trans("input_invoices.obl"));
        var invoice          = @json(trans("input_invoices.invoice"));
        var trans            = @json(trans("service_purchases.buy_service_of"));
        var statistical_code = @json(trans("input_invoices.statistical_code"));
        callICheck();
        dateInvoice();
        callInputMaskDecimal();
    </script>

    <script>
        $(document).on('change', 'select[name="sale_code_item[]"]', function () {
            var id =  $(this).val()
            var tr = $(this).closest('tr');
            var index = tr.index();
            var tr_tax = $('#table_tax').find('tr:eq('+index+')');
            var td_sale_code_item_tax = tr_tax.find('select[name="sale_code_item_tax[]"]');
            var td_description_tax = tr_tax.find('input[name="description_tax[]"]');
            var td_explain_tax = tr_tax.find('input[name="explain_tax[]"]');
            td_description = tr.find('input[name="description[]"]');
            td_debit_account = tr.find('input[name="debit_account[]"]');   
            $.ajax({
                type: "get",
                url: "{!! route('admin.service-purchases.get-info-sale-cost-item') !!}",
                data: {id:id},
                dataType: "json",
                success: function (response) {
                    td_description.val(response.data.description).prop('readonly', true);
                    td_debit_account.val(response.data.debit_account).prop('readonly', true);
                    td_sale_code_item_tax.val(response.data.id).change();
                    td_description_tax.val(response.data.description).prop('readonly', true);
                    td_explain_tax.val("VAT " + response.data.description)
                },
                error:function (response) {
                    toastr.error(response.message);
                }
            });
        })
    </script>

    <script>
        !function ($) {
            $(function() {
                enterTab();
                select2Tab();
                renderTableS2('select[name="vendor"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllVendor);
                renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);
            });
        }(window.jQuery);

    </script>
@stop
