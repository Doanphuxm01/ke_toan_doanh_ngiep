@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('menus.instrument-tool-disposal.label') !!}
@stop
@section('head')
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('menus.fixed-asset-disposal.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.instrument-tool-disposal.index') !!}">{!! trans('menus.fixed-asset-disposal.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
<div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.instrument-tool-disposal.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('from', trans('system.from')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('to', trans('system.to')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getStatusForOption(), Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.instrument-tool-disposal.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $disposals->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($disposals) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($disposals->currentPage() - 1) * $disposals->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $disposals->count()) . ' ( ' . trans('system.total') . ' ' . $disposals->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" style="border-collapse: collapse;">
                    <thead style="background: #3C8DBC; color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('fixed_asset_depreciation.voucher_date') !!}</th>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('fixed_asset_depreciation.voucher_number') !!}</th>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('fixed_asset_disposal.disposal_reason') !!}</th>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('fixed_asset_depreciation.money_amount') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                        </tr>
                    </thead>
                    <tbody class="borderless">
                        @foreach ($disposals as $item)
                            <tr class="treegrid-{!! $item->id !!}">
                                <td style="text-align: center; vertical-align: middle;">
                                    {!! date_format(date_create($item->voucher_date), "d/m/Y") !!}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="{!! route('admin.instrument-tool-disposal.show', $item->id) !!}">{!! $item->voucher_no !!}</a>
                                </td>
                                <td style="vertical-align: middle;">
                                    {!! $item->disposal_reason !!}
                                </td>
                                <td style="text-align: right; vertical-align: middle;">
                                    {!! number_format(App\Models\InstrumentToolDisposal::getTotal($item->id)) !!}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    @if($item->status == "DRAFT")
                                        <span class="label label-danger">{!! trans('system.status.draft') !!}</span>
                                    @elseif($item->status == "SAVE")
                                        <span class="label label-primary">{!! trans('system.status.save') !!}</span>
                                    @elseif($item->status == "APPROVED")
                                        <span class="label label-success">{!! trans('system.status.approved') !!}</span>
                                    @endif
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    @if(count(App\Models\InstrumentToolDisposal::checkEditDelete($item->id)) > 0)
                                       
                                    @else
                                        <a href="{!! route('admin.instrument-tool-disposal.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}">
                                            <i class="text-warning glyphicon glyphicon-edit"></i>
                                        </a>
                                        &nbsp;
                                        <a href="javascript:void(0)" link="{!! route('admin.instrument-tool-disposal.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                            <i class="text-danger glyphicon glyphicon-remove"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>   
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
</script>
<script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop