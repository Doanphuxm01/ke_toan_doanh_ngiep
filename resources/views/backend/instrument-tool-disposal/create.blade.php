@extends('backend.master')
@section('title')
{!! trans('system.action.create') !!} - {!! trans('menus.instrument-tool-disposal.label') !!}
@stop
@section('head')
    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.instrument-tool-disposal.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.instrument-tool-disposal.index') !!}">{!! trans('menus.instrument-tool-disposal.label') !!}</a></li>
        </ol>
    </section>
    
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.instrument-tool-disposal.store'), 'role' => 'form']) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_disposal.disposal_reason') !!}</b>
                    </div>
                    <div class="col-md-5">
                        {!! Form::text('disposal_reason', old('disposal_reason'), ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_disposal.voucher_date') !!}</b>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('voucher_date', old('voucher_date'), ['class' => 'form-control date_picker', 'required']) !!}       
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2" >
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_disposal.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('voucher_no', old('voucher_no', $voucher_number), ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('menus.instrument-tool-disposal.label') !!}</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style="min-width: 200px">
                                            {!! trans('instrument_tools.code') !!}
                                        </th>
                                        <th class="text-center" style="min-width: 300px">
                                            {!! trans('instrument_tools.name') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" >
                                        {!! Form::select('instrument_tool_id', ['' => trans('system.dropdown_choice')] + $instrument_tools, old('instrument_tool_id'), ['class' => 'form-control select2', 'style' => 'width: 100%', 'required']) !!}
                                        </td>
                                        <td class="text-center" >
                                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'readonly']) !!}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>  
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style="min-width: 200px">
                                            {!! trans('fixed_assets.use_department') !!}
                                        </th>
                                        <th class="text-center" style="min-width: 200px">
                                            Residual quantity
                                        </th>
                                        <th class="text-center" style="min-width: 200px">
                                            Disposal quantity
                                        </th>
                                        <th class="text-center" style="min-width: 200px">
                                            {!! trans('fixed_assets.residual_value') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="department">
                                    <tr>
                                        <td class="text-center" >
                                        {!! Form::text('residual_quantity', old('residual_quantity', ), ['class' => 'form-control money', 'readonly']) !!}
                                        </td>
                                        <td class="text-center" >
                                        {!! Form::text('residual_quantity', old('residual_quantity'), ['class' => 'form-control money']) !!}
                                        </td>
                                        <td class="text-center" >
                                        {!! Form::text('disposal_quantity', old('disposal_quantity'), ['class' => 'form-control money']) !!}
                                        </td>
                                        <td class="text-center" >
                                        {!! Form::text('residual_value1', old('residual_value1'), ['class' => 'form-control money', 'readonly']) !!}
                                        </td>
                                    </tr>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    {!! HTML::link(route( 'admin.instrument-tool-disposal.index' ), trans('system.action.cancel'), [ 'class' => 'btn btn-danger btn-flat ']) !!}
                    {!! Form::submit(trans('system.action.save'), ['id' => 'save-btn', 'class' => 'btn btn-primary btn-flat submit']) !!}
                </div>
            </div>
        </div>
        
        {!! Form::close() !!}
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
   
    $('.select2').select2({
        dropdownAutoWidth: true,
    });
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
</script>
<script>
    $(document).on('change', "select[name='instrument_tool_id']", function () {
        var instrument_tool_id = $(this).val()
        var name = $(this).closest('tr').find("input[name='name']")
        var residual_quantity = $(this).closest('tr').find("input[name='residual_quantity']")
        var unit_price = $(this).closest('tr').find("input[name='unit_price']")
        if(instrument_tool_id == ""){
            name.val("")
        }
        else{
            $.ajax({
            url: "{!! route('admin.get-instrument-tool-info') !!}",
            data: { instrument_tool_id: instrument_tool_id},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                console.log(data)
                name.val(data.instrument_tool.name)
                unit_price.val(data.instrument_tool.unit_price)
                $('#department').empty()
                console.log(data)
                jQuery.each(data.instrument_tool.instrument_tool_allocation, function(index, value){
                    var residual_quantity = value.quantity - value.allocated_quantity
                    html = '<tr>'
                    html += '<td class="text-center" >'
                    html += '<input class="form-control" type="hidden" name="department_id[]" value="'+ value.department.id +'">'
                    html += '<input class="form-control" type="text" value="'+ value.department.name +'">'
                    html += '</td>'
                    html += '<td class="text-center" >'
                    if (value.allocated_quantity) {
                        html += '<input class="form-control money" type="text" name="residual_quantity[]" value="'+ residual_quantity +'">'
                    }
                    else{
                        html += '<input class="form-control money" type="text" name="residual_quantity[]" value="'+ value.quantity +'">'
                    }
                    html += '</td>'
                    html += '<td class="text-center" >'
                    html += '<input class="form-control money" type="text" name="disposal_quantity[]"  >'
                    html += '</td>'
                    html += '<td class="text-center" >'
                    html += '<input class="form-control money" type="text" name="residual_value[]" value="'+data.residual_quantity * (value.allocation_rate / 100) * data.instrument_tool.unit_price +'" >'
                    html += '</td>'
                    html += '</tr>'
                    $('#department').append(html)
                    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'digits': 2,   'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                })
                $(document).on('change', "input[name='disposal_quantity[]']", function(){
                    var disposal_quantity = $(this).val().replace(/,/g, "")
                    var residual_value = $(this).closest('tr').find("input[name='residual_value[]']")
                    var residual_quantity = $(this).closest('tr').find("input[name='residual_quantity[]']").val().replace(/,/g, "")
                    if(parseInt(disposal_quantity) > parseInt(residual_quantity)){
                        toastr.error("Enter voucher number")
                        $('.submit').prop('disabled', true)
                        residual_value.val("")
                    }
                    else{
                        $('.submit').prop('disabled', false);
                        residual_value.val(disposal_quantity * data.instrument_tool.unit_price)
                    } 
                })
            },
            error: function(obj, status, err) {
            }
        })
        }
        
        
    });

    $(document).on('change', 'select[name="disposal_account"]', function(){
        $(".debit_account").val($(this).val()).change()
    })
    $(document).on("change", ".debit_object_type",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find((".debit_object_id"));
        var option = $(this).closest('tr').find((".debit_object_id option"));
        $.ajax({
            url: "{!! route('admin.get-debit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
    $(document).on("change", ".credit_object_type",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find((".credit_object_id"));
        var option = $(this).closest('tr').find((".credit_object_id option"));
        $.ajax({
            url: "{!! route('admin.get-credit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
                
</script>
@stop
