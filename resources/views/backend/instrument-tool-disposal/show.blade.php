@extends('backend.master')
@section('title')
{!! trans('system.action.create') !!} - {!! trans('menus.instrument-tool-disposal.label') !!}
@stop
@section('head')
    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.instrument-tool-disposal.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.instrument-tool-disposal.index') !!}">{!! trans('menus.instrument-tool-disposal.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.instrument-tool-disposal.update', $disposal->id),  'method' => 'PUT', 'role' => 'form', 'files' => true]) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_disposal.disposal_reason') !!}</b>
                    </div>
                    <div class="col-md-5">
                        {!! Form::text('disposal_reason', old('disposal_reason', $disposal->disposal_reason), ['class' => 'form-control', 'disabled']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_disposal.voucher_date') !!}</b>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('voucher_date', old('voucher_date', \Carbon\Carbon::createFromTimestamp(strtotime($disposal->voucher_date))->format('d-m-Y')), ['class' => 'form-control date_picker', 'disabled']) !!}       
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2" >
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_disposal.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('voucher_no', old('voucher_no', $disposal->voucher_no), ['class' => 'form-control', 'disabled']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('menus.instrument-tool-disposal.label') !!}</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style="min-width: 200px">
                                            {!! trans('instrument_tools.code') !!}
                                        </th>
                                        <th class="text-center" style="min-width: 300px">
                                            {!! trans('instrument_tools.name') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" >
                                        {!! Form::select('instrument_tool_id', ['' => trans('system.dropdown_choice')] + $instrument_tools, old('instrument_tool_id', $disposal->instrument_tool_id), ['class' => 'form-control select2', 'style' => 'width: 100%', 'disabled']) !!}
                                        </td>
                                        <td class="text-center" >
                                        {!! Form::text('name', old('name', App\Models\InstrumentTool::getName($disposal->instrument_tool_id)), ['class' => 'form-control', 'disabled']) !!}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>  
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style="min-width: 200px">
                                            {!! trans('fixed_assets.use_department') !!}
                                        </th>
                                        <th class="text-center" style="min-width: 200px">
                                            Residual quantity
                                        </th>
                                        <th class="text-center" style="min-width: 200px">
                                            Disposal quantity
                                        </th>
                                        <th class="text-center" style="min-width: 200px">
                                            {!! trans('fixed_assets.residual_value') !!}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="department">
                                    @foreach($details as $detail)
                                    <tr>
                                        <td class="text-center" >
                                            {!! Form::hidden('detail_id[]', old('detail_id[]', $detail->id), ['class' => 'form-control money', 'disabled']) !!}
                                            {!! Form::hidden('department_id', old('department_id', $detail->department_id), ['class' => 'form-control money', 'disabled']) !!}
                                            {!! Form::hidden('unit_price[]', old('unit_price', $instrument_tool->unit_price), ['class' => 'form-control money', 'disabled']) !!}
                                            {!! Form::text('department', old('department', App\Models\Department::getDepartment($detail->department_id)), ['class' => 'form-control', 'disabled']) !!}
                                        </td>
                                        <td class="text-center" >
                                            {!! Form::text('residual_quantity[]', old('residual_quantity[]', $detail->residual_quantity), ['class' => 'form-control money', 'disabled']) !!}
                                        </td>
                                        <td class="text-center" >
                                            {!! Form::text('disposal_quantity[]', old('disposal_quantity[]', $detail->disposal_quantity), ['class' => 'form-control money', 'disabled']) !!}
                                        </td>
                                        <td class="text-center" >
                                            {!! Form::text('residual_value[]', old('residual_value[]', $detail->amount), ['class' => 'form-control money', 'disabled']) !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        {!! Form::close() !!}
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
   
    $('.select2').select2({
        dropdownAutoWidth: true,
    });
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'digits': 2, 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
</script>
<script>
@stop
