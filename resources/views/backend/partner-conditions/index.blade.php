@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('partner_conditions.label') !!}
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('partner_conditions.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.partner-conditions.index') !!}">{!! trans('partner_conditions.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="row">
        <div class="col-md-2">
            <a href="{!! route('admin.partner-conditions.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp;{!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-md-10">
        </div>
    </div>
    @if (count($conditions) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-body no-padding">
            <table class='table table-striped table-bordered tree'>
                <thead>
                    <tr>
                        <?php
                            $vip = App\StaticPage::getFullBySlug('level-vip');
                            $svip = App\StaticPage::getFullBySlug('level-svip');
                            $ssvip = App\StaticPage::getFullBySlug('level-ssvip');
                        ?>
                        <th style="text-align: center; vertical-align: middle;"></th>
                        <th style="text-align: center; vertical-align: middle;">{!! $vip['title'] !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! $svip['title'] !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! $ssvip['title'] !!}</th>
                        {{-- <th style="text-align: center; vertical-align: middle;"> {!! trans('system.position') !!} </th> --}}
                        <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                    </tr>
                </thead>
                <tbody class="borderless">
                    @foreach ($conditions as $item)
                        <tr>
                            <td  style="text-align: justify; vertical-align: middle;">
                                {!! $item->name !!}
                            </td>
                            <td style="text-align: justify; vertical-align: middle;">
                                {!! nl2br($item->vip) !!}
                            </td>
                            <td style="text-align: justify; vertical-align: middle;">
                                {!! nl2br($item->svip) !!}
                            </td>
                            <td style="text-align: justify; vertical-align: middle;">
                                {!! nl2br($item->ssvip) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <a href="{!! route('admin.partner-conditions.edit', $item->id) !!}" class="btn-edit edit text-warning">
                                    <i class="glyphicon glyphicon-edit"></i>
                                    {!! trans('system.action.edit') !!}
                                </a>
                                <a style="cursor: pointer;" link="{!! route('admin.partner-conditions.destroy', $item->id) !!}" class="btn-confirm-del text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop