@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('partner_conditions.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('partner_conditions.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.partner-conditions.index') !!}">{!! trans('partner_conditions.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.partner-conditions.store'), 'role' => 'form']) !!}
        <table class='table borderless' style="width: 80%;">
            <tr>
                <th class="table_right_middle">
                    {!! trans('partner_conditions.name') !!}
                </th>
                <td>
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => 255, 'required']) !!}
                </td>
            </tr>
            <?php
                $vip = App\StaticPage::getFullBySlug('level-vip');
                $svip = App\StaticPage::getFullBySlug('level-svip');
                $ssvip = App\StaticPage::getFullBySlug('level-ssvip');
            ?>
            <tr>
                <th class="table_right_middle">
                    {!! $vip['title'] !!}
                </th>
                <td>
                    {!! Form::textarea('vip', old('vip'), ['class' => 'form-control', 'maxlength' => 1024, 'required', 'rows' => 5]) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! $svip['title'] !!}
                </th>
                <td>
                    {!! Form::textarea('svip', old('svip'), ['class' => 'form-control', 'maxlength' => 1024, 'required', 'rows' => 5]) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! $ssvip['title'] !!}
                </th>
                <td>
                    {!! Form::textarea('ssvip', old('ssvip'), ['class' => 'form-control', 'maxlength' => 1024, 'required', 'rows' => 5]) !!}
                </td>
            </tr>
            <tr>
                <td colspan="3" class="text-center">
                    {!! HTML::link(route( 'admin.partner-conditions.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat'])!!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop