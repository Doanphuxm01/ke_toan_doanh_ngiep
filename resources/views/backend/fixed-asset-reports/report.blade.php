<?php
    $use_time = 0;
    $residual_use_time = 0;
    $original_price = 0;
    $depreciation_value = 0;
    $accumulated_depreciation = 0;
    $residual_value = 0;
    $monthly_depreciation_value = 0;
    $periodical_depreciation_value = 0;
    foreach (Session::get('fixed_assets') as $item) {
        $use_time += $item->use_time;
        $residual_use_time += $item->residual_use_time;
        $original_price += $item->original_price;
        $depreciation_value += $item->depreciation_value;
        $accumulated_depreciation += $item->accumulated_depreciation;
        $residual_value += $item->residual_value;
        $monthly_depreciation_value += $item->monthly_depreciation_value;
        $periodical_depreciation_value += $item->periodical_depreciation_value;
    }
?>
<table class="table table-bordered" id="report-data">
    <tr>
        <th  style="text-align:center" colspan="13">
            <b style="font-size: 28px; font-weight: bold">FIXED ASSET REPORT</b>
        </th>
    </tr>
    <tr>
        <th  style="text-align:center" colspan="13">
            @if(Session::get('department') == "" && Session::get('from_date') == "")
            @elseif(Session::get('from_date') == "")
            <b style="font-size: 20px; font-weight: bold">Use department: {!! Session::get('department') !!} </b>
            @elseif(Session::get('department') == "")
            <b style="font-size: 20px; font-weight: bold">From {!! Session::get('from_date') !!} to {!! Session::get('to_date') !!} </b>
            @elseif(Session::get('department') != "" && Session::get('from_date') != "")
            <b style="font-size: 20px; font-weight: bold">Use department: {!! Session::get('department') !!}; From {!! Session::get('from_date') !!} to {!! Session::get('to_date') !!} </b>
            @endif
        </th>
    </tr>

        <tr >
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center" >
            {!! trans('fixed_assets.asset_code') !!}
            </td>
            <td style="white-space: nowrap; min-width: 300px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.asset_name') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.use_department') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.voucher_creation_date') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.voucher_number') !!}
            </td>
            <td style="white-space: nowrap; min-width: 250px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.begin_use_date') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.use_time') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.residual_time') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.original_price') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.depreciation_value') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.periodical_depreciation_value') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.accumulated_depreciation') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.residual_value') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.monthly_depreciation_value') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.original_price_account') !!}
            </td>
            <td style="white-space: nowrap; min-width: 150px; background: #adc7e7; text-align: center">
            {!! trans('fixed_assets.depreciation_account') !!}
            </td>
        </tr>

    <tbody>
        @foreach(Session::get('fixed_assets') as $item)
        <tr>
            <td style="white-space: nowrap;">
                {!! $item->asset_code !!}
            </td>
            <td style="white-space: nowrap;">
                {!! $item->asset_name !!}
            </td>
            <td style="white-space: nowrap;">
                {!! $item->department_name !!}
            </td>
            <td style="white-space: nowrap; text-align: center">
                {!! date_format(date_create($item->voucher_creation_date), "d/m/Y") !!}
            </td>
            <td style="white-space: nowrap;">
                {!! $item->voucher_number !!}
            </td>
            <td style="white-space: nowrap; text-align: center">
                {!! date_format(date_create($item->begin_use_date), "d/m/Y") !!}
            </td>
            <td style="white-space: nowrap; text-align: right" class="use_time">
                {!! $item->use_time !!}
            </td>
            <td style="white-space: nowrap;  text-align: right" class="residual_use_time">
                {!! $item->residual_use_time !!}
            </td>
            <td style="white-space: nowrap;  text-align: right" class="original_price">
                {!! number_format($item->original_price) !!}
            </td>
            <td style="white-space: nowrap;  text-align: right" class="depreciation_value">
                {!! number_format($item->depreciation_value) !!}
            </td>
            <td style="white-space: nowrap;  text-align: right" class="periodical_depreciation_value">
                {!! number_format($item->periodical_depreciation_value) !!}
            </td>
            <td style="white-space: nowrap; text-align: right" class="accumulated_depreciation">
                {!! number_format($item->accumulated_depreciation) !!}
            </td>
            <td style="white-space: nowrap; text-align: right" class="residual_value">
                {!! number_format($item->residual_value) !!}
            </td>
            <td style="white-space: nowrap; text-align: right" class="monthly_depreciation_value">
                {!! number_format($item->monthly_depreciation_value) !!}
            </td>
            <td style="white-space: nowrap;">
                {!! $item->original_price_account !!}
            </td>
            <td style="white-space: nowrap;">
                {!! $item->depreciation_account !!}
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td style="white-space: nowrap; background:#d7d7d7">
                {!! trans('fixed_assets.line_number') !!} = {!! count(Session::get('fixed_assets')) !!}
            </td>
            <td style="white-space: nowrap;">
               
            </td>
            <td style="white-space: nowrap;">
                
            </td>
            <td style="white-space: nowrap; text-align: center">
               
            </td>
            <td style="white-space: nowrap;">
                
            </td>
            <td style="white-space: nowrap; text-align: center">
                
            </td>
            <td style="white-space: nowrap; text-align: right; background:#d7d7d7" >
                {!! number_format($use_time) !!}
            </td>
            <td style="white-space: nowrap;  text-align: right; background:#d7d7d7">
                {!! number_format($residual_use_time) !!}
            </td>
            <td style="white-space: nowrap; text-align: right; background:#d7d7d7" >
                {!! number_format($original_price) !!}
            </td>
            <td style="white-space: nowrap;  text-align: right; background:#d7d7d7" >
                {!! number_format($depreciation_value) !!}
            </td>
            <td style="white-space: nowrap;  text-align: right; background:#d7d7d7" >
                {!! number_format($periodical_depreciation_value) !!}
            </td>
            <td style="white-space: nowrap; text-align: right; background:#d7d7d7">
               {!! number_format($accumulated_depreciation) !!}
            </td>
            <td style="white-space: nowrap; text-align: right; background:#d7d7d7">
                {!! number_format($residual_value) !!}
            </td>
            <td style="white-space: nowrap; text-align: right; background:#d7d7d7">
                {!! number_format($monthly_depreciation_value) !!}
            </td>
            <td style="white-space: nowrap;">
            </td>
            <td style="white-space: nowrap;">
            </td>
        </tr>
    </tfoot>
</table>
<!-- <script>
    $(document).ready(function(){
        var use_time = 0
        var residual_use_time = 0
        var original_price = 0
        var depreciation_value = 0
        var accumulated_depreciation = 0
        var residual_value = 0
        var monthly_depreciation_value = 0
        var periodical_depreciation_value = 0
        jQuery.each($('.use_time'), function(){
            use_time += Number($(this).text().replace(/,/g, ""))
        })
        $('#use_time').text(formatNumber(use_time))

        jQuery.each($('.residual_use_time'), function(){
            residual_use_time += Number($(this).text().replace(/,/g, ""))
        })
        $('#residual_use_time').text(formatNumber(residual_use_time))

        jQuery.each($('.original_price'), function(){
            original_price += Number($(this).text().replace(/,/g, ""))
        })
        $('#original_price').text(formatNumber(original_price))

        jQuery.each($('.depreciation_value'), function(){
            depreciation_value += Number($(this).text().replace(/,/g, ""))
        })
        $('#depreciation_value').text(formatNumber(depreciation_value))

        jQuery.each($('.accumulated_depreciation'), function(){
            accumulated_depreciation += Number($(this).text().replace(/,/g, ""))
        })
        $('#accumulated_depreciation').text(formatNumber(accumulated_depreciation))

        jQuery.each($('.residual_value'), function(){
            residual_value += Number($(this).text().replace(/,/g, ""))
        })
        $('#residual_value').text(formatNumber(residual_value))

        jQuery.each($('.monthly_depreciation_value'), function(){
            monthly_depreciation_value += Number($(this).text().replace(/,/g, ""))
        })
        $('#monthly_depreciation_value').text(formatNumber(monthly_depreciation_value))

        jQuery.each($('.periodical_depreciation_value'), function(){
            periodical_depreciation_value += Number($(this).text().replace(/,/g, ""))
        })
        $('#periodical_depreciation_value').text(formatNumber(periodical_depreciation_value))
    })
</script> -->