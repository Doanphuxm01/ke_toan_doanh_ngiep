@extends('backend.master')
@section('title')
{!! trans('menus.fixed-asset-reports.label') !!}
@stop
@section('head')
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
<style type="text/css">
    .uppercase {
        text-transform: uppercase;
    }
</style>
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('menus.fixed-asset-reports.label') !!}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.fixed-asset-reports.index') !!}">{!! trans('menus.fixed-asset-reports.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-body">
        {!! Form::open([ 'url' => route('admin.fixed-asset-reports.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
            	<div class="col-md-4">
                    {!! Form::label('type', trans('fixed_assets.use_department') ) !!}
            		{!! Form::select('department',['' => trans('system.dropdown_choice')] + $departments ,old('department', Request::input('department')), ['class' => 'form-control select2']) !!}
            	</div>
            	<div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('from', trans('system.from')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('to', trans('system.to')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                        <button type="submit" class="btn btn-info" id="report">
                            <span class="glyphicon glyphicon-flash"></span>&nbsp; {!! trans('system.create_report') !!}
                        </button>
                    </div>
                </div>
            	<div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                        <a href="{!! route('admin.fixed-asset-export') !!}" id="export"> 
                            <button type="button"  class="btn btn-success" >
                                <span class="far fa-file-excel fa-fw"></span>&nbsp; {!! trans('system.export') !!}
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
    <div class="box">
        <div class="box-body">
            <div id="result" class="table-responsive">
                @if(count($fixed_assets) > 0)
                    @include('backend.fixed-asset-reports.report')
                @else
                @endif
            </div>
        </div>
    </div>
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.js') !!}"></script>
<script>
$('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
 function formatDate(date) {
    var d = new Date(date);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;
    return date;
};

    !function ($) {
        $(function(){
            $(".select2").select2({ width: '100%' });
            $('.date_range').daterangepicker({
                autoUpdateInput: false,
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Áp dụng",
                    "cancelLabel": "Huỷ bỏ",
                    "fromLabel": "Từ ngày",
                    "toLabel": "Tới ngày",
                    "customRangeLabel": "Tuỳ chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Thg 1",
                        "Thg 2",
                        "Thg 3",
                        "Thg 4",
                        "Thg 5",
                        "Thg 6",
                        "Thg 7",
                        "Thg 8",
                        "Thg 9",
                        "Thg 10",
                        "Thg 11",
                        "Thg 12"
                    ],
                    "firstDay": 1
                },
                ranges: {
                   'Hôm nay': [moment(), moment()],
                   'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                   '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                   'Tháng này': [moment().startOf('month'), moment()],
                   'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                },
                "alwaysShowCalendars": true,
                maxDate: moment(),
                minDate: moment().subtract(1, "years"),
            }, function(start, end, label) {
                $('.date_range').val(start.format('DD/MM/YYYY') + " - " + end.format('DD/MM/YYYY'));
            });
            $('#export').click(function(){
                if($('#report-data').length){
                    
                    console.log('ád')
                }
                else{
                    toastr.error("Please create a report");
                    return false;
                    
                }
            })
            $('#report').click(function(){
                var from_date = $('input[name="from_date"]').val()
                var to_date = $('input[name="to_date"]').val()
                if(from_date == "" && to_date != 0){
                    toastr.error("Choos from date");
                    return false
                }
                if(to_date == "" && from_date != 0){
                    toastr.error("Choos to date");
                    return false
                }

            })
            // $("#report").click(function(event) {
            //     $('#result').empty()
            //     var department = $("select[name='department']").val();
            //     $.ajax({
            //         url: "{!! route('admin.get-report') !!}",
            //         type: 'POST',
            //         headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            //         data: { department: department, date_range: $(".date_range").val() },
            //         datatype: 'json',
            //         success: function(data) {
            //             if(data == ""){
            //                 html = '<h4>There are no assets</h4>'
            //                 $('#result').append(html)
            //             }
            //             else{
            //                 html = '<table class="table table-bordered">'
            //             html += '<thead style="background: #3C8DBC; color: white;">'
            //             html += '<tr>'
            //             html += '<th style="white-space: nowrap; min-width: 300px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.asset_code') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.asset_name') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.use_department') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.voucher_creation_date') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.voucher_number') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 250px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.begin_use_date') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.use_time') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 250px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.residual_time') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.accumulated_depreciation') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.residual_value') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.monthly_depreciation_value') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.original_price_account') !!}'
            //             html += '</th>'
            //             html += '<th style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //             html += '{!! trans('fixed_assets.depreciation_account') !!}'
            //             html += '</th>'
            //             html += '</tr>'
            //             html += '</thead>'
            //             html += '<tbody>'
            //             jQuery.each(data, function(index, value){
            //                 html += '<tr>'
            //                 html += '<td style="white-space: nowrap; min-width: 300px;" class="text-center">'
            //                 html += ''+value.asset_code+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+value.asset_name+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+value.department_name+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+formatDate(value.voucher_creation_date)+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+value.voucher_number+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 250px;" class="text-center">'
            //                 html += ''+formatDate(value.begin_use_date)+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+value.use_time+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 250px;" class="text-center">'
            //                 html += ''+value.residual_use_time+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+formatNumber(value.accumulated_depreciation)+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+formatNumber(value.residual_value)+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+formatNumber(value.monthly_depreciation_value)+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+value.original_price_account+''
            //                 html += '</td>'
            //                 html += '<td style="white-space: nowrap; min-width: 150px;" class="text-center">'
            //                 html += ''+value.depreciation_account+''
            //                 html += '</td>'
            //                 html += '</tr>'
            //             })
            //             html += '</tbody>'
            //             html += '</table>'
            //             $('#result').append(html)
            //             }
            //         },
            //         error: function(obj, status, err) {
                       
            //         }
            //     }).always(function() {
            //     });
            // });
        });
}(window.jQuery);

</script>
@stop