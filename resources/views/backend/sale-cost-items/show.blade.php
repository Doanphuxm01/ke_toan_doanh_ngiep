@extends('backend.master')
@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('sale_cost_items.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('sale_cost_items.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($saleCostItem->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.sale-cost-items.index') !!}">{!! trans('sale_cost_items.label') !!}</a></li>
        </ol>
    </section>
    <table class='table borderless'>
        <tr>
            <th class="table_right_middle" style="width: 15%;">
                {!! trans('sale_cost_items.code') !!}
            </th>
            <td style="width: 35%;">
                {!! Form::text('code', old('code', $saleCostItem->code), ['class' => 'form-control', 'maxlength' => 20, 'disabled']) !!}
            </td>
            <th class="table_right_middle" style="width: 15%;">
                {!! trans('sale_cost_items.branch_code') !!}
            </th>
            <td>
                {!! Form::select('branch_code', $branchCodes, old('branch_code', $saleCostItem->branch_code), ['class' => 'form-control select2', 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('sale_cost_items.description') !!}
            </th>
            <td colspan="3">
                {!! Form::text('description', old('description', $saleCostItem->description), ['class' => 'form-control', 'maxlength' => 255, 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('sale_cost_items.charge_type') !!}
            </th>
            <td>
                {!! Form::select('charge_type', ['charge_type1' => 'charge type 1', 'charge_type2' => 'charge type 2', 'charge_type3' => 'charge type 3'], old('charge_type', $saleCostItem->charge_type), ['class' => 'form-control select2', 'disabled']) !!}
            </td>
            <th class="table_right_middle">
                {!! trans('sale_cost_items.tax_value') !!}
            </th>
            <td>
                {!! Form::select('tax_value', [0 => '0%', 10 => '10%'], old('tax_value', $saleCostItem->tax_value), ['class' => 'form-control select2', 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('sale_cost_items.sale_account_code') !!}
            </th>
            <td>
                {!! Form::select('sale_account_code', $accounts, old('sale_account_code', $saleCostItem->sale_account_code), ['class' => 'form-control select2', 'disabled']) !!}
            </td>
            <th class="table_right_middle">
                {!! trans('sale_cost_items.cost_account_code') !!}
            </th>
            <td>
                {!! Form::select('cost_account_code', $accounts, old('cost_account_code', $saleCostItem->cost_account_code), ['class' => 'form-control select2', 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('sale_cost_items.prov_account_code') !!}
            </th>
            <td>
                {!! Form::select('prov_account_code', $accounts, old('prov_account_code', $saleCostItem->prov_account_code), ['class' => 'form-control select2', 'disabled']) !!}
            </td>
        </tr>
    </table>
@stop
