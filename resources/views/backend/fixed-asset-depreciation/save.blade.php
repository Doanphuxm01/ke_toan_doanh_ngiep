@extends('backend.master')
@section('title')
{!! trans('system.action.edit') !!} - {!! trans('menus.fixed-asset-depreciation.label') !!}
@stop
@section('head')
    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.fixed-asset-depreciation.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.fixed-asset-depreciation.label') !!}</a></li>
        </ol>
    </section>
    
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.fixed-asset-depreciation.save', $voucher->id), 'method' => 'POST', 'role' => 'form', 'files' => true]) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <table class='table borderless'>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('fixed_asset_depreciation.description') !!}
                        </th>
                        <td colspan=3>
                            {!! Form::text('voucher_description', old('voucher_description', $voucher->description), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_asset_depreciation.voucher_date') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('voucher_date', old('voucher_date', \Carbon\Carbon::createFromTimestamp(strtotime($voucher->voucher_date))->format('d-m-Y')), ['class' => 'form-control voucher_date']) !!}       
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('fixed_asset_depreciation.month') !!}
                        </th>
                        <td>
                            {!! Form::text('month', old('month', $voucher->month), ['class' => 'form-control', 'readonly']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_asset_depreciation.year') !!}
                        </th>
                        <td>
                            {!! Form::text('year', old('year', $voucher->year), ['class' => 'form-control', 'readonly']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_asset_depreciation.voucher_number') !!}
                        </th>
                        <td>
                            {!! Form::text('voucher_number', old('voucher_number', $voucher->voucher_number), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.determine_cost_of_allocation') !!}</h3>
                <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div style="margin-right:20px"> 
                            <table>
                                <tr>
                                    <th class="text-center" style=" min-width: 100px;">
                                        {!! trans('fixed_assets.asset_code') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 250px;">
                                        {!! trans('fixed_assets.asset_name') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 150px;">
                                        {!! trans('fixed_assets.monthly_depreciation_value') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 150px;">
                                        {!! trans('fixed_asset_depreciation.value_in_cost') !!}
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div> 
                        <?php $allocation_info = App\Models\FixedAsset::getAllocationInfo($fixed_asset->asset_code) ?>    
                            <table>
                                <tr>
                                    <th class="text-center" style=" min-width: 200px;">
                                        {!! trans('fixed_assets.use_department') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 80px;">
                                        {!! trans('fixed_assets.allocation_rate') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 150px;">
                                        {!! trans('fixed_asset_depreciation.money_amount') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 150px;">
                                        {!! trans('fixed_assets.expense_account') !!}
                                    </th>
                                    <th class="text-center" style=" min-width: 150px;">
                                        {!! trans('fixed_assets.cost_item') !!}
                                    </th>
                                </tr>
                            </table>
                            <br>
                        </div>
                    </div>
                    @foreach($fixed_assets as $fixed_asset)
                    <div class='cost' style="display: flex;">
                        <div style="margin-right:20px"> 
                            <table>
                                <tr>
                                    <td style="white-space: nowrap; min-width: 100px;">
                                        {!! Form::text('asset_code[]', old('asset_code', $fixed_asset->asset_code), ['class' => 'form-control', 'readonly' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 250px;">
                                        {!! Form::text('asset_name[]', old('asset_name', $fixed_asset->asset_name), ['class' => 'form-control', 'readonly' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::text('monthly_depreciation_value[]', old('monthly_depreciation_value',  App\Models\FixedAssetAllocationCost::getValue($fixed_asset->asset_code, $voucher->id)), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::text('value_in_cost[]', old('value_in_cost', App\Models\FixedAssetAllocationCost::getValue($fixed_asset->asset_code, $voucher->id)), ['class' => 'form-control money', 'required']) !!}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-7"> 
                        <?php $allocation_cost = App\Models\FixedAsset::getAllocationCost($fixed_asset->asset_code, $voucher->id) ?>     
                            <table>
                                @foreach($allocation_cost as $allo_cost)
                                <tr>
                                    {!! Form::hidden('allo_cost_id[]', old('allo_cost_id', $allo_cost->id), ['class' => 'form-control', 'readonly' ]) !!}
                                    {!! Form::hidden('a_c[]', old('a_c', $allo_cost->asset_code), ['class' => 'form-control']) !!}
                                    {!! Form::hidden('department_id[]', old('department_id', $allo_cost->department_id), ['class' => 'form-control', 'readonly']) !!}
                                    <td style="white-space: nowrap; min-width: 200px;"> 
                                        {!! Form::text('use_department[]', old('use_department', App\Models\Department::getDepartment($allo_cost->department_id)), ['class' => 'form-control', 'readonly' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 80px;">
                                        {!! Form::text('allocation_rate[]', old('allocation_rate', $allo_cost->allocation_rate), ['class' => 'form-control', 'readonly' ]) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::text('money_amount[]', old('money_amount', $allo_cost->money_amount), ['class' => 'form-control money', 'readonly']) !!}
                                    </td>   
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::select('expense_account[]', ['' => trans('system.dropdown_choice')] + App\Models\Account::getExpenseAccounts(session('current_company')), old('expense_account', $allo_cost->expense_account), ['class' => 'form-control select2 expense_account']) !!}
                                        {!! Form::hidden('depreciation_account[]', old('depreciation_account', $fixed_asset->depreciation_account), ['class' => 'form-control']) !!}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 150px;">
                                        {!! Form::select('cost_item[]',['' => trans('system.dropdown_choice')] + App\Models\CostItem::getActiveCostItem(session('current_company')), old('cost_item', $allo_cost->cost_item), ['class' => 'form-control select2', ]) !!}
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            <br>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.accounting') !!}</h3>
            </div>
            <div class="box-body">
               <div class="table-responsive">
                    <table>
                        <thead>
                            <tr>
                                <th style="white-space: nowrap; min-width: 300px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.description') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.debit_account') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.credit_account') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.money_amount') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.debit_object') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 250px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.object_name') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.credit_object') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 250px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.object_name') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('cost_items.cost_item') !!}
                                </th>
                            </tr>
                        </thead>
                        <tbody class='accounting'>
                        @foreach($accounting as $item)
                            <tr>
                            {!! Form::hidden('accounting_id[]', old('accounting_id', $item->id), ['class' => 'form-control']) !!}
                                <td style="width:20%">
                                    {!! Form::text('description[]', old('description', $item->description), ['class' => 'form-control']) !!}
                                </td>
                                <td style="width:10%"> 
                                    {!! Form::text('debit_account[]', old('debit_account', $item->debit_account), ['class' => 'form-control', 'readonly']) !!}
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('credit_account[]', old('credit_account', $item->credit_account), ['class' => 'form-control', 'readonly']) !!}
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('amount[]', old('amount', $item->amount), ['class' => 'form-control money', 'readonly']) !!}
                                </td>   
                                <td style="width:10%">
                                    {!! Form::select('debit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption(), old('debit_object_type[]', $item->debit_object_type), ['class' => 'form-control select2']) !!}
                                </td>
                                <td style="width:10%">
                                @if($item->debit_object_type == "DEPARTMENTS")
                                    {!! Form::select('debit_object[]', App\Models\Accounting::departmentType(session('current_company')), old('debit_object[]', $item->debit_object), ['class' => 'form-control select2']) !!}
                                @elseif($item->debit_object_type == "USERS")
                                    {!! Form::select('debit_object[]', App\Models\Accounting::userType(session('current_company')), old('debit_object[]', $item->debit_object), ['class' => 'form-control select2']) !!}
                                @elseif($item->debit_object_type == "PARTNERS")
                                    {!! Form::select('debit_object[]', App\Models\Accounting::partnerType(session('current_company')), old('debit_object[]', $item->debit_object), ['class' => 'form-control select2']) !!}                               
                                @else
                                    {!! Form::select('debit_object[]', ['' => trans('system.dropdown_choice')], old('debit_object[]', $item->debit_object), ['class' => 'form-control select2']) !!}
                                @endif
                                </td>
                                <td style="width:10%">
                                    {!! Form::select('credit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption(), old('credit_object_type[]', $item->credit_object_type), ['class' => 'form-control select2']) !!}
                                </td>
                                <td style="width:10%">
                                @if($item->credit_object_type == "DEPARTMENTS")
                                    {!! Form::select('credit_object[]', App\Models\Accounting::departmentType(session('current_company')), old('credit_object[]', $item->credit_object), ['class' => 'form-control select2']) !!}
                                @elseif($item->credit_object_type == "USERS")  
                                    {!! Form::select('credit_object[]', App\Models\Accounting::userType(session('current_company')), old('credit_object[]', $item->credit_object), ['class' => 'form-control select2']) !!}  
                                @elseif($item->credit_object_type == "PARTNERS")
                                    {!! Form::select('credit_object[]', App\Models\Accounting::partnerType(session('current_company')), old('credit_object[]', $item->credit_object), ['class' => 'form-control select2']) !!}
                                @else
                                {!! Form::select('credit_object[]', ['' => trans('system.dropdown_choice')], old('credit_object[]', $item->credit_object), ['class' => 'form-control select2']) !!}
                                @endif
                                </td>
                                <td style="width:10%">
                                    {!! Form::text('c_a[]', old('c_a', $item->cost_item), ['class' => 'form-control']) !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" style="font-size:18px" >
                                    {!! trans('fixed_asset_depreciation.total') !!}:
                                </th>
                                <th class="text-right" style="font-size:18px" id="total" >
                                </th> 
                                <th colspan="5" >
                                </th>
                            </tr>
                        </tfoot>
                    </table>   
                </div>
                <br>
                <div class="text-center">
                    {!! HTML::link(route( 'admin.fixed-asset-depreciation.index' ), trans('system.action.cancel'), [ 'class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.confirm'), ['id' => 'save-btn', 'class' => 'btn btn-primary btn-flat']) !!}
                </div>
            </div>
        </div>
        
        {!! Form::close() !!}
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $(".select2").select2({
        dropdownAutoWidth: true,
    });
    $('.voucher_date').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'removeMaskOnSubmit': true});
</script>
<script>
    var d = new Date();
    var y = d.getFullYear();
    $('select[name="year"]').yearselect({start:2010}).select2();
    
    
    $(document).ready(function(){
        var total_amount = 0;
        $("input[name='money_amount[]']").each(function() {
            var money_amount = $(this).val().replace(/,/g, "");
            total_amount += Number(money_amount);
        }); 
        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        $("input[name='value_in_cost[]']").each(function() {
            var value_in_cost = $(this).val().replace(/,/g, "");
            var a = $(this).closest('.cost').find(("input[name='allocation_rate[]']"))
            a.each(function() {
                var allocation_rate = $(this).val() 
                b = $(this).closest('tr').find(("input[name='money_amount[]']"))
                b.val(value_in_cost*allocation_rate/100)
            });
        });            
    });    
        
    $(document).on("change","input[name='voucher_description']",function() {  
        var description = $(this).val()
        $("input[name='description[]']").each(function(){
            $(this).val(description)
        })
    });
    
    $(("input[name='value_in_cost[]']")).change(function () {
        var month = $("input[name='month']").val()
        var year = $("input[name='year']").val()
        $("input[name='value_in_cost[]']").each(function() {
            var value_in_cost = $(this).val().replace(/,/g, "");
            $(this).closest('tr').find(("input[name='monthly_depreciation_value[]']")).val($(this).val())
            var a = $(this).closest('.cost').find(("input[name='allocation_rate[]']"))
            a.each(function() {
                var allocation_rate = $(this).val() 
                b = $(this).closest('tr').find(("input[name='money_amount[]']"))
                b.val(value_in_cost*allocation_rate/100)
            });
        });       
        $(".accounting tr").remove();
        var array = new Array();
        $("select[name='expense_account[]']").each(function(key, value) {
            debit_account = $(value).val();
            amount = $(value).closest('tr').find(("input[name='money_amount[]']"));
            amount = parseFloat($(amount).val().replace(/,/g, ""));
            cost_item = $(value).closest('tr').find(("select[name='cost_item[]']")).val();
            credit_account = $(value).closest('td').find(("input[name='depreciation_account[]']")).val();
            array.push({debit_account:debit_account, amount:amount, cost_item: cost_item, credit_account: credit_account})
        });
        var newArray = groupBy(array, ['debit_account', 'cost_item', 'credit_account'], 'amount');
        jQuery.each(newArray, function(index, value) {
            var amount = $(this).closest('tr').find(("input[name='money_amount[]']"))
            var html = '<tr>'
                html += '<td>'
                html += '<input name="description[]" type="text" class="form-control description" value="Khấu hao TSCĐ tháng ' + month + ' năm ' + year + '">'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="debit_account[] type="text" class="form-control" value="' + value.debit_account + '" readonly>'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="credit_account[]  type="text" class="form-control" value="' + value.credit_account + '" readonly>'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="amount[] type="text" class="form-control money" value="' + value.amount + '" readonly>'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '{!! Form::select('debit_object[]', ['' => trans('system.dropdown_choice')] + array('Partners' => $partners, 'Departments' => $departments, 'Staffs' => $staffs), old('debit_object'), ['class' => 'form-control select2']) !!}'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="debit_object_name[] type="text" class="form-control">'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '{!! Form::select('credit_object[]', ['' => trans('system.dropdown_choice')] + array('Partners' => $partners, 'Departments' => $departments, 'Staffs' => $staffs), old('credit_object'), ['class' => 'form-control select2']) !!}'
                html += '</td>'
                html += '<td  style="width:10%">'
                html += '<input name="credit_object_name[] type="text" class="form-control">'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="c_i[] type="text" class="form-control" value="' + value.cost_item + '" readonly>'
                html += '</td>'
                html += '</tr>'
            $(".accounting").append(html)
            $(".select2").select2({
                dropdownAutoWidth: true
            });
            $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'rightAlign': false,  'removeMaskOnSubmit': true});
        });
        var total_amount = 0;
        $("input[name='money_amount[]']").each(function() {
            var money_amount = $(this).val().replace(/,/g, "");
            total_amount += Number(money_amount);
        }); 
        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    })

    $(("select[name='expense_account[]'], select[name='cost_item[]']")).change(function () {
        var month = $("input[name='month']").val()
        var year = $("input[name='year']").val()
        console.log(month, year)
        $(".accounting tr").remove();
        var array = new Array();
        $("select[name='expense_account[]']").each(function(key, value) {
            debit_account = $(value).val();
            amount = $(value).closest('tr').find(("input[name='money_amount[]']"));
            amount = parseFloat($(amount).val().replace(/,/g, ""));
            cost_item = $(value).closest('tr').find(("select[name='cost_item[]']")).val();
            credit_account = $(value).closest('td').find(("input[name='depreciation_account[]']")).val();
            array.push({debit_account:debit_account, amount:amount, cost_item: cost_item, credit_account: credit_account})
        });
        var newArray = groupBy(array, ['debit_account', 'cost_item', 'credit_account'], 'amount');
        jQuery.each(newArray, function(index, value) {
            var amount = $(this).closest('tr').find(("input[name='money_amount[]']"))
            var html = '<tr>'
                html += '<td>'
                html += '<input name="description[]" type="text" class="form-control description" value="Khấu hao TSCĐ tháng ' + month + ' năm ' + year + '">'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="debit_account[] type="text" class="form-control" value="' + value.debit_account + '" readonly>'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="credit_account[]  type="text" class="form-control" value="' + value.credit_account + '" readonly>'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="amount[] type="text" class="form-control money" value="' + value.amount + '" readonly>'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '{!! Form::select('debit_object[]', ['' => trans('system.dropdown_choice')] + array('Partners' => $partners, 'Departments' => $departments, 'Staffs' => $staffs), old('debit_object'), ['class' => 'form-control select2']) !!}'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="debit_object_name[] type="text" class="form-control">'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '{!! Form::select('credit_object[]', ['' => trans('system.dropdown_choice')] + array('Partners' => $partners, 'Departments' => $departments, 'Staffs' => $staffs), old('credit_object'), ['class' => 'form-control select2']) !!}'
                html += '</td>'
                html += '<td  style="width:10%">'
                html += '<input name="credit_object_name[] type="text" class="form-control">'
                html += '</td>'
                html += '<td style="width:10%">'
                html += '<input name="c_i[] type="text" class="form-control" value="' + value.cost_item + '" readonly>'
                html += '</td>'
                html += '</tr>'
            $(".accounting").append(html)
            $(".select2").select2({
                dropdownAutoWidth: true
            });
            $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'rightAlign': false,  'removeMaskOnSubmit': true});
        });       
        var total_amount = 0;
        $("input[name='money_amount[]']").each(function() {
            var money_amount = $(this).val().replace(/,/g, "");
            total_amount += Number(money_amount);
        }); 
        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    })
    $(document).on("change", "select[name='debit_object_type[]']",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find(("select[name='debit_object[]']"));
        var option = $(this).closest('tr').find(("select[name='debit_object[]'] option"));
        $.ajax({
            url: "{!! route('admin.get-debit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
    $(document).on("change", "select[name='credit_object_type[]']",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find(("select[name='credit_object[]']"));
        var option = $(this).closest('tr').find(("select[name='credit_object[]'] option"));
        $.ajax({
            url: "{!! route('admin.get-credit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
    </script>
    <script>
        function groupBy(array, groups, valueKey) {
        var map = new Map;
        groups = [].concat(groups);
        return array.reduce((r, o) => {
            groups.reduce((m, k, i, { length }) => {
                var child;
                if (m.has(o[k])) return m.get(o[k]);
                if (i + 1 === length) {
                    child = Object
                        .assign(...groups.map(k => ({ [k]: o[k] })), { [valueKey]: 0 });
                    r.push(child);
                } else {
                    child = new Map;
                }
                m.set(o[k], child);
                return child;
            }, map)[valueKey] += +o[valueKey];
            return r;
        }, [])
    };
    </script>
@stop
