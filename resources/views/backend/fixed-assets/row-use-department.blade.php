<tr>
    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
        <a href="javascript:void(0);" class="btn btn-xs btn-default remove-row"><i class="text-danger fas fa-minus"></i></a>
    </td>
    <td style="white-space: nowrap; min-width: 170px;;">
        {!! Form::select('use_department[]', ['' => trans('system.dropdown_choice')] + App\Models\Department::getDepartments(session('current_company')), old('use_department[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 180px;">
        {!! Form::text('allocation_rate[]', old('allocation_rate[]'), ['class' => 'form-control', 'required']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 320px;;">
        {!! Form::select('expense_account[]', ['' => trans('system.dropdown_choice')] + App\Models\Account::getExpenseAccounts(session('current_company')), old('expense_account[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 180px">
        {!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + App\Models\CostItem::getActiveCostItem(session('current_company')), old('cost_item[]'), ['class' => 'form-control select2 cost_item']) !!}
        <a href="javascript:void(0);" class="btn btn-xs btn-default show-add" style="font-size:10px">
            <i class="text-success fa fa-plus"></i>
        </a>
    </td>
 </tr>
