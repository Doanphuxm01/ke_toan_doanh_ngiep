@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('menus.fixed-assets.label') !!}
@stop
@section('head')
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('menus.fixed_assets.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.fixed_assets.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.fixed-assets.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', trans('fixed_assets.asset_name')) !!}
                        {!! Form::text('asset_name', Request::input('asset_name'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', trans('fixed_assets.original_price')) !!}
                        {!! Form::text('original_price', Request::input('original_price'), ['id' => 'original_price', 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', trans('fixed_assets.use_time')) !!}
                        {!! Form::text('use_time', Request::input('use_time'), ['id' => 'use_time', 'class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', trans('fixed_assets.use_department')) !!}
                        {!! Form::select('use_department', ['' => trans('system.dropdown_choice')] + App\Models\Department::getDepartments(session('current_company')), old('use_department', Request::input('use_department')), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('from', trans('system.from')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('from_date', Request::input('from_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('to', trans('system.to')) !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('to_date', Request::input('to_date'), ['class' => 'form-control pull-right date_picker']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2','style' => 'width: 100%'])!!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label')) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{!! route('admin.fixed-assets.create') !!}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $fixed_assets->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($fixed_assets) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($fixed_assets->currentPage() - 1) * $fixed_assets->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $fixed_assets->count()) . ' ( ' . trans('system.total') . ' ' . $fixed_assets->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" style="border-collapse: collapse;">
                    <thead style="background: #3C8DBC; color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">{!! trans('fixed_assets.voucher_creation_date') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.asset_code') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.asset_name') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.use_department') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.original_price') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.depreciation_value') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.use_time') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('fixed_assets.monthly_depreciation_value') !!} </th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('system.status.label') !!}</th>
                            <th style="text-align: center; vertical-align: middle;"> {!! trans('system.action.label') !!} </th>
                        </tr>
                    </thead>
                    <tbody class="borderless">
                        @foreach ($fixed_assets as $item)
                            <tr class="treegrid-{!! $item->id !!}">
                                <td style="text-align: center; vertical-align: middle;">
                                    {!! date_format(date_create($item->voucher_creation_date), "d/m/Y") !!}
                                </td>
                                <td  style="text-align: center; vertical-align: middle;">
                                    {!! $item->asset_code !!}
                                </td>
                                <td  style="vertical-align: middle;">
                                    <a href="{!! route('admin.fixed-assets.show', $item->id) !!}">{!! $item->asset_name !!}</a>
                                </td>
                                <td  style=" vertical-align: middle;">
                                @for ($i=0; $i < count(App\Models\FixedAssetAllocationInformation::getUseDepartment($item->asset_code)) ; $i++) 
                                    &#8226; {!! App\Models\FixedAssetAllocationInformation::getUseDepartment($item->asset_code)[$i] !!}<br>
                                @endfor
                                    
                                </td>
                                <td  style="text-align: right; vertical-align: middle;">
                                    {!! number_format($item->original_price) !!}
                                </td>
                                <td  style="text-align: right; vertical-align: middle;">
                                    {!! number_format($item->depreciation_value) !!}
                                </td>
                                <td style="text-align: right; vertical-align: middle;">
                                {!! $item->use_time !!}
                                </td>
                                <td style="text-align: right; vertical-align: middle;">
                                {!! number_format($item->monthly_depreciation_value) !!}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    @if($item->status == 0)
                                        <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                    @elseif($item->status == 1)
                                        <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                    @endif
                                </td>
                                @if(count(App\Models\FixedAsset::checkUpdateDelete($item->asset_code)) <= 0)
                                <td style="text-align: center; vertical-align: middle;width:80px">
                                    <a href="{!! route('admin.fixed-assets.edit', $item->id) !!}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}">
                                        <i class="text-warning glyphicon glyphicon-edit"></i>
                                    </a>
                                    &nbsp;
                                    <a href="javascript:void(0)" link="{!! route('admin.fixed-assets.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                        <i class="text-danger glyphicon glyphicon-remove"></i>
                                    </a>
                                </td>
                                @else
                                <td style="text-align: center; vertical-align: middle;width:80px">
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="alert alert-info"> {!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.select2').select2();
</script>
<script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
