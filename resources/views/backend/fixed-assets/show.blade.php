@extends('backend.master')
@section('title')
{!! trans('system.action.detail') !!} - {!! trans('menus.fixed-assets.label') !!}
@stop
@section('head')
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
@stop
@section('content')
    <section class="content-header">
        <h1>
        
            {!! trans('menus.fixed-assets.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            @if($fixed_asset->status)
                <label class="label label-success">
                    {!! trans('system.status.active') !!}
                </label>
            @else
                <label class="label label-default">
                    {!! trans('system.status.deactive') !!}
                </label>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.fixed-assets.label') !!}</a></li>
        </ol>
    </section>
    <section class="content overlay">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <table class='table borderless'>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.asset_code') !!}
                        </th>
                        <td  style="width: 35%;">
                            {!! Form::text('asset_code', old('asset_code', $fixed_asset->asset_code), ['class' => 'form-control', 'disabled']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.voucher_number') !!}
                        </th>
                        <td  style="width: 35%;">
                            {!! Form::text('voucher_number', old('voucher_number', $fixed_asset->voucher_number), ['class' => 'form-control', 'disabled']) !!}
                        </td>
                    </tr>
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('fixed_assets.asset_name') !!}
                        </th>
                        <td style="width: 35%;">
                            {!! Form::text('asset_name', old('asset_name', $fixed_asset->asset_name), ['class' => 'form-control', 'disabled']) !!}
                        </td>
                        
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('fixed_assets.voucher_creation_date') !!}
                        </th>
                        <td style="width: 35%;">
                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('voucher_creation_date', old('voucher_creation_date', \Carbon\Carbon::createFromTimestamp(strtotime($fixed_asset->voucher_creation_date))->format('d-m-Y')), ['class' => 'form-control date_picker', 'disabled']) !!}             
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.depreciation_information') !!}</h3>
            </div>
            <div class="box-body">
                <table class='table borderless'>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.original_price_account') !!}
                        </th>
                        <td>
                            {!! Form::text('original_price_account', old('original_price_account', $fixed_asset->original_price_account), ['class' => 'form-control', 'disabled']) !!}
                        </td>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('fixed_assets.begin_use_date') !!}
                        </th>
                        <td style="width: 35%;">
                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('begin_use_date', old('begin_use_date', \Carbon\Carbon::createFromTimestamp(strtotime($fixed_asset->begin_use_date))->format('d-m-Y')), ['class' => 'form-control date_picker', 'disabled']) !!}       
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.depreciation_account') !!}
                        </th>
                        <td>
                            {!! Form::text('depreciation_account', old('depreciation_account', $fixed_asset->depreciation_account), ['class' => 'form-control', 'disabled']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.monthly_depreciation_value') !!}
                        </th>
                        <td>
                            {!! Form::text('monthly_depreciation_value', old('monthly_depreciation_value', $fixed_asset->monthly_depreciation_value), ['class' => 'form-control money', 'disabled' ]) !!}
                        </td> 
                    </tr>
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('fixed_assets.original_price') !!}
                        </th>
                        <td style="width: 35%;">
                            {!! Form::text('original_price', old('original_price', $fixed_asset->original_price), ['class' => 'form-control money', 'disabled']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.yearly_depreciation_value') !!}
                        </th>
                        <td>
                            {!! Form::text('yearly_depreciation_value', old('yearly_depreciation_value', $fixed_asset->yearly_depreciation_value), ['class' => 'form-control money', 'disabled']) !!}
                        </td>      
                    </tr>
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('fixed_assets.depreciation_value') !!}
                        </th>
                        <td style="width: 35%;">
                            {!! Form::text('depreciation_value', old('depreciation_value', $fixed_asset->depreciation_value), ['class' => 'form-control money', 'disabled']) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.accumulated_depreciation') !!}
                        </th>
                        <td>
                            {!! Form::text('accumulated_depreciation', old('accumulated_depreciation', $fixed_asset->accumulated_depreciation), ['class' => 'form-control money', 'disabled']) !!}
                        </td>     
                    </tr>  
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('fixed_assets.use_time') !!}
                        </th>
                        <td style="width: 35%;">
                            {!! Form::text('use_time', old('use_time', $fixed_asset->use_time), ['class' => 'form-control money', 'disabled']) !!}
                        </td>
                        
                        <th class="table_right_middle">
                            {!! trans('fixed_assets.residual_value') !!}
                        </th>
                        <td style="width: 35%;">  
                            {!! Form::text('residual_value', old('residual_value', $fixed_asset->residual_value), ['class' => 'form-control money', 'disabled']) !!}
                        </td>
                    </tr>     
                </table>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('expenses_prepaid.voucher_collection') !!}</h3>                
            </div>
            <div class="box-body">
                <table class="table table-bordered " id="voucher_table">
                    <thead style="background: #3C8DBC;color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;">{!! trans('expenses_prepaid.voucher_date') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;">{!! trans('expenses_prepaid.voucher_no') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 40%;">{!! trans('expenses_prepaid.desc') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;">{!! trans('fixed_asset_depreciation.money_amount') !!}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($vouchers) == 0)
                            <tr>
                                <td colspan="4" style="font-size: 16px;" class="text-center" ><b>{!! trans('expenses_prepaid.no_data') !!}</b></td>
                            </tr>
                        @else
                        @foreach($vouchers as $voucher)
                            <tr>
                                <td style="text-align: center">
                                    {!! date_format(date_create($voucher->voucher_date), "d/m/Y") !!}
                                </td>
                                <td style="text-align: center">
                                    <a href="{!! route('admin.fixed-asset-depreciation.show', $voucher->id) !!}">{!! $voucher->voucher_number !!}</a>
                                </td>
                                <td>
                                    {!! $voucher->description !!}  
                                </td>
                                <td style="text-align: right" class="amount">
                                    {!! number_format(App\Models\FixedAssetDepreciation::amount($fixed_asset->asset_code, $voucher->id), 2) !!}
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot style="background: #3C8DBC;color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;"> {!! trans('fixed_asset_depreciation.total') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 40%;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 20%;" id="total" class="money"></th>
                        </tr>
                        </tfoot>
                </table>
            </div>
        </div>
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script>
    $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.select2').select2();
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
</script>
<script>
    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
    var total = 0
    $(document).ready(function(){
        jQuery.each($('.amount'), function(){
            total += Number($(this).text().replace(/,/g, ""))
        })
        $('#total').text(formatNumber(total))
        
    })
</script>

@stop
