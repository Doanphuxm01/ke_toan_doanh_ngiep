@extends('backend.master')
@section('title')
{!! trans('system.action.create') !!} - {!! trans('menus.fixed-assets.label') !!}
@stop
@section('head')
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
        .voucher_modal tr td {
            vertical-align: middle;
            padding: 10px !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
        
            {!! trans('menus.fixed_assets.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.fixed_assets.label') !!}</a></li>
        </ol>
    </section>
    
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.fixed-assets.store'), 'role' => 'form']) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.asset_code') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('asset_code', old('asset_code'), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('voucher_number', old('voucher_number', $voucher_number), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.asset_name') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('asset_name', old('asset_name'), ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.voucher_creation_date') !!}</b>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('voucher_creation_date', old('voucher_creation_date'), ['class' => 'form-control date_picker', 'maxlength' => 50,]) !!}       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.depreciation_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.original_price_account') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('original_price_account', ['' => trans('system.dropdown_choice')] + $accounts, old('original_price_account'), ['class' => 'form-control select2', 'required', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.begin_use_date') !!}</b>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('begin_use_date', old('begin_use_date'), ['class' => 'form-control date_picker', 'maxlength' => 50,]) !!}      
                        </div>
                             
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.depreciation_account') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('depreciation_account', ['' => trans('system.dropdown_choice')] + $accounts, old('depreciation_account'), ['class' => 'form-control select2', 'required', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.monthly_depreciation_value') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('monthly_depreciation_value', old('monthly_depreciation_value'), ['class' => 'form-control money', 'maxlength' => 50, 'readonly', 'required', ]) !!}      
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.original_price') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('original_price', old('original_price'), ['class' => 'form-control money', 'maxlength' => 50, 'required']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.yearly_depreciation_value') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('yearly_depreciation_value', old('yearly_depreciation_value'), ['class' => 'form-control money', 'maxlength' => 50, 'readonly', 'required']) !!}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.depreciation_value') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('depreciation_value', old('depreciation_value'), ['class' => 'form-control money', 'maxlength' => 50, 'required']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.accumulated_depreciation') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('accumulated_depreciation', old('accumulated_depreciation'), ['class' => 'form-control money', 'maxlength' => 50, ]) !!}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.use_time') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('use_time', old('use_time'), ['id' => 'use_time', 'class' => 'form-control money', 'maxlength' => 50, 'required']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_assets.residual_value') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('residual_value', old('residual_value'), ['class' => 'form-control money', 'maxlength' => 50, 'readonly', 'required']) !!}
                    </div>
                </div>
                <br>    
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
            <div style="display:inline-block">
                <h3 class="box-title">{!! trans('expenses_prepaid.voucher_collection') !!}</h3>
                
                </div>
                <div style="display:inline-block; margin-left: 50px;">
                <button type="button" class="btn btn-primary show">Search</button>
                </div>
                <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered " id="voucher_table" style="display: none">
                    <thead id="thead" style="background: #3C8DBC;color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('expenses_prepaid.voucher_date') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('expenses_prepaid.voucher_no') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 300px;">{!! trans('expenses_prepaid.desc') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('fixed_asset_depreciation.debit_account') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('fixed_asset_depreciation.credit_account') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;">{!! trans('fixed_asset_depreciation.money_amount') !!}</th>
                        </tr>
                    </thead>
                    <tbody class="voucher">
                    </tbody>
                    <tfoot id="tfoot" style="background: #3C8DBC;color: white;">
                        <tr>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;"> {!! trans('fixed_asset_depreciation.total') !!}</th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 300px;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;"></th>
                            <th style="text-align: center; vertical-align: middle; min-width: 100px;" id="total" class="money"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.allocation_information') !!}</h3>
            </div>
            <div class="box-body">
                <table class="table plane">
                    <thead style="background: #3C8DBC; color: white;">
                        <tr>
                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                {!! trans('system.action.label') !!}
                            </td>
                            <td style="white-space: nowrap; width: 20%;">
                                {!! trans('fixed_assets.use_department') !!}
                            </td>
                            <td style="white-space: nowrap; width: 20%;">
                                {!! trans('fixed_assets.allocation_rate') !!}
                            </td>
                            <td style="white-space: nowrap; width: 35%;">
                                {!! trans('fixed_assets.expense_account') !!}
                            </td>
                            <td style="white-space: nowrap; width: 20%;">
                                {!! trans('fixed_assets.cost_item') !!}  
                            </td>
                        </tr>
                    </thead>
                    <?php $use_department = old('use_department', []); ?>
                        @if (count($use_department))
                        <?php $allocation_rate = old('allocation_rate'); $expense_account = old('expense_account'); $cost_item = old('cost_item')?>
                        @for ($i = 0; $i < count($use_department); $i++)
                        <tr>
                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                            @if ($i > 0)
                                <a href="javascript:void(0);" class="btn btn-xs btn-default remove-row" title="{!! trans('fixed_assets.remove_this_row') !!}">
                                    <i class="text-danger fas fa-minus"></i>
                                </a>
                            @else
                                <a href="javascript:void(0);" class="btn btn-xs btn-default add-row" title="{!! trans('fixed_assets.create_new_row') !!}">
                                    <i class="text-success fa fa-plus"></i>
                                </a>
                            @endif
                            </td>
                            <td style="white-space: nowrap; width: 20%;">
                            {!! Form::select("use_department[$i]", $departments, old("use_department[$i]", $use_department[$i]), ['class' => 'form-control select2', 'required','style' => 'width: 100%']) !!}
                            </td>
                            <td style="white-space: nowrap;width: 20%;">
                                {!! Form::text('allocation_rate[]', old('allocation_rate[]', $allocation_rate[$i]), ['class' => 'form-control', 'required']) !!}
                            </td>
                            <td style="white-space: nowrap; width: 35%;">
                                {!! Form::select("expense_account[$i]",  $expense_accounts, old("expense_account[$i]", $expense_account[$i]), ['class' => 'form-control select2', 'required', 'style' => 'width: 100%']) !!}
                            </td>
                            <td style="white-space: nowrap; width: 20%;">
                                
                                {!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + $cost_items , old('cost_item[]'), ['class' => 'form-control select2 cost_item', 'style' => 'min-width:200px']) !!}
                                
                                <a href="javascript:void(0);" class="btn btn-xs btn-default show-add" style="font-size:10px; width:20px" title="{!! trans('fixed_assets.create_new_cost_item') !!}">
                                    <i class="text-success fa fa-plus"></i>
                                </a>
                     
                            </td>
                        </tr>
                    @endfor
                    @else
                    <tr>
                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                            <a href="javascript:void(0);" class="btn btn-xs btn-default add-row" title="{!! trans('fixed_assets.create_new_row') !!}">
                                <i class="text-success fa fa-plus"></i>
                            </a>
                        </td>
                        <td style="white-space: nowrap; width: 20%;">
                            {!! Form::select('use_department[]', ['' => trans('system.dropdown_choice')] + $departments, old('use_department[]'), ['class' => 'form-control select2', 'style' => 'width: 100%', 'readonly']) !!}
                        </td>
                        <td style="white-space: nowrap; width: 20%;">
                            {!! Form::text('allocation_rate[]', old('allocation_rate[]'), ['class' => 'form-control', 'required']) !!}
                        </td>
                        <td style="white-space: nowrap; width: 35%;">
                            {!! Form::select('expense_account[]', ['' => trans('system.dropdown_choice')] + $expense_accounts, old('expense_account[]'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                        </td>
                        <td style="white-space: nowrap; width: 20%;">
                            
                            {!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + $cost_items , old('cost_item[]'), ['class' => 'form-control select2 cost_item', 'style' => 'min-width:200px']) !!}
                            
                            <a href="javascript:void(0);" class="btn btn-xs btn-default show-add" style="font-size:10px; width:20px" title="{!! trans('fixed_assets.create_new_cost_item') !!}">
                                <i class="text-success fa fa-plus"></i>
                            </a>
                            
                        </td>
                    </tr>
                    @endif
                </table>  
                <br>
                <div class="text-center">
                    {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                    {!! trans('system.status.active') !!}                
                </div>
                <br>
                <div class="text-center">  
                    <a href="javascript:void(0);" class="btn-flat btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat', 'id' => 'save-btn']) !!}
                </div>
            </div>
        </div>
        <div id="confirm-save" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"> {!! trans('expenses_prepaid.confirm') !!} </h4>
                    </div>
                    <div class="modal-body">
                        <p style="font-size: 18px;"> {!! trans('fixed_assets.save_message') !!} </p>
                        <p style="font-size: 18px"> {!! trans('expenses_prepaid.confirm_save') !!} </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float: left">{!! trans('system.confirm_no') !!}</button>
                        <button type="submit" class="btn btn-primary" style="float: right" id="confirm_save">{!! trans('system.confirm_yes') !!}</button>
                                          
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        @include('backend.expenses-prepaid.modal_cost_item')
        @include('backend.expenses-prepaid.modal_voucher')
    </section>
    
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
           
            $('.date_picker').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $('.date_picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $('.voucher_date').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    

    $('.select2').select2({
        dropdownAutoWidth: true,
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
</script>
<script>
    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
    function checkSubmit() {
                    if(Number($('input[name="depreciation_value"]').val().replace(/,/g, "")) != total){
                        $(this).attr('type', 'button')
                    }
                }

    var total = 0
    $(document).on('click', '#confirm_voucher', function(){
        $('#voucher').modal('hide')
        var checkbox = $("input:checkbox[name='checkbox']:checked")
        if(checkbox.length > 0){
            $("#voucher_table").css("display", "");
        }
        if(checkbox.length == 0){
            $("#voucher_table").css("display", "none");
        }
        $('.voucher').empty();
        total = 0;
        jQuery.each($("input:checkbox[name='checkbox']:checked"), function(index, value) {
            var id = $(this).closest('tr').find('.id').val()
            var voucher_date = $(this).closest('tr').find('.voucher_date').text()
            var voucher_no = $(this).closest('tr').find('.voucher_no').text()
            var description = $(this).closest('tr').find('.description').text()
            var debit_account = $(this).closest('tr').find('.debit_account').text()
            var credit_account = $(this).closest('tr').find('.credit_account').text()
            var amount = $(this).closest('tr').find('.amount').text()
            var html = '<tr>'
                html += '<td style="display:none">'
                html += '<input name="accounting_id[]" class="form-control" type="text" value="'+id+'">'
                html += '</td>'
                html += '<td>'
                html += '<input class="form-control" type="text" value="'+voucher_date+'">'
                html += '</td>'
                html += '<td>'
                html += '<input name="voucher_no[]" class="form-control" type="text" value="'+voucher_no+'">'
                html += '</td>'
                html += '<td>'
                html += '<input name="description[]" class="form-control" type="text" value="'+description+'">'
                html += '</td>'
                html += '<td>'
                html += '<input name="debit_account[]" class="form-control" type="text" value="'+debit_account+'">'
                html += '</td>'
                html += '<td>'
                html += '<input name="credit_account[]" class="form-control" type="text" value="'+credit_account+'">'
                html += '</td>'
                html += '<td>'
                html += '<input name="amount[]" class="form-control money" type="text" value="'+formatNumber(amount)+'">'
                html += '</td>'
                html += '</tr>'
            $('.voucher').append(html)
            total += Number(amount.replace(/,/g, ""))
            $(".money").inputmask({'alias': 'decimal', 'digits' : 2, 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
        });  
        $(".money").inputmask({'alias': 'decimal', 'digits' : 2, 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
        $("#total").text((formatNumber(total)))
        $("#amount").val(total).keyup()
    })
    $(document).on("change","select[name='original_price_account']",function(e) {  
        checkSubmit();
        $(".voucher").empty()
        $('input[name="from_date"]').val("")
        $('input[name="to_date"]').val("")
        $("#voucher_table").css("display", "none");
        total = 0;
        $("#amount").val(total).keyup()
        $(".voucher_modal").empty()
        var allocation_account = $(this).val()
        $.ajax({
            url: "{!! route('admin.get-vouchers') !!}",
            data: { allocation_account: allocation_account },
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                jQuery.each(data, function(index, value) {
                    var date = value.voucher_date.split('-')
                    var voucher_date = date[2] + "/" + date[1] + "/" + date[0]
                    var html = '<tr>'
                        html += '<td>'
                        html += '{!! Form::checkbox('checkbox',  old('checkbox')) !!}'
                        html += '</td>'
                        html += '<input type="hidden" class="form-control id" type="text" value="'+value.id+'">'
                        html += '<td class="voucher_date">'+voucher_date+'</td>'
                        html += '<td class="voucher_no">'+value.voucher_no+'</td>'
                        html += '<td class="description">'+value.desc+'</td>'
                        html += '<td class="debit_account">'+value.debit_account+'</td>'
                        html += '<td class="credit_account">'+value.credit_account+'</td>'
                        html += '<td class="amount money">'+formatNumber(value.amount)+'</td>'
                        html += '</tr>'
                    $('.voucher_modal').append(html)
                    $(".money").inputmask({'alias': 'decimal', 'digits' : 2, 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                });   
                $("#total").text((formatNumber(total)))
            },
            error: function(obj, status, err) {
            }
        })
    });
    $(document).on("click","#search",function(e) {  
        checkSubmit();
        var from_date = $('input[name="from_date"]').val()
            to_date = $('input[name="to_date"]').val();
            allocation_account = $('select[name="allocation_account"]').val()
            accounting_id = new Array()
            jQuery.each($('input[name="accounting_id[]"]'), function(index, value) {
                  accounting_id.push($(this).val())
                });  
        $.ajax({
            url: "{!! route('admin.search-vouchers') !!}",
            data: { from_date: from_date, to_date: to_date, allocation_account: allocation_account, accounting_id: accounting_id  },
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                $(".voucher_modal").empty()
                jQuery.each(data, function(index, value) {
                    var date = value.voucher_date.split('-')
                    var voucher_date = date[2] + "/" + date[1] + "/" + date[0]
                    var html = '<tr>'
                        html += '<td>'   
                        if(value.status == 'checked'){
                            html += '{!! Form::checkbox('checkbox', 1, old('checkbox', 1)) !!}'
                        }
                        else{
                            html += '{!! Form::checkbox('checkbox',  old('checkbox')) !!}'
                        }
                        html += '</td>'
                        html += '<input type="hidden" class="form-control id" type="text" value="'+value.id+'">'
                        html += '<td class="voucher_date">'+    voucher_date+'</td>'
                        html += '<td class="voucher_no">'+value.voucher_no+'</td>'
                        html += '<td class="description">'+value.desc+'</td>'
                        html += '<td class="debit_account">'+value.debit_account+'</td>'
                        html += '<td class="credit_account">'+value.credit_account+'</td>'
                        html += '<td class="amount money">'+formatNumber(value.amount)+'</td>'
                        html += '</tr>'
                    $('.voucher_modal').append(html)
                    $(".money").inputmask({'alias': 'decimal', 'digits' : 2, 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                });   
                $("#total").text((formatNumber(total)))
            },
            error: function(obj, status, err) {
            }
        })
    });
</script>
    <script>
        !function ($) {
            $(function() {
                var result = ''
                var result = ''
                $(document).on("click",".show",function(e) {
                    $('#voucher').modal('show');
                });
                $(document).keydown(function(event) { 
                    if (event.keyCode == 27) { 
                        $('#voucher').modal('hide');
                    }
                });

                $(document).on("click",".show-add",function(e) {
                    $('#add-cost-item').modal('show');
                    result = $(this).closest('td').find($(".cost_item"));  
                });
                $(document).on("click","#save-add",function(e) {  
                    var new_cost_item = $("input[name='new_cost_item']").val(),
                        description = $("input[name='description']").val(),
                        interpretation = $("textarea[name='interpretation']").val()
                    $.ajax({
                        url: "{!! route('admin.storeCostItem') !!}",
                        data: { new_cost_item: new_cost_item, description: description, interpretation: interpretation },
                        type: 'POST',
                        datatype: 'json',
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        success: function(data) {
                            $('#add-cost-item').modal('hide');
                            $("input[name='new_cost_item']").val("");
                            $("input[name='description']").val("");
                            $("textarea[name='interpretation']").val();
                            result.append($("<option selected></option>").attr("value", data.cost_item).text(data.cost_item));
                        },
                        error: function(obj, status, err) {
                            var error = $.parseJSON(obj.responseText);
                            toastr.error(error.message, '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                        }
                    })
                });
                
                $(document).on("click","#save-btn",function(event) { 
                    var percent = 0;
                    $("input[name='allocation_rate[]']").each(function() {
                    percent += Number($(this).val());
                    });
                    if(percent < 100){
                        toastr.error("{!! trans('fixed_assets.must_100') !!}");
                        $('#save-btn').prop('disabled', true);
                    } 
                   
                    if(Number($('input[name="depreciation_value"]').val().replace(/,/g, "")) != total){
                        $(this).attr('type', 'button')
                        $('#confirm-save').modal('show');
                    }
                    else{
                        $(this).attr('type', 'submit')
                    }
                });

                $(document).on("keyup","input[name='allocation_rate[]']",function() {
                    var percent = 0;
                    $("input[name='allocation_rate[]']").each(function() {
                        percent += Number($(this).val());
                    });
                    if(percent > 100){
                        toastr.error("{!! trans('fixed_assets.should_100') !!}");
                        $('#save-btn').prop('disabled', true);
                    }
                    else{
                        $('#save-btn').prop('disabled', false);
                    }  
                });

                $("input[name='depreciation_value'], input[name='use_time']").keyup(function () {
                    var depreciation_value = parseFloat($("input[name='depreciation_value']").val().replace(/,/g, ""));
                    var use_time = parseFloat($("input[name='use_time']").val().replace(/,/g, ""));
                    var year = use_time/12;
                    var monthly_depreciation_value = depreciation_value/use_time;
                    var yearly_depreciation_value = monthly_depreciation_value*12;
                    if(isNaN(depreciation_value) || isNaN(use_time)){
                        $("input[name='yearly_depreciation_value']").val(0);
                        $("input[name='monthly_depreciation_value']").val(0);
                    }
                    else{
                        $("input[name='yearly_depreciation_value']").val(Math.round(yearly_depreciation_value));
                        $("input[name='monthly_depreciation_value']").val(Math.round(monthly_depreciation_value));  
                    }
                })

                $("input[name='depreciation_value'], input[name='accumulated_depreciation']").keyup(function () {
                    var depreciation_value = parseFloat($("input[name='depreciation_value']").val().replace(/,/g, ""));
                    var accumulated_depreciation = parseFloat($("input[name='accumulated_depreciation']").val().replace(/,/g, ""));
                    if(accumulated_depreciation){
                        $("input[name='residual_value']").val(depreciation_value-accumulated_depreciation);   
                    }
                    else{
                        $("input[name='residual_value']").val(depreciation_value);
                    }   
                })
              
                $(document).on('click', '.add-row', function(event) {
                    var percent = 0;
                    $("input[name='allocation_rate[]']").each(function() {
                        percent += Number($(this).val());
                    });
                    if(percent >= 100){
                        toastr.error("{!! trans('fixed_assets.already_100') !!}");
                    }
                    else{
                        var newRow = $("<tr>");
                        var cols = "";
                        
                        cols += '<td style="white-space: nowrap; vertical-align: middle; text-align: center;">';
                        cols += '<div>'
                        cols += '<a href="javascript:void(0);" class="btn btn-xs btn-default remove-row"><i class="text-danger fas fa-minus" title="{!! trans('fixed_assets.remove_this_row') !!}"></i></a>';
                        cols += '</div>'
                        cols += '</td>';
                        cols += '<td style="white-space: nowrap; width: 20%;">';
                        cols += '{!! Form::select('use_department[]', ['' => trans('system.dropdown_choice')] + $departments, old('use_department[]'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}';
                        cols += '</td>';
                        cols += '<td style="white-space: nowrap; width: 20%;">';
                        cols += '{!! Form::text('allocation_rate[]', old('allocation_rate[]'), ['class' => 'form-control', 'required']) !!}';
                        cols += '</td>';
                        cols += '<td style="white-space: nowrap; width: 35%;">';
                        cols += '{!! Form::select('expense_account[]', ['' => trans('system.dropdown_choice')] + $expense_accounts, old('expense_account[]'), ['class' => 'form-control select2', 'style' => 'width: 100%']) !!}';
                        cols += '</td>';
                        cols += '<td style="white-space: nowrap; width: 20%;">';
                        cols += '<div  style="display: inline-block">'
                        cols += '{!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + $cost_items, old('cost_item[]'), ['class' => 'form-control select2 cost_item',]) !!}';
                        cols += '</div>'
                        cols += '<div style="display: inline-block">'
                        cols += '<a href="javascript:void(0);" class="btn btn-xs btn-default show-add" style="font-size:10px" title="{!! trans('fixed_assets.create_new_cost_item') !!}">'
                        cols += '<i class="text-success fa fa-plus"></i>'
                        cols += '</a>'
                        cols += '</div>'
                        cols += '</td>';           
                    newRow.append(cols);
                    $("table.plane").append(newRow);    
                    }
                    $(".select2").select2();
                });
                $(document).on('click', '.remove-row', function(event) {
                    var length = $(this).closest('tbody').find('tr').length;
                    $(this).closest('tr').fadeOut(150, function(){ $(this).remove();});
                });
            });
        }(window.jQuery);  
    </script>
    <script>
        jQuery(document).ready(function(a){var b=a(document.body),c=!1,d=!1;b.on("keydown",function(a){var b=a.keyCode?a.keyCode:a.which;16==b&&(c=!0)}),b.on("keyup",function(a){var b=a.keyCode?a.keyCode:a.which;16==b&&(c=!1)}),b.on("mousedown",function(b){d=!1,1!=a(b.target).is('[class*="select2"]')&&(d=!0)}),b.on("select2:opening",function(b){d=!1,a(b.target).attr("data-s2open",1)}),b.on("select2:closing",function(b){a(b.target).removeAttr("data-s2open")}),b.on("select2:close",function(b){var e=a(b.target);e.removeAttr("data-s2open");var f=e.closest("form-1"),g=f.has("[data-s2open]").length;if(0==g&&0==d){var h=f.find(":input:enabled:not([readonly], input:hidden, button:hidden, textarea:hidden)").not(function(){return a(this).parent().is(":hidden")}),i=null;if(a.each(h,function(b){var d=a(this);if(d.attr("id")==e.attr("id"))return i=c?h.eq(b-1):h.eq(b+1),!1}),null!==i){var j=i.siblings(".select2").length>0;j?i.select2("open"):i.focus()}}}),b.on("focus",".select2",function(b){var c=a(this).siblings("select");0==c.is("[disabled]")&&0==c.is("[data-s2open]")&&a(this).has(".select2-selection--single").length>0&&(c.attr("data-s2open",1),c.select2("open"))})});
        !function ($) {
            $(function() {
                $(".select2").select2().on("select2:close", function (e) {
                    var html = $(this).closest('td').next().find('select');
                    if (html.is("select")) {
                        $(this).closest('td').next().find('select').select2("open");
                    } else {
                        $(this).closest('td').next().find('input').focus();
                    }
                });
                $(document).on('focus', '.select2.select2-container', function (e) {
                    var isOriginalEvent = e.originalEvent
                    var isSingleSelect = $(this).find(".select2-selection--single").length > 0
                    if (isOriginalEvent && isSingleSelect) {
                        $(this).siblings('select:enabled').select2('open');
                    }
                });
            });
        }(window.jQuery);
    </script>

@stop