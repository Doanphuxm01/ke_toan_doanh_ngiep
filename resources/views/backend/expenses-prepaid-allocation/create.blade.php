@extends('backend.master')
@section('title')
{!! trans('system.action.create') !!} - {!! trans('menus.expenses-prepaid-allocation.label') !!}
@stop
@section('head')
    
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.2.6/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <style>
        .container {
            margin-top: 15px;
        }
        button, .btn {
            outline: none !important;
        }
        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }
        .container label {
            margin-top: 5px;
        }
        .box {
            border-top: none!important;
        }
        table {
            font-size: 13px;
        }
        table tbody tr td {
            vertical-align: middle;
            padding: 0 !important;
        }
    </style>
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('menus.expenses-prepaid-allocation.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.fixed-assets.index') !!}">{!! trans('menus.expenses-prepaid-allocation.label') !!}</a></li>
        </ol>
    </section>
    
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                    <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <section class="content overlay">
        {!! Form::open(['url' => route('admin.expenses-prepaid-allocation.store'), 'role' => 'form']) !!}
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_assets.general_information') !!}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_depreciation.description') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('voucher_description', old('voucher_description'), ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_depreciation.voucher_date') !!}</b>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('voucher_date', old('voucher_date'), ['class' => 'form-control voucher_date', 'readonly']) !!}       
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <b>{!! trans('fixed_asset_depreciation.month') !!}</b>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('month', old('month'), ['class' => 'form-control', 'readonly']) !!}
                            </div>
                            <div class="col-md-2">
                                <b>{!! trans('fixed_asset_depreciation.year') !!}</b>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('year', old('year'), ['class' => 'form-control', 'readonly']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <b>{!! trans('fixed_asset_depreciation.voucher_number') !!}</b>
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('voucher_no', old('voucher_no'), ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.determine_cost_of_allocation') !!}</h3>
                <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <div style="display: flex;">      
                        <div style="margin-right:20px"> 
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 100px;">
                                            {!! trans('expenses_prepaid.code') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 300px;">
                                            {!! trans('expenses_prepaid.name') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('expenses_prepaid.periodical_allocation_value') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.value_in_cost') !!}
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div>  
                            <table class="table table-bordered">
                                <thead style="background: #3C8DBC; color: white;">
                                    <tr>
                                        <th class="text-center" style=" min-width: 200px;">
                                            {!! trans('fixed_assets.use_department') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 80px;">
                                            {!! trans('fixed_assets.allocation_rate') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_asset_depreciation.money_amount') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.expense_account') !!}
                                        </th>
                                        <th class="text-center" style=" min-width: 150px;">
                                            {!! trans('fixed_assets.cost_item') !!}
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="asset">
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{!! trans('fixed_asset_depreciation.accounting') !!}</h3>
            </div>
            <div class="box-body">
               <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead style="background: #3C8DBC; color: white;">
                            <tr>
                                <th style="white-space: nowrap; min-width: 300px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.description') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.debit_account') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.credit_account') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.money_amount') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.debit_object') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 250px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.object_name') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.credit_object') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 250px;" class="text-center">
                                    {!! trans('fixed_asset_depreciation.object_name') !!}
                                </th>
                                <th style="white-space: nowrap; min-width: 150px;" class="text-center">
                                    {!! trans('cost_items.cost_item') !!}
                                </th>
                            </tr>
                        </thead>
                        <tbody class='accounting'>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" style="font-size:18px" >
                                    {!! trans('fixed_asset_depreciation.total') !!}:
                                </th>
                                <th class="text-right" style="font-size:18px" id="total" >
                                </th> 
                                <th colspan="5" >
                                </th>
                            </tr>
                        </tfoot>
                    </table>   
                </div>
                <br>
                <div class="text-center">
                    {!! HTML::link(route( 'admin.expenses-prepaid-allocation.index' ), trans('system.action.cancel'), [ 'class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.save'), ['id' => 'save-btn', 'class' => 'btn btn-primary btn-flat']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        <div id="select_month_year" class="modal fade" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{!! trans('expenses_prepaid.select_period') !!}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Tháng</p>
                                {!! Form::select('month', ['01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05' ,'06' => '06' , '07' => '07' , '08' => '08' , '09' => '09', '10' => '10' ,'11' => '11' ,'12' => '12'], old('month',date('m')), ['class' => 'form-control select2', 'required', 'style'=>'width:50%']) !!}
                            </div>
                            <div class="col-md-6">
                                <p>Năm</p>
                                {!! Form::select('year',['' => '--'], old('year'), ['class' => 'form-control select2', 'style'=>'width:50%']) !!}
                            </div>
                        </div>      
                        <br>
                        <div class="row" style="text-align:center;">
                            {!! Form::submit(trans('system.action.confirm'), ['class' => 'btn btn-primary btn-flat', 'id' => 'confirm']) !!}
                            <a href="{!! route('admin.expenses-prepaid-allocation.index') !!}" class="btn-flat btn btn-default">{!! trans('system.action.cancel') !!}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        
    </section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
<script>
    $(".select2").select2({
        dropdownAutoWidth: true,
        
    });
    $('.voucher_date').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
        },
        autoUpdateInput:false,
        singleDatePicker: true,
        showDropdowns: true,
        }, function(start, end, label) {       
    });
    $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});

</script>
<script>
    var d = new Date();
    var y = d.getFullYear();
    $('select[name="year"]').yearselect({start:2010}).select2();
    $(document).ready(function(){
        $("#select_month_year").modal('show')
    });    
    
    $(document).on("change","input[name='voucher_description']",function() {  
        var description = $(this).val()
        $("input[name='description[]']").each(function(){
            $(this).val(description)
        })
    });
    $(document).on("click","#confirm",function() { 
        $("#confirm").attr("disabled", true);
        var month = $("select[name='month']").val()
        var year = $("select[name='year']").val()
        $.ajax({
            url: "{!! route('admin.expenses-prepaid-allocation') !!}",
            data: { month: month, year: year },
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.status == 'success'){
                    var lastday = function(y,m){
                        return  new Date(y, m +1, 0).getDate();
                        }
                    var day = lastday(year,month-1)
                    $("input[name='voucher_date']").val(day+"/"+month+"/"+year);
                    $("input[name='month']").val(data.month)
                    $("input[name='year']").val(data.year)
                    $("input[name='voucher_description']").val('Phân bổ CPTT tháng ' + month + ' năm ' + year)
                    $("input[name='voucher_no']").val('PBCPTT' + month + '/' + year)
                    $('#select_month_year').modal('hide') 
                    
                    jQuery.each(data.expenses_prepaid, function(index, value) {
                        var amount = $(this).closest('tr').find(("input[name='money_amount[]']"))
                        var html = '<div class="cost" style="display: flex;">'
                            html += '<div style="margin-right:20px">'
                            html += '<table>'
                            html += '<tr>'
                            html += '<td style="white-space: nowrap; min-width: 100px;">'
                            html += '<input name="expenses_prepaid_code[]" type="text" class="form-control" value="' + value.expenses_prepaid_code + '" readonly>'
                            html += '</td>'
                            html += '<td style="white-space: nowrap; min-width: 300px;">'
                            html += '<input name="expenses_prepaid_name[]" type="text" class="form-control" value="' + value.expenses_prepaid_name + '" readonly>'
                            html += '</td>'
                            html += '<td style="white-space: nowrap; min-width: 150px;">'
                            html += '<input name="periodical_allocation_value[]" type="text" class="form-control money" value="' + value.periodical_allocation_value + '" readonly>'
                            html += '</td>'
                            html += '<td style="white-space: nowrap; min-width: 150px;">'
                            html += '<input name="value_in_cost[]" type="text" class="form-control money" value="' + value.periodical_allocation_value + '" >'
                            html += '</td>'
                            html += '</tr>'
                            html += '</table>'
                            html += '</div>'
                            html += '<div> '
                            html += '<table>'
                            jQuery.each(value.expenses_prepaid_allocation, function(index1, value1) {
                                html += '<input name="e_p[]" type="hidden" class="form-control" value="' + value1.expenses_prepaid_id + '" >'
                                html += '<input name="department_id[]" type="hidden" class="form-control" value="' + value1.department_id + '" >'
                                html += '<tr>'
                                html += '<td style="white-space: nowrap; min-width: 200px;">'
                                html += '<input name="use_department[]" type="text" class="form-control" value="' + value1.department.name + '" readonly>'
                                html += '</td>'
                                html += '<td style="white-space: nowrap; min-width: 80px;">'
                                html += '<input name="allocation_rate[]" type="text" class="form-control" value="' + value1.allocation_rate + '" readonly>'
                                html += '</td>'
                                html += '<td style="white-space: nowrap; min-width: 150px;">'
                                html += '<input name="money_amount[]" type="text" class="form-control money" value="" readonly>'
                                html += '</td>'
                                html += '<td class="expense_account'+value1.id+'" style="white-space: nowrap; min-width: 150px;">'
                                html += '{!! Form::select('expense_account[]', ['' => trans('system.dropdown_choice')] + App\Models\Account::getExpenseAccounts(session('current_company')) , old('expense_account'), ['class' => 'form-control select2']) !!}'
                                html += '<input name="allocation_account[]" type="hidden" class="form-control" value="' + value.allocation_account + '" >'
                                html += '</td>'
                                html += '<td class="cost_item'+value1.id+'" style="white-space: nowrap; min-width: 150px;">'
                                html += '{!! Form::select('cost_item[]', ['' => trans('system.dropdown_choice')] + App\Models\CostItem::getActiveCostItem(session('current_company')) , old('cost_item'), ['class' => 'form-control select2']) !!}'
                                html += '</td>'
                                html += '</tr>'
                            });
                            html += '</table>'
                            html += '<br>'
                            html += '</div>'
                            html += '</div>'
                        $(".asset").append(html)
                        
                        $(".select2").select2({
                            dropdownAutoWidth: true
                        });
                        $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                    });
                    jQuery.each(data.expenses_prepaid, function(index, value) {
                        jQuery.each(value.expenses_prepaid_allocation, function(index1, value1) {
                            $('.expense_account'+value1.id).find('select').val(value1.expense_account)
                            $('.cost_item'+value1.id).find('select').val(value1.cost_item)
                        });
                    });
                    $("input[name='value_in_cost[]']").each(function() {
                        var value_in_cost = $(this).val().replace(/,/g, "");
                        var a = $(this).closest('.cost').find(("input[name='allocation_rate[]']"))
                        a.each(function() {
                            var allocation_rate = $(this).val() 
                            b = $(this).closest('tr').find(("input[name='money_amount[]']"))
                            b.val(value_in_cost*allocation_rate/100)
                        });
                    }); 
                    var array = new Array();
                    $("select[name='expense_account[]']").each(function(key2, value2) {
                        debit_account = $(value2).val();
                        amount = $(value2).closest('tr').find(("input[name='money_amount[]']"));
                        amount = parseFloat($(amount).val().replace(/,/g, ""));
                        cost_item = $(value2).closest('tr').find(("select[name='cost_item[]']")).val();
                        credit_account = $(value2).closest('td').find(("input[name='allocation_account[]']")).val();
                        array.push({debit_account:debit_account, amount:amount, cost_item: cost_item, credit_account: credit_account})
                    });
                    var newArray = groupBy(array, ['debit_account', 'cost_item', 'credit_account'], 'amount');
                    jQuery.each(newArray, function(index3, value3) {
                        var amount = $(this).closest('tr').find(("input[name='money_amount[]']"))
                        var html = '<tr>'
                            html += '<td>'
                            html += '<input name="description[]" type="text" class="form-control description" value="Phân bổ CPTT tháng ' + month + ' năm ' + year + '">'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="debit_account[] type="text" class="form-control" value="' + value3.debit_account + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="credit_account[]  type="text" class="form-control" value="' + value3.credit_account + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="amount[] type="text" class="form-control money" value="' + value3.amount + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('debit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('debit_object_type'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('debit_object_id[]', ['' => trans('system.dropdown_choice')], old('debit_object_id'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td  style="width:10%">'
                            html += '{!! Form::select('credit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('credit_object_type'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('credit_object_id[]', ['' => trans('system.dropdown_choice')], old('credit_object_id'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="c_i[] type="text" class="form-control" value="' + value3.cost_item + '" readonly>'
                            html += '</td>'
                            html += '</tr>' 
                        $(".accounting").append(html)
                        $(".select2").select2({
                            dropdownAutoWidth: true
                        });
                        $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                        });
                        var total_amount = 0;
                        $("input[name='money_amount[]']").each(function() {
                            var money_amount = $(this).val().replace(/,/g, "");
                            total_amount += Number(money_amount);
                        }); 
                        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                }
                else if(data.status == "error"){
                    toastr.error("{!! trans('fixed_asset_depreciation.no_asset') !!}");
                    $("#confirm").attr("disabled", false);
                }
                else if(data.status == "created"){
                    toastr.error("{!! trans('fixed_asset_depreciation.created') !!}");
                    $("#confirm").attr("disabled", false);
                }
                else if(data.status == "cannot"){
                    toastr.error("{!! trans('fixed_asset_depreciation.cannot') !!}");
                    $("#confirm").attr("disabled", false);
                }
                
                },
            error: function(obj, status, err) {
            }
        })
    });
    $(document).on("change","input[name='value_in_cost[]']",function() { 
        var month = $("select[name='month']").val()
        var year = $("select[name='year']").val()
        $("input[name='value_in_cost[]']").each(function() {
            var value_in_cost = $(this).val().replace(/,/g, "");
            $(this).closest('tr').find(("input[name='monthly_depreciation_value[]']")).val($(this).val())
            var a = $(this).closest('.cost').find(("input[name='allocation_rate[]']"))
            a.each(function() {
                var allocation_rate = $(this).val() 
                b = $(this).closest('tr').find(("input[name='money_amount[]']"))
                b.val(value_in_cost*allocation_rate/100)
            });
        });       
        $(".accounting tr").remove();
        var array = new Array();
        $("select[name='expense_account[]']").each(function(key, value) {
            debit_account = $(value).val();
            amount = $(value).closest('tr').find(("input[name='money_amount[]']"));
            amount = parseFloat($(amount).val().replace(/,/g, ""));
            cost_item = $(value).closest('tr').find(("select[name='cost_item[]']")).val();
            credit_account = $(value).closest('td').find(("input[name='allocation_account[]']")).val();
            array.push({debit_account:debit_account, amount:amount, cost_item: cost_item, credit_account: credit_account})
        });
        var newArray = groupBy(array, ['debit_account', 'cost_item', 'credit_account'], 'amount');
        jQuery.each(newArray, function(index3, value3) {
                        var amount = $(this).closest('tr').find(("input[name='money_amount[]']"))
                        var html = '<tr>'
                            html += '<td>'
                            html += '<input name="description[]" type="text" class="form-control description" value="Phân bổ CPTT tháng ' + month + ' năm ' + year + '">'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="debit_account[] type="text" class="form-control" value="' + value3.debit_account + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="credit_account[]  type="text" class="form-control" value="' + value3.credit_account + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="amount[] type="text" class="form-control money" value="' + value3.amount + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('debit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('debit_object_type'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('debit_object_id[]', ['' => trans('system.dropdown_choice')], old('debit_object_id'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td  style="width:10%">'
                            html += '{!! Form::select('credit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('credit_object_type'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('credit_object_id[]', ['' => trans('system.dropdown_choice')], old('credit_object_id'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="c_i[] type="text" class="form-control" value="' + value3.cost_item + '" readonly>'
                            html += '</td>'
                            html += '</tr>' 
                        $(".accounting").append(html)
                        $(".select2").select2({
                            dropdownAutoWidth: true
                        });
                        $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                        });
        var total_amount = 0;
        $("input[name='money_amount[]']").each(function() {
            var money_amount = $(this).val().replace(/,/g, "");
            total_amount += Number(money_amount);
        }); 
        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    });

    $(document).on("change","select[name='expense_account[]'], select[name='cost_item[]']",function() { 
        var month = $("select[name='month']").val()
        var year = $("select[name='year']").val()
        $(".accounting tr").remove();
        var array = new Array();
        $("select[name='expense_account[]']").each(function(key, value) {
            debit_account = $(value).val();
            amount = $(value).closest('tr').find(("input[name='money_amount[]']"));
            amount = parseFloat($(amount).val().replace(/,/g, ""));
            cost_item = $(value).closest('tr').find(("select[name='cost_item[]']")).val();
            credit_account = $(value).closest('td').find(("input[name='allocation_account[]']")).val();
            array.push({debit_account:debit_account, amount:amount, cost_item: cost_item, credit_account: credit_account})
        });
        var newArray = groupBy(array, ['debit_account', 'cost_item', 'credit_account'], 'amount');
        jQuery.each(newArray, function(index3, value3) {
                        var amount = $(this).closest('tr').find(("input[name='money_amount[]']"))
                        var html = '<tr>'
                            html += '<td>'
                            html += '<input name="description[]" type="text" class="form-control description" value="Phân bổ CPTT tháng ' + month + ' năm ' + year + '">'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="debit_account[] type="text" class="form-control" value="' + value3.debit_account + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="credit_account[]  type="text" class="form-control" value="' + value3.credit_account + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="amount[] type="text" class="form-control money" value="' + value3.amount + '" readonly>'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('debit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('debit_object_type'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('debit_object_id[]', ['' => trans('system.dropdown_choice')], old('debit_object_id'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td  style="width:10%">'
                            html += '{!! Form::select('credit_object_type[]', ['' => trans('system.dropdown_choice')] + App\Defines\FixedAssetDepreciation::getTypeForOption() , old('credit_object_type'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '{!! Form::select('credit_object_id[]', ['' => trans('system.dropdown_choice')], old('credit_object_id'), ['class' => 'form-control select2']) !!}'
                            html += '</td>'
                            html += '<td style="width:10%">'
                            html += '<input name="c_i[] type="text" class="form-control" value="' + value3.cost_item + '" readonly>'
                            html += '</td>'
                            html += '</tr>' 
                        $(".accounting").append(html)
                        $(".select2").select2({
                            dropdownAutoWidth: true
                        });
                        $(".money").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0,  'removeMaskOnSubmit': true});
                        });
        var total_amount = 0;
        $("input[name='money_amount[]']").each(function() {
            var money_amount = $(this).val().replace(/,/g, "");
            total_amount += Number(money_amount);
        }); 
        $("#total").html(String(total_amount).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));       
    });
    $(document).on("change", "select[name='debit_object_type[]']",function() { 
        
        var type = $(this).val()
        var result = $(this).closest('tr').find(("select[name='debit_object_id[]']"));
        var option = $(this).closest('tr').find(("select[name='debit_object_id[]'] option"));
        if($(this).val() == ""){
            option.remove()
            result.append($("<option></option>").attr("value", "").text("{!! trans('system.dropdown_choice') !!}"));
        }
        $.ajax({
            url: "{!! route('admin.get-debit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
    $(document).on("change", "select[name='credit_object_type[]']",function() { 
        var type = $(this).val()
        var result = $(this).closest('tr').find(("select[name='credit_object_id[]']"));
        var option = $(this).closest('tr').find(("select[name='credit_object_id[]'] option"));
        if($(this).val() == ""){
            option.remove()
            result.append($("<option></option>").attr("value", "").text("{!! trans('system.dropdown_choice') !!}"));
        }
        $.ajax({
            url: "{!! route('admin.get-credit-object') !!}",
            data: { type: type},
            type: 'POST',
            datatype: 'json',
            headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
            success: function(data) {
                if(data.type == "DEPARTMENTS"){
                    option.remove();
                    jQuery.each(data.departments, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
                if(data.type == "USERS"){
                    option.remove();
                    jQuery.each(data.users, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.fullname));
                    });
                }
                if(data.type == "PARTNERS"){
                    option.remove();
                    jQuery.each(data.partners, function(index, value) {
                    result.append($("<option></option>").attr("value", value.id).text(value.name));
                    });
                }
            },
            error: function(obj, status, err) {
            }
        })
    });
    </script>
    <script>
        function groupBy(array, groups, valueKey) {
        var map = new Map;
        groups = [].concat(groups);
        return array.reduce((r, o) => {
            groups.reduce((m, k, i, { length }) => {
                var child;
                if (m.has(o[k])) return m.get(o[k]);
                if (i + 1 === length) {
                    child = Object
                        .assign(...groups.map(k => ({ [k]: o[k] })), { [valueKey]: 0 });
                    r.push(child);
                } else {
                    child = new Map;
                }
                m.set(o[k], child);
                return child;
            }, map)[valueKey] += +o[valueKey];
            return r;
        }, [])
    };
    </script>
@stop
