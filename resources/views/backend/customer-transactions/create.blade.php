@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('customer_transactions.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/dropzone/dropzone.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('customer_transactions.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.customer-transactions.index') !!}">{!! trans('customer_transactions.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.customer-transactions.store'), 'role' => 'form']) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('customers.label') !!}
                </th>
                <td style="width: 35%;">
                    {!! Form::select('customer', empty($customer) ? ['' => trans('system.dropdown_choice')] : $customer, old('customer'), ["class" => "form-control"]) !!}
                </td>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('customer_transactions.amount') !!}
                </th>
                <td>
                    <div class="input-group">
                        {!! Form::text('amount', old('amount'), ['class' => 'form-control', 'required']) !!}
                        <div class="input-group-addon">
                            VNĐ
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customer_transactions.note') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('note', old('note'), ['class' => 'form-control']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('customer_transactions.files') !!}
                </th>
                <td colspan="3">
                    <div class="dropzone" id="dropzone" product_id="{!! $product->id !!}">
                        <div class="dz-preview dz-file-preview">
                            <div class="dz-details">
                                <div class="dz-filename"><span data-dz-name></span></div>
                                <div class="dz-size" data-dz-size></div>
                                <img data-dz-thumbnail />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    {!! HTML::link(route( 'admin.customer-transactions.index' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/dropzone/dropzone.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("input[name='amount']").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'digits': 0, 'removeMaskOnSubmit': true, 'allowMinus': true, 'max': 99999999});
                $("select[name='customer']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    width: '100%',
                    // allowClear: true,
                    minimumResultsForSearch: 10,
                    ajax: {
                        url: "{!! route('admin.customers.search') !!}?type=ctv",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    // console.log(item.level, '{!! \App\Define\Customer::LEVEL_PARTNER !!}', '{!! \App\Define\Customer::LEVEL_DROPSHIP !!}')
                                    if (item.level == '{!! \App\Define\Customer::LEVEL_PARTNER !!}' || item.level == '{!! \App\Define\Customer::LEVEL_DROPSHIP !!}') {
                                        return {
                                            text: item.code + " - " + item.fullname,
                                            id: item.id
                                        }
                                    }
                                })
                            };
                        }
                    }
                }).on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    $(this).closest('form').submit();
                });

                Dropzone.options.dropzone = {
                    url: "{!! route('admin.customer-transactions.save-file') !!}",
                    headers: { "X-CSRF-TOKEN": "{!! csrf_token() !!}" },
                    method: "POST",
                    data: { id: this.element},
                    acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf,.rar,.zip",
                    paramName: "file",
                    maxFilesize: 10, // MB
                    init: function() {
                        this.on("sending", function(file, xhr, data) {
                            // data.append('id', this.element.attributes.product_id.value);
                        });
                        this.on("success", function(file, response) {
                            file.fileId = response.id;
                            file.name = response.title;
                            toastr.success(response.message);
                        });
                        this.on("addedfile", function(file) {
                            var removeButton = Dropzone.createElement("<a class='dz-remove' href='javascript:void(0);' data-dz-remove=''>Xóa file</a>");
                            var _this = this;
                            removeButton.addEventListener("click", function(e) {
                                e.preventDefault();
                                e.stopPropagation();
                                _this.removeFile(file);
                                $.ajax({
                                    url: "{!! route('admin.customer-transactions.delete-file') !!}",
                                    data: { id: file.fileId},
                                    headers: { "X-CSRF-TOKEN": "{!! csrf_token() !!}" },
                                    type: 'DELETE',
                                    success: function (data) {
                                        toastr.success(data.message);
                                    },
                                    error: function (data) {
                                        toastr.error(data.message);
                                    }
                                });
                            });
                            file.previewElement.appendChild(removeButton);
                        });
                    }
                };
            });
        }(window.jQuery);
    </script>
@stop