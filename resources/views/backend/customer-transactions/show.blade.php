@extends('backend.master')
@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('customer_transactions.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('customer_transactions.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
            <label class="label label-success">
                {!! date("d/m/Y H:i", strtotime($transaction->created_at)) !!}
            </label>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.customer-transactions.index') !!}">{!! trans('customer_transactions.label') !!}</a></li>
        </ol>
    </section>
    <table class='table borderless'>
        <tr>
            <th class="table_right_middle">
                {!! trans('customers.label') !!}
            </th>
            <td>
                {!! Form::text('fullname', old('fullname', $customer->fullname), ['class' => 'form-control', 'disabled']) !!}
            </td>
            <th class="table_right_middle">
                {!! trans('customer_transactions.amount') !!}
            </th>
            <td>
                <div class="input-group">
                    {!! Form::text('amount', old('amount', $transaction->amount), ['class' => 'form-control', 'disabled']) !!}
                    <div class="input-group-addon">
                        VNĐ
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('customer_transactions.note') !!}
            </th>
            <td colspan="3">
                {!! Form::text('note', old('note', $customer->note), ['class' => 'form-control', 'disabled']) !!}
            </td>
        </tr>
        <tr>
            <th class="table_right_middle">
                {!! trans('customer_transactions.files') !!}
            </th>
            <td colspan="3">
                <?php $files = $transaction->medias()->get(); ?>
                @if ($files->count())
                    @foreach ($files as $file)
                        <?php
                            // $ext = strtolower(pathinfo($file->title, PATHINFO_EXTENSION));
                            $createdDay = date("Ymd", strtotime($file->created_at));
                        ?>
                        <div class="col-md-2 text-center">
                            <a href="{!! asset(config('upload.customer-transactions') . $createdDay . '/' . $file->title) !!}" title="{!! $file->title !!}" target="_blank"><i class="fas fa-file-download"></i> {!! $file->title !!}</a>
                        </div>
                        {{-- @if ($ext == "pdf")
                            <div class="col-md-4 text-center">
                                <object data="{!! asset(config('upload.customer-transactions') . $createdDay . '/' . $file->title) !!}?#zoom=50" type="application/pdf" width="100%" height="400" title="{!! $file->title !!}">
                                        alt : {!! $file->title !!}
                                </object>
                            </div>
                        @elseif (in_array($ext, ['png', 'img', 'jpg', 'jpeg', 'gif']))
                            <div class="col-md-4 text-center">
                                <img src="{!! asset(config('upload.customer-transactions') . '/' . $createdDay . '/' . $file->title) !!}" title="{!! $file->title !!}" alt="{!! $file->title !!}">
                            </div>
                        @else
                            <div class="col-md-4 text-center">
                                <a href="{!! asset(config('upload.customer-transactions') . '/' . $createdDay . '/' . $file->title) !!}" title="{!! $file->title !!}"><i class="fas fa-file-download"></i></a>
                            </div>
                        @endif --}}
                    @endforeach
                @endif
            </td>
        </tr>
    </table>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $("input[name='amount']").inputmask({'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'min': 0, 'digits': 0, 'removeMaskOnSubmit': true, 'allowMinus': true, 'max': 99999999});
            });
        }(window.jQuery);
    </script>
@stop