@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('banks.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('banks.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.banks.index') !!}">{!! trans('banks.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open([ 'url' => route('admin.banks.store-accounts'), 'role' => 'form']) !!}
    <br>
        <table class='table borderless' style="width: 55%; margin:auto">
            <tr>
                <th class="text-right" style="width: 20%;">
                    {!! trans('banks.account_number') !!}*
                </th>
                <td style="width: 30%;">
                    {!! Form::text('account_number', old('account_number'), array('class' => 'form-control', 'required', 'maxlength' => 20)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right" style="width: 20%;">
                    {!! trans('banks.label') !!}*
                </th>
                <td style="width: 30%;">
                    {!! Form::select('full_name', [' ' => trans('system.dropdown_all')] + $bank, old('full_name'), ['class' => 'form-control select2']) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.branch') !!}
                </th>
                <td>
                    {!! Form::text('branch', old('branch'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.province_city') !!}
                </th>
                <td>
                    {!! Form::text('province_city', old('province_city'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.address') !!}
                </th>
                <td>
                    {!! Form::text('address', old('address'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.account_holder') !!}
                </th>
                <td>
                    {!! Form::text('account_holder', old('account_holder'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.description') !!}
                </th>
                <td>
                    {!! Form::text('desc', old('desc'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-center">
                   
                </th>
                <td class="text-center">
                    {!! Form::checkbox('status', 1, old('status', 1), ['class' => 'minimal']) !!}
                    {!! trans('system.status.label') !!}
                </td>
            </tr>
            <tr>
                <th>

                </th>
                <td class="text-center">
                    {!! HTML::link(route( 'admin.banks.index' ), trans('system.action.cancel'), array('class' => 'btn btn-danger btn-flat'))!!}
                    {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                    <span class="label label-danger message"></span>
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $(".select2").select2({ width: '100%' });
        });
    }(window.jQuery);
</script>
@stop