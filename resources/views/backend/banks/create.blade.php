@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('banks.label') !!}
@stop
@section('head')
    {!! HTML::style(asset('assets/backend/plugins/jasny/css/jasny-bootstrap.min.css')) !!}
    {!! HTML::style('assets/backend/plugins/iCheck/all.css') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('banks.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.banks.index') !!}">{!! trans('banks.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open([ 'url' => route('admin.banks.store'), 'role' => 'form', 'files' => true ]) !!}
    <br>
        <table class='table borderless' style="width: 55%; margin:auto">
            <tr>
                <th class="text-right" style="width: 20%;">
                    {!! trans('banks.abbreviations') !!}*
                </th>
                <td style="width: 30%;">
                    {!! Form::text('abbreviation', old('abbreviation'), array('class' => 'form-control', 'required', 'maxlength' => 10)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right" style="width: 20%;">
                    {!! trans('banks.full_name') !!}*
                </th>
                <td style="width: 30%;">
                    {!! Form::text('full_name', old('full_name'), array('class' => 'form-control', 'required', 'maxlength' => 255)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.address') !!}
                </th>
                <td>
                    {!! Form::text('address', old('address'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    {!! trans('banks.desc') !!}
                </th>
                <td>
                    {!! Form::text('desc', old('desc'), array('class' => 'form-control', 'maxlength' => 250)) !!}
                </td>
            </tr>
            <tr>
                <th class="text-center">
                   
                </th>
                <td class="text-center">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                        {!! trans('system.status.label') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <th>

                </th>
                <td class="text-center">
                    {!! HTML::link(route( 'admin.banks.index' ), trans('system.action.cancel'), array('class' => 'btn btn-danger btn-flat'))!!}
                    {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                    <span class="label label-danger message"></span>
                </td>
            </tr>
        </table>
    {!! Form::close() !!}
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/jasny/js/bootstrap-fileupload.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.js') !!}"></script>
<script>
    !function ($) {
        $(function() {
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            });
            $('input[type="checkbox"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red'
            });
        });
    }(window.jQuery);
</script>
@stop