<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="text-center">
        Thêm biến động kho {!! $product['sku'] !!} - {!! $product['name'] !!} tại <span class="label label-success">{!! is_null($category) ? 'Kho khởi tạo' : $category->name !!}</span>
        <br/><br/>Tồn kho hiện tại <span class="label label-danger">{!! $inventory->quantity !!}/{!! $product['quantity'] !!}</span>
   </h4>
</div>
<div class="modal-body ">
    {!! Form::hidden('inventory_id', $inventory->id, []) !!}
    <table class="table">
        <thead>
            <tr>
                <th style="text-align: center; vertical-align: middle; width: 30%;">{!! trans('inventories.type') !!}</th>
                <th style="text-align: center; vertical-align: middle; width: 20%;">{!! trans('inventories.quantity') !!}</th>
                <th style="text-align: center; vertical-align: middle;">{!! trans('inventories.note') !!}</th>
           </tr>
       </thead>
        <tbody id="inventory">
       </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                    <a href="javascript:void(0)" id="addrow"><span class="fa fa-plus"></span> Thêm biến động</a>
               </td>
           </tr>
       </tfoot>
   </table>
    <div style="max-height: 300px; overflow-y: scroll; border: 1px dashed #c2c2c2;">
        <ul class="timeline">
            @foreach ($timelines as $timeline)
                <li>
                    <i class="fa fa-bolt bg-blue"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> {!! date("d/m/Y H:i:s", strtotime($timeline->action_at)) !!}</span>
                        @if ($timeline->field == 'status')
                            <h3 class="timeline-header">
                                Thay đổi {!! trans('system.status.label') !!}
                        </h3>
                            <div class="timeline-body">
                                    {!! trans('system.status.' . $timeline->data_new) !!} <br/>
                                    <span style="text-decoration: line-through;">{!! trans('data_numbers.status.' . $timeline->data_old) !!}</span>@if($timeline->note)<br/> Ghi chú: {!! $timeline->note !!} @endif
                        </div>
                            <div class="timeline-footer">
                                <span class="btn btn-primary btn-xs">{!! isset($users[$timeline->action_by]) ? $users[$timeline->action_by] : '-' !!}</span>
                        </div>
                        @elseif ($timeline->field == 'assign')
                            <h3 class="timeline-header">
                                Thay đổi {!! trans('data_numbers.' . $timeline->field) !!}
                        </h3>
                            <div class="timeline-body">
                                <?php
                                    $old = App\User::find($timeline->data_old);
                                    $new = App\User::find($timeline->data_new);
                                ?>
                                    Từ {!! is_null($old) ? '' : $old->fullname !!} sang {!! is_null($new) ? '' : $new->fullname !!}
                        </div>
                            <div class="timeline-footer">
                                <span class="btn btn-primary btn-xs">{!! isset($users[$timeline->action_by]) ? $users[$timeline->action_by] : '-' !!}</span>
                        </div>
                        @else
                            <h3 class="timeline-header">
                                Thay đổi {!! trans('inventories.' . $timeline->field) !!}
                        </h3>
                            <div class="timeline-body">
                                    Giá trị mới: {!! $timeline->data_new !!}<br/>
                                <span style="text-decoration: line-through;">
                                    Giá trị cũ: {!! $timeline->data_old !!}
                            </span>
                                <i>@if($timeline->note)<br/> Ghi chú: {!! $timeline->note !!} @endif</i>
                        </div>
                            <div class="timeline-footer">
                                <span class="btn btn-primary btn-xs">{!! isset($users[$timeline->action_by]) ? $users[$timeline->action_by] : '-' !!}</span>
                        </div>
                        @endif
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('system.action.cancel') !!}</button>
    <a href="javascript:void(0)" class="btn btn-info saveInventory">{!! trans('system.action.save') !!}</a>
</div>
