
<ul class="timeline">
    <li class="time-label">
        <span class="bg-red">
            {!! $product->sku !!}
        </span>
    </li>
    @foreach ($timelines as $timeline)
        <li>
            <i class="fa fa-bolt bg-blue"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {!! date("d/m/Y H:i:s", strtotime($timeline->action_at)) !!}</span>
                @if ($timeline->field == 'status')
                    <h3 class="timeline-header">
                        Thay đổi {!! trans('system.status.label') !!}
                    </h3>
                    <div class="timeline-body">
                            {!! trans('system.status.' . $timeline->data_new) !!} <br/>
                            <span style="text-decoration: line-through;">{!! trans('data_numbers.status.' . $timeline->data_old) !!}</span>@if($timeline->note)<br/> Ghi chú: {!! $timeline->note !!} @endif
                    </div>
                    <div class="timeline-footer">
                        <span class="btn btn-primary btn-xs">{!! isset($users[$timeline->action_by]) ? $users[$timeline->action_by] : '-' !!}</span>
                    </div>
                @elseif ($timeline->field == 'assign')
                    <h3 class="timeline-header">
                        Thay đổi {!! trans('data_numbers.' . $timeline->field) !!}
                    </h3>
                    <div class="timeline-body">
                        <?php
                            $old = App\User::find($timeline->data_old);
                            $new = App\User::find($timeline->data_new);
                        ?>
                            Từ {!! is_null($old) ? '' : $old->fullname !!} sang {!! is_null($new) ? '' : $new->fullname !!}
                    </div>
                    <div class="timeline-footer">
                        <span class="btn btn-primary btn-xs">{!! isset($users[$timeline->action_by]) ? $users[$timeline->action_by] : '-' !!}</span>
                    </div>
                @else
                    <h3 class="timeline-header">
                        Thay đổi {!! trans('inventories.' . $timeline->field) !!}
                    </h3>
                    <div class="timeline-body">
                            Giá trị mới: {!! $timeline->data_new !!}<br/>
                        <span style="text-decoration: line-through;">
                            Giá trị cũ: {!! $timeline->data_old !!}
                        </span>
                        <i>@if($timeline->note)<br/> Ghi chú: {!! $timeline->note !!} @endif </i>
                    </div>
                    <div class="timeline-footer">
                        <span class="btn btn-primary btn-xs">{!! isset($users[$timeline->action_by]) ? $users[$timeline->action_by] : '-' !!}</span>
                    </div>
                @endif
            </div>
        </li>
    @endforeach
</ul>
