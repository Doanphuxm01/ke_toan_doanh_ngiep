@extends('backend.master')
@section('title')
    {!! trans('system.action.list') !!} {!! trans('inventories.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <style>
        .select2-selection__clear::after {
            /*content: ' mặc định';*/
        }
        .select2-selection__clear {
            /*background: #a2a2a2;*/
            padding: 0 5px;
            border-radius: 6px;
            line-height: 150%;
            margin: 4px;
            color: red !important;
            /*color: #fff !important;*/
        }
        .select2-selection__clear:hover {
            background: red;
            color: white !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/treegrid/css/jquery.treegrid.css') !!}">
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('inventories.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.inventories.index') !!}">{!! trans('inventories.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.inventories.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('category', trans('inventories.category')) !!}
                            {!! Form::select('category', [-1 => trans('system.dropdown_all')] + $filterInventoryCategories, Request::input('category'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('province', trans('inventories.province')) !!}
                            {!! Form::select('province', [-1 => trans('system.dropdown_all')] + $provinces, Request::input('province'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('product', trans('products.label')) !!}
                            {!! Form::select('product', $product ?? [-1 => trans('system.dropdown_all')], Request::input('product'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('date_range', trans('system.update_range')) !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('date_range', Request::input('date_range'), ['class' => 'form-control pull-right date_range']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('type', trans('inventories.type')) !!}
                            {!! Form::select('type', ['' => trans('system.dropdown_all')] + $types, Request::input('type'), ['class' => 'form-control select2'])!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('page_num', trans('system.page_num')) !!}
                            {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2',  "style" => "width: 100%;"]) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                            <button type="submit" class="btn btn-default btn-flat">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                            </button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group">
                <a href="{!! route('admin.inventories.create') !!}" class='btn btn-primary btn-flat'>
                    <span class="ion-plus"></span>&nbsp;{!! trans('system.action.create') !!}
                </a>
            </div>
            <div class="btn-group">
                <a href="{!! route('admin.inventories.create-bulk') !!}" class='btn btn-info btn-flat'>
                    <span class="fa fa-file-excel-o"></span>&nbsp;{!! trans('system.action.import') !!}
                </a>
            </div>
        </div>
        <div class="col-md-12 text-right">
            {!! $inventories->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($inventories) > 0)
        <div class="box">
            <div class="box-header">
                <?php $i = (($inventories->currentPage() - 1) * $inventories->perPage()) + 1; ?>
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $inventories->count()) . ' ( ' . trans('system.total') . ' ' . $inventories->total() . ' )' !!}
                    | <i>Chú giải: </i>
                    <span class="text-warning"><i class="fas fa-random"></i> Nhập xuất kho </span>&nbsp;&nbsp;
                    <span class="text-info"><i class="fas fa-history"></i> {!! trans('system.history') !!}</span>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class='table table-striped table-bordered tree'>
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('inventories.category') !!}<br/>{!! trans('inventories.province') !!}</th>
                                <th style="vertical-align: middle;">{!! trans('products.label') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('inventories.quantity') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_by') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $labels = ['success', 'danger', 'info', 'warning', 'default'];
                                $bg = ['red', 'olive', 'orange', 'maroon', 'purple', 'navy'];
                                $data = [];
                                $tmp = $inventories->getCollection();
                                $inventories = $tmp->keyBy('id');
                            ?>
                            @foreach ($inventories as $id => $item)
                                <?php
                                    if (isset($data[$item->product_id])) {
                                        $product = $data[$item->product_id];
                                    } else {
                                        $product = $item->product()->first();
                                        if (is_null($product)) {
                                            continue;
                                        }
                                        if ($product->parent_id) {
                                            $parent = App\Product::find($product->parent_id);//where('status', 1)->
                                            if (is_null($parent)) {

                                                continue;
                                            }
                                            $product->name = $parent->name;
                                            $product->id = $parent->id;
                                        }
                                    }
                                ?>
                                <tr class="treegrid-{!! $item->product_id !!}">
                                    {{-- <td></td> --}}
                                    <td style="text-align: center; width: 3%; vertical-align: middle;"></td>
                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                        {{-- <span class="badge bg-{!! $bg[$item->inventory_category_id%6] !!}">{!! $inventoryCategories[$item->inventory_category_id] ?? '' !!}</span><br/>
                                        <span class="label label-{!! $labels[$item->province_id%5] !!}">{!! $provinces[$item->province_id] ?? '' !!}</span> --}}
                                    </td>
                                    <td style="vertical-align: middle; white-space: nowrap;">
                                        {{-- <span class="badge bg-teal">{!! $item->quantity !!}/{!! $product->quantity !!}</span> --}}
                                        <a target="_blank" href="{!! route('admin.products.show', $product->id) !!}">
                                            {!! $product->sku !!} - {!! $product->name !!}
                                            <span class="fas fa-external-link-alt"></span>
                                        </a>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {{-- {!! $item->quantity !!} --}}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                    </td>

                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                    </td>
                                </tr>
                                <tr class="treegrid-parent-{!! $item->product_id !!}">
                                    <td style="text-align: center; width: 3%; vertical-align: middle;">{!! $i++ !!}</td>
                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                        <span class="badge bg-{!! $bg[$item->inventory_category_id%6] !!}">{!! $inventoryCategories[$item->inventory_category_id] ?? '' !!}</span><br/>
                                        <span class="label label-{!! $labels[$item->province_id%5] !!}">{!! $provinces[$item->province_id] ?? '' !!}</span>
                                    </td>
                                    <td style="vertical-align: middle; white-space: nowrap;">
                                        <span class="badge bg-teal">{!! $item->quantity !!}/{!! $product->quantity !!}</span>
                                        {{-- <a target="_blank" href="{!! route('admin.products.show', $product->id) !!}">
                                            {!! $product->sku !!} - {!! $product->name !!}
                                            <span class="fas fa-external-link-alt"></span>
                                        </a> --}}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        {!! $item->quantity !!}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                        <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <?php $adm = App\User::find($item->created_by); ?>
                                        <a href="{!! is_null($adm) ? '#' : route('admin.users.show', $adm->id) !!}" target="_blank">
                                            {!! is_null($adm) ? "" : $adm->fullname !!}<i class="fas fa-external-link-alt"></i>
                                        </a>
                                    </td>

                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                        <a href="javascript:void(0)" class="btn btn-xs btn-default inventory" data-id="{!! $item->id !!}">
                                            <i class="text-warning fas fa-random"></i>
                                        </a>
                                        &nbsp;&nbsp;
                                        <a href="javascript:void(0)" class="btn btn-xs btn-default history" data-id="{!! $item->id !!}">
                                            <i class="text-info fas fa-history"></i>
                                        </a>

                                    </td>
                                </tr>
                                <?php
                                    unset($inventories[$id]);
                                    // dd(count($inventories));
                                    $children = $tmp->where('id', '<>', $item->id)->where('product_id', $item->product_id);
                                ?>
                                @if (count($children))
                                    @foreach ($children as $child)
                                        <tr class="treegrid-parent-{!! $item->product_id !!}">
                                            <td style="text-align: center; width: 3%; vertical-align: middle;">{!! $i++ !!}</td>
                                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                                <span class="badge bg-{!! $bg[$child->inventory_category_id%6] !!}">{!! $inventoryCategories[$child->inventory_category_id] ?? '' !!}</span><br/>
                                                <span class="label label-{!! $labels[$child->province_id%5] !!}">{!! $provinces[$child->province_id] ?? '' !!}</span>
                                            </td>
                                            <td style="vertical-align: middle; white-space: nowrap;">
                                                <span class="badge bg-teal">{!! $child->quantity !!}/{!! $product->quantity !!}</span>
                                                {{-- <a target="_blank" href="{!! route('admin.products.show', $product->id) !!}">
                                                    {!! $product->sku !!} - {!! $product->name !!}
                                                    <span class="fas fa-external-link-alt"></span>
                                                </a> --}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                {!! $child->quantity !!}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($child->created_at)) !!}</span><br/>
                                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($child->updated_at)) !!}</span>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                <?php $adm = App\User::find($child->created_by); ?>
                                                <a href="{!! is_null($adm) ? '#' : route('admin.users.show', $adm->id) !!}" target="_blank">
                                                    {!! is_null($adm) ? "" : $adm->fullname !!}<i class="fas fa-external-link-alt"></i>
                                                </a>
                                            </td>

                                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-default inventory" data-id="{!! $child->id !!}">
                                                    <i class="text-warning fas fa-random"></i>
                                                </a>
                                                &nbsp;&nbsp;
                                                <a href="javascript:void(0)" class="btn btn-xs btn-default history" data-id="{!! $child->id !!}">
                                                    <i class="text-info fas fa-history"></i>
                                                </a>

                                            </td>
                                        </tr>
                                        <?php unset($inventories[$child->id]); ?>
                                    @endforeach
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <div class="modal fade" id="importExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop1="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <div class="modal fade" id="timeline" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="text-center">Lịch sử thao tác</h5>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-info" data-dismiss="modal"> {!! trans('system.action.ok') !!} </a>
                </div>
            </div>
        </div>
    </div>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/treegrid/js/jquery.treegrid.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function(){
                $('.tree').treegrid({
                    // "initialState": "collapsed"
                });

                function formatState (state) {
                    if (!state.id) { return state.text; }
                    var $state = $(
                        '<span><img height="30px" src="' + state.image + '" class="img-flag" /> ' + state.text + '</span>'
                    );
                    return $state;
                };
                $(".select2").select2({'width': '100%'});
                $('.date_range').daterangepicker({
                    autoUpdateInput: false,
                    "locale": {
                        "format": "DD/MM/YYYY HH:mm",
                        "separator": " - ",
                        "applyLabel": "Áp dụng",
                        "cancelLabel": "Huỷ bỏ",
                        "fromLabel": "Từ ngày",
                        "toLabel": "Tới ngày",
                        "customRangeLabel": "Tuỳ chọn",
                        "weekLabel": "W",
                        "daysOfWeek": [
                            "CN",
                            "T2",
                            "T3",
                            "T4",
                            "T5",
                            "T6",
                            "T7"
                        ],
                        "monthNames": [
                            "Thg 1",
                            "Thg 2",
                            "Thg 3",
                            "Thg 4",
                            "Thg 5",
                            "Thg 6",
                            "Thg 7",
                            "Thg 8",
                            "Thg 9",
                            "Thg 10",
                            "Thg 11",
                            "Thg 12"
                        ],
                        "firstDay": 1
                    },
                    ranges: {
                       'Hôm nay': [moment(), moment()],
                       'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                       '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                       'Tháng này': [moment().startOf('month'), moment()],
                       'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    },
                    "alwaysShowCalendars": true,
                    maxDate: moment(),
                    minDate: moment().subtract(1, "years"),
                }, function(start, end, label) {
                    $('.date_range').val(start.format('DD/MM/YYYY HH:mm') + " - " + end.format('DD/MM/YYYY HH:mm'));
                });
                $(document).on('click', '.history', function(event) {
                    NProgress.start();
                    $.getJSON("{!! route('admin.inventories.timeline') !!}?id=" + $(this).attr('data-id')).done(function (data) {
                        $("#timeline .modal-body").html(data.data);
                        $("#timeline").modal('show');
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                });

                $(document).on('click', '.inventory', function(event) {
                    NProgress.start();
                    $.getJSON("{!! route('admin.inventories.info') !!}?id=" + $(this).attr('data-id')).done(function (data) {
                        $("#importExport .modal-content").html(data.data);
                        $("#importExport ").modal('show');
                        $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '1', 'max': '9999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                });

                $("select[name='product']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    allowClear: true,
                    templateResult: formatState,
                    minimumResultsForSearch: 20,
                    ajax: {
                        url: "{{ route('admin.products.search') }}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    if (typeof(item.children) == 'undefined') {
                                        return {
                                            text: item.sku + ' - ' + item.name,
                                            id: item.id,
                                            image: item.image,
                                        }
                                    } else {
                                        return {
                                            text: item.text,
                                            children: $.map(item.children, function(child){
                                                return {
                                                    id: child.id,
                                                    text: child.sku + ' - ' + child.name,
                                                    image: child.image,
                                                }
                                            })
                                        }
                                    }

                                })
                            };
                        }
                    }
                }).on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    $(this).closest('form').submit();
                });

                $(document).on('click','#addrow',function(){
                    var newRow = $("<tr>");
                    var cols = "";
                    cols += '<td>'+ '{!! Form::select('type', ['' => trans('system.dropdown_choice')] + $types, old('type'), ['class' => 'form-control select2']) !!}'  +'</td>';
                    cols += '<td>' + '{!! Form::text('quantity', old('quantity'), ['class' => 'form-control decimal']) !!}' + '</td>';
                    cols += '<td>' + '{!! Form::text('note', old('note'), ['class' => 'form-control']) !!}' + '</td>';
                    cols += '<td><input type="button" class="del-row btn btn-md btn-danger" value="x"></td>';

                    newRow.append(cols);
                    $("#inventory").append(newRow);

                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 2, 'min': '1', 'max': '9999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                    $(".select2").select2({'width': '100%'});
                });

                $(document).on("click", ".del-row", function (event) {
                    $(this).closest("tr").remove();
                });

                $(document).on("click", ".saveInventory", function (event) {
                    var id = parseInt($("input[name='inventory_id']").val()) || 0;
                    if (!id) {
                        toastr.error("Đã có lỗi xảy ra");
                        return false;
                    }
                    var table = $("#inventory");
                    var items = {},
                        types = [],
                        quantities = [],
                        notes = [];
                    var have_error = false;
                    table.find('tr').each(function (i) {
                        var $tds = $(this).find('td');
                        var type = $.trim($tds.eq(0).find("select").val()),
                            quantity = $.trim($tds.eq(1).find("input").val()).replace(/,/g,""),
                            note = $.trim($tds.eq(2).find("input").val());
                        if (type != "{!! \App\Define\Inventory::TYPE_IN_STOCK !!}" && type != "{!! \App\Define\Inventory::TYPE_OUT_STOCK !!}") {
                            toastr.error("Phân loại là nhập hoặc xuất kho");
                            have_error = true;
                            return false;
                        }
                        if (quantity < 1 | quantity > 9999) {
                            toastr.error("Số lượng 1-9,999");
                            have_error = true;
                            return false;
                        }
                        types.push(type);
                        quantities.push(quantity);
                        notes.push(note);
                    });
                    items = {'type': types, 'quantity': quantities, 'note': notes };
                    if (have_error) {
                        return false;
                    }

                    if (types.length < 1) {
                        toastr.error("Cần tối thiểu 1 dòng biến động.");
                        return false;
                    }

                    NProgress.start();
                    $.ajax({
                        url: "{!! route('admin.inventories.save-inventory') !!}",
                        data: { id: id, items: items },
                        type: 'POST',
                        datatype: 'json',
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        success: function(res) {
                            $("#importExport").modal('hide');
                            toastr.success(res.message);
                            location.reload();
                        },
                        error: function(obj, status, err) {
                            var error = $.parseJSON(obj.responseText);
                            toastr.error(error.message, '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                        }
                    }).always(function() {
                        NProgress.done();
                    });
                });
            });
        }(window.jQuery);
    </script>
@stop
