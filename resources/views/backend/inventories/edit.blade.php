@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('inventories.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('inventories.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.inventories.index') !!}">{!! trans('inventories.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.inventories.update', $inventory->id), 'method' => 'PUT']) !!}
    <div class="box box-default">
        <div class="box-header">
            <div class="box-title">{!! $product->code . ' - ' . $product->name !!} {!! $categories[$inventory->inventory_category_id] ?? '-' !!}</div>
            <small class="form-group">
                <i>Chú giải: </i>&nbsp;&nbsp;
                <span class="text-success"><i class="fas fa-random"></i> Biến động tồn kho </span>&nbsp;&nbsp;
                <span class="text-danger"><i class="glyphicon glyphicon-remove"></i>{!! trans('system.action.delete') !!}</span>
            </small>
        </div>
        <div class="box-body">
            <?php $lots = $inventory->lots()->get(); ?>
            <table class="table">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('inventory_lots.code') !!}</th>
                        <th style="text-align: right; vertical-align: middle;">{!! trans('inventories.quantity_roll') !!}</th>
                        <th style="text-align: right; vertical-align: middle;">{!! trans('inventories.quantity_meter') !!}</th>
                        <th style="vertical-align: middle;">{!! trans('inventory_lots.note') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody id="inventory">
                    @foreach ($lots as $lot)
                        <tr>
                            <td style="vertical-align: middle;">
                                {!! Form::text('code[]', old('code[]', $lot->code), ['class' => 'form-control', 'maxlength' => 255, 'required']) !!}
                                {!! Form::hidden('id[]', old('id[]', $lot->id), []) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::text('roll[]', old('roll[]', $lot->roll), ['class' => 'form-control number', 'required']) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::text('md[]', old('md[]', $lot->md), ['class' => 'form-control decimal', 'required']) !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! Form::text('note[]', old('note[]'), ['class' => 'form-control', 'maxlength' => 255]) !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <a href="javascript:void(0)" class="btn btn-xs btn-default history" data-id="{!! $inventory->id !!}" data-lot-id="{!! $lot->id !!}">
                                    <i class="text-success fas fa-random"></i>
                                </a>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0)" link="{!! route('admin.inventories.delete-inventory', $lot->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    <?php $code = old('code'); $inDB = $lots->count(); ?>
                    @if (count($code) > $inDB)
                        <?php $md = old('md'); $roll = old('roll'); $note = old('note'); ?>
                        @for ($i = $inDB; $i < count($code); $i++)
                            <tr>
                                <td style="vertical-align: middle;">
                                    {!! Form::text('code[]', old('code[]', $code[$i]), ['class' => 'form-control', 'maxlength' => 255, 'required']) !!}
                                </td>
                                <td style="vertical-align: middle;">
                                    {!! Form::text('roll[]', old('roll[]', $roll[$i]), ['class' => 'form-control number', 'required']) !!}
                                </td>
                                <td style="vertical-align: middle;">
                                    {!! Form::text('md[]', old('md[]', $md[$i]), ['class' => 'form-control decimal', 'required']) !!}
                                </td>
                                <td style="vertical-align: middle;">
                                    {!! Form::text('note[]', old('note[]', $note[$i]), ['class' => 'form-control', 'maxlength' => 255]) !!}
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <a href="javascript:void(0)" class="del-row btn btn-default btn-xs"><i class="text-danger glyphicon glyphicon-remove"></i></a>
                                </td>
                            </tr>
                        @endfor
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <a href="javascript:void(0)" id="addrow"><span class="fa fa-plus"></span> Thêm lô</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="4">
                            <label>
                                {!! Form::checkbox('status', 1, old('status', $inventory->status), [ 'class' => 'minimal' ]) !!}
                                {!! trans('system.status.active') !!}
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-center">
                            <a class="btn btn-default btn-flat" href="{!! route('admin.inventories.index') !!}">{!! trans('system.action.cancel') !!}</a>
                            {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('footer')
    <div class="modal fade" id="timeline" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="text-center">Biến động tồn kho</h5>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-info" data-dismiss="modal"> {!! trans('system.action.ok') !!} </a>
                </div>
            </div>
        </div>
    </div>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $(".decimal").inputmask({'alias': 'decimal', 'digits': 2, 'min': '0', 'max': '10000', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                $(".number").inputmask({'alias': 'integer', 'min': '0', 'max': '10000', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});

                $(document).on('click','#addrow',function(){
                    var newRow = $("<tr>");
                    var cols = "";
                    cols += '<td><input type="text" maxlength="255" class="form-control" required name="code[]"/></td>';
                    cols += '<td><input type="text" class="form-control number" required name="roll[]"/></td>';
                    cols += '<td><input type="text" class="form-control decimal" required name="md[]"/></td>';
                    cols += '<td><input type="text" maxlength="255" class="form-control" name="note[]"/></td>';
                    cols += '<td style="text-align: center; vertical-align: middle;"><a href="javascript:void(0)" class="del-row btn btn-default btn-xs"><i class="text-danger glyphicon glyphicon-remove"></i></a></td>';
                    newRow.append(cols);
                    $("#inventory").append(newRow);
                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 2, 'min': '0', 'max': '10000', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                    $(".number").inputmask({'alias': 'integer', 'min': '0', 'max': '10000', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                });

                $(document).on("click", ".del-row", function (event) {
                    $(this).closest("tr").remove();
                });

                $(document).on('click', '.history', function(event) {
                    NProgress.start();
                    $.getJSON("{!! route('admin.inventories.timeline') !!}?id=" + $(this).attr('data-id') + "&lot_id=" + $(this).attr('data-lot-id')).done(function (data) {
                        $("#timeline .modal-body").html(data.data);
                        $("#timeline").modal('show');
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                });
            });
        }(window.jQuery);
    </script>
@stop
