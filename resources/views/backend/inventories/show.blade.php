@extends('backend.master')

@section('title')
    {!! trans('system.action.detail') !!} - {!! trans('inventories.label') !!}
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('inventories.label') !!}
            <small>{!! trans('system.action.detail') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.inventories.index') !!}">{!! trans('inventories.label') !!}</a></li>
        </ol>
    </section>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">
                {!! trans('customers.label') !!} {!! $customer->fullname !!}
                <span class="{!! \App\Define\Customer::getPackagerColors($customer->package) !!}">{!! trans('customer_shared.types.' . $customer->package) !!}</span>
            </h3>
        </div>
        <div class="box-body">
            <?php $payment = $customer->payments()->orderBy('id', 'DESC')->first(); ?>
            @if (is_null($payment))
                <div class="row">
                    <div class="col-md-12">
                        <span class="text-danger">K/H chưa thực hiện thanh toán lần nào</span>
                    </div>
                </div>
            @else
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered">
                        <tbody>
                            @unless ($customer->package == App\Define\Customer::TYPE_PERSONAL)
                                <tr>
                                    <th style="text-align: right; vertical-align: middle;">{!! trans('inventories.from_date') !!}</th>
                                    <td>{!! date('d/m/Y', $customer->from_date) !!}</td>
                                    <th style="text-align: right; vertical-align: middle;">{!! trans('inventories.to_date') !!}</th>
                                    <td>{!! date('d/m/Y', $customer->to_date) !!}</td>
                                </tr>
                            @endunless
                            <tr>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.fee_per_mail') !!}</th>
                                <td>{!! App\Helper\HString::currencyFormat($customer->fee_per_mail) !!} đ</td>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.fee_per_validate') !!}</th>
                                <td>{!! App\Helper\HString::currencyFormat($customer->fee_per_validate) !!} đ</td>
                            </tr>
                            <tr>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customers.sent_today') !!}</th>
                                <td>{!! App\Helper\HString::currencyFormat($customer->sent_today) !!}</td>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customers.sent_month') !!}</th>
                                <td>{!! App\Helper\HString::currencyFormat($customer->sent_month) !!}</td>
                            </tr>
                            <tr>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customers.validated_today') !!}</th>
                                <td>{!! App\Helper\HString::currencyFormat($customer->validated_today) !!}</td>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customers.validated_month') !!}</th>
                                <td>{!! App\Helper\HString::currencyFormat($customer->validated_month) !!}</td>
                            </tr>
                            <tr>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.quota_send_monthly') !!}</th>
                                <td>{!! $customer->quota_send_monthly == \App\Define\CustomerPackage::MONTHLY_UNLIMITED ? trans('customer_shared.monthlys.' . \App\Define\CustomerPackage::MONTHLY_UNLIMITED) : App\Helper\HString::currencyFormat($customer->quota_send_monthly) !!}</td>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.quota_validate_monthly') !!}</th>
                                <td>{!! $customer->quota_validate_monthly == \App\Define\CustomerPackage::MONTHLY_UNLIMITED ? trans('customer_shared.monthlys.' . \App\Define\CustomerPackage::MONTHLY_UNLIMITED) : App\Helper\HString::currencyFormat($customer->quota_validate_monthly) !!}</td>
                            </tr>
                            <tr>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.quota_send_daily') !!}</th>
                                <td>{!! $customer->quota_send_daily == \App\Define\CustomerPackage::MONTHLY_UNLIMITED ? trans('customer_shared.monthlys.' . \App\Define\CustomerPackage::MONTHLY_UNLIMITED) : App\Helper\HString::currencyFormat($customer->quota_send_daily) !!}</td>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.quota_validate_daily') !!}</th>
                                <td>{!! $customer->quota_validate_daily == \App\Define\CustomerPackage::MONTHLY_UNLIMITED ? trans('customer_shared.monthlys.' . \App\Define\CustomerPackage::MONTHLY_UNLIMITED) : App\Helper\HString::currencyFormat($customer->quota_validate_daily) !!}</td>
                            </tr>
                            <tr>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.quota_send_limit') !!}</th>
                                <td>{!! $customer->quota_send_limit == \App\Define\CustomerPackage::MONTHLY_UNLIMITED ? trans('customer_shared.monthlys.' . \App\Define\CustomerPackage::MONTHLY_UNLIMITED) : App\Helper\HString::currencyFormat($customer->quota_send_limit) !!}</td>
                                <th style="text-align: right; vertical-align: middle;">{!! trans('customer_shared.quota_validate_limit') !!}</th>
                                <td>{!! $customer->quota_validate_limit == \App\Define\CustomerPackage::MONTHLY_UNLIMITED ? trans('customer_shared.monthlys.' . \App\Define\CustomerPackage::MONTHLY_UNLIMITED) : App\Helper\HString::currencyFormat($customer->quota_validate_limit) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @endif
        </div><!-- /.box-body -->
    </div>
    <div class="box box-succes">
        <div class="box-body">
            <table class='table table-responsive'>
                <tr>
                    <th class="text-right" style="width: 15%;">
                        {!! trans('customer_shared.package') !!}
                    </th>
                    <td style="width: 35%;">
                        {!! \App\Define\Customer::getPremiumTypesForOption()[$inventory->package] !!}
                    </td>
                    <th class="text-right" style="width: 15%;">
                        {!! trans('inventories.from_date') !!}
                    </th>
                    <td>
                        {!! date("d/m/Y", strtotime($inventory->from_date)) !!}
                    </td>
                </tr>
                <tr>
                    <th class="text-right">
                        {!! trans('inventories.period') !!}
                    </th>
                    <td>
                        {!! \App\Define\Customer::getPeriodsForOption()[$inventory->period] !!}
                    </td>
                    <th class="text-right">
                        {!! trans('inventories.source') !!}
                    </th>
                    <td>
                        {!! \App\Define\CustomerPayment::getSourcesForOption()[$inventory->source] !!}
                    </td>
                </tr>
                <tr>
                    <th class="text-right">
                        {!! trans('inventories.price_ipo') !!}
                    </th>
                    <td>
                        {!! $inventory->price_ipo !!}
                    </td>
                    <th class="text-right">
                        {!! trans('inventories.price_discount') !!}
                    </th>
                    <td>
                        {!! $inventory->price_discount !!}
                    </td>
                </tr>
                <tr>
                    <th class="text-right">
                        {!! trans('inventories.note') !!}
                    </th>
                    <td colspan="3">
                        {!! $inventory->note !!}
                    </td>
                </tr>
            </table>
        </div>
    </div>
@stop
