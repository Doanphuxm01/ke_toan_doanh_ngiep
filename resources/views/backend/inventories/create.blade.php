@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('inventories.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('inventories.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.inventories.index') !!}">{!! trans('inventories.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.inventories.store')]) !!}
    <div class="box box-default">
        <div class="box-body">
            <table class="table">
                <tbody>
                    <tr>
                        <th class="table_right_middle" style="width: 10%;">
                            {!! trans('inventories.type') !!}
                        </th>
                        <td style="width: 15%;">
                            {!! Form::select('type', ['' => trans('system.dropdown_choice')] + $types, old('type'), ["class" => "form-control select2", "required"]) !!}
                        </td>
                        <th class="table_right_middle" style="width: 10%;">
                            {!! trans('inventories.category') !!}
                        </th>
                        <td style="width: 20%;">
                            {!! Form::select('category', ['' => trans('system.dropdown_choice')] + $categories, old('category'), ["class" => "form-control select2", "required"]) !!}
                        </td>
                        <th class="table_right_middle" style="width: 10%;">
                            {!! trans('products.sku') !!}
                        </th>
                        <td>
                            {!! Form::select('product', $product ?? ['' => trans('system.dropdown_choice')], old('product'), ["class" => "form-control select2", "required"]) !!}
                        </td>
                    </tr>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('inventories.quantity') !!}
                        </th>
                        <td>
                            {!! Form::text('quantity', old('quantity'), ["class" => "form-control decimal", "required"]) !!}
                        </td>
                        <th class="table_right_middle">
                            {!! trans('inventories.note') !!}
                        </th>
                        <td colspan="4">
                            {!! Form::text('note', old('note'), ["class" => "form-control"]) !!}
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" class="text-center">
                            <a class="btn btn-default btn-flat" href="{!! route('admin.inventories.index') !!}">{!! trans('system.action.cancel') !!}</a>
                            {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                function formatState (state) {
                    if (!state.id) { return state.text; }
                    var $state = $(
                        '<span><img height="30px" src="' + state.image + '" class="img-flag" /> ' + state.text + '</span>'
                    );
                    return $state;
                };

                $(".select2").select2({ width: '100%' });
                $("select[name='product']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    minimumResultsForSearch: 20,
                    ajax: {
                        url: "{!! route('admin.products.search') !!}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    if (typeof(item.children) == 'undefined') {
                                        return {
                                            text: item.sku + ' - ' + item.name,
                                            id: item.id,
                                            image: item.image,
                                        }
                                    } else {
                                        return {
                                            text: item.text,
                                            children: $.map(item.children, function(child){
                                                return {
                                                    id: child.id,
                                                    text: child.sku + ' - ' + child.name,
                                                    image: child.image,
                                                }
                                            })
                                        }
                                    }

                                })
                            };
                        }
                    }
                });
                $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '9999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
            });
        }(window.jQuery);
    </script>
@stop
