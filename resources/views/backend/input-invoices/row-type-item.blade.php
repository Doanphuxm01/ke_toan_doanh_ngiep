<div class="box">
    <div class="box-body">
        <div class="row section_type">
            <div class="col-md-3">
                <table class="table">
                    <tr>
                        <td style="width: 50%; white-space: nowrap; min-width: 100px;">
                            <span class="for_type">{!! trans('input_invoices.type') !!}</span>
                        </td>
                        <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                            <span class="for_mawb">{!! trans('input_invoices.mawb') !!}</span>
                        </td>
                        <td style="white-space: nowrap; min-width: 100px;">
                            <span class="for_hawb">{!! trans('input_invoices.hawb') !!}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; white-space: nowrap; min-width: 100px;">
                            {!! Form::select('type', \App\Define\Workflow::getAllReportsForOption(), old('type', \App\Define\Workflow::TYPE_AIR), ['class' => 'type form-control select2']) !!}
                        </td>
                        <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                            {!! Form::text('mawb', old('mawb'), ['class' => 'form-control']) !!}
                        </td>
                        <td style="white-space: nowrap; min-width: 100px;">
                            {!! Form::text('hawb', old('hawb'), ['class' => 'form-control']) !!}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-9">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                {!! trans('system.action.label') !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 120px;">
                                {!! trans('input_invoices.sale_cost_item') !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 180px;">
                                {!! trans('input_invoices.description') !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 120px;">
                                {!! trans('input_invoices.job_code') !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 90px;">
                                {!! trans('input_invoices.quantity') !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 90px;">
                                {!! trans('input_invoices.unit') !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 90px;">
                                {!! trans('input_invoices.amount') !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! trans('input_invoices.amount_convert') !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_percent') !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_account') !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_value') !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_value_convert') !!}
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                <p style="vertical-align: middle; text-align: left;">
                                    <a tabindex="-1" href="javascript:void(0);" class="text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                        <i class="far fa-eye"></i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="text-success clone-row"><i class="fas fa-copy"></i></a>
                                    &nbsp;&nbsp;
                                    <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.remove_current_row') !!}" class="text-danger remove-row"><i class="fas fa-minus"></i></a>
                                </p>
                            </td>
                            <td style="white-space: nowrap; min-width: 120px;">
                                {!! Form::select('sci[]', ['' => trans('system.dropdown_choice')] + $sci, old('sci[]'), ['class' => 'form-control select2 sci']) !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 180px;">
                                {!! Form::text('description[]', old('description[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 120px;">
                                {!! Form::text('job_code[]', old('job_code[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 90px;">
                                {!! Form::text('quantity[]', old('quantity[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 90px;">
                                {!! Form::text('unit[]', old('unit[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap; min-width: 90px;">
                                {!! Form::text('amount[]', old('amount[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! Form::text('amount_convert[]', old('amount_convert[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! Form::select('vat_percent[]', \App\Define\SaleCostItem::getTaxValuesForOption(), old('vat_percent[]'), ['class' => 'form-control select2']) !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! Form::text('vat_account[]', old('vat_account[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! Form::text('vat_value[]', old('vat_value[]'), ['class' => 'form-control']) !!}
                            </td>
                            <td style="white-space: nowrap;">
                                {!! Form::text('vat_value_convert[]', old('vat_value_convert[]'), ['class' => 'form-control']) !!}
                            </td>
                        </tr>
                        <tr>
                            <th style="white-space: nowrap; vertical-align: middle; text-align: center1;">
                                <a href="javascript:void(0);" class="add-row text-aqua" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                                    <i class="fas fa-plus"></i> {!! trans('system.action.create') !!}
                                </a>
                            </th>
                            <th style="white-space: nowrap; vertical-align: middle; text-align: center;" colspan="2">
                                {!! trans('input_invoices.total') !!}
                            </th>
                            <th style="white-space: nowrap; min-width: 120px;">
                            </th>
                            <th style="white-space: nowrap; min-width: 90px;">
                                {!! trans('input_invoices.quantity') !!}
                            </th>
                            <th style="white-space: nowrap; min-width: 90px;">
                                {!! trans('input_invoices.unit') !!}
                            </th>
                            <th style="white-space: nowrap; min-width: 90px;">
                                {!! trans('input_invoices.amount') !!}
                            </th>
                            <th style="white-space: nowrap;">
                                {!! trans('input_invoices.amount_convert') !!}
                            </th>
                            <th style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_percent') !!}
                            </th>
                            <th style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_account') !!}
                            </th>
                            <th style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_value') !!}
                            </th>
                            <th style="white-space: nowrap;">
                                {!! trans('input_invoices.vat_value_convert') !!}
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>