<tr class="apeendrow">
    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
        <p style="vertical-align: middle; text-align: left;">
            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">
                <i class="far fa-eye"></i>
            </a>
            &nbsp;&nbsp;
            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row text-success"><i class="fas fa-copy"></i></a>
        </p>
    </td>
    <td style="white-space: nowrap; min-width: 120px;">
        <div class="form-group">
            <select id="ls-data" name="obj[sci[]]" class="form-control select2 sci" style="width: 100%;">
                @if(@$saleCostItems)
                    @foreach($saleCostItems as $key => $ls)
                        <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>
                    @endforeach
                    @endif
            </select>
        </div>
    </td>
    <td style="white-space: nowrap; min-width: 180px;" class="child-description" id="html">
        <input type="text" name="obj[description[]]" class="form-control description">
    </td>
    <td style="white-space: nowrap; min-width: 120px;">
        {!! Form::text('job_code[]', old('job_code[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">
        <input type="text" name="obj[tk_co[]]" class="form-control tk-co">
    </td>
    <td style="white-space: nowrap; min-width: 90px;">
        {!! Form::text('unit[]', old('unit[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap; min-width: 90px;">
        {!! Form::text('amount[]', old('amount[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::text('amount_convert[]', old('amount_convert[]'), ['class' => 'form-control']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::select('vat_percent[]', \App\Define\SaleCostItem::getTaxValuesForOption(), old('vat_percent[]'), ['class' => 'form-control select2']) !!}
    </td>
    <td style="white-space: nowrap;">
        {!! Form::text('vat_account[]', old('vat_account[]'), ['class' => 'form-control']) !!}
    </td>
</tr>
{{--@section('JS_REGION_BOTTOM')--}}
{{--    <script>--}}
{{--        $(function () {--}}
{{--            $('#ls-data').on('change',function(){--}}
{{--                var html = '';--}}
{{--                var tkco = '';--}}
{{--                $('#ls-data :selected').each(function(i, obj) {--}}
{{--                    var description = $(this).data('description');--}}
{{--                    var sale = $(this).data('sale');--}}
{{--                    // var check = $('td.child-description').find('.description').remove();--}}
{{--                    // var checkrow = $('td.child-tk-co').find('.tk-co').remove();--}}
{{--                    html += '<input type="text" name="obj[description[]]" class="form-control description" value="'+description+'">';--}}
{{--                    tkco += '<input type="text" name="obj[tk_co[]]" class="form-control tk-co" value="'+sale+'">';--}}
{{--                });--}}
{{--                $('#html').html(html);--}}
{{--                $('#tk-co').html(tkco);--}}
{{--            });--}}
{{--        })--}}
{{--    </script>--}}

{{--@endsection--}}
