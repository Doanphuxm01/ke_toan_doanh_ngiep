@extends('backend.master')
@section('title')
{!! trans('system.action.list') !!} {!! trans('input_invoices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('input_invoices.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.accounting.invoices') !!}">{!! trans('input_invoices.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-bconsumer">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.accounting.invoices'), 'method' => 'GET', 'role' => 'search' ]) !!}
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('code', trans('input_invoices.code')) !!}
                        {!! Form::text('code', Request::input('code'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('status', trans('system.status.label')) !!}
                        {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2'])!!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'display: block;']) !!}
                        <button type="submit" class="btn btn-default btn-flat" style="display: block;">
                            <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row" style="display: flex;">
        <div class="col-sm-1" style="margin-right: 3px;">
            <a href="{{  admin_link('admin/input-invoices/create') }}" class='btn btn-primary btn-flat'>
                <span class="glyphicon glyphicon-plus"></span>&nbsp; {!! trans('system.action.create') !!}
            </a>
        </div>
        <div class="col-sm-12 pull-right">
            <span class="pull-right">
                {!! $saleCostItems->appends( Request::except('page') )->render() !!}
            </span>
            <span class="pull-right">
                {!! Form::select('page_num', [ 10 => trans('system.show') . ' 10' . trans('system.items'), 20 => trans('system.show') . ' 20' . trans('system.items'), 50 => trans('system.show') . ' 50' . trans('system.items') , 100 => trans('system.show') . ' 100' . trans('system.items'), 500 => trans('system.show') . ' 500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2']) !!}
            </span>
        </div>
    </div>
    @if (count($saleCostItems) > 0)
    <?php $labels = ['default', 'success', 'info', 'danger', 'warning']; ?>
    <div class="box">
        <div class="box-header">
            <?php $i = (($saleCostItems->currentPage() - 1) * $saleCostItems->perPage()) + 1; ?>
            <div class="form-inline">
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $saleCostItems->count()) . ' ( ' . trans('system.total') . ' ' . $saleCostItems->total() . ' )' !!}
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('input_invoices.code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('input_invoices.description') !!}</th>
                        <th style="text-align: center; vertical-align: middle; white-space: nowrap;">{!! trans('input_invoices.branch_code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('input_invoices.sale_account_code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('input_invoices.cost_account_code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('input_invoices.prov_account_code') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!}</th>
                        <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($saleCostItems as $item)
                        <tr>
                            <td style="text-align: center; vertical-align: middle;">{!! $i++ !!}</td>
                            <td style="vertical-align: middle;">
                                <a href="{{  admin_link('admin/input-invoices/show?id='.$id.'') }}">{!! $item->code !!}</a><br/>
                                {{--  <a href="{!! route('admin.input-invoices.show', $item->id) !!}">{!! $item->code !!}</a><br/>  --}}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->description !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->branch_code !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->sale_account_code !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->cost_account_code !!}
                            </td>
                            <td style="vertical-align: middle;">
                                {!! $item->prov_account_code !!}
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                @if($item->status == 0)
                                    <span class="label label-default">{!! trans('system.status.deactive') !!}</span>
                                @elseif($item->status == 1)
                                    <span class="label label-success">{!! trans('system.status.active') !!}</span>
                                @endif
                            </td>
                            <td style="text-align: center; vertical-align: middle;">
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                            </td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                <a href="{{  admin_link('admin/input-invoices/edit?id='.$id.'')}}" class="btn btn-xs btn-default" title="{!! trans('system.action.edit') !!}"><i class="text-warning glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0)" link="{{ admin_link('admin/input-invoices/destroy?id='.@$id.'') }}" class="btn-confirm-del btn btn-default btn-xs" title="{!! trans('system.action.delete') !!}">
                                    <i class="text-danger glyphicon glyphicon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $("select[name='page_num']").change(function(event) {
                    onReload('page_num', $(this).val());
                });
            });
        }(window.jQuery);
        function onReload ($key, $value) {
            var queryParameters = {}, queryString = location.search.substring(1),
                re = /([^&=]+)=([^&]*)/g, m;
            while (m = re.exec(queryString)) {
                queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            queryParameters[$key]   = $value;
            queryParameters['page'] = 1;
            location.search = $.param(queryParameters);
        }
    </script>
@stop
