@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('input_invoices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('input_invoices.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.accounting.invoices') !!}">{!! trans('input_invoices.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.input-invoices.update', $saleCostItem->id), 'method' => 'PUT', 'role' => 'form']) !!}
        <table class='table borderless'>
            <tr>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('input_invoices.code') !!}
                </th>
                <td style="width: 35%;">
                    {!! Form::text('code', old('code', $saleCostItem->code), ['class' => 'form-control', 'maxlength' => 20, 'required']) !!}
                </td>
                <th class="table_right_middle" style="width: 15%;">
                    {!! trans('input_invoices.branch_code') !!}
                </th>
                <td>
                    {!! Form::select('branch_code', $branchCodes, old('branch_code', $saleCostItem->branch_code), ['class' => 'form-control select2', 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('input_invoices.description') !!}
                </th>
                <td colspan="3">
                    {!! Form::text('description', old('description', $saleCostItem->description), ['class' => 'form-control', 'maxlength' => 255, 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('input_invoices.charge_type') !!}
                </th>
                <td>
                    {!! Form::select('charge_type', ['charge_type1' => 'charge type 1', 'charge_type2' => 'charge type 2', 'charge_type3' => 'charge type 3'], old('charge_type', $saleCostItem->charge_type), ['class' => 'form-control select2']) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('input_invoices.tax_value') !!}
                </th>
                <td>
                    {!! Form::select('tax_value', [0 => '0%', 10 => '10%'], old('tax_value', $saleCostItem->tax_value), ['class' => 'form-control select2', 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('input_invoices.sale_account_code') !!}
                </th>
                <td>
                    {!! Form::select('sale_account_code', $accounts, old('sale_account_code', $saleCostItem->sale_account_code), ['class' => 'form-control select2', 'required']) !!}
                </td>
                <th class="table_right_middle">
                    {!! trans('input_invoices.cost_account_code') !!}
                </th>
                <td>
                    {!! Form::select('cost_account_code', $accounts, old('cost_account_code', $saleCostItem->cost_account_code), ['class' => 'form-control select2', 'required']) !!}
                </td>
            </tr>
            <tr>
                <th class="table_right_middle">
                    {!! trans('input_invoices.prov_account_code') !!}
                </th>
                <td>
                    {!! Form::select('prov_account_code', $accounts, old('prov_account_code', $saleCostItem->prov_account_code), ['class' => 'form-control select2']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-center table_right_middle1" colspan="4">
                    <label>
                        {!! Form::checkbox('status', 1, old('status', $saleCostItem->status), [ 'class' => 'minimal' ]) !!}
                        {!! trans('system.status.active') !!}
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center">
                    {!! HTML::link(route( '' ), trans('system.action.cancel'), ['class' => 'btn btn-danger btn-flat']) !!}
                    {!! Form::submit(trans('system.action.save'), ['class' => 'btn btn-primary btn-flat']) !!}
                </td>
            </tr>
        </table>

    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $(".select2").select2({ width: '100%' });
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
            });
        }(window.jQuery);
    </script>
@stop