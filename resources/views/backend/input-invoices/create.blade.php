@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('input_invoices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}"/>
    <style>
        .select2 {
            display: block;
        }
        .vcenter {
            /*display: inline-block;*/
            vertical-align: middle;
            /*float: none;*/
        }
    </style>
@stop
@push('JS_REGION_BOTTOM')
    <script src="{!! asset('assets/backend/js/input-invoice.js') !!}"></script>
@endpush
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('input_invoices.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.accounting.invoices') !!}">{!! trans('input_invoices.label') !!}</a></li>
        </ol>
    </section>
    <form method="post" id="mainFormInput" name="mainFormInput">
        <section class="content overlay">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="alert-bctech">

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b> {!! trans('partners.label') !!}</b>
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::select('obj[partner_code]', $partners ?? [-1 => trans('system.dropdown_choice')], old('partner_code'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}

                                        </div>
                                        <div class="col-md-5">
                                            {!! Form::text('obj[vendor_name]', old('vendor_name'), ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('partners.tax_code') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('obj[tax_code]', old('tax_code[]'), ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('input_invoices.input_date') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                                                <input class="form-control datepicker" readonly="" type="text" name="obj[voucher_date]">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('partners.address') !!}</b>
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('obj[address]', old('customer_address'), ['class' => 'form-control', 'disabled']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('input_invoices.input_no') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            {!! Form::text('obj[voucher_no]', old('input_no'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('input_invoices.description') !!}</b>
                                        </div>
                                        <div class="col-md-7">
                                            {!! Form::text('obj[description]', old('description'), ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('input_invoices.currency_code') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            @php($objMoney = App\Models\InputInvoice::$objectMoney)
                                            <select name="obj[currency]" id="" class="form-control select2" style="width:100%;">
                                                <option value="0"> Chưa lựa chọn</option>
                                                @foreach($objMoney as $key => $item)
                                                    <option value="{{ @$item['key']  }}">{{ @$item['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <b>{!! trans('input_invoices.invoice_no') !!}</b>
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::text('obj[invoice_no]', old('invoice_no'), ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-1">
                                            <b> {!! trans('input_invoices.invoice_date') !!}</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                                                <input class="form-control datepicker" readonly="" type="text" name="obj[invoice_date]">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <b>{!! trans('input_invoices.due_payment') !!}</b>
                                        </div>
                                        <div class="col-md-2">
                                            <div id="datepicker_due_payment" class="input-group date" data-date-format="dd-mm-yyyy">
                                            <input type="text" name="obj[due_payment]" {{ old('due_payment') }} class="form-control datepicker" >
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right vcenter">
                                            <b>{!! trans('input_invoices.currency_rate') !!}</b>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="currency_rate" name="obj[currency_rate]"  class="form-control currency_rate currency input-type-number-format">
{{--                                            {!! Form::text('obj[currency_rate][]', old('currency_rate'), ['class' => 'form-control']) !!}--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="service-content">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#hoach_toan" data-toggle="tab">Hoạch Toán</a></li>
                            <li><a href="#thue" data-toggle="tab">Thuế</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="hoach_toan">
                               @include('backend.input-invoices.include.content-hoach-toan')
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="thue">
                               @include('backend.input-invoices.include.content-thue')
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            </div>
        </section>
        <div class="row">
            <div class="bottom-control">
                <div class="container-fluid justify-content-end control-item" style="float: right">
                    <div class="float-right">
                        <a href="{{ url()->previous() }}" class="btn btn-warning">Bỏ qua</a>
                        <a href="javascript:void(0)" onclick="_submitForm()" class="btn btn-primary waves-effect waves-light mr-3 legitRipple"><i class="fe-save"></i> Cập nhật</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop
@push('JS_REGION_BOTTOM_MAIN')
    <script>
        function _submitForm() {
            return _POST_FORM('#mainFormInput', '{!! admin_link('admin/input-invoices/store') !!}')
        }
    </script>
@endpush
@section('footer')
    <style>
        .vendor-dropdown {
            width: 800px !important;
        }

        .custom-dropdown {
            width: 500px !important;
            left: -300px !important;
        }
    </style>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/fix-tabindex.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/call-plugins.js') !!}"></script>
{{--    <script src="{!! asset('assets/backend/js/service-purchase.js') !!}"></script>--}}
    <script src="{!! asset('assets/backend/js/input-invoice.js') !!}"></script>
    <script src="{!! asset('assets/backend/js/select2-table-partner.js') !!}"></script>
    <script>
        var typeAir   = @json(\App\Define\Workflow::TYPE_AIR);
        var typeSea   = @json(\App\Define\Workflow::TYPE_SEA);
        var typeDom   = @json(\App\Define\Workflow::TYPE_DOM);
        var typeOther = @json(\App\Define\Workflow::TYPE_OTHER);
        var mawb             = @json(trans("input_invoices.mawb"));
        var hawb             = @json(trans("input_invoices.hawb"));
        var hbl              = @json(trans("input_invoices.hbl"));
        var obl              = @json(trans("input_invoices.obl"));
        var invoice          = @json(trans("input_invoices.invoice"));
        var trans            = @json(trans("service_purchases.buy_service_of"));
        var statistical_code = @json(trans("input_invoices.statistical_code"));
        callICheck();
        callInputMaskDecimal();
    </script>

    <script>

        $(document).on('change', 'select[name="sale_code_item[]"]', function () {
            var id =  $(this).val()
            var tr = $(this).closest('tr');
            var index = tr.index();
            var tr_tax = $('#table_tax').find('tr:eq('+index+')');
            var td_sale_code_item_tax = tr_tax.find('select[name="sale_code_item_tax[]"]');
            var td_description_tax = tr_tax.find('input[name="description_tax[]"]');
            var td_explain_tax = tr_tax.find('input[name="explain_tax[]"]');
            td_description = tr.find('input[name="description[]"]');
            td_debit_account = tr.find('input[name="debit_account[]"]');
            $.ajax({
                type: "get",
                url: "{!! route('admin.service-purchases.get-info-sale-cost-item') !!}",
                data: {id:id},
                dataType: "json",
                success: function (response) {
                    td_description.val(response.data.description).prop('readonly', true);
                    td_debit_account.val(response.data.debit_account).prop('readonly', true);
                    td_sale_code_item_tax.val(response.data.id).change();
                    td_description_tax.val(response.data.description).prop('readonly', true);
                    td_explain_tax.val("VAT " + response.data.description)
                },
                error:function (response) {
                    toastr.error(response.message);
                }
            });
        })
    </script>

    <script>
        // ToltalExChange();
        !function ($) {
            $(function() {
                enterTab();
                select2Tab();
                renderTableS2('select[name="obj[partner_code]"]', "{!! route('admin.partners.get-vendor') !!}", 'vendor-dropdown', s2FormatResultPartner, getInfoAllInput);
                {{--renderTableS2('select[name="object_code_tax[]"]', "{!! route('admin.partners.get-all-partner') !!}", 'custom-dropdown', s2FormatResultPartner, getInfoTax);--}}
                renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);
            });
        }(window.jQuery);

    </script>
    <script>


        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            autoclose: true,
        });

    </script>
@stop