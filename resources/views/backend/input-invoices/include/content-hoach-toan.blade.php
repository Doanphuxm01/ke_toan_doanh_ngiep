@section('JS_REGION_BOTTOM')
    <script src="{!! asset('assets/backend/js/service.js') !!}"></script>
@endsection
<form method="post" id="mainFormInput" name="mainFormInput">
    <div class="row section_type" id="okchua">
        <div class="remove-love">
            <div class="col-md-3">
                <table class="table table-responsive" v-for="(the_loai, _i_) in danh_sach_hoach_toan">
                    <tr>
                        <td style="width: 50%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                            <span class="for_type">{!! trans('input_invoices.type') !!}</span>
                        </td>
                        <td style="width: 25%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                            <span class="for_mawb">{!! trans('input_invoices.mawb') !!}</span>
                        </td>
                        <td style="white-space: nowrap; min-width: 100px;background: #3C8DBC;color: white;">
                            <span class="for_hawb">{!! trans('input_invoices.hawb') !!}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; white-space: nowrap; min-width: 100px;">
                            {!! Form::select('obj[hoach_toan][0][type]', \App\Define\Workflow::getAllReportsForOption(), old('type', \App\Define\Workflow::TYPE_AIR), ['class' => 'type lua-chon-type form-control select2']) !!}
                        </td>
                        <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                            {!! Form::text('obj[hoach_toan][0][mawb]', old('mawb'), ['class' => 'form-control']) !!}
                        </td>
                        <td style="white-space: nowrap; min-width: 100px;">
                            {!! Form::text('obj[hoach_toan][0][hawb]', old('hawb'), ['class' => 'form-control']) !!}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-9">
                <div class="box">
                    <div class="">
                        <div class="row section_type">
                            <div class="col-md-12">
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <thead style="background: #3C8DBC;color: white;">
                                        <th>{!! trans('system.action.label') !!}</th>
                                        <th>{!! trans('input_invoices.sale_cost_item') !!}</th>
                                        <th>{!! trans('input_invoices.description') !!}</th>
                                        <th>{!! trans('input_invoices.tk_no') !!}</th>
                                        <th>{!! trans('input_invoices.tk_co') !!}</th>
                                        <th>{!! trans('input_invoices.don_gia') !!}</th>
                                        <th>{!! trans('input_invoices.so_luong') !!}</th>
                                        <th>{!! trans('input_invoices.thanh_tien') !!}</th>
                                        <th>{!! trans('input_invoices.quy_doi') !!}</th>
                                        <th>{!! trans('input_invoices.jobcode') !!}</th>
                                        <th>{!! trans('input_invoices.branch_code') !!}</th>
                                        </thead>
                                        <tbody id="tbody" class="tbodys table_hoan_tien">
                                        <tr class="apeendrow">
                                            <td  style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                <p style="vertical-align: middle; text-align: left;">
    {{--                                                                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">--}}
    {{--                                                                                                <i class="far fa-eye"></i>--}}
    {{--                                                                                            </a>--}}
                                                    &nbsp;&nbsp;
                                                    <a data-append="0" data-id="0" tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-hoach-toan text-success">
                                                        <i class="fas fa-copy"></i></a>

                                                    <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-hoach-toan text-success mf-1"><i class="fas fa-trash"></i></a>
                                                </p>
                                            </td>
                                            <td class="sale_account_code" style="white-space: nowrap; min-width: 120px;">
                                                <div class="form-group data-bind-ls" data-count="0">
                                                    <select id="ls-data" name="obj[hoach_toan][0][chi_tiet][0][sci]" class="form-control ls-data select2 sci" style="width: 100%;">
                                                        @foreach($saleCostItems as $key => $ls)
                                                            <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td  style="white-space: nowrap; min-width: 180px;" class="child-description description" id="html">
                                                <input type="text" id="description-0" name="obj[hoach_toan][0][chi_tiet][0][description]" class="form-control description">
                                            </td>
                                            <td style="white-space: nowrap; min-width: 120px;">
                                                <div class="form-group data-bind-ls" data-count="0">
                                                    <select  name="obj[hoach_toan][0][chi_tiet][0][debt_account]" class="form-control selectdect2020" style="width: 100%;">
                                                    </select>
                                                </div>
                                                {{--                                            <input  type="text" class="form-control" name="obj[job_code[]]">--}}
                                            </td>
                                            <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">
                                                <input id="account_has-0" type="text" name="obj[hoach_toan][0][chi_tiet][0][account_has]" class="form-control tk-co">
                                            </td>
                                            <div class="milt">
                                                <td style="white-space: nowrap; min-width: 90px;">
                                                    <input id="unit_price-0" type="text" step="any" name="obj[hoach_toan][0][chi_tiet][0][unit_price]" class="form-control currency unit_price input-type-number-format">
                                                </td>
                                                <td style="white-space: nowrap; min-width: 90px;">
                                                    <input id="amount-0" type="text" name="obj[hoach_toan][0][chi_tiet][0][amount]" class="form-control currency amount input-type-number-format">
                                                </td>
                                                <td style="white-space: nowrap; min-width: 90px;" class="money">
                                                    <input id="into_money-0" class="form-control currency int_money input-type-number-format"  type="text" name="obj[hoach_toan][0][chi_tiet][0][into_money]"  value="">
                                                </td>
                                            </div>
                                            <td style="white-space: nowrap;">
                                                <input id="exchange-0" type="text" name="obj[hoach_toan][0][chi_tiet][0][exchange]" class="form-control currency input-type-number-format exchange input-type-number-format">
                                            </td>
                                            <td style="white-space: nowrap;">
                                                <div class="form-group">
                                                    @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())
                                                    <select id="ls-data" name="obj[hoach_toan][0][chi_tiet][0][job_code]" class="form-control select2 sci" style="width: 100%;">
                                                        @foreach($vat_percent as $key => $ls)
                                                            <option  value="{{ @$key }}">{{ @$ls }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td style="white-space: nowrap;">
                                                <input type="text" name="obj[hoach_toan][0][chi_tiet][0][branch_code]" class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <button type="button" class="add-segment-hoach-toan btn btn-block btn-success btn-xs"><i class="fas fa-plus"></i>{!! trans('system.action.create') !!}</button>
    </div>
    {{--use jsavascript in blade--}}
    <style>
        .vendor-dropdown {
            width: 800px !important;
        }

        .custom-dropdown {
            width: 500px !important;
            left: -300px !important;
        }
    </style>
</form>
@push('JS_REGION_BOTTOM_MAIN')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script>
        var DANH_SACH_TYPE = {!! json_encode(\App\Define\Workflow::getAllReportsForOption()) !!}
        var io = new Vue({
            el: '#okchua',
            data: {
                danh_sach_hoach_toan: [
                    {
                        type: '',
                        MAWB: '',
                        HWAB: '',
                        chi_tiet: [
                            {
                                sale_item: '',
                                mo_ta: '',
                                tai_khoan_no: '',
                                tai_khoan_co: '',
                                so_luong: 0,
                                gia_niem_yet: 0,
                                gia_ban: 0,
                                exchange: 0,
                                jobcode: '',
                                branch_code: '',
                            }
                        ]
                    }
                ],
                DANH_SACH_TYPE: DANH_SACH_TYPE,
                dannh_sach_thue: {}
            }
        })
    </script>
    <script>
        !function ($) {
            $(function() {
                $(document).on('click', '.add-row', function(event) {
                    $(this).closest('tr').prev().after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.input-invoices.row-sale-cost-item", compact('saleCostItems'))->render()) !!}');
                    // $(".select2").select2().on("select2:close", function (e) {$(this).closest('td').next().find('input').focus();});
                });
                $('.add-segment-hoach-toan').click(function() {
                    if(!isDoubleClicked($(this))){
                        var length_div = $('.remove-love').length;
                        var _token = jQuery('meta[name=_token]').attr("content");
                        var  prev = $(this).prev();
                        var service_content = $(this).closest('div#service-content');
                        $.ajax({
                            url: 'addRow',
                            type: 'POST',
                            data: {
                                _token: _token,
                                'length' : length_div
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                prev.after(data);
                                enterTab();
                                callInputMaskDecimal();
                                TolTalMoney();
                                ToltalExChange();
                                renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);
                                append_hoach_toan_row_thue();
                            }
                        });
                    }
                });
                $(document).on('change', ".lua-chon-type", function(event) {
                    var type = $(this).val();
                    if (type == "{!! \App\Define\Workflow::TYPE_AIR !!}") {
                        $(this).closest('table').find('.for_mawb').html('{!! trans("input_invoices.mawb") !!}');
                        $(this).closest('table').find('.for_hawb').html('{!! trans("input_invoices.hawb") !!}');
                        $(this).closest('table').find("input[name='hawb']").prop('disabled', false);
                    } else if(type == "{!! \App\Define\Workflow::TYPE_SEA !!}") {
                        $(this).closest('table').find('.for_mawb').html('{!! trans("input_invoices.hbl") !!}');
                        $(this).closest('table').find('.for_hawb').html('{!! trans("input_invoices.obl") !!}');
                        $(this).closest('table').find("input[name='hawb']").prop('disabled', false);
                    } else if(type == "{!! \App\Define\Workflow::TYPE_DOM !!}") {
                        $(this).closest('table').find('.for_mawb').html('{!! trans("input_invoices.invoice") !!}');
                        $(this).closest('table').find('.for_hawb').html('&nbsp;');
                        $(this).closest('table').find("input[name='hawb']").prop('disabled', true);
                    } else {
                        toastr.error("");
                        return false;
                    }
                });
                $(document).on('click', '.remove-row', function(event) {
                    $(this).closest('div.remove-love').remove();
                });
                $(document).on('change', ".sci", function(event) {
                });
                 var count = 1;
                $(document).on('click', '.clone-row-hoach-toan', function(event) {

                    var data_id = $(this).data("id");
                    var service_tab = $(this).closest('#service-content');
                    var data_ = $(this).data("append");
                    // var count = count + 1;
                    var html = '<tr class="apeendrow">\n' +
                        '                                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">\n' +
                        '                                        <p style="vertical-align: middle; text-align: left;">\n' +
                            {{--'                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">\n' +--}}
                                    {{--'                                                <i class="far fa-eye"></i>\n' +--}}
                                    {{--'                                            </a>\n' +--}}
                                '                                            &nbsp;&nbsp;\n' +
                        '                                            <a data-id="'+data_id+'" tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-hoach-toan text-success"><i class="fas fa-copy"></i></a>\n' +
                        '                                            <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-hoach-toan text-success mf-1"><i class="fas fa-trash"></i></a>\n'+
                        '                                        </p>\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap; min-width: 120px;">\n' +
                        '                                        <div class="form-group">\n' +
                        '                                            <select id="ls-data" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][sci]" class="form-control select2 ls-data sci" style="width: 100%;">'+
                        '                                                @foreach($saleCostItems as $key => $ls)' +
                        '                                                    <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>\n' +
                        '                                                @endforeach\n' +
                        '                                            </select>\n' +
                        '                                        </div>\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap; min-width: 180px;" class="child-description" id="html">\n' +
                        '                                        <input id="description-'+count+'" type="text" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][description]" class="form-control description ">\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap; min-width: 120px;">\n' +
                        '                                        <select  name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][debt_account]" class="form-control selectdect2020" style="width: 100%;">\n' +
                        '                                                </select>\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">\n' +
                        '                                        <input id="account_has-'+count+'" type="text" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][account_has]" class="form-control tk-co">\n' +
                        '                                    </td>\n' +
                        '                                        <td style="white-space: nowrap; min-width: 90px;">\n' +
                        '                                            <input id="unit_price-'+count+'" type="text" step="any" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][unit_price]" class=" input-type-number-format form-control currency unit_price">\n' +
                        '                                        </td>'+
                        '                                    <td style="white-space: nowrap; min-width: 90px;">\n' +
                        '                                        <input id="amount-'+count+'" type="text" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][amount]" class=" input-type-number-format form-control currency amount">\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap; min-width: 90px;">\n' +
                        '                                        <input id="into_money-'+count+'" type="text" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][into_money]" class=" input-type-number-format form-control int_money total currency">\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap;">\n' +
                        '                                        <input id="exchange-'+count+'" type="text" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][exchange]" class=" exchange form-control currency input-type-number-format">\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap;">\n' +
                        '                                        <div class="form-group">\n' +
                        '                                            @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())' +
                        '                                            <select id="ls-data" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][job_code]" class="form-control select2 sci" style="width: 100%;">\n' +
                        '                                                @foreach($vat_percent as $key => $ls)' +
                        '                                                    <option  value="{{ @$key }}">{{ @$ls }}</option>\n' +
                        '                                                @endforeach' +
                        '                                            </select>\n' +
                        '                                        </div>\n' +
                        '                                    </td>\n' +
                        '                                    <td style="white-space: nowrap;">\n' +
                        '                                        <input type="text" name="obj[hoach_toan]['+data_id+'][chi_tiet]['+count+'][branch_code]" class="form-control ">\n' +
                        '                                    </td>\n' +
                        '                                </tr>'
                    var append = $(this).closest('table');
                    append.find('tbody#tbody').append(html);
                    count = count + 1;
                    callInputMaskDecimal();
                    TolTalMoney();
                    ToltalExChange();
                    renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);

                    /*test append theo so lan click */
                    var xac_dinh_vi_tri = $(this).closest('div#service-content');
                    var hoach_toan = xac_dinh_vi_tri.find('div#hoach_toan');
                    var thue = xac_dinh_vi_tri.find('div#thue');

                    var tim_kiem_hoach_toan = hoach_toan.find('tr.apeendrow').length;
                    var xac_vi_tri_dinh_thue = thue.find('tbody#tbody-thue > tr.apeendrow');
                    var x = document.activeElement.value;
                    document.getElementsByClassName("clone-row-hoach-toan").innerHTML = x;
                    console.log($( ":focus" ));
                    return;
                    append_data_thue();
                    console.log(xac_vi_tri_dinh_thue.length);
                    var phan_tu_cha_hoach_toan = xac_dinh_vi_tri.find('div.remove-love');
                    /*test*/

                    return;
                });
            });
        }(window.jQuery);
        $(function () {
            $(document).on('change', '.ls-data', function(event) {
                var description = '';
                var sale = '';
                var tr = $(this).closest('tr');
                $('.ls-data :selected').each(function(i, obj) {
                    description = $(this).data('description');
                    sale = $(this).data('sale');
                });
                var id_description = tr.find('input.description').attr('id');
                var account_has = tr.find('input.tk-co').attr('id');
                $('#'+id_description+'').val(description)
                $('#'+account_has+'').val(sale)
            });

            $(document).on('click', '.clone-trash-hoach-toan', function(event) {
                if(confirm("Are you sure you want to delete this?")){
                    event.preventDefault();
                    console.log($(this).closest('tr'));
                    $(this).closest('tr').remove();
                }
                else{
                    return false;
                }
            });
            TolTalMoney();
            ToltalExChange();
            callInputMaskDecimal();
        })
    </script>
@endpush
