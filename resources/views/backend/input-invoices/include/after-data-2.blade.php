<?php
$type = $count + 1;
$mawb = $count + 1;
$hawb = $count + 1;
$sci = $count + 1;
$data_id = $count + 1;
$append = 1;
?>
@if($count)
    <div class="remove-love">
        <div class="col-md-3">
            <table class="table table-responsive">
                <tr>
                    <td style="width: 50%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                        <span class="for_type">{!! trans('input_invoices.type') !!}</span>
                    </td>
                    <td style="width: 25%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                        <span class="for_mawb">{!! trans('input_invoices.mawb') !!}</span>
                    </td>
                    <td style="white-space: nowrap; min-width: 100px;background: #3C8DBC;color: white;">
                        <span class="for_hawb">{!! trans('input_invoices.hawb') !!}</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%; white-space: nowrap; min-width: 100px;">
                        {!! Form::select('obj[hoach_toan]['.$type.'][type]', \App\Define\Workflow::getAllReportsForOption(), old('type', \App\Define\Workflow::getTypeAit()  ), ['class' => 'lua-chon-type type form-control select2']) !!}
                    </td>
                    <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                        {!! Form::text('obj[hoach_toan]['.$mawb.'][mawb]', old('mawb'), ['class' => 'form-control']) !!}
                    </td>
                    <td style="white-space: nowrap; min-width: 100px;">
                        {!! Form::text('obj[hoach_toan]['.$hawb.'][hawb]', old('hawb'), ['class' => 'form-control']) !!}
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="">
                    <div class="row section_type">
                        <div class="col-md-12">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <thead style="background: #3C8DBC;color: white;">
                                    <th>{!! trans('system.action.label') !!}</th>
                                    <th>{!! trans('input_invoices.sale_cost_item') !!}</th>
                                    <th>{!! trans('input_invoices.description') !!}</th>
                                    <th>{!! trans('input_invoices.tk_no') !!}</th>
                                    <th>{!! trans('input_invoices.tk_co') !!}</th>
                                    <th>{!! trans('input_invoices.don_gia') !!}</th>
                                    <th>{!! trans('input_invoices.so_luong') !!}</th>
                                    <th>{!! trans('input_invoices.thanh_tien') !!}</th>
                                    <th>{!! trans('input_invoices.quy_doi') !!}</th>
                                    <th>{!! trans('input_invoices.jobcode') !!}</th>
                                    <th>{!! trans('input_invoices.branch_code') !!}</th>
                                    </thead>
                                    <tbody id="tbody" class="tbodys table_hoan_tien">
                                    <tr class="apeendrow">
                                        <td  style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                            <p style="vertical-align: middle; text-align: left;">
                                                {{--                                                                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">--}}
                                                {{--                                                                                                <i class="far fa-eye"></i>--}}
                                                {{--                                                                                            </a>--}}
                                                &nbsp;&nbsp;
                                                <a data-append="0" data-id="{{$data_id}}" tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-hoach-toan text-success">
                                                    <i class="fas fa-copy"></i></a>

                                                <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-hoach-toan text-success mf-1"><i class="fas fa-trash"></i></a>
                                            </p>
                                        </td>
                                        <td class="sale_account_code" style="white-space: nowrap; min-width: 120px;">
                                            <div class="form-group data-bind-ls" data-count="0">
                                                <select id="ls-data" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][sci]" class="form-control ls-data select2 sci" style="width: 100%;">
                                                    @foreach($saleCostItems as $key => $ls)
                                                        <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td  style="white-space: nowrap; min-width: 180px;" class="child-description description" id="html">
                                            <input type="text" id="description-0-{{$sci}}" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][description]" class="form-control description">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 120px;">
                                            <div class="form-group data-bind-ls" data-count="0">
                                                <select  name="obj[hoach_toan][{{$count++}}][{{$sci}}][chi_tiet][0][debt_account]" class="form-control selectdect2020" style="width: 100%;">
                                                </select>
                                            </div>
                                            {{--                                            <input  type="text" class="form-control" name="obj[job_code[]]">--}}
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">
                                            <input id="account_has-0-{{$sci}}" type="text" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][account_has]" class="form-control tk-co">
                                        </td>
                                        <div class="milt">
                                            <td style="white-space: nowrap; min-width: 90px;">
                                                <input id="unit_price-0-{{$sci}}" type="text" step="any" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][unit_price]" class="form-control currency unit_price">
                                            </td>
                                            <td style="white-space: nowrap; min-width: 90px;">
                                                <input id="amount-0-{{$sci}}" type="text" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][amount]" class="form-control currency amount">
                                            </td>
                                            <td style="white-space: nowrap; min-width: 90px;">

                                                <input id="into_money-0-{{$sci}}" class="form-control currency int_money"  type="text" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][into_money]"  value="">
                                            </td>
                                        </div>
                                        <td style="white-space: nowrap;">
                                            <input id="exchange-0-{{$sci}}" type="text" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][exchange]" class="form-control currency input-type-number-format exchange">
                                        </td>
                                        <td style="white-space: nowrap;">
                                            <div class="form-group">
                                                @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())
                                                <select id="ls-data" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][job_code]" class="form-control select2 sci" style="width: 100%;">
                                                    @foreach($vat_percent as $key => $ls)
                                                        <option  value="{{ @$key }}">{{ @$ls }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td style="white-space: nowrap;">
                                            <input type="text" name="obj[hoach_toan][{{$sci}}][chi_tiet][0][branch_code]" class="form-control">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endif