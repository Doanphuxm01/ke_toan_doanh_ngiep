<?php
$dem = $count + 1;
?>
@if($count)
<div class="append-thue-pre">
    <div class="col-md-3">
        <table class="table table-responsive">
            <tr>
                <td style="width: 50%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                    <span class="for_type">{!! trans('input_invoices.type') !!}</span>
                </td>
                <td style="width: 25%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                    <span class="for_mawb">{!! trans('input_invoices.mawb') !!}</span>
                </td>
                <td style="white-space: nowrap; min-width: 100px;background: #3C8DBC;color: white;">
                    <span class="for_hawb">{!! trans('input_invoices.hawb') !!}</span>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; white-space: nowrap; min-width: 100px;">
                    {!! Form::select('obj[hoach_toan]['.$dem.'][chi_tiet][0][detail_thue][type]', \App\Define\Workflow::getAllReportsForOption(), old('type', \App\Define\Workflow::TYPE_AIR), ['class' => 'type form-control select2']) !!}
                </td>
                <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                    {!! Form::text('obj[hoach_toan]['.$dem.'][chi_tiet][0][detail_thue][mawb]', old('mawb'), ['class' => 'form-control']) !!}
                </td>
                <td style="white-space: nowrap; min-width: 100px;">
                    {!! Form::text('obj[hoach_toan]['.$dem.'][chi_tiet][0][detail_thue][hawb]', old('hawb'), ['class' => 'form-control']) !!}
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-9">
        <div class="box">
            <div class="">
                <div class="row section_type">
                    <div class="col-md-12">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead style="background: #3C8DBC;color: white;">
                                <th>{!! trans('system.action.label') !!}</th>
                                <th>{!! trans('input_invoices.sale_cost_item') !!}</th>
                                <th>{!! trans('input_invoices.dien_gia_thue') !!}</th>
                                <th>{!! trans('input_invoices.tk_no') !!}</th>
                                <th>{!! trans('input_invoices.tk_co') !!}</th>
                                <th>{!! trans('input_invoices.thue_xuat') !!}</th>
                                <th>{!! trans('input_invoices.tien_thue') !!}</th>
                                <th>{!! trans('input_invoices.quy_doi') !!}</th>
                                <th>{!! trans('input_invoices.jobcode') !!}</th>
                                <th>{!! trans('input_invoices.branch_code') !!}</th>
                                </thead>
                                <tbody id="tbody" class="tbodys">
                                <tr class="apeendrow">
                                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                        <p style="vertical-align: middle; text-align: left;">
                                            {{--                                                                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">--}}
                                            {{--                                                                                                <i class="far fa-eye"></i>--}}
                                            {{--                                                                                            </a>--}}
                                            &nbsp;&nbsp;
                                            <a  tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-thue text-success">
                                                <i class="fas fa-copy"></i></a>

                                            <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-thue text-success mf-1"><i class="fas fa-trash"></i></a>
                                        </p>
                                    </td>
                                    <td style="white-space: nowrap; min-width: 120px;">
                                        <div class="form-group data-bind-ls" data-count="0">
                                            <select  name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][sci_thue]"  class="form-control ls-data-thue select2 sci" style="width: 100%;">
                                                @foreach($saleCostItems as $key => $ls)
                                                    <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td style="white-space: nowrap; min-width: 180px;" class="child-description">
                                        <input type="text" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][tax_speaker]" class="form-control">
                                    </td>
                                    <td style="white-space: nowrap; min-width: 120px;">
                                        <div class="form-group data-bind-ls" data-count="0">
                                            <select  name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][debt_account]" class="form-control selectdect2020" style="width: 100%;">
                                            </select>
                                        </div>
                                        {{--                                            <input  type="text" class="form-control" name="obj[job_code[]]">--}}
                                    </td>
                                    <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">
                                        <input type="text" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][account_has]" class="form-control tk-co">
                                    </td>
                                    <div class="milt">
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            <input type="text" step="any" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][tax]" class="form-control currency">
                                        </td>
                                        <td style="white-space: nowrap; min-width: 90px;">
                                            <input type="text" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][tax_money]" class="form-control currency">
                                        </td>
                                    </div>
                                    <td style="white-space: nowrap;">
                                        <input type="text" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][exchange]" class="form-control currency">
                                    </td>
                                    <td style="white-space: nowrap;">
                                        <div class="form-group">
                                            @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())

                                                <select id="ls-data-thue" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][job_code]" class="form-control select2 sci" style="width: 100%;">
                                                @foreach($vat_percent as $key => $ls)
                                                    <option  value="{{ @$key }}">{{ @$ls }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td style="white-space: nowrap;">
                                        <input type="text" name="obj[hoach_toan][{{ $dem }}][chi_tiet][0][detail_thue][branch_code]" class="form-control">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endif