@section('JS_REGION_BOTTOM')
    <script src="{!! asset('assets/backend/js/service.js') !!}"></script>
@endsection
    <div class="row section_type_thue append-pre2020">
        <div class="append-thue-pre">
            <div class="col-md-3">
                <table class="table table-responsive">
                    <tr>
                        <td style="width: 50%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                            <span class="for_type">{!! trans('input_invoices.type') !!}</span>
                        </td>
                        <td style="width: 25%; white-space: nowrap; min-width: 100px; background: #3C8DBC;color: white;">
                            <span class="for_mawb">{!! trans('input_invoices.mawb') !!}</span>
                        </td>
                        <td style="white-space: nowrap; min-width: 100px;background: #3C8DBC;color: white;">
                            <span class="for_hawb">{!! trans('input_invoices.hawb') !!}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; white-space: nowrap; min-width: 100px;">
                            {!! Form::select('obj[hoach_toan][0][chi_tiet][0][detail_thue][type]', \App\Define\Workflow::getAllReportsForOption(), old('type', \App\Define\Workflow::TYPE_AIR), ['class' => 'lua-chon-type type form-control select2']) !!}
                        </td>
                        <td style="width: 25%; white-space: nowrap; min-width: 100px;">
                            {!! Form::text('obj[hoach_toan][0][chi_tiet][0][detail_thue][mawb]', old('mawb'), ['class' => 'form-control']) !!}
                        </td>
                        <td style="white-space: nowrap; min-width: 100px;">
                            {!! Form::text('obj[hoach_toan][0][chi_tiet][0][detail_thue][hawb]', old('hawb'), ['class' => 'form-control']) !!}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-9">
                    <div class="box">
                        <div class="">
                            <div class="row section_type">
                                <div class="col-md-12">
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover">
                                            <thead style="background: #3C8DBC;color: white;">
                                            <th>{!! trans('system.action.label') !!}</th>
                                            <th>{!! trans('input_invoices.sale_cost_item') !!}</th>
                                            <th>{!! trans('input_invoices.dien_gia_thue') !!}</th>
                                            <th>{!! trans('input_invoices.tk_no') !!}</th>
                                            <th>{!! trans('input_invoices.tk_co') !!}</th>
                                            <th>{!! trans('input_invoices.thue_xuat') !!}</th>
                                            <th>{!! trans('input_invoices.tien_thue') !!}</th>
                                            <th>{!! trans('input_invoices.quy_doi') !!}</th>
                                            <th>{!! trans('input_invoices.jobcode') !!}</th>
                                            <th>{!! trans('input_invoices.branch_code') !!}</th>
                                            </thead>
                                            <tbody id="tbody-thue" class="tbodys-thue">
                                                <div class="total-body">
                                                    <tr class="apeendrow">
                                                        <td style="white-space: nowrap; vertical-align: middle; text-align: center;">
                                                            <p style="vertical-align: middle; text-align: left;">
                                                                {{--                                                                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">--}}
                                                                {{--                                                                                                <i class="far fa-eye"></i>--}}
                                                                {{--                                                                                            </a>--}}
                                                                &nbsp;&nbsp;
                                                                <a onclick="append_data_thue(this)" tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-thue text-success">
                                                                    <i class="fas fa-copy"></i></a>

                                                                <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-thue text-success mf-1"><i class="fas fa-trash"></i></a>
                                                            </p>
                                                        </td>
                                                        <td style="white-space: nowrap; min-width: 120px;">
                                                            <div class="form-group data-bind-ls" data-count="0">
                                                                <select  name="obj[hoach_toan][0][chi_tiet][0][detail_thue][sci_thue]" class="form-control ls-data-thue select2 sci" style="width: 100%;">
                                                                    @foreach($saleCostItems as $key => $ls)
                                                                        <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td style="white-space: nowrap; min-width: 180px;" class="child-description">
                                                            <input type="text" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][tax_speaker]" class="form-control">
                                                        </td>
                                                        <td style="white-space: nowrap; min-width: 120px;">
                                                            <div class="form-group data-bind-ls" data-count="0">
                                                                <select  name="obj[hoach_toan][0][chi_tiet][0][detail_thue][debt_account]" class="form-control selectdect2020" style="width: 100%;">
                                                                </select>
                                                            </div>
                                                            {{--                                            <input  type="text" class="form-control" name="obj[job_code[]]">--}}
                                                        </td>
                                                        <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">
                                                            <input type="text" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][account_has]" class="form-control tk-co">
                                                        </td>
                                                        <div class="milt">
                                                            <td style="white-space: nowrap; min-width: 90px;">
                                                                <input type="text" step="any" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][tax]" class="form-control currency">
                                                            </td>
                                                            <td style="white-space: nowrap; min-width: 90px;">
                                                                <input type="text" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][tax_money]" class="form-control currency">
                                                            </td>
                                                        </div>
                                                        <td style="white-space: nowrap;">
                                                            <input type="text" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][exchange]" class="form-control currency">
                                                        </td>
                                                        <td style="white-space: nowrap;">
                                                            <div class="form-group">
                                                                @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())
                                                                <select id="ls-data" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][job_code]" class="form-control select2 sci" style="width: 100%;">
                                                                    @foreach($vat_percent as $key => $ls)
                                                                        <option  value="{{ @$key }}">{{ @$ls }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td style="white-space: nowrap;">
                                                            <input type="text" name="obj[hoach_toan][0][chi_tiet][0][detail_thue][branch_code]" class="form-control">
                                                        </td>
                                                    </tr>
                                                </div>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
        </div>
        <div class="append-pre"> </div>
        <button id="addRowThue" onclick="append_hoach_toan_row_thue(this)" type="button" class="add-segment-thue btn btn-block btn-success btn-xs"><i class="fas fa-plus"></i>{!! trans('system.action.create') !!}</button>
    </div>
    {{--use jsavascript in blade--}}
    <style>
        .vendor-dropdown {
            width: 800px !important;
        }

        .custom-dropdown {
            width: 500px !important;
            left: -300px !important;
        }
    </style>
@push('JS_REGION_BOTTOM_MAIN')
    <script>
        function Append_hoach_toan_thue(event){
            $('.append-pre').after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.input-invoices.include.after-data-thue", compact('saleCostItems'))->render()) !!}');
            // var obj = $('.add-segment-thue').prev();
            {{--jQuery(event).prev().after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.input-invoices.include.after-data-thue", compact('saleCostItems'))->render()) !!}');--}}
            enterTab();
            // TolTalMoney();
            // ToltalExChange();
            renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);
        }
        function append_hoach_toan_row_thue(dom,name){
            if(!issetJs(dom)) {
                data_thue_after();
            }else{
                if(!isDoubleClicked(jQuery(dom))){
                    data_thue_after();
                }
            }
        }
        function data_thue_after(){
            var length_div = $('.append-thue-pre').length;
            var _token = jQuery('meta[name=_token]').attr("content");
            $.ajax({
                url: 'append_thue',
                type: 'POST',
                data: {
                    _token: _token,
                    'length' : length_div
                },
                dataType: 'JSON',
                success: function (data) {
                    $('.append-pre').prev().after(data);
                    enterTab();
                    callInputMaskDecimal();
                    TolTalMoney();
                    ToltalExChange();
                    INPUT_NUMBER_FORMAT();
                    renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);
                    // Append_hoach_toan_thue();
                }
            });
        }

        function copy_ban_sao_thue(event){
            append_data_thue(event);
        };
        /* bien moi truong global */
        let count = 0;
        function append_data_thue(event){
            count++;
            var html = '<tr class="apeendrow">\n' +
                '                                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">\n' +
                '                                        <p style="vertical-align: middle; text-align: left;">\n' +
                    {{--'                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">\n' +--}}
                            {{--'                                                <i class="far fa-eye"></i>\n' +--}}
                            {{--'                                            </a>\n' +--}}
                        '                                            &nbsp;&nbsp;\n' +
                '                                            <a onclick="copy_ban_sao_thue(this)" tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-thue text-success"><i class="fas fa-copy"></i></a>\n' +
                '                                            <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-thue text-success mf-1"><i class="fas fa-trash"></i></a>\n'+
                '                                        </p>\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap; min-width: 120px;">\n' +
                '                                        <div class="form-group">\n' +
                '                                            <select id="ls-data" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][sci_thue]" class="form-control select2 ls-data sci" style="width: 100%;">'+
                '                                                @foreach($saleCostItems as $key => $ls)' +
                '                                                    <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>\n' +
                '                                                @endforeach\n' +
                '                                            </select>\n' +
                '                                        </div>\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap; min-width: 180px;" class="child-description" id="html">\n' +
                '                                        <input type="text" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][tax_speaker]" class="form-control">\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap; min-width: 120px;">\n' +
                '                                        <select  name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][debt_account]" class="form-control selectdect2020" style="width: 100%;">\n' +
                '                                                </select>\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">\n' +
                '                                        <input type="text" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][account_has]" class="form-control tk-co">\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap; min-width: 90px;">\n' +
                '                                        <input type="text" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][tax]" class="form-control currency">\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap; min-width: 90px;">\n' +
                '                                        <input type="text" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][tax_money]" class="form-control total currency">\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap;">\n' +
                '                                        <input type="text" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][exchange]" class="form-control currency">\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap;">\n' +
                '                                        <div class="form-group">\n' +
                '                                            @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())' +
                '                                            <select id="ls-data" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][job_code]" class="form-control select2 sci" style="width: 100%;">\n' +
                '                                                @foreach($vat_percent as $key => $ls)' +
                '                                                    <option  value="{{ @$key }}">{{ @$ls }}</option>\n' +
                '                                                @endforeach' +
                '                                            </select>\n' +
                '                                        </div>\n' +
                '                                    </td>\n' +
                '                                    <td style="white-space: nowrap;">\n' +
                '                                        <input type="text" name="obj[hoach_toan][0][chi_tiet]['+count+'][detail_thue][branch_code]" class="form-control">\n' +
                '                                    </td>\n' +
                '                                </tr>';
            if(!issetJs(event)){
                var append = $('.section_type_thue').closest('tbody.tbodys-thue');
            }else{
                var tr = jQuery(event).closest('table');
            }
            (!issetJs(event)) ? append.find('#tbody-thue').append(html) : tr.find('tbody.tbodys-thue').append(html)

            // append.find('#tbody-thue').append(html);
            {{--callInputMaskDecimal();--}}
            {{--TolTalMoney();--}}
            {{--ToltalExChange();--}}
            {{--renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);--}}
            {{--return;--}}
        }
    </script>
    <script>
        !function ($) {
            $(function() {
                $(document).on('click', '.add-row', function(event) {
                    $(this).closest('tr').prev().after('{!! str_replace(array("\n\r", "\n", "\r"), '', view("backend.input-invoices.row-sale-cost-item", compact('saleCostItems'))->render()) !!}');
                    // $(".select2").select2().on("select2:close", function (e) {$(this).closest('td').next().find('input').focus();});
                });

                // $('.add-segment-thue').click(function (e) {
                //
                //     // $(".select2").select2().on("select2:close", function (e) {$(this).closest('td').next().find('input').focus();});
                // });
                $(document).on('change', "select[name='obj[type]']", function(event) {
                    var type = $(this).val();
                    if (type == "{!! \App\Define\Workflow::TYPE_AIR !!}") {
                        $(this).closest('table').find('.for_mawb').html('{!! trans("input_invoices.mawb") !!}');
                        $(this).closest('table').find('.for_hawb').html('{!! trans("input_invoices.hawb") !!}');
                        $(this).closest('table').find("input[name='hawb']").prop('disabled', false);
                    } else if(type == "{!! \App\Define\Workflow::TYPE_SEA !!}") {
                        $(this).closest('table').find('.for_mawb').html('{!! trans("input_invoices.hbl") !!}');
                        $(this).closest('table').find('.for_hawb').html('{!! trans("input_invoices.obl") !!}');
                        $(this).closest('table').find("input[name='hawb']").prop('disabled', false);
                    } else if(type == "{!! \App\Define\Workflow::TYPE_DOM !!}") {
                        $(this).closest('table').find('.for_mawb').html('{!! trans("input_invoices.invoice") !!}');
                        $(this).closest('table').find('.for_hawb').html('&nbsp;');
                        $(this).closest('table').find("input[name='hawb']").prop('disabled', true);
                    } else {
                        toastr.error("");
                        return false;
                    }
                });
                $(document).on('click', '.remove-row', function(event) {
                    $(this).closest('div.remove-love').remove();
                });
                $(document).on('change', ".sci", function(event) {
                });

                {{--$(document).on('click', '.clone-row-thue', function(event) {--}}
                {{--    var html = '<tr class="apeendrow">\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; vertical-align: middle; text-align: center;">\n' +--}}
                {{--        '                                        <p style="vertical-align: middle; text-align: left;">\n' +--}}
                {{--            --}}{{--'                                            <a tabindex="-1" href="javascript:void(0);" class="view-sci text-info" title="{!! trans('input_invoices.view_sale_cost_item') !!}">\n' +--}}
                {{--                    --}}{{--'                                                <i class="far fa-eye"></i>\n' +--}}
                {{--                    --}}{{--'                                            </a>\n' +--}}
                {{--                '                                            &nbsp;&nbsp;\n' +--}}
                {{--        '                                            <a tabindex="-1" href="javascript:void(0);" title="{!! trans('input_invoices.copy_current_row') !!}" class="clone-row-thue text-success"><i class="fas fa-copy"></i></a>\n' +--}}
                {{--        '                                            <a tabindex="-1" style="color: red;margin-left: 8px;" href="javascript:void(0);" class="clone-trash-thue text-success mf-1"><i class="fas fa-trash"></i></a>\n'+--}}
                {{--        '                                        </p>\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; min-width: 120px;">\n' +--}}
                {{--        '                                        <div class="form-group">\n' +--}}
                {{--        '                                            <select id="ls-data" name="obj[sci][]" class="form-control select2 ls-data sci" style="width: 100%;">'+--}}
                {{--        '                                                @foreach($saleCostItems as $key => $ls)' +--}}
                {{--        '                                                    <option data-sale="{{ @$ls['sale_account_code'] }}" data-description="{{ @$ls['description'] }}" value="{{ @$ls['id'] }}">{{ @$ls['code'] }}</option>\n' +--}}
                {{--        '                                                @endforeach\n' +--}}
                {{--        '                                            </select>\n' +--}}
                {{--        '                                        </div>\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; min-width: 180px;" class="child-description" id="html">\n' +--}}
                {{--        '                                        <input type="text" name="obj[tax_speaker][]" class="form-control">\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; min-width: 120px;">\n' +--}}
                {{--        '                                        <select  name="obj[debt_account][]" class="form-control selectdect2020" style="width: 100%;">\n' +--}}
                {{--        '                                                </select>\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; min-width: 90px;" id="tk-co" class="child-tk-co">\n' +--}}
                {{--        '                                        <input type="text" name="obj[account_has][]" class="form-control tk-co">\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; min-width: 90px;">\n' +--}}
                {{--        '                                        <input type="text" name="obj[tax][]" class="form-control currency">\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap; min-width: 90px;">\n' +--}}
                {{--        '                                        <input type="text" name="obj[tax_money][]" class="form-control total currency">\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap;">\n' +--}}
                {{--        '                                        <input type="text" name="obj[exchange][]" class="form-control currency">\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap;">\n' +--}}
                {{--        '                                        <div class="form-group">\n' +--}}
                {{--        '                                            @php($vat_percent = \App\Define\SaleCostItem::getTaxValuesForOption())' +--}}
                {{--        '                                            <select id="ls-data" name="obj[job_code][]" class="form-control select2 sci" style="width: 100%;">\n' +--}}
                {{--        '                                                @foreach($vat_percent as $key => $ls)' +--}}
                {{--        '                                                    <option  value="{{ @$key }}">{{ @$ls }}</option>\n' +--}}
                {{--        '                                                @endforeach' +--}}
                {{--        '                                            </select>\n' +--}}
                {{--        '                                        </div>\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                    <td style="white-space: nowrap;">\n' +--}}
                {{--        '                                        <input type="text" name="obj[branch_code][]" class="form-control">\n' +--}}
                {{--        '                                    </td>\n' +--}}
                {{--        '                                </tr>'--}}
                {{--    var append = $(this).closest('table');--}}
                {{--    append.find('tbody#tbody').append(html);--}}
                {{--    callInputMaskDecimal();--}}
                {{--    TolTalMoney();--}}
                {{--    ToltalExChange();--}}
                {{--    renderTableS3('.selectdect2020',"{{ route('admin.cb') }}",'vendor-dropdown',TEMPALTE_SELECT2_DATA);--}}
                {{--    return;--}}
                {{--});--}}
            });
        }(window.jQuery);
        $(function () {
            $(document).on('change', '.ls-data-thue', function(event) {
                var description = '';
                var sale = '';
                $('.ls-data :selected').each(function(i, obj) {
                    description = $(this).data('description');
                    sale = $(this).data('sale');
                });
                $(this).closest('tr').find('input[name="obj[description][]"]').val(description)
                $(this).closest('tr').find('input[name="obj[account_has][]"]').val(sale)
                // console.log($(this).closest('tr').find('input[name="obj[description[]]"]'));
            });
            $(document).on('click', '.clone-trash-thue', function(event) {
                if(confirm("Are you sure you want to delete this?")){
                    event.preventDefault();
                    console.log($(this).closest('tr'));
                    $(this).closest('tr').remove();
                }
                else{
                    return false;
                }
            });
            TolTalMoney();
        })
    </script>
@endpush
