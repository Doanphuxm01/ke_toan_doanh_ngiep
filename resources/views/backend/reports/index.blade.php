@extends('backend.master')
@section('title')
{!! trans('reports.label') !!}
@stop
@section('head')
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
<link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
<style type="text/css">
    .uppercase {
        text-transform: uppercase;
    }
</style>
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('reports.label') !!}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.reports.index') !!}">{!! trans('reports.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
            	<div class="col-md-4">
                    {!! Form::label('type', trans('reports.label') ) !!}
            		<select class="form-control select2" name="type">
            			<option value="NONE">{!!  trans('reports.types.label') !!}</option>
            			@foreach ($types as $key => $name)
                            <option value="{!! $key !!}" @if(Request::input('type') == $key) selected @endif >{!! $name !!} - {!! $key !!}</option>
                        @endforeach
            		</select>
            	</div>
            	<div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('date_range', "Khoảng thời gian") !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('date_range', Request::input('date_range', date('01/m/Y 00:00') . ' - ' . date('d/m/Y H:i')), ['class' => 'form-control pull-right date_range']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                        <button type="submit" class="btn btn-info" id="report">
                            <span class="glyphicon glyphicon-flash"></span>&nbsp; Lập báo cáo
                        </button>
                    </div>
                </div>
            	<div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                    	<button type="button"  class="btn btn-success" onclick="window.open('{!! route('admin.reports.export', ['type' => 'excel']) !!}')">
                    		<span class="far fa-file-excel fa-fw"></span>&nbsp; Xuất excel
                    	</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body no-padding">
            <div id="result" class="table-responsive">
            </div>
        </div>
    </div>
</section>
@stop
@section('footer')
<script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
<script src="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.js') !!}"></script>
<script>
    !function ($) {
        $(function(){
            $(".select2").select2({ width: '100%' });
            $('.date_range').daterangepicker({
                autoUpdateInput: false,
                "locale": {
                    "format": "DD/MM/YYYY HH:mm",
                    "separator": " - ",
                    "applyLabel": "Áp dụng",
                    "cancelLabel": "Huỷ bỏ",
                    "fromLabel": "Từ ngày",
                    "toLabel": "Tới ngày",
                    "customRangeLabel": "Tuỳ chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Thg 1",
                        "Thg 2",
                        "Thg 3",
                        "Thg 4",
                        "Thg 5",
                        "Thg 6",
                        "Thg 7",
                        "Thg 8",
                        "Thg 9",
                        "Thg 10",
                        "Thg 11",
                        "Thg 12"
                    ],
                    "firstDay": 1
                },
                ranges: {
                   'Hôm nay': [moment(), moment()],
                   'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                   '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                   'Tháng này': [moment().startOf('month'), moment()],
                   'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                },
                "alwaysShowCalendars": true,
                maxDate: moment(),
                minDate: moment().subtract(1, "years"),
            }, function(start, end, label) {
                $('.date_range').val(start.format('DD/MM/YYYY HH:mm') + " - " + end.format('DD/MM/YYYY HH:mm'));
            });
            $(".product_model").hide();
            $(".product_name").hide();
            $(".product_in_stock").hide();
            $(".product_price").hide();

            $("select[name='type']").change(function(event) {
                $('#result').html('');
                //$("#date_label").html('').append('Từ ngày');

                // $("input[name='product_model']").val('');
                // $("input[name='product_name']").val('');
                // $("select[name='product_in_stock_operator']").val(">=");
                // $("input[name='product_in_stock']").val(0);
                // $("select[name='product_price_operator']").val(">=");
                // $("input[name='product_price']").val(0);

                switch($(this).val()) {
                    case "{!! \App\Define\Report::ORDER_CPN !!}":
                    case "{!! \App\Define\Report::ORDER_BANK_NL !!}":
                    case "{!! \App\Define\Report::ORDER_ALL !!}":
                        //$("#date_label").html('').append('Ngày tạo đơn hàng từ ngày');
                        // $(".product_model").show();
                        // $(".product_name").show();
                        // $(".product_in_stock").hide();
                        // $(".product_price").show();
                        break;
                    default:
                        // $(".product_model").hide();
                        // $(".product_name").hide();
                        // $(".product_in_stock").hide();
                        // $(".product_price").hide();
                        toastr.warning('Bạn cần chọn 1 loại báo cáo.');
                }
            });

            $("#report").click(function(event) {
                var type = $("select[name='type']").val();
                if( type == 'NONE' ) {
                    toastr.warning("Bạn cần chọn 1 loại báo cáo.")
                    return false;
                }


                NProgress.start();
                $.ajax({
                    url: "{!! route('admin.reports.store') !!}",
                    type: 'POST',
                    headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                    data: { type: type, date_range: $(".date_range").val(), product_model: $("input[name='product_model']").val(), product_name: $("input[name='product_name']").val(), product_in_stock_operator: $("select[name='product_in_stock_operator']").val(), product_in_stock: $("input[name='product_in_stock']").val(), product_price_operator: $("select[name='product_price_operator']").val(), product_price: $("input[name='product_price']").val() },
                    datatype: 'json',
                    success: function(data) {
                        if (data.error) {
                            toastr.warning(data.message);
                        } else {
                            $('#result').html('').append(data.message);
                        }
                    },
                    error: function(obj, status, err) {
                        var error = $.parseJSON(obj.responseText);
                        toastr.warning(error.message);
                    }
                }).always(function() {
                    NProgress.done();
                });
            });
        });
}(window.jQuery);

</script>
@stop