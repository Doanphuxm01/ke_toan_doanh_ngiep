<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle !important;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <div class="row table-responsive" style="margin: auto;">
        <table style="width: 100%">
            <tr>
                <td colspan="2" height="40" align="left">WinMart</td>
                <td colspan="4" align="center" class="header">{!! $info['name'] !!}</td>
                <td colspan="2" style="font-style: italic" align="right">Mẫu báo cáo: {!! $type !!}</td>
            </tr>
            <tr>
                <td colspan="2" height="30" align="left">Giờ in: {{ date('H:m:s d/m/Y') }}</td>
                <td colspan="4" align="center" valign="middle"  style="font-style: italic">Từ ngày: {!! $info['from_date'] !!} - Đến ngày: {!! $info['to_date'] !!}</td>
                <td colspan="2" align="right" style="font-style: italic">Số báo cáo: {{ date('YmdHms') }}</td>
            </tr>
            <tr>
                <td colspan="8" height="30" align="right" valign="middle">Loại tiền: VNĐ</td>
            </tr>
        </table>
    </div>
    <br/>
    <div class="row table-responsive" style="margin: auto;">
        <table class="table table-bordered" style="width: 100%">
            <tr style="background-color: #92D050;">
                <td rowspan="2" height="30" align="center" valign="middle" width="5" style="border: 1px solid #000000; text-transform: uppercase;"><b>TT</b></td>
                <td rowspan="2" align="center" valign="middle" width="25" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ngày cập nhật cuối</b></td>
                <td rowspan="2" align="center" valign="middle" width="25" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ngày tạo đơn</b></td>
                <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Mã đơn hàng</b></td>
                <td colspan="4" align="center" valign="middle" width="30" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Doanh thu</b></td>
                <td rowspan="2" align="center" valign="middle" width="40" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Hình thức thanh toán</b></td>
                <td rowspan="2" align="center" valign="middle" width="40" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ghi chú</b></td>
            </tr>
            <tr>
                @if(isset($is_insert_num) && $is_insert_num)
                    <td></td><td></td><td></td><td></td>
                @endif
                <td height="30" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #92D050;"><b>Hàng bán</b></td>
                <td align="center" valign="middle" width="10" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #92D050;"><b>VAT</b></td>
                <td align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #92D050;"><b>Thu vận chuyển</b></td>
                <td align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #92D050;"><b>Hàng trả lại</b></td>
            </tr>
            <?php $counter = 1; $totalAmount = 0; $totalVat = 0; $totalShipfee = 0; $totalRefund = 0; ?>
            @foreach($data['orders'] as $item)
                <?php $totalAmount += $item->total_amount; $totalVat += ($item->vat ? (0.1 * $item->total_amount) : 0); $totalShipfee += $item->delivery_fee; ?>
                <tr>
                    <td height="30" align="center" valign="middle" style="border: 1px solid #000000;">{!! $counter++ !!}</td>
                    <td align="center" valign="middle" style="border: 1px solid #000000; white-space: nowrap;">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</td>
                    <td align="center" valign="middle" style="border: 1px solid #000000; white-space: nowrap;">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;">{!! $item->code !!}</td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($item->total_amount) !!}
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        @if($item->vat)
                            {!! \App\Helper\HString::currencyFormat(0.1 * $item->total_amount) !!}
                        @else
                            -
                        @endif
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($item->delivery_fee) !!}
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        -
                    </td>
                    <td align="center" valign="middle" style="border: 1px solid #000000; white-space: nowrap;">
                        @if($item->payment_method)
                            {!! trans("orders.payment_methods." . $item->payment_method) !!}<br/>{!! isset($is_insert_num) && $is_insert_num ? ' - ' : '' !!}
                            {!! trans("orders.payment_status." . $item->payment_status) !!}
                        @endif
                    </td>
                    <td align="" valign="middle" style="border: 1px solid #000000; white-space: nowrap;">{!! $item->message !!}</td>
                </tr>
            @endforeach
            <tr>
                <td valign="middle" height="30" colspan="4" align="center" style="border: 1px solid #000000; border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #cdc9c9;">Tổng cộng</td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #cdc9c9;">{!! \App\Helper\HString::currencyFormat($totalAmount) !!}</td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #cdc9c9;">{!! \App\Helper\HString::currencyFormat($totalVat) !!}</td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #cdc9c9;">{!! \App\Helper\HString::currencyFormat($totalShipfee) !!}</td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #cdc9c9;">{!! \App\Helper\HString::currencyFormat($totalRefund) !!}</td>
                <td style="border: 1px solid #000000; background-color: #cdc9c9;"></td>
                <td style="border: 1px solid #000000; background-color: #cdc9c9;"></td>
            </tr>
            <tr>
                <td valign="middle" height="30" colspan="4" align="center" style="border: 1px solid #000000; border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #92D050;"><b>Thực thu</b></td>
                <td align="center" valign="middle" style="border: 1px solid #000000; background-color: #92D050;" colspan="4"><b>{!! \App\Helper\HString::currencyFormat($totalAmount + $totalVat + $totalShipfee + $totalRefund) !!}</b></td>
                <td style="border: 1px solid #000000; background-color: #92D050;"></td>
                <td style="border: 1px solid #000000; background-color: #92D050;"></td>
            </tr>
        </table>
    </div>
</body>
</html>