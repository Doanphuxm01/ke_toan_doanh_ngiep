<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle !important;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <table style="width: 100%">
        <tr>
            <td height="40" colspan="2" align="left" valign="middle">WinMart</td>
            <td colspan="4" align="center" valign="middle" style="text-transform: uppercase;">{!! $info['name'] !!}</td>
            <td colspan="2" style="font-style: italic" align="right" valign="middle">Mẫu báo cáo: {!! $type !!}</td>
        </tr>
        <tr>
            <td height="30" colspan="2" align="left" valign="middle">Giờ in: {!! date('H:m:s d/m/Y') !!}</td>
            <td colspan="4" align="center" valign="middle"  style="font-style: italic">Từ ngày: {!! $info['from_date'] !!} - Đến ngày: {!! $info['to_date'] !!}</td>
            <td colspan="2" style="font-style: italic" align="right" valign="middle">Số báo cáo: {!! date('YmdHms') !!}</td>
        </tr>
        <tr>
            <td height="30" colspan="8" align="right" valign="middle">Loại tiền: VNĐ</td>
        </tr>
    </table>
    <br/>
    <table class="table table-bordered">
        <tr style="background-color: #92D050;">
            <td rowspan="2" height="30" align="center" valign="middle" width="5" style="border: 1px solid #000000;"><b>{!! trans("system.no.") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="35" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.name") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.model") !!}</b></td>
            <td colspan="3" align="center" valign="middle" width="30" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.property") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase;"><b>ĐVT</b></td>
            <td rowspan="2" align="right" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.price_ipo") !!}</b></td>
        </tr>
        <tr>
            @if(isset($is_insert_num) && $is_insert_num)
                <td></td><td></td><td></td>
            @endif
            <td align="center" height="30" valign="middle" width="15" style="border: 1px solid #000000;background-color: #92D050; text-transform: uppercase;"><b>{!! trans("products.weight") !!}</b></td>
            <td align="center" valign="middle" width="30" style="border: 1px solid #000000;background-color: #92D050; text-transform: uppercase;"><b>{!! trans("products.size") !!}</b></td>
            <td align="center" valign="middle" width="30" style="border: 1px solid #000000;background-color: #92D050; text-transform: uppercase;"><b>{!! trans("products.other_properties") !!}</b></td>
        </tr>
        <?php $i = 1; ?>
        @foreach($data['productsNoChild'] as $product)
            <tr>
                <td height="30" align="center" valign="middle" style="border: 1px solid #000000;">{!! $i++ !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->name !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->model !!}</td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->weight) !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->length !!}x{!! $product->width !!}x{!! $product->height !!}</td>
                <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat( $product->price_ipo ) !!} </td>
            </tr>
        @endforeach
        @foreach($data['productsHaveChildren'] as $product)
            @if(count($product->children) == 1)
                <tr>
                    <td height="30" align="center" valign="middle" style="border: 1px solid #000000;">{!! $i++ !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->name !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->model !!}</td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->children{0}->weight) !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->length !!}{!! $product->children{0}->width !!}x{!! $product->children{0}->height !!}</td>
                    <td valign="middle" style="border: 1px solid #000000;">
                        <?php $k = 0; ?>
                        @foreach($product->children{0}->attributes as $key => $value )
                            @if($k<>0) | @endif
                            {!! $key !!}: {!! $value !!}
                            <?php $k++; ?>
                        @endforeach
                    </td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat( $product->children{0}->price_ipo ) !!} </td>
                </tr>
            @else
                <?php $j = 0; $counterChild = $product->children->count(); ?>
                @foreach($product->children as $child)
                    <?php $j++; ?>
                    <tr>
                        @if(round($counterChild/2) == $j)
                            <td align="center" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">{{ $i++ }}</td>
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">{{ $child->name }}</td>
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">{{ $child->model }}</td>
                        @elseif(isset($is_insert_num) && $is_insert_num && $j==1)
                            <td style="border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @elseif($counterChild == $j)
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @else
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @endif
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($child->weight) !!}</td>
                        <td align="" valign="middle" style="border: 1px solid #000000;">{!! $child->length !!}x{!! $child->width !!}x{!! $child->height !!}</td>
                        <td height="30" valign="middle" style="border: 1px solid #000000;">
                            <?php $k = 0; ?>
                            @foreach($child->attributes as $key => $value )
                                @if($k<>0) | @endif
                                {!! $key !!}: {!! $value !!}
                                <?php $k++; ?>
                            @endforeach
                        </td>
                        <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat( $child->price_ipo ) !!} </td>
                    </tr>
                @endforeach
            @endif
        @endforeach
    </table>
</body>
</html>