<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle !important;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <table style="width: 100%">
        <tr>
            <td height="40" colspan="2" align="left" valign="middle">WinMart</td>
            <td colspan="4" align="center" valign="middle" style="text-transform: uppercase;">{!! $info['name'] !!}</td>
            <td colspan="2" style="font-style: italic" align="right" valign="middle">Mẫu báo cáo: {!! $type !!}</td>
        </tr>
        <tr>
            <td height="30" colspan="2" align="left" valign="middle">Giờ in: {!! date('H:m:s d/m/Y') !!}</td>
            <td colspan="4" align="center" valign="middle"  style="font-style: italic">Từ ngày: {!! $info['from_date'] !!} - Đến ngày: {!! $info['to_date'] !!}</td>
            <td colspan="2" style="font-style: italic" align="right" valign="middle">Số báo cáo: {!! date('YmdHms') !!}</td>
        </tr>
        <tr>
            <td height="30" colspan="8" align="right" valign="middle">Loại tiền: VNĐ</td>
        </tr>
    </table>
    <br/>
    <table class="table table-bordered">
        <tr style="background-color: #92D050;">
            <td align="center" valign="middle" width="10" style="border: 1px solid #000000; text-transform: uppercase;"><b>ID</b></td>
            <td align="center" valign="middle" width="35" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.name") !!}</b></td>
            <td align="center" valign="middle" width="35" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.category") !!}</b></td>
            <td align="center" valign="middle" width="30" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.property") !!}</b></td>
            <td align="right" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.price_ipo") !!}</b></td>
            <td align="right" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.price_sale") !!}</b></td>
            <td align="right" valign="middle" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.href_url") !!}</b></td>
            <td align="right" valign="middle" style="border: 1px solid #000000; text-transform: uppercase;"><b>{!! trans("products.href_image") !!}</b></td>
        </tr>
        <?php $i = 1; ?>
        @foreach($data['productsNoChild'] as $product)
            <?php $productPromotion = App\Product::getById($product->id); ?>
            <tr>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->id !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->name !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! isset($data['categories'][$product->product_category_id]) ? $data['categories'][$product->product_category_id] : '-' !!}</td>
                <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->price_ipo) !!} </td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! isset($productPromotion['promotion_id']) ? \App\Helper\HString::roundAmountWithDiscountPercentFormat($productPromotion['price_ipo'], $productPromotion['promotion_percent']) : '-' !!} </td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">
                    <a href="{!! route('home.product', ['slug' => str_slug($product->name), 'id' => $product->id]) !!}" >
                        {!! route('home.product', ['slug' => str_slug($product->name), 'id' => $product->id]) !!}
                    </a>
                </td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">
                    <a href="{!! asset($product->image) !!}">
                        {!! asset($product->image) !!}
                    </a>
                </td>
            </tr>
        @endforeach
        @foreach($data['productsHaveChildren'] as $product)
            @if(count($product->children) == 1)
                <?php $productPromotion = App\Product::getById($product->children{0}->id); ?>
                <tr>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->id !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->name !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! isset($data['categories'][$product->children{0}->product_category_id]) ? $data['categories'][$product->children{0}->product_category_id] : '-' !!}</td>
                    <td valign="middle" style="border: 1px solid #000000;">
                        <?php $k = 0; ?>
                        @foreach($product->children{0}->attributes as $key => $value )
                            @if($k<>0) | @endif
                            {!! $key !!}: {!! $value !!}
                            <?php $k++; ?>
                        @endforeach
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat( $product->children{0}->price_ipo ) !!} </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! isset($productPromotion['promotion_id']) ? \App\Helper\HString::roundAmountWithDiscountPercentFormat($productPromotion['price_ipo'], $productPromotion['promotion_percent']) : '-' !!} </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        <a href="{!! route('home.product', ['slug' => str_slug($product->children{0}->name), 'id' => $product->children{0}->id]) !!}">
                            {!! route('home.product', ['slug' => str_slug($product->children{0}->name), 'id' => $product->children{0}->id]) !!}
                        </a>
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        <a href="{!! asset($product->children{0}->image) !!}">
                            {!! asset($product->children{0}->image) !!}
                        </a>
                    </td>
                </tr>
            @else
                <?php $j = 0; $counterChild = $product->children->count(); ?>
                @foreach($product->children as $child)
                    <?php $j++; $productPromotion = App\Product::getById($product->children{0}->id); ?>
                    <tr>
                        @if(round($counterChild/2) == $j)
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                {!! $child->id !!}
                            </td>
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">
                                {!! $child->name !!}
                            </td>
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">  {!! isset($data['categories'][$child->product_category_id]) ? $data['categories'][$child->product_category_id] : '-' !!}
                            </td>
                        @elseif((isset($is_insert_num) && $is_insert_num && $j==1) || $counterChild == $j)
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @else
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @endif
                        <td height="30" valign="middle" style="border: 1px solid #000000;">
                            <?php $k = 0; ?>
                            @foreach($child->attributes as $key => $value )
                                @if($k<>0) | @endif
                                {!! $key !!}: {!! $value !!}
                                <?php $k++; ?>
                            @endforeach
                        </td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat( $child->price_ipo )
                            !!} </td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! isset($productPromotion['promotion_id']) ? \App\Helper\HString::roundAmountWithDiscountPercentFormat($productPromotion['price_ipo'], $productPromotion['promotion_percent']) : '-' !!} </td>

                        <td align="right" valign="middle" style="border: 1px solid #000000;">
                            <a href="{!! route('home.product', ['slug' => str_slug($child->name), 'id' => $child->id]) !!}">
                                {!! route('home.product', ['slug' => str_slug($child->name), 'id' => $child->id]) !!}
                            </a>
                        </td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">
                            <a href="{!! asset($child->image) !!}">
                                {!! asset($child->image) !!}
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endforeach
    </table>
</body>
</html>