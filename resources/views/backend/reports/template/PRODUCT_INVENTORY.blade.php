<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle !important;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <table style="width: 100%">
        <tr>
            <td height="40" colspan="2" align="left" valign="middle">WinMart</td>
            <td colspan="4" align="center" valign="middle">{!! $info['name'] !!}</td>
            <td colspan="2" style="font-style: italic" align="right" valign="middle">Mẫu báo cáo: {!! $type !!}</td>
        </tr>
        <tr>
            <td height="30" colspan="2" align="left" valign="middle">Giờ in: {!! date('H:m:s d/m/Y') !!}</td>
            <td colspan="4" align="center" valign="middle"  style="font-style: italic">Từ ngày: {!! $info['from_date'] !!} - Đến ngày: {!! $info['to_date'] !!}</td>
            <td colspan="2" style="font-style: italic" align="right" valign="middle">Số báo cáo: {!! date('YmdHms') !!}</td>
        </tr>
        <tr>
            <td height="30" colspan="8" align="right" valign="middle">Loại tiền: VNĐ</td>
        </tr>
    </table>
    <br/>
    <div class="row table-responsive" style="margin: auto;">
    <table class="table table-bordered">
        <tr style="background-color: #92D050;">
            <td rowspan="2" height="30" align="center" valign="middle" width="5" style="border: 1px solid #000000;"><b>{!! trans("system.no.") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="35" style="border: 1px solid #000000;"><b>{!! trans("products.name") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000;"><b>{!! trans("products.model") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="30" style="border: 1px solid #000000;"><b>{!! trans("products.property") !!}</b></td>
            <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000;"><b>ĐVT</b></td>
            <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000;"><b>Tồn đầu</b></td>
            @for ($timeStamp = $data['timeStampFrom']; $timeStamp <= $data['timeStampTo']; $timeStamp+=86400)
                <td colspan="2" align="center" valign="middle" width="35" style="border: 1px solid #000000;"><b>{!! date("d/m", $timeStamp) !!}</b></td>
            @endfor
            <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000;"><b>Tổng nhập</b></td>
            <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000;"><b>Tổng xuất</b></td>
            <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000;"><b>Tồn cuối</b></td>
        </tr>
        <tr>
            @if(isset($is_insert_num) && $is_insert_num)
                <td></td><td></td><td></td><td></td><td></td><td></td>
            @endif
            @for ($timeStamp = $data['timeStampFrom']; $timeStamp <= $data['timeStampTo']; $timeStamp+=86400)
                <td align="center" height="30" valign="middle" width="20" style="border: 1px solid #000000;background-color: #92D050;"><b>{!! trans("system.import") !!}</b></td>
                <td align="center" valign="middle" width="15" style="border: 1px solid #000000;background-color: #92D050;"><b>{!! trans("system.export") !!}</b></td>
            @endfor
        </tr>
        <?php $i = 1; ?>
        @foreach($data['productsNoChild'] as $product)
            <?php $importQuantity = $exportQuantity = 0; ?>
            <tr>
                <td height="30" align="center" valign="middle" style="border: 1px solid #000000;">{!! $i++ !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->name !!}</td>
                <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->model !!}</td>
                <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->in_stock) !!} </td>
                @for ($timeStamp = $data['timeStampFrom']; $timeStamp <= $data['timeStampTo']; $timeStamp+=86400)
                    @if(isset($product['in_stock_import'][$timeStamp]))
                        <?php $importQuantity += $product['in_stock_import'][$timeStamp]; ?>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product['in_stock_import'][$timeStamp]) !!} </td>
                    @else
                        <td align="right" valign="middle" style="border: 1px solid #000000;">0</td>
                    @endif
                    @if(isset($product['in_stock_export'][$timeStamp]))
                        <?php $exportQuantity += $product['in_stock_export'][$timeStamp]; ?>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product['in_stock_export'][$timeStamp]) !!} </td>
                    @else
                        <td align="right" valign="middle" style="border: 1px solid #000000;">0</td>
                    @endif
                @endfor
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($importQuantity) !!} </td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($exportQuantity) !!} </td>
                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->quantity) !!} </td>
            </tr>
        @endforeach
        @foreach($data['productsHaveChildren'] as $product)
            @if(count($product->children) == 1)
                <?php $importQuantity = $exportQuantity = 0; ?>
                <tr>
                    <td height="30" align="center" valign="middle" style="border: 1px solid #000000;">{!! $i++ !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->name !!}</td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">{!! $product->children{0}->model !!}</td>
                    <td valign="middle" style="border: 1px solid #000000;">
                        <?php $k = 0; ?>
                        @foreach($product->children{0}->attributes as $key => $value )
                            @if($k<>0) | @endif
                            {!! $key !!}: {!! $value !!}
                            <?php $k++; ?>
                        @endforeach
                    </td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->in_stock) !!} </td>
                    @for ($timeStamp = $data['timeStampFrom']; $timeStamp <= $data['timeStampTo']; $timeStamp+=86400)
                        @if(isset($product->children{0}->in_stock_import[$timeStamp]))
                            <?php $importQuantity += $product->children{0}->in_stock_import[$timeStamp]; ?>
                            <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->children{0}->in_stock_import[$timeStamp]) !!} </td>
                        @else
                            <td align="right" valign="middle" style="border: 1px solid #000000;">0</td>
                        @endif
                        @if(isset($product->children{0}->in_stock_export[$timeStamp]))
                            <?php $exportQuantity += $product->children{0}->in_stock_export[$timeStamp]; ?>
                            <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->children{0}->in_stock_export[$timeStamp]) !!} </td>
                        @else
                            <td align="right" valign="middle" style="border: 1px solid #000000;">0</td>
                        @endif
                    @endfor
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($importQuantity) !!} </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($exportQuantity) !!} </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($product->children{0}->quantity) !!} </td>
                </tr>
            @else
                <?php $j = 0; $counterChild = $product->children->count(); ?>
                @foreach($product->children as $child)
                    <?php $j++; $importQuantity = $exportQuantity = 0; ?>
                    <tr>
                        @if(round($counterChild/2) == $j)
                            <td align="center" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">{{ $i++ }}</td>
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">{{ $child->name }}</td>
                            <td align="" valign="middle" style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;">{{ $child->model }}</td>
                        @elseif((isset($is_insert_num) && $is_insert_num && $j==1) || $counterChild == $j)
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @else
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                            <td style="border: 1px solid #fff; border-right: 1px solid #000000; border-left: 1px solid #000000;"></td>
                        @endif
                        <td height="30" valign="middle" style="border: 1px solid #000000;">
                            <?php $k = 0; ?>
                            @foreach($child->attributes as $key => $value )
                                @if($k<>0) | @endif
                                {!! $key !!}: {!! $value !!}
                                <?php $k++; ?>
                            @endforeach
                        </td>
                        <td align="center" valign="middle" style="border: 1px solid #000000;">-</td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($child->in_stock) !!} </td>
                        @for ($timeStamp = $data['timeStampFrom']; $timeStamp <= $data['timeStampTo']; $timeStamp+=86400)
                            @if(isset($child->in_stock_import[$timeStamp]))
                                <?php $importQuantity += $child->in_stock_import[$timeStamp]; ?>
                                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($child->in_stock_import[$timeStamp]) !!} </td>
                            @else
                                <td align="right" valign="middle" style="border: 1px solid #000000;">0</td>
                            @endif
                            @if(isset($child->in_stock_export[$timeStamp]))
                                <?php $exportQuantity += $child->in_stock_export[$timeStamp]; ?>
                                <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($child->in_stock_export[$timeStamp]) !!} </td>
                            @else
                                <td align="right" valign="middle" style="border: 1px solid #000000;">0</td>
                            @endif
                        @endfor
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($importQuantity) !!} </td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($exportQuantity) !!} </td>
                        <td align="right" valign="middle" style="border: 1px solid #000000;">{!! \App\Helper\HString::currencyFormat($child->quantity) !!} </td>
                    </tr>
                @endforeach
            @endif
        @endforeach
    </table>
    </div>
</body>
</html>