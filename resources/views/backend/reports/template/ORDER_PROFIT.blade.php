<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle !important;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <div class="row table-responsive" style="margin: auto;">
        <table style="width: 100%">
            <tr>
                <td colspan="2" height="40" align="left">WinMart</td>
                <td colspan="4" align="center" class="header">{!! $info['name'] !!}</td>
                <td colspan="2" style="font-style: italic" align="right">Mẫu báo cáo: {!! $type !!}</td>
            </tr>
            <tr>
                <td colspan="2" height="30" align="left">Giờ in: {{ date('H:m:s d/m/Y') }}</td>
                <td colspan="4" align="center" valign="middle"  style="font-style: italic">Từ ngày: {!! $info['from_date'] !!} - Đến ngày: {!! $info['to_date'] !!}</td>
                <td colspan="2" align="right" style="font-style: italic">Số báo cáo: {{ date('YmdHms') }}</td>
            </tr>
            <tr>
                <td colspan="8" height="30" align="right" valign="middle">Loại tiền: VNĐ</td>
            </tr>
        </table>
    </div>
    <br/>
    <div class="row table-responsive" style="margin: auto;">
        <table class="table table-bordered" style="width: 100%">
            <tr style="background-color: #92D050;">
                <td height="30" align="center" valign="middle" width="5" style="border: 1px solid #000000; text-transform: uppercase;"><b>TT</b></td>
                <td align="center" valign="middle" width="25" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ngày bán</b></td>
                <td align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Mã đơn hàng</b></td>
                <td align="center" valign="middle" width="30" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Giá vốn</b></td>
                <td align="center" valign="middle" width="40" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Giá bán</b></td>
                <td align="center" valign="middle" width="40" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Lãi/Lỗ</b></td>
            </tr>
            <?php $counter = 1; $original = $sale = $profit = 0; ?>
            @foreach($data['orders'] as $item)
                <?php
                    $orderOriginal = $orderSale = 0;
                    foreach ($item->details()->get() as $detail) {
                        $orderOriginal += ($detail->price_original * $detail->quantity);
                        $orderSale += ($detail->price_unit * $detail->quantity);
                    }
                    $original += $orderOriginal;
                    $sale += $orderSale;
                    $profit += ($orderSale - $orderOriginal);
                ?>
                <tr>
                    <td height="30" align="center" valign="middle" style="border: 1px solid #000000;">{!! $counter++ !!}</td>
                    <td align="center" valign="middle" style="border: 1px solid #000000; white-space: nowrap;">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;">{!! $item->code !!}</td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($orderOriginal) !!}
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($orderSale) !!}
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($orderSale - $orderOriginal) !!}
                    </td>
                </tr>
            @endforeach
            <tr>
                <td valign="middle" height="30" colspan="3" align="center" style="border: 1px solid #000000; border: 1px solid #000000; text-transform: uppercase; white-space: nowrap; background-color: #92D050;"><b>Tổng cộng</b></td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #92D050;"><b>{!! \App\Helper\HString::currencyFormat($original) !!}</b></td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #92D050;"><b>{!! \App\Helper\HString::currencyFormat($sale) !!}</b></td>
                <td align="right" valign="middle" style="border: 1px solid #000000; background-color: #92D050;"><b>{!! \App\Helper\HString::currencyFormat($profit) !!}</b></td>
            </tr>
        </table>
    </div>
</body>
</html>