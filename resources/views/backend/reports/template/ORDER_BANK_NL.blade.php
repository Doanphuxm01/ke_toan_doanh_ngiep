<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle !important;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <div class="row table-responsive" style="margin: auto;">
        <table style="width: 100%">
            <tr>
                <td colspan="2" height="40" align="left">{!! env('APP_NAME') !!}</td>
                <td colspan="4" align="center" class="header">{!! $info['name'] !!}</td>
                <td colspan="2" style="font-style: italic" align="right">Mẫu báo cáo: {!! $type !!}</td>
            </tr>
            <tr>
                <td colspan="2" height="30" align="left">Giờ in: {{ date('H:m:s d/m/Y') }}</td>
                <td colspan="4" align="center" valign="middle"  style="font-style: italic">Từ ngày: {!! $info['from_date'] !!} - Đến ngày: {!! $info['to_date'] !!}</td>
                <td colspan="2" align="right" style="font-style: italic">Số báo cáo: {{ date('YmdHms') }}</td>
            </tr>
            <tr>
                <td colspan="8" height="30" align="right" valign="middle">Loại tiền: VNĐ</td>
            </tr>
        </table>
    </div>
    <br/>
    <div class="row table-responsive" style="margin: auto;">
        <table class="table table-bordered" style="width: 100%">
            @if(0 && isset( $is_insert_num ) && $is_insert_num)
                <tr style="border-bottom: 3px solid #000000;">
                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                </tr>
            @endif
            <tr style="background-color: #92D050;">
                <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ngày</td>
                <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Tên khách hàng</b></td>
                <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Số điện thoại</b></td>
                <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ngân hàng</b></td>
                <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Loại thẻ</b></td>
                <td rowspan="2" align="center" valign="middle" width="10" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Số chứng từ/Mã đơn hàng/Mã Giao dịch</b></td>
                <td rowspan="2" align="center" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Nội dung</b></td>
                <td align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;" colspan="4"><b>Số phát sinh</b></td>
                <td rowspan="2" align="right" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Số dư cuối kỳ</b></td>
                <td rowspan="2" align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Nhóm khách hàng</b></td>
                <td rowspan="2" align="center" valign="middle" width="40" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>Ghi chú</b></td>
            </tr>
            <tr style="background-color: #92D050;">
                <td align="center" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>TIỀN KHÁCH CHUYỂN</td>
                <td align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>% PHÍ</b></td>
                <td align="center" valign="middle" width="15" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>TIỀN PHÍ</b></td>
                <td align="center" valign="middle" width="20" style="border: 1px solid #000000; text-transform: uppercase; white-space: nowrap;"><b>HOÀN TRẢ SUBE</b></td>
            </tr>
            <?php $counter = 1; ?>
            @foreach($data['orders'] as $item)
                <?php
                    $customer = null;
                    if ($item->customer_id) $customer = \App\Customer::find($item->customer_id);
                ?>
                <tr>
                    <td align="center" valign="middle" style="border: 1px solid #000000;">
                        {!! date("d/m/y") !!}
                    </td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        {!! is_null($user) ? $item->delivery_fullname : $user->fullname !!}
                    </td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        {!! is_null($user) ? $item->delivery_phone : $user->phone !!}
                    </td>
                    <td align="right" valign="middle" style="border: 1px solid #000000;">
                        <?php preg_match_all('#\((.*?)\)#', $item->bank_name, $matches);?>
                        @if (isset($matches[1]) && $matches[1])
                            {!! $matches[1][0]!!}
                        @else
                            {!! $item->bank_name !!}
                        @endif
                    </td>
                    <td align="center1" valign="middle" style="border: 1px solid #000000; white-space: nowrap;">
                        {{-- {!! trans('orders.delivery_providers.' . $item->delivery_provider) !!} --}}
                    </td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;">
                        {!! $item->code !!}
                    </td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;"></td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($item->total_amount) !!}
                    </td>
                    <td align="center" valign="middle" style="border: 1px solid #000000;"></td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($item->bank_fee) !!}
                    </td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        {!! \App\Helper\HString::currencyFormat($item->total_amount-$item->bank_fee) !!}
                    </td>
                    <td align="" valign="middle" style="border: 1px solid #000000;"></td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        @if (is_null($customer))
                            Khách lẻ
                        @else
                            {!! trans('customers.levels.' . $customer->level) !!}
                        @endif
                    </td>
                    <td align="" valign="middle" style="border: 1px solid #000000;">
                        {!! $item->message !!}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</body>
</html>