@extends('backend.master')
@section('title')
    {!! trans('system.action.list') !!} {!! trans('prices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.css') !!}" />
@stop
@section('content')
<section class="content-header">
    <h1>
        {!! trans('prices.label') !!}
        <small>{!! trans('system.action.list') !!}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
        <li><a href="{!! route('admin.prices.index') !!}">{!! trans('prices.label') !!}</a></li>
    </ol>
</section>
<section class="content overlay">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{!! trans('system.action.filter') !!}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([ 'url' => route('admin.prices.index'), 'method' => 'GET', 'role' => 'search' ]) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('type', trans('prices.type')) !!}
                            {!! Form::select('type', ['' => trans('system.dropdown_all')] + $types, Request::input('type'), ['class' => 'form-control select2'])!!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('name', trans('prices.category') . ' / ' . trans('prices.product')) !!}
                            {!! Form::text('name', Request::input('name'), ['class' => "form-control"])!!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('date_range', trans('system.update_range')) !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('date_range', Request::input('date_range'), ['class' => 'form-control pull-right date_range']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('status', trans('system.status.label')) !!}
                            {!! Form::select('status', [ -1 => trans('system.dropdown_all'), 0 => trans('system.status.deactive'), 1 => trans('system.status.active') ], Request::input('status'), ['class' => 'form-control select2',  "style" => "width: 100%;"])!!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('page_num', trans('system.page_num')) !!}
                            {!! Form::select('page_num', [ 10 => '10' . trans('system.items'), 20 => '20' . trans('system.items'), 50 => '50' . trans('system.items') , 100 => '100' . trans('system.items'), 500 => '500' . trans('system.items') ], Request::input('page_num', 20), ['class' => 'form-control select2',  "style" => "width: 100%;"]) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('filter', trans('system.action.label'), ['style' => 'width: 100%;']) !!}
                            <button type="submit" class="btn btn-default btn-flat">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; {!! trans('system.action.search') !!}
                            </button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group">
                <a href="{!! route('admin.prices.create') !!}" class='btn btn-primary btn-flat'>
                    <span class="ion-plus"></span>&nbsp;{!! trans('system.action.create') !!}
                </a>
            </div>
            <div class="btn-group">
                <a href="{!! route('admin.prices.create-bulk') !!}" class='btn btn-info btn-flat'>
                    <span class="fa fa-file-excel-o"></span>&nbsp;{!! trans('system.action.import') !!}
                </a>
            </div>
            <div class="col-sm-1" style="margin-right: 3px;">
                <button type="button"  class="btn btn-success" id='btnExport'>
                    <span class="far fa-file-excel fa-fw"></span>&nbsp; {!! trans('service_purchases.export_excel') !!}
                </button>
            </div>
        </div>
        <div class="col-md-12 text-right">
            {!! $prices->appends( Request::except('page') )->render() !!}
        </div>
    </div>
    @if (count($prices) > 0)
        <div class="box">
            <div class="box-header">
                <?php $i = (($prices->currentPage() - 1) * $prices->perPage()) + 1; ?>
                <div class="form-group">
                    {!! trans('system.show_from') !!} {!! $i . ' ' . trans('system.to') . ' ' . ($i - 1 + $prices->count()) . ' ( ' . trans('system.total') . ' ' . $prices->total() . ' )' !!}
                    | <i>Chú giải: </i>
                    <span class="text-warning"><i class="glyphicon glyphicon-edit"></i> {!! trans('system.action.update') !!} </span>&nbsp;&nbsp;
                    <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> {!! trans('system.action.delete') !!} </span>&nbsp;&nbsp;
                    <span class="text-info"><i class="fas fa-history"></i> {!! trans('system.history') !!}</span>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="table-responsive">
                    <table class='table table-striped table-bordered tree'>
                        <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.no.') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('prices.type') !!}</th>
                                <th style="vertical-align: middle;">{!! trans('prices.category') !!}/{!! trans('prices.product') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('prices.price_ipo') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('prices.dropship_percent') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('prices.partner_percent') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('prices.customer_percent') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.status.label') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_at') !!}<br/>{!! trans('system.updated_at') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.created_by') !!}</th>
                                <th style="text-align: center; vertical-align: middle;">{!! trans('system.action.label') !!}</th>
                            </tr>
                        </thead>
                        <tbody class="borderless">
                            <?php
                                $labels = ['success', 'danger', 'info', 'warning', 'default'];
                                $bg = ['red', 'olive', 'orange', 'maroon', 'purple', 'navy'];
                                $data = [];
                            ?>
                            @foreach ($prices as $item)
                                <?php
                                    $product = null;
                                    if ($item->type == \App\Define\Price::TYPE_PRODUCT) {
                                        $product = App\Product::find($item->product_id);
                                        if (is_null($product)) continue;
                                    }
                                ?>
                                <tr>
                                    <td style="text-align: center; width: 3%; vertical-align: middle;">{!! $i++ !!}</td>
                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                        <span class="btn btn-flat bg-{!! $item->type == \App\Define\Price::TYPE_CATEGORY ? 'orange' : 'olive' !!}">{!! trans('prices.types.' . $item->type) !!}</span>
                                    </td>
                                    <td style="vertical-align: middle; white-space: nowrap;">
                                        @if ($item->type == \App\Define\Price::TYPE_CATEGORY)
                                            <span class="label label-{!! $labels[$item->category_id%5] !!}">
                                                <a target="_blank" href="{!! route('admin.product-categories.show', $item->category_id) !!}" style="color: white;">
                                                    {!! $rawCategories[$item->category_id] ?? "" !!}

                                                </a>
                                            </span>
                                        @else
                                            <a target="_blank" href="{!! route('admin.products.show', $product->id) !!}">
                                                {!! $product->sku !!} - {!! $product->name !!}

                                            </a>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if ($item->type == \App\Define\Price::TYPE_PRODUCT)
                                            {!! App\Helper\HString::currencyFormat($product->price_ipo) !!}đ
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if ($item->type == \App\Define\Price::TYPE_CATEGORY)
                                            {!! App\Helper\HString::decimalFormat($item->dropship_percent) !!}%
                                        @else
                                            @if ($item->dropship_amount)
                                                {!! App\Helper\HString::currencyFormat($item->dropship_amount) !!}đ<br/>
                                                <small style="white-space: nowrap;">(CK {!! App\Helper\HString::currencyFormat($product->price_ipo-$item->dropship_amount) !!})</small>
                                            @endif
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if ($item->type == \App\Define\Price::TYPE_CATEGORY)
                                            {!! App\Helper\HString::decimalFormat($item->partner_percent) !!}%
                                        @else
                                            @if ($item->partner_amount)
                                                {!! App\Helper\HString::currencyFormat($item->partner_amount) !!}đ<br/>
                                                <small style="white-space: nowrap;">(CK {!! App\Helper\HString::currencyFormat($product->price_ipo-$item->partner_amount) !!})</small>
                                            @endif
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if ($item->type == \App\Define\Price::TYPE_CATEGORY)
                                            <span class="label label-success">{!! App\Helper\HString::decimalFormat($item->customer_percent_default) !!}%</span>
                                        @else
                                            @if ($item->customer_amount_default)
                                                <span class="label label-success">{!! App\Helper\HString::currencyFormat($item->customer_amount_default) !!}đ</span>
                                                <small style="white-space: nowrap;">(CK {!! App\Helper\HString::currencyFormat($product->price_ipo-$item->customer_amount_default) !!})</small>
                                            @endif
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if($item->status == 0)
                                        <span class="label label-danger"><span class='glyphicon glyphicon-remove'></span></span>
                                        @elseif($item->status == 1)
                                        <span class="label label-success"><span class='glyphicon glyphicon-ok'></span></span>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->created_at)) !!}</span><br/>
                                        <span class="label label-default">{!! date("d/m/Y H:i", strtotime($item->updated_at)) !!}</span>
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        @if ($item->source == App\Define\Price::SOURCE_ADMIN)
                                            <?php $adm = App\User::find($item->created_by); ?>
                                            <span class="label label-info">{!! trans('prices.sources.' . $item->source) !!}</span><br/>
                                            <a href="{!! is_null($adm) ? '#' : route('admin.users.show', $adm->id) !!}" target="_blank">
                                                <small>{!! is_null($adm) ? "" : $adm->fullname !!}</small> <i class="fas fa-external-link-alt"></i>
                                            </a>
                                        @else
                                            {!! trans('prices.sources.' . $item->source) !!}
                                            <?php $cus = App\Customer::find($item->created_by); ?>
                                            <span class="label label-danger">{!! trans('prices.sources.' . $item->source) !!}</span><br/>
                                            <a href="{!! is_null($cus) ? '#' : route('admin.customers.show', $cus->id) !!}" target="_blank">
                                                <small>{!! is_null($cus) ? "" : $cus->fullname !!}</small> <i class="fas fa-external-link-alt"></i>
                                            </a>
                                        @endif
                                    </td>

                                    <td style="text-align: center; vertical-align: middle; white-space: nowrap;">
                                        <a href="{!! route('admin.prices.edit', $item->id) !!}" class="btn btn-xs btn-default">
                                            <i class="text-warning glyphicon glyphicon-edit"></i>
                                        </a>
                                        &nbsp;&nbsp;
                                        @if ($item->type == \App\Define\Price::TYPE_CATEGORY)
                                        <a href="javascript:void(0)" link="{!! route('admin.prices.destroy', $item->id) !!}" class="btn-confirm-del btn btn-default btn-xs">
                                            <i class="text-danger glyphicon glyphicon-remove"></i>
                                        </a>
                                        @endif
                                        &nbsp;&nbsp;
                                        <a href="javascript:void(0)" class="btn btn-xs btn-default history" data-id="{!! $item->id !!}">
                                            <i class="text-info fas fa-history"></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="alert alert-info">{!! trans('system.no_record_found') !!}</div>
    @endif
</section>
@stop
@section('footer')
    <div class="modal fade" id="importExport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop1="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <div class="modal fade" id="timeline" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="text-center">Lịch sử thao tác</h5>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-info" data-dismiss="modal"> {!! trans('system.action.ok') !!} </a>
                </div>
            </div>
        </div>
    </div>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/moment.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/daterangepicker/daterangepicker.js') !!}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <script>
          $(document).ready(function(){
            $("#btnExport").click(function() {
                let table = document.getElementsByTagName("table");
                TableToExcel.convert(table[0], { 
                    name: `export.xlsx`, 
                    sheet: {
                        name: 'Sheet 1' 
                    }
                });
            });
        });
        !function ($) {
            $(function(){
                $(".select2").select2({'width': '100%'});
                $('.date_range').daterangepicker({
                    autoUpdateInput: false,
                    "locale": {
                        "format": "DD/MM/YYYY HH:mm",
                        "separator": " - ",
                        "applyLabel": "Áp dụng",
                        "cancelLabel": "Huỷ bỏ",
                        "fromLabel": "Từ ngày",
                        "toLabel": "Tới ngày",
                        "customRangeLabel": "Tuỳ chọn",
                        "weekLabel": "W",
                        "daysOfWeek": [
                            "CN",
                            "T2",
                            "T3",
                            "T4",
                            "T5",
                            "T6",
                            "T7"
                        ],
                        "monthNames": [
                            "Thg 1",
                            "Thg 2",
                            "Thg 3",
                            "Thg 4",
                            "Thg 5",
                            "Thg 6",
                            "Thg 7",
                            "Thg 8",
                            "Thg 9",
                            "Thg 10",
                            "Thg 11",
                            "Thg 12"
                        ],
                        "firstDay": 1
                    },
                    ranges: {
                       'Hôm nay': [moment(), moment()],
                       'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                       '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                       'Tháng này': [moment().startOf('month'), moment()],
                       'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    },
                    "alwaysShowCalendars": true,
                    maxDate: moment(),
                    minDate: moment().subtract(1, "years"),
                }, function(start, end, label) {
                    $('.date_range').val(start.format('DD/MM/YYYY HH:mm') + " - " + end.format('DD/MM/YYYY HH:mm'));
                });
                $(document).on('click', '.history', function(event) {
                    NProgress.start();
                    $.getJSON("{!! route('admin.prices.timeline') !!}?id=" + $(this).attr('data-id')).done(function (data) {
                        $("#timeline .modal-body").html(data.data);
                        $("#timeline").modal('show');
                    }).fail(function(jqxhr, textStatus, error) {
                        toastr.error("{!! trans('system.have_an_error') !!}", '', {positionClass: 'toast-bottom-left', progressBar: 'true'});
                    }).always(function() {
                        NProgress.done();
                    });
                });

                $("select[name='product']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    allowClear: true,
                    minimumResultsForSearch: 20,
                    ajax: {
                        url: "{{ route('admin.products.search') }}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            return {
                                tag: term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    }
                }).on('select2:unselecting', function(e) {
                    $(this).val('').trigger('change');
                    $(this).closest('form').submit();
                });
            });
        }(window.jQuery);
    </script>
@stop
