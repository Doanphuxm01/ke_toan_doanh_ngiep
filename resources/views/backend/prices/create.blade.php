@extends('backend.master')
@section('title')
    {!! trans('system.action.create') !!} - {!! trans('prices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('prices.label') !!}
            <small>{!! trans('system.action.create') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.prices.index') !!}">{!! trans('prices.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.prices.store')]) !!}
    <div class="box box-default">
        <div class="box-body">
            <table class="table">
                <tbody>
                    <?php $type = old('type'); ?>
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('prices.type') !!}
                        </th>
                        <td style="width: 35%;">
                            {!! Form::select('type', ['' => trans('system.dropdown_choice')] + \App\Define\Price::getTypesForOption(), $type, ['class' => 'form-control select2', 'required']) !!}
                        </td>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('prices.apply') !!}
                        </th>
                        <td>
                            {!! Form::select('product', $product ?? [], old('product'), ["class" => "form-control", 'placeholder' => 'Gõ để tìm kiếm ' .  trans('prices.category') . ' hoặc ' . trans('prices.product'), 'required']) !!}
                        </td>
                    </tr>
                    <tr>
                        <th class="table_right_middle">
                            <span class="input-group price_ipo" style="display: {!! $type == App\Define\Price::TYPE_PRODUCT ? 'table-row1' : 'none'; !!}; float: right;">
                                {!! trans('prices.price_ipo') !!}
                            </span>
                        </th>
                        <td>
                            <div class="input-group price_ipo" style="display: {!! $type == App\Define\Price::TYPE_PRODUCT ? 'table-row1' : 'none'; !!};">
                                {!! Form::text('price_ipo', old('price_ipo'), ["class" => "form-control"]) !!}
                                <div class="input-group-addon price-addon">VNĐ</div>
                            </div>
                        </td>
                        <th class="table_right_middle">
                            {!! trans('prices.dropship_percent') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('dropship_percent', old('dropship_percent'), ["class" => "form-control decimal"]) !!}
                                <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('prices.partner_percent') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('partner_percent', old('partner_percent'), ["class" => "form-control decimal"]) !!}
                                <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                            </div>
                        </td>
                        <th class="table_right_middle">
                            {!! trans('prices.customer_percent') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('customer_percent', old('customer_percent'), ["class" => "form-control decimal"]) !!}
                                <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="4">
                            <label>
                                {!! Form::checkbox('status', 1, old('status', 1), [ 'class' => 'minimal' ]) !!}
                                {!! trans('system.status.active') !!}
                            </label>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-center">
                            <a class="btn btn-default btn-flat" href="{!! route('admin.prices.index') !!}">{!! trans('system.action.cancel') !!}</a>
                            {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $(".select2").select2({ width: '100%' });
                @if($type == App\Define\Price::TYPE_PRODUCT)
                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                @else
                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 2, 'min': '0', 'max': '100.00', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                @endif
                $("input[name='price_ipo']").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                $("select[name='type']").change(function(event) {
                    $("input[name='partner_percent']").val("");
                    $("input[name='dropship_percent']").val("");
                    $("input[name='customer_percent']").val("");
                    if ($("select[name='type']").val() == '{!! App\Define\Price::TYPE_PRODUCT !!}') {
                        $(".price-addon").html("VNĐ");
                        $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                    } else {
                        $(".price-addon").html("%");
                        $(".decimal").inputmask({'alias': 'decimal', 'digits': 2, 'min': '0', 'max': '100.00', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                    }
                    $("select[name='product']").val("").trigger('change');
                });
                var dataPrices = [];
                $("select[name='product']").select2({
                    multiple: false,
                    minimumInputLength: 2,
                    minimumResultsForSearch: 10,
                    width: '100%',
                    templateResult: formatState,
                    ajax: {
                        url: "{{ route('admin.products.search-for-price-by-type') }}",
                        dataType: 'json',
                        type: "POST",
                        headers: {'X-CSRF-Token': "{!! csrf_token() !!}"},
                        data: function (term, page) {
                            var type = $("select[name='type']").val();
                            if (!type) {
                                toastr.error("Bạn cần chọn mục Áp dụng");
                                this._request.abort();
                                return false;
                            }
                            return {
                                tag: term,
                                type: type,
                            };
                        },
                        processResults: function (data) {
                            dataPrices = [];
                            return {
                                results: $.map($.parseJSON(data.message), function (item) {
                                    if (typeof(item.children) == 'undefined') {
                                        dataPrices[item.id] = item.price_ipo;
                                        return {
                                            text: item.name,
                                            id: item.id,
                                            image: item.image,
                                        }
                                    } else {
                                        return {
                                            text: item.text,
                                            children: $.map(item.children, function(child){
                                                dataPrices[child.id] = child.price_ipo;
                                                return {
                                                    id: child.id,
                                                    text: child.name,
                                                    image: child.image,
                                                }
                                            })
                                        }
                                    }
                                })
                            };
                        }
                    }
                });

                $("select[name='product']").change(function(event) {
                    if ($("select[name='type']").val() == '{!! App\Define\Price::TYPE_PRODUCT !!}') {
                        $("input[name='price_ipo']").val(dataPrices[$(this).val()]);
                        $(".price_ipo").css('display', 'table-row');
                    } else {
                        $("input[name='price_ipo']").val("");
                        $(".price_ipo").css('display', 'none');
                    }
                });
            });
        }(window.jQuery);
        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img height="30px" src="' + state.image + '" class="img-flag" /> ' + state.text + '</span>'
            );
            return $state;
        };
    </script>
@stop
