<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
    <table>
        <tr>
            <th style="text-align: center;">STT</th>
            <th style="text-align: center;" width="10">MaGiay</th>
            <th style="text-align: center;" width="10">SoLot</th>
            @foreach($categories as $code)
                <th style="text-align: center;" width="10">{!! $code !!}_roll</th>
                <th style="text-align: center;" width="10">{!! $code !!}_md</th>
                <th width="30">{!! $code !!}_note</th>
            @endforeach
        </tr>
        <tr>
            <td colspan="{!! 2 + count($categories) * 3 !!}" height="180" style="color: #ff0000;">
                Ghi chú:<br/>
                <span style="color: #c2c2c2; font-weight: bold;">- !!! KHÔNG ĐƯỢC THAY ĐỔI HÀNG ĐẦU TIÊN ĐI !!!</span><br/>
                - Nếu chỉ nhập cho 01, 02, 03... kho, bỏ các cột của kho khác đi<br/>
                - Dữ liệu chuẩn theo định dạng sau:<br/>
                    + roll (Số cuộn): Yêu cầu, số nguyên dương từ 1-10000<br/>
                    + md (Mét dài lẻ ra): Yêu cầu, Số thực dương, chấp nhận 2 số phần lẻ vd: 123.45<br/>
                    + note (Ghi chú): KHÔNG yêu cầu, dài không quá 255 kí tự<br/>
                - Bỏ trống ô, hệ thống coi như không thay đổi<br/>
                Ví dụ:<br/>
            </td>
        </tr>
        <tr>
            <th style="text-align: center;">1</th>
            <th style="text-align: center;">TWP1234</th>
            <th style="text-align: center;">AA123456</th>
            @foreach($categories as $code)
                <th style="text-align: center;">3</th>
                <th style="text-align: center;">2.05</th>
                <th>Ghi chú 1</th>
            @endforeach
        </tr>
        <tr>
            <th style="text-align: center;">2</th>
            <th style="text-align: center;">TWP1234</th>
            <th style="text-align: center;">XX987654</th>
            @foreach($categories as $code)
                <th style="text-align: center;">1</th>
                <th style="text-align: center;">2.82</th>
                <th>Ghi chú 2</th>
            @endforeach
        </tr>
        <tr>
            <th style="text-align: center;">3</th>
            <th style="text-align: center;">TWP9876</th>
            <th style="text-align: center;">CC23456</th>
            @foreach($categories as $code)
                <th style="text-align: center;">2</th>
                <th style="text-align: center;">10.9</th>
                <th>Ghi chú 3</th>
            @endforeach
        </tr>
    </table>
</body>
</html>
