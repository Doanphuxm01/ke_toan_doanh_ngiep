@extends('backend.master')
@section('title')
    {!! trans('system.action.edit') !!} - {!! trans('prices.label') !!}
@stop
@section('head')
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/iCheck/all.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/backend/plugins/select2/select2.min.css') !!}" />
@stop
@section('content')
    <section class="content-header">
        <h1>
            {!! trans('prices.label') !!}
            <small>{!! trans('system.action.edit') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.home') !!}">{!! trans('system.home') !!}</a></li>
            <li><a href="{!! route('admin.prices.index') !!}">{!! trans('prices.label') !!}</a></li>
        </ol>
    </section>
    @if($errors->count())
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> {!! trans('messages.error') !!}</h4>
            <ul>
                @foreach($errors->all() as $message)
                <li>{!! $message !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => route('admin.prices.update', $price->id), 'method' => 'PUT']) !!}
    <div class="box box-default">
        <div class="box-body">
            <table class="table">
                <tbody>
                    <?php $type = $price->type; ?>
                    <tr>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('prices.type') !!}
                        </th>
                        <td style="width: 35%;">
                            {!! Form::select('type', \App\Define\Price::getTypesForOption(), $type, ['class' => 'form-control select2', 'disabled']) !!}
                        </td>
                        <th class="table_right_middle" style="width: 15%;">
                            {!! trans('prices.apply') !!}
                        </th>
                        <td>
                            {!! Form::select('product', $product, old('product'), ["class" => "form-control", 'disabled']) !!}
                        </td>
                    </tr>
                    @if ($price->type == App\Define\Price::TYPE_PRODUCT)
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('prices.price_ipo') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('price_ipo', old('price_ipo', $priceIPO), ["class" => "form-control"]) !!}
                                <div class="input-group-addon price-addon">VNĐ</div>
                            </div>
                        </td>
                        <th class="table_right_middle">
                            {!! trans('prices.dropship_percent') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('dropship_percent', old('dropship_percent', ($type == App\Define\Price::TYPE_PRODUCT ? $price->dropship_amount : $price->dropship_percent)), ["class" => "form-control decimal"]) !!}
                                <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                            </div>
                        </td>
                    </tr>
                    @else
                        <tr>
                            <th></th>
                            <th></th>
                            <th class="table_right_middle">
                                {!! trans('prices.dropship_percent') !!}
                            </th>
                            <td>
                                <div class="input-group">
                                    {!! Form::text('dropship_percent', old('dropship_percent', ($type == App\Define\Price::TYPE_PRODUCT ? $price->dropship_amount : $price->dropship_percent)), ["class" => "form-control decimal"]) !!}
                                    <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                                </div>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th class="table_right_middle">
                            {!! trans('prices.partner_percent') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('partner_percent', old('partner_percent', ($type == App\Define\Price::TYPE_PRODUCT ? $price->partner_amount : $price->partner_percent)), ["class" => "form-control decimal"]) !!}
                                <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                            </div>
                        </td>
                        <th class="table_right_middle">
                            {!! trans('prices.customer_percent') !!}
                        </th>
                        <td>
                            <div class="input-group">
                                {!! Form::text('customer_percent', old('customer_percent', ($type == App\Define\Price::TYPE_PRODUCT ? $price->customer_amount_default : $price->customer_percent_default)), ["class" => "form-control decimal"]) !!}
                                <div class="input-group-addon price-addon">{!! $type == App\Define\Price::TYPE_PRODUCT ? 'VNĐ'  : '%' !!}</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="4">
                            <label>
                                {!! Form::checkbox('status', 1, old('status', $price->status), [ 'class' => 'minimal' ]) !!}
                                {!! trans('system.status.active') !!}
                            </label>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-center">
                            <a class="btn btn-default btn-flat" href="{!! route('admin.prices.index') !!}">{!! trans('system.action.cancel') !!}</a>
                            {!! Form::submit(trans('system.action.save'), array('class' => 'btn btn-primary btn-flat')) !!}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('footer')
    <script src="{!! asset('assets/backend/plugins/moment/min/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/input-mask/jquery.inputmask.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/select2/select2.full.min.js') !!}"></script>
    <script src="{!! asset('assets/backend/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        !function ($) {
            $(function() {
                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue'
                });
                $(".select2").select2({ width: '100%' });
                @if($type == App\Define\Price::TYPE_PRODUCT)
                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
                @else
                    $(".decimal").inputmask({'alias': 'decimal', 'digits': 2, 'min': '0', 'max': '100.00', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false});
                @endif
                $("input[name='price_ipo']").inputmask({'alias': 'decimal', 'digits': 0, 'min': '0', 'max': '99999999', 'groupSeparator': ',', 'autoGroup': true, 'allowMinus': false, 'removeMaskOnSubmit': true});
            });
        }(window.jQuery);
        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img height="30px" src="' + state.image + '" class="img-flag" /> ' + state.text + '</span>'
            );
            return $state;
        };
    </script>
@stop
