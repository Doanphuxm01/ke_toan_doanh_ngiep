@for ($i = 1; $i < count($data); $i++)
    <?php if (!isset($data[$i][1]) || trim($data[$i][1]) == "") continue; ?>
    <tr>
        <td style="text-align: center; vertical-align: middle;">
            {!! $data[$i][0] ?? '' !!}
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="text_require">{!! $data[$i][1] !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="integer">{!! $data[$i][2] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="integer_nullable">{!! $data[$i][3] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="integer_nullable">{!! $data[$i][4] ?? '' !!}</span>
        </td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="integer_nullable">{!! $data[$i][5] ?? '' !!}</span>
        </td>
        <td style="vertical-align: middle; white-space: nowrap;">
            <span class="text">{!! trim($data[$i][6] ?? '') !!}</span>
        </td>
    </tr>
@endfor
<script>
    $(function(){
        $.fn.editable.defaults.mode = 'inline';
        $('.text').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                var value = $.trim(value);
                if (value.length > 255) return "Chuỗi không được vượt quá 255 kí tự";
            }
        });
        $('.text_require').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                var value = $.trim(value);
                if(value == '') return "{!! trans('system.data_not_empty') !!}";
                if (value.length > 100) return "Chuỗi không được vượt quá 100 kí tự";
            }
        });
        $('.integer').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                if($.trim(value) == '') return "{!! trans('system.data_not_empty') !!}";
                if (isNaN(value))  return 'Dữ liệu phải là một số nguyên dương';
                if (value%1 != 0)  return 'Dữ liệu phải là một số nguyên dương';
                if (value%100 != 0)  return 'Dữ liệu phải có đơn vị là trăm';
                if(value < 1 || value > 99999999) return 'Dữ liệu nằm trong khoảng từ 1-99,999,999';
            }
        });
        $('.integer_nullable').editable({emptytext: '-', unsavedclass: null, validate: function(value) {
                if($.trim(value) != '') {
                    if (isNaN(value))  return 'Dữ liệu phải là một số nguyên dương';
                    if (value%1 != 0)  return 'Dữ liệu phải là một số nguyên dương';
                    if (value%100 != 0)  return 'Dữ liệu phải có đơn vị là trăm';
                    if(value < 1 || value > 99999999) return 'Dữ liệu nằm trong khoảng từ 1-99,999,999';
                }
            }
        });
    });
</script>
