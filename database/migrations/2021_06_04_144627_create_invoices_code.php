<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices_code', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id');
            $table->string('voucher_date');
            $table->string('voucher_no');
            $table->string('description');
            $table->string('currency');
            $table->string('due_payment');
            $table->float('currency_rate');
            $table->integer('company_id');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('status');
            $table->string('invoice_no')->nullable();
            $table->string('invoice_date')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices_code');
    }
}
