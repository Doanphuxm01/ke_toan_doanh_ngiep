<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc')->nullable();
            $table->string('voucher_no');
            $table->date('voucher_date');
            $table->date('payment_term');
			$table->decimal('exchange_rate', 13, 2);
			$table->smallInteger('currency');
			$table->unsignedInteger('company_id')->nullable();
			$table->smallInteger('status')->default(1);
			$table->unsignedInteger('created_by')->nullable();
			$table->unsignedInteger('updated_by')->nullable();
			$table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_invoices');
    }
}
