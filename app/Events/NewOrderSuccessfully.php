<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

// now de gui async
class NewOrderSuccessfully implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $productId;
    public $productName;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        $message = "";
        $provinces = \App\Library\Giaohangtietkiem::getActiveProvince();
        $provinces = $provinces['message'];
        $prefixs = ['091', '092', '093', '094', '096', '097', '098', '099', '090', '070', '079', '077', '076', '078',  '032', '033', '034', '035', '036', '037', '038', '039', '083', '084', '085', '081', '082', '056', '058', '059'];
        // get all product name
        $products = \App\Product::getRandomProducts();
        $freq = [1, 1, 1, 1, 1, 2, 3, 2, 3, 1, 2, 3, 3, 1, 2, 1, 2, 1, 1, 1, 1, 2, 2, 3, 3, 2, 1, 1, 1];
        // random gender
        $gender = rand(0, 1);
        $faker = \Faker\Factory::create('vi_VN');
        // fullname
        $fullname = ($gender ? ('Anh ' . $faker->firstNameMale) : ('Chị ' . $faker->firstNameFemale));
        $message .= $fullname  . " (" . $prefixs[array_rand($prefixs)] . '***' . str_pad(rand(0, 999), 3, '0', STR_PAD_LEFT) . ') ở ' .  $provinces[array_rand($provinces)] . ' vừa đặt mua ' . $freq[array_rand($freq)] . ' sản phẩm ';
        $productId      = array_rand($products);
        $productName    = $products[$productId];
        $message       .= $productName;

        $this->message      = $message;
        $this->productId    = $productId;
        $this->productName  = $productName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['notification'];
    }

    public function broadcastAs()
    {
        return 'order.new';
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['link' => route('home.product', ['id' => $this->productId, 'slug' => str_slug($this->productName)]), 'message' => $this->message];
    }
}
