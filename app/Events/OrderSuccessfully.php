<?php

namespace App\Events;

use App\Order;

class OrderSuccessfully
{
    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
