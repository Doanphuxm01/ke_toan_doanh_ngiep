<?php

namespace App\Events;

use App\Order;

class OrderSendMail
{
    public $order;
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
