<?php

namespace App\Define;

class Media
{
    const TYPE_PRODUCT = "PRODUCT";
    const TYPE_COIN    = "COIN";
}
