<?php

namespace App\Defines;

class Account {

    const PROPERTY_0 = "DEBT";
    const PROPERTY_1 = "CREDIT";
    const PROPERTY_2 = "OTHER";
    const PROPERTY_3 = "NO";

    public static function getProperties()
    {
        return [
            self::PROPERTY_0,
            self::PROPERTY_1,
            self::PROPERTY_2,
            self::PROPERTY_3,
        ];
    }

    public static function getPropertiesForOption()
    {
        return [
            self::PROPERTY_0 => trans('accounts.properties.'.self::PROPERTY_0),
            self::PROPERTY_1 => trans('accounts.properties.'.self::PROPERTY_1),
            self::PROPERTY_2 => trans('accounts.properties.'.self::PROPERTY_2),
            self::PROPERTY_3 => trans('accounts.properties.'.self::PROPERTY_3)
        ];
    }
}

