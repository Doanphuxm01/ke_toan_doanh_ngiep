<?php

namespace App\Define;

class CustomerTransaction
{
    const SOURCE_ADMIN      = "ADMIN";
    const SOURCE_PARTNER    = "PARTNER";
    const SOURCE_SYSTEM     = "SYSTEM";

    const COIN_TYPE_REFILL     = "REFILL";
    const COIN_TYPE_TRANSACTION= "TRANSACTION";
    const COIN_TYPE_COMMISSION = "COMMISSION";

    public static function getSources() {
        return [self::SOURCE_ADMIN, self::SOURCE_PARTNER, self::SOURCE_SYSTEM];
    }

    public static function getSourcesForOption() {
        return [self::SOURCE_ADMIN => trans('customer_transactions.sources.' . self::SOURCE_ADMIN), self::SOURCE_PARTNER => trans('customer_transactions.sources.' . self::SOURCE_PARTNER), self::SOURCE_SYSTEM => trans('customer_transactions.sources.' . self::SOURCE_SYSTEM)];
    }

    public static function getTypes() {
        return [self::COIN_TYPE_REFILL, self::COIN_TYPE_TRANSACTION, self::COIN_TYPE_COMMISSION];
    }

    public static function getTypesForOption() {
        return [self::COIN_TYPE_REFILL => trans('customer_transactions.types.' . self::COIN_TYPE_REFILL), self::COIN_TYPE_TRANSACTION => trans('customer_transactions.types.' . self::COIN_TYPE_TRANSACTION), self::COIN_TYPE_COMMISSION => trans('customer_transactions.types.' . self::COIN_TYPE_COMMISSION)];
    }

    public static function getColorTypes($type) {
        switch ($type) {
            case self::COIN_TYPE_REFILL:
                return "<span class='btn bg-teal btn-flat margin'>" . trans('customer_transactions.types.' . self::COIN_TYPE_REFILL) . "</span>";
            case self::COIN_TYPE_TRANSACTION:
                return "<span class='btn bg-purple btn-flat margin'>" . trans('customer_transactions.types.' . self::COIN_TYPE_TRANSACTION) . "</span>";
            case self::COIN_TYPE_COMMISSION:
                return "<span class='btn bg-navy btn-flat margin'>" . trans('customer_transactions.types.' . self::COIN_TYPE_COMMISSION) . "</span>";
        }
    }
}
