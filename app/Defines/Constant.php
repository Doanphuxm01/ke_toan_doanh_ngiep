<?php

namespace App\Define;

class Constant
{

    const PAGE_NUM_FRONTEND = 12;
    const PAGE_NUM_MOBILE   = 6;

	const PAGE_NUM           	= 10;
    const PAGE_NUM_20           = 20;
    const PAGE_NUM_50           = 50;
    const PAGE_NUM_500          = 500;
    const IMAGE_SUPPLIER_WIDTH  = 300;
    const IMAGE_SUPPLIER_HEIGHT = 300;

    const IMAGE_PAPER_WIDTH     = 200;
    const IMAGE_PAPER_HEIGHT    = 200;
    const IMAGE_PAPER_RATIO     = 1;

    const GROUP_PAYMENT_METHOD      = 3;
    const MAX_BUY_QUANTITY_PRODUCT  = 99;

    const STATIC_PAGE_SIMPLE    = "SIMPLE";
    const STATIC_PAGE_COMPLEX   = "COMPLEX";
    const STATIC_PAGE_HTML      = "HTML";
    const STATIC_PAGE_IMAGE     = "IMAGE";

    const IMAGE_NEWS_WIDTH  = 1240;
    const IMAGE_NEWS_HEIGHT = 300;
    const IMAGE_NEWS_RATIO  = 3;

    const IMAGE_SLIDER_WIDTH    = 866;
    const IMAGE_SLIDER_HEIGHT   = 351;

    const GROUP_MORE = 1;

    const IMAGE_BRAND_WIDTH     = 250;
    const IMAGE_BRAND_HEIGHT    = 250;

    const IMAGE_BRAND_IMG1_WIDTH     = 1237;
    const IMAGE_BRAND_IMG1_HEIGHT    = 303;
    const IMAGE_BRAND_IMG2_WIDTH     = 1237;
    const IMAGE_BRAND_IMG2_HEIGHT    = 165;
    const IMAGE_BRAND_IMG3_WIDTH     = 1237;
    const IMAGE_BRAND_IMG3_HEIGHT    = 303;
    const IMAGE_BRAND_RATIO = 1.2;

    const IMAGE_TYPE_BANNER_TOP         = "IMAGE_TYPE_BANNER_TOP";
    const IMAGE_TYPE_BANNER_MIDDLE      = "IMAGE_TYPE_BANNER_MIDDLE";
    const IMAGE_TYPE_BANNER_BOTTOM      = "IMAGE_TYPE_BANNER_BOTTOM";
    const IMAGE_TYPE_BANNER_RIGHT_SLIDE = "IMAGE_TYPE_BANNER_RIGHT_SLIDE";
    const IMAGE_TYPE_BANNER_EMPTY_CART  = "IMAGE_TYPE_BANNER_EMPTY_CART";

    const IMAGE_CATEGORY_MENU_ICON_WIDTH   = 81;
    const IMAGE_CATEGORY_MENU_ICON_HEIGHT  = 81;

    const IMAGE_CATEGORY_IMG_CAT_WIDTH  = 1240;
    const IMAGE_CATEGORY_IMG_CAT_HEIGHT = 350;

    const IMAGE_CATEGORY_IMG_VIEW_WIDTH  = 120;
    const IMAGE_CATEGORY_IMG_VIEW_HEIGHT = 120;

    const IMAGE_PROMOTION_HEIGHT    = 350;
    const IMAGE_PROMOTION_WIDTH     = 1240;
    const IMAGE_FLASH_SALE_HEIGHT   = 350;
    const IMAGE_FLASH_SALE_WIDTH    = 1240;


    const IMAGE_TYPE_BANNER_TOP_HEIGHT  = 50;
    const IMAGE_TYPE_BANNER_TOP_WIDTH   = 1920;

    const IMAGE_TYPE_BANNER_MIDDLE_HEIGHT   = 160;
    const IMAGE_TYPE_BANNER_MIDDLE_WIDTH    = 1240;

    const IMAGE_TYPE_BANNER_BOTTOM_HEIGHT   = 134;
    const IMAGE_TYPE_BANNER_BOTTOM_WIDTH    = 1240;

    const IMAGE_TYPE_BANNER_RIGHT_SLIDE_HEIGHT  = 170;
    const IMAGE_TYPE_BANNER_RIGHT_SLIDE_WIDTH   = 358;

    const IMAGE_TYPE_BANNER_EMPTY_CART_HEIGHT  = 250;
    const IMAGE_TYPE_BANNER_EMPTY_CART_WIDTH   = 600;

    public static function getBannerTypes() {
        return [self::IMAGE_TYPE_BANNER_TOP, self::IMAGE_TYPE_BANNER_RIGHT_SLIDE, self::IMAGE_TYPE_BANNER_MIDDLE, self::IMAGE_TYPE_BANNER_BOTTOM, self::IMAGE_TYPE_BANNER_EMPTY_CART];
    }

    public static function getBannerTypesForOption() {
        return [
            self::IMAGE_TYPE_BANNER_TOP => trans('banner_advertisements.types.' . self::IMAGE_TYPE_BANNER_TOP),
            self::IMAGE_TYPE_BANNER_RIGHT_SLIDE => trans('banner_advertisements.types.' . self::IMAGE_TYPE_BANNER_RIGHT_SLIDE),
            self::IMAGE_TYPE_BANNER_MIDDLE => trans('banner_advertisements.types.' . self::IMAGE_TYPE_BANNER_MIDDLE),
            self::IMAGE_TYPE_BANNER_BOTTOM => trans('banner_advertisements.types.' . self::IMAGE_TYPE_BANNER_BOTTOM),
            self::IMAGE_TYPE_BANNER_EMPTY_CART => trans('banner_advertisements.types.' . self::IMAGE_TYPE_BANNER_EMPTY_CART),
        ];
    }
}
