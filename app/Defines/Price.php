<?php

namespace App\Define;

class Price
{
    const TYPE_CATEGORY = 'CATEGORY';
    const TYPE_PRODUCT 	= 'PRODUCT';

    const SOURCE_ADMIN      = "ADMIN";
    const SOURCE_PARTNER    = "PARTNER";

    public static function getTypes() {
        return [self::TYPE_CATEGORY, self::TYPE_PRODUCT];
    }

    public static function getTypesForOption() {
        return [self::TYPE_CATEGORY => trans('prices.types.' . self::TYPE_CATEGORY), self::TYPE_PRODUCT => trans('prices.types.' . self::TYPE_PRODUCT)];
    }

    public static function getSources() {
        return [self::SOURCE_ADMIN, self::SOURCE_PARTNER];
    }

    public static function getSourcesForOption() {
        return [self::SOURCE_ADMIN => trans('prices.sources.' . self::SOURCE_ADMIN), self::SOURCE_PARTNER => trans('prices.sources.' . self::SOURCE_PARTNER)];
    }
}
