<?php

namespace App\Defines;

class PaymentReceiptVoucher
{
  
    const TYPE_0 = 0;
    const TYPE_1 = 1;
    const OBJECT_TYPE_1 = 1;
    const OBJECT_TYPE_2 = 2;
    const OBJECT_TYPE_3 = 3;
    const OBJECT_TYPE_4 = 4;

    public static function getColorTypeVouchers($type) {
        switch ($type) {
            case self::TYPE_0:
                return "<span class='btn bg-teal btn-flat margin'>" . trans('payment_receipt_vouchers.type.' .self::TYPE_0) . "</span>";
            case self::TYPE_1:
                return "<span class='btn bg-purple btn-flat margin'>" . trans('payment_receipt_vouchers.type.'.self::TYPE_1) . "</span>";
        }
    }

    public static function getTypeVouchers($type) {
        switch ($type) {
            case self::TYPE_0:
                return trans('payment_receipt_vouchers.type.' .self::TYPE_0);
            case self::TYPE_1:
                return trans('payment_receipt_vouchers.type.'.self::TYPE_1);
        }
    }

    public static function getTypeBankVouchers($type) {
        switch ($type) {
            case self::TYPE_0:
                return trans('payment_receipt_vouchers.type_bank.' .self::TYPE_0);
            case self::TYPE_1:
                return trans('payment_receipt_vouchers.type_bank.'.self::TYPE_1);
        }
    }

    public static function getValueObjectType($object_type) {
        switch ($object_type) {
            case self::OBJECT_TYPE_1:
                return trans('payment_receipt_vouchers.object_type.' .self::OBJECT_TYPE_1);
            case self::OBJECT_TYPE_2:
                return trans('payment_receipt_vouchers.object_type.' .self::OBJECT_TYPE_2);
            case self::OBJECT_TYPE_3:
                return trans('payment_receipt_vouchers.object_type.' .self::OBJECT_TYPE_3);
            case self::OBJECT_TYPE_4:
                return trans('payment_receipt_vouchers.object_type.' .self::OBJECT_TYPE_4);
        }
    }

    public static function getTypeValueForOption() {
        return [
            self::TYPE_0  => trans('payment_receipt_vouchers.type.' .self::TYPE_0),
            self::TYPE_1  => trans('payment_receipt_vouchers.type.' .self::TYPE_1),
        ];
    }

    public static function getObjectTypeValueForOption() {
        return [
            self::OBJECT_TYPE_1  => trans('payment_receipt_vouchers.object_type' .self::OBJECT_TYPE_1),
            self::OBJECT_TYPE_2  => trans('payment_receipt_vouchers.object_type' .self::OBJECT_TYPE_2),
            self::OBJECT_TYPE_3  => trans('payment_receipt_vouchers.object_type' .self::OBJECT_TYPE_3),
            self::OBJECT_TYPE_4  => trans('payment_receipt_vouchers.object_type' .self::OBJECT_TYPE_4),
        ];
    }
}
