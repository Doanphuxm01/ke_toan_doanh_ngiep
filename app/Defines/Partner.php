<?php 
namespace App\Defines;

class Partner {
    
    const TYPE_0 = "IS_CONTRA";
    const TYPE_1 = "NOT_CONTRA";

    
    public static function getGroupForOption() {
        return [self::TYPE_0 => trans('partners.contra'.self::TYPE_0),
                self::TYPE_1 => trans('partners.contra.'.self::TYPE_1),
        ];
    }
}