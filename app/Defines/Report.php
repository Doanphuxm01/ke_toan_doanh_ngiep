<?php

namespace App\Define;

class Report
{
    const PRODUCT_ALL       = 'PRODUCT_ALL';
    const PRODUCT_VIEW      = 'PRODUCT_VIEW';
    const PRODUCT_PROMOTION = 'PRODUCT_PROMOTION';
    const PRODUCT_INVENTORY = 'PRODUCT_INVENTORY';
    const PRODUCT_CLEVERAD  = 'PRODUCT_CLEVERAD';
    const ORDER_REVENUE     = 'ORDER_REVENUE';
    const ORDER_PROFIT      = 'ORDER_PROFIT';
    const PRODUCT_PROFIT    = 'PRODUCT_PROFIT';

    const ORDER_ALL         = 'ORDER_ALL';
    const ORDER_CPN         = 'ORDER_CPN';
    const ORDER_BANK_NL     = 'ORDER_BANK_NL';

    const PRODUCT_BEST_SELLER   = 'PRODUCT_BEST_SELLER';
    const PRODUCT_SUPPLIER_AP   = 'PRODUCT_SUPPLIER_AP';
    const OPERATOR_GREATER_EQUAL= ">=";
    const OPERATOR_EQUAL        = "=";
    const OPERATOR_GREATER      = ">";
    const OPERATOR_LITTER_EQUAL = "<=";
    const OPERATOR_LITTER       = "<";

    public static function getOperators()
    {
        return [self::OPERATOR_GREATER_EQUAL => self::OPERATOR_GREATER_EQUAL, self::OPERATOR_EQUAL => self::OPERATOR_EQUAL, self::OPERATOR_GREATER => self::OPERATOR_GREATER, self::OPERATOR_LITTER_EQUAL => self::OPERATOR_LITTER_EQUAL, self::OPERATOR_LITTER => self::OPERATOR_LITTER];
    }

    public static function getAllReportsForOption()
    {//self::PRODUCT_ALL => trans('reports.types.' . self::PRODUCT_ALL),
        return [self::ORDER_ALL => trans('reports.types.' . self::ORDER_ALL), self::ORDER_CPN => trans('reports.types.' . self::ORDER_CPN), self::ORDER_BANK_NL => trans('reports.types.' . self::ORDER_BANK_NL)];
    }
}