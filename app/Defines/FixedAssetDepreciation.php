<?php

namespace App\Defines;

class FixedAssetDepreciation {

    const TYPE_0 = "DEPARTMENTS";
    const TYPE_1 = "USERS";
    const TYPE_2 = "PARTNERS";

    const STATUS_0 = "DRAFT";
    const STATUS_1 = "SAVE";
    const STATUS_2 = "APPROVED";

    public static function getType()
    {
        return [
            self::TYPE_0,
            self::TYPE_1,
            self::TYPE_2,
        ];
    }

    public static function getTypeForOption()
    {
        return [
            self::TYPE_0 => trans('fixed_asset_depreciation.type.'.self::TYPE_0),
            self::TYPE_1 => trans('fixed_asset_depreciation.type.'.self::TYPE_1),
            self::TYPE_2 => trans('fixed_asset_depreciation.type.'.self::TYPE_2),
        ];
    }
    public static function getStatus()
    {
        return [
            self::STATUS_0,
            self::STATUS_1,
            self::STATUS_2,
        ];
    }

    public static function getStatusForOption()
    {
        return [
            self::STATUS_0 => trans('fixed_asset_depreciation.status.'.self::STATUS_0),
            self::STATUS_1 => trans('fixed_asset_depreciation.status.'.self::STATUS_1),
            self::STATUS_2 => trans('fixed_asset_depreciation.status.'.self::STATUS_2),
        ];
    }
}

