<?php

namespace App\Define;

class Workflow
{
    const TYPE_AIR  = 'TYPE_AIR';
    const TYPE_SEA  = 'TYPE_SEA';
    const TYPE_DOM  = 'TYPE_DOM';
    const TYPE_OTHER  = 'TYPE_OTHER';
    public static function getTypeAit(){
        return self::TYPE_AIR;
    }
    public function getTYPEOTHER(){
        return self::TYPE_OTHER;
    }
    public static function getAllReportsForOption()
    {
        return [
             self::TYPE_AIR   => trans('workflows.types.' . self::TYPE_AIR),
             self::TYPE_SEA   => trans('workflows.types.' . self::TYPE_SEA),
             self::TYPE_DOM   => trans('workflows.types.' . self::TYPE_DOM),
             self::TYPE_OTHER => trans('workflows.types.' . self::TYPE_OTHER)
        ];
    }
}