<?php

namespace App\Defines;

class ServicePurchase
{
    const TAX_VALUE_0  = 0;
    const TAX_VALUE_10 = 10;

    const STATUS_DRAFT      = 'DRAFT';
    const STATUS_REJECTED   = 'REJECTED';
    const STATUS_APPROVED   = 'APPROVED';
    const STATUS_SAVE       = 'SAVE';

    public static function getAllStatus()
    {
        return [self::STATUS_DRAFT, self::STATUS_REJECTED, self::STATUS_APPROVED, self::STATUS_SAVAE];
    }

    public static function getAllStatusForOptions()
    {
        return [self::STATUS_DRAFT => trans("service_purchases.status.". self::STATUS_DRAFT), self::STATUS_REJECTED => trans("service_purchases.status.". self::STATUS_REJECTED), self::STATUS_APPROVED => trans("service_purchases.status.". self::STATUS_APPROVED), self::STATUS_SAVE => trans("service_purchases.status.". self::STATUS_SAVE)];
    }

    public static function getColorForState($state)
    {
        switch ($state) {
            case self::STATUS_DRAFT:
                return  "<span class='badge bg-gray'>".trans("service_purchases.status.". self::STATUS_DRAFT)."</span>";
            case self::STATUS_REJECTED:
                return  "<span class='badge bg-maroon'>".trans("service_purchases.status.". self::STATUS_REJECTED)."</span>";
            case self::STATUS_APPROVED:
                return "<span class='badge bg-purple'>".trans("service_purchases.status.". self::STATUS_APPROVED)."</span>";
            case self::STATUS_SAVE:
                return "<span class='badge bg-olive'>".trans("service_purchases.status.". self::STATUS_SAVE)."</span>";

        }
    }
    public static function getTaxValues() {
        return [self::TAX_VALUE_0, self::TAX_VALUE_10];
    }

    public static function getTaxValuesForOption() {
        return [
            self::TAX_VALUE_0   => trans('sale_cost_items.tax_values.' . self::TAX_VALUE_0),
            self::TAX_VALUE_10  => trans('sale_cost_items.tax_values.' . self::TAX_VALUE_10),
        ];
    }
}
