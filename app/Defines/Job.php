<?php 
namespace App\Defines;

class Job {
    
    const TYPE_0 = "AE";
    const TYPE_1 = "AI";
    const TYPE_2 = "MS";
    const TYPE_3 = "SE";
    const TYPE_4 = "SI";
    const TYPE_5 = "TR";
    const TYPE_6 = "WH";

    const GROUP_0 = 'NORMAL';
    const GROUP_1 = 'HOUSE';

    public static function getGroup($jobGroup) {
        switch($jobGroup) {
            case self::GROUP_0:
                return trans('jobs.job_group'.GROUP_0);
            case self::GROUP_1:
                return trans('jobs.job_group'.GROUP_1);
        }
    }
    
    public static function getGroupForOption() {
        return [self::GROUP_0 => trans('jobs.job_group.'.self::GROUP_0),
                self::GROUP_1 => trans('jobs.job_group.'.self::GROUP_1),
        ];
    }

    public static function getType($jobType) {
        switch($jobType) {
            case self::TYPE_0:
                return trans('jobs.job_type'.TYPE_0);
            case self::TYPE_1:
                return trans('jobs.job_type'.TYPE_1);
            case self::TYPE_2:
                return trans('jobs.job_type'.TYPE_2);
            case self::TYPE_3:
                return trans('jobs.job_type'.TYPE_3);
            case self::TYPE_4:
                return trans('jobs.job_type'.TYPE_4);
            case self::TYPE_5:
                return trans('jobs.job_type'.TYPE_5);
            case self::TYPE_6:
                return trans('jobs.job_type'.TYPE_6);
        }
    }

    public static function getJobTypeForOption() {
        return [self::TYPE_0 => trans('jobs.job_type.'.self::TYPE_0),
                self::TYPE_1 => trans('jobs.job_type.'.self::TYPE_1),
                self::TYPE_2 => trans('jobs.job_type.'.self::TYPE_2),
                self::TYPE_3 => trans('jobs.job_type.'.self::TYPE_3),
                self::TYPE_4 => trans('jobs.job_type.'.self::TYPE_4),
                self::TYPE_5 => trans('jobs.job_type.'.self::TYPE_5),
                self::TYPE_6 => trans('jobs.job_type.'.self::TYPE_6),
        ];
    }


   

}