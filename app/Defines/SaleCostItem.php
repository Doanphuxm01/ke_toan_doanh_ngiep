<?php

namespace App\Define;

class SaleCostItem
{
    const TAX_VALUE_0  = 0;
    const TAX_VALUE_10 = 10;

    static $objectForOption = [
        self::TAX_VALUE_0 =>[
            'name' => 'Việt Nam Đồng (VND)',
            'key' => self::VND
        ],
        self::TAX_VALUE_10 =>[
            'name' => 'Đô La Mỹ ($)',
            'key' => self::DOLLAR
        ],
    ];

    public static function getTaxValues() {
        return [self::TAX_VALUE_0, self::TAX_VALUE_10];
    }

    public static function getTaxValuesForOption() {
        return [
            self::TAX_VALUE_0   => trans('sale_cost_items.tax_values.' . self::TAX_VALUE_0),
            self::TAX_VALUE_10  => trans('sale_cost_items.tax_values.' . self::TAX_VALUE_10),
        ];
    }
}
