<?php

namespace App\Define;

class Customer
{
    // const LOCATION_OTHER = 0;
    const SOURCE_ADMIN  = 1;
    const SOURCE_FORM   = 2;

    const SOURCE_REVIEW_ADMIN   = "ADMIN";
    const SOURCE_REVIEW_CUSTOMER= "CUSTOMER";

    const GENDER_FEMALE = 0;
    const GENDER_MALE   = 1;
    const GENDER_OTHER  = 2;

    const LEVEL_MEMBER  = 1;
    const LEVEL_PARTNER = 2;
    const LEVEL_AGENCY  = 3;
    const LEVEL_DROPSHIP= 4;

    const COIN_SOURCE_REFILL     = 1;
    const COIN_SOURCE_TRANSACTION= 2;
    const COIN_SOURCE_COMMISSION = 3;

    const WITHDRAWAL_STATUS_REQUESTED   = "REQUESTED";
    const WITHDRAWAL_STATUS_PROCESSING  = "PROCESSING";
    const WITHDRAWAL_STATUS_REJECTED    = "REJECTED";
    const WITHDRAWAL_STATUS_SUCCESS     = "SUCCESS";

    public static function getSources() {
        return [self::SOURCE_ADMIN, self::SOURCE_FORM];
    }

    public static function getSourcesForOption() {
        return [self::SOURCE_ADMIN => trans('customers.sources.' . self::SOURCE_ADMIN), self::SOURCE_FORM => trans('customers.sources.' . self::SOURCE_FORM)];
    }

    public static function getGenders() {
        return [self::GENDER_FEMALE, self::GENDER_MALE, self::GENDER_OTHER];
    }

    public static function getGendersForOption() {
        return [self::GENDER_FEMALE => trans('customers.genders.' . self::GENDER_FEMALE), self::GENDER_MALE => trans('customers.genders.' . self::GENDER_MALE),
            self::GENDER_OTHER => trans('customers.genders.' . self::GENDER_OTHER)];
    }

    public static function getWithdrawalStatusForOption() {
        return [self::WITHDRAWAL_STATUS_REQUESTED => trans('customer_withdrawals.status.' . self::WITHDRAWAL_STATUS_REQUESTED), self::WITHDRAWAL_STATUS_PROCESSING => trans('customer_withdrawals.status.' . self::WITHDRAWAL_STATUS_PROCESSING), self::WITHDRAWAL_STATUS_REJECTED => trans('customer_withdrawals.status.' . self::WITHDRAWAL_STATUS_REJECTED), self::WITHDRAWAL_STATUS_SUCCESS => trans('customer_withdrawals.status.' . self::WITHDRAWAL_STATUS_SUCCESS)];
    }

    public static function getLevelsForOption() {
        return [self::LEVEL_MEMBER => trans('customers.levels.' . self::LEVEL_MEMBER), self::LEVEL_PARTNER => trans('customers.levels.' . self::LEVEL_PARTNER), self::LEVEL_DROPSHIP => trans('customers.levels.' . self::LEVEL_DROPSHIP), self::LEVEL_AGENCY => trans('customers.levels.' . self::LEVEL_AGENCY)];
    }

    public static function getCoinSourcesForOption() {
        return [self::COIN_SOURCE_REFILL => trans('customers.source_coins.' . self::COIN_SOURCE_REFILL), self::COIN_SOURCE_TRANSACTION => trans('customers.source_coins.' . self::COIN_SOURCE_TRANSACTION), self::COIN_SOURCE_COMMISSION => trans('customers.source_coins.' . self::COIN_SOURCE_COMMISSION)];
    }

    public static function getColorLevels($level) {
        switch ($level) {
            case self::LEVEL_MEMBER:
                return "<span class='btn bg-teal btn-flat margin'>" . trans('customers.levels.' . self::LEVEL_MEMBER) . "</span>";
            case self::LEVEL_PARTNER:
                return "<span class='btn bg-purple btn-flat margin'>" . trans('customers.levels.' . self::LEVEL_PARTNER) . "</span>";
            case self::LEVEL_DROPSHIP:
                return "<span class='btn bg-orange btn-flat margin'>" . trans('customers.levels.' . self::LEVEL_DROPSHIP) . "</span>";
            case self::LEVEL_AGENCY:
                return "<span class='btn bg-navy btn-flat margin'>" . trans('customers.levels.' . self::LEVEL_AGENCY) . "</span>";
        }
    }
}
