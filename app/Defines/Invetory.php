<?php

namespace App\Define;

class Inventory
{
    const TYPE_IN_STOCK     = 'IN_STOCK';
    const TYPE_OUT_STOCK    = 'OUT_STOCK';

    public static function getTypes() {
        return [self::TYPE_IN_STOCK, self::TYPE_OUT_STOCK];
    }

    public static function getTypesForOption() {
        return [self::TYPE_IN_STOCK => trans('inventories.types.' . self::TYPE_IN_STOCK), self::TYPE_OUT_STOCK => trans('inventories.types.' . self::TYPE_OUT_STOCK)];
    }
}
