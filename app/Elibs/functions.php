<?php
function admin_link($router = '',$withoutProject = FALSE)
{
    return url(str_replace('//', '/', '/' . $router));
}


function public_link($router = '',$withoutProject = FALSE)
{
    $host = 'https://bctc.test';
    if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'],['accounting.bctech.com.vn'])) {
        $host = 'http://accounting.bctech.com.vn/';
    }
    $host .= str_replace('//', '/', '/' . $router);
    return $host;
}
function value_show($value, $default = 'Chưa cập nhật')
{
    if (is_string($value) || is_numeric($value)) {
        if(empty($value)){
            return $default;
        }
        return $value;
    }
    if(is_array($value)){
        if(isset($value['name']) && is_string($value['name'])){
            return $value['name'];
        }else if(isset($value['value']) && is_string($value['value'])){
            return $value['value'];
        }else{
            return $default;
        }
    }

    return $default;
}

/**
 * @param $array1
 * @param $array2
 * @return array
 * So sánh 2 mảng
 */
function array_diff_assoc_recursive($array1, $array2)
{
    $difference = [];
    foreach ($array1 as $key => $value) {
        if (is_array($value)) {
            if (!isset($array2[$key]) || !is_array($array2[$key])) {
                $difference[$key] = $value;
            } else {
                $new_diff = array_diff_assoc_recursive($value, $array2[$key]);
                if (!empty($new_diff))
                    $difference[$key] = $new_diff;
            }
        } else if (!array_key_exists($key, $array2) || $array2[$key] !== $value) {
            $difference[$key] = $value;
        }
    }
    return $difference;
}
 function formatMoney($stringNumber, $sep = '.', $₫ = ' ₫') {
    if(!$stringNumber) {
        return 0;
    }
    $stringNumber = (double)$stringNumber;
    if(!$stringNumber){
        return $stringNumber;
    }
    return number_format($stringNumber, 0, ',', $sep).$₫;
}

function WriteVndText($amount)
{
    /*ham doc so thanh chua
    ** todo truyen param = so ko can fomat
    */
    if($amount <=0)
    {
        return $textnumber="Tiền phải là số nguyên dương lớn hơn số 0";
    }
    $Text=array("không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín");
    $TextLuythua =array("","nghìn", "triệu", "tỷ", "ngàn tỷ", "triệu tỷ", "tỷ tỷ");
    $textnumber = "";
    $length = strlen($amount);

    for ($i = 0; $i < $length; $i++)
        $unread[$i] = 0;

    for ($i = 0; $i < $length; $i++)
    {
        $so = substr($amount, $length - $i -1 , 1);

        if ( ($so == 0) && ($i % 3 == 0) && ($unread[$i] == 0)){
            for ($j = $i+1 ; $j < $length ; $j ++)
            {
                $so1 = substr($amount,$length - $j -1, 1);
                if ($so1 != 0)
                    break;
            }

            if (intval(($j - $i )/3) > 0){
                for ($k = $i ; $k <intval(($j-$i)/3)*3 + $i; $k++)
                    $unread[$k] =1;
            }
        }
    }

    for ($i = 0; $i < $length; $i++)
    {
        $so = substr($amount,$length - $i -1, 1);
        if ($unread[$i] ==1)
            continue;

        if ( ($i% 3 == 0) && ($i > 0))
            $textnumber = $TextLuythua[$i/3] ." ". $textnumber;

        if ($i % 3 == 2 )
            $textnumber = 'trăm ' . $textnumber;

        if ($i % 3 == 1)
            $textnumber = 'mươi ' . $textnumber;


        $textnumber = $Text[$so] ." ". $textnumber;
    }

    $textnumber = str_replace("không mươi", "lẻ", $textnumber);
    $textnumber = str_replace("lẻ không", "", $textnumber);
    $textnumber = str_replace("mươi không", "mươi", $textnumber);
    $textnumber = str_replace("một mươi", "mười", $textnumber);
    $textnumber = str_replace("mươi năm", "mươi lăm", $textnumber);
    $textnumber = str_replace("mươi một", "mươi mốt", $textnumber);
    $textnumber = str_replace("mười năm", "mười lăm", $textnumber);

    return ucfirst($textnumber." đồng chẵn");
}
function TrimTrailingZeroes($nbr,$array = false) {
    /**
     * @__note: ham trim dau .00 neu la tien viet con tien do se ko xoa di
     */
    if($array = true){
        foreach ($array as $_key => $obj){
            return strpos($obj,'.')!==false ? rtrim(rtrim($obj,'0'),'.') : $obj;
        }
    }else{
        return strpos($nbr,'.')!==false ? rtrim(rtrim($nbr,'0'),'.') : $nbr;
    }
}
function validateDate($date, $format = 'm/d/Y'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

//function convertDateToTimestamp($date){
//    return strtotime($date);
//}
 function convertTimestampToDate($timestamp){
     $date = new DateTime();
     return $date->setTimestamp($timestamp/1000);
 }

function convertDateToTimestamp($time, $format = 'd/m/Y', $MiniSecond = false)
{
    $date = \DateTime::createFromFormat($format, $time);
    $time = strtotime($date->format('Y/m/d'));
    if($MiniSecond){
        return $time * 1000;
    }
    return $time;
}

