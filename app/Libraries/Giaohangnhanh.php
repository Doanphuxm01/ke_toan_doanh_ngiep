<?php

namespace App\Library;
use Illuminate\Support\Facades\Cache;

class Giaohangnhanh
{
    public static function calculateServiceFee($shopId, $data)
    {
        $return = ['message' => '', 'success' => false];
        $data = json_encode($data);
        if (Cache::has('ghn_fee_' . $shopId . '_' . md5($data))) {
            $return['success'] = true;
            $return['message'] = json_decode(Cache::get('ghn_fee_' . $shopId . '_' . md5($data)), 1);
            return $return;
        }
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "v2/shipping-order/fee",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => $data,
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json',
                "Token: " . env('GHN_TOKEN'),
                "ShopId: " . $shopId,
                'Content-Length: ' . strlen($data)
            ],
        ]);

        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }
        $return['success'] = true;
        $return['message'] = $tmp['data'];

        if (isset($return['message']['total'])) {
            Cache::put('ghn_fee_' . $shopId . '_' . md5($data), json_encode($tmp['data']), 86400);//don vi s
        }

        return $return;
    }

    public static function createShippingOrder($shopId, $data)
    {
        $return = ['message' => '', 'success' => false];
        $data = json_encode($data);
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "v2/shipping-order/create",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => $data,
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json',
                "Token: " . env('GHN_TOKEN'),
                "ShopId: " . $shopId,
                'Content-Length: ' . strlen($data)
            ],
        ]);

        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }
        $return['success'] = true;
        $return['message'] = $tmp['data'];
        return $return;
    }

    public static function cancelOrder($shopId, $data)
    {
        $return = ['message' => '', 'success' => false];
        $data = json_encode($data);
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "v2/switch-status/cancel",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => $data,
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json',
                "Token: " . env('GHN_TOKEN'),
                "ShopId: " . $shopId,
                'Content-Length: ' . strlen($data)
            ],
        ]);

        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }
        $return['success'] = true;
        $return['message'] = $tmp['data'];
        return $return;
    }

    public static function getProvinces()
    {
        $return = ['message' => '', 'success' => false];
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "master-data/province",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => [
                "Token: " . env('GHN_TOKEN'),
            ],
        ]);
        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }

        $return['success'] = true;
        $return['message'] = $tmp['data'];

        return $return;
    }

    public static function getDistricts()
    {
        $return = ['message' => '', 'success' => false];
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "master-data/district",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => [
                "Token: " . env('GHN_TOKEN'),
            ],
        ]);
        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }

        $return['success'] = true;
        $return['message'] = $tmp['data'];

        return $return;
    }

    public static function getWards()
    {
        $return = ['message' => '', 'success' => false];
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "master-data/ward",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => [
                "Token: " . env('GHN_TOKEN'),
            ],
        ]);
        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }

        $return['success'] = true;
        $return['message'] = $tmp['data'];

        return $return;
    }

    public static function getShops() {
        $return = ['message' => '', 'success' => false];
        $ch = curl_init(env('GHN_URL') . 'v2/shop/all');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                "Token: " . env('GHN_TOKEN'),
                // 'Content-Length: ' . strlen($data)
        ]);

        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }

        $return['success'] = true;
        $return['message'] = $tmp['data'];

        return $return;
    }

    function syncToDB()
    {
        $districts = District::pluck('id', 'code');
        $districts1 = District::pluck('province_id', 'code');
        $wards = \App\Library\Giaohangnhanh::getWards();
        if ($wards['success']) {
            $wards = $wards['message'];
            foreach ($wards as $w) {
                if (!isset($districts1[$w['DistrictID']])) continue;
                $p = $districts1[$w['DistrictID']];
                Ward::create([
                    'code'  => $w['WardCode'],
                    'name'  => $w['WardName'],
                    'district_id'   => $districts[$w['DistrictID']] ?? 0,
                    'province_id'   => $p,
                    'status'    => 1,
                ]);
            }
        }
        dd(123);
        $provinces = Province::pluck('id', 'code');
        $districts = \App\Library\Giaohangnhanh::getDistricts();
        if ($districts['success']) {
            $districts = $districts['message'];
            foreach ($districts as $d) {
                District::create([
                    'code'  => $d['DistrictID'],
                    'name'  => $d['DistrictName'],
                    'province_id'   => $provinces[$d['ProvinceID']] ?? 0,
                    'status'    => 1,
                ]);
            }
        }
        dd(123);
        $provinces = \App\Library\Giaohangnhanh::getProvinces();
        if ($provinces['success']) {
            $provinces = $provinces['message'];
            foreach ($provinces as $p) {
                Province::create([
                    'code'  => $p['ProvinceID'],
                    'name'  => $p['ProvinceName'],
                    'status'    => 1,
                ]);
            }
        }
        dd(123);
    }

    public static function getServiceList($data) {
        $return = ['message' => '', 'success' => false];
        $data = json_encode($data);
        $ch = curl_init(env('GHN_URL') . 'v2/shipping-order/available-services');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            "Token: " . env('GHN_TOKEN'),
            'Content-Length: ' . strlen($data)
        ]);
        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
        }
        dd($tmp);
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }
        $return['success'] = true;
        $return['message'] = $tmp['data'];
    }

    public static function printLabel($orderCode) {
        $return = ['message' => '', 'success' => false];
        $ch = curl_init();
        $data = json_encode([
            'order_codes' => $orderCode
        ]);
        curl_setopt_array($ch, [
            CURLOPT_URL => env('GHN_URL') . "v2/a5/gen-token",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => $data,
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json',
                "Token: " . env('GHN_TOKEN'),
            ],
        ]);

        $tmp = json_decode(curl_exec($ch), 1);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) <> 200) {
            $return['message'] = json_encode($tmp);
            return $return;
        }
        if (!isset($tmp['code']) || $tmp['code'] <> 200) {
            $return['message'] = $tmp['message'];
            return $return;
        }
        $return['success'] = true;
        $return['message'] = $tmp['data']['token'];
        return $return;
    }
}


