<?php

namespace App\Exports;

use App\Models\Account;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
class AccountExport implements FromCollection,WithHeadings,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Account::all();
    }

    public function headings():array{
        return [
           trans('account_childs.account_number'),
           trans('account_childs.account_name'),
           trans('account_childs.account_total.label'),
           trans('account_childs.nature.label'),
           trans('account_childs.description'),
           trans('account_childs.parent_id'),
           trans('system.created_by'),
        ];
    }
}
