<?php

namespace App\Exports;

use App\Survey;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\Exportable;


class ReportExport implements FromView
{
	use Exportable;

    protected $reports_info;
    protected $data;

	public function __construct($reports_info, $data)
	{
        $this->reports_info = $reports_info;
        $this->data = $data;
	}

	public function view(): View
    {
        return view("backend.reports.template." . $this->reports_info['type'], [
            'type' => $this->reports_info['type'],
            'info' => $this->reports_info['name'],
            'data' => $this->data,
        ]);
    }
}
