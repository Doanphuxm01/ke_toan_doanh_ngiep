<?php

namespace App\Exports;

use App\Models\FixedAsset;
use App\Models\Department;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\Session;
use Config;
class FixedAssetExport implements FromView, WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        
        return view('backend.fixed-asset-reports.report', [
            $fixed_asset = Session::get('fixed_assets')
        ]);
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange1 = '1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(18);
                $cellRange2 = '2'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
            },
        ];
    }
    
}
