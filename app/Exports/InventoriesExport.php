<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\Exportable;

class InventoriesExport implements FromView, WithEvents
{
	use Exportable;
    protected $categories;

	public function __construct($categories)
	{
        $this->categories = $categories;
	}

	public function view(): View
    {
        return view('backend.inventories.template_excel', [
            'categories' => $this->categories
        ]);
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Sube');
        return $drawing;
    }

	public function registerEvents(): array
	{
		return [
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->setCreator('BCTech.,JSC');
                // dd($event->writer->getDelegate()->getProperties());
            },
            AfterSheet::class => function(AfterSheet $event) {
                // $totalHeight = 12 + count($this->data['totalPaper']) + count($this->data['pShips']) + 1 + ($this->data['decorating'] ? 1 : 0);
                // $event->sheet->allRowsHeight(20);

            },
        ];
	}
}
