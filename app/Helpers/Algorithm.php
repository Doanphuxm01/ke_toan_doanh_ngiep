<?php

namespace App\Helper;

class Algorithm {

    public function permutations(array $array, $inb=false)
    {
        switch (count($array)) {
            case 1:
                // Return the array as-is; returning the first item
                // of the array was confusing and unnecessary
                $arrayKeys = array_keys($array);

                // the first element of your array is:
                // echo '=====';
                // print_r( $array[$arrayKeys[0]]);
                // echo '=====';exit;

                return $array[$arrayKeys[0]];
                break;
            case 0:
                throw new InvalidArgumentException('Requires at least one array');
                break;
        }

        // We 'll need these, as array_shift destroys them
        $keys = array_keys($array);

        $a = array_shift($array);
        $k = array_shift($keys); // Get the key that $a had

        $b = $this->permutations($array, 'recursing');

        $return = array();
        foreach ($a as $v) {
            if($v)
            {
                foreach ($b as $v2) {
                    // array($k => $v) re-associates $v (each item in $a)
                    // with the key that $a originally had
                    // array_combine re-associates each item in $v2 with
                    // the corresponding key it had in the original array
                    // Also, using operator+ instead of array_merge
                    // allows us to not lose the keys once more
                    if (!is_array($v2)) $v2 = array($v2);
                    if($inb == 'recursing')
                        $return[] = array_merge(array($v), (array) $v2);
                    else
                        $return[] = array($k => $v) + array_combine($keys, $v2);
                }
            }
        }

        return $return;
    }

    //source: http://dannyherran.com/2011/06/finding-unique-array-combinations-with-php-permutations/
    public function permutationsBackupGreater2D(array $array, $inb=false)
    {
        switch (count($array)) {
            case 1:
                // Return the array as-is; returning the first item
                // of the array was confusing and unnecessary
                return $array[0];
                break;
            case 0:
                throw new InvalidArgumentException('Requires at least one array');
                break;
        }

        // We 'll need these, as array_shift destroys them
        $keys = array_keys($array);

        $a = array_shift($array);
        $k = array_shift($keys); // Get the key that $a had
        $b = $this->permutations($array, 'recursing');

        $return = array();
        foreach ($a as $v) {
            if($v)
            {
                foreach ($b as $v2) {
                    // array($k => $v) re-associates $v (each item in $a)
                    // with the key that $a originally had
                    // array_combine re-associates each item in $v2 with
                    // the corresponding key it had in the original array
                    // Also, using operator+ instead of array_merge
                    // allows us to not lose the keys once more
                    if($inb == 'recursing')
                        $return[] = array_merge(array($v), (array) $v2);
                    else
                        $return[] = array($k => $v) + array_combine($keys, $v2);
                }
            }
        }

        return $return;
    }
}
