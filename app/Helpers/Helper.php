<?php

namespace App\Helper;

if (! function_exists('formatCurrency')) {
	function formatCurrency($number)
	{
		return number_format($number, 0, "." , ",");
	}
}

?>