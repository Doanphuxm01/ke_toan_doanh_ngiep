<?php namespace App\Listeners;

use App\Mail\RegisterMail;

use Illuminate\Support\Facades\Mail;
#use Illuminate\Bus\Queueable;

use App\Events\RegisterSuccessfully;

#use Illuminate\Queue\InteractsWithQueue;
#use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

class RegisterSuccessfullyListener // implements ShouldQueue
{
    //use Queueable;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //\Log::info(__METHOD__ . ' in ' . __CLASS__);
    }

    /**
     * Handle the event.
     *
     * @param  RegisterSuccessfully  $event
     * @return void
     */
    public function handle(RegisterSuccessfully $event)
    {
        $event->customer->activation_code = str_random(100);
        $event->customer->activation_code_expire = Carbon::now()->addDays(3);
        $event->customer->save();
        //\Log::info($event->customer->email . ' = ' . $event->customer->fullname);
        Mail::to($event->customer->email, $event->customer->fullname)->later(Carbon::now()->addMinutes(1), new RegisterMail($event->customer));
    }
}
