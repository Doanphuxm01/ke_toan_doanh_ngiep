<?php

namespace App\Listeners;

use App\Customer;
use Carbon\Carbon;
use App\Events\OrderSuccessfully;
use App\Mail\OrderMailSuccessfully;
use Illuminate\Support\Facades\Mail;

class OrderSuccessfullyListener
{
    public function __construct()
    {
    }

    public function handle(OrderSuccessfully $event)
    {
        $recievers = [
            ['email' => env('ADMIN_EMAIL'), 'name' => env('ADMIN_NAME')],
            ['email' => $event->order->delivery_email, 'name' => $event->order->delivery_fullname]
        ];
        if ($event->order->voucher) {
            $partner = Customer::where('code', $event->order->voucher)->first();
            if (!is_null($partner)) {
                array_push($recievers, [
                    'email' => $partner->email,
                    'name'  => $partner->fullname,
                ]);
            }
        }
        Mail::to($recievers)->later(Carbon::now()->addSeconds(5), new OrderMailSuccessfully($event->order));
    }
}
