<?php

namespace App\Listeners;

use App\Customer;
use Carbon\Carbon;
use App\Mail\OrderMailNotify;
use App\Events\OrderSendMail;
use Illuminate\Support\Facades\Mail;

class OrderSendMailListener
{
    public function __construct()
    {
    }

    public function handle(OrderSendMail $event)
    {
        $recievers = [
            ['email' => env('ADMIN_EMAIL'), 'name' => env('ADMIN_NAME')],
            ['email' => $event->order->delivery_email, 'name' => $event->order->delivery_fullname]
        ];
        if ($event->order->voucher) {
            $partner = Customer::where('code', $event->order->voucher)->first();
            if (!is_null($partner)) {
                array_push($recievers, [
                    'email' => $partner->email,
                    'name'  => $partner->fullname,
                ]);
            }
        }
        Mail::to($recievers)->later(Carbon::now()->addSeconds(5), new OrderMailNotify($event->order));
    }
}
