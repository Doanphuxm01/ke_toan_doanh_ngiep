<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class PushPartnerMakeOrderMail extends Mailable
{
    protected $day;
    protected $partner;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($day, $partner)
    {
        $this->day = $day;
        $this->partner = $partner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $day = $this->day;
        $partner = $this->partner;

        return $this->view('emails.push_partner_make_order', compact('day', 'partner'))->subject($partner->fullname . ' đã lâu rồi bạn chưa có đơn hàng mới');
    }
}
