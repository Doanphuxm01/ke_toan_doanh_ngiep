<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class OrderMailNotify extends Mailable
{
    protected $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function build()
    {
        $order = $this->order;
        return $this->view('emails.notify_order', compact('order'))->subject('SUBE.VN Giao hàng thành công');
    }
}
