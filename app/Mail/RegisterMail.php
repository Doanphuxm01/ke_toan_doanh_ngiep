<?php namespace App\Mail;

use Illuminate\Mail\Mailable;

class RegisterMail extends Mailable
{
    protected $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        //\Log::info(__METHOD__ . ' in ' . __CLASS__);
        $this->customer = $customer;
    }

    /**
     *
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $customer = $this->customer;
        return $this->view('emails.register', compact('customer'))->subject('Đăng ký tài khoản thành công');
    }
}
