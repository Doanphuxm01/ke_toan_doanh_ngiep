<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class OrderMailSuccessfully extends Mailable
{
    protected $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function build()
    {
        $order = $this->order;
        return $this->view('emails.success_order', compact('order'))->subject('SUBE.VN Xác nhận đơn hàng');
    }
}
