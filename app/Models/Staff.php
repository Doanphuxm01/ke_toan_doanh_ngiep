<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
     protected $connection = "mysql_hrm";
      protected $table = "staffs";
}