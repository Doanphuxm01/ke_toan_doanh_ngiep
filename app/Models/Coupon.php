<?php namespace App;

use Illuminate\Support\Facades\Cache;

class Coupon extends \Eloquent {

    public static function rules($id = 0) {
        return [
            "name"          => 'required|max:100',
            'code'          => $id ? '' : 'required|max:20',
            "note"          => 'max:255',
            "from_date"     => 'required|date_format:Y-m-d H:i',
            "to_date"       => 'required|date_format:Y-m-d H:i|after:from_date',
            //"contact_ids"   => $id ? '' : 'required',
            'percent'       => 'required_without:amount|between:0.01,99.99',
            'amount'        => 'required_without:percent|integer|min:100',
            'max_time'      => 'required|between:1,100',
        ];
    }

	// Don't forget to fill this array
	protected $fillable = ["name", "code", "created_by", "note", "status", "from_date", "to_date", "contact_ids", "percent", "contact_ids", 'max_time', 'counter', 'amount'];

    public function orders()
    {
        return $this->hasMany("\App\Order", 'discount_ref_id');
    }

    public static function boot()
    {
        parent::boot();

        static::updated(function($coupon)
        {
            self::clearCache();
        });

        static::deleted(function($coupon)
        {
            self::clearCache();
        });

        static::created(function($coupon) // ko co?
        {
            self::clearCache();
        });

        static::saved(function($coupon)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('coupons_all');
    }

    public static function getCoupons()
    {
        $coupons = [];
        # calc minute auto clean cache
        if (!Cache::has('coupons_all')) {
            $coupons = Coupon::where('status', 1)->where("from_date", "<=", date("Y-m-d H:i"))->where("to_date", ">=", date("Y-m-d H:i"))->select('name', 'id', 'code', 'amount', 'percent', 'from_date', 'to_date')->orderBy('updated_at', 'DESC')->get();
            $coupons = json_encode($coupons);
            Cache::put('coupons_all', $coupons, Define\Constant::SHORT_TTL);
        } else {
            $coupons = Cache::get('coupons_all');
        }

        return json_decode($coupons, 1);
    }
}