<?php
namespace App;

use Illuminate\Support\Facades\Cache;

class Attribute extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'name'                  => 'required|max:100|unique:attributes,name' . ($id == 0 ? '' : ',' . $id),
            'type'                  => $id ? "" : 'required',
            'select'                => $id ? "" : 'required',
        ];
    }
	protected $fillable = [ 'name', 'type', 'select', 'status' ];

    public static function getByAll()
    {
        $redis  = \App::make('redis');
        $attributes = $redis->get("attributes");
        if (!$attributes) {
            $attributes = Attribute::select('id', 'name', 'type', 'select')->get()->keyBy('id');
            // ->toArray();
            // $aValues = $aValues
            $attributes = json_encode($attributes);
            $redis->set("attributes", $attributes);
        }

        return json_decode($attributes, 1);
    }

    public static function boot()
    {
        parent::boot();
        self::clearCache();
    }

    public static function clearCache()
    {
    }

    public function values()
    {
        return $this->hasMany("\App\AttributeValue");
    }
}