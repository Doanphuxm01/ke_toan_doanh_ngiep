<?php namespace App;

class ProductView extends \Eloquent {
	public $timestamps = false;
	protected $fillable = [ 'product_id', 'counter', 'time' ];
}