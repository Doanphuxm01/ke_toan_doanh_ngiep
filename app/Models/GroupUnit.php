<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupUnit extends Model
{
    protected $table = 'group_units';

    public static function getGroupUnit()
    {
        $getGroup = [];
        $groupUnit = GroupUnit::all()->toArray();
        // dd( $groupUnit);
        foreach($groupUnit as $items){
            $getGroup["group".$items["id"]] = [$items['id']=>$items['details']];
        }
        return $getGroup;
    }
}
