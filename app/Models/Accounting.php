<?php

namespace App\Models;
use  App\User;

use Illuminate\Database\Eloquent\Model;

class Accounting extends Model
{
    protected $table = "accounting";
   	protected $guarded = [];
    
    public static function rules()
    {
        return [
          
            "credit_account" 	=> 'required',
            "debit_account"  	=> 'required',
            "exchange" 			=> 'required|min:0',
            "sale_code_item" 	=> 'sometimes|required',
            "amount" 			=> 'required',
//            "branch_code_id"  	=> 'sometimes|required',
            "quantity"       	=> 'sometimes|required',
            "price_unit"        => 'sometimes|required',
//			'job_code_id'		=> 'required',
        ];
    } 

    public static function departmentType($companyId)
    {
        $results=[];
        $tmp = Department::where('company_id', $companyId)->where('status', 1)->get();
        foreach ($tmp as $department) {
            $results[$department->id] = $department->name;
        }
        return $results;
    }
    public static function userType($companyId)
    {
        $results=[];
        $tmp = User::where('company_id', $companyId)->get();
        foreach ($tmp as $user) {
            $results[$user->id] = $user->fullname;
        }
        return $results;
    }
    public static function partnerType($companyId)
    {
        $results=[];
        $tmp = Partner::where('company_id', $companyId)->where('status', 1)->get();
        foreach ($tmp as $partner) {
            $results[$partner->id] = $partner->name;
        }
        return $results;
    }
    public static function getDepartmentObject($id)
    {
        $result = Department::where('id', $id)->pluck('name')->first();
        return $result;
    }
    public static function getUserObject($id)
    {
        $result = User::where('id', $id)->pluck('fullname')->first();
        return $result;
    }
    public static function getPartnerObject($id)
    {
        $result = Partner::where('id', $id)->pluck('name')->first();
        return $result;
    }

	public function accountingInvoice()
	{
		return $this->belongsTo(AccountingInvoice::class);
    }
}
