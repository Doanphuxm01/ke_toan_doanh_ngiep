<?php
namespace App;

class OrderInstalment extends \Eloquent {

	protected $fillable = [ "order_id", "installment", "checkoutType", "month", "bankCode", "paymentMethod", "amountFee", "amountFinal", "amountByMonth", "errorCode", "data", "cancel" ];
}