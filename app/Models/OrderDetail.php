<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class OrderDetail extends \Eloquent {

	protected $fillable = [ "price_unit", "order_id", "product_id", "price_ipo", "price_original", "sale_amount", "quantity", "sale_percent", 'product_gift_ids', 'free_shipping_id', 'sale_id', 'sale_gift_id', 'voucher_amount', 'voucher', 'inventory_category_id', 'voucher_partner_id', 'voucher_admin_id', 'voucher_admin_amount', 'customer_id', 'bonus_coin' ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}