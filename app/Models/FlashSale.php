<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlashSale extends \Eloquent {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function rules($id = 0) {
        return [
            "name"  => 'required|max:255',
            // "time_range"  => 'required',//|format:d/m/Y H:i - d/m/Y H:i
            "image" => ($id ? 'nullable' : 'required') . '|max:2048|mimes:jpg,jpeg,png,gif',
            "note"  => 'max:255',
        ];
    }

	protected $fillable = ["name", "image", "note", "status", "created_by", "from_date", "to_date", "deleted_by", "from_time", "to_time", "is_flashsale"];

    public static function boot()
    {
        parent::boot();

        static::updated(function($sale)
        {
            self::clearCache();
        });

        static::deleted(function($sale)
        {
            self::clearCache();
        });

        static::created(function($sale)
        {
            self::clearCache();
        });

        static::saved(function($sale)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::tags('sale_by_categories')->flush();
        Cache::forget('sale_products_has_sale');
        Cache::forget('flash_sale_now');
        Cache::forget('flash_sale_other');
    }

    public static function getNowFlashSale()
    {
        $flashSale = [];
        if (!Cache::has('flash_sale_now')) {
            $now = time();
            $flashSale = FlashSale::where('status', 1)->where('is_flashsale', 1)->where('from_time', "<=", $now)->where('to_time', ">=", $now)->select('id', 'name', 'image', 'from_time', 'to_time')->first();
            if (!is_null($flashSale)) {
                $flashSale = $flashSale->toArray();
                // get sale have product discount
                $sales = Sale::where('flash_sale_id', $flashSale['id'])->where('status', 1)->whereIn('group', \App\Define\Sale::getGroupByProducts())->select('id', 'name', 'group', 'gift_product_ids', 'discount_percent', 'discount_amount')->orderBy('updated_at', 'DESC')->get();
                for ($i=0; $i < $sales->count(); $i++) {
                    $productIds = SaleProduct::where('flash_sale_id', $flashSale['id'])->where('sale_id', $sales[$i]->id)->where('status', 1)->pluck('root_product_id', 'root_product_id')->toArray();
                    if (count($productIds) == 0) continue;
                    $sales[$i]['productIds'] = $productIds;
                }
                // get list of products
                $flashSale['sales'] = $sales;
            } else {
                $flashSale = [];
            }
            $flashSale = json_encode($flashSale);
            Cache::forever('flash_sale_now', $flashSale);
        } else {
            $flashSale = Cache::get('flash_sale_now');
        }

        return json_decode($flashSale, 1);
    }

    public static function getOtherSales()
    {
        $flashSales = [];
        if (!Cache::has('flash_sale_other')) {
            $now = time();
            $tmp = FlashSale::where('status', 1)->where('is_flashsale', 0)->where('from_time', "<=", $now)->where('to_time', ">=", $now)->select('id', 'name', 'image', 'from_time', 'to_time')->get()->keyBy('id')->toArray();
            if (count($tmp)) {
                $sales = Sale::whereIn('flash_sale_id', array_keys($tmp))->where('status', 1)->whereIn('group', \App\Define\Sale::getGroupByProducts())->select('id', 'name', 'group', 'gift_product_ids', 'discount_percent', 'discount_amount')->orderBy('updated_at', 'DESC')->get();
                $data = [];
                for ($i=0; $i < $sales->count(); $i++) {
                    $productIds = SaleProduct::where('sale_id', $sales[$i]->id)->where('status', 1)->pluck('root_product_id', 'root_product_id')->toArray();
                    if (count($productIds) == 0) continue;
                    $data += $productIds;
                }
                $flashSales = $data;
            } else {
                $flashSales = [];
            }
            $flashSales = json_encode($flashSales);
            Cache::forever('flash_sale_other', $flashSales);
        } else {
            $flashSales = Cache::get('flash_sale_other');
        }

        return json_decode($flashSales, 1);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }
}