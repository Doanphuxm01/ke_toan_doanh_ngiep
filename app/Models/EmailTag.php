<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class EmailTag extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'value'    => 'max:20|required|unique:email_tags,value' . ($id == 0 ? '' : ',' . $id),
        	'sample'   => 'max:100|required',
        ];
    }

    protected $fillable = ['value', 'sample'];

    public static function boot()
    {
        parent::boot();

        static::updated(function($district)
        {
            self::clearCache();
        });

        static::deleted(function($district)
        {
            self::clearCache();
        });

        static::created(function($district)
        {
            self::clearCache();
        });

        static::saved(function($district)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('email_tag_field');
        Cache::forget('email_tag_sample');
    }

    public static function getFields() {
        $fields = [];
        if (!Cache::has('email_tag_field')) {
            $fields = EmailTag::pluck('field', 'value')->toArray();
            $fields = json_encode($fields);
            Cache::forever('email_tag_field', $fields);
        } else {
            $fields = Cache::get('email_tag_field');
        }

        return json_decode($fields, 1);
    }

    public static function getSamples() {
    	$fields = [];
        if (!Cache::has('email_tag_sample')) {
            $fields = EmailTag::pluck('sample', 'value')->toArray();
            $fields = json_encode($fields);
            Cache::forever('email_tag_sample', $fields);
        } else {
            $fields = Cache::get('email_tag_sample');
        }

        return json_decode($fields, 1);
    }
}