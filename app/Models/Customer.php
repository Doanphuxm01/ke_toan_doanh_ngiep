<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable {

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $hidden = ['password', 'remember_token'];
    protected $fillable = ['code', 'fullname', 'email', 'password', 'status', 'last_login', 'remember_token', 'phone', 'address', 'source', 'created_by', 'deleted_by', 'level', 'recover_code', 'recover_time', 'coin_available', 'coin_holding', 'coin_threshold', 'avatar', 'gender', 'dob', 'facebook_id', 'google_id', 'bonus_coin'];

    public static function rules($id = 0) {
        return [
            'code'          => 'required|max:50|regex:/^[A-Za-z0-9_.-]+$/|unique:customers,code' . ($id == 0 ? '' : ',' . $id),
            'coin_threshold'=> 'required|integer|min:0',
            'fullname'  => 'required|max:255',
            'phone'     => 'required|regex:/[0]\d{9,10}$/iD',
            'email'     => 'email|max:100|min:6|unique:customers,email' . ($id == 0 ? '|required' : ',' . $id),
            'level'     => 'required',
            'password'      => 'min:6|max:20' . ($id == 0 ? '|required' : ''),
            're_password'   => 'same:password',
            'address'   => 'required|max:255',
        ];
    }

    public static function loginRules() {
        return [
            'email'                 => 'required|email|max:50|min:6',
            'password'              => 'required|min:6|max:20',
        ];
    }

    public static function registerRules() {
        return [
            'fullname'  => 'required|max:100',
            // 'phone'     => 'required|regex:/[0]\d{9,10}$/iD',
            'email'     => 'required|email|max:100|min:6|unique:customers,email',
            'password'  => 'required|min:6|max:20',
            // 're_password'           => 'same:password',
        ];
    }

    public function contacts() {
        return $this->hasMany(CustomerContact::class, "customer_id");
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function accounts() {
        return $this->hasMany(CustomerBankAccount::class);
    }
}
