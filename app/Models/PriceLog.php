<?php

namespace App;

class PriceLog extends \Eloquent
{

    public $timestamps = false;

    protected $fillable = ['source', 'field', 'data_old', 'data_new', 'action_by', 'note', 'action_at', 'product_id', 'category_id'];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->action_by = \Auth::guard('admin')->user()->id;
            $model->action_at = date("Y-m-d H:i:s");
        });

        self::created(function ($model) {
            // ... code here
        });

        self::updating(function ($model) {
            // ... code here
        });

        self::updated(function ($model) {
            // ... code here
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            // ... code here
        });
    }
}
