<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerSaleProduct extends \Eloquent {
	use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable = ["partner_sale_id", "from_time", "to_time", "status", "object_id", "created_by", "deleted_by", "type"];
}