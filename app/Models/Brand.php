<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class Brand extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'name'      => 'required|max:50|unique:brands,name' . ($id == 0 ? '' : ',' . $id),
            'summary'   => 'max:255',
            'logo'      => 'max:2048|mimes:jpg,jpeg,png,gif' . ($id ? '' : '|required'),
            'image1'    => 'nullable|max:2048|mimes:jpg,jpeg,png,gif',
            'image2'    => 'nullable|max:2048|mimes:jpg,jpeg,png,gif',
            'image3'    => 'nullable|max:2048|mimes:jpg,jpeg,png,gif',
            'href'      => 'url|max:255|nullable',
            'seo_keywords'      => 'max:50',
            'seo_description'   => 'max:255',
        ];
    }
	protected $fillable = [ 'name', 'summary', 'href', 'logo', 'status', 'seo_keywords', 'seo_description', 'is_top', 'image1', 'image2', 'image3' ];

    public static function boot()
    {
        parent::boot();

        static::updated(function($brand)
        {
            self::clearCache();
        });

        static::deleted(function($brand)
        {
            self::clearCache();
        });

        static::created(function($brand)
        {
            self::clearCache();
        });

        static::saved(function($brand)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('brands_categories');
        Cache::forget('brands');
        Cache::forget('home_brands');
        Cache::tags('brand_categories')->flush();
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id');
    }

    public function countSameType()
    {
        return ProductCategory::where("product_category_id", $this->product_category_id)->count();
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_categories', 'product_category_id', 'attribute_id');
    }

    public function categories()
    {
        // return $this->belongsToMany(Brand::class, 'brand_categories', 'product_category_id', 'brand_id');
        return $this->belongsToMany(ProductCategory::class, 'brand_categories', 'brand_id', 'product_category_id')->withPivot('position');
    }

    public static function getBrandByCategories($category)
    {
        $categoryBrands = Cache::tags(['brand_categories'])->get('brand_categories_' . $category->id);
        if (!$categoryBrands) {
            $categoryBrands = $category->brands()->where('status', 1)->orderBy('brand_categories.position')->select('brands.name', 'brands.href', 'brands.logo', 'brands.id')->get()->toArray();
            $categoryBrands = json_encode($categoryBrands);
            if ($categoryBrands) Cache::tags(['brand_categories'])->put('brand_categories_' . $category->id, $categoryBrands);
        }

        return json_decode($categoryBrands, 1);
    }

    public static function getBrands()
    {
        $brands = [];
        if (!Cache::has('brands_categories')) {
            $productCategories = ProductCategory::where('status', 1)->where('product_category_id', 0)->orderBy('position')->get();
            foreach ($productCategories as $pCategory) {
                $brands[$pCategory->id] = $pCategory->brands()->where('status', 1)->orderBy('brand_categories.position')->select('brands.name', 'brands.href', 'brands.logo', 'brands.id')->get()->toArray();
            }
            $brands = json_encode($brands);
            Cache::forever('brands_categories', $brands);
        } else {
            $brands = Cache::get('brands_categories');
        }

        return json_decode($brands, 1);
    }

    public static function getHomeBrands()
    {
        $brands = [];
        if (!Cache::has('home_brands')) {
            $brands = Brand::where('status', 1)->where('is_top', 1)->orderBy('updated_at', 'DESC')->select('id', 'name', 'href', 'logo')->get()->toArray();
            $brands = json_encode($brands);
            Cache::forever('home_brands', $brands);
        } else {
            $brands = Cache::get('home_brands');
        }

        return json_decode($brands, 1);
    }

    public static function getByAll()
    {
        $brands = [];
        if (!Cache::has('brands')) {
            $brands = Brand::select('name', 'id', 'logo')->get()->keyBy('id')->toArray();//where('status', 1)->
            $brands = json_encode($brands);
            Cache::forever('brands', $brands);
        } else {
            $brands = Cache::get('brands');
        }

        return json_decode($brands, 1);
    }
}