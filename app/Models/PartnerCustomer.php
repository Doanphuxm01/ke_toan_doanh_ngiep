<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerCustomer extends \Eloquent {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
	protected $fillable = ["fullname", "partner_id", "phone", "status", "address", "deleted_by"];
}