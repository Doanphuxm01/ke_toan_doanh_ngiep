<?php

namespace App;

class Mailgun extends \Eloquent {
    protected $fillable = ['status', 'mailgun_id', 'trans_id'];
}