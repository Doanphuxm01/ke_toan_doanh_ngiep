<?php

namespace App;

class SaleLog extends \Eloquent {
    public $timestamps = false;
    protected $fillable = ['flash_sale_id', 'sale_id', 'field', 'data_old', 'data_new', 'action_by', 'note', 'action_at'];
}