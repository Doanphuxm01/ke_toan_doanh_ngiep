<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleCostItem extends BaseModels
{
    protected $fillable = ['code', 'description', 'branch_code', 'charge_type', 'tax_value', 'sale_account_code', 'created_by', 'cost_account_code', 'prov_account_code', 'company_id', 'status'];

    public static function rules($id = 0){
        return [
            'code'          => 'required|max:20|regex:/^[a-zA-Z0-9_]+([-.][a-zA-Z0-9_]+)*$/|unique:sale_cost_items,code' . ($id == 0 ? '' : ',' . $id),
            'branch_code'   => 'required',
            'description'   => 'required|max:255',
            'charge_type'   => 'required',
            'tax_value'         => 'required',
            'sale_account_code' => 'required',
            'cost_account_code' => 'required',
            'prov_account_code' => 'required',
        ];
    }
    public static function getSaleCostItems($companyId)
    {
        $results=[];
        $tmp = SaleCostItem::where('company_id', $companyId)->where('status', '1')->get();
        foreach ($tmp as $sale_cost_item) {
            $results[$sale_cost_item->code] = $sale_cost_item->code;
        }
        return $results;
    }


}
