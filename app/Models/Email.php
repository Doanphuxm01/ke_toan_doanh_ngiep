<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends \Eloquent {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public static function rules($id = 0) {
        return [
        	'fullname' 	=> 'max:100',
        	'email' 	=> 'max:50|min:10|unique:emails,email' . ($id == 0 ? '|required' : ',' . $id),
			'mobile'	=> 'max:11|regex:/[0]\d{9,11}$/|nullable',
			'gender' 	=> 'in:' . implode(',', Define\Customer::getGenders()) . '|nullable',
            'dob' 		=> 'date:d/m/Y',
            'district'	=> 'integer|nullable',
			'province'	=> 'integer|nullable',
			// 'status'	=> 'integer',
			'address'	=> 'max:255',
			'ward'		=> 'max:100',
			'note'		=> '',
        ];
    }

    protected $fillable = ['fullname', 'email', 'mobile', 'gender', 'dob', 'district', 'province', 'status', 'address', 'district_id', 'province_id', 'ward', 'source', 'note', 'info_updated_by', 'info_updated_at', 'created_by', 'unsubscribe_des', 'trans_id', 'deleted_by'];

    public function contacts()
    {
        return $this->belongsToMany('App\Contact', 'contact_emails', 'email_id', 'contact_id');
    }
}