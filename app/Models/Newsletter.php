<?php
namespace App;

class Newsletter extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'email'                 => 'required|email|max:100|min:10|',
        ];
    }

	protected $fillable = [ 'email' ];
}