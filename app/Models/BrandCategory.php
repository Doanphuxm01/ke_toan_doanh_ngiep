<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class BrandCategory extends \Eloquent {

    public $timestamps = false;
    protected $fillable = [ 'product_category_id', 'brand_id', 'position' ];

    public static function boot()
    {
        parent::boot();

        static::updated(function($brand)
        {
            self::clearCache();
        });

        static::deleted(function($brand)
        {
            self::clearCache();
        });

        static::created(function($brand)
        {
            self::clearCache();
        });

        static::saved(function($brand)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('brands_categories');
    }
}