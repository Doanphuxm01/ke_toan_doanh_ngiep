<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class InventoryCategoryMap extends \Eloquent {
	public static function getByAll()
    {
        $maps = [];
        if (!Cache::has('inventory_category_maps')) {
            $maps = InventoryCategoryMap::where('status', 1)->select('shop_id', 'code', 'inventory_category_id', 'type', 'district_id')->get()->toArray();
            $maps = json_encode($maps);
            if ($maps) Cache::forever('inventory_category_maps', $maps);
        } else {
            $maps = Cache::get('inventory_category_maps');
        }
        return json_decode($maps, 1);
    }
}
