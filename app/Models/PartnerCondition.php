<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerCondition extends \Eloquent {
    protected $fillable = ['name', 'vip', 'svip', 'ssvip', 'position'];
    public static function rules($id = 0) {
        return [
            'name' => 'required|max:255',
            'vip'  => 'required|max:1024',
            'svip' => 'required|max:1024',
            'ssvip'=> 'required|max:1024',
        ];
    }
}