<?php
namespace App\Models;

class FixedAssetAllocationInformation extends \Eloquent {
    protected $table = "fixed_asset_allocation_information";
	protected $fillable = [ 
        'asset_code', 
        'department_id', 
        'allocation_rate', 
        'expense_account', 
        'cost_item', 
    ];
    public static function getUseDepartment($asset_code)
    {
        $results = [];
        $use_department = FixedAssetAllocationInformation::where('asset_code', $asset_code)->get();
        foreach ($use_department as $ud) {
           $department = Department::where('id', $ud->department_id)->get();
           foreach ($department as $key => $d) {
                array_push($results, $d->name);
           }
        }    
        return $results;
    }
    public function fixed_asset()
    {
        return $this->belongsTo(FixedAsset::class, 'asset_code', 'asset_code');
    }
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }
}