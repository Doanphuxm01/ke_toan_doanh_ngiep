<?php
namespace App\Models;

class InstrumentToolDisposal extends \Eloquent {
    protected $table = "instrument_tool_disposal";
	protected $fillable = [ 
        'voucher_no', 
        'voucher_date', 
        'disposal_reason', 
        'instrument_tool_id',
        'status',
        'company_id',
        'status',
        'created_by'
    ];
    public static function checkAllocatedQuantity($instrument_tool_id)
    {
        $instrument_tool = InstrumentTool::where('id', $instrument_tool_id)->first();
        $allocated_value = 0;
        $cost = InstrumentToolAllocationCost::where('instrument_tool_id', $instrument_tool_id)->get();
        $allo_quantity = [];
        $disposal = InstrumentToolDisposal::where('instrument_tool_disposal.instrument_tool_id', $instrument_tool_id)
                                            ->join('instrument_tool_disposal_details', 'instrument_tool_disposal.id', '=', 'instrument_tool_disposal_details.disposal_voucher_id')
                                            ->get();
        if(count($disposal)){
            foreach ($disposal as $key => $value) {
                    $allo_quantity[$value->department_id] += $value->disposal_quantity; 
               
            }
        }
        return $allo_quantity;
    }
    public static function getTotal($disposal_voucher_id)
    {   
        $disposal = InstrumentToolDisposalDetail::where('disposal_voucher_id', $disposal_voucher_id)->get();
        $result = 0;
        foreach ($disposal as $key => $value) {
            $result += $value->amount;
        }
        return $result;
    }
    public static function checkEditDelete($id){
        $disposal = InstrumentToolDisposal::find(intval($id));
        $date = explode("-", $disposal->voucher_date);
        return $allocation_voucher = InstrumentToolAllocation::where('month', $date[1])->where('year', $date[0])->get();
    }
}