<?php

namespace App;

class Sendgrid extends \Eloquent {
    protected $fillable = ['status', 'sendgrid_id', 'trans_id'];
}