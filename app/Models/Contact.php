<?php

namespace App;

class Contact extends \Eloquent {
    protected $connection = "mysql_hrm";
    protected $table = "contacts";
    public static function rules($id = 0) {
        return [
            "name"          => "required|max:100|unique:contacts,name" . ($id == 0 ? '' : ',' . $id),
            "note"          => "max:100",
        ];
    }
	protected $fillable = [ 'status', 'name', 'created_by', "note", 'status', 'source' ];

    public function emails()
    {
        return $this->belongsToMany('App\Email', 'contact_emails', 'contact_id', 'email_id');
    }
}