<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class ProductEntity extends \Eloquent {
	protected $fillable = [ 'product_id', 'attribute_id', 'attribute_value', 'attribute_value_note', 'status', 'is_variant' ];

	public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}