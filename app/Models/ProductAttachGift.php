<?php namespace App;

use Illuminate\Support\Facades\Cache;

class ProductAttachGift extends \Eloquent {
	protected $fillable = ["attach_gift_product_id", "status", "from_date", "to_date", "ref_product_id", "quantity"];
	public $timestamps = false;

	public static function rules($id = 0) {
        return [
            "from_date"     => 'required|date_format:Y-m-d H:i',
            "to_date"       => 'required|date_format:Y-m-d H:i|after:from_date',
            "product_ids"   => 'required',
        ];
    }
}