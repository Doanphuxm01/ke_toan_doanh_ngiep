<?php
namespace App\Models;

class FixedAssetDepreciation extends \Eloquent {
    protected $table = "fixed_asset_depreciation";
	protected $fillable = [ 
        'voucher_number', 
        'description',
        'month',
        'year',
        'status',
        'voucher_date', 
        'company_id',
        'created_by'
    ];

    public static function check($month, $year)
    {
       return FixedAssetDepreciation::where('month', $month)->where('year', $year)->get();
    }
    public static function getTotal($id)
    {   
        $allo_cost = FixedAssetAllocationCost::where('voucher_id', $id)->get();
        $result = 0;
        foreach ($allo_cost as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
    public static function amount($asset_code, $voucher_id)
    {   
        $result = 0;
        $costs = FixedAssetAllocationCost::where('asset_code', $asset_code)->where('voucher_id', $voucher_id)->get();
        foreach ($costs as $key => $value) {
           $result += $value->money_amount;
        }
        return $result;
    }
}