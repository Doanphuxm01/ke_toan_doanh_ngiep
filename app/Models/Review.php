<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class Review extends \Eloquent {
    protected $table = "product_reviews";
	protected $fillable = ['rating', 'status', 'comment', 'product_id', 'customer_id', 'reply', 'reply_by'];

    public static function boot()
    {
        parent::boot();

        static::updated(function($review)
        {
            self::clearCache($review);
        });

        static::deleted(function($review)
        {
            self::clearCache($review);
        });

        static::saved(function($review)
        {
            self::clearCache($review);
        });
    }

    public static function clearCache($review)
    {
        // update rating for product
        $product = Product::find($review->product_id);
        if (!is_null($product)) {
            $reviews = Review::where('status', 1)->where('product_id', $review->product_id)->get();
            $star1 = $star2 = $star3 = $star4 = $star5 = $average = 0; $total = count($reviews); if ($total == 0) $total = 1;
            if (count($reviews)) {
                foreach ($reviews as $review) {
                    switch ($review['rating']) {
                        case 1:
                            $star1++;
                            $average += 1;
                            break;
                        case 2:
                            $star2++;
                            $average += 2;
                            break;
                        case 3:
                            $star3++;
                            $average += 3;
                            break;
                        case 4:
                            $star4++;
                            $average += 4;
                            break;
                        case 5:
                            $star5++;
                            $average += 5;
                            break;
                    }
                }
                $average = $average/$total;
            }
            $product->rating = round($average);
            $product->save();
            $product->children()->update(['rating' => $product->rating]);
        }
        Cache::forget('reviews_by_product_id' . $review->product_id);
        Cache::forget('product_' . $review->product_id);
    }

    public static function getByProductId($productId)
    {
        $reviews = [];
        if (!Cache::has('reviews_by_product_id' . $productId)) {
            $reviews = Review::where('status', 1)->where('product_id', $productId)->orderBy('created_at', 'DESC')->select('rating', 'comment', 'customer_id', 'created_at')->get()->toArray();
            $reviews = json_encode($reviews);
            if ($reviews) Cache::forever('reviews_by_product_id' . $productId, $reviews);
        } else {
            $reviews = Cache::get('reviews_by_product_id' . $productId);
        }
        return json_decode($reviews, 1);
    }

    public function images()
    {
        return $this->morphMany(Media::class, 'mediable');
    }
}
