<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class Product extends \Eloquent {

    public static function rules($step = 1, $id = 0, $isUpdate = false) {
        switch ($step) {
            case Define\Product::STEP_1:
                return [
                    'category'              => 'required|integer',
                    'name'                  => 'required|max:255',
                    'sku'                   => 'required|max:100|unique:products,sku' . ($id == 0 ? '' : ',' . $id),
                    'image'                 => ($id == 0 ? 'required|' : '') . 'max:2048|mimes:jpg,jpeg,png,gif',
                    'description'           => 'required',
                    'seo_keywords'          => 'max:255',
                    // 'made_in'               => 'max:50',
                    'seo_description'       => 'max:255',
                    'brand'                 => 'required|integer',
                    'supplier'              => 'required|integer',
                    // 'ref_video'             => 'url|max:255|nullable',
                    'summary'               => 'max:10240',
                    'note_delivery'         => 'max:255',
                    'note_promotion'        => 'max:255',
                    // 'unit'                  => 'max:20',
                ];
            case Define\Product::STEP_2:
                return [
                ];
            case Define\Product::STEP_3:
                return [
                    // "sku"                   => $hasChild ? "required|array" : '',
                    // "sku.*"                 => $hasChild ? 'required|max:100|unique:products,sku' . ($id == 0 ? '' : ',' . $id), : '',
                    // "length"                => "required|array|min:1",
                    // "length.*"              => "required|min:1",
                    // "width"                 => "required|array|min:1",
                    // "width.*"               => "required|min:1",
                    // "height"                => "required|array|min:1",
                    // "height.*"              => "required|min:1",
                    // "weight"                => "required|array|min:1",
                    // "weight.*"              => "required|integer|min:1",
                    // "images"                => "array|min:1",
                    // "images.*"              => "min:1",
                    // "length"                => "array|min:1",
                    // "length.*"              => "min:1",
                    // "width"                 => "array|min:1",
                    // "width.*"               => "min:1",
                    // "height"                => "array|min:1",
                    // "height.*"              => "min:1",
                    // "weight"                => "array|min:1",
                    // "weight.*"              => "nullable|integer|min:1",
                    "price_original"        => "array|min:1",
                    "price_original.*"      => "nullable|integer|min:0",
                    "price_ipo"             => "array|min:1",
                    "price_ipo.*"           => "nullable|integer|min:0",
                    // "price_market"          => "required|array|min:1",
                    // "price_market.*"        => "required|integer|min:0",
                    "quantity"              => ($isUpdate ? "" : "array|min:1"),
                    "quantity.*"            => ($isUpdate ? "" : "nullable|integer|min:0"),
                ];
            case Define\Product::STEP_5:
                return [

                ];
        }
    }

	protected $fillable = [ 'name', 'product_category_id', 'made_in', 'view_counter', 'status', 'image', 'seo_keywords', 'seo_description', 'created_by', 'featured', 'other_interest', 'description', 'more_info', 'brand_id', 'best_sale', 'image', 'parent_id', 'sku', 'model', 'view', 'summary', 'product_supplier_id', 'ghn_supplier_district', 'new', 'ref_video', 'news_ref_ids', 'attach_gift_status', 'attach_gift_from_date', 'attach_gift_to_date', 'attach_gift_product_ids', 'price_market', 'vat', 'note_delivery', 'note_promotion', 'unit', 'slug', 'refreshed_at', 'product_ref_ids', 'url', 'type', 'bundle_product_id', 'info', 'seo_title', 'rating', 'like', 'bonus_coin' ];

    private static function increase_count( $id, $redis ) {

        //$redis = \App::make('redis');

        $redis->hincrby("product_counter_view_$id", 'counter_view_product', 1);
        $add_to_change_list = $redis->get("add_to_change_list_$id");
        if ( !$add_to_change_list || 1 ) {
            $redis->lpush("product_change_list", $id);
            $redis->setex("add_to_change_list_$id", \Config::get('products.write_count_cycle'), "change_after");
        }
    }

    public static function boot()
    {
        parent::boot();

        static::updated(function($product)
        {
            self::clearCache($product->id, $product->product_category_id);
        });

        static::created(function($product)
        {
            self::clearCache($product->id, $product->product_category_id);
        });

        static::deleted(function($product)
        {
            self::clearCache($product->id, $product->product_category_id);
        });

        static::saved(function($product)
        {
            self::clearCache($product->id, $product->product_category_id);
        });
    }

    public static function clearCache($productId = 0, $categoryId = 0)
    {
        \Illuminate\Support\Facades\Redis::flushall();
        //clear cache
        Cache::forget('best_sale_products');
        Cache::forget('new_products');
        Cache::forget('featured_products');
        Cache::forget('product_' . $productId);
        Cache::forget('product_children_' . $productId);
        Cache::forget('product_children_props_' . $productId);
        Cache::forget('product_images_' . $productId);
        Cache::forget('sale_products_' . $categoryId);
        Cache::forget('product_relation_' . $categoryId);
        Cache::forget('new_products_' . $categoryId);
        // del child
        $childIds = Product::where('parent_id', $productId)->pluck('id')->toArray();
        foreach ($childIds as $childId) {
            Cache::forget('product_' . $childId);
        }
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, "product_category_id");
    }

    public function calcRate()
    {
        return rand(0,5);
    }

    public function children()
    {
        return $this->hasMany("\App\Product", "parent_id");
    }

    public function properties()
    {
        return $this->hasMany(ProductProperty::class, "product_id");
    }

    public function entities()
    {
        return $this->hasMany("\App\ProductEntity");
    }

    public function orders()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function sales()
    {
        return $this->hasMany(SaleProduct::class);
    }

    public function images()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function brand()
    {
        return $this->belongsTo("\App\Brand", "brand_id");
    }

    public function parent()
    {
        return $this->belongsTo(Product::class, "parent_id");
    }

    public function supplier()
    {
        return $this->belongsTo("\App\ProductSupplier", "product_supplier_id");
    }

    public static function getBestSaleProducts()
    {
        $newProducts = [];
        if (!Cache::has('best_sale_products')) {
            $newProducts = Product::where('status', 1)->where('best_sale', 1)->where('parent_id', 0)->take(15)->orderBy('updated_at', 'DESC')->pluck('id', 'id')->toArray(); // ->where('new', 1)
            $newProducts = json_encode($newProducts);
            if ($newProducts) Cache::forever('best_sale_products', $newProducts);
        } else {
            $newProducts = Cache::get('best_sale_products');
        }
        return json_decode($newProducts, 1);
    }

    public static function getFeaturedProducts()
    {
        $newProducts = [];
        if (!Cache::has('featured_products')) {
            $newProducts = Product::where('status', 1)->where('featured', 1)->where('parent_id', 0)->take(15)->orderBy('updated_at', 'DESC')->pluck('id', 'id')->toArray(); // ->where('new', 1)
            $newProducts = json_encode($newProducts);
            if ($newProducts) Cache::forever('featured_products', $newProducts);
        } else {
            $newProducts = Cache::get('featured_products');
        }
        return json_decode($newProducts, 1);
    }

    public static function getNewProducts()
    {
        $newProducts = [];
        if (!Cache::has('new_products')) {
            $newProducts = Product::where('status', 1)->where('new', 1)->where('parent_id', 0)->take(15)->orderBy('updated_at', 'DESC')->pluck('id', 'id')->toArray(); // ->where('new', 1)
            $newProducts = json_encode($newProducts);
            if ($newProducts) Cache::forever('new_products', $newProducts);
        } else {
            $newProducts = Cache::get('new_products');
        }
        return json_decode($newProducts, 1);
    }

    public static function getById($id)
    {
        $product = [];
        if (1 || !Cache::has('product_' . $id)) {
            $product = Product::where('id', $id)->where('status', 1)->first();
            if (is_null($product)) return false;
            $rootProduct = null;
            $currentChildId = $newChildId = null;
            $searchChildIds = [-1 => $product->id];
            if ($product->parent_id) {
                $rootProduct = Product::where('id', $product->parent_id)->where('status', 1)->first();
                if (is_null($rootProduct)) return [];
                $searchChildIds += [0 => $rootProduct->id];
            } elseif(count($children = $product->children()->where('status', 1)->get())) {
                $searchChildIds += array_column($children->toArray(), 'id');
                $rootProduct    = $product;
                $product        = $children[0];
                $currentChildId = $newChildId = $product->id;
            } else {
                $rootProduct = $product;
            }
            $product = [
                'id'            => $rootProduct->id,
                'name'          => $rootProduct->name,
                'image'         => $rootProduct->image,
                'product_category_id' => $rootProduct->product_category_id,
                'sku'           => ($rootProduct->id == $product->id ? $rootProduct->sku : $product->sku),
                'price_ipo'     => intval($rootProduct->id == $product->id ? $rootProduct->price_ipo : $product->price_ipo),
                'price_original'=> ($rootProduct->id == $product->id ? $rootProduct->price_original : $product->price_original),
                'quantity'      => ($rootProduct->id == $product->id ? $rootProduct->quantity : $product->quantity),
                'view'          => $rootProduct->view,
                'best_sale'     => $rootProduct->best_sale,
                'featured'      => $rootProduct->featured,
                'new'           => $rootProduct->new,
                'rate'          => $rootProduct->rating,
                'like'          => $rootProduct->like,
                'buy'           => $rootProduct->buy,
                'weight'        => ($rootProduct->id == $product->id ? $rootProduct->weight : $product->weight),
                'length'        => ($rootProduct->id == $product->id ? $rootProduct->length : $product->length),
                'width'         => ($rootProduct->id == $product->id ? $rootProduct->width : $product->width),
                'height'        => ($rootProduct->id == $product->id ? $rootProduct->height : $product->height),
                'info'          => $rootProduct->info,
                'child_id'      => ($rootProduct->id == $product->id ? 0 : $product->id),
                'bonus_coin'    => $rootProduct->bonus_coin,
            ];
            $now = time();
            $pSales = SaleProduct::whereIn('product_id', $searchChildIds)->where('status', 1)->where('from_time', '<=', $now)->where('to_time', '>=', $now)->pluck("sale_id", "sale_id")->toArray();
            $sales = Sale::whereIn('id', $pSales)->where('status', 1)->where('from_time', '<=', $now)->where('to_time', '>=', $now)->get()->keyBy('id');
            $salesPrices = $sales->where('group', \App\Define\Sale::GROUP_DPRODUCT);
            $tmp = SaleProduct::whereIn('sale_id', array_column($salesPrices->toArray(), 'id'))->whereIn('product_id', $searchChildIds)->where('status', 1)->where('from_time', '<=', $now)->where('to_time', '>=', $now)->selectRaw("COALESCE(percent, 0) as percent, product_id, sale_id, root_product_id")->get();
            $maxProductSale = [];
            foreach ($tmp as $t) {
                if ($t->percent == 0) {
                    $t->percent = $sales[$t->sale_id]->discount_percent;
                }
                if ($children && $children->count() && $t->product_id == $t->root_product_id) {
                    // lay nhung thang con khac ma ko co trong danh sach khuyen mai
                    $childIdHasSale = array_column($tmp->toArray(), 'product_id', 'product_id');
                    foreach ($searchChildIds as $searchChildId) {
                        if (!isset($childIdHasSale[$searchChildId])) {
                            $t->product_id = $searchChildId;
                            break;
                        }
                    }
                    if ($t->product_id == $t->root_product_id) {
                        continue;
                    }
                }
                if (!isset($maxProductSale['percent'])) {
                    $maxProductSale = [
                        'percent'   => $t->percent,
                        'sale_id'   => $t->sale_id,
                        'product_id'=> $t->product_id,
                    ];
                    continue;
                }
                if ($maxProductSale['percent'] < $t->percent) {
                    $maxProductSale['percent']      = $t->percent;
                    $maxProductSale['sale_id']      = $t->sale_id;
                    $maxProductSale['product_id']   = $t->product_id;
                }
            }
            $data = [];
            foreach ($sales as $sale) {
                if ($sale->group == \App\Define\Sale::GROUP_GPRODUCT) {
                    if (isset($data[$sale->group])) { // qua tang ko biet so sanh ntn, nen giu lai cai dau tien
                        continue;
                    } else {
                        $data[$sale->group] = [
                            'sale_id'           => $sale->id,
                            'gift_product_ids'  => $sale->gift_product_ids,
                            'double_discount'    => 1,
                            'name'  => $sale->name,
                        ];
                    }
                } elseif ($sale->group == \App\Define\Sale::GROUP_DPRODUCT) {
                } elseif ($sale->group == \App\Define\Sale::GROUP_PFREESHIP) {
                    if (isset($data[$sale->group])) { // qua tang ko biet so sanh ntn, nen giu lai cai dau tien
                        continue;
                    } else {
                        $data[$sale->group] = [
                            'sale_id'           => $sale->id,
                            'double_discount'    => 1,
                        ];
                    }
                }
            }
            if (!empty($maxProductSale)) {
                $data[\App\Define\Sale::GROUP_DPRODUCT] = [
                    'sale_id'           => $maxProductSale['sale_id'],
                    'discount_percent'  => $maxProductSale['percent'],
                    'discount_amount'   => null,
                    'double_discount'    => 1,
                ];
                if (count($children)) {
                    $newProduct = $children->firstWhere('id', $maxProductSale['product_id']);
                    if (!is_null($newProduct)) {
                        $product['sku']           = ($rootProduct->id == $newProduct->id ? $rootProduct->sku : $newProduct->sku);
                        $product['price_ipo']     = intval($rootProduct->id == $newProduct->id ? $rootProduct->price_ipo : $newProduct->price_ipo);
                        $product['price_original']= ($rootProduct->id == $newProduct->id ? $rootProduct->price_original : $newProduct->price_original);
                        $product['quantity']      = ($rootProduct->id == $newProduct->id ? $rootProduct->quantity : $newProduct->quantity);
                        $product['weight']        = ($rootProduct->id == $newProduct->id ? $rootProduct->weight : $newProduct->weight);
                        $product['length']        = ($rootProduct->id == $newProduct->id ? $rootProduct->length : $newProduct->length);
                        $product['width']         = ($rootProduct->id == $newProduct->id ? $rootProduct->width : $newProduct->width);
                        $product['height']        = ($rootProduct->id == $newProduct->id ? $rootProduct->height : $newProduct->height);
                        $product['child_id']      = $newProduct->id;
                    }
                }
            }
            $product['sales'] = $data;
            $product = json_encode($product);
            if ($product) Cache::forever('product_' . $id, $product);
        } else {
            $product = Cache::get('product_' . $id);
        }

        return json_decode($product, 1);
    }

    public static function getChildrenProps($product)
    {
        $results = [];
        if (!Cache::has('product_children_' . $product->id)) {
            $product = Product::where('status', 1)->where('id', $product->id)->first();
            if (is_null($product)) return false;
            $children = $product->children()->where('status', 1)->get();
            if (!$children->count()) return false;
            $labels = [];
            $entities = $children[0]->entities()->where('is_variant', 0)->get();
            foreach($entities as $entity) {
                $labels[$entity->attribute_id] = $entity->attribute()->first()->name;
            }
            foreach ($labels as $key => $value) {
                $values = [];
                foreach($children as $child) {
                    $entity = $child->entities()->where('attribute_id', $key)->first();
                    if (isset($values[$entity->attribute_value]))
                        $values[$entity->attribute_value] .= ',' . $child->id;
                    else
                        $values[$entity->attribute_value] = $child->id;
                }
                array_push($results, [
                    'label'     => $value,
                    'values'    => array_flip($values)
                ]);
            }
            $results = json_encode($results);
            if ($results) Cache::put('product_children_' . $product->id, $results);
        } else {
            $results = Cache::get('product_children_' . $product->id);
        }
        return json_decode($results, 1);
    }

    public static function getChildrenPropsById($parentId)
    {
        $results = [];
        if (!Cache::has('product_children_props_' . $parentId)) {
            $product = Product::where('id', $parentId)->where('parent_id', 0)->first();//where('status', 1)->
            if (is_null($product))
                return [];
            $children = $product->children()->where('status', 1)->get();
            if (!$children->count())
                return [];

            $labels = [];
            $entities = $children[0]->entities()->where('is_variant', 0)->get();
            foreach($entities as $entity) {
                $labels[$entity->attribute_id] = $entity->attribute()->first()->name;
            }

            $keep = [];
            foreach ($entities as $entity) {
                foreach($children as $child) {
                    $e = $child->entities()->where('is_variant', 0)->pluck('attribute_value', 'attribute_id')->toArray();
                    if ($e[$entity->attribute_id] <> $entity->attribute_value) {
                        $keep[$entity->attribute_id] = $entity->attribute_id;
                    }
                }
            }
            foreach($children as $child) {
                $tmp = [];
                $entities = $child->entities()->where('is_variant', 0)->pluck('attribute_value', 'attribute_id')->toArray();
                foreach ($labels as $key => $value) {
                    if (!isset($keep[$key])) continue;
                    if (isset($entities[$key]))
                        $tmp[$value] = $entities[$key];
                }
                $results[$child->id] = $tmp;
            }
            $results = json_encode($results);
            if ($results) Cache::put('product_children_props_' . $parentId, $results);
        } else {
            $results = Cache::get('product_children_props_' . $parentId);
        }
        return json_decode($results, 1);
    }

    public static function getImagesById($id)
    {
        $images = [];
        if (!Cache::has('product_images_' . $id)) {
            $product = Product::where('status', 1)->where('id', $id)->first();
            if (is_null($product)) return false;
            $tmp = $product->images()->where('status', 1)->select('title')->orderBy('position')->get();
            foreach ($tmp as $image) {
                array_push($images, [
                    'thumb' => asset(config('upload.product') .  str_pad($product->id, 5, "0", STR_PAD_LEFT) . '/thumbs/' . $image->title),
                    'path'  => asset(config('upload.product') .  str_pad($product->id, 5, "0", STR_PAD_LEFT) . '/' . $image->title)
                ]);
            }
            if ($product->parent_id) {
                $parent = Product::where('status', 1)->where('id', $product->parent_id)->first();
                if (is_null($parent)) return [];
                $tmp = $parent->images()->where('status', 1)->select('title')->orderBy('position')->get();
                foreach ($tmp as $image) {
                    array_push($images, [
                        'thumb' => asset(config('upload.product') .  str_pad($product->parent_id, 5, "0", STR_PAD_LEFT) . '/thumbs/' . $image->title),
                        'path'  => asset(config('upload.product') .  str_pad($product->parent_id, 5, "0", STR_PAD_LEFT) . '/' . $image->title)
                    ]);
                }
            }
            $images = json_encode($images);
            if ($images) Cache::put('product_images_' . $id, $images);
        } else {
            $images = Cache::get('product_images_' . $id);
        }
        return json_decode($images, 1);
    }

    public static function searchProducts($categoryId, $childrenIds, $where, $orderBy)
    {
        $key = "product_search_$categoryId" . (isset($where['new']) ? '-new' : '') . (isset($where['best_sale']) ? '-best_sale' : '').
            (isset($where['featured']) ? '-featured' : '') . "-price_" . (isset($where['price']) ? $where['price'] : '0_0')  . "-brand_id_" . (isset($where['brand_id']) ? $where['brand_id'] : '0')  . "-product_supplier_id_" . (isset($where['product_supplier_id']) ? $where['product_supplier_id'] : '0') . '-' . current(array_keys($orderBy)) . "_" . current($orderBy);

        $productIds = [];
        if (1 || !Cache::has($key)) {
            $query = 'status=1';
            $searchChild = 0;
            if (isset($where['price'])) {
                $price = explode('.', $where['price']);
                if (isset($price[1])) {
                    $fromPrice = intval($price[0]);
                    $toPrice = intval($price[1]);
                    $query .= " AND price_ipo <= {$toPrice} AND price_ipo >= {$fromPrice}";
                    $searchChild = 1;
                }
            }

            if (isset($where['rating'])) {
                $query .= " AND rating = {$where['rating']}";
            }

            if (isset($where['brand_id'])) {
                $query .= " AND brand_id = {$where['brand_id']}";
            }
            if (isset($where['category'])) {
                // tim child cua danh muc con
                $tmp = ProductCategory::getChildrenLevelDown($where['category']);
                $tmp = array_column($tmp, 'name', 'id');
                if (count($tmp)) {
                    $tmp = array_keys($tmp);
                    $childIds = $where['category'] . ',' . implode(',', $tmp);
                } else {
                    $childIds = $where['category'];
                }
            } else {
                $childIds = $childrenIds;
            }
            if (isset($where['product_supplier_id'])) {
                $query .= " AND product_supplier_id = {$where['product_supplier_id']}";
            }

            // $query .= isset($where['new']) ? ' AND new=1' : '';
            $query .= isset($where['featured']) ? ' AND featured=1' : '';
            $query .= isset($where['best_sale']) ? ' AND best_sale=1' : '';
            // find by relative categories & the categories's children
            // $relativeProductIds = \DB::table('product_relative_categories')->whereRaw("product_category_id IN(" . $childIds . ")")->pluck('product_id')->toArray();
            // $relativeProductIds = (count($relativeProductIds) ? implode($relativeProductIds, ",") : -1);
            // $productIds = Product::whereRaw("(product_category_id IN(" . $childIds . ") OR id IN(" . $relativeProductIds . ")) AND " . $query)->orderBy(current(array_keys($orderBy)), current($orderBy))->pluck('id')->toArray();

            $queryForChildren = str_replace(" AND best_sale=1", "", $query);
            $queryForChildren = str_replace(" AND featured=1", "", $queryForChildren);
            if (isset($orderBy['price_ipo'])) {
                $productChildIds = Product::whereRaw(($childIds ? "product_category_id IN(" . $childIds . ")" : "1=1") . " AND " . $queryForChildren . " AND parent_id <> 0")->orderBy(current(array_keys($orderBy)), current($orderBy))->selectRaw("IFNULL(price_ipo, 0) as price_ipo, parent_id")->pluck('price_ipo', 'parent_id')->toArray();
                // tim cha
                $products = Product::where('status', 1)->where('parent_id', 0)->whereIn('id', array_keys($productChildIds))->pluck('id', 'id')->toArray();
                foreach ($products as $k => $v) {
                    $products[$k] = $productChildIds[$k];
                }
                if (count($products)) {
                    $productIds = Product::whereRaw("id NOT IN (". implode(',', array_keys($products)) .") AND " . ($childIds ? " product_category_id IN(" . $childIds . ")" : "1=1") . " AND " . $query . " AND parent_id = 0")->orderBy(current(array_keys($orderBy)), current($orderBy))->selectRaw("IFNULL(price_ipo, 0) as price_ipo, id")->pluck('price_ipo', 'id')->toArray();
                    $productIds += $products;
                } else {
                    $productIds = Product::whereRaw(($childIds ? "product_category_id IN(" . $childIds . ")" : "1=1") . " AND " . $query . " AND parent_id = 0")->orderBy(current(array_keys($orderBy)), current($orderBy))->selectRaw("IFNULL(price_ipo, 0) as price_ipo, id")->pluck('price_ipo', 'id')->toArray();
                }
                if ($orderBy['price_ipo'] == 'desc') {
                    arsort($productIds);
                } else {
                    asort($productIds);
                }
                if (!count($productIds)) return false;
                $productIds = array_keys($productIds);
            } else {
                $productIds = Product::whereRaw(($childIds ? "product_category_id IN(" . $childIds . ")" : "1=1") . " AND " . $query . " AND parent_id = 0")->orderBy(current(array_keys($orderBy)), current($orderBy))->pluck('id', 'id')->toArray();
                if ($searchChild) {
                    $productChildIds = Product::whereRaw(($childIds ? "product_category_id IN(" . $childIds . ")" : "1=1") . " AND " . $queryForChildren . " AND parent_id <> 0")->orderBy(current(array_keys($orderBy)), current($orderBy))->pluck('parent_id')->toArray();
                    // tim cha
                    $products = Product::where('status', 1)->where('parent_id', 0)->whereIn('id', $productChildIds)->pluck('id', 'id')->toArray();
                    $productIds += $products;
                }
                if (!count($productIds)) return false;
            }
            $productIds = json_encode($productIds);
            Cache::forever($key, $productIds);
        } else {
            $productIds = Cache::get($key);
        }

        return json_decode($productIds, 1);
    }

    public static function countView($id)
    {
        $redis = \App::make('redis');
        $redis->hincrby("wm_product_counter_view_$id", 'wm_counter_view_product', 1);
        $add_to_change_list = $redis->get("wm_add_to_change_list_$id");
        if ( !$add_to_change_list || 1 ) {
            $redis->lpush("wm_product_change_list", $id);
            $redis->setex("wm_add_to_change_list_$id", \App\Define\Product::WRITE_COUNT_CYCLE, "change_after");
        }
    }

    public static function updateCountView()
    {
        $redis = \App::make('redis');
        $updated = [];
        do {
            $product_id = $redis->lpop('wm_product_change_list');
            if ($product_id && !in_array($product_id, $updated)) {
                $product = $redis->hmget("wm_product_counter_view_$product_id", array('wm_counter_view_product'));
                if ($product) {
                    Product::where('id', $product_id )->increment('view', $product[0]);
                    $updated[] = $product_id;
                }
            }
        } while ($product_id);
        foreach ($updated as $product_id) {
            $redis->hset("wm_product_counter_view_$product_id", "wm_counter_view_product", 0);
        }
    }

    public static function getPriceByPartner($customerId, $productId)
    {
        $pricesPartner = [];
        if (1 || !Cache::tags(['prices', 'products'])->has('product_' . $productId . '_price_partner' . $customerId)) {
            // phai duoc admin set chiet khau cho partner, customer
            $product = Product::find($productId);
            if (is_null($product) || $product->status <> 1) return false;
            $price_ipo = $product->price_ipo;
            $discountAmount = [
                'price_ipo' => $price_ipo,
            ];

            $priceDetail = Price::where('product_id', $product->id)->where('dropship_amount', '>', 0)->where('status', 1)->first();
            $customer = Customer::find($customerId);
            if (is_null($priceDetail)) { // tim theo danh muc
                $cat1 = ProductCategory::where('status', 1)->where('id', $product->product_category_id)->first();
                if (is_null($cat1)) return false;
                $priceCat1 = Price::where('category_id', $cat1->id)->where('status', 1)->first(); // tìm theo danh muc cấp 3
                if (is_null($priceCat1)) {
                    if (!$cat1->product_category_id) return false;
                    $cat2 = ProductCategory::where('status', 1)->where('id', $cat1->product_category_id)->first();  // tìm theo danh muc cấp 2
                    if (is_null($cat2)) return false;
                    $priceCat2 = Price::where('category_id', $cat2->id)->where('status', 1)->first();
                    if (is_null($priceCat2)) {
                        if (!$cat2->product_category_id) return false;
                        $cat3 = ProductCategory::where('status', 1)->where('id', $cat2->product_category_id)->first();   // tìm theo danh muc cấp 1
                        if (is_null($cat3)) return false;
                        $priceCat3 = Price::where('category_id', $cat3->id)->where('status', 1)->first();
                        if (is_null($priceCat3)) return false; // chưa được set khuyến mãi cho partner
                        $discountAmount['customer_percent'] =  $priceCat3->customer_percent_default;
                        $discountAmount['partner_percent_id'] = $priceCat3->id;
                        if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                            $discountAmount['partner_percent'] = $priceCat3->dropship_percent;
                        } else {
                            $discountAmount['partner_percent'] = $priceCat3->partner_percent;
                        }
                    } else {
                        $discountAmount['customer_percent'] =  $priceCat2->customer_percent_default;
                        $discountAmount['partner_percent_id'] = $priceCat2->id;
                        if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                            $discountAmount['partner_percent'] = $priceCat2->dropship_percent;
                        } else {
                            $discountAmount['partner_percent'] = $priceCat2->partner_percent;
                        }
                    }
                } else {
                    $discountAmount['customer_percent'] = $priceCat1->customer_percent_default;
                    $discountAmount['partner_percent_id'] = $priceCat1->id;
                    if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                        $discountAmount['partner_percent'] = $priceCat1->dropship_percent;
                    } else {
                        $discountAmount['partner_percent'] = $priceCat1->partner_percent;
                    }
                }
            } else {
                $discountAmount['customer_amount'] = $priceDetail->customer_amount_default;
                $discountAmount['partner_amount_id'] = $priceDetail->id;
                if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                    $discountAmount['partner_amount'] = $priceDetail->dropship_amount;
                } else {
                    $discountAmount['partner_amount'] = $priceDetail->partner_amount;
                }
            }
            if (!isset($discountAmount['customer_percent']) && !isset($discountAmount['customer_amount'])) return false;
            // neu bi ngung khuyen mai cung ko cho ap ma
            if ((isset($discountAmount['customer_percent']) && $discountAmount['customer_percent'] == 0) || (isset($discountAmount['customer_amount']) && $discountAmount['customer_amount'] == 0)) return false;

            // kiem tra xem partner co set cho customer ko
            // $now = time();
            $partnerPrice = PartnerSale::getByCustomer($customerId);
            if (isset($partnerPrice['products'][$productId])) {
                if (isset($partnerPrice['products'][$productId]['percent'])) {
                    if (isset($discountAmount['customer_amount'])) {
                        unset($discountAmount['customer_amount']);
                        // unset($discountAmount['customer_amount_id']);
                    }
                    $discountAmount['customer_percent'] = $partnerPrice['products'][$productId]['percent']['value'];
                    $discountAmount['customer_limit']   = $partnerPrice['products'][$productId]['percent']['limit'];
                    $discountAmount['customer_percent_id'] = $partnerPrice['products'][$productId]['percent']['sale_id'];
                }
                if (isset($partnerPrice['products'][$productId]['amount'])) {
                    if (isset($discountAmount['customer_percent'])) {
                        // tính ra giá trị để so sánh
                        $ipoAfterCustomerPercent = (1 - $discountAmount['customer_percent']/100) * $price_ipo;
                        if ($ipoAfterCustomerPercent > ($price_ipo-$partnerPrice['products'][$productId]['amount']['value']) || $discountAmount['customer_limit'] < $partnerPrice['products'][$productId]['amount']['value']) {
                            $discountAmount['customer_amount'] = $partnerPrice['products'][$productId]['amount']['value'];
                            $discountAmount['customer_amount_id'] = $partnerPrice['products'][$productId]['amount']['sale_id'];
                            unset($discountAmount['customer_percent']);
                            unset($discountAmount['customer_percent_id']);
                            unset($discountAmount['customer_limit']);
                        }
                    } else {
                        $discountAmount['customer_amount'] = $partnerPrice['products'][$productId]['amount']['value'];
                        $discountAmount['customer_amount_id'] = $partnerPrice['products'][$productId]['amount']['sale_id'];
                    }
                }
            } else {
                // tim truc tiep category
                if (!isset($cat1)) { // toi uu
                    $cat1 = ProductCategory::where('status', 1)->where('id', $product->product_category_id)->first();
                    if (is_null($cat1)) return false;
                }
                if (isset($partnerPrice['categories'][$cat1->id])) {
                    if (isset($partnerPrice['categories'][$cat1->id]['percent'])) {
                        if (isset($discountAmount['customer_amount'])) unset($discountAmount['customer_amount']);
                        $discountAmount['customer_percent'] = $partnerPrice['categories'][$cat1->id]['percent']['value'];
                        $discountAmount['customer_limit']   = $partnerPrice['categories'][$cat1->id]['percent']['limit'];
                        $discountAmount['customer_percent_id'] = $partnerPrice['categories'][$cat1->id]['percent']['sale_id'];
                    } else {
                        if (isset($discountAmount['customer_percent'])) unset($discountAmount['customer_percent']);
                        $discountAmount['customer_amount'] = $partnerPrice['categories'][$cat1->id]['amount']['value'];
                        $discountAmount['customer_amount_id'] = $partnerPrice['categories'][$cat1->id]['amount']['sale_id'];
                    }
                } elseif ($cat1->product_category_id <> 0) {
                    if (!isset($cat2)) {
                        $cat2 = ProductCategory::where('status', 1)->where('id', $cat1->product_category_id)->first();  // tìm theo danh muc cấp 2
                        if (is_null($cat2)) return false;
                    }
                    if (isset($partnerPrice['categories'][$cat2->id])) {
                        if (isset($partnerPrice['categories'][$cat2->id]['percent'])) {
                            if (isset($discountAmount['customer_amount'])) unset($discountAmount['customer_amount']);
                            $discountAmount['customer_percent'] = $partnerPrice['categories'][$cat2->id]['percent']['value'];
                            $discountAmount['customer_limit']   = $partnerPrice['categories'][$cat2->id]['percent']['limit'];
                            $discountAmount['customer_percent_id'] = $partnerPrice['categories'][$cat2->id]['percent']['sale_id'];
                        } else {
                            if (isset($discountAmount['customer_percent'])) unset($discountAmount['customer_percent']);
                            $discountAmount['customer_amount'] = $partnerPrice['categories'][$cat2->id]['amount']['value'];
                            $discountAmount['customer_amount_id'] = $partnerPrice['categories'][$cat2->id]['amount']['sale_id'];
                        }
                    } elseif ($cat2->product_category_id <> 0) {
                        if (!isset($cat3)) {
                            $cat3 = ProductCategory::where('status', 1)->where('id', $cat2->product_category_id)->first();  // tìm theo danh muc cấp cao nhat 1
                            if (is_null($cat3)) return false;
                        }
                        if (isset($partnerPrice['categories'][$cat3->id])) {
                            if (isset($partnerPrice['categories'][$cat3->id]['percent'])) {
                                if (isset($discountAmount['customer_amount'])) unset($discountAmount['customer_amount']);
                                $discountAmount['customer_percent'] = $partnerPrice['categories'][$cat3->id]['percent']['value'];
                                $discountAmount['customer_limit']   = $partnerPrice['categories'][$cat3->id]['percent']['limit'];
                                $discountAmount['customer_percent_id'] = $partnerPrice['categories'][$cat3->id]['percent']['sale_id'];
                            } else {
                                if (isset($discountAmount['customer_percent'])) unset($discountAmount['customer_percent']);
                                $discountAmount['customer_amount'] = $partnerPrice['categories'][$cat3->id]['amount']['value'];
                                $discountAmount['customer_amount_id'] = $partnerPrice['categories'][$cat3->id]['amount']['sale_id'];
                            }
                        }
                    }
                }
            }
            // check ban ra co am ko
            $pAmount = 0;
            if (isset($discountAmount['partner_amount'])) {
                $pAmount = $discountAmount['partner_amount'];
            } else {
                $pAmount = \App\Helper\HString::roundAmountWithDiscountPercent($discountAmount['price_ipo'], $discountAmount['partner_percent']);
            }
            $cAmount = $discountAmount['price_ipo'];
            if (isset($discountAmount['customer_amount'])) {
                $cAmount = $discountAmount['customer_amount'];
            } else {
                $dAmount = $discountAmount['price_ipo'] - \App\Helper\HString::roundAmountWithDiscountPercent($discountAmount['price_ipo'], $discountAmount['customer_percent']);
                if (isset($discountAmount['customer_limit']) && $discountAmount['customer_limit'] && $dAmount > $discountAmount['customer_limit']) {
                    $dAmount = $discountAmount['customer_limit'];
                }
                $cAmount -= $dAmount;
            }
            if ($cAmount <= 0 || $pAmount <= 0 || ($cAmount - $pAmount) < 0) return false;
            $pricesPartner = json_encode($discountAmount);
            if ($pricesPartner) Cache::tags(['sale_products'])->forever('product_' . $productId . '_price_partner' . $customerId, $pricesPartner);
        } else {
            $pricesPartner = Cache::tags(['prices', 'products'])->get('product_' . $productId . '_price_partner' . $customerId);
        }

        return json_decode($pricesPartner, 1);
    }

    public static function getPriceForOnlyPartner($customer, $productId)
    {
        $product = Product::find($productId);
        if (is_null($product) || $product->status <> 1) return [];
        $price_ipo = $product->price_ipo;
        $discountAmount = [
            'price_ipo' => $price_ipo,
        ];
        $priceDetail = Price::where('product_id', $product->id)->where('status', 1)->first();
        if (is_null($priceDetail)) { // tim theo danh muc
            $cat1 = ProductCategory::where('status', 1)->where('id', $product->product_category_id)->first();
            if (is_null($cat1)) return [];
            $priceCat1 = Price::where('category_id', $cat1->id)->where('status', 1)->first(); // tìm theo danh muc cấp 3
            if (is_null($priceCat1)) {
                if (!$cat1->product_category_id) return [];
                $cat2 = ProductCategory::where('status', 1)->where('id', $cat1->product_category_id)->first();  // tìm theo danh muc cấp 2
                if (is_null($cat2)) return [];
                $priceCat2 = Price::where('category_id', $cat2->id)->where('status', 1)->first();
                if (is_null($priceCat2)) {
                    if (!$cat2->product_category_id) return [];
                    $cat3 = ProductCategory::where('status', 1)->where('id', $cat2->product_category_id)->first();   // tìm theo danh muc cấp 1
                    if (is_null($cat3)) return [];
                    $priceCat3 = Price::where('category_id', $cat3->id)->where('status', 1)->first();
                    if (is_null($priceCat3)) return []; // chưa được set khuyến mãi cho partner
                    $discountAmount['customer_percent'] =  $priceCat3->customer_percent_default;
                    $discountAmount['partner_percent_id'] = $priceCat3->id;
                    if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                        $discountAmount['partner_percent'] = $priceCat3->dropship_percent;
                    } else {
                        $discountAmount['partner_percent'] = $priceCat3->partner_percent;
                    }

                } else {
                    $discountAmount['customer_percent'] =  $priceCat2->customer_percent_default;
                    $discountAmount['partner_percent_id'] = $priceCat2->id;
                    if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                        $discountAmount['partner_percent'] = $priceCat2->dropship_percent;
                    } else {
                        $discountAmount['partner_percent'] = $priceCat2->partner_percent;
                    }
                }
            } else {
                $discountAmount['customer_percent'] = $priceCat1->customer_percent_default;
                $discountAmount['partner_percent_id'] = $priceCat1->id;
                if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                    $discountAmount['partner_percent'] = $priceCat1->dropship_percent;
                } else {
                    $discountAmount['partner_percent'] = $priceCat1->partner_percent;
                }
            }
        } else {
            $discountAmount['customer_amount'] = $priceDetail->customer_amount_default;
            $discountAmount['partner_amount_id'] = $priceDetail->id;
            if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                $discountAmount['partner_amount'] = $priceDetail->dropship_amount;
            } else {
                $discountAmount['partner_amount'] = $priceDetail->partner_amount;
            }
        }
        if (!isset($discountAmount['customer_percent']) && !isset($discountAmount['customer_amount'])) return [];
        return $discountAmount;
    }

    public static function getPriceCategoryForOnlyPartner($customer, $category)
    {
        $discountAmount = [];
        $priceDetail = Price::where('category_id', $category->id)->where('status', 1)->first();
        if (is_null($priceDetail)) { // tim theo danh muc
            $cat1 = ProductCategory::where('status', 1)->where('id', $category->product_category_id)->first();
            if (is_null($cat1)) return [];
            $priceCat1 = Price::where('category_id', $cat1->id)->where('status', 1)->first(); // tìm theo danh muc cấp 3
            if (is_null($priceCat1)) {
                if (!$cat1->product_category_id) return [];
                $cat2 = ProductCategory::where('status', 1)->where('id', $cat1->product_category_id)->first();  // tìm theo danh muc cấp 2
                if (is_null($cat2)) return [];
                $priceCat2 = Price::where('category_id', $cat2->id)->where('status', 1)->first();
                if (is_null($priceCat2)) return [];
                if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                    $discountAmount['partner_percent'] = $priceCat2->dropship_percent;
                } else {
                    $discountAmount['partner_percent'] = $priceCat2->partner_percent;
                }
            } else {
                if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                    $discountAmount['partner_percent'] = $priceCat1->dropship_percent;
                } else {
                    $discountAmount['partner_percent'] = $priceCat1->partner_percent;
                }
            }
        } else {
            if ($customer->level == \App\Define\Customer::LEVEL_DROPSHIP) {
                $discountAmount['partner_percent'] = $priceDetail->dropship_percent;
            } else {
                $discountAmount['partner_percent'] = $priceDetail->partner_percent;
            }
        }
        return $discountAmount;
    }

    public static function getPricePartnerByCategory($product)
    {
        $discountAmount = [];
        $cat1 = ProductCategory::where('status', 1)->where('id', $product->product_category_id)->first();
        if (is_null($cat1)) return [];
        $priceCat1 = Price::where('category_id', $cat1->id)->where('status', 1)->first(); // tìm theo danh muc cấp 3
        if (is_null($priceCat1)) {
            if (!$cat1->product_category_id) return [];
            $cat2 = ProductCategory::where('status', 1)->where('id', $cat1->product_category_id)->first();  // tìm theo danh muc cấp 2
            if (is_null($cat2)) return [];
            $priceCat2 = Price::where('category_id', $cat2->id)->where('status', 1)->first();
            if (is_null($priceCat2)) {
                if (!$cat2->product_category_id) return [];
                $cat3 = ProductCategory::where('status', 1)->where('id', $cat2->product_category_id)->first();   // tìm theo danh muc cấp 1
                if (is_null($cat3)) return [];
                $priceCat3 = Price::where('category_id', $cat3->id)->where('status', 1)->first();
                if (is_null($priceCat3)) return []; // chưa được set khuyến mãi cho partner
                $discountAmount['customer_percent'] =  $priceCat3->customer_percent_default;
                $discountAmount['dropship_percent'] = $priceCat3->dropship_percent;
                $discountAmount['partner_percent'] = $priceCat3->partner_percent;
            } else {
                $discountAmount['customer_percent'] =  $priceCat2->customer_percent_default;
                $discountAmount['dropship_percent'] = $priceCat2->dropship_percent;
                $discountAmount['partner_percent'] = $priceCat2->partner_percent;
            }
        } else {
            $discountAmount['customer_percent'] = $priceCat1->customer_percent_default;
            $discountAmount['dropship_percent'] = $priceCat1->dropship_percent;
            $discountAmount['partner_percent'] = $priceCat1->partner_percent;
        }

        return $discountAmount;
    }

    public static function relationProducts($product)
    {
        $relationProducts = [];
        if (!Cache::has('product_relation_' . $product->id)) {
            $category = $product->category()->first();
            if (is_null($category)) return [];
            $childIds = $category->getChildrenIds();
            if (!$childIds) $childIds = "-1";
            $relationProducts = Product::where('parent_id', 0)->where('status', 1)->where('id', '<>', $product->id)->whereRaw("product_category_id IN(" . $childIds . ")")->orderBy('updated_at', 'DESC')->take(6)->pluck('id')->toArray();
            $relationProducts = json_encode($relationProducts);
            if ($relationProducts) Cache::forever('product_relation_' . $product->id, $relationProducts);
        } else {
            $relationProducts = Cache::get('product_relation_' . $product->id);
        }
        return json_decode($relationProducts, 1);
    }
}
