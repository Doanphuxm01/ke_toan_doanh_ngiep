<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends \Eloquent {
    protected $dates = ['deleted_at'];
    protected $fillable = ['product_id', 'quantity', 'note', 'action_at', 'created_by', 'inventory_category_id', 'province_id', 'deleted_by'];

    public function logs()
    {
        return $this->hasMany(InventoryLog::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function category()
    {
        return $this->belongsTo(InventoryCategory::class, 'inventory_category_id');
    }
}
