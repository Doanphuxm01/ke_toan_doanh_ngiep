<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class News extends \Eloquent {

    public static function rules($id = 0) {

        return [
            'title'                 => 'required|max:255',
            'summary'               => 'required|max:500',
            'content'               => 'required',
            'image'                 => ($id == 0 ? 'required|' : '') . 'max:2048|mimes:jpg,jpeg,png,gif',
            'seo_keywords'          => 'max:255',
            'seo_description'       => 'max:1024',
            'created_by'            => 'integer',
            'category'              => 'required|integer',
        ];

    }

	protected $fillable = [ 'title', 'summary', 'content', 'view_counter', 'status', 'image', 'seo_keywords', 'seo_description', 'created_by', 'featured', 'category_id' ];

    public function category()
    {
        return $this->belongsTo(NewsCategory::class);
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($news)
        {
            self::clearCache();
        });

        static::updated(function($news)
        {
            self::clearCache();
        });

        static::deleted(function($news)
        {
            self::clearCache();
        });

        static::saved(function($news)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('home_news');
        // Cache::forget('recent_news');
    }

    public static function getHomeNews()
    {

        $homeNews = [];
        if (!Cache::has('home_news')) {
            $homeNews = NewsCategory::where('status', 1)->where('show_homepage', 1)->orderBy('id')->select('name', 'id')->get()->toArray();
            if (0 == count($homeNews)) return [];
            for ($i=0; $i < count($homeNews); $i++) {
                $homeNews[$i]['news'] = News::where('status', 1)->where('featured', 1)->where('category_id', $homeNews[$i]['id'])->orderBy('id')->take(5)->pluck('title', 'id')->toArray();
            }
            $homeNews = json_encode($homeNews);
            if ($homeNews) Cache::forever('home_news', $homeNews);
        } else {
            $homeNews = Cache::get('home_news');
        }
        return json_decode($homeNews, 1);
    }

    // public static function getRecentNews()
    // {
    //     $recentNews = [];
    //     if (!Cache::has('recent_news')) {
    //         $categories = NewsCategory::where('status', 1)->pluck('name', 'id')->toArray();
    //         if (0 == count($categories)) return [];
    //         $homeNews = self::getHomeNews();
    //         $recentNews = News::where('status', 1)->where('featured', 1)->whereIn('category_id', array_keys($categories))->whereNotIn('id', array_keys($homeNews))->orderBy('updated_at', "DESC")->take(9)->select('title', 'id', 'image', 'summary')->get()->toArray();
    //         $recentNews = json_encode($recentNews);
    //         if ($recentNews) Cache::forever('recent_news', $recentNews);
    //     } else {
    //         $recentNews = Cache::get('recent_news');
    //     }
    //     return json_decode($recentNews, 1);
    // }
}