<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class BannerAdvertisement extends \Eloquent {

    public static function rules($id = 0) {

        return [
            'name'  => 'required|max:50',
            'href'  => 'nullable|url|max:255',
            'image' => ($id == 0 ? 'required|' : '') . 'max:4096|mimes:jpg,jpeg,png,gif',
            'type'  => 'required|in:' . implode(',', Define\Constant::getBannerTypes()),
        ];
    }

    protected $fillable = [ 'name', 'href', 'created_by', 'type', 'image', 'status' ];

    public static function boot()
    {
        parent::boot();

        static::updated(function($page)
        {
            self::clearCache();
        });

        static::deleted(function($page)
        {
            self::clearCache();
        });

        static::saved(function($page)
        {
            self::clearCache();
        });

        static::created(function($page)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('banner_advertisements');
    }

    public static function getBannerAdvertisements()
    {
        $banners = [];
        if (!Cache::has('banner_advertisements')) {
            $types = Define\Constant::getBannerTypes();
            foreach ($types as $type) {
                $banners[$type] = BannerAdvertisement::where('status', 1)->where('type', $type)->take(2)->select('image', 'href', 'name')->orderBy('updated_at', 'DESC')->get()->toArray();
            }
            $banners = json_encode($banners);
            Cache::forever('banner_advertisements', $banners);
        } else {
            $banners = Cache::get('banner_advertisements');
        }

        return json_decode($banners, 1);
    }
}