<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Supplier extends Authenticatable {

    use SoftDeletes;
    protected $fillable = ['code', 'name', 'status', 'phone', 'address', 'deleted_by', 'description', 'email'];

    public static function rules($id = 0) {
        return [
            'name'  	=> 'required|max:255|unique:suppliers,name' . ($id == 0 ? '|required' : ',' . $id),
            'description'	=> 'max:255',
            'phone'     => 'nullable|regex:/[0]\d{9,10}$/iD',
            'email'     => 'nullable|email|max:100|min:6',
            'address'   => 'nullable|max:255',

        ];
    }

    public function containers() {
        return $this->hasMany(Container::class, 'supplier_id');
    }
}
