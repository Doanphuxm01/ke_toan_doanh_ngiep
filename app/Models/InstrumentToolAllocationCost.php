<?php
namespace App\Models;

class InstrumentToolAllocationCost extends \Eloquent {
    protected $table = "instrument_tool_allocation_cost";
	protected $fillable = [ 
        'voucher_id', 
        'expenses_prepaid_id', 
        'department_id', 
        'allocation_rate',
        'allocation_quantity',
        'money_amount', 
        'expense_account', 
        'cost_item', 
    ];
    public static function getValue($instrument_tool_id, $voucher_id)
    {   
        $costs = InstrumentToolAllocationCost::where('instrument_tool_id', $instrument_tool_id)->where('voucher_id', $voucher_id)->get();
        foreach ($costs as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
}