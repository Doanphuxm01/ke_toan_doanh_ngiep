<?php
namespace App\Models;

class InstrumentToolAllocation extends \Eloquent {
    protected $table = "instrument_tool_allocation";
	protected $fillable = [ 
        'voucher_no', 
        'description',
        'month',
        'year',
        'status',
        'voucher_date', 
        'company_id',
        'created_by'
    ];

    public static function check($month, $year)
    {
        $result = FixedAssetDepreciation::where('month', $month)->where('year', $year)->get();
        return $result;
    }
    public static function getTotal($id)
    {   
        $allo_cost = InstrumentToolAllocationCost::where('voucher_id', $id)->get();
        $result = 0;
        foreach ($allo_cost as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
    public static function amount($instrument_tool_id, $voucher_id)
    {   
        $result = 0;
        $costs = InstrumentToolAllocationCost::where('instrument_tool_id', $instrument_tool_id)->where('voucher_id', $voucher_id)->get();
        foreach ($costs as $key => $value) {
           $result += $value->money_amount;
        }
        return $result;
    }
    public static function getAllocationInfo($instrument_tool_id)
    {
        $result = InstrumentToolAllocation::where('instrument_tool_id', $instrument_tool_id)->get();
        return $result;
    }
    public static function getAllocationCost($instrument_tool_id, $voucher_id)
    {
        $result = InstrumentToolAllocationCost::where('instrument_tool_id', $instrument_tool_id)->where('voucher_id', $voucher_id)->get();
        return $result;
    }
    public static function getResidualValue($voucher_id, $instrument_tool_id)
    {
        $allocation_voucher = InstrumentToolAllocation::where("id", $voucher_id)->first();
        $instrument_tool = InstrumentTool::find($instrument_tool_id);
        $disposal_voucher = InstrumentToolDisposal::whereMonth('voucher_date', $allocation_voucher->month)
                                                    ->whereYear('voucher_date', $allocation_voucher->year)
                                                    ->where('instrument_tool_disposal.instrument_tool_id', $instrument_tool_id)
                                                    ->join('instrument_tool_disposal_details', 'instrument_tool_disposal.id', '=', 'instrument_tool_disposal_details.disposal_voucher_id')
                                                    ->get();
        $residual_value = 0;
        foreach ($disposal_voucher as $key => $value) {
            $residual_value += $value->disposal_quantity * $instrument_tool->unit_price;
        }
       
        return $residual_value;
    }
    
}