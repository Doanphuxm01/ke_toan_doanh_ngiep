<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{   
    public $timestamps = false;
    protected $table = "jobs";
    protected $fillable = ['code', 'branch_code', 'type', 'month', 'year', 'company_id', 'group', 'created_by', 'status'];
    
    public static function rules($id = 0, $code = 0)
    {
        return [
            'code'          => 'required|max:30|',
            'branch_code'   => 'required',
            'type'   => 'required',
            'month' => 'required|date_format:m',
            'year'  => 'required|date_format:Y',
            'month' => 'required',
            'group' => 'required',
        ];
    }

    public static function getJob() 
    {
        return  Job::where('status', 1)->where('company_id', session('current_company'))->pluck('code','code')->toArray();
    }

	public static function getJobsForOption($companyId = null)
	{
		$companyId = $companyId ?? session('current_company');
		return  Job::where('status', 1)->where('company_id', $companyId )->pluck('code','id')->toArray();
	}
}
