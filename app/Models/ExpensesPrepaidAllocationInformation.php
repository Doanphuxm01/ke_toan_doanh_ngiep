<?php
namespace App\Models;

class ExpensesPrepaidAllocationInformation extends \Eloquent {
    protected $table = "expenses_prepaid_allocation_information";
	protected $fillable = [ 
        'department_id', 
        'expenses_prepaid_id', 
        'allocation_rate',
        'expense_account', 
        'cost_item', 
    ];
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }
}