<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerSale extends \Eloquent {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $fillable = ["name", "from_date", "to_date", "status", "customer_id", "discount_percent", "discount_amount", "discount_limit", "from_time", "to_time", "type", "deleted_by"];

    public static function boot()
    {
        parent::boot();

        static::updated(function($sale)
        {
            self::clearCache();
        });

        static::deleted(function($sale)
        {
            self::clearCache();
        });

        static::created(function($sale)
        {
            self::clearCache();
        });

        static::saved(function($sale)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {

    }

    public function objects()
    {
        return $this->hasMany(PartnerSaleProduct::class);
    }

    public static function getByCustomer($customerId)
    {
        $pAllCats = ProductCategory::where('status', 1)->pluck('id')->toArray();
        /*
            return
            product_id => [
                'percent' => [
                    'sale_id'
                    'value'
                    'litmit'
                ],
                'amount' => [
                    'sale_id'
                    'value'
                ]
            ]
            category_id => [
                'percent' => [
                    'sale_id'
                    'value'
                    'litmit'
                ],
                'amount' => [
                    'sale_id'
                    'value'
                ]
            ]
        */
        $now = time();
        $products = $categories = [];
        $pSales = PartnerSale::whereRaw("customer_id = {$customerId} AND status=1 AND ((from_time <= {$now} AND to_time >= {$now}) OR (from_time IS NULL AND to_time IS NULL))")->get();
        foreach ($pSales as $pSale) {
            if (in_array($pSale->type, [\App\Define\Sale::PARTNER_FILTER_INCLUDE_PRO, \App\Define\Sale::PARTNER_FILTER_EXCLUDE_PRO])) {
                $pSaleProducts = PartnerSaleProduct::where('partner_sale_id', $pSale->id)->pluck('object_id')->toArray();
                if (count($pSaleProducts) == 0) continue;
                foreach ($pSaleProducts as $productId) {
                    if ($pSale->discount_percent) {
                        if (!isset($products[$productId]['percent']['value']) || $products[$productId]['percent']['value'] < $pSale->discount_percent) {
                            $products[$productId]['percent']['value']     = $pSale->discount_percent;
                            $products[$productId]['percent']['sale_id']   = $pSale->id;
                            $products[$productId]['percent']['limit']     = $pSale->discount_limit;
                        }
                    } else {
                        if (!isset($products[$productId]['amount']['value']) || $products[$productId]['amount']['value'] < $pSale->discount_amount) {
                            $products[$productId]['amount']['value']     = $pSale->discount_amount;
                            $products[$productId]['amount']['sale_id']   = $pSale->id;
                        }
                    }
                }
            } elseif (in_array($pSale->type, [\App\Define\Sale::PARTNER_FILTER_INCLUDE_CAT, \App\Define\Sale::PARTNER_FILTER_EXCLUDE_CAT])) {
                $pSaleCategories = PartnerSaleProduct::where('partner_sale_id', $pSale->id)->pluck('object_id')->toArray();
                if (count($pSaleCategories) == 0) continue;
                foreach ($pSaleCategories as $categoryId) {
                    if ($pSale->discount_percent) {
                        if (!isset($categories[$categoryId]['percent']['value']) || $categories[$categoryId]['percent']['value'] < $pSale->discount_percent) {
                            $categories[$categoryId]['percent']['value']     = $pSale->discount_percent;
                            $categories[$categoryId]['percent']['sale_id']   = $pSale->id;
                            $categories[$categoryId]['percent']['limit']    = $pSale->discount_limit;
                        }
                    } else {
                        if (!isset($categories[$categoryId]['amount']['value']) || $categories[$categoryId]['amount']['value'] < $pSale->discount_amount) {
                            $categories[$categoryId]['amount']['value']     = $pSale->discount_amount;
                            $categories[$categoryId]['amount']['sale_id']   = $pSale->id;
                        }
                    }
                }
            } else {
                foreach ($pAllCats as $pCatId) {
                    if ($pSale->discount_percent) {
                        if (!isset($categories[$pCatId]['percent']['value']) || $categories[$pCatId]['percent']['value'] < $pSale->discount_percent) {
                            $categories[$pCatId]['percent']['value']     = $pSale->discount_percent;
                            $categories[$pCatId]['percent']['sale_id']   = $pSale->id;
                            $categories[$pCatId]['percent']['limit']    = $pSale->discount_limit;
                        }
                    } else {
                        if (!isset($categories[$pCatId]['amount']['value']) || $categories[$pCatId]['amount']['value'] < $pSale->discount_amount) {
                            $categories[$pCatId]['amount']['value']     = $pSale->discount_amount;
                            $categories[$pCatId]['amount']['sale_id']   = $pSale->id;
                        }
                    }
                }
            }
        }
        return [
            'products'      => $products,
            'categories'    => $categories,
        ];
    }
}