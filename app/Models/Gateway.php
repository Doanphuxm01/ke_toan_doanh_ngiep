<?php

namespace App;

class Gateway extends \Eloquent {
    public static function rules( $id = 0 ) {

        return [
            'name'                      => 'required|max:100',
            'logo'                      => 'max:2048|mimes:jpg,jpeg,png,gif' . ($id == 0 ? '|required': ''),
            'fee_internal_fixed'        => 'required|integer',
            'fee_internal_percent'      => 'required|between:0,99.99',
            'fee_external_fixed'        => 'required|integer',
            'fee_external_percent'      => 'required|between:0,99.99',
            'merchant_id'               => 'max:50',
            'key_send'                  => 'max:255',
            'key_recieve'               => 'max:255',
            'url'                       => 'required|max:255',
            'issuer_id'                 => 'max:50',
            'status'                    => 'integer',
        ];
    }

	protected $fillable = [ 'name', 'logo', 'fee_internal_fixed', 'fee_internal_percent', 'fee_external_fixed',
        'fee_external_percent', 'merchant_id', 'key_send', 'key_recieve', 'url', 'issuer_id', 'status' ];

    public function banks()
    {
        return $this->hasMany('\App\Bank');
    }

    public static function getByAll($del = 0, $time = 86400) //in 24h = 3600*24=86400)
    {
        $redis      = \App::make('redis');
        if( $del ) $redis->del("gateways_by_all");
        $gateways = $redis->get("gateways_by_all");

        if ( !$gateways ) {
            $gateways = Gateway::where('status', 1)->lists('name', 'id');
            #$gateways = $gateways->getDictionary();
            $redis->set( "gateways_by_all", json_encode($gateways) );
            $redis->expire( "gateways_by_all", $time);
            $gateways = json_encode($gateways);
        }

        $gateways = json_decode( $gateways, 1 );
        return $gateways;
    }
}