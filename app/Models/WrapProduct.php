<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class WrapProduct extends \Eloquent {

    public static function rules($id = 0) {
        return [
            "name"          => 'required|max:255',
            // "url"           => 'required|max:255|unique:wrap_products,url' . ($id == 0 ? '' : ',' . $id),
            "product_ids"   => 'required',
            'summary'       => 'max:10240',
            'description'   => 'required',
            // 'other_interest',
            // 'more_info',
            // 'featured',
            // 'best_sale',
            // 'new',
            // 'view',
            'image'             => ($id == 0 ? 'required|' : '') . 'max:2048|mimes:jpg,jpeg,png,gif',
            'seo_keywords'      => 'max:255',
            'seo_description'   => 'max:255',
        ];
    }

	protected $fillable = ["name", "url", "created_by", "status", "product_ids", 'summary', 'description', 'other_interest', 'more_info', 'featured', 'best_sale', 'new', 'view', 'image', 'seo_keywords', 'seo_description'];

    public static function boot()
    {
        parent::boot();

        static::updated(function($wrapProduct)
        {
            self::clearCache($wrapProduct);
        });

        static::deleted(function($wrapProduct)
        {
            //\Log::info('deleted');
            self::clearCache();
        });

        static::created(function($wrapProduct)
        {
            //\Log::info('created');
            self::clearCache($wrapProduct);
        });

        static::saved(function($wrapProduct)
        {
            //\Log::info('saved');
            self::clearCache($wrapProduct);
        });
    }

    public static function clearCache($wrapProduct = null)
    {
        //clear cache
        // if (!is_null($wrapProduct)) Cache::forget('promotions_category_' . $wrapProduct->category_id);
        // Cache::forget('promotions_all');
        // Cache::forget('promotion_products');
        // Cache::tags('promotion_products')->flush();
    }

    // public function category()
    // {
    //     return $this->belongsTo("\App\ProductCategory", "category_id");
    // }

    // public function children()
    // {
    //     return $this->hasMany("\App\Product", "parent_id");
    // }

    // public function products()
    // {
    //     return $this->belongsToMany("\App\Product", 'promotion_products', 'promotion_id', 'product_id');
    // }

    // public function orders()
    // {
    //     return $this->hasMany("\App\OrderDetail", 'promotion_id');
    // }

    // public static function getPromotions()
    // {
    //     $promotions = [];
    //     # calc minute auto clean cache
    //     if (!Cache::has('promotions_all')) {
    //         $promotions = Promotion::where('status', 1)->where("from_date", "<=", date("Y-m-d H:i"))->where("to_date", ">=", date("Y-m-d H:i"))->select('name', 'id', 'image', 'percent', 'from_date', 'to_date')->orderBy('updated_at', 'DESC')->get();
    //         $promotions = json_encode($promotions);
    //         Cache::put('promotions_all', $promotions, Define\Constant::SHORT_TTL);//
    //     } else {
    //         $promotions = Cache::get('promotions_all');
    //     }

    //     return json_decode($promotions, 1);
    // }

    // public static function getPromotionsByCategory($categoryId)
    // {
    //     $promotions = [];
    //     # calc minute auto clean cache
    //     if (!Cache::has('promotions_category_' . $categoryId)) {
    //         $promotions = Promotion::where('status', 1)->where('category_id', $categoryId)->where("from_date", "<=", date("Y-m-d H:i"))->where("to_date", ">=", date("Y-m-d H:i"))->select('name', 'id', 'image', 'percent', 'from_date', 'to_date')->orderBy('updated_at', 'DESC')->get();
    //         $promotions = json_encode($promotions);
    //         Cache::put('promotions_category_' . $categoryId, $promotions, Define\Constant::SHORT_TTL);//
    //     } else {
    //         $promotions = Cache::get('promotions_category_' . $categoryId);
    //     }

    //     return json_decode($promotions, 1);
    // }
}
