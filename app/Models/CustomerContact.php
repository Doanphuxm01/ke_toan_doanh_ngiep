<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class CustomerContact extends \Eloquent {
    protected $fillable = [ 'fullname', 'phone', 'address', 'district', 'province', 'is_default', 'customer_id', 'ward', 'ward_code'];
}