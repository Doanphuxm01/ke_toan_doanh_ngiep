<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';

    public static function getCurrency()
    {
        return Currency::all()->pluck('name','id')->toArray();
    }
    public static function getName($id)
    {
        return Currency::where('id', $id)->pluck('name')->first();
    }
}
