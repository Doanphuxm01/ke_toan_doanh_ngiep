<?php

namespace App\Models;

class InstrumentTool extends \Eloquent {
    protected $fillable = ['code', 'name', 'voucher_date', 'voucher_no', 'type_id', 'currency_id', 'reason', 'unit_price', 'quantity', 'period_number', 'periodical_allocation_value', 'residual_time', 'status', 'allocation_account', 'company_id', 'created_by'];

    public static function getName($id){
        return InstrumentTool::find($id)->name;
    }
    public function instrumentToolAllocation()
    {
        return $this->hasMany(InstrumentToolAllocationInformation::class, 'instrument_tool_id', 'id');
    }
}
