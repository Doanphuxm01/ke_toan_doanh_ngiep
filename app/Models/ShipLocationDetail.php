<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class ShipLocationDetail extends \Eloquent {
	protected $fillable = ['ship_location_id', 'province_id', 'district_id', 'fee', 'status'];
}