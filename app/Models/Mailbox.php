<?php
namespace App;

class Mailbox extends \Eloquent {
    public static function rules($id = 0) {
        return [
        	"subject"       => 'required|max:255',
            "content"       => 'required',
            "ref"           => 'integer|min:1|exists:feedback,id|nullable',
            "address"       => 'email|nullable|required_without:ref',
        ];
    }

    protected $fillable = ['address', 'subject', 'content', 'created_by', 'status', 'feedback_id'];

    public function feedback()
    {
        return $this->belongsTo('App\FeedBack');
    }
}