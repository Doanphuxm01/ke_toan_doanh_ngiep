<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryCategory extends \Eloquent {

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'code', 'phone', 'address', 'note', 'deleted_by', 'status', 'created_by', 'province_id'];

    public static function rules($id=0) {
        return [
            'name'      => 'required|max:255',
            'code'      => 'required|max:20|regex:/^[A-Za-z][A-Za-z0-9]{2,20}$/|unique:inventory_categories,code' . ($id ? ',' . $id : ''),
            'phone'     => 'required|max:11|regex:/[0]\d{9,10}$/',
            'province'  => 'required',
            'address'       => 'required|max:255',
            'note'          => 'max:255',
        ];
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }
}
