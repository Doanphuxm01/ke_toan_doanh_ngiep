<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class NganLuongError extends \Eloquent {

    protected $table = "nganluong_errors";

    public static function getByAll()
    {
        $errors = [];
        if (!Cache::has('nganluong_errors_all')) {
            $errors = NganLuongError::pluck('des_vi', 'error_code')->toArray();
            $errors = json_encode($errors);
            if ($errors) Cache::forever('nganluong_errors_all', $errors);
        } else {
            $errors = Cache::get('nganluong_errors_all');
        }
        return json_decode($errors, 1);
    }
}