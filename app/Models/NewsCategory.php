<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class NewsCategory extends \Eloquent {

    public static function rules( $id = 0 ) {

        return [
            'name'                  => 'required|max:50',
            'summary'               => 'required|max:255',
        ];

    }

    protected $fillable = [ 'name', 'parent_id', 'summary', 'status', 'show_homepage', 'show_menu', 'is_system' ];

    public function parent()
    {
        return $this->belongsTo('App\NewsCategory', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\NewsCategory');
    }

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($news)
        {
            self::clearCache();
        });

        static::updated(function($news)
        {
            self::clearCache();
        });

        static::deleted(function($news)
        {
            self::clearCache();
        });

        static::saved(function($news)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('menu_news_categories');
        Cache::forget('all_news_categories');
        Cache::forget('home_news');
        Cache::forget('recent_news');
    }

    public static function getByMenu()
    {
        $categories = [];
        if (!Cache::has('menu_news_categories')) {
            $categories = NewsCategory::where('status', 1)->where("parent_id", 0)->where("show_menu", 1)->select('name', 'id')->get();
            for ($i=0; $i < count($categories); $i++) {
                $categories[$i]->children = NewsCategory::where('status', 1)->where("parent_id", $categories[$i]->id)->where("show_menu", 1)->select('name', 'id')->get();
            }
            $categories = json_encode($categories);
            if ($categories) Cache::forever('menu_news_categories', $categories);
        } else {
            $categories = Cache::get('menu_news_categories');
        }
        return json_decode($categories, 1);
    }

    public static function getByAll()
    {
        $categories = [];
        if (!Cache::has('all_news_categories')) {
            $categories = [];
            $tmp = NewsCategory::where('parent_id', 0)->where('status', 1)->get();
            foreach ($tmp as $category) {
                $categories[$category->id] = $category->name;
                $children = NewsCategory::where('parent_id', $category->id)->where('status', 1)->get();
                foreach ($children as $child) {
                    $categories[$child->id] = '-- ' . $child->name;
                }
            }

            $categories = json_encode($categories);
            if ($categories) Cache::forever('all_news_categories', $categories);
        } else {
            $categories = Cache::get('all_news_categories');
        }
        return json_decode($categories, 1);
    }
}
