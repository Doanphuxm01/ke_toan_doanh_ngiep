<?php
namespace App;

class Feedback extends \Eloquent {
    public static function rules($id = 0) {
        return [
            "fullname"  => "required|max:100",
            "email"     => "required|email|max:100",
            'phone'     => 'max:15',
            'content'   => 'required',
        ];
    }

    public static function rulesCom($id = 0) {
        return [
            "com_name"  => "required|max:100",
            'com_phone'     => 'max:15',
            'com_address'   => 'required|max:255',
            'com_product'   => 'required|max:255',
            "com_contact_name"  => "required|max:100",
            'com_contact_phone'     => 'max:15',
            'com_content'   => 'required',
        ];
    }
	protected $fillable = [ 'fullname', 'status', 'name', 'email', 'phone', "subject", 'content', 'type', 'contact_name', 'contact_phone', 'com_address', 'com_product', 'synced_mail' ];

    public function replies() {
        return $this->hasMany('\App\Mailbox');
    }
}