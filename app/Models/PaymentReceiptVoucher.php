<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class PaymentReceiptVoucher extends Model
{   
    public $timestamps = true;
    protected $fillable = ['object_type', 'receipt_account_id', 'payment_account_id', 'partner_id', 'voucher_date', 'voucher_no', 'beneficiary', 'desc', 'address', 'currency_id', 'exchange_rate', 'total_amount_accounting', 'total_exchange_accounting', 'total_taxation', 'total_exchange_taxation', 'total_amount_deptv', "total_exchange_deptv",'total_unit_price_tax', 'total_exchange_unit_price_tax', 'type', 'company_id', 'status', 'created_by', 'created_at', 'updated_at'];
   

    public static function rules($trx_no = 0)
    {
        return [
            "voucher_date" => 'required|date_format:d/m/Y',
            'voucher_no' => 'required|'.Rule::unique('payment_receipt_vouchers')->ignore($trx_no, 'voucher_no'),
            'beneficiary' => 'nullable',
            'details' => 'nullable',
            'currency_id' => 'required',
            "exchange_rate" => 'required',
            'object_name'   => 'required',
            'object' => 'required',

        ];
    }

    public function accounting()
    {
        return $this->hasMany('App\Models\Accounting','payment_receipt_voucher_id');
    }

    public function Taxes()
    {
        return $this->hasMany('App\Models\Tax','payment_receipt_voucher_id');
    }

    public function dept_vouchers()
    {
        return $this->hasMany('App\Models\DeptVoucher','payment_receipt_voucher_id');
    }
}
