<?php
namespace App\Models;

class FixedAssetAllocationCost extends \Eloquent {
    protected $table = "fixed_asset_allocation_cost";
	protected $fillable = [ 
        'voucher_id', 
        'asset_code', 
        'department_id', 
        'money_amount', 
        'expense_account', 
        'cost_item', 
    ];
    public static function getValue($asset_code, $voucher_id)
    {   
        $costs = FixedAssetAllocationCost::where('asset_code', $asset_code)->where('voucher_id', $voucher_id)->get();
        foreach ($costs as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
}