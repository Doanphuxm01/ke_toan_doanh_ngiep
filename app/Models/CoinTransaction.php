<?php

namespace App;

class CoinTransaction extends \Eloquent {
    protected $fillable = ['customer_id', 'amount', 'type', 'status', 'note', 'ref_order_id'];
}