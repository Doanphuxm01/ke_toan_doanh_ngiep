<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends BaseModels
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['code', 'name', 'property', 'is_system', 'description', 'parent_id', 'created_by', 'status', 'company_id', 'group_code', 'deleted_by'];

    public static function rules($id = 0)
    {
        return [
            'group'         => 'required',
            // 'parent'     => 'required',
            'code'          => "required|max:20|regex:/^[0-9]{3}VN[A-Za-z0-9]{0,15}$/",//unique:accounts,code,$id|
            'name'          => 'required|max:255',
            'description'   => 'required|max:255',
            'property'      => 'required|in:' . implode(',', \App\Defines\Account::getProperties()),
        ];
    }

    public function companies()
    {
        return $this->hasOne("App\Models\Company", "id", "company_id");
    }
    public function activeChildren()
    {
        $children = Account::where('company_id', $this->company_id)->where('parent_id', $this->code)->where("status", 1)->orderBy('code')->get();
        if ($children->count()) {
            foreach ($children as $child) {
                $child->activeChildren();
            }
        } else {
            return $this;
        }
    }
    public static function getActiveLowestLevel($companyId)
    {
        $results = [];
        $accounts = Account::where('company_id', $companyId)->where('status', 1)->orderBy('code')->get();
        foreach ($accounts as $account) {
            $tmp = $account->activeChildren();
            if ($tmp ){
                $results[$account->code] = $account->code . ' - ' . $account->name;
            }
        }
        return $results;
    }
    public static function getActiveLowestLevelId($companyId)
    {
        $results = [];
        $accounts = Account::where('company_id', $companyId)->where('status', 1)->orderBy('code')->get();
        foreach ($accounts as $account) {
            $tmp = $account->activeChildren();
            if ($tmp ){
                $results[$account->code] = $account->code;
            }
        }
        return $results;
    }
    public static function getActiveAccounts($companyId)
    {
        $results=[];
        $accounts = Account::where('company_id', $companyId)->where('status', 1)->get();
        foreach ($accounts as $key => $value) {
                $results[$value->code] = $value->code;
        }
        return $results;
    }
    public static function getOriginalPriceAccounts($companyId)
    {
        $results=[];
        $accounts = Account::where('company_id', $companyId)->where('status', 1)->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        foreach ($accounts as $key => $value) {
            $test = Account::where('parent_id', $key)->get();
            if(count($test) == 0){
                if($key[0] == 1){
                    $results[$key] = $value;
                }
            }
        }
        return $results;
    }
    public static function getExpenseAccounts($companyId)
    {
        $results=[];
        $accounts = Account::where('company_id', $companyId)->where('status', 1)->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        foreach ($accounts as $key => $value) {
            $test = Account::where('parent_id', $key)->get();
            if(count($test) == 0){
                if($key[0] == 7 || $key[0] == 8 || $key[0] == 9 ){
                    $results[$key] = $value;
                }
            }
        }
        return $results;
    }

    public static function getAllocationAccounts($companyId)
    {
        $results=[];
        $accounts = Account::where('company_id', $companyId)->where('status', 1)->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        foreach ($accounts as $key => $value) {
            $test = Account::where('parent_id', $key)->get();
            if(count($test) == 0){
                if(substr($key, 0, 5) == '008VN' || substr($key, 0, 5) == '106VN'){
                    $results[$key] = $value;
                }
            }
        }
        return $results;
    }
    public static function getCodeName($code)
    {
        return Account::where('code', $code)->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->first();
    }
    public function children($code, $level = 1)
    {
        $account = $this;
        echo view('backend.accounts.row', compact('account', 'code'))->render();
        $level++;
        $children = Account::where('company_id', session('current_company'))->where('parent_id', $this->code)->orderBy('code')->get();
        if ($children->count()) {
            foreach ($children as $child) {
                $child->children($code, $level);
            }
        } else {
        }
    }

    public function levelChildren($level = 1)
    {
        $account = $this;
        $dots = "";
        for($i = 0; $i < $level; $i++) $dots .= "*";
        print($account->code . '|' . $dots . '' . $account->code . ";");

        $children = Account::where('status', 1)->where('company_id', $this->company_id)->where('parent_id', $this->code)->orderBy('code')->get();
        if ($children->count()) {
            foreach ($children as $child) {
                $child->levelChildren(++$level);
            }
        } else {
            return ;
        }
    }

	public function getCodeFormatAttribute()
	{
		return $this->code . ' - ' . $this->name;
    }
}
