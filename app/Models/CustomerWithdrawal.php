<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class CustomerWithdrawal extends \Eloquent {
    protected $fillable = [ 'amount', 'note', 'customer_id', 'note', 'status', 'processed_by' ];

    // public static function rules() {
    //     return [
    //         'customer'  => 'required',
    //         'amount' 	=> 'min:0|required|integer',
    //         'note'  	=> 'max:255',
    //     ];
    // }

    // public function medias()
    // {
    //     return $this->morphMany(Media::class, 'mediable');
    // }

    // public function order()
    // {
    //     return $this->belongsTo(CustomerTransaction::class, 'ref_order_id');
    // }
}