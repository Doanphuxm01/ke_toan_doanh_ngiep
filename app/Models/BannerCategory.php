<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class BannerCategory extends \Eloquent {

    public static function rules($id = 0) {

        return [
            'name'                  => 'required|max:50',
            'href'                  => 'required|url|max:150',
            'image'                  => ($id == 0 ? 'required|' : '') . 'max:4096|mimes:jpg,jpeg,png,gif',
            'type'      => 'required|in:' . implode(',', Define\Constant::getCategoryBannerTypes()),
            'category'  => 'required',
        ];
    }

    protected $fillable = [ 'name', 'href', 'created_by', 'type', 'image', 'status', 'product_category_id' ];

    public static function boot()
    {
        parent::boot();

        static::updated(function($page)
        {
            self::clearCache();
        });

        static::deleted(function($page)
        {
            self::clearCache();
        });

        static::saved(function($page)
        {
            self::clearCache();
        });
    }

    public function category()
    {
        return $this->belongsTo('\App\ProductCategory', 'product_category_id');
    }

    public static function clearCache()
    {
        Cache::forget('banners');
    }

    public static function getBannerCategories()
    {
        $banners = [];
        if (!Cache::has('banners')) {
            $productCategories = ProductCategory::where('status', 1)->where('product_category_id', 0)->orderBy('position')->get();
            foreach ($productCategories as $pCategory) {
                $tmp = BannerCategory::where('status', 1)->where('product_category_id', $pCategory->id)->where('type', '<>', Define\Constant::IMAGE_TYPE_CATEGORY_MENU_SMALL)->select('name', 'image', 'type', 'href')->get()->keyBy('type');
                if ($tmp->count()) $banners['single'][$pCategory->id] = $tmp;

                $tmp = BannerCategory::where('status', 1)->where('product_category_id', $pCategory->id)->where('type', Define\Constant::IMAGE_TYPE_CATEGORY_MENU_SMALL)->select('name', 'image', 'href')->orderBy('updated_at', 'DESC')->take(3)->get();
                if ($tmp->count()) $banners['multi'][$pCategory->id] = $tmp;
            }
            $banners = json_encode($banners);
            Cache::put('banners', $banners, Define\Constant::SHORT_TTL);
        } else {
            $banners = Cache::get('banners');
        }

        return json_decode($banners, 1);
    }
}