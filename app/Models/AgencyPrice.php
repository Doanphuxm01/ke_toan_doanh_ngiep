<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgencyPrice extends \Eloquent {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['product_id', 'status', 'created_by', 'deleted_by'];

    public function logs()
    {
        return $this->hasMany(AgencyPriceLog::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function list()
    {
        return $this->hasMany(AgencyPriceList::class);
    }

    public static function boot()
    {
        parent::boot();

        static::updated(function($agency)
        {
            self::clearCache($agency->product_id);
        });

        static::created(function($agency)
        {
            self::clearCache($agency->product_id);
        });

        static::deleted(function($agency)
        {
            self::clearCache($agency->product_id);
        });

        static::saved(function($agency)
        {
            self::clearCache($agency->product_id);
        });
    }

    public static function clearCache($productId) {
        Cache::forget('agency_price_lists_' . $productId);
    }

    public static function getByProductId($productId)
    {
        $prices = [];
        if (!Cache::has('agency_price_lists_' . $productId)) {
            $ap = AgencyPrice::where('status', 1)->where('product_id', $productId)->first();
            if (is_null($ap)) return [];
            $prices = $ap->list()->orderBy('greater_than_equal')->select('greater_than_equal', 'less_than', 'price_ipo')->get()->toArray();
            $prices = json_encode($prices);
            if ($prices) Cache::forever('agency_price_lists_' . $productId, $prices);
        } else {
            $prices = Cache::get('agency_price_lists_' . $productId);
        }
        return json_decode($prices, 1);
    }
}