<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
     protected $connection = "mysql_hrm";
      protected $table = "departments";
    public static function getDepartments($companyId)
    {
        $results=[];
        $tmp = Department::where('company_id', $companyId)->where('status', '1')->get();
        foreach ($tmp as $department) {
            $results[$department->id] = $department->name;
        }
        return $results;
    }
    public static function getDepartment($id){
        $result = Department::where('id', $id)->pluck('name')->first();
        return $result;
    }
    public function getName()
    {
        return $this->hasMany(FixedAssetAllocationInformation::class, 'department_id');
    }
    
}