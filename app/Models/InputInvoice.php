<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InputInvoice extends Model
{
    public $timestamps = FALSE;
    const table_name        = 'input-invoice';
    protected $table              = self::table_name;
    static    $unguarded          = TRUE;

    const VND  = 'vnd';
    const DOLLAR  = 'dollar';

    static $objectMoney = [
        self::VND =>[
            'name' => 'Việt Nam Đồng (VND)',
            'key' => self::VND
        ],
        self::DOLLAR =>[
            'name' => 'Đô La Mỹ ($)',
            'key' => self::DOLLAR
        ],
    ];

    static function  getSaleCostItem($model){

    }
}
