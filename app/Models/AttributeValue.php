<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class AttributeValue extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'value'                 => 'required|max:100',
            'attribute'             => 'required',
            'code'                  => 'regex:/#([a-f0-9]{3}){1,2}\b/i',
        ];
    }

	protected $fillable = [ 'value', 'attribute_id', 'code', 'status', 'search' ];

    public static function getByAll()
    {
        $redis  = \App::make('redis');
        $aValues = $redis->get("attribute_values");
        if (!$aValues) {
            $tmp = AttributeValue::select('attribute_id', 'value', 'code', 'search')->get();
            $aValues = [];
            foreach ($tmp as $value) {
                $aValues[$value->attribute_id . '_' . str_slug($value->value, '_')] = ['code' => $value->code, 'search' => $value->search];
            }

            // $aValues = $aValues->keyBy('value');
            //->toArray();
            $aValues = json_encode($aValues);
            $redis->set("attribute_values", $aValues);
        }

        return json_decode($aValues, 1);
    }

    public static function boot()
    {
        parent::boot();
        self::clearCache();
    }

    public static function clearCache()
    {
        $redis  = \App::make('redis');
        $redis->del("attribute_values");
    }

    public function attribute()
    {
        return $this->belongsTo("\App\Attribute");
    }
}