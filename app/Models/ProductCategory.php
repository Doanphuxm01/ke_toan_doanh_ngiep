<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class ProductCategory extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'name'                  => 'required|max:100',
            'display_name'          => 'max:255',
            'summary'               => 'max:150',
            'icon'                  => 'max:2048|mimes:jpg,jpeg,png,gif',
            'image_cat'             => 'max:2048|mimes:jpg,jpeg,png,gif',
            'image_view'            => 'max:2048|mimes:jpg,jpeg,png,gif',
            'seo_keywords'          => 'max:50',
            'seo_description'       => 'max:255',
            'level1'                => ($id == 0 ? 'required|' : '') . 'integer',
            'ref_link'  => 'nullable|max:255|url',
        ];
    }

	protected $fillable = ['name', 'summary', 'product_category_id', 'icon', 'status', 'seo_keywords', 'seo_description', 'created_by', 'position', 'image_view', 'image_cat', 'show_view', 'display_name', 'ref_link'];

    private static function increase_count( $id, $redis ) {
        $redis->hincrby("product_counter_view_$id", 'counter_view_product', 1);
        $add_to_change_list = $redis->get("add_to_change_list_$id");
        if ( !$add_to_change_list || 1 ) {
            $redis->lpush("product_change_list", $id);
            $redis->setex("add_to_change_list_$id", \Config::get('products.write_count_cycle'), "change_after");
        }
    }

    public static function boot()
    {
        parent::boot();

        static::updated(function($cat)
        {
            self::clearCache($cat);
        });

        static::deleted(function($cat)
        {
            // BannerCategory::where('product_category_id', $page->id)->delete();
            self::clearCache($cat);
        });

        static::saved(function($cat)
        {
            self::clearCache($cat);
        });

        static::created(function($cat)
        {
            self::clearCache($cat);
        });
    }

    public static function clearCache($cat)
    {
        Cache::forget('root_product_categories');
        Cache::forget('product_categories_quick_links');
        Cache::forget('product_categories');
        Cache::tags('parents_categories_recursions')->flush();
        Cache::tags('parents_categories_recursions_and_self')->flush();
        Cache::forget('home_page_cat_view');
        Cache::forget('product_categories_children_' . $cat->id);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'product_category_id');
    }

    public function getChildrenIds()
    {
        $ids = $this->id;
        $childrenLevel1 = ProductCategory::where('product_category_id', $this->id)->where('status', 1)->get();
        foreach ($childrenLevel1 as $children) {
            $ids .= ',' . $children->id;
            $childrenLevel2 = ProductCategory::where('product_category_id', $children->id)->where('status', 1)->select('id')->get()->toArray();
            if (count($childrenLevel2))
                $ids .= ',' . implode(',', array_column($childrenLevel2, 'id'));
        }
        return $ids;
    }

    public function countAllProducts()
    {
        $counter = $this->products()->where('parent_id', 0)->where('status', 1)->count();
        $childrenLevel1 = ProductCategory::where('product_category_id', $this->id)->where('status', 1)->get();
        foreach ($childrenLevel1 as $children) {
            $counter += $children->products()->where('parent_id', 0)->where('status', 1)->count();
            $childrenLevel2 = ProductCategory::where('product_category_id', $children->id)->where('status', 1)->select('id')->get();
            foreach ($childrenLevel2 as $c) {
                $counter += $c->products()->where('parent_id', 0)->where('status', 1)->count();
            }
        }
        return $counter;
    }

    public function countSameType()
    {
        return ProductCategory::where("product_category_id", $this->product_category_id)->count();
    }

    public function attributes()
    {
        return $this->belongsToMany("\App\Attribute", 'attribute_categories', 'product_category_id', 'attribute_id');
    }

    public function getRoot()
    {
        if ($this->product_category_id) {
            $rootCat = ProductCategory::find($this->product_category_id);
            if (is_null($rootCat)) return $this; // moi them vao ErrorException: Trying to get property of non-object in...
            if ($rootCat->product_category_id) {
                $rootCat = ProductCategory::find($rootCat->product_category_id);
            }

            return $rootCat;
        }

        return $this;
    }

    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'brand_categories', 'product_category_id', 'brand_id');
    }

    public function suppliers()
    {
        return $this->belongsToMany(ProductSupplier::class, 'supplier_categories', 'product_category_id', 'supplier_id');
    }

    public static function getRootCategories()
    {
        $categories = [];
        if (!Cache::has('root_product_categories')) {
            $categories = ProductCategory::where('product_category_id', 0)->where('status', 1)->select('id', 'name', 'icon')->orderBy('position')->get();
            $categories = json_encode($categories);
            Cache::forever('root_product_categories', $categories);
        } else {
            $categories = Cache::get('root_product_categories');
        }

        return json_decode($categories, 1);
    }

    public static function getHomePageCatView()
    {
        $categories = [];
        if (!Cache::has('home_page_cat_view')) {
            $categories = ProductCategory::where('status', 1)->where('show_view', 1)->orderBy('updated_at', 'DESC')->select('id', 'name', 'icon', 'ref_link')->get();
            for ($i=0; $i < $categories->count(); $i++) {
                $categories[$i]->product_count = $categories[$i]->countAllProducts();
            }
            $categories = json_encode($categories);
            Cache::forever('home_page_cat_view', $categories);
        } else {
            $categories = Cache::get('home_page_cat_view');
        }

        return json_decode($categories, 1);
    }

    public static function getChildrenLevelDown($id)
    {
        $categories = [];
        if (1 || !Cache::has('product_categories_children_' . $id)) {
            $categories = ProductCategory::where('product_category_id', $id)->where('status', 1)->select('id', 'name', 'icon')->orderBy('position')->get();
            $categories = json_encode($categories);
            Cache::put('product_categories_children_' . $id, $categories);
        }

        return json_decode($categories, 1);
    }

    public static function getCategories()
    {
        $categories = [];
        if (!Cache::has('product_categories')) {
            $categories = ProductCategory::where('product_category_id', 0)->where('status', 1)->select('id', 'name', 'icon', 'display_name')->orderBy('position')->get();
            for ($i=0; $i < $categories->count(); $i++) {
                $children = ProductCategory::where('product_category_id', $categories[$i]->id)->where('status', 1)->select('id', 'name')->orderBy('position')->get();
                for ($j=0; $j < $children->count(); $j++) {
                    $children[$j]->children = ProductCategory::where('product_category_id', $children[$j]->id)->where('status', 1)->select('id', 'name')->orderBy('position')->get();
                }
                $categories[$i]->children = $children;
            }

            $categories = json_encode($categories);
            Cache::put('product_categories', $categories, 1);//
        } else {
            $categories = Cache::get('product_categories');
        }

        return json_decode($categories, 1);
    }

    public static function getRecursionParents($catId)
    {
        $categories = [];
        if (!Cache::tags(['parents_categories_recursions'])->has('parents_categories_recursion_' . $catId)) {
            $cat = ProductCategory::where('id', $catId)->where('status', 1)->first();
            if (is_null($cat)) return []; // not active
            if ($cat->product_category_id == 0) { // current is root
                $categories = [];
            } else {
                $root1 = ProductCategory::where('id', $cat->product_category_id)->where('status', 1)->first();
                if (is_null($root1)) return []; // not active

                if ($root1->product_category_id) {
                    $root2 = ProductCategory::where('id', $root1->product_category_id)->where('status', 1)->first();
                    if (is_null($root2)) return []; // not active
                    $categories[$root2->id] = $root2->name;
                }

                $categories[$root1->id] = $root1->name;
            }

            $categories = json_encode($categories);
            Cache::tags(['parents_categories_recursions'])->forever('parents_categories_recursion_' . $catId, $categories);
        } else {
            $categories = Cache::tags(['parents_categories_recursions'])->get('parents_categories_recursion_' . $catId);
        }

        return json_decode($categories, 1);
    }

    public static function getRecursionParentsAndSelf($catId)
    {
        $categories = [];
        if (!Cache::tags(['parents_categories_recursions_and_self'])->has('parents_categories_recursion_' . $catId)) {
            $cat = ProductCategory::where('id', $catId)->where('status', 1)->first();
            if (is_null($cat)) return []; // not active
            if ($cat->product_category_id <> 0) { // current is root
                $root1 = ProductCategory::where('id', $cat->product_category_id)->where('status', 1)->first();
                if (is_null($root1)) return []; // not active
                if ($root1->product_category_id) {
                    $root2 = ProductCategory::where('id', $root1->product_category_id)->where('status', 1)->first();
                    if (is_null($root2)) return []; // not active
                    $categories[$root2->id] = $root2->name;
                }
                $categories[$root1->id] = $root1->name;
            }
            $categories[$cat->id] = $cat->name;

            $categories = json_encode($categories);
            Cache::tags(['parents_categories_recursions_and_self'])->forever('parents_categories_recursion_' . $catId, $categories);
        } else {
            $categories = Cache::tags(['parents_categories_recursions_and_self'])->get('parents_categories_recursion_' . $catId);
        }
        return json_decode($categories, 1);
    }
}