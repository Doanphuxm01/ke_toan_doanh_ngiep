<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class ServicePurchase extends Model
{   
    protected $timestamp = true;
    protected $table = 'service_purchases';
    protected $fillable = ['partner_id', 'status', "desc", "time_for_payment", "voucher_date", "voucher_no", "currency_id", "exchange_rate", "total_amount", "vat_amount", "total_payment", "total_quantity_accounting", "total_price_unit_accounting", "total_amount_accounting", "total_exchange_accounting", "total_taxation", "total_exchange_taxation", "total_unit_price_tax", "total_exchange_unit_tax", "company_id", "created_by", "created_at", "updated_at"];
    
    public static function rules($voucher_no) 
    {
        return [
            'voucher_no'              => 'required|'.Rule::unique('service_purchases')->ignore($voucher_no, 'voucher_no'),
            'time_for_payment'    => 'required|date_format:d/m/Y',
            'vendor'              => 'required',
            'vendor_name'         => 'required',
            'currency_id'         => 'required',
            'exchange_rate'       => 'required',
            'voucher_date'         => 'required',
            'number_of_days_owed' => 'required|max:100000000',
            'deleted_at'          => 'nullable',
        ];
    }

    public function accounting()
    {
        return $this->hasMany('App\Models\Accounting','service_purchase_id');
    }

    public function Taxes()
    {
        return $this->hasMany('App\Models\Tax','service_purchase_id');
    }

    public static function getServiceVoucher($partner_id)
    {
        return ServicePurchase::where('company_id', session('current_company'))->where('partner_id', $partner_id)->get();
    }
}
