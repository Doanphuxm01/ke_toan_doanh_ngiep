<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class CustomerTransaction extends \Eloquent {
    protected $fillable = [
        'amount', 'ref_order_id', 'type', 'customer_id', 'created_by', 'note', 'source', 'status',
        // for online tracsaction
        'payment_status', 'gateway_id', 'bank_name', 'bank_code', 'bank_fee', 'bank_status', 'bank_message', 'bank_counter', 'trans_code',
    ];

    public static function rules() {
        return [
            'customer'  => 'required',
            'amount' 	=> 'min:0|required|integer',
            'note'  	=> 'max:255',
        ];
    }

    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function order()
    {
        return $this->belongsTo(CustomerTransaction::class, 'ref_order_id');
    }
}