<?php namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class Popup extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'name'  => 'required|max:50',
            'href'  => 'url|max:150',
            'image' => ($id == 0 ? 'required|' : '') . 'max:4096|mimes:jpg,jpeg,png,gif',
            'type'      => 'required|in:' . implode(',', Define\Popup::getTypes()),
        ];
    }

    protected $fillable = [ 'name', 'href', 'created_by', 'type', 'image', 'status' ];

    public static function boot()
    {
        parent::boot();

        static::updated(function($page)
        {
            self::clearCache();
        });

        static::deleted(function($page)
        {
            self::clearCache();
        });

        static::saved(function($page)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('popup');
        Cookie::forget('popup');
    }

    public static function getPopup()
    {
        $popup = [];
        if (!Cache::has('popup')) {
            $popup = Popup::where('status', 1)->select('name', 'image', 'href', 'type')->first();
            $popup = json_encode($popup);
            Cache::put('popup', $popup, Define\Constant::TTL);
        } else {
            $popup = Cache::get('popup');
        }

        return json_decode($popup, 1);
    }
}