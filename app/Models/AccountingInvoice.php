<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AccountingInvoice extends Model
{
    protected $guarded = [];
    protected $dates = ['voucher_date', 'payment_term'];

	public static function rules()
	{
		return [
			'voucher_no'    => 'required',
			'voucher_date'  => 'required',
			'payment_term'  => 'required',
			'exchange_rate' => 'required',
			'currency'      => 'required',
		];
    }

	public function setVoucherDateAttribute($value)
	{
		$this->attributes['voucher_date'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}

	public function setPaymentTermAttribute($value)
	{
		$this->attributes['payment_term'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}

//	public function accounting()
//	{
//		return $this->hasMany(Accounting::class);
//	}
//
//	public function taxes()
//	{
//		return $this->hasMany(Tax::class);
//	}

	public static function getPropertyAccountForOption($property, $companyId = null)
	{
		$companyId = $companyId ?? session('current_company');
		$temp = Account::where('property', $property)
			->where('company_id', $companyId)
			->where('status', 1)
			->get();
		return $temp->pluck('code_format', 'code')->toArray();
    }

	public static function getObjectForOption($property, $companyId = null)
	{
		$companyId = $companyId ?? session('current_company');
		$partners = Partner::getPartnerForOption($companyId);
		$accounts = self::getPropertyAccountForOption($property, $companyId);
//		$departments = Department::getDepartments($companyId);
//		$childrenPartner = [];
//		$childrenAccount = [];
//		$childrenDepartment = [];
//		foreach ($partners as $key => $value) {
//			array_push($childrenPartner, ['id' => $key, 'text' => $value]);
//		}
//		foreach ($accounts as $key => $value) {
//			array_push($childrenAccount, ['id' => $key, 'text' => $value]);
//		}
//		return [
//			[
//				'text' => 'Partners',
//				'children' => $childrenPartner
//			],
//			[
//				'text' => 'Account',
//				'children' => $childrenAccount
//			],
//		];
		return [
				'Partners' => $partners,
				'Account' => $accounts,
		];
    }
}
