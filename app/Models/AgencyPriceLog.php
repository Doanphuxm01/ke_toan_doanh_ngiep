<?php

namespace App;

class AgencyPriceLog extends \Eloquent
{
    public $timestamps = false;
    protected $fillable = ['field', 'data_old', 'data_new', 'action_by', 'note', 'action_at', 'agency_price_id'];
}