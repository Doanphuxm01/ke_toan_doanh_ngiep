<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class ShipLocation extends \Eloquent {

    public static function rules($id = 0) {

        return [
            'province'  => 'required',
            'district'  => 'nullable',
            'fee'       => 'nullable|min:0|max:1000000',
        ];
    }

	protected $fillable = ['province_id', 'fee', 'status'];
    public static function boot()
    {
        parent::boot();

        static::updated(function($page)
        {
            self::clearCache();
        });

        static::deleted(function($page)
        {
            self::clearCache();
        });

        static::saved(function($page)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('order_sube_ship_all');
    }

    public function details()
    {
        return $this->hasMany(ShipLocationDetail::class);
    }


}