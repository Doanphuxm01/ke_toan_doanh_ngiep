<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use App\Models\PaymentReceiptVoucher;
use phpDocumentor\Reflection\Types\This;

class Partner extends Model
{
	protected $table = 'partners';
    public $timestamps  = true;
    protected $fillable = ['tax_code', 'contact', 'name', 'code', 'address_1', 'address_2', 'address_3', 'address_4', 'created_by', 'is_vendor', 'company_id', 'status', 'city_code', 'country_code', 'email', 'phone_no', 'fax_no', 'is_customer', 'is_contra','created_at', 'updated_at'];

    public static function rules($taxCode = 0, $code = 0)
    {
        return [
            'tax_code'  => 'nullable|regex:/^[0-9]{10}(-[0-9]{3}$)?$/',
            'code'      => 'required|max:15|'.Rule::unique('partners')->ignore($code, 'code'),
            'name'      => 'required|max:255',
            'address_1' => 'nullable|max:255',
            'address_2' => 'nullable|max:255',
            'address_3' => 'nullable|max:255',
            'address_4' => 'nullable|max:255',
        ];
    }

    public static function getInfoPartner($id)
    {
        $result = Partner::where("id", $id)->get()->first();
        return $result; 
    }

	public function getCodeFormatAttribute()
	{
		return $this->code . ' - ' . $this->name;
    }

	public static function getPartnerForOption($companyId = null)
	{
		$companyId = $companyId ?? session('current_company');
		$temp = Partner::where('company_id', $companyId)
			->where('status', 1)
			->get();
		return $temp->pluck('code_format', 'id')->toArray();
	}

    public static function getVendor()
    {
        return Partner::where('status', '1')->where('company_id', session('current_company'))->where('is_vendor', '1')->pluck('code', 'id')->toArray();
    }

    public static function getCustomer()
    {
        return Partner::where('status', '1')->where('company_id', session('current_company'))->where('is_customer', '1')->pluck('code', 'id')->toArray();
    }

    public static function getAllPartner()
    {
        return Partner::where('status', 1)->where('company_id', session('current_company'))->pluck('code','id')->toArray();
    }

    public static function getAllPartner2()
    {
        return Partner::where('status', 1)->where('company_id', session('current_company'))->get()->toArray();
    }

}
