<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoicesCode extends BaseModels
{
    public $timestamps = false;
    const table_name = 'invoices_code';
    protected $table = self::table_name;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
