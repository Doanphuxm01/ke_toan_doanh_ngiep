<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleProduct extends \Eloquent {
	use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable = ["product_id", "from_time", "to_time", "status", "sale_id", "flash_sale_id", "created_by", "deleted_by", "percent", "root_product_id"];

	public static function boot()
    {
        parent::boot();

        static::updated(function($saleProduct)
        {
            self::clearCache($saleProduct);
        });

        static::deleted(function($saleProduct)
        {
            self::clearCache($saleProduct);
        });

        static::created(function($saleProduct)
        {
            self::clearCache($saleProduct);
        });

        static::saved(function($saleProduct)
        {
            self::clearCache($saleProduct);
        });
    }

    public static function clearCache($saleProduct)
    {
    	Cache::forget('product_' . $saleProduct->product_id);
    }
}