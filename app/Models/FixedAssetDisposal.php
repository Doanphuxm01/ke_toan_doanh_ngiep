<?php
namespace App\Models;

class FixedAssetDisposal extends \Eloquent {
    protected $table = "fixed_asset_disposal";
    public static function rules($id = 0) {
        return [
            'voucher_number'        => 'required',
            'voucher_date'          => 'required',
            'asset_code'            => 'required',
            'disposal_account'      => 'required',
        ];
    }
	protected $fillable = [ 
        'voucher_number', 
        'disposal_reason',
        'asset_code',
        'disposal_account',
        'voucher_date', 
        'company_id',
        'created_by',
        'status'
    ];

    public static function check($month, $year)
    {
        $result = FixedAssetDepreciation::where('month', $month)->where('year', $year)->get();
        return $result;
    }
    public static function getTotal($id)
    {   
        $accounting = Accounting::where('disposal_voucher_id', $id)->get();
        $result = 0;
        foreach ($accounting as $key => $value) {
            $result += $value->amount;
        }
        return $result;
    }
}