<?php

namespace App;

class ProductLog extends \Eloquent {
    public $timestamps = false;
    protected $fillable = ["field", 'old_data', "note", "new_data", "action_at", "action_by", 'product_id'];
}
