<?php
namespace App\Models;

class ExpensesPrepaid extends \Eloquent {
    protected $table = "expenses_prepaid";
    public static function rules($id = 0) {
        return [
            'expenses_prepaid_code'             => 'required|max:25|unique:expenses_prepaid,expenses_prepaid_code' . ($id == 0 ? '' : ',' . $id),
            'expenses_prepaid_name'             => 'required',
            'voucher_date'                      => 'required',
            'money_amount'                       => 'required',
            'period_number'                     => 'required',
            'periodical_allocation_value'       => 'required',
            'allocation_account'                => 'required',
        ];

    }
	protected $fillable = [ 
        'expenses_prepaid_code', 
        'expenses_prepaid_name', 
        'voucher_date', 
        'money_amount', 
        'period_number', 
        'residual_time',
        'periodical_allocation_value',
        'allocation_account',
        'company_id',
        'status',
        'created_by'
    ];
    public function expensesPrepaidAllocation()
    {
        return $this->hasMany(ExpensesPrepaidAllocationInformation::class, 'expenses_prepaid_id', 'id');
    }
    public static function checkAllocation($id){
        return count(ExpensesPrepaidAllocationCost::where('expenses_prepaid_id', $id)->get());
    }
    public static function getVoucherInfo($accounting_id)
    {   
        $accounting = Accounting::where('id', $accounting_id)->first();
        if($accounting->service_purchase_id != null){
            return Accounting::where('accounting.id', $accounting_id)->join('service_purchases', 'accounting.service_purchase_id', '=', 'service_purchases.id')
                            ->select('accounting.id', 'service_purchases.voucher_date', 'service_purchases.desc', 'service_purchases.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                            ->first();
        }
        else if($accounting->accounting_invoice_id != null){
            return Accounting::where('accounting.id', $accounting_id)->join('accounting_invoices', 'accounting.accounting_invoice_id', '=', 'accounting_invoices.id')
                            ->select('accounting.id', 'accounting_invoices.voucher_date', 'accounting_invoices.desc', 'accounting_invoices.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                            ->first();
        }
        else if($accounting->payment_receipt_voucher_id != null){
            return Accounting::where('accounting.id', $accounting_id)->join('payment_receipt_vouchers', 'accounting.payment_receipt_voucher_id', '=', 'payment_receipt_vouchers.id')
            ->select('accounting.id', 'payment_receipt_vouchers.voucher_date', 'payment_receipt_vouchers.desc', 'payment_receipt_vouchers.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
            ->first();
        }   
    }
    public static function residualValue($id)
    {
        $result = 0;
        $costs = ExpensesPrepaidAllocationCost::where('expenses_prepaid_id', $id)->get();
        foreach ($costs as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
}
