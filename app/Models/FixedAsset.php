<?php
namespace App\Models;

class FixedAsset extends \Eloquent {
    public static function rules($id = 0) {
        return [
            'asset_code'                    => 'required|max:25|unique:fixed_assets,asset_code' . ($id == 0 ? '' : ',' . $id),
            'asset_name'                    => 'required|max:100',
            'original_price_account'        => 'required',
            'depreciation_account'          => 'required',
            'original_price'                => 'required',
            'depreciation_value'            => 'required',
            'begin_use_date'                => 'required',
            'use_time'                      => 'required',
            'use_department'                => 'required',
            'allocation_rate'               => 'required',
        ];

    }
	protected $fillable = [ 
        'voucher_number', 
        'voucher_creation_date', 
        'asset_code', 
        'asset_name', 
        'original_price_account', 
        'depreciation_account',
        'original_price',
        'depreciation_value',
        'begin_use_date',
        'use_time',
        'residual_use_time',
        'monthly_depreciation_value',
        'yearly_depreciation_value',
        'accumulated_depreciation',
        'residual_value',
        'company_id',
        'status',
        'created_by'
    ];
    public function allocation_information()
    {
        return $this->hasMany(FixedAssetAllocationInformation::class, 'asset_code', 'asset_code');
    }
    public static function getAllocationInfo($asset_code)
    {
        return FixedAssetAllocationInformation::where('asset_code', $asset_code)->get();
    }
    public static function getAllocationCost($asset_code, $voucher_id)
    {
        return FixedAssetAllocationCost::where('asset_code', $asset_code)->where('voucher_id', $voucher_id)->get();
    }
    public static function checkUpdateDelete($asset_code)
    {
        return FixedAssetAllocationCost::where('asset_code', $asset_code)->get();
    }
    
    
}