<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModels extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_CRALLW = 'cralw-ok';
    const STATUS_DISABLE = 'disabled';
    const STATUS_DELETED = 'deleted';
    const STATUS_DELETED_PHY = 'remove-phy';//xoa vat ly
    const STATUS_PENDING = 'pending';// chờ duyệt
    const STATUS_DRATF = 'dratf';// bản nháp
    const STATUS_PROCESS_OK = 'processed';// Bản xử lý mọi thứ OK và chờ mở dần dần => dùng job để update mở bài này ra hàng ngày mỗi ngày 1 ít
    const STATUS_PROCESS_DONE = 'done';// Bản xử lý mọi thứ OK và chờ mở dần dần => dùng job để update mở bài này ra hàng ngày mỗi ngày 1 ít
    const STATUS_NO_PAID = 'no_paid'; //chưa thanh toán
    const STATUS_NO_PROCESS = 'no_process'; //chưa xử lý
    const STATUS_PROCESSING = 'process'; //đang xử lý
    const STATUS_CANCEL = 'cancel'; //hủy

    static function getListStatus($selected = FALSE)
    {
        $listStatus = [
            self::STATUS_ACTIVE => ['id' => self::STATUS_ACTIVE, 'style' => 'success', 'text' => 'Đang hoạt động ', 'text-action' => 'Kích hoạt hiển thị'],
            self::STATUS_INACTIVE => ['id' => self::STATUS_INACTIVE, 'style' => 'secondary', 'text' => 'Chờ kích hoạt', 'text-action' => 'Chờ kích hoạt'],
            self::STATUS_PROCESSING => ['id' => self::STATUS_PROCESSING, 'style' => 'warning', 'text' => 'Đang xử lý ', 'text-action' => 'Đang xử lý'],
            self::STATUS_NO_PROCESS => ['id' => self::STATUS_NO_PROCESS, 'style' => 'warning', 'text' => 'Chưa xử lý ', 'text-action' => 'Chưa xử lý'],
            self::STATUS_PROCESS_DONE => ['id' => self::STATUS_PROCESS_DONE, 'style' => 'success', 'text' => 'Đã xử lý', 'text-action' => 'Đã xử lý'],
            self::STATUS_DRATF => ['id' => self::STATUS_DRATF, 'style' => 'secondary', 'text' => 'Bản nháp ', 'text-action' => 'Bản nháp'],
            self::STATUS_CANCEL => ['id' => self::STATUS_CANCEL, 'style' => 'warning', 'text' => 'Hủy', 'text-action' => 'Hủy'],
            self::STATUS_DISABLE => ['id' => self::STATUS_DISABLE, 'style' => 'warning', 'text' => 'Không sử dụng', 'text-action' => 'Hủy'],
            self::STATUS_DELETED => ['id' => self::STATUS_DELETED, 'style' => 'danger', 'text' => 'Đã xóa', 'text-action' => 'Đã xóa'],
        ];
        if ($selected && isset($listStatus[$selected])) {
            $listStatus[$selected]['checked'] = 'checked';
        }

        return $listStatus;
    }
    static function getCreatedByToSaveDb(){
        $member = auth()->user();
        if(!isset($member)){
            return abort(403);
        }else{
            return json_encode([
                'id'      => (!empty($member->id) ? $member->id : ''),
                'name'    => (!empty($member->username) ? $member->username : ''),
                'email'   => (!empty($member->email) ? $member->email : ''),
                'phone'   => (!empty($member->phone) ? $member->phone : ''),
            ]);
        }
    }

    public static function table($table)
    {
        return DB::table($table);
    }

    public static function getNextNumberWithYear($table){
        $table = $table.date("Y");
        return self::getNextNumber($table);
    }

    public static function getInPluckData($pluck = ''){
        $where = [];
        $where['status'] = 1;
        $query = self::select('*')->where($where);
        if(!empty($pluck)){
            return $query->pluck($pluck)->toArray();
        }else{
            return $query->get()->toArray();
        }
        return flase;
    }

    public static function getActiveMember($select = 'fullname'){
        $user = auth()->user();
        if(!empty($user)){
            return $user->$select;
        }else{
            return abort('403');
        }
    }
}
