<?php
namespace App\Models;

class InstrumentToolDisposalDetail extends \Eloquent {
	protected $fillable = [ 
        'disposal_voucher_id',
        'department_id', 
        'residual_quantity',
        'disposal_quantity',
        'amount',

    ];
    
}