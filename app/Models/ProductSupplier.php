<?php
namespace App;

use Illuminate\Support\Facades\Cache;

class ProductSupplier extends \Eloquent {

    public static function rules($id = 0) {
        return [
            'name'              => 'required|max:100|unique:product_suppliers,name' . ($id == 0 ? '' : ',' . $id),
            "logo"              => 'max:2048|mimes:jpg,jpeg,png,gif',
            "phone"             => 'max:15',
            "email"             => 'max:50|email|nullable',
            "ghn_hub"           => 'max:255',
            "note"              => 'max:255',
        ];
    }

    protected $fillable = [ "name", "logo", "phone", "email", "ghn_hub_id", "ghn_hub_address", "note", "status", "created_by", "district_code" ];

    public static function boot()
    {
        parent::boot();

        static::created(function($page)
        {
            self::clearCache();
        });

        static::updated(function($page)
        {
            self::clearCache();
        });

        static::deleted(function($page)
        {
            self::clearCache();
        });

        static::saved(function($page)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('suppliers');
        Cache::tags('supplier_categories')->flush();
    }

    public function products()
    {
        return $this->hasMany('App\Product', 'product_category_id');
    }

    public function countSameType()
    {
        return ProductCategory::where("product_category_id", $this->product_category_id)->count();
    }

    public function attributes()
    {
        return $this->belongsToMany("\App\Attribute", 'attribute_categories', 'product_category_id', 'attribute_id');
    }

    public function categories()
    {
        return $this->belongsToMany("\App\ProductCategory", 'supplier_categories', 'product_category_id', 'brand_id')->withPivot('position');
    }

    public static function getSupplierByCategories($category)
    {
        $categorySuppliers = Cache::tags(['supplier_categories'])->get('supplier_categories_' . $category->id);
        if (!$categorySuppliers) {
            $categorySuppliers = $category->suppliers()->where('status', 1)->orderBy('supplier_categories.position')->select('product_suppliers.name', 'product_suppliers.logo', 'product_suppliers.id')->get()->toArray();
            $categorySuppliers = json_encode($categorySuppliers);
            if ($categorySuppliers) Cache::tags(['supplier_categories'])->put('supplier_categories_' . $category->id, $categorySuppliers);
        }

        return json_decode($categorySuppliers, 1);
    }

    public static function getByAll()
    {
        $suppliers = [];
        if (!Cache::has('suppliers')) {
            $suppliers = ProductSupplier::where('status', 1)->pluck('name', 'id')->toArray();
            $suppliers = json_encode($suppliers);
            Cache::forever('suppliers', $suppliers);
        } else {
            $suppliers = Cache::get('suppliers');
        }

        return json_decode($suppliers, 1);
    }
}