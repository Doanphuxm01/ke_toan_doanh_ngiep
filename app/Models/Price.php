<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends \Eloquent {

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['product_id', 'category_id', 'note',  'partner_percent', 'partner_amount', 'customer_percent_default', 'customer_percent', 'customer_amount_default', 'customer_amount', 'type', 'status', 'source', 'created_by', 'deleted_by', 'dropship_percent', 'dropship_amount'];

    public function logs()
    {
        return $this->hasMany(PriceLog::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

}