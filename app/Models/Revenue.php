<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Revenue extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    const table_name        = 'revenue';
    protected $table              = self::table_name;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
}
