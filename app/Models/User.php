<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\UserInterface;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $connection = "mysql_hrm";
    use HasApiTokens, Notifiable, LaratrustUserTrait;
    protected $table = "users";
    protected $dates = ['deleted_at'];

    public function companies()
    {
      return $this->hasOne("App\Models\Company", "id", "company_id");

    }
   
    public static function rules($id = 0) {
        return [
        ];
    }
    // protected $fillable = [];
    protected $fillable = [ 'username', 'email', 'fullname', 'password', 'phone', 'remember_token', 'activated', 'activation_code', 'activated_at', 'last_login', 'reset_password_code', 'facebook_id', 'google_id', 'title_id', 'department_id', 'dept_temp', 'title_temp'];

    public static function boot()
    {
        parent::boot();

       
        static::updated(function($bank)
        {
            // abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // self::clearCache();
        });
        static::updating(function($bank)
        {
            // abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // self::clearCache();
        });

        static::created(function($bank)
        {
            // abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // // self::clearCache();
        });
        static::creating(function($bank)
        {
            // abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // // self::clearCache();
        });

        static::deleted(function($bank)
        {
            abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // self::clearCache();
        });
        static::deleting(function($bank)
        {
            abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // self::clearCache();
        });

        static::saved(function($bank)
        {
            // abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // self::clearCache();
        });

        static::saving(function($bank)
        {
            // abort(401, "Bạn không có quyền update info cho user tới HRM connection...");
            // self::clearCache();
        });
    }
}
