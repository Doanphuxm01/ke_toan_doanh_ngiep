<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{   
    protected $guarded = [];
    
    public static function rules()
    {
        return [
        	'tax_rate' => 'required',
			'taxation'	=> 'required',
			'exchange_tax'	=> 'required',
			//'unit_price'	=> 'required',
			//'exchange_unit'	=> 'required',
			'account_tax'	=> 'required',
            "invoice_date" => 'required',
//            "partner_id" => 'sometimes|required',
         ];
    }

//	public function setInvoiceDateAttribute($value)
//	{
//		$this->attributes['invoice_date'] = $value ? Carbon::create($value)->format('Y-m-d') : null;
//	}

	public function accountingInvoice()
	{
		return $this->belongsTo(AccountingInvoice::class);
	}
}
