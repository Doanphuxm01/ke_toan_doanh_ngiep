<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends \Eloquent {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function rules($group = "", $id = 0) {
        $rules = [];
        switch ($group) {
            case Define\Sale::GROUP_GPRODUCT:
                $rules = [
                    "name"          => 'required|max:100',
                    // "type"          => 'required|in:' . implode(',', Define\Sale::getTypes()),
                    // "product"       => 'required',
                    "gift_products" => 'required',
                    // 'image'         => ($id ? 'nullable' : 'required') . '|max:2048|mimes:jpg,jpeg,png,gif',
                    // "category"      => 'required',
                    "note"          => 'max:255',
                ];
                break;
            case Define\Sale::GROUP_DPRODUCT:
                $rules = [
                    "name"          => 'required|max:100',
                    // "type"          => 'required|in:' . implode(',', Define\Sale::getTypes()),
                    // "product"       => 'required',
                    // 'image'         => ($id ? 'nullable' : 'required') . '|max:2048|mimes:jpg,jpeg,png,gif',
                    // "category"      => 'required',
                    "discount_percent"  => 'required_if:discount_amount,null',
                    "discount_amount"   => 'required_if:discount_percent,null',//|different:discount_amount
                    "note"          => 'max:255',
                ];
                break;
            // case Define\Sale::GROUP_COUPON:
            //     $rules = [
            //         "name"          => 'required|max:100',
            //         "type"          => 'required|in:' . implode(',', Define\Sale::getTypes()),
            //         "product"       => 'required',
            //         "coupon_code"   => 'required|min:3|max:50|regex:/^[a-zA-Z0-9]+$/',
            //         "coupon_amount" => "required|integer:min:1",
            //         "note"          => 'max:255',
            //     ];
            //     break;
            case Define\Sale::GROUP_PFREESHIP:
                $rules = [
                    "name"          => 'required|max:100',
                    // "type"          => 'required|in:' . implode(',', Define\Sale::getTypes()),
                    // "product"       => 'required',
                    "note"          => 'max:255',
                ];
                break;
            case Define\Sale::GROUP_OFREESHIP:
                $rules = [
                    "name"          => 'required|max:100',
                    // "type"          => 'required|in:' . implode(',', Define\Sale::getTypes()),
                    "from_amount"   => 'required|integer|lte:to_amount|min:0',
                    "to_amount"     => 'required|integer',
                    "note"          => 'max:255',
                ];
                break;
            case Define\Sale::GROUP_GORDER:
                $rules = [
                    "name"          => 'required|max:100',
                    "from_amount"   => 'required|integer|min:0|lte:to_amount',
                    "to_amount"     => 'required|integer',
                    "gift_products" => 'required',
                    "note"          => 'max:255',
                    // "product"       => 'nullable',
                    // "type"          => 'nullable|in:' . implode(',', Define\Sale::getTypes()),
                ];
                break;
            case Define\Sale::GROUP_DORDER:
                $rules = [
                    "name"          => 'required|max:100',
                    "from_amount"   => 'required|integer|lte:to_amount|min:0',
                    "to_amount"     => 'required|integer',
                    "discount_percent"  => 'required_if:discount_amount,null',
                    "discount_amount"   => 'required_if:discount_percent,null',//|different:discount_amount
                    "note"          => 'max:255',
                    // "product"       => 'nullable',
                    // "type"          => 'nullable|in:' . implode(',', Define\Sale::getTypes()),
                ];
                break;
        }
        return $rules;
    }

	protected $fillable = ["name", "note", "from_date", "to_date", "status", "group", "from_amount", "to_amount", "discount_percent", "discount_amount", "gift_product_ids", "created_by", "deleted_by", "flash_sale_id", "from_time", "to_time"];

    public static function boot()
    {
        parent::boot();

        static::updated(function($sale)
        {
            self::clearCache();
        });

        static::deleted(function($sale)
        {
            self::clearCache();
        });

        static::created(function($sale)
        {
            self::clearCache();
        });

        static::saved(function($sale)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::tags('sale_by_categories')->flush();
        Cache::forget('sale_products_has_sale');
        Cache::forget('flash_sale_now');
        Cache::forget('flash_sale_other');
    }

    public function orderFreeShippings()
    {
        return $this->hasMany("\App\Order", 'free_shipping_id');
    }

    public function orderFreeShippingDetails()
    {
        return $this->hasMany("\App\OrderDetail", 'free_shipping_id');
    }

    public function orders()
    {
        return $this->hasMany("\App\Order", 'sale_id');
    }

    public function products()
    {
        return $this->hasMany(SaleProduct::class);
    }

    public function flash()
    {
        return $this->belongsTo(FlashSale::class, 'flash_sale_id');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'sale_id');
    }

    public static function getFreeShippingByAll()
    {
        $_freeShippings = [];
        $minTime = 604800; // 1 week in second
        $current = time();
        # calc minute auto clean cache
        if (!Cache::has('free_shipping_all')) {
            $freeShippings = Sale::where('group', \App\Define\Sale::GROUP_FREESHIP)->where('status', 1)->where("from_date", "<=", date("Y-m-d H:i"))->where("to_date", ">=", date("Y-m-d H:i"))->select('type', 'id', 'object_id', 'from_date', 'to_date', 'province', 'district')->orderBy('updated_at', 'DESC')->get();
            foreach ($freeShippings as $freeShipping) {
                // get list product free
                if ($freeShipping->type == \App\Define\Sale::TYPE_PRODUCT) {
                    $object = Product::where('id', $freeShipping->object_id)->where('status', 1)->first();
                    if (is_null($object)) continue;
                    $diffTime = strtotime($freeShipping->to_date) - $current;
                    $minTime = ($diffTime > $minTime ? $minTime : $diffTime);
                    $_freeShippings[$object->id] = [
                        'to_date'   => $freeShipping->to_date,
                        'id'        => $freeShipping->id,
                        'province'  => $freeShipping->province,
                        'discount'  => $freeShipping->discount,
                    ];
                } elseif ($freeShipping->type == \App\Define\Sale::TYPE_BRAND) {
                    $object = Brand::where('id', $freeShipping->object_id)->where('status', 1)->first();
                    if (is_null($object)) continue;
                    // find all products
                    $products = Product::where('parent_id', 0)->where('brand_id', $object->id)->where('status', 1)->get();
                    if ($products->count()) {
                        $diffTime = strtotime($freeShipping->to_date) - $current;
                        $minTime = ($diffTime > $minTime ? $minTime : $diffTime);
                        foreach ($products as $product) {
                            $_freeShippings[$product->id] = [
                                'to_date'   => $freeShipping->to_date,
                                'id'        => $freeShipping->id,
                                'province'  => $freeShipping->province,
                                'discount'  => $freeShipping->discount,
                            ];
                        }
                    }
                } elseif ($freeShipping->type == \App\Define\Sale::TYPE_SUPPLIER) {
                    $object = ProductSupplier::where('id', $freeShipping->object_id)->where('status', 1)->first();
                    if (is_null($object)) continue;
                    $products = Product::where('parent_id', 0)->where('product_supplier_id', $object->id)->where('status', 1)->get();
                    if ($products->count()) {
                        $diffTime = strtotime($freeShipping->to_date) - $current;
                        $minTime = ($diffTime > $minTime ? $minTime : $diffTime);
                        foreach ($products as $product) {
                            $_freeShippings[$product->id] = [
                                'to_date'   => $freeShipping->to_date,
                                'id'        => $freeShipping->id,
                                'province'  => $freeShipping->province,
                                'discount'  => $freeShipping->discount,
                            ];
                        }
                    }
                }
            }
            $_freeShippings = json_encode($_freeShippings);
            Cache::put('free_shipping_all', $_freeShippings, $minTime);
        } else {
            $_freeShippings = Cache::get('free_shipping_all');
        }

        return json_decode($_freeShippings, 1);
    }

    // get all products have sale program
    public static function productsHaveSale1()
    {
        $now = date("Y-m-d H:i");
        $products = [];
        if (!Cache::has('sale_products_has_sale')) {
            $sales = Sale::where('status', 1)->where("from_date", "<=", $now)->where("to_date", ">=", $now)->whereIn('group', [\App\Define\Sale::GROUP_GPRODUCT, \App\Define\Sale::GROUP_DPRODUCT])->select('name', 'id', 'image', 'discount_percent', 'discount_amount', 'gift_product_ids', 'from_date', 'to_date', 'group', 'type', 'object_id', 'gift_product_ids')->orderBy('updated_at', 'DESC')->get();

            foreach ($sales as $sale) {
                $p = [];
                $p['id']        = $sale->id;
                $p['group']     = $sale->group;
                $p['to_date']   = $sale->to_date;
                if ($sale->group == \App\Define\Sale::GROUP_GPRODUCT) {
                    $p['gift_product_ids'] = $sale->gift_product_ids;
                } else {
                    $p['discount_percent'] = $sale->discount_percent;
                    $p['discount_amount'] = $sale->discount_amount;
                }

                switch ($sale->type) {
                    case \App\Define\Sale::TYPE_PRODUCT:
                        // $p['product_id'] = $sale->object_id;
                        $products[$sale->object_id] = $p;
                        break;
                    case \App\Define\Sale::TYPE_SUPPLIER:
                        $_products = Product::where('status', 1)->where('product_supplier_id', $sale->object_id)->where('parent_id', 0)->get();
                        foreach ($_products as $_p) {
                            $products[$_p->id] = $p;
                        }
                        break;
                    case \App\Define\Sale::TYPE_BRAND:
                        $_products = Product::where('status', 1)->where('brand_id', $sale->object_id)->where('parent_id', 0)->get();
                        foreach ($_products as $_p) {
                            $products[$_p->id] = $p;
                        }
                        break;
                }
            }

            $products = json_encode($products);
            if ($products) Cache::forever('sale_products_has_sale', $products);
        } else {
            $products = Cache::get('sale_products_has_sale');
        }
        return json_decode($products, 1);
    }

    public static function getByCategory($categoryId)
    {
        $sales = [];
        if (!Cache::tags('sale_by_categories')->has('sale_by_category_' . $categoryId)) {
            $sales = Sale::where('status', 1)->where('category_id', $categoryId)->where("from_date", "<=", date("Y-m-d H:i"))->where("to_date", ">=", date("Y-m-d H:i"))->whereIn('group', [\App\Define\Sale::GROUP_GPRODUCT, \App\Define\Sale::GROUP_DPRODUCT])->select('name', 'id', 'image', 'discount_amount', 'discount_percent', 'from_date', 'to_date', 'type', 'object_id')->orderBy('updated_at', 'DESC')->get();
            // get first product images
            for ($i=0; $i < $sales->count(); $i++) {
                if ($sales[$i]->image) {
                    $sales[$i]->image_product = $sales[$i]->image;
                } else {
                    $firstProduct = null;
                    switch ($sales[$i]->type) {
                        case \App\Define\Sale::TYPE_PRODUCT:
                            $firstProduct = Product::where('status', 1)->where('id', $sales[$i]->object_id)->where('parent_id', 0)->first();
                            break;
                        case \App\Define\Sale::TYPE_SUPPLIER:
                            $firstProduct = Product::where('status', 1)->where('product_supplier_id', $sales[$i]->object_id)->where('parent_id', 0)->first();
                            break;
                        case \App\Define\Sale::TYPE_BRAND:
                            $firstProduct = Product::where('status', 1)->where('brand_id', $sales[$i]->object_id)->where('parent_id', 0)->first();
                            break;
                    }
                    if (is_null($firstProduct)) {
                        unset($sales[$i]);
                        continue;
                    }
                    $sales[$i]->image_product = $firstProduct->image;
                }
            }

            $sales = json_encode($sales);
            Cache::tags('sale_by_categories')->forever('sale_by_category_' . $categoryId, $sales);
        } else {
            $sales = Cache::tags('sale_by_categories')->get('sale_by_category_' . $categoryId);
        }

        return json_decode($sales, 1);
    }

    public static function productsHaveSale()
    {
        return [];
        $now = date("Y-m-d H:i");
        $products = [];
        if (!Cache::has('sale_products_has_sale')) {
            $sales = Sale::where('status', 1)->where("from_date", "<=", $now)->where("to_date", ">=", $now)->whereIn('group', [\App\Define\Sale::GROUP_GPRODUCT, \App\Define\Sale::GROUP_DPRODUCT])->select('name', 'id', 'image', 'discount_percent', 'discount_amount', 'gift_product_ids', 'from_date', 'to_date', 'group', 'type', 'object_id', 'gift_product_ids', 'province', 'district')->orderBy('updated_at', 'DESC')->get(); // \App\Define\Sale::GROUP_FREESHIP

            foreach ($sales as $sale) {
                $p = [];
                $p['id']        = $sale->id;
                $p['group']     = $sale->group;
                $p['to_date']   = $sale->to_date;
                if ($sale->group == \App\Define\Sale::GROUP_GPRODUCT) {
                    $p['gift_product_ids'] = $sale->gift_product_ids;
                } elseif ($sale->group == \App\Define\Sale::GROUP_DPRODUCT) {
                    $p['discount_percent'] = $sale->discount_percent;
                    $p['discount_amount'] = $sale->discount_amount;
                }

                // elseif ($sale->group == \App\Define\Sale::GROUP_FREESHIP) {
                //     $p['province'] = $sale->province;
                //     $p['discount'] = $sale->discount;
                // }

                switch ($sale->type) {
                    case \App\Define\Sale::TYPE_PRODUCT:
                        $p['product_id']            = $sale->object_id;
                        $products[$sale->object_id] = $p;
                        break;
                    case \App\Define\Sale::TYPE_SUPPLIER:
                        $_products = Product::where('status', 1)->where('product_supplier_id', $sale->object_id)->where('parent_id', 0)->get();
                        foreach ($_products as $_p) {
                            $p['product_id'] = $_p->id;
                            $products[$_p->id] = $p;
                        }
                        break;
                    case \App\Define\Sale::TYPE_BRAND:
                        $_products = Product::where('status', 1)->where('brand_id', $sale->object_id)->where('parent_id', 0)->get();
                        foreach ($_products as $_p) {
                            $p['product_id'] = $_p->id;
                            $products[$_p->id] = $p;
                        }
                        break;
                }
            }
            $products = json_encode($products);
            if ($products) Cache::forever('sale_products_has_sale', $products);
        } else {
            $products = Cache::get('sale_products_has_sale');
        }
        return json_decode($products, 1);
    }
    // get lists of all sale by product
}