<?php

namespace App\Models;

class InstrumentToolType extends \Eloquent {
    protected $fillable = ['code', 'name', 'description', 'status', 'company_id', 'created_by'];

    public static function rules($id = 0)
    {
        return [
            'type_code'  => 'required|unique:instrument_tool_types,code' . ($id == 0 ? '' : ',' . $id),
            'type_name'  => 'required'
        ];
    }
    public static function getActiveTypes($companyId)
    {
        $results=[];
        $tmp = InstrumentToolType::where('company_id', $companyId)->where('status', 1)->orderBy("id", 'DESC')->get();
        foreach ($tmp as $type) {
            $results[$type->id] = $type->code;
        }
        return $results;
    }
    public static function getName($id)
    {
        return InstrumentToolType::where('id', $id)->pluck('code')->first();
    }
}
