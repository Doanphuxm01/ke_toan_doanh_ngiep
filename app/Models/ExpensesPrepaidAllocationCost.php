<?php
namespace App\Models;

class ExpensesPrepaidAllocationCost extends \Eloquent {
    protected $table = "expenses_prepaid_allocation_cost";
	protected $fillable = [ 
        'voucher_id', 
        'expenses_prepaid_id', 
        'department_id', 
        'money_amount', 
        'expense_account', 
        'cost_item', 
    ];
    public static function getValue($expenses_prepaid_id, $voucher_id)
    {   
        $costs = ExpensesPrepaidAllocationCost::where('expenses_prepaid_id', $expenses_prepaid_id)->where('voucher_id', $voucher_id)->get();
        foreach ($costs as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
}