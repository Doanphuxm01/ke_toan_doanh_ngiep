<?php

namespace App;

class EmailTemplate extends \Eloquent {
	public static function rules($id = 0) {
        return [
        	'name' 		=> 'required|max:255',
			'content' 	=> 'required',
        ];
    }

    protected $fillable = [ 'name', 'content', 'status', 'created_by' ];
}