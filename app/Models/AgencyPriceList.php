<?php

namespace App;

class AgencyPriceList extends \Eloquent {
    protected $fillable = ['agency_price_id', 'greater_than_equal', 'less_than', 'price_ipo', 'created_by'];
}