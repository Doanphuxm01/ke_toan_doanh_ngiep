<?php
namespace App;

class OrderProcess extends \Eloquent {

	protected $fillable = [ "order_id", "processed_type", "processed_from", "processed_to", "processed_by", "processed_at", "note" ];
}