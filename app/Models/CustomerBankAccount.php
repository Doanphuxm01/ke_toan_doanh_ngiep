<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class CustomerBankAccount extends \Eloquent {
    protected $fillable = [ 'bank_name', 'bank_id', 'bank_no', 'bank_branch', 'is_default', 'customer_id'];
}