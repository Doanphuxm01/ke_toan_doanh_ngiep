<?php namespace App;

use Illuminate\Support\Facades\DB as DB;

class VnptError extends \Eloquent {

    protected $table = "vnpt_errors";


    public static function getByAll($errorCode = '', $del = 0, $time = 86400) //in 24h = 3600*24=86400)
    {
        $redis      = \App::make('redis');
        if( $del ) $redis->del("vnpt_error" );
        $vnptErrors = $redis->get("vnpt_error");

        if ( !$vnptErrors ) {
            $vnptErrors = VnptError::pluck('des_vi', 'error_code')->toArray();
            $redis->set( "vnpt_error", json_encode($vnptErrors) );
            $redis->expire( "vnpt_error", $time);
        } else {
            $vnptErrors = json_decode( $vnptErrors, 1 );
        }

        return $vnptErrors;
    }

}