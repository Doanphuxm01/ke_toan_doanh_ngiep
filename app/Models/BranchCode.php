<?php

namespace App\Models;

// use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class BranchCode extends Model
{
    // public $timestamps = true;
    // protected $connection = "mysql";
    // protected $table = "accounts";
    protected $fillable = ['code', 'name', 'property', 'is_system', 'description', 'parent_id', 'created_by', 'follow'];
    // public static function rules( $group = 0, $code = 0){
    //     return [
    //         'code' => 'required|max:20|'.Rule::unique('accounts')->ignore($code, 'code').'|regex:/^'.$group.'[0-9]{2}VN[0-9]{2}/',
    //         'name'   => 'required|max:200|',
    //         'property'  => 'required|max:50',
    //         'parent_id' => 'required|max:100',
    //         'description'    => 'required|max:200',
    //         'follow'  => 'nullable',

    //     ];
    // }

    // public function companies(){
    //     return $this->hasOne("App\Models\Company", "id", "company_id");
    // }

    public static function getBranchCode() {
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        return $branchCodes;
    }
}
