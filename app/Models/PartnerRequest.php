<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerRequest extends \Eloquent {
    protected $fillable = ['customer_id', 'target', 'status', 'old'];

    public static function boot()
    {
        parent::boot();

        static::updated(function($page)
        {
            self::clearCache();
        });

        static::deleted(function($page)
        {
            self::clearCache();
        });

        static::saved(function($page)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('partner_request_counter');
    }

    public static function getNumberInQueue()
    {
        $counter = 0;
        if (!Cache::has('partner_request_counter')) {
            $counter = PartnerRequest::where('status', 0)->count();
            Cache::forever('partner_request_counter', $counter);
        } else {
            $counter = Cache::get('partner_request_counter');
        }

        return $counter;
    }
}