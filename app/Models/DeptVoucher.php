<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeptVoucher extends Model
{
    protected $connection = "mysql";
    protected $table = "dept_vouchers";
}