<?php
namespace App\Models;

class ExpensesPrepaidAllocation extends \Eloquent {
    protected $table = "expenses_prepaid_allocation";
	protected $fillable = [ 
        'voucher_no', 
        'description',
        'month',
        'year',
        'status',
        'voucher_date', 
        'company_id',
        'created_by'
    ];

    public static function check($month, $year)
    {
        $result = FixedAssetDepreciation::where('month', $month)->where('year', $year)->get();
        return $result;
    }
    public static function getTotal($id)
    {   
        $allo_cost = ExpensesPrepaidAllocationCost::where('voucher_id', $id)->get();
        $result = 0;
        foreach ($allo_cost as $key => $value) {
            $result += $value->money_amount;
        }
        return $result;
    }
    public static function amount($expenses_prepaid_id, $voucher_id)
    {   
        $result = 0;
        $costs = ExpensesPrepaidAllocationCost::where('expenses_prepaid_id', $expenses_prepaid_id)->where('voucher_id', $voucher_id)->get();
        foreach ($costs as $key => $value) {
           $result += $value->money_amount;
        }
        return $result;
    }
    public static function getAllocationInfo($expenses_prepaid_id)
    {
        $result = ExpensesPrepaidAllocation::where('expenses_prepaid_id', $expenses_prepaid_id)->get();
        return $result;
    }
    public static function getAllocationCost($expenses_prepaid_id, $voucher_id)
    {
        $result = ExpensesPrepaidAllocationCost::where('expenses_prepaid_id', $expenses_prepaid_id)->where('voucher_id', $voucher_id)->get();
        return $result;
    }
}