<?php
namespace App\Models;

class VoucherCollection extends \Eloquent {
    protected $table = "voucher_collection";
	protected $fillable = [ 
        'expenses_prepaid_id', 
        'accounting_id', 
        'instrument_tool_id', 
        
    ];
}