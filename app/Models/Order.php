<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class Order extends \Eloquent {

    public static function rules($id = 0, $status = -2) {
        switch ($status) {
            case -2:
                return [
                    "info"          => "required",
                    "source"        => "required|in:" . implode(',', Define\Order::getAvailableSources()),
                    "products"      => "required|array|min:1",
                    "products.*"    => "required|integer|min:1",
                    "promotion_products"    => "required|array|min:1",
                    "promotion_products.*"  => "required|between:0.00,99.99",
                    "created_at"    => 'date_format:d/m/Y H:i',
                    'message'       => 'max:255',
                ];
            case Define\Order::STATUS_RECORDED:
                return [
                    "status"            => "required|in:" . implode(',', self::getOptionByStatus($status)),
                    "discount_amount"   => "integer",
                    'discount_note' => "max:255",
                    "note"          => "max:255",
                ];
            case Define\Order::STATUS_ACCEPTED:
                return [
                    "status"            => "required|in:" . implode(',', self::getOptionByStatus($status)),
                    "diposit_amount"    => "integer",
                ];
        }
    }

    public static function deliveryRules() {

        return [
            "delivery_provider" => "required|in:" . implode(',', Define\Order::getDeliveryProviders()),
            "delivery_method"   => "required",
            'delivery_fee'      => 'required|min:1',
            'note'              => 'max:255',
        ];
    }

    public static function shippingRuleAddress($existedAccount = 0) {
        return [
            'fullname'          => $existedAccount ? '' : 'required|max:100',
            'phone'             => 'required|max:15|regex:/[0]\d{9,11}$/',
            'province'          => 'required|max:50',
            'district'          => 'required|max:50',
            'address'           => 'required|max:255',
        ];
    }

	protected $fillable = ['code', 'total_amount', 'discount_amount', 'discount_note', 'discount_created_by', 'payment_method', 'payment_status', 'payment_note', 'delivery_provider', 'delivery_method', 'delivery_option', 'delivery_status', 'delivery_fee', 'delivery_note', 'customer_id', 'delivery_email', 'delivery_fullname', 'contact_id', 'delivery_phone', 'delivery_province', 'delivery_district', 'delivery_address', 'message', 'weight', 'cod_fee', 'cod_note', 'bank_code', 'bank_fee', 'bank_name', 'gateway_id', 'bank_status', 'bank_message', 'bank_counter', 'campaign_trans_id', 'synced_mail', 'vat', 'company_name', 'company_registration_number', 'company_address', 'source', 'created_by', 'status', 'note', 'free_shipping_id', 'sale_amount', 'sale_percent', 'sale_id', 'voucher_amount', 'voucher', 'delivery_fee_last', 'delivery_ward', 'inventory_category_id', 'delivery_code', 'delivery_pick_id', 'voucher_partner', 'bonus_coin', 'use_coin', 'delivery_provider_fee', 'partner_customer_id', 'delivery_type', 'cancel_reason'];

    public function category()
    {
        return $this->belongsTo("\App\ProductCategory", "product_category_id");
    }

    public function customer()
    {
        return $this->belongsTo("\App\Customer", "customer_id");
    }

    public static function calcFree()
    {
        return rand(10,99) . '000';
    }

    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    public function contact()
    {
        return $this->belongsTo("\App\CustomerContact", "contact_id");
    }

    public function children()
    {
        return $this->hasMany("\App\Product", "parent_id");
    }

    public function details()
    {
        return $this->hasMany("\App\OrderDetail", "order_id");
    }

    public function entities()
    {
        return $this->hasMany("\App\ProductEntity");
    }

    public static function genCode($isAdmin = 0)
    {
        $auto = '0';
        if (\Storage::has('increase_code_auto.txt'))
            $auto = \Storage::disk('local')->get('increase_code_auto.txt');
        $transId    = str_pad($auto, 5, '0', STR_PAD_LEFT);
        $code       = "";
        if ($isAdmin == 0) {
            $code = 'SBA_'. date('dmHi') . $transId;
        } elseif ($isAdmin == -1) {
            $code = 'CTV_'. date('dmHi') . $transId;
        } else {
            $code = 'SB1_'. date('dmHi') . $transId;
        }
        \Storage::disk('local')->put('increase_code_auto.txt', $transId + 1);
        return $code;
    }

    public static function getOptionByStatus($status)
    {
        switch ($status) {
            case \App\Define\Order::STATUS_RECORDED:
                return [\App\Define\Order::STATUS_ACCEPTED, \App\Define\Order::STATUS_REJECTED];
            case \App\Define\Order::STATUS_ACCEPTED:
                return [\App\Define\Order::STATUS_DELIVERED];
        }
    }

    public static function getOptionForSelectByStatus($status, $deliveryStatus = \App\Define\Order::DELIVERY_STATUS_READY)
    {
        switch ($status) {
            case \App\Define\Order::STATUS_RECORDED:
                return [\App\Define\Order::STATUS_ACCEPTED => trans('orders.order_status.' . \App\Define\Order::STATUS_ACCEPTED), \App\Define\Order::STATUS_REJECTED => trans('orders.order_status.' . \App\Define\Order::STATUS_REJECTED)];
            case \App\Define\Order::STATUS_ACCEPTED:
                return [\App\Define\Order::STATUS_DELIVERED => trans('orders.order_status.' . \App\Define\Order::STATUS_DELIVERED), \App\Define\Order::STATUS_REJECTED => trans('orders.order_status.' . \App\Define\Order::STATUS_REJECTED)];
            case \App\Define\Order::STATUS_DELIVERED:
                switch ($deliveryStatus) {
                    case \App\Define\Order::DELIVERY_STATUS_WAIT:
                    case \App\Define\Order::DELIVERY_STATUS_READY:
                    case \App\Define\Order::DELIVERY_STATUS_CONFIRM_COD:
                        return [\App\Define\Order::STATUS_CANCELED => trans('orders.order_status.' . \App\Define\Order::STATUS_CANCELED)];
                    case \App\Define\Order::DELIVERY_STATUS_DELIVERING:
                        return [\App\Define\Order::STATUS_COMPLETED => trans('orders.order_status.' . \App\Define\Order::STATUS_COMPLETED), \App\Define\Order::STATUS_CANCELED => trans('orders.order_status.' . \App\Define\Order::STATUS_CANCELED)];
                }
        }
    }

    public static function boot()
    {
        parent::boot();

        static::updating(function ($order) {
            self::logProcess($order);

            if (isset($order->send_mail)) {
                if ($order->send_mail) {
                    event(new \App\Events\OrderSendMail($order));
                }
                unset($order->send_mail);
            }
            // if ($news->sendMail) {
            //     $changeAttributes = [];
            //     if ($news->title <> $news->getOriginal('title')) $changeAttributes['title'] = 'title';
            //     if ($news->summary <> $news->getOriginal('summary')) $changeAttributes['summary'] = 'summary';
            //     if ($news->content <> $news->getOriginal('content')) $changeAttributes['content'] = 'content';
            //     if ($news->image <> $news->getOriginal('image')) $changeAttributes['image'] = 'image';
            //     if ($news->category_id <> $news->getOriginal('category_id')) $changeAttributes['category_id'] = 'category_id';
            //     if ($news->status <> $news->getOriginal('status')) $changeAttributes['status'] = 'status';
            //     if ($news->featured <> $news->getOriginal('featured')) $changeAttributes['featured'] = 'featured';
            //     if ($news->closed <> $news->getOriginal('closed')) $changeAttributes['closed'] = 'closed';
            //     if ($news->paid <> $news->getOriginal('paid')) $changeAttributes['paid'] = 'paid';
            //     if (!count($changeAttributes)) {
            //         unset($news->sendMail);
            //         return;
            //     }
            //     try {
            //         $user = \Auth::guard('admin')->user();
            //         $when = Carbon::now()->addSeconds(5);
            //         if ($news->created_by <> $user->id) { // => admin cap nhat
            //             $creator = User::find($news->created_by);
            //             // var_dump(view('emails.update_post', ['news' => $news, 'user' => $user, 'changeAttributes' => $changeAttributes, 'original' =>  $news->original, 'creator' => $creator])->render());exit;
            //             // \Log::info("<>: " . json_encode($creator));
            //             // \Log::info("<>: " . json_encode($user));
            //             //'email' => $creator->email, 'name' => $creator->fullname
            //             \Mail::to([$creator->fullname => $creator->email])->cc([$user->fullname => $user->email])->later($when, new \App\Mail\UpdatePost($user, $changeAttributes, $news, $news->original));

            //             // \Mail::to([['email' => $creator->email, 'name' => $creator->fullname]])->cc([['email' => env('MAIL_CC_ADDRESS'), 'name' => env('MAIL_CC_NAME')], ['email' => $user->email, 'name' => $user->fullname]])->later($when, new \App\Mail\UpdatePost($user, $changeAttributes, $news, $news->original, $creator));

            //             // $user->notify((new \App\Notifications\NewsUpdated($user, $news))->delay($when));
            //         } else {
            //             $creator = User::find($news->created_by);
            //             // var_dump(view('emails.update_post', ['news' => $news, 'user' => $user, 'changeAttributes' => $changeAttributes,  'original' => $news->original, 'creator' => null])->render());exit;
            //             // \Log::info("==: " . json_encode($user));
            //             \Mail::to([$user->fullname => $user->email])->later($when, new \App\Mail\UpdatePost($user, $changeAttributes, $news, $news->original, $creator));
            //             //\Mail::to([['email' => $user->email, 'name' => $user->fullname]])->cc([['email' => env('MAIL_CC_ADDRESS'), 'name' => env('MAIL_CC_NAME')]])->later($when, new \App\Mail\UpdatePost($user, $changeAttributes, $news, $news->original));

            //             // $user->notify((new \App\Notifications\NewsUpdated($user, $news, $creator))->delay($when));
            //         }
            //     } catch (Exception $e) {
            //         \Log::info('#Email: ' . $title . '<br/>Description: ' . $e->getMessage());
            //     }
            // }
        });

        static::updated(function($order)
        {
        });

        static::creating(function($order)
        {
        });

        static::created(function($order)
        {
            $order->delivery_status = \App\Define\Order::DELIVERY_STATUS_WAIT;
            $order->save();
        });

        static::deleted(function($order)
        {
        });

        static::saved(function($order)
        {
        });

        static::saving(function($order)
        {
        });
    }

    public static function logProcess($order)
    {
        $time = time();
        if ($order->payment_method <> $order->getOriginal('payment_method') && !is_null($order->getOriginal('payment_method'))) {
            OrderProcess::create([
                'order_id'      => $order->id,
                'processed_type'=> \App\Define\Order::LABEL_PAYMENT_METHOD,
                'processed_from'=> $order->getOriginal('payment_method'),
                'processed_to'  => $order->payment_method,
                'processed_by'  => auth()->guard('admin')->user() ? auth()->guard('admin')->user()->id : null,
                'processed_at'  => $time,
                'note'          => $order->note,
            ]);
        }
        if ($order->payment_status <> $order->getOriginal('payment_status') && !is_null($order->getOriginal('payment_status'))) {
            OrderProcess::create([
                'order_id'      => $order->id,
                'processed_type'=> \App\Define\Order::LABEL_PAYMENT_STATUS,
                'processed_from'=> $order->getOriginal('payment_status'),
                'processed_to'  => $order->payment_status,
                'processed_by'  => auth()->guard('admin')->user() ? auth()->guard('admin')->user()->id : null,
                'processed_at'  => $time,
                'note'          => $order->note,
            ]);
        }
        if ($order->delivery_provider <> $order->getOriginal('delivery_provider') && !is_null($order->getOriginal('delivery_provider'))) {
            OrderProcess::create([
                'order_id'      => $order->id,
                'processed_type'=> \App\Define\Order::LABEL_DELIVERY_PROVIDER,
                'processed_from'=> $order->getOriginal('delivery_provider'),
                'processed_to'  => $order->delivery_provider,
                'processed_by'  => auth()->guard('admin')->user() ? auth()->guard('admin')->user()->id : null,
                'processed_at'  => $time,
                'note'          => $order->note,
            ]);
        }
        if ($order->delivery_status <> $order->getOriginal('delivery_status') && !is_null($order->getOriginal('delivery_status'))) {
            OrderProcess::create([
                'order_id'      => $order->id,
                'processed_type'=> \App\Define\Order::LABEL_DELIVERY_STATUS,
                'processed_from'=> $order->getOriginal('delivery_status'),
                'processed_to'  => $order->delivery_status,
                'processed_by'  => auth()->guard('admin')->user() ? auth()->guard('admin')->user()->id : null,
                'processed_at'  => $time,
                'note'          => $order->note,
            ]);
        }
        if ($order->status <> $order->getOriginal('status')) { // && !is_null($order->getOriginal('status'))
            OrderProcess::create([
                'order_id'      => $order->id,
                'processed_type'=> \App\Define\Order::LABEL_STATUS,
                'processed_from'=> $order->getOriginal('status'),
                'processed_to'  => $order->status,
                'processed_by'  => auth()->guard('admin')->user() ? auth()->guard('admin')->user()->id : null,
                'processed_at'  => $time,
                'note'          => $order->note,
            ]);
        }
    }

    public function processes()
    {
        return $this->hasMany("\App\OrderProcess", "order_id");
    }

    public function instalment()
    {
        return $this->hasMany("\App\OrderInstalment", "order_id");
    }
}