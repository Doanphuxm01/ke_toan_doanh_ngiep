<?php

namespace App\Models;

class InstrumentToolAllocationInformation extends \Eloquent {
    protected $fillable = ['instrument_tool_id', 'quantity', 'allocation_rate', 'department_id', 'expense_account', 'cost_item'];
    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }
}
