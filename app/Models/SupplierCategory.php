<?php namespace App;

use Illuminate\Support\Facades\Cache;

class SupplierCategory extends \Eloquent {
    public $timestamps = false;
    protected $fillable = [ 'product_category_id', 'supplier_id', 'position' ];

    public static function boot()
    {
        parent::boot();

        static::updated(function($brand)
        {
            self::clearCache();
        });

        static::deleted(function($brand)
        {
            self::clearCache();
        });

        static::created(function($brand)
        {
            self::clearCache();
        });

        static::saved(function($brand)
        {
            self::clearCache();
        });
    }

    public static function clearCache()
    {
        Cache::forget('suppliers_categories');
    }
}