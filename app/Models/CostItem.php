<?php

namespace App\Models;

class CostItem extends \Eloquent {
    protected $fillable = ['cost_item', 'description', 'interpretation', 'status', 'company_id', 'created_by'];

    public static function rules($id = 0)
    {
        return [
            'new_cost_item'  => 'required|unique:cost_items,cost_item' . ($id == 0 ? '' : ',' . $id),
        ];
    }
    
    public static function getActiveCostItem($companyId)
    {
        $results=[];
        $tmp = CostItem::where('company_id', $companyId)->where('status', 1)->orderBy("id", 'DESC')->get();
        foreach ($tmp as $cost_item) {
            $results[$cost_item->cost_item] = $cost_item->cost_item;
        }
        return $results;
    }
    public static function getCostItem($cost_item)
    {
        $result = CostItem::where('cost_item', $cost_item)->pluck('cost_item')->first();
        return $result;
    }

    public static function checkUpdateDelete($cost_item)
    {
        $result = FixedAssetAllocationCost::where('cost_item', $cost_item)->get();
        return $result;
    }
}
