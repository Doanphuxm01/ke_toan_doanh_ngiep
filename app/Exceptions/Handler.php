<?php

namespace App\Exceptions;

use App\Elibs\Debug;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    static $__countEx = false;
    public function report(Exception $exception)
    {
        $email = @(isset(auth()->user()->email)) ? auth()->user()->email : 'EMail Người dùng vô danh';
        $account = @(isset(auth()->user()->fullname)) ? auth()->user()->fullname : 'Name Người dùng vô danh' ;
        $code = 0;
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface) {
            $code = $exception->getStatusCode();
        }
        if (!self::$__countEx && !in_array($code,[404,300])) {
            self::$__countEx = true;
            $msg = "Bctech Dự Án Kế Toán Thông Báo Lỗi Máy Chủ Như Sau\n" ;
            $msg .= "\n\nMessage: " .       $exception->getMessage();
            $msg .= "\nEmail: " .           $email;
            $msg .= "\nAccount: " .         $account;
            $msg .= "\nStatusCode: " .      $code;
            $msg .= "\nFile: " .            $exception->getFile() . ':' . $exception->getLine();
            $msg .= "\nREQUEST_URI: " .     @$_SERVER['REQUEST_URI'];
            $msg .= "\nREMOTE_ADDR: " .     @$_SERVER['REMOTE_ADDR'];
            $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
            $msg .= "\nHTTP_REFERER: " .    @$_SERVER['HTTP_REFERER'];
            $msg .= "\nREQUEST_METHOD: " .  @$_SERVER['REQUEST_METHOD'];
            $msg .= "\nSERVER_NAME: " .     @$_SERVER['SERVER_NAME'];
            $msg .= "\nHTTP_HOST: " .       @$_SERVER['HTTP_HOST'];
            Debug::pushNotification($msg);
            parent::report($exception);
        }

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
        dd($exception);
        if (substr($request->path(), 0, 5) == "admin") return parent::render($request, $exception);
        $message = $exception->getMessage();
        if ($exception instanceof ModelNotFoundException) {
            $exception = new NotFoundHttpException($message, $exception);
        }
        \Session::flash('error_404', $message);
        return redirect()->route('frontend.404');
    }
}
