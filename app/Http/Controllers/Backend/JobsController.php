<?php

namespace App\Http\Controllers\Backend;

use App\Models\Job;
use App\Models\BranchCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class JobsController extends Controller
{
    public function index(Request $request) {
        $query = "1=1";
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $type         = intval($request->input('type', -1));
        $code           = $request->input('code');
        if($code) $query .= " AND (code like '%" . $code . "%')";
        if($type <> -1) $query .= " AND type = {$type}";
        $jobs = Job::whereRaw($query)->orderBy('code')->where('company_id', session('current_company'))->paginate($page_num);
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        return view('backend.jobs.index', compact('jobs', 'branchCodes'));
    }

    public function create(Request $request) {
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        return view('backend.jobs.create', compact('branchCodes'));
    }

    public function store(Request $request)
    {   
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), Job::rules());
        $validator->setAttributeNames(trans('jobs'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['created_by'] = $request->user()->id;
        $data['company_id'] = session('current_company');
        Job::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.jobs.index');
    }

    public function edit(Request $request, $id)
    {
        $job = Job::find(intval($id));
        if (is_null($job)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.jobs.index');
        }
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        return view('backend.jobs.edit', compact('job', 'branchCodes'));
    }

    public function update(Request $request, $id)
    {
        $job = Job::find(intval($id));
        if (is_null($job)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.jobs.index');
        }
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), job::rules(intval($id), intval($request->code)));
        $validator->setAttributeNames(trans('jobs'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $job->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.jobs.index');
    }

    public function destroy(Request $request, $id)
    {
        $job = Job::find(intval($id));
        if (is_null($job)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.jobs.index');
        }
        
        $job->deleted_by = $request->user()->id;
        $job->save();
        $job->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.jobs.index');
    }
}
