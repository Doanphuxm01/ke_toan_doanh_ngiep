<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use App\Contact as Contact;
use App\ContactDetail as ContactDetail;

class ContactsController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $status                 = intval($request->input('status', -1));
        $page_num               = intval($request->input('page_num', 10));

        if($status <> -1) $query .= " AND status = {$status}";
        $contacts = Contact::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);

        return view('backend.contacts.index', compact('contacts'));
    }

    public function create()
    {
        return view('backend.contacts.create');
    }

    public function show(Request $request, $id)
    {
        $contact   = Contact::find(intval($id));
        if (is_null($contact)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.contacts.index');
        }

        $query = "1=1";
        $page_num = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $fullname = $request->input('fullname');

        if($fullname) $query .= " AND (fullname like '%" . $fullname . "%' OR email like '%" . $fullname . "%')";
        $details = $contact->emails()->whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        return view('backend.contacts.show', compact('contact', 'details'));
    }

    public function edit($id)
    {
        $contact   = Contact::find(intval($id));
        if (is_null($contact) || !is_null($contact->source)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.contacts.index');
        }
        return view('backend.contacts.edit', compact('contact'));
    }

    public function store(Request $request)
    {
        $request->merge(['status' => $request->input('status', 0)]);
        $validator = \Validator::make($data = $request->all(), Contact::rules());
        $validator->setAttributeNames(trans('contacts'));

        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        if ($request->hasFile('logo')) {
            $logo  = $request->file('logo');
            $name   = str_slug($data['name']) . '_' . date("d_m_Y");
            $ext    = pathinfo($logo->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['logo'] = $name . '.' . $ext;
            $logo->move(\Config::get('upload.contact'), $data['logo']);
            // origin
            $path = \Config::get('upload.contact') . $data['logo'];
            // resize
            $logo = \Image::make($path);
            if ($logo->height() > $logo->width()) {
                $logo->resize(60, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $logo->save(\Config::get('upload.contact') . $data['logo']);

            } else {
                $logo->resize(null, 60, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $logo->save(\Config::get('upload.contact') . $data['logo']);
            }

            $data['logo'] = \Config::get('upload.contact') . $data['logo'];
        }

        Contact::create($data);

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.contacts.index');
    }

    public function update(Request $request, $id)
    {
        $contact   = Contact::find(intval($id));
        if (is_null($contact) || !is_null($contact->source)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.contacts.index');
        }

        $request->merge(['status' => $request->input('status', 0)]);
        $validator = \Validator::make($data = $request->only(['status', 'name']), Contact::rules(intval($id)));
        $validator->setAttributeNames(trans('contacts'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $contact->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.contacts.index');
    }

    public function destroy(Request $request, $id)
    {
        $contact = Contact::find(intval($id));
        if (is_null($contact) || !is_null($contact->source)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.contacts.index');
        }

        $contact->emails()->detach();
        $contact->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.contacts.index');
    }

    public function search(Request $request)
    {
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $tag = $request->input("tag");
                $tag = trim($tag['term']);
                if (!$tag) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm thấy từ khoá", 1);
                }
                $tmp        = [];
                $query      = "(name like '%" . $tag . "%' OR note like '%" . $tag . "%')";
                $contacts   = Contact::where('status', 1)->whereRaw($query)->get();
                foreach ($contacts as $contact) {
                    $count = $contact->emails()->count();
                    array_push($tmp, [
                        'id'    => $contact->id,
                        'name'  => $contact->name,
                        'info'  => $contact->name . ' (' . $count . ')',
                        'link'  => e(\HTML::link(route('admin.contacts.show', $contact->id), $contact->name . ' (' . $count . ')', ['title' => trans('system.detail'), 'target' => '_blank']))//, 'style' => 'color: white;'
                    ]);
                }
                $response['message'] = json_encode($tmp);
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getById(Request $request)
    {
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $type = $request->input("type");
                if (!in_array($type, \App\Define\Contact::getSearchContacts())) {
                    $statusCode = 400;
                    throw new \Exception("Phân loại tìm theo không đúng.", 1);
                }
                $searchId = intval($request->input("search_id"));
                if (!$searchId) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm lựa chọn của bạn.", 1);
                }
                $tmp        = [];
                $query      = "status = 1";
                $contactIds = $request->input("contact_ids");
                $counter    = 1 ;
                if ($contactIds) {
                    $temp = explode(',', $contactIds);
                    foreach ($temp as $t) {
                        if (!is_numeric($t)) {
                            $statusCode = 400;
                            throw new \Exception("Dữ liệu không toàn vẹn.", 1);
                        }
                    }
                    $query .= " AND id NOT IN(" . $contactIds . ")";
                    $counter = count($temp) + 1;
                    $contactIds .= ",";
                }

                $contact   = Contact::whereRaw($query)->where('id', $searchId)->first();
                if (is_null($contact)) {
                    $statusCode = 400;
                    throw new \Exception("Dữ liệu đã bị thay đổi.", 1);
                }
                $tmp = [
                    'id'    => $contact->id,
                    'name'  => $contact->name,
                    'number'=> count(explode(',', $contact->emails))
                ];

                $response['message'] = view('backend.coupons.ajaxContact', compact('tmp', 'counter'))->render();
                $response['contactIds'] = $contactIds . $tmp['id'];
                $response['contactId']  = $tmp['id'];
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function detachEmail(Request $request, $contactId, $emailId)
    {
        $contact = Contact::find(intval($contactId));
        if (is_null($contact) || !is_null($contact->source)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.contacts.index');
        }

        // remove from tracsaction => ko can

        $contact->emails()->detach(intval($emailId));

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
}