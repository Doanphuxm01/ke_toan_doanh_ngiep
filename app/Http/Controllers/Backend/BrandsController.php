<?php

namespace App\Http\Controllers\Backend;

use App\Brand;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class BrandsController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $status = intval($request->input('status', -1));
        $is_top = intval($request->input('is_top'));
        $name = $request->input('name');
        $page_num = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        if($name) $query .= " AND name like '%" . $name . "%'";
        if($status <> -1) $query .= " AND status = {$status}";
        if($is_top) $query .= " AND is_top = {$is_top}";
        $brands = Brand::whereRaw($query)->orderBy('name')->orderBy('updated_at', 'DESC')->paginate($page_num);

        return view('backend.brands.index', compact('brands'));
    }

    public function create()
    {
        return view('backend.brands.create');
    }

    public function show($id)
    {
        $brand = Brand::find(intval($id));
        if (is_null($brand)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.brands.index');
        }

        return view('backend.brands.show', compact('brand'));
    }

    public function edit($id)
    {
        $brand = Brand::find(intval($id));
        if (is_null($brand)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.brands.index');
        }
        return view('backend.brands.edit', compact('brand'));
    }

    public function store(Request $request)
    {   dd($request->all());
        $request->merge(['status' => $request->input('status', 0), 'is_top' => $request->input('is_top', 0)]);
        $validator = \Validator::make($data = $request->all(), Brand::rules());
        $validator->setAttributeNames(trans('brands'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        if ($request->hasFile('logo')) {
            $logo  = $request->file('logo');
            $name   = str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($logo->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['logo'] = $name . '.' . $ext;
            $logo->move(config('upload.brand'), $data['logo']);
            $path = config('upload.brand') . $data['logo'];
            $image = \Image::make($path);
            if ($image->height() > $image->width()) {
                $image->resize(\App\Define\Constant::IMAGE_BRAND_HEIGHT, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(config('upload.brand') . $data['logo']);
            } else {
                $image->resize(null, \App\Define\Constant::IMAGE_BRAND_WIDTH, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save(config('upload.brand') . $data['logo']);
            }
            $data['logo'] = config('upload.brand') . $data['logo'];
        }
        if ($request->hasFile('image1')) {
            $image1 = $request->file('image1');
            $name   = 'img1_' . str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($image1->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image1'] = $name . '.' . $ext;
            $image1->move(config('upload.brand'), $data['image1']);
            $data['image1'] = config('upload.brand') . $data['image1'];
        }
        if ($request->hasFile('image2')) {
            $image2 = $request->file('image2');
            $name   = 'img2_' . str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($image2->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image2'] = $name . '.' . $ext;
            $image2->move(config('upload.brand'), $data['image2']);
            $data['image2'] = config('upload.brand') . $data['image2'];
        }
        if ($request->hasFile('image3')) {
            $image3 = $request->file('image3');
            $name   = 'img3_' . str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($image3->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image3'] = $name . '.' . $ext;
            $image3->move(config('upload.brand'), $data['image3']);
            $data['image3'] = config('upload.brand') . $data['image3'];
        }

        Brand::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.brands.index');
    }

    public function update(Request $request, $id)
    {
        $brand = Brand::find(intval($id));
        if (is_null($brand)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.brands.index');
        }

        $request->merge(['status' => $request->input('status', 0), 'is_top' => $request->input('is_top', 0)]);
        $validator = \Validator::make($data = $request->all(), Brand::rules($brand->id));
        $validator->setAttributeNames(trans('brands'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        if ($request->hasFile('logo')) {
            if (\File::exists(public_path() . '/' . $brand->logo)) \File::delete(public_path() . '/' . $brand->logo);
            $logo  = $request->file('logo');
            $name   = str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($logo->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['logo'] = $name . '.' . $ext;
            $logo->move(config('upload.brand'), $data['logo']);
            $path = config('upload.brand') . $data['logo'];
            $logo = \Image::make($path);
            if ($logo->height() > $logo->width()) {
                $logo->resize(\App\Define\Constant::IMAGE_BRAND_HEIGHT, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $logo->save(config('upload.brand') . $data['logo']);
            } else {
                $logo->resize(null, \App\Define\Constant::IMAGE_BRAND_WIDTH, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $logo->save(config('upload.brand') . $data['logo']);
            }
            $data['logo'] = config('upload.brand') . $data['logo'];
        }
        if ($request->hasFile('image1')) {
            if (\File::exists(public_path() . '/' . $brand->image1)) \File::delete(public_path() . '/' . $brand->image1);
            $image1 = $request->file('image1');
            $name   = 'img1_' . str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($image1->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image1'] = $name . '.' . $ext;
            $image1->move(config('upload.brand'), $data['image1']);
            $data['image1'] = config('upload.brand') . $data['image1'];
        }
        if ($request->hasFile('image2')) {
            if (\File::exists(public_path() . '/' . $brand->image2)) \File::delete(public_path() . '/' . $brand->image2);
            $image2 = $request->file('image2');
            $name   = 'img2_' . str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($image2->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image2'] = $name . '.' . $ext;
            $image2->move(config('upload.brand'), $data['image2']);
            $data['image2'] = config('upload.brand') . $data['image2'];
        }
        if ($request->hasFile('image3')) {
            if (\File::exists(public_path() . '/' . $brand->image3)) \File::delete(public_path() . '/' . $brand->image3);
            $image3 = $request->file('image3');
            $name   = 'img3_' . str_slug($data['name']) . '_' . time();
            $ext    = pathinfo($image3->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image3'] = $name . '.' . $ext;
            $image3->move(config('upload.brand'), $data['image3']);
            $data['image3'] = config('upload.brand') . $data['image3'];
        }
        $brand->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.brands.index');
    }

    public function destroy($id)
    {
        $brand = Brand::find(intval($id));
        if (is_null($brand)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.brands.index');
        }

        if ($brand->categories()->count()) {
            Session::flash('message', "Thương hiệu đã có danh mục.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.brands.index');
        }

        if ($brand->products()->count()) {
            Session::flash('message', "Thương hiệu đã có sản phẩm, cần xoá bỏ sản phẩm thuộc thương hiệu trước.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.brands.index');
        }
        //$brand->categories()->detach();
        // delete image
        if (\File::exists(public_path() . '/' . $brand->logo)) \File::delete(public_path() . '/' . $brand->logo);
        if (\File::exists(public_path() . '/' . $brand->image1)) \File::delete(public_path() . '/' . $brand->image1);
        if (\File::exists(public_path() . '/' . $brand->image2)) \File::delete(public_path() . '/' . $brand->image2);
        if (\File::exists(public_path() . '/' . $brand->image3)) \File::delete(public_path() . '/' . $brand->image3);
        $brand->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.brands.index');
    }
}