<?php

namespace App\Http\Controllers\Backend;

use App\News;
use App\NewsCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class NewsCategoriesController extends Controller
{
    public function index(Request $request)
    {
        $categories = NewsCategory::where('parent_id', 0)->orderBy('order')->get();

        for ($i=0; $i < $categories->count(); $i++) {
            $categories[$i]->children = NewsCategory::where('parent_id', $categories[$i]->id)->get();
        }

        return view('backend.news-categories.index', compact('categories'));
    }

    public function create()
    {
        $categories = NewsCategory::where('parent_id', 0)->where('status', 1)->pluck("name", 'id')->all();

        return view('backend.news-categories.create', compact('categories'));
    }

    public function show(Request $request, $id)
    {
        return redirect()->route('admin.news-categories.index');
    }

    public function edit(Request $request, $id)
    {
        $id = intval($id);
        $category   = NewsCategory::find($id);
        if(is_null($category)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.news-categories.index');
        }

        if ( $category && $category->is_system ) {
            Session::flash('message', 'Nhóm hệ thống, bạn không thể sửa.');
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.news-categories.index');
        }

        $categories = NewsCategory::where('parent_id', 0)->where('id', '<>', $id)->where('status', 1)->pluck("name", 'id')->all();

        return view('backend.news-categories.edit', compact('categories', 'category'));
    }

    public function store(Request $request)
    {
        $request->merge(['status' => intval($request->status), 'show_homepage' => intval($request->show_homepage), 'show_menu' => intval($request->show_menu)]);
        $validation = Validator::make($data = $request->only(['parent', 'status', 'summary', 'name', 'show_homepage', 'show_menu']), NewsCategory::rules());
        $validation->setAttributeNames(trans('news.categories'));
        if ($validation->fails()) return back()->withErrors($validation)->withInput();
        $data['parent_id'] = intval($data['parent']);
        $data['is_system'] = 0;
        NewsCategory::create($data);

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.news-categories.index');
    }

    public function update(Request $request, $id)
    {
        $request->merge(['status' => intval($request->status), 'show_homepage' => intval($request->show_homepage), 'show_menu' => intval($request->show_menu)]);
        $category = NewsCategory::find(intval($id));
        if(is_null($category)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.news-categories.index');
        }

        $validation = Validator::make($data = $request->only(['parent', 'status', 'summary', 'name', 'show_homepage', 'show_menu']), NewsCategory::rules($category->id));
        $validation->setAttributeNames(trans('news.categories'));
        if ($validation->fails()) return back()->withErrors($validation)->withInput();
        $data['parent_id'] = intval($data['parent']);
        $category->update($data);

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.news-categories.index');
    }

    public function destroy(Request $request, $id)
    {
        $id = intval($id);
        $category = NewsCategory::find($id);

        if ($category == null) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.news-categories.index');
        }

        if(NewsCategory::where('parent_id', $id)->count()) {
            Session::flash('message', "Danh mục đã tồn tại danh mục con.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.news-categories.index');
        }

        if(News::where('category_id', $id)->count()) {
            Session::flash('message', "Danh mục đã tồn tại bài viết.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.news-categories.index');
        }

        $category->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.news-categories.index');
    }
}