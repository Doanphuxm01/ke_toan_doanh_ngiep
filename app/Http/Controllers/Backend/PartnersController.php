<?php

namespace App\Http\Controllers\Backend;

use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use  App\Imports\PartnersImport;
class PartnersController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval($request->input('status', -1));
        $tax_code           = $request->input('tax_code');
        $name = $request->input('name');
        if($tax_code) $query .= " AND (tax_code like '%" . $tax_code . "%')";
        if($name) $query .= " AND (name like '%" . $name . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        $partners = Partner::whereRaw($query)->where('company_id', session('current_company'))->orderBy('tax_code')->paginate($page_num);
        return view('backend.partners.index', compact('partners'));
    }

    public function create(Request $request)
    {
        $latest_code = Partner::max('id');
        $new_code = $latest_code + 1;
        if(strlen( $new_code) == 1){
            $code = "PA0000".$new_code;
        }
        if(strlen( $new_code) == 2){
            $code = "PA000".$new_code;
        }
        if(strlen( $new_code) == 3){
            $code = "PA00".$new_code;
        }
        if(strlen( $new_code) == 4){
            $code = "PA0".$new_code;
        }
        if(strlen( $new_code) == 5){
            $code = "PA".$new_code;
        }
        return view('backend.partners.create', compact("code"));
    }

    public function edit(Request $request, $id)
    {
        $partner = Partner::find(intval($id));
        if (is_null($partner)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.partners.index');
        }

        return view('backend.partners.edit', compact('partner'));
    }

    public function store(Request $request)
    {  
        $request->merge(['status' => intval($request->input('status', 0))]);
        $request->merge(['is_contra' => intval($request->input('is_contra', 0))]);
        $request->merge(['is_vendor' => intval($request->input('is_vendor', 0))]);
        $request->merge(['is_customer' => intval($request->input('is_customer', 0))]);
        $validator = \Validator::make( $data = $request->all(), Partner::rules());
        $validator->setAttributeNames(trans('partners'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['created_by'] = $request->user()->id;
        $data['company_id'] = session('current_company');
        Partner::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.partners.index');
    }

    public function createBulk(Request $request)
    {
        return view('backend.partners.create_multi');
    }

    public function readBulk(Request $request)
    {
        ini_set('memory_limit', '4096M');
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
       
        if($request->ajax()) {
            try {
                $file = $request->file;
                switch ($file->getClientOriginalExtension()) {
                    case 'xlsx'||'xls':
                        $data = \Excel::toArray(new PartnersImport, $file);
                        if ($data) $data = $data[0];
                        if (count($data) == 0 || !isset($data[0][0]) || count($data[0]) != 17) {
                            throw new \Exception(trans('partners.error_modify_first_line'), 1);
                        }
                        if (!isset($data[0][0])) {
                            throw new \Exception(trans('partners.error_first_line'), 1);
                        }
                        $response['message'] = view('backend.partners.excel', compact('data'))->render();
                        break;
                    default:
                        throw new \Exception(trans('partners.error_format_file'), 1);
                }
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function saveBulk(Request $request)
    {    
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $data = $request->data;
                $token = $request->_token;
                if (!is_array($data) && count($data) == 0) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $partners = [];
                foreach($data as $d) {
                    if($d[1] == '-') {
                        $d[1] = str_replace('-','',$d[1]);
                    }
                    $validator = \Validator::make(['tax_code'=>$d[1]], ['tax_code'  => 'nullable|regex:/^[0-9]{10}(-[0-9]{3}$)?$/|unique:partners']);
                    $validator->setAttributeNames(trans('partners'));
                    if($validator->fails()){
                        throw new \Exception($validator->errors()->messages()['tax_code'][0]." ".trans('partners.number_line').$d[0]);
                    }
                    $validator = \Validator::make(['code'=>$d[2]], ['code'  => 'required|max:15|unique:partners']);
                    $validator->setAttributeNames(trans('partners'));
                    if($validator->fails()){
                        throw new \Exception($validator->errors()->messages()['code'][0]." ".trans('partners.number_line').$d[0]);
                    }
                    if (!isset($d[2])) {
                        throw new \Exception(trans('partners.error_code'));
                    }
                    if (!isset($d[3])) {
                        throw new \Exception(trans('partners.error_name'));
                    }
                    if ($d[14]=='-' && $d[15] =='-') {
                        throw new \Exception(trans('partners.error_check'));
                    }
                    $is_vendor = 0;
                    if($d[14] == 'x') {
                        $is_vendor = 1;
                    }
                    $is_customer = 0;
                    if($d[15] == 'x') {
                        $is_customer = 1;
                    }
                    $is_contra = 0;
                    if($d[16] == 'x') {
                        $is_contra = 1;
                    }
                    $activate = 0;
                    if($d[17] == 'x') {
                        $activate = 1;
                    }
                    $new_data = [
                        'tax_code'     => $d[1],
                        'code'         => $d[2],
                        'name'         => $d[3],
                        'address_1'    => str_replace('-','',$d[4]),
                        'address_2'    => str_replace('-','',$d[5]),
                        'address_3'    => str_replace('-','',$d[6]),
                        'address_4'    => str_replace('-','',$d[7]),
                        'city_code'    => str_replace('-','',$d[8]),
                        'country_code' => str_replace('-','',$d[9]),
                        'phone_no'     => str_replace('-','',$d[10]),
                        'fax_no'       => str_replace('-','',$d[11]),
                        'contact'      => str_replace('-','',$d[12]),
                        'email'        => str_replace('-','',$d[13]),
                        'is_vendor'    => $is_vendor,
                        'is_customer'  => $is_customer,
                        'is_contra'    => $is_contra,
                        'status'       => $activate,
                        'company_id'   => session('current_company'),
                        'created_by'   => session('created_by'),
                        'created_at'   => date("Y-m-d H:i:s"),
                        'updated_at'   => date("Y-m-d H:i:s"),
                    ];
                    array_push($partners, $new_data);
                }
              
                Partner::insert($partners);
                $response['message'] = trans('system.success');
                Session::flash('message', $response['message']);
                Session::flash('alert-class', 'success');

            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function update(Request $request, $id)
    {
        $partner = Partner::find(intval($id));
        if (is_null($partner)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.partners.index');
        }
        $request->merge(['status' => intval($request->input('status', 0))]);
        $request->merge(['is_contra' => intval($request->input('is_contra', 0))]);
        $request->merge(['is_vendor' => intval($request->input('is_vendor', 0))]);
        $request->merge(['is_customer' => intval($request->input('is_customer', 0))]);
        $validator = \Validator::make($data = $request->all(), Partner::rules($request->tax_code, $request->code ));
        $validator->setAttributeNames(trans('partners'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $partner->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.partners.index');
    }

    public function download(Request $request)
    {
        $file= public_path() . "/assets/media/files/templates/imports_partner.xlsx";
        $headers = [
            'Content-Type: application/xls',
        ];
        return response()->download($file, 'imports_partner.xlsx', $headers);
    }

    public function destroy(Request $request, $id)
    {
        $partner = Partner::find(intval($id));
        if (is_null($partner)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.partners.index');
        }
        // check cac rang buoc #
        $partner->deleted_by = $request->user()->id;
        $partner->save();
        $partner->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.partners.index');
    }

    public function getInforCustomer(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $url = 'https://thongtindoanhnghiep.co/api/company/'.$request->msdn;
                if(is_null($url)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . '1', 1);
                }
                $data = file_get_contents($url);
                $response['message'] = trans('system.success');
                $data = json_decode($data);
                if (is_null($data->MaSoThue)) {
                    $statusCode = 400;
                    throw new \Exception("cant not find company", 1);
                }
                $response['data']  = ['msdn' => $data->MaSoThue, 'address' => $data->DiaChiCongTy, 'name' => $data->Title, 'city_code' => $data->TinhThanhID, 'contact' =>$data->ChuSoHuu];
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function show(Request $request)
    {

    }
    public function import(Request $request)
    {  
        $message = [];
        if (!$request->hasFile('file_import')) {
            $message = [0 => trans('partners.error_check')];
            return back()->withErrors($message);
        } else {
            $file_info = $request->file_import->getClientOriginalExtension();
            if ($file_info != 'xlsx' && $file_info != 'xls') {
                array_push($message, __(trans('partners.error_format_file')));
                return back()->withErrors($message);
            }
        }
        try {
            $import = (new PartnerImport());
            $import->import($request->file('file_import'), 'null', \Maatwebsite\Excel\Excel::XLSX);
            $message = [0 => 'you saved record but still record is error: '];
            $message = array_merge($message, $import->getErrors());
            if (!empty($message)) {
                return back()->withErrors($message);
            }
        } catch (Exception $e) {
            throw $e;
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.partners.index');
    }

    public function getVendor(Request $request){
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $code = trim($request->input('q'));
                $companyId = intval(session('current_company'));
                $partners = Partner::whereRaw("(company_id IS NULL OR company_id={$companyId}) AND status=1 AND is_vendor=1")->selectRaw(" id, code as text, name as name, address_1 as address, tax_code")->paginate(5)->toArray();
                $response['message'] = trans('system.success');
                $statusCode = 200;
                $response['partners'] = $partners;
            } catch (\Exception $e) {
                if ($statusCode == 400) $statusCode = 500; 
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getCustomer(Request $request){
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $code = trim($request->input('q'));
                $companyId = intval(session('current_company'));
                    $partners = Partner::whereRaw("(company_id IS NULL OR company_id={$companyId}) AND status=1 AND is_customer=1")->selectRaw(" id, code as text, name as name, address_1 as address, tax_code")->paginate(5)->toArray();
                    $response['message'] = trans('system.success');
                    $statusCode = 200;
                    $response['partners'] = $partners;
            } catch (\Exception $e) {
                if ($statusCode == 400) $statusCode = 500; 
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getAllPartner(Request $request){
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $code = trim($request->input('q'));
                $companyId = intval(session('current_company'));
                $partners = Partner::whereRaw("(company_id IS NULL OR company_id={$companyId}) AND status=1")->selectRaw(" id, code as text, name as name, address_1 as address, tax_code")->paginate(5)->toArray();
                $response['message'] = trans('system.success');
                $statusCode = 200;
                $response['partners'] = $partners;
            } catch (\Exception $e) {
                if ($statusCode == 400) $statusCode = 500; 
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
    public function searchByCode(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $code = trim($request->input('code'));
                $companyId = intval(session('current_company'));
                $partners = Partner::whereRaw("(company_id IS NULL OR company_id={$companyId}) AND status=1 AND is_customer=1 AND customer_code like '%" . $code . "%'")->selectRaw("CONCAT(customer_code, ' - ' , customer_name) as fullname, id, customer_code as code, customer_name as name, address_1 as address, tax_code")->get()->keyBy('id')->toArray();
                $response['message'] = trans('system.success');
                $statusCode = 200;
                $response['partners'] = $partners;
            } catch (\Exception $e) {
                if ($statusCode == 400) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}
