<?php

namespace App\Http\Controllers\Backend;

use App\Province;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CustomersController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval($request->input('status', -1));
        $source         = $request->input('source', -1);
        $level         = $request->input('level', -1);
        $fullname_email = $request->input('fullname_email');
        $province       = intval($request->input('province', -1));

        if($fullname_email) $query .= " AND (fullname like '%" . $fullname_email . "%' OR email like '%" . $fullname_email . "%' OR phone like '%" . $fullname_email . "%' OR tax_code like '%" . $fullname_email . "%' OR address like '%" . $fullname_email . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        if($province <> -1) $query .= " AND (province_id = '{$province}' OR province_id like '{$province},%' OR province_id like '%,{$province}' OR province_id like '%,{$province},%')";
        if($source <> -1) $query .= " AND source = {$source}";
        if($level <> -1) $query .= " AND level = {$level}";
        $customers = Customer::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $provinces = Province::pluck('name', 'id')->toArray();

        return view('backend.customers.index', compact('customers', 'provinces'));
    }

    public function create(Request $request)
    {
        $provinces = Province::where('status', 1)->pluck('name', 'id')->toArray();
        return view('backend.customers.create', compact('provinces'));
    }

    public function show(Request $request, $id)
    {
        $customer = Customer::find(intval($id));
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }
        $provinces = Province::where('status', 1)->pluck('name', 'id')->toArray();
        return view('backend.customers.show', compact('customer', 'provinces'));
    }

    public function edit(Request $request, $id)
    {
        $customer = Customer::find(intval($id));
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }
        $provinces = Province::where('status', 1)->pluck('name', 'id')->toArray();
        return view('backend.customers.edit', compact('customer', 'provinces'));
    }

    public function store(Request $request)
    {
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), Customer::rules());
        $validator->setAttributeNames(trans('customers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['source']     = \App\Define\Customer::SOURCE_ADMIN;
        $data['created_by'] = $request->user()->id;
        $data['password'] = \Hash::make($data['password']);
        Customer::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.customers.index');
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::find(intval($id));
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }

        $request->merge(['status' => $request->input('status', 0)]);
        $validator = \Validator::make($data = $request->only(['status', 'fullname', 'email', 'phone', 'code', 'address', 'level', 'coin_threshold']), Customer::rules(intval($id)));
        $validator->setAttributeNames(trans('customers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $customer->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.customers.index');
    }

    public function destroy(Request $request, $id)
    {
        $customer = Customer::find(intval($id));
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }
        if ($customer->orders()->count()) {
            Session::flash('message', "Khách đã có đơn hàng.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }
        if (\App\PartnerSale::where('customer_id', $customer->id)->count()) {
            Session::flash('message', "Khách đã là CTV đã tạo mã Voucher.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }

        if (\App\CustomerContact::where('customer_id', $customer->id)->count()) {
            Session::flash('message', "Khách đã có danh bạ.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }

        if (\App\CustomerTransaction::where('customer_id', $customer->id)->count()) {
            Session::flash('message', "Khách đã có giao dịch.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }

        if (\App\CustomerWithdrawal::where('customer_id', $customer->id)->count()) {
            Session::flash('message', "Khách đã nạp tiền.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }

        // $customer->deleted_by = $request->user()->id;
        // $customer->save();
        $customer->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.customers.index');
    }

    public function search(Request $request)
    {
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $tag = $request->input("tag");
                $tag = trim($tag['term']);
                if (!$tag) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm thấy từ khoá", 1);
                }
                $query = "(code like '%" . $tag . "%' OR fullname like '%" . $tag . "%' OR email like '%" . $tag . "%' OR phone like '%" . $tag . "%' OR address like '%" . $tag . "%')";
                // if (trim($request->type) == 'ctv') {
                //     $query = " AND level IN(" .  . ")";
                // }
                $customers = Customer::where('status', 1)->whereRaw($query)->selectRaw("fullname, code, id, level")->get()->toArray();
                $response['message'] = json_encode($customers);
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function changePassword(Request $request, $id)
    {
        $customer = Customer::find(intval($id));
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customers.index');
        }

        return view('backend.customers.change-password', compact('customer'));
    }

    public function postChangePassword(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'new_password'      => 'required|min:6|max:30',
            're_password'       => 'same:new_password',
        ]);
        $validator->setAttributeNames(trans('customers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $customer = Customer::find(intval($id));
        if (is_null($customer)) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', trans('system.have_an_error'));
            return back()->withErrors($errors)->withInput();
        }

        $customer->password = \Hash::make($request->input('new_password'));
        $customer->save();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.customers.index');
    }
}
