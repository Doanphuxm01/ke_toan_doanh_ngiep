<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAsset;
use App\Models\Account;
use App\Models\CostItem;
use App\Models\FixedAssetAllocationInformation;
use App\Models\AllocationCost;
use App\Models\ExpensesPrepaid;
use App\Models\PaymentReceiptVoucher;
use App\Models\ExpensesPrepaidAllocationInformation;
use App\Models\AccountingInvoice;
use App\Models\Accounting;
use App\Models\ServicePurchase;
use App\Models\ExpensesPrepaidAllocationCost;
use App\Models\ExpensesPrepaidAllocation;
use Illuminate\Support\Facades\Validator;
use App\Models\Department;
use Illuminate\Support\Collection;
use App\Models\SaleCostItem;
use App\Models\InstrumentToolType;
use App\Models\VoucherCollection;
use Illuminate\Support\Facades\Session;

class ExpensesPrepaidController 
{
    public function index(Request $request){
      $query = "1=1"; 
      $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
      $expenses_prepaid_name = $request->input('expenses_prepaid_name');
      $money_amount = str_replace( ",","", $request->input('money_amount'));
      $period_number = $request->input('period_number');
      $date_range = $request->input('date_range'); 
      $from_date = $request->input('from_date'); 
      $to_date = $request->input('to_date'); 
      $status = intval($request->input('status', -1));
      if($status <> -1) $query .= " AND status = {$status}";
      if($expenses_prepaid_name) $query .= " AND (expenses_prepaid_name like '%" . $expenses_prepaid_name . "%' )" ;
      if($money_amount) $query .= " AND (money_amount like $money_amount)" ;
      if($period_number) $query .= " AND (period_number like $period_number )" ;
      if($from_date && $to_date) {
         $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
         $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
         $query .= " AND voucher_date >= '" . $from_date . "' AND voucher_date <= '" .$to_date. "'";
      }else if( !$from_date && $to_date) {
         $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
         $query .= " AND  voucher_date <= '" .$to_date. "'";
      }else if ($from_date && !$to_date) {
         $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
         $query .= " AND  voucher_date >= '" .$from_date. "'";
      }
      if($request->input('use_department')){
         $expenses_prepaids = ExpensesPrepaid::where('id', "")->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->paginate($page_num);
         $a = ExpensesPrepaidAllocationInformation::where('department_id', $request->input('use_department'))->get();
         foreach ($a as $key => $value) {
         $b = ExpensesPrepaid::whereRaw($query)->where('id', $value->expenses_prepaid_id)->first();
         if($b != null){
            $expenses_prepaids->push($b);
         }
      }
      }
      else{
         $expenses_prepaids = ExpensesPrepaid::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->paginate($page_num);
      }
      return view('backend.expenses-prepaid.index', compact('expenses_prepaids'));
   }
   public function show(Request $request, $id){
      $expenses_prepaid = ExpensesPrepaid::find($id);
      $a = ExpensesPrepaidAllocationCost::where('expenses_prepaid_id', $id)->pluck('voucher_id')->toArray();
      $vouchers = ExpensesPrepaidAllocation::whereIn('id', $a)->get();
      return view('backend.expenses-prepaid.show', compact('expenses_prepaid', 'vouchers'));
   }
   public function create(Request $request){
      $allocation_accounts = Account::getAllocationAccounts(session('current_company'));
      $expense_accounts = Account::getExpenseAccounts(session('current_company'));
      $departments = Department::getDepartments(session('current_company'));
      $cost_items = CostItem::getActiveCostItem(session('current_company'));
      $latest_voucher_number = FixedAsset::latest()->pluck('id')->first();
      $str = substr($latest_voucher_number, -7);
      $new_voucher_number = $str + 1;
      if(strlen($new_voucher_number) == 1){
         $voucher_number = "GT000000" . $new_voucher_number;
      }
      if(strlen($new_voucher_number) == 2){
         $voucher_number = "GT00000" . $new_voucher_number;
      }
      if(strlen($new_voucher_number) == 3){
         $voucher_number = "GT0000" . $new_voucher_number;
      }
      if(strlen($new_voucher_number) == 4){
          $voucher_number = "GT000" . $new_voucher_number;
      }
      if(strlen($new_voucher_number) == 5){
         $voucher_number = "GT00" . $new_voucher_number;
      }
      if(strlen($new_voucher_number) == 6){
         $voucher_number = "GT0" . $new_voucher_number;
      }
      if(strlen($new_voucher_number) == 7){
         $voucher_number = "GT" . $new_voucher_number;
      }
      return view('backend.expenses-prepaid.create', compact('allocation_accounts', 'departments', 'cost_items', 'expense_accounts', 'voucher_number'));
   }
   public function store(Request $request){
      // dd($request->all());
      $data = $request->all();
      if(!isset($request->status)){
         $data['status'] = '1';
      }
      $data['company_id'] = session('current_company');
      $data['created_by'] = session('created_by');
      $request->merge(['status' => intval($request->input('status', 1))]);
      $voucher_date = str_replace("/", "-", $request->voucher_date);
      $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
      $data['residual_time'] = $request->period_number;
      $expenses_prepaid  = ExpensesPrepaid::create($data);
      
      for ($i=0; $i < count($request->use_department) ; $i++) { 
         $expenses_prepaid_allocation = new ExpensesPrepaidAllocationInformation();
         $expenses_prepaid_allocation->expenses_prepaid_id = $expenses_prepaid->id;
         $expenses_prepaid_allocation->department_id = $request->use_department[$i];
         $expenses_prepaid_allocation->allocation_rate = $request->allocation_rate[$i];
         $expenses_prepaid_allocation->expense_account = $request->expense_account[$i];
         $expenses_prepaid_allocation->cost_item = $request->cost_item[$i];
         $expenses_prepaid_allocation->save();
      }
      for ($i=0; $i < count($request->accounting_id) ; $i++) { 
         $voucher_collection = new VoucherCollection();
         $voucher_collection->expenses_prepaid_id = $expenses_prepaid->id;
         $voucher_collection->accounting_id = $request->accounting_id[$i];
         $voucher_collection->save();
      }
      
      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');
      return redirect()->route('admin.expenses-prepaid.index');
   }
   public function edit(Request $request, $id){
      if(ExpensesPrepaid::checkAllocation($id) == 0){
         $expenses_prepaid = ExpensesPrepaid::find($id);
         $voucher_collection = VoucherCollection::where('expenses_prepaid_id', $id)->get();
         $allocation_accounts = Account::getAllocationAccounts(session('current_company'));
         $expense_accounts = Account::getExpenseAccounts(session('current_company'));
         $departments = Department::getDepartments(session('current_company'));
         $cost_items = CostItem::getActiveCostItem(session('current_company'));
         $check = VoucherCollection::where('expenses_prepaid_id', $id)->get();
         $use = ExpensesPrepaidAllocationInformation::where('expenses_prepaid_id', $id )->selectRaw("id, department_id, allocation_rate, expense_account, cost_item")->get();
         // dd($use);
         return view('backend.expenses-prepaid.edit', compact('expenses_prepaid', 'allocation_accounts', 'expense_accounts', 'departments', 'cost_items', 'voucher_collection', 'use', 'check'));
      }
      else{
         return redirect()->route('admin.expenses-prepaid.index');
      } 
   }
   public function update(Request $request, $id){
      $expenses_prepaid = ExpensesPrepaid::find(intval($id));
      $data = $request->all();
      if (is_null($expenses_prepaid)) {
         Session::flash('message', trans('system.have_an_error'));
         Session::flash('alert-class', 'danger');
         return redirect()->route('admin.expenses-prepaid.index');
      }
      if(!isset($request->status)){
         $data['status'] = '0';
      }
      $request->merge(['status' => intval($request->input('status', 1))]);
      $voucher_date = str_replace("/", "-", $request->voucher_date);
      $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
      $data['residual_time'] = $request->period_number;
      // dd($request->period_number);
      $expenses_prepaid->update($data);
      if($request->voucher_collection_id != ''){
         VoucherCollection::whereNotIn("id", $request->voucher_collection_id)->where('expenses_prepaid_id', $id)->delete();
      }
      if($request->voucher_collection_id == ''){
         VoucherCollection::where('expenses_prepaid_id', $id)->delete();
      }
      for ($i=0; $i < count($request->accounting_id) ; $i++) { 
         $voucher_collection = VoucherCollection::find($request->voucher_collection_id[$i]); 
         if($voucher_collection == null){
            $new = new VoucherCollection();
            $new->expenses_prepaid_id = $expenses_prepaid->id;
            $new->accounting_id = $request->accounting_id[$i];
            $new->save();
         }
         else{
         
            $voucher_collection->accounting_id = $request->accounting_id[$i];
            $voucher_collection->update();
         }  
      }

      if($request->allocation_info_id != ''){
         $allo_del = ExpensesPrepaidAllocationInformation::whereNotIn("id", $request->allocation_info_id)->where('expenses_prepaid_id', $id)->delete();
      }
      for ($i=0; $i < count($request->use_department) ; $i++) { 
         $allocation_information = ExpensesPrepaidAllocationInformation::find($request->allocation_info_id[$i]); 
         if($allocation_information == null){
            $new = new ExpensesPrepaidAllocationInformation();
            $new->expenses_prepaid_id = $id;
            $new->department_id = $request->use_department[$i];
            $new->allocation_rate = $request->allocation_rate[$i];
            $new->expense_account = $request->expense_account[$i];
            $new->cost_item = $request->cost_item[$i];
            $new->save();
         }
         else{
            $allocation_information->department_id = $request->use_department[$i];
            $allocation_information->allocation_rate = $request->allocation_rate[$i];
            $allocation_information->expense_account = $request->expense_account[$i];
            $allocation_information->cost_item = $request->cost_item[$i];
            $allocation_information->update();
         }  
      }
      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');

      return redirect()->route('admin.expenses-prepaid.index');
   }
   public function destroy(Request $request, $id)
   {
      $expenses_prepaid = ExpensesPrepaid::find($id);
      $expenses_prepaid_allocation_info = ExpensesPrepaidAllocationInformation::where('expenses_prepaid_id', $expenses_prepaid->id);
      $voucher_collection = VoucherCollection::where('expenses_prepaid_id', $expenses_prepaid->id);
      if (is_null($expenses_prepaid)) {
         Session::flash('message', trans('system.have_an_error'));
         Session::flash('alert-class', 'danger');
         return back();
      }
      $expenses_prepaid->delete();
      $expenses_prepaid_allocation_info->delete();
      $voucher_collection->delete();

      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');
      return back();
   }
   public function getVouchers(Request $request){
      if ($request->ajax()) {
         $expenses_prepaid_id = ExpensesPrepaid::where('company_id', session('current_company'))->pluck('id')->toArray();
         $accounting_id = VoucherCollection::all()->pluck('accounting_id')->toArray();
         $service_purchase_id = ServicePurchase::where('company_id', session('current_company'))->pluck('id')->toArray();
         $service_purchase = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('service_purchase_id', $service_purchase_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('service_purchases', 'accounting.service_purchase_id', '=', 'service_purchases.id')
                                       ->select('accounting.id', 'service_purchases.voucher_date', 'service_purchases.desc', 'service_purchases.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $payment_receipt_id = PaymentReceiptVoucher::where('company_id', session('current_company'))->where('type', 1)->pluck('id')->toArray();
         $payment_receipt = Accounting::whereNotIn('accounting.id', $accounting_id)
         ->whereIn('payment_receipt_voucher_id', $payment_receipt_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('payment_receipt_vouchers', 'accounting.payment_receipt_voucher_id', '=', 'payment_receipt_vouchers.id')
                                       ->select('accounting.id', 'payment_receipt_vouchers.voucher_date', 'payment_receipt_vouchers.desc', 'payment_receipt_vouchers.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $accounting_invoice_id = AccountingInvoice::where('company_id', session('current_company'))->pluck('id')->toArray();
         $accounting_invoice = Accounting::whereNotIn('accounting.id', $accounting_id)
         ->whereIn('accounting_invoice_id', $accounting_invoice_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('accounting_invoices', 'accounting.accounting_invoice_id', '=', 'accounting_invoices.id')
                                       ->select('accounting.id', 'accounting_invoices.voucher_date', 'accounting_invoices.desc', 'accounting_invoices.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $a = $service_purchase->merge($payment_receipt);
         $vouchers = $a->merge($accounting_invoice);
         $accounting_id = VoucherCollection::where('expenses_prepaid_id', $request->id)->pluck('accounting_id')->toArray();
         if($request->allocation_account == $request->allo_acc){
            foreach ($accounting_id as $key => $value) {
               $accounting = Accounting::where('id', $value)->first();
               if($accounting->service_purchase_id != null){
                     $b = Accounting::where('accounting.id', $value)->join('service_purchases', 'accounting.service_purchase_id', '=', 'service_purchases.id')
                                    ->select('accounting.id', 'service_purchases.voucher_date', 'service_purchases.desc', 'service_purchases.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                    ->first();
                     $vouchers->push($b);
               }
               else if($accounting->accounting_invoice_id != null){
                     $b = Accounting::where('accounting.id', $value)->join('accounting_invoices', 'accounting.accounting_invoice_id', '=', 'accounting_invoices.id')
                                    ->select('accounting.id', 'accounting_invoices.voucher_date', 'accounting_invoices.desc', 'accounting_invoices.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                    ->first();
                     $vouchers->push($b);
               }
               else if($accounting->payment_receipt_voucher_id != null){
                     $b = Accounting::where('accounting.id', $value)->join('payment_receipt_vouchers', 'accounting.payment_receipt_voucher_id', '=', 'payment_receipt_vouchers.id')
                                    ->select('accounting.id', 'payment_receipt_vouchers.voucher_date', 'payment_receipt_vouchers.desc', 'payment_receipt_vouchers.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                    ->first();
                     $vouchers->push($b);
               }
            }
         }
         return response($vouchers);
      }
   }
   public function checkCode(Request $request){
      if ($request->ajax()) {
         $check = ExpensesPrepaid::where('company_id', session('current_company'))->where('expenses_prepaid_code', $request->expenses_prepaid_code)->first();
         if(count($check) > 0){
            $response = [
               'status' => "error",
            ];
         }
         return response($response);
     }
   }
   public function searchVouchers(Request $request){
      if ($request->ajax()) {
         $from_date = $request->from_date;
         $to_date = $request->to_date;
         if($from_date && $to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $service_purchase_id = ServicePurchase::where('voucher_date', '<=', $to_date)->where('voucher_date', '>=', $from_date)->where('company_id', session('current_company'))->pluck('id')->toArray();
            $payment_receipt_id = PaymentReceiptVoucher::where('voucher_date', '<=', $to_date)->where('voucher_date', '>=', $from_date)->where('company_id', session('current_company'))->where('type', 1)->pluck('id')->toArray();
            $accounting_invoice_id = AccountingInvoice::where('voucher_date', '<=', $to_date)->where('voucher_date', '>=', $from_date)->where('company_id', session('current_company'))->pluck('id')->toArray();
         } else if( !$from_date && $to_date) {
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $service_purchase_id = ServicePurchase::where('voucher_date', '<=', $to_date)->where('company_id', session('current_company'))->pluck('id')->toArray();
            $payment_receipt_id = PaymentReceiptVoucher::where('voucher_date', '<=', $to_date)->where('company_id', session('current_company'))->where('type', 1)->pluck('id')->toArray();
            $accounting_invoice_id = AccountingInvoice::where('voucher_date', '<=', $to_date)->where('company_id', session('current_company'))->pluck('id')->toArray();
         } else if ($from_date && !$to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $service_purchase_id = ServicePurchase::where('voucher_date', '>=', $from_date)->where('company_id', session('current_company'))->pluck('id')->toArray();
            $payment_receipt_id = PaymentReceiptVoucher::where('voucher_date', '>=', $from_date)->where('company_id', session('current_company'))->where('type', 1)->pluck('id')->toArray();
            $accounting_invoice_id = AccountingInvoice::where('voucher_date', '>=', $from_date)->where('company_id', session('current_company'))->pluck('id')->toArray();
         } else if(!$from_date && !$to_date){
            $service_purchase_id = ServicePurchase::where('company_id', session('current_company'))->pluck('id')->toArray();
            $payment_receipt_id = PaymentReceiptVoucher::where('company_id', session('current_company'))->where('type', 1)->pluck('id')->toArray();
            $accounting_invoice_id = AccountingInvoice::where('company_id', session('current_company'))->pluck('id')->toArray();
         }
         $expenses_prepaid_id = ExpensesPrepaid::where('company_id', session('current_company'))->pluck('id')->toArray();
         $accounting_id = VoucherCollection::all()->pluck('accounting_id')->toArray();
         $service_purchase = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('service_purchase_id', $service_purchase_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('service_purchases', 'accounting.service_purchase_id', '=', 'service_purchases.id')
                                       ->select('accounting.id', 'service_purchases.voucher_date', 'service_purchases.desc', 'service_purchases.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $payment_receipt = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('payment_receipt_voucher_id', $payment_receipt_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('payment_receipt_vouchers', 'accounting.payment_receipt_voucher_id', '=', 'payment_receipt_vouchers.id')
                                       ->select('accounting.id', 'payment_receipt_vouchers.voucher_date', 'payment_receipt_vouchers.desc', 'payment_receipt_vouchers.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $accounting_invoice = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('accounting_invoice_id', $accounting_invoice_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('accounting_invoices', 'accounting.accounting_invoice_id', '=', 'accounting_invoices.id')
                                       ->select('accounting.id', 'accounting_invoices.voucher_date', 'accounting_invoices.desc', 'accounting_invoices.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $a = $service_purchase->merge($payment_receipt);
         $vouchers = $a->merge($accounting_invoice);
         foreach ($vouchers as $key => $value) {
            foreach ($request->accounting_id as $id) {
               if($value->id == $id){
                  $value['status'] = "checked";
               }
            }
         }
         return response($vouchers);
      }
   }
   public function editVouchers(Request $request){

      $service_purchase_id = ServicePurchase::where('company_id', session('current_company'))->pluck('id')->toArray();
      $payment_receipt_id = PaymentReceiptVoucher::where('company_id', session('current_company'))->where('type', 1)->pluck('id')->toArray();
      $accounting_invoice_id = AccountingInvoice::where('company_id', session('current_company'))->pluck('id')->toArray();
   
         $expenses_prepaid_id = ExpensesPrepaid::where('company_id', session('current_company'))->pluck('id')->toArray();
         $accounting_id = VoucherCollection::all()->pluck('accounting_id')->toArray();
         $service_purchase = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('service_purchase_id', $service_purchase_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('service_purchases', 'accounting.service_purchase_id', '=', 'service_purchases.id')
                                       ->select('accounting.id', 'service_purchases.voucher_date', 'service_purchases.desc', 'service_purchases.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $payment_receipt = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('payment_receipt_voucher_id', $payment_receipt_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('payment_receipt_vouchers', 'accounting.payment_receipt_voucher_id', '=', 'payment_receipt_vouchers.id')
                                       ->select('accounting.id', 'payment_receipt_vouchers.voucher_date', 'payment_receipt_vouchers.desc', 'payment_receipt_vouchers.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $accounting_invoice = Accounting::whereNotIn('accounting.id', $accounting_id)
                                       ->whereIn('accounting_invoice_id', $accounting_invoice_id)
                                       ->where('debit_account', $request->allocation_account)
                                       ->join('accounting_invoices', 'accounting.accounting_invoice_id', '=', 'accounting_invoices.id')
                                       ->select('accounting.id', 'accounting_invoices.voucher_date', 'accounting_invoices.desc', 'accounting_invoices.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                       ->get();
         $a = $service_purchase->merge($payment_receipt);
         $vouchers = $a->merge($accounting_invoice);
         foreach ($request->accounting_id as $key => $value) {
            $accounting = Accounting::where('id', $value)->first();
            if($accounting->service_purchase_id != null){
                  $b = Accounting::where('accounting.id', $value)->join('service_purchases', 'accounting.service_purchase_id', '=', 'service_purchases.id')
                                 ->select('accounting.id', 'service_purchases.voucher_date', 'service_purchases.desc', 'service_purchases.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                 ->first();
                  $vouchers->push($b);
            }
            else if($accounting->accounting_invoice_id != null){
                  $b = Accounting::where('accounting.id', $value)->join('accounting_invoices', 'accounting.accounting_invoice_id', '=', 'accounting_invoices.id')
                                 ->select('accounting.id', 'accounting_invoices.voucher_date', 'accounting_invoices.desc', 'accounting_invoices.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                 ->first();
                  $vouchers->push($b);
            }
            else if($accounting->payment_receipt_voucher_id != null){
                  $b = Accounting::where('accounting.id', $value)->join('payment_receipt_vouchers', 'accounting.payment_receipt_voucher_id', '=', 'payment_receipt_vouchers.id')
                                 ->select('accounting.id', 'payment_receipt_vouchers.voucher_date', 'payment_receipt_vouchers.desc', 'payment_receipt_vouchers.voucher_no', 'accounting.debit_account', 'accounting.credit_account', 'accounting.amount')
                                 ->first();
                  $vouchers->push($b);
            }
         }
         
         foreach ($vouchers as $key => $value) {
            foreach ($request->accounting_id as $id) {
               if($value->id == $id){
                  $value['status'] = "checked";
               }
            }
         }
         return response($vouchers);
   }

   
   
}
