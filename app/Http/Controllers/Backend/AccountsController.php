<?php

namespace App\Http\Controllers\Backend;

use App\Models\Account;
use App\Helper\HString;
use App\Models\SaleCostItem;
use Illuminate\Http\Request;
use App\Exports\AccountExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class AccountsController
{
    public function index(Request $request)
    {
        $query    = "parent_id IS NULL";
        $accounts = Account::whereRaw($query)->orderBy('code')->get();
        $groups = Account::whereNull('parent_id')->orderBy('code')->pluck('code', 'code')->toArray();
        return view('backend.accounts.index', compact('accounts', 'groups'));
    }

    public function store(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $parent = trim($request->parent);
                $request->merge(['code' => substr($parent, 0, 5) . trim($request->code), 'status' => intval($request->status)]);
                $validator = \Validator::make($data = $request->only(['group', 'parent', 'code', 'name', 'description', 'property', 'status']), Account::rules());
                $validator->setAttributeNames(trans('accounts'));
                if($validator->fails()) throw new \Exception($validator->messages()->first());
                if ($parent) {
                    $pAccount = Account::where('status', 1)->where('code', $parent)->where('company_id', session('current_company'))->first();
                    if (is_null($pAccount)) throw new \Exception(trans('system.have_an_error'));
                }
                $data['company_id'] = session("current_company");
                $existedCode = Account::where('code', $data['code'])->where('company_id', $data['company_id'])->first();
                if (!is_null($existedCode)) {
                    throw new \Exception(trans("accounts.existed_code"));
                }
                if ($parent == null) {
                    $data['parent_id']  = $data['group'];
                    $data['group_code'] = $data['group'];
                } else {
                    $data['parent_id']  = $data['parent'];
                    $data['group_code'] = $pAccount->group_code;
                }
                $data['created_by'] = $request->user()->id;
                $account = Account::create($data);
                $statusCode = 200;
                $response['message'] = trans('system.success');
                $response['code'] = $account->code;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function destroy(Request $request, $id)
    {
        $account = Account::where('id', intval($id))->where('company_id', session('current_company'))->first();
        if (is_null($account)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        if (Account::where('parent_id', $account->code)->count()) {
            Session::flash('message', trans("accounts.cannot_delete_code"));
            Session::flash('alert-class', 'danger');
            return back();
        }

        $sci = SaleCostItem::whereRaw("company_id={$account->company_id} AND (sale_account_code='{$account->code}' OR cost_account_code='{$account->code}' OR prov_account_code='{$account->code}')")->first();
        if (!is_null($sci))  {
            Session::flash('message', trans('accounts.existed_ref_to_sci') . ': ' . $sci->code);
            Session::flash('alert-class', 'danger');
            return back();
        }
        $account->delete();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }

    public function update(Request $request, $id)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $account = Account::where('id', intval($id))->where('company_id', session('current_company'))->first();
                if (is_null($account)) throw new \Exception(trans('system.have_an_error'), 1);
                $parent = trim($request->parent);
                $request->merge(['code' => substr($parent, 0, 5) . trim($request->code), 'status' => intval($request->status)]);
                $validator = \Validator::make($data = $request->only(['group', 'parent', 'code', 'name', 'description', 'property', 'status']), Account::rules($account->id));
                $validator->setAttributeNames(trans('accounts'));
                if($validator->fails()) throw new \Exception($validator->messages()->first());
                // co con roi, ko duoc doi ma code


                $existedCode = Account::where('id', '<>', $account->id)->where('code', $data['code'])->where('company_id', $account->company_id)->first();
                if (!is_null($existedCode)) {
                    throw new \Exception(trans("accounts.existed_code"));
                }
                // tim child
                $childrenObjects = [];
                $children = Account::where('parent_id', $account->code)->where('company_id', $account->company_id)->get();
                if ($children->count()) {
                    if ($account->code <> $data['code']) {
                        throw new \Exception(trans("accounts.cannot_change_code"));
                    }
                    if (($account->status == 1 && $data['status'] == 0) && $children->where('status', 1)->count()) {
                        throw new \Exception(trans("accounts.existed_children_accounts_active"));
                    }
                }
                if ($parent) {
                    $pAccount = Account::where('status', 1)->where('code', $parent)->where('company_id', $account->company_id)->first();
                    if (is_null($pAccount)) throw new \Exception(trans('system.have_an_error'));
                }

                if ($account->code <> $data['code']) {
                    $sci = SaleCostItem::whereRaw("company_id={$account->company_id} AND (sale_account_code='{$account->code}' OR cost_account_code='{$account->code}' OR prov_account_code='{$account->code}')")->first();
                    if (!is_null($sci)) throw new \Exception(trans('accounts.existed_ref_to_sci') . ': ' . $sci->code);
                }

                if ($parent == null) {
                    $data['parent_id']  = $data['group'];
                    $data['group_code'] = $data['group'];
                } else {
                    $data['parent_id']  = $data['parent'];
                    $data['group_code'] = $pAccount->group_code;
                }
                $account->update($data);
                $statusCode = 200;
                $response['message'] = trans('system.success');
                $response['code'] = $account->code;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getByGroup(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $group = Account::where('code', trim($request->id))->where('is_system', 1)->first();
                if (is_null($group)) throw new \Exception(trans('system.have_an_error'), 1);
                $accounts = Account::where('company_id', session('current_company'))->where('status', 1)->where('parent_id', $group->code)->orderBy('code')->get();
                $statusCode = 200;
                $level = 1;
                $render = view('backend.accounts.row-tree', compact('accounts', 'level'))->render();
                $data = [];
                $lines = explode(PHP_EOL, $render);
                foreach ($lines as $line) {
                    $line = explode("|", $line);
                    if (isset($line[1])) {
                        $data[$line[0]] = $line[1];
                    }
                }
                $response['accounts'] = $data;
                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getParents(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $parents = [];
                $codes = explode('.', trim($request->input('codes')));
                foreach ($codes as $code) {
                    $parent = Account::where('code', $code)->first();
                    if (!is_null($parent)) {
                        array_push($parents, $parent->code);
                        while ($parent->parent_id <> null) {
                            $parent = Account::where('code', $parent->parent_id)->first();
                            if (!is_null($parent)) {
                                array_push($parents, $parent->code);
                            }
                        }
                    }
                }
                $statusCode = 200;
                $response['parents'] = $parents;
                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getById(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $account = Account::where('company_id', session('current_company'))->where('id', intval($request->id))->first();
                if (is_null($account)) throw new \Exception(trans('system.have_an_error'), 1);
                $groups = Account::whereNull('parent_id')->orderBy('code')->pluck('code', 'code')->toArray();
                $code = $account->code;
                $parentCode = $account->parent_id;
                if (!isset($groups[$account->parent_id])) {
                    $code = explode(substr($account->parent_id, 0, 5), $account->code);
                    $code = $code[1];
                } else {
                    $parentCode = "";
                }
                // tim cac id cha
                $parentIds = [];
                $parent = Account::where('company_id', $account->company_id)->where('code', $account->parent_id)->first();
                while (!is_null($parent)) {
                    array_push($parentIds, $parent->id);
                    $parent = Account::where('company_id', $parent->company_id)->where('code', $parent->parent_id)->first();
                }
                if (count($parentIds) == 0) $parentIds = [-1];
                // $accounts = Account::whereRaw("company_id=" . $account->company_id . " AND parent_id = '" . $account->group_code . "' AND (status = 1 OR id IN(" . implode(',', $parentIds) . "))")->orderBy('code')->get();
                $accounts = Account::whereRaw("id <> " . $account->id . " AND company_id=" . $account->company_id . " AND parent_id = '" . $account->group_code . "' AND (status = 1 OR id IN(" . implode(',', $parentIds) . "))")->orderBy('code')->get();
                // dd("id <> " . $account->id . " AND company_id=" . $account->company_id . " AND parent_id = '" . $account->group_code . "' AND (status = 1 OR id IN(" . implode(',', $parentIds) . "))");
                // dd($accounts);
                $level = 1;
                $render = view('backend.accounts.row-edit-tree', compact('accounts', 'level', 'parentIds'))->render();
                $data = [];
                $lines = explode(PHP_EOL, $render);
                foreach ($lines as $line) {
                    $line = explode("|", $line);
                    if (isset($line[1])) {
                        $data[$line[0]] = $line[1];
                    }
                }
                $statusCode = 200;
                $response['code'] = $code;
                $response['data'] = $data;
                $response['account'] = $account;
                $response['parent_code'] = $parentCode;
                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function CallbackDebtAccount(Request $request){
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $accountLowestLevel = '';
                $accountLowestLevel = Account::getActiveLowestLevel(Account::getActiveMember('company_id'));
                $account = Account::whereIn('code',array_keys($accountLowestLevel))->selectRaw(" id, code as text, name as name,description as description")->paginate(5)->toArray();
                $response['message'] = trans('system.success');
                $statusCode = 200;
                $response['kq'] = $account;
            } catch (\Exception $e) {
                if ($statusCode == 400) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}
