<?php

namespace App\Http\Controllers\backend;

use App\Models\Tax;
use App\Models\Job;
use App\Models\Partner;
use App\Models\Account;
use App\Models\Currency;
use App\Models\GroupUnit;
use App\Models\Accounting;
use App\Models\BranchCode;
use App\Models\DeptVoucher;
use Illuminate\Http\Request;
use App\Models\ServicePurchase;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\PaymentReceiptVoucher;
use Illuminate\Support\Facades\Session;

class PaymentReceiptVouchersController extends Controller
{   
    public function index(Request $request)
    {   
        $query = '1=1';
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval($request->input('status', -1));
        $fromDate       = $request->input('from_date');
        $toDate         = $request->input('to_date');
        if($fromDate && $toDate) {
            $fromDate = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
            $toDate = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
            $query .= " AND created_at >= '" . $fromDate . "' AND created_at <= '" .$toDate. "'";
        } else if( !$fromDate && $toDate) {
            $toDate = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
            $query .= " AND  created_at <= '" .$toDate. "'";
        } else if ($fromDate && !$toDate) {
            $fromDate = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
            $query .= " AND  created_at >= '" .$fromDate. "'";
        }
        $objectName     = $request->input('object_name');
        if($objectName) {
            $queryObjectName = '1=1';
            $queryObjectName .= " AND (name like '%" . $objectName . "%')";
            $id = Partner::whereRaw($queryObjectName)->where('company_id', session('current_company'))->pluck('id')->toArray();
            $id =  implode(",", $id);
            $query .= " AND partner_id IN ($id)";
            
        }
        if($status<>-1) $query .=  " AND status = {$status}";
        $paymentReceiptVoucher = PaymentReceiptVoucher::whereRaw($query)->where('company_id', session('current_company'))->orderBy('id')->paginate($page_num);
        return view('backend.payment-receipt-vouchers.index', compact('paymentReceiptVoucher'));
    }

    public function create(Request $request) 
    {   
        $type = $request->type;
        if($type == 0 || $type ==1){
            $branchCodes        = BranchCode::getBranchCode();
            $partnerVendorCodes = Partner::getAllPartner();  
            $jobs               = Job::getJob();
            $currency           = Currency::getCurrency();
            $accounts           = Account::getActiveLowestLevel(session('current_company'));
            $group_unit         = GroupUnit::getGroupUnit();    
            $latest_trx_no      = PaymentReceiptVoucher::max('id');
            $trx_no             = $this->randomVoucher($type);
            return view('backend.payment-receipt-vouchers.create', compact('branchCodes', 'jobs', 'currency', 'partnerVendorCodes', 'accounts', 'trx_no', 'group_unit', 'type'));
            
        }
        Session::flash('message', trans('system.have_an_error'));
        Session::flash('alert-class', 'danger');
        return redirect()->route('admin.payment-receipt-vouchers.index');
      
    }

    public function store(Request $request)
    {   
        $data_payment_receipt_voucher = $this->getDataVoucher($request);
        $validator = \Validator::make($data_payment_receipt_voucher, PaymentReceiptVoucher::rules($data_payment_receipt_voucher['voucher_no']));
        $validator->setAttributeNames(trans('payment_receipt_vouchers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data_payment_receipt_voucher['created_by'] = $request->user()->id;
        $data_payment_receipt_voucher['company_id'] = session('current_company');
        unset($data_payment_receipt_voucher['object']);
        unset($data_payment_receipt_voucher['object_name']);
        $data_accounting = $this->getDataAccounting($request);
        $data_tax        = $this->getDataTax($request);
        $payment_voucher_receipt_id = 0;
        $data_payment_receipt_voucher['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->date_vouchers)));
        $request->merge(['status' => intval($request->input('status', 0))]);
        $data_payment_receipt_voucher['status'] = $request->status;
        $payment_receipt_voucher_id = PaymentReceiptVoucher::create($data_payment_receipt_voucher)->id;
        $data_tax = array_map(function ($value) use ($payment_receipt_voucher_id) {
            $value['invoice_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['invoice_date'])));
            return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
        }, $data_tax);
        $data_accounting = array_map(function ($value) use ($payment_receipt_voucher_id) {
            return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
            }, $data_accounting);
        Accounting::insert($data_accounting);
        Tax::insert($data_tax);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.payment-receipt-vouchers.index');
		
    }

    public function edit(Request $request, $id) 
    {   
        $branchCodes = BranchCode::getBranchCode();
        $vendor      = Partner::getVendor();
        $partnerVendorCodes = Partner::getAllPartner();  
        $jobs    = Job::getJob();
        $accounts = Account::getActiveLowestLevel(session('current_company'));
        $currency = Currency::getCurrency();
        $group_unit = GroupUnit::getGroupUnit();
        $paymentReceiptVoucher = PaymentReceiptVoucher::find(intval($id))->load('accounting','taxes');
        return view('backend.payment-receipt-vouchers.edit', compact('paymentReceiptVoucher', 'accounting', 'branchCodes', 'vendor', 'jobs', 'accounts', 'currency', 'partnerVendorCodes', 'group_unit')); 
    }

    public function update(Request $request, $id)
    {   
        $paymentReceiptVoucher = PaymentReceiptVoucher::find(intval($id));
        if (is_null($paymentReceiptVoucher)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.payment-receipt-vouchers.index');
        }
        $accounting = Accounting::where('payment_receipt_voucher_id', $paymentReceiptVoucher->id);
        $tax = Tax::where('payment_receipt_voucher_id', $paymentReceiptVoucher->id);
        $data_payment_receipt_voucher = $this->getDataVoucher($request);
        $validator = \Validator::make($data_payment_receipt_voucher, PaymentReceiptVoucher::rules($data_payment_receipt_voucher['voucher_no']));
        $validator->setAttributeNames(trans('payment_receipt_vouchers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data_payment_receipt_voucher['created_by'] = $request->user()->id;
        $data_payment_receipt_voucher['company_id'] = session('current_company');
        unset($data_payment_receipt_voucher['object']);
        unset($data_payment_receipt_voucher['object_name']);
        $accounting = PaymentReceiptVoucher::find($id)->accounting->pluck('id')->toArray();
        if ($accounting) {
            $data_accounting = $this->getDataAccounting($request);
        }
        $tax = PaymentReceiptVoucher::find($id)->Taxes->pluck('id')->toArray();
        if ($tax) {
            $data_tax = $this->getDataTax($request);
        }
        $data_payment_receipt_voucher['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->date_vouchers)));
        $request->merge(['status' => intval($request->input('status', 0))]);
        $data_payment_receipt_voucher['status'] = $request->status;
        DB::beginTransaction();
        try {
            $payment_receipt_voucher_id = $paymentReceiptVoucher->updateOrCreate($data_payment_receipt_voucher)->id;
            $data_tax = array_map(function ($value) use ($payment_receipt_voucher_id) {
                $value['invoice_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['invoice_date'])));
                return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
            }, $data_tax);
            $data_accounting = array_map(function ($value) use ($payment_receipt_voucher_id) {
            return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
            }, $data_accounting);
            Accounting::destroy($accounting);
            Tax::destroy($tax);
            Accounting::insert($data_accounting);
            Tax::insert($data_tax);
            DB::commit();
            Session::flash('message', trans('system.success'));
            Session::flash('alert-class', 'success');
            return redirect()->route('admin.payment-receipt-vouchers.index');
        } catch (\Exception $e) {
			DB::rollBack();
			return back()->withErrors($e->getMessage())->withInput();
		}
        
    }

    public function createPaymentVendorStep_1(Request $request)
    {   
        $vendor = Partner::getVendor();
        $currency = Currency::getCurrency();
        return view('backend.payment-receipt-vouchers.vendor.create-payment-vendor-1', compact('vendor', 'currency'));
    }

    public function getVoucher(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $request->id_check;
                $id = $request->id;
                $exchange_rate = $request->exchange_rate??0;
                $servicePurchase = ServicePurchase::where('partner_id', $id);
                $id = $servicePurchase->pluck('id')->toArray();
                if(is_null($id)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . '1', 1);
                }
                foreach ($id as $b) {
                    $data1 = [];
                    $accounting_tax = Accounting::where('accounting.service_purchase_id', $b)
                    ->join('taxes','taxes.service_purchase_id', '=', 'accounting.service_purchase_id')
                    ->where('taxes.credit_account', 'like', '201VN00%')
                    ->groupBy('taxes.credit_account')
                    ->groupBy('taxes.service_purchase_id')
                    ->groupBy('taxes.invoice_no')
                    ->groupBy('taxes.invoice_date')
                    ->selectRaw('taxes.invoice_no,taxes.invoice_date,taxes.credit_account, taxes.service_purchase_id,sum(amount)+sum(taxation) as amount, (sum(amount)+sum(taxation))*'.$exchange_rate.' as exchange')
                    ->get()
                    ->toArray();
                    $count = count($accounting_tax);
                    for($i=0; $i<$count; $i++){
                        if(count(DeptVoucher::where('service_purchase_id', $accounting_tax[$i]['service_purchase_id'])->Where('account_payment',$accounting_tax[$i]['credit_account'])->Where('invoice_no', $accounting_tax[$i]['invoice_no'])->get())>0)
                        {
                            unset($accounting_tax[$i]);
                        }
                    }
                    if(empty($accounting_tax)){
                        continue;
                    } else {
                        $data[$b] = $accounting_tax;
                    }
                }
                if(is_null($data)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . 'can not find vouchers', 1);
                }
                $servicePurchase = $servicePurchase->get()->toArray();
                if(is_null($servicePurchase)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . '1', 1);
                }
                foreach($servicePurchase as $key => $value)
                {
                    $voucher[$value['id']] = $value;
                }
                $htmlview = view('backend.payment-receipt-vouchers.dept-voucher-check', compact('voucher', 'data'))->render();
                $response['data']  = ['html'=>$htmlview];
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function createPaymentVendorStep_2(Request $request)
    {
        $dataStep_1 = [];
        $data = [];
        if(!isset($request->amount)){
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('message', 'you need choose voucher');
            return back()->withErrors($errors)->withInput();
        }

        if(!isset($request->currency)){
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('message', 'currency is required');
            return back()->withErrors($errors)->withInput();
        }

        for ($i=0; $i < count($request->amount) ; $i++) { 
            $data['id'] = $request->id[$i];
            $data['voucher_date'] = $request->voucher_date[$i];
            $data['voucher_no'] = $request->voucher_no[$i];
            $data['invoice_date'] = $request->invoice_date[$i];
            $data['invoice_no'] = $request->invoice_no[$i];
            $data['explain'] = $request->explain[$i];
            $data['due_date'] = $request->due_date[$i];
            $data['amount'] = str_replace('/,/g', '', $request->amount[$i]);
            $data['exchange'] = $request->exchange[$i];
            $data['account_payment'] = $request->account_payment[$i];
            array_push($dataStep_1, $data);
        }
        $accounting = [];
        foreach($dataStep_1 as $key =>$item)
        {  
            $accounting[$item['account_payment']][$key] = [
                'amount'=>$item['amount'],
                'exchange' => $item['exchange'],
            ];
        }
        $newAccounting = [];
        foreach($accounting as $key => $value)
        {
            $amount = 0;
            $exchange = 0;
            foreach($value as $k => $v)
            {   
                $amount = $amount + (float)$v['amount'];
                $exchange = $exchange + (float)$v['exchange'];
            }
            array_push($newAccounting, ['account_payment' => $key, "amount" => $amount, 'exchange' => $exchange]);
        }

        $data_start = [
            "idVendor"       => $request->vendor,
            "vendorName"     => $request->vendor_name,
            "date_payment"   => $request->date_payment,
            "with_amount"    => $request->with_amount,
            "currency"       => $request->currency,
            "type"           => $request->type,
            "total_amount"   => $request->total_amount,
            "total_exchange" => $request->total_exchange,
            "exchange_rate"  => $request->exchange_rate,
        ];

        $type = $request->type;
        if($type == 0 || $type ==1){
            $branchCodes        = BranchCode::getBranchCode();
            $partnerVendorCodes = Partner::getAllPartner();  
            $jobs               = Job::getJob();
            $currency           = Currency::getCurrency();
            $accounts           = Account::getActiveLowestLevel(session('current_company'));
            $group_unit         = GroupUnit::getGroupUnit();
            $trx_no             = $this->randomVoucher($type);
            return view('backend.payment-receipt-vouchers.vendor.create-payment-vendor-2', compact('branchCodes', 'jobs', 'currency', 'partnerVendorCodes', 'accounts', 'trx_no', 'group_unit', 'type', 'dataStep_1', 'data_start', 'newAccounting'));
            
        }
        Session::flash('message', trans('system.have_an_error'));
        Session::flash('alert-class', 'danger');
        return redirect()->route('admin.payment-receipt-vouchers.index');
    }
    
    public function storePaymentVendor(Request $request)
    {   
        $paymentReceiptVoucher = PaymentReceiptVoucher::find(intval($id));
        $data_payment_receipt_voucher = $this->getDataVoucher($request);
        $data_payment_receipt_voucher['total_amount_deptv'] = $request->total_amount;
        $data_payment_receipt_voucher['total_exchange_deptv'] = $request->total_exchange;
        $validator = \Validator::make($data_payment_receipt_voucher, PaymentReceiptVoucher::rules($data_payment_receipt_voucher['trx_no']));
        $validator->setAttributeNames(trans('payment_receipt_vouchers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data_payment_receipt_voucher['created_by'] = $request->user()->id;
        $data_payment_receipt_voucher['company_id'] = session('current_company');
        unset($data_payment_receipt_voucher['object']);
        unset($data_payment_receipt_voucher['object_name']);
        $data_accounting = $this->getDataAccounting($request);
        $data_deptv = $this->getDataDeptV($request);
        $payment_voucher_receipt_id = 0;
        $data_payment_receipt_voucher['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->date_vouchers)));
        $data_payment_receipt_voucher['status'] = $request->status;
        $payment_receipt_voucher_id = PaymentReceiptVoucher::create($data_payment_receipt_voucher)->id;
        $data_deptv = array_map(function ($value) use ($payment_receipt_voucher_id) {
           $value['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['voucher_date'])));
           $value['invoice_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['invoice_date'])));
           $value['due_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['due_date'])));
           return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
        }, $data_deptv);
        $data_accounting = array_map(function ($value) use ($payment_receipt_voucher_id) {
            return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
         }, $data_accounting);
    
        Accounting::insert($data_accounting);
        DeptVoucher::insert($data_deptv);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.payment-receipt-vouchers.index');
        
    }

    public function editPaymentVendor(Request $request, $id)
    {
        $branchCodes = BranchCode::getBranchCode();
        $vendor      = Partner::getVendor();
        $partnerVendorCodes = Partner::getAllPartner();  
        $jobs     = Job::getJob();
        $accounts = Account::getActiveLowestLevel(session('current_company'));
        $currency = Currency::getCurrency();
        $group_unit = GroupUnit::getGroupUnit();
        $paymentReceiptVoucher = PaymentReceiptVoucher::find($id)->load('accounting','dept_vouchers');
        return view('backend.payment-receipt-vouchers.vendor.edit-payment-vendor', compact('paymentReceiptVoucher', 'accounting', 'branchCodes', 'vendor', 'jobs', 'accounts', 'currency', 'partnerVendorCodes', 'group_unit')); 
    }

    public function updatePaymentVendor(Request $request, $id)
    {
        $paymentReceiptVoucher = PaymentReceiptVoucher::find($id);
        $data_payment_receipt_voucher = $this->getDataVoucher($request);
        $data_payment_receipt_voucher['total_amount_deptv'] = $request->total_amount;
        $data_payment_receipt_voucher['total_exchange_deptv'] = $request->total_exchange;
        $validator = \Validator::make($data_payment_receipt_voucher, PaymentReceiptVoucher::rules($data_payment_receipt_voucher['voucher_no']));
        $validator->setAttributeNames(trans('payment_receipt_vouchers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data_payment_receipt_voucher['created_by'] = $request->user()->id;
        $data_payment_receipt_voucher['company_id'] = session('current_company');
        unset($data_payment_receipt_voucher['object']);
        unset($data_payment_receipt_voucher['object_name']);
        $data_payment_receipt_voucher['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->date_vouchers)));
        $paymentReceiptVoucher->update($data_payment_receipt_voucher);
        $data_accounting = [];
        for ($i = 0; $i < count($request->id_accounting); $i++) {
            $accounting = Accounting::find($request->id_accounting[$i]);
            $accounting->description = $request->description[$i];
            $accounting->save();
        } 
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.payment-receipt-vouchers.index');
    }

    public function createPaymentOrder(Request $request)
    {
        $action = 1;
        $type = 1;
        if($type == 0 || $type == 1){
            $branchCodes        = BranchCode::getBranchCode();
            $partnerVendorCodes = Partner::getAllPartner();  
            $jobs               = Job::getJob();
            $currency           = Currency::getCurrency();
            $accounts           = Account::getActiveLowestLevel(session('current_company'));
            $group_unit         = GroupUnit::getGroupUnit();
            $trx_no             = $this->randomVoucher($type);
            return view('backend.payment-receipt-vouchers.bank.create-payment-order', compact('branchCodes', 'jobs', 'currency', 'partnerVendorCodes', 'accounts', 'trx_no', 'group_unit', 'type', 'action'));
        }
        Session::flash('message', trans('system.have_an_error'));
        Session::flash('alert-class', 'danger');
        return redirect()->route('admin.payment-receipt-vouchers.index');
    }
    public function createReceiptBank(Request $request)
    {   
        $action = 1;
        $type = 0;
        if($type == 0 || $type == 1){
            $branchCodes        = BranchCode::getBranchCode();
            $partnerVendorCodes = Partner::getAllPartner();  
            $jobs               = Job::getJob();
            $currency           = Currency::getCurrency();
            $accounts           = Account::getActiveLowestLevel(session('current_company'));
            $group_unit         = GroupUnit::getGroupUnit();
            $trx_no             = $this->randomVoucher($type);
            return view('backend.payment-receipt-vouchers.bank.create_receipt_bank', compact('branchCodes', 'jobs', 'currency', 'partnerVendorCodes', 'accounts', 'trx_no', 'group_unit', 'type', 'action'));
            
        }
        Session::flash('message', trans('system.have_an_error'));
        Session::flash('alert-class', 'danger');
        return redirect()->route('admin.payment-receipt-vouchers.index');
    }

    public function storeReceiptPaymentBank(Request $request)
    {
        $data_payment_receipt_voucher = $this->getDataVoucher($request);
        $data_payment_receipt_voucher['total_amount_deptv'] = $request->total_amount;
        $data_payment_receipt_voucher['total_exchange_deptv'] = $request->total_exchange;
        $validator = \Validator::make($data_payment_receipt_voucher, PaymentReceiptVoucher::rules($data_payment_receipt_voucher['trx_no']));
        $validator->setAttributeNames(trans('payment_receipt_vouchers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data_payment_receipt_voucher['created_by'] = $request->user()->id;
        $data_payment_receipt_voucher['company_id'] = session('current_company');
        unset($data_payment_receipt_voucher['object']);
        unset($data_payment_receipt_voucher['object_name']);
        $data_accounting = [];
        $data_accounting = $this->getDataAccounting($request);
        $data_tax        = $this->getDataTax($request);

        $payment_voucher_receipt_id = 0;
        $data_payment_receipt_voucher['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->date_vouchers)));
        $data_payment_receipt_voucher['status'] = $request->status;
        $payment_receipt_voucher_id = PaymentReceiptVoucher::create($data_payment_receipt_voucher)->id;
        $data_accounting = array_map(function ($value) use ($payment_receipt_voucher_id) {
            return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
        }, $data_accounting);
        Accounting::insert($data_accounting);
        if(!empty($data_tax))
        {
            $data_tax = array_map(function ($value) use ($payment_receipt_voucher_id) {
                $value['invoice_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['invoice_date'])));
                return array_merge($value, ['payment_receipt_voucher_id' => $payment_receipt_voucher_id]);
            }, $data_tax);
            Tax::insert($data_tax);
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.payment-receipt-vouchers.index');
    }

    public function editReceiptPaymentBank(Request $request, $id)
    {
        $action = 1;
        $branchCodes = BranchCode::getBranchCode();
        $vendor      = Partner::getVendor();
        $partnerVendorCodes = Partner::getAllPartner();  
        $jobs     = Job::getJob();
        $accounts = Account::getActiveLowestLevel(session('current_company'));
        $currency = Currency::getCurrency();
        $group_unit = GroupUnit::getGroupUnit();
        $paymentReceiptVoucher = PaymentReceiptVoucher::find(intval($id))->load('accounting','taxes');
        return view('backend.payment-receipt-vouchers.bank.edit-receipt-payment-bank', compact('paymentReceiptVoucher', 'accounting', 'branchCodes', 'vendor', 'jobs', 'accounts', 'currency', 'partnerVendorCodes', 'group_unit', 'action')); 
    }

    public function UpdateReceiptBank(Request $request, $id)
    {
        $paymentReceiptVoucher = PaymentReceiptVoucher::find($id);
        $data_payment_receipt_voucher = $this->getDataVoucher($request);
        $data_payment_receipt_voucher['total_amount_deptv'] = $request->total_amount;
        $data_payment_receipt_voucher['total_exchange_deptv'] = $request->total_exchange;
        $validator = \Validator::make($data_payment_receipt_voucher, PaymentReceiptVoucher::rules($data_payment_receipt_voucher['voucher_no']));
        $validator->setAttributeNames(trans('payment_receipt_vouchers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data_payment_receipt_voucher['created_by'] = $request->user()->id;
        $data_payment_receipt_voucher['company_id'] = session('current_company');
        unset($data_payment_receipt_voucher['object']);
        unset($data_payment_receipt_voucher['object_name']);
        $data_payment_receipt_voucher['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->date_vouchers)));
        $paymentReceiptVoucher->update($data_payment_receipt_voucher);
        $data_accounting = [];
        for ($i = 0; $i < count($request->id_accounting); $i++) {
            $accounting = Accounting::find($request->id_accounting[$i]);
            $accounting->description = $request->description[$i];
            $accounting->save();
        } 
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.payment-receipt-vouchers.index');
    }

    public function destroy(Request $request)
    {
        
    }

    public function delete_all(Request $request)
    {
        if ($request->ajax()) {
            $return = ['error' => true, 'message' => trans('system.have_an_error')];
            $ids = json_decode($request->input('ids'));
            if (empty($ids)) return response()->json(['error' => true, 'message' => trans('system.no_item_selected')]);
            foreach ($ids as $id) {
                foreach ($ids as $id) {
                    $paymentReceiptVoucher = PaymentReceiptVoucher::find(intval($id));
                    $accounting = $paymentReceiptVoucher->accounting->pluck('id')->toArray();
                    $tax = $paymentReceiptVoucher->Taxes->pluck('id')->toArray();
                    $paymentReceiptVoucher->delete();
                    Accounting::destroy($accounting);
                    Tax::destroy($tax);
                }
            }
            $return['error'] = false;
            $return['message'] = trans('system.success');
            Session::flash('message', $return['message']);
            Session::flash('alert-class', 'success');
            return response()->json($return);
        }
    }

    public function show()
    {

    }

    public function getDataAccounting(Request $request)
    {
        $data_accounting = [];
       
        for ($i = 0; $i < count($request->debit_account); $i++) {
            $data_accounting_row['description'] = $request->description[$i];
            $data_accounting_row['job_code_id'] = $request->job_code[$i];
            $data_accounting_row['debit_account'] = $request->debit_account[$i];
            $data_accounting_row['credit_account'] = $request->credit_account[$i];
            $data_accounting_row['amount'] = $request->amount[$i]??$request->amount_accounting[$i];
            $data_accounting_row['exchange'] = $request->exchange[$i]??$request->exchange_accounting[$i];
            $data_accounting_row['branch_code_id'] = $request->branch_code[$i];
            $data_accounting_row['created_by'] = $request->user()->id;
            $data_accounting_row['company_id'] = session('current_company');
            if($request->type == 1) {
                $data_accounting_row['debit_object_id'] = intval($request->object_code_accounting[$i]);
            } else {
                $data_accounting_row['credit_object_id'] = intval($request->object_code_accounting[$i]);
            }
            $validator = \Validator::make($data_accounting_row, Accounting::rules());
            $validator->setAttributeNames(trans('accounting')); 
            if ($validator->fails()) return back()->withErrors($validator)->withInput();
            array_push($data_accounting, $data_accounting_row);
        }   

        return $data_accounting;
    }

    public function getDataDeptV(Request $request)
    {
        $data_deptv = [];
        for ($i = 0; $i < count($request->amount); $i++) {
            $data_dept_row['desc'] = $request->explain[$i];
            $data_dept_row['voucher_date'] = $request->day_voucher[$i];
            $data_dept_row['invoice_date'] = $request->invoice_date[$i];
            $data_dept_row['invoice_no'] = $request->invoice_no[$i];
            $data_dept_row['voucher_no'] = $request->trx_no[$i];
            $data_dept_row['due_date'] = $request->due_date[$i];
            $data_dept_row['amount'] = $request->amount[$i];
            $data_dept_row['exchange'] = $request->exchange[$i];
            $data_dept_row['account_payment'] = $request->account_payment[$i];
            $data_dept_row['created_by'] = $request->user()->id;
            $data_dept_row['service_purchase_id'] = $request->id[$i];
            array_push($data_deptv, $data_dept_row);
        } 
        return $data_deptv;
    }
    public function getDataVoucher(Request $request)
    {
        $data_payment_receipt_voucher['partner_id']    = $request->object??$request->object_id;
        $data_payment_receipt_voucher['beneficiary']   = $request->beneficiary;
        $data_payment_receipt_voucher['object']        = $request->object??$request->object_id;
        $data_payment_receipt_voucher['object_name']   = $request->object_name;
        $data_payment_receipt_voucher['desc']          = $request->details;
        $data_payment_receipt_voucher['voucher_date']    = $request->date_vouchers;
        $data_payment_receipt_voucher['voucher_no']      = $request->voucher_no;
        $data_payment_receipt_voucher['currency_id']     = $request->currency??$request->currency_id;
        $data_payment_receipt_voucher['exchange_rate']   = $request->exchange_rate;
        $data_payment_receipt_voucher['address']         = $request->address;
        if($request->type == 1) {
            $data_payment_receipt_voucher['receipt_account_id'] = intval($request->payment_account);
        } else {
            $data_payment_receipt_voucher['payment_account_id'] = intval($request->payment_account);
        }
        $data_payment_receipt_voucher['payment_account'] = $request->payment_account;
        $data_payment_receipt_voucher['type'] = $request->type??0;
        $data_payment_receipt_voucher['object_type'] = $request->object_type??0;
        $data_payment_receipt_voucher['total_amount_accounting'] = $request->total_amount_accounting??$request->total_with_amount;
        $data_payment_receipt_voucher['total_exchange_accounting'] = $request->total_exchange_accounting??$request->total_exchange;
        $data_payment_receipt_voucher['total_taxation'] = $request->total_taxation;
        $data_payment_receipt_voucher['total_exchange_taxation'] = $request->total_exchange_tax;
        $data_payment_receipt_voucher['total_unit_price_tax'] = $request->total_service_value;
        $data_payment_receipt_voucher['total_exchange_unit_price_tax'] = $request->total_exchange_service;
        return $data_payment_receipt_voucher;
    }

    public function getDataTax(Request $request)
    {
        $data_tax = [];
        for ($i = 0; $i < count($request->tax_rate); $i++) {
            $data_tax_row['description']    = $request->description_tax[$i];
            $data_tax_row['tax_rate']       = $request->tax_rate[$i];
            $data_tax_row['credit_account'] = $request->credit_account[$i];
            $data_tax_row['taxation']       = $request->taxation[$i];
            $data_tax_row['exchange_tax']   = $request->exchange_tax[$i];
            $data_tax_row['unit_price']     = $request->unit_price[$i];
            $data_tax_row['exchange_unit']  = $request->exchange_unit_price[$i];
            $data_tax_row['account_tax']    = $request->account_tax[$i];
            $data_tax_row['group_unit_buy'] = $request->group_unit[$i];
            $data_tax_row['invoice_no']     = $request->invoice_no[$i];
            $data_tax_row['invoice_date']   =  $request->date_invoice[$i];
            $data_tax_row['partner_id']     = intval($request->object_code_tax[$i]);
            $data_tax_row['tax_code']       = $request->tax_code[$i];
            $data_tax_row['created_by']     = $request->user()->id;
            $data_tax_row['company_id']     = session('current_company');
            $validator = \Validator::make($data_tax_row, Tax::rules());
            $validator->setAttributeNames(trans('taxes')); 
            if ($validator->fails()) return back()->withErrors($validator)->withInput();
            array_push($data_tax, $data_tax_row);
        }
        return $data_tax;
    }

    public function randomVoucher($type)
    {
        $latest_trx_no      = PaymentReceiptVoucher::max('id');
        $new_trx_no         = $latest_trx_no + 1;
        $firstTrx_no        = "";

        if($type == 1) {
            $firstTrx_no = "PC";
        } else {
            $firstTrx_no = "PT";
        }

        if(strlen($new_trx_no) == 1)
        {
            $trx_no = $firstTrx_no."000000".$new_trx_no;
        }

        if(strlen($new_trx_no) == 2)
        {
            $trx_no = $firstTrx_no."00000".$new_trx_no;
        }

        if(strlen($new_trx_no) == 3)
        {
            $trx_no = $firstTrx_no."0000".$new_trx_no;
        }

        if(strlen($new_trx_no) == 4)
        {
            $trx_no = $firstTrx_no."000".$new_trx_no;
        }

        if(strlen($new_trx_no) == 5)
        {
            $trx_no = $firstTrx_no."00".$new_trx_no;
        }

        if(strlen($new_trx_no) == 6)
        {
            $trx_no = $firstTrx_no."0".$new_trx_no;
        }

        if(strlen($new_trx_no) == 7){
            $trx_no = $firstTrx_no.$new_trx_no;
        }

        return $trx_no;
    }
}
