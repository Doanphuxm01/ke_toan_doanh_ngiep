<?php

namespace App\Http\Controllers\Backend;

use App\Models\Account;
use Carbon\Carbon;
use App\Models\Tax;
use App\Models\Accounting;
use Illuminate\Http\Request;
use App\Models\AccountingInvoice;
use Illuminate\Support\Facades\DB;
use App\Repositories\TaxRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Repositories\AccountingRepository;
use App\Repositories\AccountingInvoiceRepository;

class AccountingInvoiceController extends Controller
{
	protected $accountingInvoiceRepo;
	protected $taxRepository;
	protected $accountingRepository;

	public function __construct(
		AccountingInvoiceRepository $accountingInvoiceRepository,
		TaxRepository $taxRepository,
		AccountingRepository $accountingRepository
	)
	{
		$this->accountingInvoiceRepo = $accountingInvoiceRepository;
		$this->taxRepository = $taxRepository;
		$this->accountingRepository = $accountingRepository;
	}

	public function index(Request $request)
    {
		$query = "1=1";
		$page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
		$status         = intval($request->input('status', -1));
		$desc 			= $request->input('desc');
		$fromDate       = $request->input('from_date');
		$toDate         = $request->input('to_date');
		if($fromDate && $toDate) {
			$fromDate = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
			$toDate = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
			$query .= " AND created_at >= '" . $fromDate . "' AND created_at <= '" .$toDate. "'";
		} else if( !$fromDate && $toDate) {
			$toDate = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
			$query .= " AND  created_at <= '" .$toDate. "'";
		} else if ($fromDate && !$toDate) {
			$fromDate = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
			$query .= " AND  created_at >= '" .$fromDate. "'";
		}
		if($desc) $query .=  " AND (`desc` like '%" . $desc . "%')";
		if($status<>-1) $query .=  " AND status = {$status}";
		$total = AccountingInvoice::sum('total_amount');
    	$accountingInvoices = AccountingInvoice::whereRaw($query)->orderBy('created_at', 'desc')->with('accounting', 'taxes')->paginate($page_num);
        return view('backend.accounting-invoices.index', compact('accountingInvoices', 'total'));
    }

    public function create()
    {
		return view('backend.accounting-invoices.create');
    }

    public function storeOrUpdate(Request $request)
    {
        $accountingInvoice = $request->only(['accounting_invoice_id', 'desc', 'voucher_no', 'voucher_date', 'payment_term', 'exchange_rate', 'currency', 'total_amount']);
		$accountingData = $request->only(['accounting_id', 'desc_accounting', 'job_code', 'debit_account', 'credit_account', 'amount', 'exchange', 'branch_code', 'debit_object', 'credit_object', 'cost_item']);
		$taxData = $request->only(['tax_id', 'desc_tax', 'tax_rate', 'taxation', 'exchange_tax', 'unit_price', 'exchange_unit', 'account_tax', 'group_unit', 'invoice_no', 'date_invoice', 'object_code_tax', 'object_name_tax', 'tax_code']);
		$validatorAccountingInvoice = \Validator::make($accountingInvoice, AccountingInvoice::rules());
		$validatorAccountingInvoice->setAttributeNames(trans('accounting_invoices'));
		if ($validatorAccountingInvoice->fails()) return back()->withErrors($validatorAccountingInvoice)->withInput();
		$countDebit = count($accountingData['debit_account']);
		$countTax = count($taxData['tax_rate']);
		$accounting = $taxes = [];
		DB::beginTransaction();
		try {
			$accountingInvoiceNew = $this->accountingInvoiceRepo->save($accountingInvoice, $accountingInvoice['accounting_invoice_id']);

			Accounting::where('accounting_invoice_id', $accountingInvoice['accounting_invoice_id'])->whereNotIn('id', $accountingData['accounting_id'])->delete();
			Tax::where('accounting_invoice_id', $accountingInvoice['accounting_invoice_id'])->whereNotIn('id', $taxData['tax_id'])->delete();
			for ($i = 0; $i < $countDebit; $i++) {
				$accounting = [
					'id'					=> $accountingData['accounting_id'][$i],
					'accounting_invoice_id' => $accountingInvoiceNew->id,
					'desc' 					=> $accountingData['desc_accounting'][$i],
					'job_code_id' 			=> $accountingData['job_code'][$i],
					'debit_account' 		=> $accountingData['debit_account'][$i],
					'credit_account' 		=> $accountingData['credit_account'][$i],
					'amount' 				=> $accountingData['amount'][$i],
					'exchange' 				=> $accountingData['exchange'][$i],
					'branch_code_id' 		=> $accountingData['branch_code'][$i],
					'debit_object_id' 		=> $accountingData['debit_object'][$i],
					'credit_object_id' 		=> $accountingData['credit_object'][$i],
					'cost_item' 			=> $accountingData['cost_item'][$i],
				];
				$validator = \Validator::make($accounting, Accounting::rules());
				$validator->setAttributeNames(trans('accounting'));
				if ($validator->fails()) return back()->withErrors($validator)->withInput();
				$this->accountingRepository->save($accounting, $accounting['id']);
			}

			for ($i = 0; $i < $countTax; $i++) {
				$taxes = [
					'id'			=> $taxData['tax_id'][$i],
					'desc' 			=> $taxData['desc_tax'][$i],
					'tax_rate' 		=> $taxData['tax_rate'][$i],
					'taxation' 		=> $taxData['taxation'][$i],
					'exchange_tax' 	=> $taxData['exchange_tax'][$i],
					'unit_price' 	=> $taxData['unit_price'][$i],
					'exchange_unit' => $taxData['exchange_unit'][$i],
					'account_tax' 	=> $taxData['account_tax'][$i],
					'group_unit' 	=> $taxData['group_unit'][$i],
					'invoice_no' 	=> $taxData['invoice_no'][$i],
					'invoice_date' 	=> $taxData['date_invoice'][$i] ? Carbon::createFromFormat('d/m/Y', $taxData['date_invoice'][$i])->format('Y-m-d') : null,
					'partner_id' 	=> $taxData['object_code_tax'][$i],
					'partner_name' 	=> $taxData['object_name_tax'][$i],
					'tax_code' 		=> $taxData['tax_code'][$i],
				];
				if ($this->checkArrNull($taxes)) continue;
				$taxes['accounting_invoice_id'] = $accountingInvoiceNew->id;
				$validator = \Validator::make($taxes, Tax::rules());
				$validator->setAttributeNames(trans('taxes'));
				if ($validator->fails()) return back()->withErrors($validator)->withInput();
				$this->taxRepository->save($taxes, $taxes['id']);
			}
			DB::commit();
			Session::flash('message', trans('system.success'));
			Session::flash('alert-class', 'success');
			return redirect()->route('admin.accounting-invoices.index');
		} catch (\Exception $e) {
			DB::rollBack();
			return back()->withErrors($e->getMessage())->withInput();
		}

    }

    public function show($id)
    {
		$accountingInvoice = AccountingInvoice::where('id', $id)->with('accounting', 'taxes')->first();
		return view('backend.accounting-invoices.show', compact('accountingInvoice'));
    }

    public function edit($id)
    {
		$accountingInvoice = AccountingInvoice::where('id', $id)->with('accounting', 'taxes')->first();
		return view('backend.accounting-invoices.edit', compact('accountingInvoice'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
		$accountingInvoice = AccountingInvoice::find($id);
		if (is_null($accountingInvoice)) {
			Session::flash('message', trans('system.have_an_error'));
			Session::flash('alert-class', 'danger');
			return redirect()->route('admin.accounting-invoices.index');
		}
		try {
			DB::beginTransaction();
			$accountingInvoice->accounting()->delete();
			$accountingInvoice->taxes()->delete();
			$accountingInvoice->delete();
			DB::commit();
			Session::flash('message', trans('system.success'));
			Session::flash('alert-class', 'success');
			return redirect()->route('admin.accounting-invoices.index');
		} catch (\Exception $e) {
			DB::rollBack();
			return back()->withErrors($e->getMessage());
		}
    }

    public function checkArrNull(array $arr)
	{
		return empty(array_filter($arr, function ($a) {return $a != null && intval($a) > 0 ;}));
	}

	public function searchForSelect(Request $request)
	{
		$search = $request->search ?? '';
		$result = Account::where('company_id', session('current_company'))->where('name', 'LIKE', '%' . $search . '%')->paginate(10);
		return response()->json($result);
	}
}
