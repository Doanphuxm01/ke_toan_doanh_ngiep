<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\CostItem;
use App\Models\FixedAssetAllocationInformation;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CostItemController 
{
    public function index(Request $request){
        $query = "1=1"; 
        $page_num = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
        $cost_item = $request->input('cost_item');
        $status = intval($request->input('status', -1));
        if($status <> -1) $query .= " AND status = {$status}";
        if($cost_item) $query .= " AND (cost_item like '%" . $cost_item . "%' )";
        $cost_items = CostItem::whereRaw($query)->where('company_id', session('current_company'))->orderBy('id', 'DESC')->paginate($page_num);
        return view('backend.cost-items.index', compact('cost_items', 'check'));
    }
    public function show(Request $request, $id)
    {
        $cost_item = CostItem::find(intval($id));
        if (is_null($cost_item)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.cost-items.index');
        }
        return view('backend.cost-items.show', compact('cost_item'));
    }
    public function create(){
        return view('backend.cost-items.create');
    }
    public function store(Request $request){
        $validator = Validator::make($data = $request->all(), CostItem::rules());
        $validator->setAttributeNames(trans('cost_items'));
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        $request->merge(['status' => intval($request->input('status', 0))]);
        $data['cost_item']   = $request->new_cost_item;
        $data['created_by']   = \Auth::guard('admin')->user()->id;
        $data['company_id'] = session('current_company');
        
        CostItem::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.cost-items.index');
    }
    public function edit(Request $request, $id){
        $cost_item = CostItem::find(intval($id));
        if (is_null($cost_item)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.cost-items.index');
        }
        return view('backend.cost-items.edit', compact('cost_item'));
    }
    public function update(Request $request, $id)
    {
        $cost_item = CostItem::find(intval($id));
        $request->merge(['status' => $request->input('status', 0)]);
        if (is_null($cost_item)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $validator = \Validator::make($data = $request->all(), CostItem::rules(intval($id)));
        $validator->setAttributeNames(trans('cost_items'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['cost_item'] = $request->new_cost_item;
        $cost_item->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.cost-items.index');
    }
    public function destroy(Request $request, $id)
    {
        $cost_item = CostItem::where('id', intval($id))->first();
        $cost_item->delete();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
}
