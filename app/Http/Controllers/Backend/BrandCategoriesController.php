<?php namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use App\ProductCategory as ProductCategory;
use App\BrandCategory as BrandCategory;
use App\Product as Product;
use App\Brand as Brand;

class BrandCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categoryId = intval($request->category);
        if ($categoryId) {
            $pCategories = ProductCategory::where('product_category_id', $categoryId)->get();
        } else {
            $pCategories = ProductCategory::where('product_category_id', 0)->orderBy('position')->get();
        }

        $categories = [];
        $parents = ProductCategory::where('product_category_id', 0)->where('status', 1)->orderBy('position')->get();
        foreach ($parents as $parent) {
            $categories[$parent->id] = $parent->name;
            $children = ProductCategory::where('product_category_id', $parent->id)->where('status', 1)->orderBy('position')->get();
            foreach ($children as $child) {
                $categories[$child->id] = '|____' . $child->name;
                // $tmp = ProductCategory::where('product_category_id', $child->id)->where('status', 1)->orderBy('position')->get();
                // foreach ($tmp as $t) {
                //     $categories[$t->id] = '|____|____' . $t->name;
                // }
            }
        }

        return view('backend.brand-categories.index', compact('pCategories', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $return = [ 'message' => trans('system.have_an_error') ];
                $ids = json_decode($request->input('brand_ids'));
                $category = ProductCategory::where('id', intval($id))->first();
                if (is_null($category)) {
                    throw new \Exception(trans('system.no_record_found'));
                }

                $brands = $category->brands()->select('brand_id')->get()->toArray();
                $brands = array_column($brands, 'brand_id');
                $addBrands = [];
                foreach ($ids as $id) {
                    if (!in_array($id, $brands)) {
                        array_push($addBrands, $id);
                    }
                }

                $removeBrands = [];
                foreach ($brands as $id) {
                    if (!in_array($id, $ids)) {
                        array_push($removeBrands, $id);
                    }
                }

                if ($removeBrands) {
                    $category->brands()->detach(array_values($removeBrands));
                }

                if ($addBrands) {
                    $category->brands()->attach(array_values($addBrands));
                }

                // update position
                for ($i = 1; $i <= count($ids); $i++) {
                    BrandCategory::where('product_category_id', $category->id)->where('brand_id', $ids[$i-1])->update(['position' => $i]);
                }

                BrandCategory::clearCache();

                $response['message'] = trans('system.success');
                Session::flash('message', $response['message']);
                Session::flash('alert-class', 'success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $pCategory = ProductCategory::find(intval($id));
        if (is_null($pCategory)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }

        $catBrands  = $pCategory->brands()->orderBy('position')->get();
        $brands     = Brand::where('status', 1)->whereNotIn('id', array_column($catBrands->toArray(), 'id'))->orderBy('name')->get();

        $categories = [];
        $parents = ProductCategory::where('product_category_id', 0)->where('status', 1)->orderBy('position')->get();
        foreach ($parents as $parent) {
            $categories[$parent->id] = $parent->name;
            $children = ProductCategory::where('product_category_id', $parent->id)->where('status', 1)->orderBy('position')->get();
            foreach ($children as $child) {
                $categories[$child->id] = '|____' . $child->name;
                $tmp = ProductCategory::where('product_category_id', $child->id)->where('status', 1)->orderBy('position')->get();
                foreach ($tmp as $t) {
                    $categories[$t->id] = '|____|____' . $t->name;
                }
            }
        }

        return view('backend.brand-categories.edit',compact('pCategory', 'catBrands', 'brands', 'categories'));
    }

    // public function getByCategory(Request $request)
    // {
    //     $response = [ 'message' => trans('system.have_an_error') ];
    //     $statusCode = 200;
    //     if($request->ajax()) {
    //         try {
    //             $category = ProductCategory::find($request->product_category_id);
    //             if (is_null($category))
    //                 $brands = [];
    //             else {
    //                 // $category = $category->getRoot();
    //                 $brands = $category->brands()->orderBy('position')->select("name", "id")->get();
    //                 $brands = array_column($brands->toArray(), "id", "name");
    //             }
    //             $response['message'] = $brands;
    //         } catch (\Exception $e) {
    //             if ($statusCode == 200) $statusCode = 500;
    //             $response['message'] = $e->getMessage();
    //         } finally {
    //             return response()->json($response, $statusCode);
    //         }
    //     } else {
    //         $statusCode = 405;
    //         return response()->json($response, $statusCode);
    //     }
    // }
}