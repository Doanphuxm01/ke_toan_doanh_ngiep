<?php

namespace App\Http\Controllers\Backend;

use App\Models\Department;
use App\Models\FixedAsset;
use App\Models\FixedAssetDepreciation;
use App\Models\FixedAssetAllocationCost;

use Illuminate\Http\Request;
use App\Exports\FixedAssetExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class FixedAssetReportController extends Controller
{

    public function index(Request $request)
    {
        $departments = Department::getDepartments(session('current_company'));
        $from_date = date("Y-m-d", strtotime(str_replace('/', '-',$request->from_date)));
        $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $request->to_date)));
        $query .= " AND voucher_date >= '" . $date_range[0] . "' AND voucher_date <= '" .$date_range[1]. "'";
        if(!$request->from_date && !$request->to_date){
            if($request->department == ""){
                $fixed_assets = FixedAsset::where('company_id', session('current_company'))
                                            ->join('fixed_asset_allocation_information', 'fixed_assets.asset_code', '=', 'fixed_asset_allocation_information.asset_code')
                                            ->select('fixed_assets.*', 'fixed_asset_allocation_information.department_id')
                                            ->get();
            }
            else{
                $fixed_assets = FixedAsset::where('company_id', session('current_company'))
                                            ->where('fixed_asset_allocation_information.department_id', $request->department)
                                            ->join('fixed_asset_allocation_information', 'fixed_assets.asset_code', '=', 'fixed_asset_allocation_information.asset_code')
                                            ->select('fixed_assets.*', 'fixed_asset_allocation_information.department_id')
                                            ->get();
            }
            foreach ($fixed_assets as $key => $value) {
                $voucher = FixedAssetDepreciation::all()->pluck('id')->toArray();
                $cost = FixedAssetAllocationCost::whereIn('voucher_id', $voucher)->where('asset_code', $value->asset_code)->get();
                $periodical_value = 0;
                foreach ($cost as $key1 => $value1) {
                    if($value->department_id == $value1->department_id){
                        $periodical_value += $value1->money_amount;
                    } 
                }
                $value['residual_value'] = $value->residual_value - $periodical_value;
                $value['accumulated_depreciation'] = $value->accumulated_depreciation + $periodical_value;
                $value['periodical_depreciation_value'] = $periodical_value;
                $value['department_name'] = Department::getDepartment($value->department_id); 
            }
        }
        else{
            if($request->department == ""){
                $fixed_assets = FixedAsset::where('company_id', session('current_company'))
                                            ->where('voucher_creation_date', '>=', $from_date)
                                            ->where('voucher_creation_date', '<=', $to_date)
                                            ->join('fixed_asset_allocation_information', 'fixed_assets.asset_code', '=', 'fixed_asset_allocation_information.asset_code')
                                            ->select('fixed_assets.*', 'fixed_asset_allocation_information.department_id')
                                            ->get();
            }
            else{
                $fixed_assets = FixedAsset::where('company_id', session('current_company'))
                                            ->where('voucher_creation_date', '>=', $from_date)
                                            ->where('voucher_creation_date', '<=', $to_date)
                                            ->where('fixed_asset_allocation_information.department_id', $request->department)
                                            ->join('fixed_asset_allocation_information', 'fixed_assets.asset_code', '=', 'fixed_asset_allocation_information.asset_code')
                                            ->select('fixed_assets.*', 'fixed_asset_allocation_information.department_id')
                                            ->get();
            }
            foreach ($fixed_assets as $key => $value) {
                $voucher1 = FixedAssetDepreciation::where('voucher_date', ">=", $from_date)->where('voucher_date', "<=", $to_date)->pluck('id')->toArray();
                $cost1 = FixedAssetAllocationCost::whereIn('voucher_id', $voucher1)->where('asset_code', $value->asset_code)->get();
                $periodical_value1 = 0;
                foreach ($cost1 as $key1 => $value1) {
                    if($value->department_id == $value1->department_id){
                        $periodical_value1 += $value1->money_amount;
                    } 
                }
                $voucher2 = FixedAssetDepreciation::where('voucher_date', "<=", $to_date)->pluck('id')->toArray();
                $cost2 = FixedAssetAllocationCost::whereIn('voucher_id', $voucher2)->where('asset_code', $value->asset_code)->get();
                $periodical_value2 = 0;
                foreach ($cost2 as $key2 => $value2) {
                    if($value->department_id == $value2->department_id){
                        $periodical_value2 += $value2->money_amount;
                    } 
                }
                $value['residual_value'] = $value->residual_value - $periodical_value2;
                $value['accumulated_depreciation'] = $value->accumulated_depreciation + $periodical_value2;
                $value['periodical_depreciation_value'] = $periodical_value1;
                $value['department_name'] = Department::getDepartment($value->department_id);  
            }
        }
        Session::put('fixed_assets', $fixed_assets);
        Session::put('from_date', $request->from_date);
        Session::put('to_date', $request->to_date);
        Session::put('department', Department::getDepartment($request->department));
        // dd($fixed_assets);
        
        return view('backend.fixed-asset-reports.index', compact('departments', 'fixed_assets'));
    }
    public function exportExcel(){
        return Excel::download(new FixedAssetExport, 'demo.xlsx'); 
    }

}