<?php

namespace App\Http\Controllers\Backend;

use App\Models\Account;
use App\Models\BranchCode;
use App\Models\SaleCostItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SaleCostItemController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $page_num   = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status     = intval($request->input('status', -1));
        $code       = $request->input('code');
        if($code) $query .= " AND (code like '%" . $code . "%' OR description like '%" . $code . "%' OR sale_account_code like '%" . $code . "%' OR cost_account_code like '%" . $code . "%' OR prov_account_code like '%" . $code . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        $saleCostItems = SaleCostItem::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();

        return view('backend.sale-cost-items.index', compact('saleCostItems', 'branchCodes'));
    }

    public function create(Request $request)
    {
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        // $accounts = Account::where('status', 1)->where('company_id', session('current_company'))->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        $accounts = Account::getActiveLowestLevel(session('current_company'));

        return view('backend.sale-cost-items.create', compact('branchCodes', 'accounts'));
    }

    public function show(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.sale-cost-items.index');
        }
        $currentCodes = [
            $saleCostItem->sale_account_code,
            $saleCostItem->cost_account_code,
            $saleCostItem->prov_account_code
        ];
        $currentCodes = implode("','", $currentCodes);
        $branchCodes = BranchCode::whereRaw("(status=1 OR code ='" . $saleCostItem->branch_code . "') AND company_id=" . session('current_company'))->pluck('code', 'code')->toArray();
        $accounts = Account::whereRaw("(status=1 OR (status=0 AND code IN('" . $currentCodes . "'))) AND company_id=" . session('current_company'))->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        return view('backend.sale-cost-items.show', compact('saleCostItem', 'branchCodes', 'accounts'));
    }

    public function edit(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.sale-cost-items.index');
        }
        $currentCodes = [
            $saleCostItem->sale_account_code,
            $saleCostItem->cost_account_code,
            $saleCostItem->prov_account_code
        ];
        $currentCodes = implode("','", $currentCodes);
        $branchCodes = BranchCode::whereRaw("(status=1 OR code ='" . $saleCostItem->branch_code . "') AND company_id=" . session('current_company'))->pluck('code', 'code')->toArray();
        $accounts = Account::whereRaw("(status=1 OR (status=0 AND code IN('" . $currentCodes . "'))) AND company_id=" . session('current_company'))->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        return view('backend.sale-cost-items.edit', compact('saleCostItem', 'branchCodes', 'accounts'));
    }

    public function store(Request $request)
    {

        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), SaleCostItem::rules());
        $validator->setAttributeNames(trans('sale_cost_items'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['created_by'] = $request->user()->id;
        $data['company_id'] = session('current_company');
        SaleCostItem::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.sale-cost-items.index');
    }

    public function update(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.sale-cost-items.index');
        }
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), SaleCostItem::rules(intval($id)));
        $validator->setAttributeNames(trans('sale_cost_items'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $saleCostItem->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.sale-cost-items.index');
    }

    public function destroy(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.sale-cost-items.index');
        }
        // check cac rang buoc #
        $saleCostItem->deleted_by = $request->user()->id;
        $saleCostItem->save();
        $saleCostItem->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.sale-cost-items.index');
    }
}
