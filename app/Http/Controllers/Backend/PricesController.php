<?php

namespace App\Http\Controllers\Backend;

use App\User;
use App\Price;
use App\Product;
use App\PriceLog;
use App\Customer;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Import\PricesImport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PricesController extends Controller
{
    public function index(Request $request)
    {
        $query      = "1=1";
        $type       = trim($request->input('type'));
        $name       = trim($request->input('name'));
        $status     = intval($request->input('status', -1));
        $page_num   = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $time_range = explode(' - ', $request->input('date_range'));
        if (isset($time_range[0]) && isset($time_range[1])) {
            $query .= " AND updated_at >= '" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $time_range[0]))) . "' AND  updated_at <= '" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $time_range[1]))) . "'";
        }
        if($type == \App\Define\Price::TYPE_PRODUCT) {
            $productIds = "";
            if ($name) {
                $products = Product::whereRaw("name like '%" .  $name . "%' OR sku like '%" .  $name . "%'")->pluck('id')->toArray();
                if (count($products)) {
                    $productIds = implode(',', $products);
                } else {
                    $productIds = -1;
                }
            }
            $query .= " AND type='" . $type .  "'" . ($productIds ? " AND product_id IN({$productIds})" : "");
        } elseif ($type == \App\Define\Price::TYPE_CATEGORY) {
            $catIds = "";
            if ($name) {
                $cats = ProductCategory::whereRaw("name like '%" .  $name . "%'")->pluck('id')->toArray();
                if (count($cats)) {
                    $catIds = implode(',', $cats);
                } else {
                    $catIds = -1;
                }
            }
            $query .= " AND type='" . $type .  "'" . ($catIds ? " AND category_id IN({$catIds})" : "");
        } else {
            $productIds = $catIds = "";
            if ($name) {
                $products = Product::whereRaw("name like '%" .  $name . "%' OR sku like '%" .  $name . "%'")->pluck('id')->toArray();
                if (count($products)) {
                    $productIds = implode(',', $products);
                } else {
                    $productIds = -1;
                }
                $cats = ProductCategory::whereRaw("name like '%" .  $name . "%'")->pluck('id')->toArray();
                if (count($cats)) {
                    $catIds = implode(',', $cats);
                } else {
                    $catIds = -1;
                }
            }
            if ($productIds || $catIds) {
                $query .= " AND (" . ($catIds ? " category_id IN({$catIds}) OR" : "") . ($productIds ? " product_id IN({$productIds})" : " product_id = -1") . ")";
            }
        }
        if ($status <> -1) {
            $query .= " AND status = {$status}";
        }
        // dd($query);
        $prices = Price::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $types = \App\Define\Price::getTypesForOption();

        $categories = $rawCategories = [];
        $parents = ProductCategory::where('product_category_id', 0)->where('status', 1)->orderBy('position')->get();
        foreach ($parents as $parent) {
            $categories[$parent->id] = $parent->name;
            $rawCategories[$parent->id] = $parent->name;
            $children = ProductCategory::where('product_category_id', $parent->id)->where('status', 1)->orderBy('position')->get();
            foreach ($children as $child) {
                $categories[$child->id] = '|__' . $child->name;
                $rawCategories[$child->id] = $child->name;
                $tmp = ProductCategory::where('product_category_id', $child->id)->where('status', 1)->orderBy('position')->get();
                foreach ($tmp as $t) {
                    $categories[$t->id] = '|__|__' . $t->name;
                    $rawCategories[$t->id] = $t->name;
                }
            }
        }

        return view('backend.prices.index', compact('prices', 'types', 'categories', 'rawCategories'));
    }

    public function create(Request $request)
    {
        $product = [];
        if (old('product')) {
            switch (old('type')) {
                case \App\Define\Price::TYPE_PRODUCT:
                    $p = Product::find(old('product'));
                    if (!is_null($p)) {
                        $product = [$p->id => $p->sku . ' - ' . $p->name];
                    }
                    break;
                case \App\Define\Price::TYPE_CATEGORY:
                    $p = ProductCategory::find(old('product'));
                    if (!is_null($p)) {
                        $product = [$p->id => $p->name];
                    }
                    break;
            }
        }

        return view('backend.prices.create', compact('product'));
    }

    public function edit(Request $request, $id)
    {
        $price = Price::find(intval($id));
        if (is_null($price)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.prices.index');
        }

        $product = [];
        $priceIPO = "";
        switch (old('type', $price->type)) {
            case \App\Define\Price::TYPE_PRODUCT:
                $p = Product::find(old('product', $price->product_id));
                if (!is_null($p)) {
                    $product = [$p->id => $p->sku . ' - ' . $p->name];
                    $priceIPO = (is_null($p) ? "" : $p->price_ipo);
                }
                break;
            case \App\Define\Price::TYPE_CATEGORY:
                $p = ProductCategory::find(old('product', $price->category_id));
                if (!is_null($p)) {
                    $product = [$p->id => $p->name];
                }
                break;
        }
        return view('backend.prices.edit', compact('price', 'product', 'priceIPO'));
    }

    public function show(Request $request, $id)
    {
    }

    public function store(Request $request)
    {
        $request->merge(['status' => $request->input('status', 0)]);
        $data = $request->only(['type', 'status', 'product', 'dropship_percent', 'partner_percent', 'customer_percent', 'price_ipo']);
        $rules = [
            'type'      => 'required|in:' .  implode(',', \App\Define\Price::getTypes()),
            'product'   => 'required|integer',
            // 'price_ipo' => $data['type'] == \App\Define\Price::TYPE_PRODUCT ? 'required|min:0|max:99999999|integer' : 'nullable',
            // '' => $data['type'] == \App\Define\Price::TYPE_PRODUCT ? 'required|min:0|max:99999999|integer' : 'nullable',
            'price_ipo'  => ($data['type'] == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'nullable'),
            'dropship_percent'  => ($data['type'] == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'required|min:0|max:100'),
            'partner_percent'  => ($data['type'] == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'required|min:0|max:100'),
            'customer_percent' => ($data['type'] == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'required|min:0|max:100'),
        ];

        $validator = \Validator::make($data, $rules);
        $validator->setAttributeNames(trans('prices'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        // validate data
        $create = 0;
        if ($data['type'] == \App\Define\Price::TYPE_PRODUCT) {
            $product = Product::where('status', 1)->where('id', $data['product'])->first();
            if (is_null($product)) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Sản phẩm Không tìm thấy hoặc không hoạt động.');
                return back()->withErrors($errors)->withInput();
            }
            $existed = Price::where('product_id', $product->id)->first();
            if (!is_null($existed)) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Sản phẩm đã được thiết lập, vui lòng cập nhật.');
                return back()->withErrors($errors)->withInput();
            }
            if (($data['dropship_percent'] || $data['partner_percent'] || $data['customer_percent']) && (!$data['dropship_percent'] || !$data['partner_percent'] || !$data['customer_percent'])) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Giá ctv+dropship+Khách là yêu cầu khi bạn nhập 1 trong 3 ô');
                return back()->withErrors($errors)->withInput();
            }
            if ($data['price_ipo'] % 100 || ($data['dropship_percent'] && $data['dropship_percent'] % 100) || ($data['partner_percent'] && $data['partner_percent'] % 100) || ($data['customer_percent'] && $data['customer_percent'] % 100)) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Số tiền có đơn vị là trăm đồng.');
                return back()->withErrors($errors)->withInput();
            }
            if ($data['dropship_percent']) {
                if ($data['price_ipo'] < $data['customer_percent'] || $data['customer_percent'] < $data['partner_percent'] || $data['partner_percent'] < $data['dropship_percent']) {
                    $errors = new \Illuminate\Support\MessageBag;
                    $errors->add('editError', 'Thứ tự giá sẽ phải là: Giá niêm yết >= Giá cho Khách >= Giá cho CTV >= Giá Dropship');
                    return back()->withErrors($errors)->withInput();
                }
            }

            if ($product->price_ipo <> $data['price_ipo']) {
                PriceLog::create([
                    'product_id'=> $product->id,
                    'data_new'  => $data['price_ipo'],
                    'data_old'  => $product->price_ipo,
                    'note'      => "Update tại Giá sản phẩm",
                    'field'     => 'price_ipo',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
                $product->price_ipo = $data['price_ipo'];
                $product->save();
            }
            $data['product_id'] = $data['product'];
            if ($data['dropship_percent']) {
                $data['dropship_amount'] = $data['dropship_percent'];
                $data['partner_amount'] = $data['partner_percent'];
                $data['customer_amount_default'] = $data['customer_percent'];
                unset($data['dropship_percent']);
                unset($data['partner_percent']);
                unset($data['customer_percent']);
                $create = 1;
            }
            $create = 1;
        } else {
            $create = 1;
            $cat = ProductCategory::where('status', 1)->where('id', $data['product'])->first();
            if (is_null($cat)) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Danh mục Không tìm thấy hoặc không hoạt động.');
                return back()->withErrors($errors)->withInput();
            }

            if ($data['dropship_percent'] < $data['partner_percent'] || $data['partner_percent'] < $data['customer_percent']) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Thứ tự giá sẽ phải là: Giá Dropship >= Giá cho CTV >= Giá cho Khách');
                return back()->withErrors($errors)->withInput();
            }

            $existed = Price::where('category_id', $cat->id)->first();
            if (!is_null($existed)) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Danh mục đã được thiết lập, vui lòng cập nhật.');
                return back()->withErrors($errors)->withInput();
            }
            unset($data['price_ipo']);
            $data['category_id']    = $data['product'];
            $data['customer_percent_default'] = $data['customer_percent'];
            unset($data['customer_percent']);
        }
        if ($create) {
            $data['created_by']     = $request->user()->id;
            $data['source']         = \App\Define\Price::SOURCE_ADMIN;
            Price::create($data);
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.prices.index');
    }

    public function update(Request $request, $id)
    {
        $price = Price::find(intval($id));
        if (is_null($price)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.prices.index');
        }
        $request->merge(['status' => $request->input('status', 0)]);
        $data = $request->only(['status', 'dropship_percent', 'partner_percent', 'customer_percent', 'price_ipo']);
        $rules = [
            'price_ipo'         => $price->type == \App\Define\Price::TYPE_PRODUCT ? 'required|min:0|max:99999999|integer' : 'nullable',
            'dropship_percent'  => ($price->type == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'required|min:0|max:100'),
            'partner_percent'  => ($price->type == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'required|min:0|max:100'),
            'customer_percent' => ($price->type == \App\Define\Price::TYPE_PRODUCT ? 'nullable|min:0|max:99999999|integer' : 'required|min:0|max:100'),
        ];
        $validator = \Validator::make($data, $rules);
        $validator->setAttributeNames(trans('prices'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        // validate data
        $logs = [];
        if ($price->type == \App\Define\Price::TYPE_PRODUCT) {
            $product = Product::where('status', 1)->where('id', $price->product_id)->first();
            if (is_null($product)) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Sản phẩm Không tìm thấy hoặc không hoạt động.');
                return back()->withErrors($errors)->withInput();
            }
            if (($data['dropship_percent'] || $data['partner_percent'] || $data['customer_percent']) && (!$data['dropship_percent'] || !$data['partner_percent'] || !$data['customer_percent'])) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Giá ctv+dropship+Khách là yêu cầu khi bạn nhập 1 trong 3 ô');
                return back()->withErrors($errors)->withInput();
            }
            if ($data['price_ipo'] % 100 || $data['dropship_percent'] % 100 || $data['partner_percent'] % 100 || $data['customer_percent'] % 100) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Số tiền có đơn vị là trăm đồng.');
                return back()->withErrors($errors)->withInput();
            }

            if ($data['price_ipo']< $data['customer_percent'] || $data['customer_percent'] < $data['partner_percent'] || $data['partner_percent'] < $data['dropship_percent']) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Thứ tự giá sẽ phải là: Giá niêm yết >= Giá cho Khách >= Giá cho CTV >= Giá Dropship');
                return back()->withErrors($errors)->withInput();
            }

            if ($product->price_ipo <> $data['price_ipo']) {
                array_push($logs, [
                    'product_id'=> $product->id,
                    'data_new'  => $data['price_ipo'],
                    'data_old'  => $product->price_ipo,
                    'note'      => "Update tại Giá sản phẩm",
                    'field'     => 'price_ipo',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
                $product->price_ipo = $data['price_ipo'];
                $product->save();
            }
            $data['product_id']    = $data['product'];
            $data['dropship_amount'] = $data['dropship_percent'];
            unset($data['dropship_percent']);
            $data['partner_amount'] = $data['partner_percent'];
            unset($data['partner_percent']);
            $data['customer_amount_default'] = $data['customer_percent'];
            unset($data['customer_percent']);
            // add other logs
            if ($price->dropship_amount <> $data['dropship_amount']) {
                array_push($logs, [
                    'product_id'=> $product->id,
                    'data_new'  => $data['dropship_amount'],
                    'data_old'  => $price->dropship_amount,
                    'note'      => "",
                    'field'     => 'dropship_amount',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
            if ($price->partner_amount <> $data['partner_amount']) {
                array_push($logs, [
                    'product_id'=> $product->id,
                    'data_new'  => $data['partner_amount'],
                    'data_old'  => $price->partner_amount,
                    'note'      => "",
                    'field'     => 'partner_amount',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
            if ($price->customer_amount_default <> $data['customer_amount_default']) {
                array_push($logs, [
                    'product_id'=> $product->id,
                    'data_new'  => $data['customer_amount_default'],
                    'data_old'  => $price->customer_amount_default,
                    'note'      => "",
                    'field'     => 'customer_amount_default',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
            if ($price->status <> $data['status']) {
                array_push($logs, [
                    'product_id'=> $product->id,
                    'data_new'  => $data['status'],
                    'data_old'  => $price->status,
                    'note'      => "",
                    'field'     => 'status',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
        } else {
            if ($data['dropship_percent'] < $data['partner_percent'] || $data['partner_percent'] < $data['customer_percent']) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Thứ tự giá sẽ phải là: Giá Dropship >= Giá cho CTV >= Giá cho Khách');
                return back()->withErrors($errors)->withInput();
            }

            unset($data['price_ipo']);
            $data['customer_percent_default'] = $data['customer_percent'];
            unset($data['customer_percent']);
            // add other logs
            if ($price->dropship_percent <> $data['dropship_percent']) {
                array_push($logs, [
                    'category_id'=> $price->category_id,
                    'data_new'  => $data['dropship_percent'],
                    'data_old'  => $price->dropship_percent,
                    'note'      => "",
                    'field'     => 'dropship_percent',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
            if ($price->partner_percent <> $data['partner_percent']) {
                array_push($logs, [
                    'category_id'=> $price->category_id,
                    'data_new'  => $data['partner_percent'],
                    'data_old'  => $price->partner_percent,
                    'note'      => "",
                    'field'     => 'partner_percent',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
            if ($price->customer_percent_default <> $data['customer_percent_default']) {
                array_push($logs, [
                    'category_id'=> $price->category_id,
                    'data_new'  => $data['customer_percent_default'],
                    'data_old'  => $price->customer_percent_default,
                    'note'      => "",
                    'field'     => 'customer_percent_default',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
            if ($price->status <> $data['status']) {
                array_push($logs, [
                    'category_id'=> $price->category_id,
                    'data_new'  => $data['status'],
                    'data_old'  => $price->status,
                    'note'      => "",
                    'field'     => 'status',
                    'action_by' => $request->user()->id,
                    'source'    => \App\Define\Price::SOURCE_ADMIN,
                    'action_at' => date("Y-m-d H:i:s"),
                ]);
            }
        }
        unset($data['product_id']);
        unset($data['category_id']);
        $price->update($data);
        PriceLog::insert($logs);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.prices.index');
    }

    public function createBulk(Request $request)
    {
        return view('backend.prices.create_multi');
    }

    public function readBulk(Request $request)
    {
        ini_set('memory_limit', '4096M');
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $file = $request->file;
                switch ($file->getClientOriginalExtension()) {
                    case 'xlsx':
                        $data = \Excel::toArray(new PricesImport, $file);
                        
                        if ($data) $data = $data[0];
                        dd($data);
                        if (count($data) == 0 || !isset($data[0][0]) || count($data[0]) < 7) {
                            throw new \Exception("Không được sửa dòng đầu tiên của file mẫu nhập liệu", 1);
                        }
                        if (!isset($data[1][0])) {
                            throw new \Exception("File tải lên không có dữ liệu dòng đầu...", 1);
                        }
                        $response['message'] = view('backend.prices.excel', compact('data'))->render();
                        break;
                    default:
                        throw new \Exception("Không hỗ trợ định dạng", 1);
                }
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function saveBulk(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $data = $request->data;
                if (!is_array($data) && count($data) == 0) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $sku = [];
                foreach($data as $d) {
                    dd($d);
                    $q = 0;
                    if (!isset($d[6])) {
                        throw new \Exception("Dữ liệu dòng số " . ($d[0] ?? "") . " không đúng");
                    }
                    if ("" == $d[1] || $d[1] <> trim($d[1]) || strlen($d[1]) > 100) {
                        throw new \Exception("SKU dòng số " . ($d[0] ?? "") . " không đúng");
                    }
                    if ($d[2] <> intval($d[2])) {// || $d[2] < 1 || $d[2] > 99999999
                        throw new \Exception("Giá niêm yết dòng số " . ($d[0] ?? "") . " không đúng");
                    }
                    if ($d[2]%100) {
                        throw new \Exception("Giá niêm yết dòng số " . ($d[0] ?? "") . " phải tròn trăm");
                    }
                    if ($d[2] == 0 && ($d[3] || $d[4] || $d[5])) {
                        throw new \Exception("Giá niêm yết dòng số " . ($d[0] ?? "") . " bằng 0 thì các giá khác cũng phải bằng 0");
                    }

                    if ($d[3] || $d[4] || $d[5]) {
                        if (!($d[3] && $d[4] && $d[5])) {
                            throw new \Exception("Giá Dropship, CTV, Khách yêu cầu đủ hoặc trống tất cả tại dòng số " . ($d[0] ?? "") . "");
                        }
                        if ($d[3] <> intval($d[3]) || $d[3] < 1 || $d[3] > 99999999) {
                            throw new \Exception("Giá Dropship dòng số " . ($d[0] ?? "") . " không đúng");
                        }
                        if ($d[3]%100) {
                            throw new \Exception("Giá Dropship dòng số " . ($d[0] ?? "") . " phải tròn trăm");
                        }

                        if ($d[4] <> intval($d[4]) || $d[4] < 1 || $d[4] > 99999999) {
                            throw new \Exception("Giá cho CTV dòng số " . ($d[0] ?? "") . " không đúng");
                        }
                        if ($d[4]%100) {
                            throw new \Exception("Giá cho CTV dòng số " . ($d[0] ?? "") . " phải tròn trăm");
                        }

                        if ($d[5] <> intval($d[5]) || $d[5] < 1 || $d[5] > 99999999) {
                            throw new \Exception("Giá cho Khách dòng số " . ($d[0] ?? "") . " không đúng");
                        }
                        if ($d[5]%100) {
                            throw new \Exception("Giá cho Khách dòng số " . ($d[0] ?? "") . " phải tròn trăm");
                        }
                        if ($d[6] <> trim($d[6]) || strlen($d[6]) > 255) {
                            throw new \Exception("Ghi chú dòng số " . ($d[0] ?? "") . " không đúng");
                        }

                        if ($d[2] < $d[5] || $d[5] < $d[4] || $d[4] < $d[3]) {
                            throw new \Exception("Thứ tự giá sẽ phải là: Giá niêm yết >= Giá cho Khách >= Giá cho CTV >= Giá Dropship dòng số " . ($d[0] ?? ""));
                        }
                    }
                    array_push($sku, $d[1]);
                }
                $products = Product::whereIn('sku', ($sku))->get()->keyBy('sku');//where('status', 1)->
                if (count($sku) <> count($products)) {
                    foreach ($sku as $sk) {
                        if (!isset($products[$sk])) {
                            throw new \Exception("Sản phẩm có SKU: `" . $sk . "` không tìm thấy hoặc đang dừng đăng bán");
                        }
                    }
                }
                $logs = [];
                foreach($data as $d) {
                    $product = $products[$d[1]];
                    $price = Price::where('product_id', $product->id)->where('type', \App\Define\Price::TYPE_PRODUCT)->first();
                    if (is_null($price)) {
                        // create new
                        Price::create([
                            'product_id'        => $product->id,
                            'type'              => \App\Define\Price::TYPE_PRODUCT,
                            'dropship_amount'   => $d[3],
                            'partner_amount'    => $d[4],
                            'customer_amount_default'   => $d[5],
                            'note'          => $d[6],
                            'created_by'    => $request->user()->id,
                            'source'    => \App\Define\Price::SOURCE_ADMIN,
                            'status'    => 1,
                        ]);
                    } elseif ($price->dropship_amount <> $d[3] || $price->partner_amount <> $d[4] || $price->customer_amount_default <> $d[5]) {
                        // add changed log
                        if ($price->dropship_amount <> $d[3]) {
                            array_push($logs, [
                                'product_id'=> $product->id,
                                'data_new'  => $d[3],
                                'data_old'  => $price->dropship_amount,
                                'note'      => "Excel",
                                'field'     => 'dropship_amount',
                                'action_by' => $request->user()->id,
                                'source'    => \App\Define\Price::SOURCE_ADMIN,
                                'action_at' => date("Y-m-d H:i:s"),
                            ]);
                        }
                        if ($price->partner_amount <> $d[4]) {
                            array_push($logs, [
                                'product_id'=> $product->id,
                                'data_new'  => $d[4],
                                'data_old'  => $price->partner_amount,
                                'note'      => "Excel",
                                'field'     => 'partner_amount',
                                'action_by' => $request->user()->id,
                                'source'    => \App\Define\Price::SOURCE_ADMIN,
                                'action_at' => date("Y-m-d H:i:s"),
                            ]);
                        }
                        if ($price->customer_amount_default <> $d[5]) {
                            array_push($logs, [
                                'product_id'=> $product->id,
                                'data_new'  => $d[5],
                                'data_old'  => $price->customer_amount_default,
                                'note'      => "Excel",
                                'field'     => 'customer_amount_default',
                                'action_by' => $request->user()->id,
                                'source'    => \App\Define\Price::SOURCE_ADMIN,
                                'action_at' => date("Y-m-d H:i:s"),
                            ]);
                        }
                        $price->update([
                            'dropship_amount'           => $d[3],
                            'partner_amount'            => $d[4],
                            'customer_amount_default'   => $d[5],
                            'note'                      => $d[6],
                        ]);
                    } else {
                        // nothing changed about price
                    }

                    if ($product->price_ipo <> $d[2]) {
                        array_push($logs, [
                            'product_id'=> $product->id,
                            'data_new'  => $d[2],
                            'data_old'  => $product->price_ipo,
                            'note'      => "Excel",
                            'field'     => 'price_ipo',
                            'action_by' => $request->user()->id,
                            'source'    => \App\Define\Price::SOURCE_ADMIN,
                            'action_at' => date("Y-m-d H:i:s"),
                        ]);
                        // update current ipo
                        $product->price_ipo = $d[2];
                        $product->save();
                    }
                }
                PriceLog::insert($logs);
                $response['message'] = trans('system.success');
                Session::flash('message', $response['message']);
                Session::flash('alert-class', 'success');

            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function download(Request $request)
    {
        $file= public_path() . "/assets/media/files/templates/price-dropship-ctv-sample.xlsx";
        $headers = [
            'Content-Type: application/xls',
        ];
        return response()->download($file, 'template-price.xlsx', $headers);
    }

    public function timeline(Request $request)
    {
        $response = ['message' => trans('system.have_an_error'), 'data' => ""];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $price = Price::find(intval($request->id));
                if(is_null($price)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'], 1);
                }
                $category = $product = null;
                if ($price->type == \App\Define\Price::TYPE_PRODUCT) {
                    $product = $price->product()->first();
                    if(is_null($product)) {
                        $statusCode = 400;
                        throw new \Exception($response['message'], 1);
                    }
                    $timelines = PriceLog::where('product_id', $product->id)->orderBy('id', 'DESC')->get();
                } else {
                    $category = $price->category()->first();
                    if(is_null($category)) {
                        $statusCode = 400;
                        throw new \Exception($response['message'], 1);
                    }
                    $timelines = PriceLog::where('category_id', $category->id)->orderBy('id', 'DESC')->get();
                }
                $response['message'] = trans('system.success');
                $users = User::where('activated', 1)->pluck('fullname', 'id')->toArray();
                $partners = Customer::pluck('fullname', 'id')->toArray();
                $response['data'] = view('backend.prices.ajaxTimeline', compact('price', 'timelines', 'users', 'product', 'category', 'partners'))->render();
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function destroy(Request $request, $id)
    {
        $price = Price::find(intval($id));
        if (is_null($price)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.prices.index');
        }
        $price->deleted_by = $request->user()->id;
        $price->save();
        $price->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.prices.index');
    }
}
