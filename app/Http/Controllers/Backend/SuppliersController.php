<?php

namespace App\Http\Controllers\Backend;

use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SuppliersController extends Controller
{
    public function index(Request $request)
    {
        $query          = "1=1";
        $status         = intval($request->input('status', -1));
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $name           = $request->input('name');
        if($name) $query .= " AND (name like '%" . $name . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        $suppliers = Supplier::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);

        return view('backend.suppliers.index', compact('suppliers'));
    }

    public function create(Request $request)
    {
        return view('backend.suppliers.create');
    }

    public function show(Request $request, $id)
    {
        $supplier = Supplier::find(intval($id));
        if (is_null($supplier)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.suppliers.index');
        }
        return view('backend.suppliers.show', compact('supplier'));
    }

    public function edit(Request $request, $id)
    {
        $supplier = Supplier::find(intval($id));
        if (is_null($supplier)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.suppliers.index');
        }
        return view('backend.suppliers.edit', compact('supplier'));
    }

    public function store(Request $request)
    {
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), Supplier::rules());
        $validator->setAttributeNames(trans('suppliers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        Supplier::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.suppliers.index');
    }

    public function update(Request $request, $id)
    {
        $supplier = Supplier::find(intval($id));
        if (is_null($supplier)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.suppliers.index');
        }

        $request->merge(['status' => $request->input('status', 0)]);
        $validator = \Validator::make($data = $request->only(['status', 'name', 'email', 'phone', 'description', 'address']), Supplier::rules(intval($id)));
        $validator->setAttributeNames(trans('suppliers'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $supplier->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.suppliers.index');
    }

    public function destroy(Request $request, $id)
    {
        $supplier = Supplier::find(intval($id));
        if (is_null($supplier)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.suppliers.index');
        }

        if ($supplier->containers()->count()) {
            Session::flash('message', "Người thuê CONT đã có đơn hàng.");
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.suppliers.index');
        }
        $supplier->deleted_by = $request->user()->id;
        $supplier->save();
        $supplier->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.suppliers.index');
    }
}
