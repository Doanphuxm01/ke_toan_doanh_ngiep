<?php

namespace App\Http\Controllers\Backend;

use App\Province;
use App\InventoryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class InventoryCategoriesController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $status         = intval($request->input('status', -1));
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $province       = intval($request->input('province', -1));
        if($province <> -1) $query .= " AND province_id = {$province}";
        if($status <> -1) $query .= " AND status = {$status}";

        $inventoryCategories = InventoryCategory::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $provinces = Province::where('status', 1)->pluck('name', 'id')->toArray();
        return view('backend.inventory-categories.index', compact('inventoryCategories', 'provinces'));
    }

    public function create(Request $request)
    {
        $provinces = Province::where('status', 1)->pluck('name', 'id')->toArray();
        $paper = intval(old('paper'));
        if ($paper) {
            $paper = Paper::where("status", 1)->where("id", $paper)->first();
        }
        return view('backend.inventory-categories.create', compact('provinces', 'paper'));
    }

    public function show(Request $request, $id)
    {
        $customerPayment = CustomerPayment::find(intval($id));
        if (is_null($customerPayment)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $customer = Customer::where('id', intval($customerPayment->customer_id))->first();
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        return view('backend.customer-payments.show', compact('customerPayment', 'customer'));
    }

    public function store(Request $request)
    {
        $request->merge(['status' => $request->input('status', 0)]);
        $validator = \Validator::make($data = $request->all(), InventoryCategory::rules());
        $validator->setAttributeNames(trans('inventory_categories'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['province_id']    = $data['province'];
        $data['created_by']     = $request->user()->id;
        InventoryCategory::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.inventory-categories.index');
    }

    public function edit(Request $request, $id)
    {
        $inventoryCategory = InventoryCategory::find(intval($id));
        if (is_null($inventoryCategory)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $provinces = Province::where('status', 1)->orWhere('id', $inventoryCategory->province_id)->pluck('name', 'id')->toArray();
        return view('backend.inventory-categories.edit', compact('inventoryCategory', 'provinces'));
    }

    public function update(Request $request, $id)
    {
        $inventoryCategory = InventoryCategory::find(intval($id));
        if (is_null($inventoryCategory)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $request->merge(['status' => $request->input('status', 0)]);
        $validator = \Validator::make($data = $request->all(), InventoryCategory::rules($inventoryCategory->id));
        $validator->setAttributeNames(trans('inventory_categories'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['province_id']    = $data['province'];
        $inventoryCategory->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.inventory-categories.index');
    }

    public function destroy(Request $request, $id)
    {
        $inventoryCategory = InventoryCategory::find(intval($id));
        if (is_null($inventoryCategory)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        if ($inventoryCategory->inventories()->count()) {
            Session::flash('message', "Kho hàng cần loại bỏ hết tồn kho.");
            Session::flash('alert-class', 'danger');
            return back();
        }
        $inventoryCategory->deleted_by = $request->user()->id;
        $inventoryCategory->save();
        $inventoryCategory->delete();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.inventory-categories.index');
    }
}
