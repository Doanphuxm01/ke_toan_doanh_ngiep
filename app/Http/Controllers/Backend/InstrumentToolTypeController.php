<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\CostItem;
use App\Models\FixedAssetAllocationInformation;
use App\Models\InstrumentTool;
use App\Models\InstrumentToolType;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class InstrumentToolTypeController 
{
    public function index(Request $request){
        $query = "1=1"; 
        $page_num = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
        $code = $request->input('code');
        $status = intval($request->input('status', -1));
        if($status <> -1) $query .= " AND status = {$status}";
        if($code) $query .= " AND (code like '%" . $code . "%' )";
        $types = InstrumentToolType::whereRaw($query)->where('company_id', session('current_company'))->orderBy('id', 'DESC')->paginate($page_num);
        return view('backend.instrument-tool-types.index', compact('types'));
    }
    public function show(Request $request, $id)
    {
        $type = InstrumentToolType::find(intval($id));
        if (is_null($type)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.cost-items.index');
        }
        return view('backend.instrument-tool-types.show', compact('type'));
    }
    public function create(){
        return view('backend.instrument-tool-types.create');
    }
    public function store(Request $request){
        $validator = Validator::make($data = $request->all(), InstrumentToolType::rules());
        $validator->setAttributeNames(trans('instrument_tool_types'));
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        $request->merge(['status' => intval($request->input('status', 0))]);
        $data['created_by']   = session('created_by');
        $data['company_id'] = session('current_company');
        $data['code'] = $request->type_code;
        $data['name'] = $request->type_name;
        InstrumentToolType::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.instrument-tool-types.index');
    }
    public function edit(Request $request, $id){
        $type = InstrumentToolType::find(intval($id));
        if (is_null($type)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.instrument-tool-types.index');
        }
        return view('backend.instrument-tool-types.edit', compact('type'));
    }
    public function update(Request $request, $id)
    {
        $type = InstrumentToolType::find(intval($id));
        $request->merge(['status' => $request->input('status', 0)]);
        if (is_null($type)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $validator = \Validator::make($data = $request->all(), InstrumentToolType::rules(intval($id)));
        $validator->setAttributeNames(trans('instrument_tool_types'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['code'] = $request->type_code;
        $data['name'] = $request->type_name;
        $type->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.instrument-tool-types.index');
    }
    public function destroy(Request $request, $id)
    {
        $type = InstrumentToolType::where('id', intval($id))->first();
        $check = InstrumentTool::where('type_id', intval($id))->get();
        if (count($check) > 0) {
            Session::flash('message', trans('instrument_tool_types.in_use'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $type->delete();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
}
