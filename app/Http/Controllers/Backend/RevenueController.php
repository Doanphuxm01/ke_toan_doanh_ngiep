<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RevenueController extends Controller
{
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this,$action)) {
            return $this->$action();
        } else {
            return $this->list();
        }
    }

    public function list(){
        $title = 'Quản Trị Doanh Thu Kế Toán';

    }
}
