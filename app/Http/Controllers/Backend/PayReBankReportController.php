<?php

namespace App\Http\Controllers\Backend;

use App\Models\PaymentReceiptVoucher;

use Illuminate\Http\Request;
use App\Exports\PayReBankExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class PayReBankReportController extends Controller
{

    public function index(Request $request)
    {
        
        return view('backend.payment-receipt-vouchers.bank.index-report', compact('departments', 'fixed_assets'));
    }
    public function exportExcel(){
        return Excel::download(new FixedAssetExport, 'demo.xlsx'); 
    }

}