<?php

namespace App\Http\Controllers\Backend;

use App\Bank;
use App\Gateway;
use App\BankAmount;
use App\Models\BankAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BanksController extends Controller
{
	public function index(Request $request)
	{
        $query = '1=1';
        $name = $request->input('name');
        $status = $request->input('status', -1);
        if( $name ) $query .= " AND full_name like '%" . $name . "%'";
        if($status <> -1) $query .= " AND status = {$status}";
        $banks = Bank::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate(\App\Define\Constant::PAGE_NUM_20);
		return view('backend.banks.index', compact('banks'));
	}

	public function create(Request $request)
	{
        return view('backend.banks.create');
	}

	public function store(Request $request)
	{
        $request->merge(['status' => $request->input('status', 0)]);
		$validator = \Validator::make($data = $request->all(), Bank::rules());
		$validator->setAttributeNames(trans('banks'));
		if ($validator->fails()) return back()->withErrors($validator)->withInput();
        Bank::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.banks.index');
	}

	public function show(Request $request, $id)
	{
        $bank = Bank::find(intval($id));
        if ( is_null( $bank ) ) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }

        $gateways   = Gateway::where('status', 1)->pluck('name', 'id')->toArray();
        $types      = \App\Define\Bank::getSelectTypes();
        $users      = \App\Define\Bank::getSelectUsers();

        return view('backend.banks.show', compact( 'bank', 'gateways', 'types', 'users' ) );
	}

	public function edit(Request $request, $id)
	{  
        $editBank = Bank::find(intval($id));
        if ( is_null( $editBank ) ) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
		return view('backend.banks.edit', compact( 'editBank' ) );
	}

    public function update(Request $request, $id)
	{
        $editBank = Bank::find(intval($id));
        if (is_null($editBank)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.partners.index');
        }
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), Bank::rules());
        $validator->setAttributeNames(trans('banks'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $editBank->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.banks.index');
	}

    public function destroy(Request $request, $id)
	{
        $bank = Bank::find($id);
        if (is_null($bank)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        if(count(BankAccount::where('bank_id', $id))>0)
        {
            Session::flash('message', trans('can not delete because still number account'));
            Session::flash('alert-class', 'danger');
            return back();
        }
     
        $bank->delete();
		Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

		return redirect()->route('admin.banks.index');
	}

    public function indexAccount(Request $request)
    {
        $query = '1=1';
        $name = $request->input('name');
        $status = $request->input('status', -1);
        if( $name ) $query .= " AND full_name_bank like '%" . $name . "%'";
        if($status <> -1) $query .= " AND status = {$status}";
        $banks = BankAccount::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate(\App\Define\Constant::PAGE_NUM_20);
		return view('backend.banks.bank-accounts.index', compact('banks'));
    }

    public function createAccount()
    {
        $bank = Bank::all()->pluck('full_name', 'id')->toArray();
        return view('backend.banks.bank-accounts.create', compact('bank'));
    }

    public function storeAccount(Request $request)
    {  
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), BankAccount::rules());
        $validator->setAttributeNames(trans('banks'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $data['bank_id'] = $request->full_name;
        $data['full_name_bank'] = Bank::find($request->full_name)->full_name;
        BankAccount::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.banks.index-accounts');
    }

    public function editAccount(Request $request, $id)
    {
        $editBank = BankAccount::find(intval($id));
        $bank = Bank::all()->pluck('full_name', 'id')->toArray();
        if ( is_null( $editBank ) ) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
		return view('backend.banks.bank-accounts.edit', compact( 'editBank', 'bank') );
    }
    
    public function updateAccount(Request $request, $id)
    {
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), BankAccount::rules());
        $validator->setAttributeNames(trans('banks'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $editBank = BankAccount::find(intval($id));
        // dd($editBank);
        if (is_null($editBank)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.partners.index');
        }
        $data['bank_id'] = $request->full_name;
        $data['full_name_bank'] = Bank::find($request->full_name)->full_name;
        // dd($data);
        $editBank->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.banks.index-accounts');
    }

    public function destroyAccount(Request $request, $id)
    {
        $deleteAccountBank = BankAccount::find($id);
        if (is_null($deleteAccountBank)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        // if(count(BankAccount::where('bank_id', $id))>0)
        // {
        //     Session::flash('message', trans('can not delete because still number account'));
        //     Session::flash('alert-class', 'danger');
        //     return back();
        // }
     
        $deleteAccountBank->delete();
		Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

		return redirect()->route('admin.banks.index-accounts');
    }
	
    public function getInfoBankAccount(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $account_number = trim($request->input('payment_account'));
                $bankAccount = BankAccount::whereRaw("status=1 OR account_number like '%" . $payment_account . "%' OR full_name_bank like '%" . $payment_account . "%' ")->selectRaw(" id, account_number as text, full_name_bank as bank")->paginate(5)->toArray();
                $response['message'] = trans('system.success');
                $statusCode = 200;
                $response['bankAccount'] = $bankAccount;
            } catch (\Exception $e) {
                if ($statusCode == 400) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
  
}