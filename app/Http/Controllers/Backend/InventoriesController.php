<?php

namespace App\Http\Controllers\Backend;

use App\User;
use App\Product;
use App\Province;
use App\Inventory;
use App\InventoryLog;
use App\InventoryCategory;
use Illuminate\Http\Request;
use App\InventoryCategoryProduct;
use App\Import\InventoriesImport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class InventoriesController extends Controller
{

    public function index(Request $request)
    {
        $query      = "1=1";
        $type       = trim($request->input('type'));
        $page_num   = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $productId  = intval($request->input('product', -1));
        $province   = intval($request->input('province', -1));
        $category   = intval($request->input('category', -1));
        $time_range = explode(' - ', $request->input('date_range'));
        if($province <> -1) $query .= " AND province_id = {$province}";
        if($category <> -1) $query .= " AND inventory_category_id = {$category}";
        if (isset($time_range[0]) && isset($time_range[1])) {
            $query .= " AND updated_at >= '" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $time_range[0]))) . "' AND  updated_at <= '" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $time_range[1]))) . "'";
        }
        if($type) $query .= " AND type = '{$type}'";
        $product = [];
        if ($productId <> -1) {
            $p = Product::where("status", 1)->where("id", $productId)->first();
            if (!is_null($p)) {
                if ($p->parent_id) {
                    $parent = Product::where('status', 1)->where('id', $p->parent_id)->first();
                    if (!is_null($parent)) {
                        $product[$p->id] = $p->sku . ' - ' . $parent->name;
                        $query .= " AND product_id = " . $p->id;
                    }
                } else {
                    $product[$p->id] = $p->sku . ' - ' . $p->name;
                    $query .= " AND product_id = " . $p->id;
                }
            }
        }
        $inventories = Inventory::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $types = \App\Define\Inventory::getTypesForOption();
        $inventoryCategories = InventoryCategory::pluck('code', 'id')->toArray();
        $filterInventoryCategories = InventoryCategory::where('status', 1)->selectRaw("id, CONCAT(`code`, ' - ', `name`) as name")->pluck('name', 'id')->toArray();
        $provinces = Province::where('status', 1)->pluck('name', 'id')->toArray();
        return view('backend.inventories.index', compact('inventories', 'product', 'types', 'provinces', 'inventoryCategories', 'filterInventoryCategories'));
    }

    public function create(Request $request)
    {
        $product = [];
        $productId = intval(old('product'));
        if ($productId) {
            $p = Product::where("status", 1)->where("id", $productId)->first();
            if (!is_null($p)) {
                if ($p->parent_id) {
                    $parent = Product::where('status', 1)->where('id', $p->parent_id)->first();
                    if (!is_null($parent)) {
                        $product[$p->id] = $p->sku . ' - ' . $parent->name;
                    }
                } else {
                    $product[$p->id] = $p->sku . ' - ' . $p->name;
                }
            }
        }
        $types = \App\Define\Inventory::getTypesForOption();
        $categories = InventoryCategory::where('status', 1)->selectRaw("id, CONCAT(`name`, ' - ', `code`) as name")->pluck('name', 'id')->toArray();

        return view('backend.inventories.create', compact('types', 'product', 'categories'));
    }

    public function show(Request $request, $id)
    {
    }

    public function store(Request $request)
    {
        $rules = [
            'type'      => 'required|in:' . implode(',', \App\Define\Inventory::getTypes()),//("'" . implode("','", \App\Define\DataNumber::getStatusForAfterService()) . "'")
            'product'   => 'required|integer',
            'quantity'  => 'required|integer|min:1|max:99999',
            'note'      => 'max:255',
            'category'  => 'required',
        ];

        $validator = \Validator::make($data = $request->all(), $rules);
        $validator->setAttributeNames(trans('inventories'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        // find warehouse
        $category = InventoryCategory::where('status', 1)->where('id', intval($data['category']))->first();
        if (is_null($category)) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', trans('system.have_and_error'));
            return back()->withErrors($errors)->withInput();
        }
        $p = Product::where("status", 1)->where("id", $data['product'])->first();
        if (is_null($p)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'error');
            return back()->withInput();
        }
        $inventory = Inventory::where('product_id', $p->id)->where('inventory_category_id', $category->id)->first();
        //. if () {
        //     $errors = new \Illuminate\Support\MessageBag;
        //     $errors->add('editError', 'Kho ' . $category->name . ' đã có tồn kho cho SKU này, vui lòng cập nhập tồn kho.');
        //     return back()->withErrors($errors)->withInput();
        // }
        // tim kho hien tai con bao nhieu
        $old = is_null($inventory) ? 0 : $inventory->quantity;
        $newQuantity = 0;
        if ($data['type'] == \App\Define\Inventory::TYPE_OUT_STOCK) {
            if ($old < $data['quantity']) {
                Session::flash('message', "Số lượng trong kho chỉ còn " . $old . " sản phẩm");
                Session::flash('alert-class', 'error');
                return back()->withInput();
            }
            $newQuantity = $old - $data['quantity'];
        } else {
            $newQuantity = $old + $data['quantity'];
        }
        if (is_null($inventory)) {
            $data['inventory_category_id'] = $category->id;
            $data['province_id']    = $category->province_id;
            $data['product_id']     = $p->id;
            $data['created_by']     = $request->user()->id;
            $data['quantity']       = $newQuantity;
            $inventory = Inventory::create($data);
        } else {
            $inventory->quantity = $newQuantity;
            $inventory->save();
        }
        // update to log
        $logs = [];
        array_push($logs, [
            'data_new'  => $newQuantity,
            'data_old'  => $old,
            'note'      => trans("inventories.types." . $data['type']) . ' ' . $data['quantity'] . ' sản phẩm' . " " . $data['note'],
            'field'     => 'quantity',
            'action_by' => $request->user()->id,
            'action_at' => date("Y-m-d H:i:s"),
            'inventory_id'  => $inventory->id,
            'product_id'    => $p->id,
        ]);
        InventoryLog::insert($logs);
        // update current for product
        $p->quantity = ($p->quantity - $old + $newQuantity);
        $p->save();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.inventories.index');
    }

    public function createBulk(Request $request)
    {
        return view('backend.inventories.create_multi');
    }

    public function readBulk(Request $request)
    {
        ini_set('memory_limit', '4096M');
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $file = $request->file;
                switch ($file->getClientOriginalExtension()) {
                    case 'xlsx':
                        $data = \Excel::toArray(new InventoriesImport, $file);
                        if ($data) $data = $data[0];
                        if (count($data) == 0 || !isset($data[0][0]) || count($data[0]) < 5) {
                            throw new \Exception("Không được sửa dòng đầu tiên của file mẫu nhập liệu", 1);
                        }
                        if (!isset($data[1][0])) {
                            throw new \Exception("File tải lên không có dữ liệu", 1);
                        }
                        $response['message'] = view('backend.inventories.excel', compact('data'))->render();
                        break;
                    default:
                        throw new \Exception("Không hỗ trợ định dạng", 1);
                }
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function saveBulk(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $data = $request->data;
                $cats = $request->categories;
                if ((!is_array($data) && count($data) == 0) || !is_array($cats) || count($data[0]) <> count($cats) || count($cats) < 4 || count($cats)%2 <> 0) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                // cac kho theo thuc tu
                $categories = [];
                // lay cac kho trong he thong
                $inventoryCategories = InventoryCategory::where('status', 1)->select('province_id', 'id', 'code')->get()->keyBy('code')->toArray();
                for($i = 2; $i < count($cats); $i=$i+2) {
                    $t = explode('_', $cats[$i]);
                    if (!isset($inventoryCategories[$t[0]])) {
                        throw new \Exception("Không tìm thấy kho " . $t[0] . "", 1);
                    }
                    $categories[$i] = $inventoryCategories[$t[0]]['id'];
                }
                $inventoryCategories = array_column($inventoryCategories, 'province_id', 'id');
                $sku = []; // lay ma sku phu thuoc cat & so luong
                foreach($data as $d) {
                    if (!array_key_exists(2, $d)) {
                        throw new \Exception("Dữ liệu dòng số " . ($d[0] ?? "") . " không đúng");
                    }
                    if ("" == $d[1] || $d[1] <> trim($d[1]) || strlen($d[1]) > 100) {
                        throw new \Exception("SKU dòng số " . ($d[0] ?? "") . " không đúng");
                    }
                    $haveData = false;
                    for ($i=2; $i < count($d); $i=$i+2) {
                        if (intval($d[$i]) == $d[$i]) {
                            $d[$i] = intval($d[$i]);
                            if (!$haveData && $d[$i] <> 0) {
                                $haveData = true;
                            }
                            if ($d[$i] < -9999 || $d[$i] > 9999) throw new \Exception("Số lượng dòng số " . ($d[0] ?? "") . " không đúng");
                        }
                        if (isset($d[$i+1]) && strlen($d[$i+1]) > 255) throw new \Exception("Ghi chú dòng số " . ($d[0] ?? "") . " không đúng");
                        if (isset($sku[$d[1]][$i])) {
                            $sku[$d[1]][$categories[$i]] += $d[$i];
                        } else {
                            $sku[$d[1]][$categories[$i]] = $d[$i];
                        }
                    }
                    if (!$haveData) throw new \Exception("Dòng số " . ($d[0] ?? "") . " không có dữ liệu");
                }
                // lay cac sp
                $tmp = $products = Product::whereIn('sku', array_keys($sku))->where('status', 1)->get()->keyBy('sku');
                if (count($sku) <> count($products)) {
                    foreach ($sku as $sk => $d) {
                        if (!isset($products[$sk])) {
                            throw new \Exception("Sản phẩm có SKU: `" . $sk . "` không tìm thấy hoặc đang dừng đăng bán");
                        }
                    }
                }
                // bang map data giua sku va product id
                $map = array_column($tmp->toArray(), 'id', 'sku');
                // lay cac kho
                $inventories = Inventory::whereIn('product_id', array_values($map))->get()->keyBy('id');
                // check xem sau khi trừ đi có bị âm ko?
                foreach ($sku as $sk => $d) {
                    foreach ($d as $catId => $changeQty) {
                        // su dung collection cua laravel
                        $inv = $inventories->where('product_id', $map[$sk])->where('inventory_category_id', $catId)->first();
                        if (is_null($inv)) { // them moi
                            if ($changeQty < 0) {
                                throw new \Exception("Sản phẩm có SKU `" . $sk . "` sau khi cập nhật sẽ bị Âm tồn kho. Vui lòng kiểm tra lại");
                            }
                        } else {
                            if ($inv->quantity + $changeQty < 0) {
                                throw new \Exception("Sản phẩm có SKU `" . $sk . "` sau khi cập nhật sẽ bị Âm tồn kho. Vui lòng kiểm tra lại");
                            }
                        }
                    }
                }
                // moi thu pass => update hoac insert inventory, log, update so luong cho product
                $logs = [];
                $indexInvNeedUpdate = [];
                $indexProductNeedUpdate = [];
                foreach($data as $d) {
                    for ($i=2; $i < count($d); $i=$i+2) {
                        if (intval($d[$i]) == $d[$i]) {
                            $d[$i] = intval($d[$i]);
                            if ($d[$i] == 0) continue;
                            $pId = $map[$d[1]];
                            // tim inv cho dong nay
                            $inv = $inventories->where('product_id', $pId)->where('inventory_category_id', $categories[$i])->first();
                            $old = 0;
                            $new = 0;
                            if (is_null($inv)) {
                                $inv = Inventory::create([
                                    'product_id'    => $pId,
                                    // 'type'          => ($d[$i]>0 ? \App\Define\Inventory::TYPE_IN_STOCK : \App\Define\Inventory::TYPE_OUT_STOCK),
                                    'quantity'      => $d[$i],
                                    'note'          => $d[$i+1] ?? "",
                                    'created_by'    => $request->user()->id,
                                    'inventory_category_id' => $categories[$i],
                                    'province_id'   => $inventoryCategories[$categories[$i]] ?? null,
                                ]);
                                // day vao kho de
                                $inventories[$inv->id] = $inv;
                                $new = $d[$i];
                            } else {
                                $old = $inventories[$inv->id]->quantity;
                                $inventories[$inv->id]->quantity += $d[$i]; // update sau, tang hieu nang cho ung dung
                                $new = $inventories[$inv->id]->quantity;
                                $indexInvNeedUpdate[$inv->id] = $inv->id; // dua vao hang doi can update
                            }
                            // thay doi so luong cho sp
                            if (isset($indexProductNeedUpdate[$d[1]])) {
                                $indexProductNeedUpdate[$d[1]] += $d[$i];
                            } else {
                                $indexProductNeedUpdate[$d[1]] = $d[$i];
                            }

                            // tao log
                            array_push($logs, [
                                'inventory_id'  => $inv->id,
                                'product_id'    => $inv->product_id,
                                'data_new'  => $new,
                                'data_old'  => $old,
                                'note'      => "(Excel) " . trans('inventories.types.' . ($d[$i]>0 ? \App\Define\Inventory::TYPE_IN_STOCK : \App\Define\Inventory::TYPE_OUT_STOCK)) . " " . abs($d[$i]) . ' sản phẩm.' . (isset($d[$i+1]) ? (" " . $d[$i+1]) : ""),
                                'field'     => 'quantity',
                                'action_by' => $request->user()->id,
                                'action_at' => date("Y-m-d H:i:s"),
                            ]);
                        }
                    }
                }
                // update cac kho
                foreach ($indexInvNeedUpdate as $index) {
                    $inventories[$index]->save();
                }
                // update so luong tong sp
                foreach ($indexProductNeedUpdate as $index => $qty) {
                    $products[$index]->quantity += $qty;
                    $products[$index]->save();
                }

                InventoryLog::insert($logs);

                $response['message'] = trans('system.success');
                Session::flash('message', $response['message']);
                Session::flash('alert-class', 'success');

            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function download(Request $request)
    {
        $categories = InventoryCategory::where('status', 1)->pluck('code')->toArray();
        return \Excel::download(new \App\Exports\InventoriesExport($categories), 'Sube_Nhap_Kho_' . date('H.m.s_d.m.Y') . '.xlsx');
    }

    public function timeline(Request $request)
    {
        $response = ['message' => trans('system.have_an_error'), 'data' => ""];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $inventory = Inventory::find(intval($request->id));
                if(is_null($inventory)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'], 1);
                }
                $product = $inventory->product()->first();
                if(is_null($product)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'], 1);
                }
                $response['message'] = trans('system.success');
                $timelines = InventoryLog::where('inventory_id', $inventory->id)->orderBy('id', 'DESC')->get();
                $users = User::where('activated', 1)->pluck('fullname', 'id')->toArray();

                $response['data'] = view('backend.inventories.ajaxTimeline', compact('inventory', 'timelines', 'users', 'product'))->render();
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function info(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $inventory = Inventory::find(intval($request->id));
                if(is_null($inventory)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . '1', 1);
                }
                $product = $inventory->product()->first();
                if(is_null($product)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . '2', 1);
                }
                $response['message'] = trans('system.success');
                $timelines = InventoryLog::where('product_id', $product->id)->orderBy('id', 'DESC')->get();
                $users = User::where('activated', 1)->pluck('fullname', 'id')->toArray();
                $product = [];
                $p = $inventory->product()->first();
                if (is_null($p)) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm thấy sản phẩm", 1);
                }
                if ($p->parent_id) {
                    $parent = Product::where('status', 1)->where('id', $p->parent_id)->first();
                    if (!is_null($parent)) {
                        $product['id'] = $p->id;
                        $product['quantity'] = $p->quantity;
                        $product['name'] = $p->sku . ' - ' . $parent->name;
                    }
                } else {
                    $product['id'] = $p->id;
                    $product['quantity'] = $p->quantity;
                    $product['name'] = $p->sku . ' - ' . $p->name;
                }
                if (empty($product)) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm thấy sản phẩm", 1);
                }

                $category = $inventory->category()->first();
                if (is_null($category)) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm thấy Kho hàng", 1);
                }
                $response['data']  = view('backend.inventories.ajaxInventory', compact('category', 'inventory', 'users', 'product', 'timelines'))->render();
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function saveInventory(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $inventory = Inventory::find(intval($request->id));
                if (is_null($inventory)) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $product = Product::where("status", 1)->where("id", $inventory->product_id)->first();
                if (is_null($product)) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $data = $request->items;
                if (!is_array($data)) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                if (!isset($data['type']) || !isset($data['quantity']) || !isset($data['note']) || count($data['type']) <> count($data['quantity']) || count($data['type']) <> count($data['note'])) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $rules = [
                    'type'      => 'required|array',
                    'type.*'    => 'required|in:' . implode(',', \App\Define\Inventory::getTypes()),
                    'quantity'  => 'required|array',
                    'quantity.*'=> 'required|integer|min:1|max:99999',
                    'note.*'    => 'max:255',
                ];
                $validator = \Validator::make($data, $rules);
                $validator->setAttributeNames(trans('inventories'));
                if ($validator->fails()) {
                    $statusCode = 400;
                    throw new \Exception($validator->messages()->first(), 1);
                }
                $logs = [];
                $result = $inventory->quantity;
                // check inventory
                $change = 0;
                for($i = 0; $i < count($data['type']); $i++) {
                    if ($data['type'][$i] == \App\Define\Inventory::TYPE_OUT_STOCK) {
                        $change -= intval($data['quantity'][$i]);
                    } else {
                        $change += intval($data['quantity'][$i]);
                    }
                }
                if ($result + $change < 0) {
                    $statusCode = 400;
                    throw new \Exception("Lượng xuất kho lớn hơn nhập kho + tồn kho.", 1);
                }

                for($i = 0; $i < count($data['type']); $i++) {
                    $old = $result;
                    if ($data['type'][$i] == \App\Define\Inventory::TYPE_OUT_STOCK) {
                        $result -= intval($data['quantity'][$i]);
                    } else {
                        $result += intval($data['quantity'][$i]);
                    }
                    array_push($logs, [
                        'inventory_id'  => $inventory->id,
                        'product_id'    => $product->id,
                        'data_new'  => $result,
                        'data_old'  => $old,
                        'note'      => trans('inventories.types.' . $data['type'][$i]) . ' ' . intval($data['quantity'][$i]) . ' sản phẩm' . ' ' . $data['note'][$i],
                        'field'     => 'quantity',
                        'action_by' => $request->user()->id,
                        'action_at' => date("Y-m-d H:i:s"),
                    ]);
                }
                $old = $inventory->quantity;
                $inventory->quantity = $result;
                $inventory->save();
                $inventory->logs()->insert($logs);
                $product->quantity = ($product->quantity - $old + $result);
                $product->save();

                $response['message'] = trans('system.success');
                Session::flash('message', trans('system.success'));
                Session::flash('alert-class', 'success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}
