<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAssetAllocationInformation;
use App\Models\Department;
use App\Models\FixedAsset;
use App\Models\FixedAssetDepreciation;
use App\Models\Account;
use App\Models\Accounting;
use App\Models\AllocationCost;
use App\Models\InstrumentToolAllocation;
use App\Models\InstrumentToolDisposal;
use App\Models\InstrumentToolAllocationCost;
use App\Models\ExpensesPrepaidAllocation;
use App\Models\ExpensesPrepaidAllocationCost;
use App\Models\Partner;
use App\Models\Staff;
use App\Models\InstrumentTool;
use App\User;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class InstrumentToolAllocationController {
    public function index(Request $request){
        $query = "1=1"; 
        $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date'); 
        $status = $request->input('status'); 
        if($status) $query .= " AND (status like '" . $status . "' )" ;
        if($from_date && $to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND voucher_date >= '" . $from_date . "' AND voucher_date <= '" .$to_date. "'";
        } else if( !$from_date && $to_date) {
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND  voucher_date <= '" .$to_date. "'";
        } else if ($from_date && !$to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $query .= " AND  voucher_date >= '" .$from_date. "'";
        }
        $vouchers = InstrumentToolAllocation::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->paginate($page_num);
        return view('backend.instrument-tool-allocation.index', compact('vouchers'));
    }
    public function show(Request $request, $id){
        $voucher = InstrumentToolAllocation::find(intval($id));
        $instrument_tool_id = InstrumentToolAllocationCost::where('voucher_id', $id)->pluck('instrument_tool_id')->toArray();
        $instrument_tool = InstrumentTool::whereIn('id', $instrument_tool_id)->get();
        $accounting = Accounting::where('instrument_tool_allocation_voucher_id', $id)->get();
        return view('backend.instrument-tool-allocation.show', compact('voucher', 'instrument_tool', 'accounting'));
    }
    public function create(Request $request){
        return view('backend.instrument-tool-allocation.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $data = $request->all();
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        $data['description'] = $request->voucher_description;
        $data['company_id'] = session('current_company');
        $data['created_by'] = session('created_by');
        $data['status'] = "DRAFT";
        $voucher = InstrumentToolAllocation::create($data);
        for ($i=0; $i < count($request->instrument_tool_id) ; $i++) { 
            $instrument_tool = InstrumentTool::where('id', $request->instrument_tool_id[$i])->first(); 
            $instrument_tool->residual_time = $instrument_tool->residual_time - 1;
            $instrument_tool->save();
        }
        for ($i=0; $i < count($request->i_t) ; $i++) { 
            $instrument_tool_allocation_cost = new InstrumentToolAllocationCost();
            $instrument_tool_allocation_cost->voucher_id = $voucher->id;
            $instrument_tool_allocation_cost->instrument_tool_id = $request->i_t[$i];
            $instrument_tool_allocation_cost->department_id = $request->department_id[$i];
            $instrument_tool_allocation_cost->allocation_rate = $request->allocation_rate[$i];
            $instrument_tool_allocation_cost->allocation_quantity = $request->p_v[$i] * $request->allocation_rate[$i] / 100 / $request->u_p[$i];
            $instrument_tool_allocation_cost->money_amount = $request->money_amount[$i];
            $instrument_tool_allocation_cost->expense_account = $request->expense_account[$i];
            $instrument_tool_allocation_cost->cost_item = $request->cost_item[$i];
            $instrument_tool_allocation_cost->save();
        }
        for ($i=0; $i < count($request->debit_account) ; $i++) { 
            $accounting = new Accounting();
            $accounting->instrument_tool_allocation_voucher_id = $voucher->id;
            $accounting->description = $request->description[$i];
            $accounting->debit_account = $request->debit_account[$i];
            $accounting->credit_account = $request->credit_account[$i];
            $accounting->amount = $request->amount[$i];
            $accounting->debit_object_id = $request->debit_object_id[$i];
            $accounting->credit_object_id = $request->credit_object_id[$i];
            $accounting->debit_object_type = $request->debit_object_type[$i];
            $accounting->credit_object_type = $request->credit_object_type[$i];
            $accounting->cost_item = $request->cost_item[$i];
            $accounting->save();
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.instrument-tool-allocation.index');
    }
    public function edit(Request $request, $id){
        $voucher = InstrumentToolAllocation::find(intval($id));
        $instrument_tool_id = InstrumentToolAllocationCost::where('voucher_id', $id)->pluck('instrument_tool_id')->toArray();
        $instrument_tool = InstrumentTool::whereIn('id', $instrument_tool_id)->get();
        $accounting = Accounting::where('instrument_tool_allocation_voucher_id', $id)->get();
        return view('backend.instrument-tool-allocation.edit', compact('voucher', 'instrument_tool', 'accounting'));
    }
    public function update(Request $request, $id){
        // dd($request->all());
        $voucher = InstrumentToolAllocation::find(intval($id));
        $data = $request->all();
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        $data['description'] = $request->voucher_description;
        if (is_null($voucher)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.instrument-tool-allocation.index');
        }
        $voucher->update($data);
        for ($i=0; $i < count($request->i_t) ; $i++) { 
            $instrument_tool_allocation_cost = InstrumentToolAllocationCost::find($request->allo_info_id[$i]); 
            $instrument_tool_allocation_cost->voucher_id = $voucher->id;
            $instrument_tool_allocation_cost->instrument_tool_id = $request->i_t[$i];
            $instrument_tool_allocation_cost->department_id = $request->department_id[$i];
            $instrument_tool_allocation_cost->allocation_rate = $request->allocation_rate[$i];
            $instrument_tool_allocation_cost->money_amount = $request->money_amount[$i];
            $instrument_tool_allocation_cost->expense_account = $request->expense_account[$i];
            $instrument_tool_allocation_cost->cost_item = $request->cost_item[$i];
            $instrument_tool_allocation_cost->update();
        }

        $accounting = Accounting::where('instrument_tool_allocation_voucher_id', $id)->delete();
        for ($i=0; $i < count($request->debit_account) ; $i++) { 
            $accounting = new Accounting();
            $accounting->instrument_tool_allocation_voucher_id = $voucher->id;
            $accounting->description = $request->description[$i];
            $accounting->debit_account = $request->debit_account[$i];
            $accounting->credit_account = $request->credit_account[$i];
            $accounting->amount = $request->amount[$i];
            $accounting->debit_object_id = $request->debit_object_id[$i];
            $accounting->credit_object_id = $request->credit_object_id[$i];
            $accounting->debit_object_type = $request->debit_object_type[$i];
            $accounting->credit_object_type = $request->credit_object_type[$i];
            $accounting->cost_item = $request->cost_item[$i];
            $accounting->save();
        }

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.instrument-tool-allocation.index');
    }
    public function destroy(Request $request, $id){
    
        $voucher = InstrumentToolAllocation::find($id);
        $allocation_cost = InstrumentToolAllocationCost::where('voucher_id', $id);
        $accounting = Accounting::where('instrument_tool_allocation_voucher_id', $id);
        $check = InstrumentToolAllocation::where('company_id', session('current_company'))->where('month', '>', $voucher->month)->where('year', $voucher->year)->orWhere('year', '>', $voucher->year)->get();
        if (is_null($voucher)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        if (count($check) > 0) {
            Session::flash('message', trans('fixed_asset_depreciation.delete'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $allo_cost = InstrumentToolAllocationCost::where('voucher_id', $id)->distinct()->get(['instrument_tool_id']);
        foreach ($allo_cost as $key => $value) {
            $instrument_tool = InstrumentTool::where('id', $value->instrument_tool_id)->first();
            $instrument_tool->residual_time = $instrument_tool->residual_time + 1;
            // dd($fixed_asset->residual_use_time);
            $instrument_tool->update();
        }
        $voucher->delete();
        $allocation_cost->delete();
        $accounting->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
    public function toSave(Request $request, $id){
        $voucher = FixedAssetDepreciation::find(intval($id));
        if($voucher->status == "DRAFT" && $voucher->created_by == session('created_by')){
            $fixed_assets = FixedAsset::where('company_id', session('current_company'))->where('status',1)->get();
            $partners = Partner::where('company_id', session('current_company'))->where('status', 1)->selectRaw("id, CONCAT('Partner', ' - ', name) as name")->pluck('name')->toArray();
            $departments = Department::where('company_id', session('current_company'))->where('status', 1)->selectRaw("id, CONCAT('Department', ' - ', name) as name")->pluck('name')->toArray();
            $staffs = Staff::where('company_id', session('current_company'))->where('status', 1)->selectRaw("id, CONCAT('Staff', ' - ', name) as name")->pluck('name')->toArray();
            $objects = array_merge($partners, $departments, $staffs);
            $accounting = Accounting::where('depreciation_voucher_id', $id)->get();
            return view('backend.fixed-asset-depreciation.save', compact('voucher', 'fixed_assets', 'partners', 'departments', 'staffs', 'objects', 'accounting'));
        }
        else{
            return redirect()->route('admin.fixed-asset-depreciation.index');
        }
    }
    public function save(Request $request, $id){
        $voucher = FixedAssetDepreciation::find(intval($id));
        $voucher->status = "SAVE";
        $voucher->update();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.fixed-asset-depreciation.index');
    }
    public function getMonthYear(Request $request){
        if ($request->ajax()) {
            $check_created = InstrumentToolAllocation::where('company_id', session('current_company'))->where('month', $request->month)->where('year', $request->year)->get();
            $check_instrument_tool = InstrumentTool::where('company_id', session('current_company'))->where('status', 1)->where('residual_time', '>', 0)->where('voucher_date', '<=', ($request->year . "-" . $request->month . "-31"))->get();
            $allocation = InstrumentToolAllocation::where('company_id', session('current_company'))->get();
            $instrument_tool = InstrumentTool::where('company_id', session('current_company'))->where('status',1)->where('residual_time', '>', 0)->where('voucher_date', '<=', ($request->year . "-" . $request->month . "-31"))->with(['instrumentToolAllocation'=>function($query){$query->with('department');}])->get();
            foreach ($instrument_tool as $key => $value) {
                $allocated_value = 0;
                $disposal_quantity = 0;
                $cost = InstrumentToolAllocationCost::where('instrument_tool_id', $value->id)->get();
                foreach ($cost as $key1 => $value1) {
                    $allocated_value += $value1->money_amount;
                }
                $allocated_quantity = $allocated_value / $value->unit_price;
                $disposal = InstrumentToolDisposal::where('instrument_tool_disposal.instrument_tool_id', $value->id)
                                                            ->whereMonth('voucher_date', $request->month)
                                                            ->whereYear('voucher_date', $request->year)
                                                            ->join('instrument_tool_disposal_details', 'instrument_tool_disposal.id', '=', 'instrument_tool_disposal_details.disposal_voucher_id')
                                                            ->get();
                if(count($disposal)){
                    foreach ($disposal as $key2 => $value2) {
                        $allocated_quantity += $value2->disposal_quantity;
                        $disposal_quantity += $value2->disposal_quantity;
                    } 
                }
                $residual_value = 0;
                $residual = InstrumentToolAllocationCost::where("instrument_tool_id", $value->id)->get();
                foreach ($residual as $key3 => $value3) {
                    $residual_value += $value3->money_amount;
                }
                $value['disposal_quantity'] = $disposal_quantity;
                $value['residual_quantity'] = $value->quantity -  $allocated_quantity;
                $value['residual_value'] = ($value->quantity * $value->unit_price) - $residual_value;
                // dd($value['residual_value']);
            }
            // dd($instrument_tool);
            if(count($check_created) > 0){
                $response = [
                    'status' => 'created'
                ];
            }
            else if(count($check_instrument_tool) == 0 ){
                $response = [
                    'status' => 'error'
                ];
            }
            else if(($request->year."-".$request->month) > date('Y-m')){
                $response = [
                    'status' => 'cannot'
                ];
            }
            else {
                $response = [
                    'month' => $request->month,
                    'year' => $request->year,
                    'instrument_tool' => $instrument_tool,
                    'status' => 'success',
                ]; 
 
            }
            // dd($instrument_tool);
            return response($response);
        }
    }
    public function getDebitObject(Request $request){
        if ($request->ajax()) {
            if($request->type == 'DEPARTMENTS'){
                $departments = Department::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'departments' => $departments,
                    'type' => 'DEPARTMENTS'
                ]; 
            }
            else if($request->type == 'PARTNERS'){
                $partners = Partner::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'partners' => $partners,
                    'type' => 'PARTNERS'
                ]; 
            }
            else if($request->type == 'USERS'){
                $users = User::where('company_id', session('current_company'))->get();
                $response = [
                    'users' => $users,
                    'type' => 'USERS'
                ];
            }
            
            return response($response);
        }
    }
    public function getCreditObject(Request $request){
        if ($request->ajax()) {
            if($request->type == 'DEPARTMENTS'){
                $departments = Department::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'departments' => $departments,
                    'type' => 'DEPARTMENTS'
                ]; 
            }
            else if($request->type == 'PARTNERS'){
                $partners = Partner::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'partners' => $partners,
                    'type' => 'PARTNERS'
                ]; 
            }
            else if($request->type == 'USERS'){
                $users = User::where('company_id', session('current_company'))->get();
                $response = [
                    'users' => $users,
                    'type' => 'USERS'
                ];
            }
            
            return response($response);
        }
    }
}
