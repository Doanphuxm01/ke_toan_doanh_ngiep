<?php

namespace App\Http\Controllers\Backend;

use App\Order;
use App\Brand;
use App\Product;
use App\Customer;
use App\Attribute;
use App\OrderDetail;
use App\ProductEntity;
use App\ProductCategory;
use App\ProductSupplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ReportsController extends Controller
{

    public function index(Request $request)
    {
        $types = \App\Define\Report::getAllReportsForOption();
        // dd($types);

        // $tmp = $request->user()->roles;
        // foreach ($tmp as $t) {
        //     $types = array_merge($types, $t->reports()->where('status', 1)->orderBy('report_roles.position')->pluck('name', 'slug')->toArray());
        // }
        // $suppliers = Supplier::where('status', 1)->pluck('name', 'id')->toArray();
        return view('backend.reports.index', compact('types', 'suppliers'));
    }

    public function export(Request $request, $type = 'excel')
    {
        if(isset(\Cache::get('reports_info_' . $request->user()->id)['type'])) {
            switch ($type) {
                case 'excel':
                    $reports_info = \Cache::get('reports_info_' . $request->user()->id);
                    return \Excel::download(new \App\Exports\ReportExport($reports_info, \Cache::get('reports_' . $request->user()->id . '_' . $reports_info['type'])), 'SUBE_' . $reports_info['type'] . '_' . date('H.m_d-m-Y'). '.xlsx');
                break;
                default:
                break;
            }
        } else {
            dd( 'Bạn cần tạo báo cáo trước!' );
        }
    }

    public function store(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;

        if ($request->ajax()) {
            try {
                $type = $request->input('type');
                $dateRange = $request->date_range;
                $query = "";
                $timeStampFrom = $timeStampTo = 0;
                $data = $request->all();

                if ($dateRange) $dateRange = explode(' - ', $dateRange);
                if (isset($dateRange[0]) && isset($dateRange[1])) {
                    $query = "created_at >= '" . date("Y-m-d 00:00:00", strtotime(str_replace('/', '-', ($dateRange[0] == '' ? '1/1/1970' : $dateRange[0]) ))) . "' AND updated_at <= '" . date("Y-m-d 23:59:59", strtotime(str_replace('/', '-', ($dateRange[1] == '' ? date("d/m/Y") : $dateRange[1]) ))) . "'";
                    $data['from_date'] = $dateRange[0];
                    $data['to_date'] = $dateRange[1];
                    $timeStampFrom = strtotime(str_replace('/', '-', ($dateRange[0] == '' ? '1/1/1970' : $dateRange[0]) ));
                    $timeStampTo = strtotime(str_replace('/', '-', ($dateRange[1] == '' ? date("d/m/Y") : $dateRange[1]) ));
                } else {
                    throw new \Exception("Khoảng thời gian lập báo cáo không hiệu lực.", 1);
                }
                // $types = [];
                // $tmp = $request->user()->roles;
                // foreach ($tmp as $t) {
                //     $types = array_merge($types, $t->reports()->where('status', 1)->orderBy('report_roles.position')->pluck('name', 'slug')->toArray());
                // }
                // if (!isset($types[$type])) throw new \Exception("Bạn không có quyền truy nhập tài nguyên này.", 1);
                $data['name'] = $types[$type];
                switch ($type) {
                    case \App\Define\Report::PRODUCT_ALL:
                        $product_model  = $request->product_model;
                        $product_name   = $request->product_name;
                        $brand          = intval($request->brand);
                        $supplier       = intval($request->supplier);
                        $product_price  = intval($request->product_price);
                        $product_price_operator = $request->product_price_operator;
                        if ($product_price < 0 || !in_array($product_price_operator, \App\Define\Report::getOperators())) {
                            throw new \Exception("Giá đăng bán không hiệu lực.", 1);
                        }

                        if ($product_model) $query  .= " AND model = '" . $product_model . "'";
                        if ($product_name) $query  .= " AND name like '%" . $product_name . "%'";
                        if ($brand) $query  .= " AND brand_id = {$brand}";
                        if ($supplier) $query  .= " AND product_supplier_id = {$supplier}";

                        // get all product id have child
                        $productsHaveChildren = $tmp = Product::whereRaw($query)->where('parent_id', "<>", 0)->orderBy('id', 'DESC')->select('parent_id')->distinct()->get();//, 'parent_id'

                        for ($i = 0; $i < count($productsHaveChildren); $i++) {
                            $product = Product::find($productsHaveChildren[$i]->parent_id);
                            $children = $product->children()->where("price_ipo", $product_price_operator, $product_price)->get();
                            for($j = 0; $j < $children->count(); $j++) {
                                $entities   = ProductEntity::where('product_id', $children[$j]->id)->get();
                                $attributes = [];
                                foreach ($entities as $entity) {
                                    $attribute = Attribute::find($entity->attribute_id);
                                    if (is_null($attribute)) continue;
                                    $attributes[$attribute->name] = $entity->attribute_value;
                                }
                                $children[$j]->attributes = $attributes;
                            }
                            $productsHaveChildren[$i]->children = $children;
                        }
                        // get products no have children
                        $productsNoChild = Product::whereRaw($query)->where('parent_id', 0)->where("price_ipo", $product_price_operator, $product_price)->whereNotIn("id", array_column($tmp->toArray(), "parent_id"))->orderBy('created_at', 'DESC')->select('id', 'model', 'name', 'price_ipo', 'price_original', 'weight', 'length', 'width', 'height', 'view')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren]])->render();
                        break;
                    case \App\Define\Report::PRODUCT_VIEW:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $product_model   = $request->product_model;
                        $product_name   = $request->product_name;
                        $product_price  = intval($request->product_price);
                        $product_price_operator = $request->product_price_operator;
                        if ($product_price < 0 || !in_array($product_price_operator, \App\Define\Report::getOperators())) {
                            throw new \Exception("Giá đăng bán không hiệu lực.", 1);
                        }

                        if ($product_model) $query  = " AND model = '" . $product_model . "'";
                        if ($product_name) $query  .= " AND name like '%" . $product_name . "%'";
                        // get all product id have child
                        $productsHaveChildren = $tmp = Product::whereRaw($query)->where('parent_id', "<>", 0)->orderBy('view', 'DESC')->select('parent_id')->distinct()->get();//, 'parent_id'

                        for ($i = 0; $i < count($productsHaveChildren); $i++) {
                            $product = Product::find($productsHaveChildren[$i]->parent_id);
                            $children = $product->children()->where("price_ipo", $product_price_operator, $product_price)->get();
                            for($j = 0; $j < $children->count(); $j++) {
                                $entities   = ProductEntity::where('product_id', $children[$j]->id)->get();
                                $attributes = [];
                                foreach ($entities as $entity) {
                                    $attribute = Attribute::find($entity->attribute_id);
                                    if (is_null($attribute)) continue;
                                    $attributes[$attribute->name] = $entity->attribute_value;
                                }
                                $children[$j]->attributes = $attributes;
                            }
                            $productsHaveChildren[$i]->children = $children;

                            $counter = ProductView::where("product_id", $product->id)->where("time", ">=", $timeStampFrom)->where("time", "<=", $timeStampTo)->groupBy("time")->selectRaw("SUM(counter) as counter, time")->pluck("counter", 'time')->toArray();

                            if (empty($counter) || !array_sum($counter)) {
                                unset($productsHaveChildren[$i]);
                                continue;
                            }
                            $productsHaveChildren[$i]->view = array_sum($counter);
                        }

                        // get products no have children
                        $productsNoChild = Product::whereRaw($query)->where('parent_id', 0)->where("price_ipo", $product_price_operator, $product_price)->whereNotIn("id", array_column($tmp->toArray(), "parent_id"))->orderBy('view', 'DESC')->select('id', 'model', 'name', 'price_ipo', 'price_original', 'weight', 'length', 'width', 'height', 'view')->get();
                        for ($i = 0; $i < count($productsNoChild); $i++) {
                            $counter = ProductView::where("product_id", $productsNoChild[$i]->id)->where("time", ">=", $timeStampFrom)->where("time", "<=", $timeStampTo)->groupBy("time")->selectRaw("SUM(counter) as counter, time")->pluck("counter", 'time')->toArray();
                            if (empty($counter) || !array_sum($counter)) {
                                unset($productsNoChild[$i]);
                                continue;
                            }
                            $productsNoChild[$i]->view = array_sum($counter);
                        }

                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren]])->render();
                        break;
                    case \App\Define\Report::PRODUCT_INVENTORY:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $product_model   = $request->product_model;
                        $product_name   = $request->product_name;
                        $product_in_stock  = intval($request->product_in_stock);
                        $product_in_stock_operator = $request->product_in_stock_operator;
                        if ($product_in_stock < 0 || !in_array($product_in_stock_operator, \App\Define\Report::getOperators())) {
                            throw new \Exception("Số tồn kho không hiệu lực.", 1);
                        }
                        // chi hien thi san co thay doi trong giai doan nay
                        $productIds = ProductInStock::whereRaw($query)->pluck('product_id', 'product_id')->toArray();
                        $productIds = ($productIds ? $productIds : [-1]);
                        $query = "1=1";
                        if ($product_model) $query  .= " AND model = '" . $product_model . "'";
                        if ($product_name) $query  .= " AND name like '%" . $product_name . "%'";
                        // get all product id have child
                        $productsHaveChildren = $tmp = Product::whereNotIn('id', $productIds)->whereRaw($query)->where('parent_id', "<>", 0)->orderBy('created_at', 'DESC')->select('parent_id')->distinct()->get();//, 'parent_id'

                        for ($i = 0; $i < count($productsHaveChildren); $i++) {
                            $product = Product::find($productsHaveChildren[$i]->parent_id);
                            $children = $product->children()->whereIn('id', $productIds)->where("quantity", $product_in_stock_operator, $product_in_stock)->get();
                            for($j = 0; $j < $children->count(); $j++) {
                                $entities   = ProductEntity::where('product_id', $children[$j]->id)->get();
                                $attributes = [];
                                foreach ($entities as $entity) {
                                    $attribute = Attribute::find($entity->attribute_id);
                                    if (is_null($attribute)) continue;
                                    $attributes[$attribute->name] = $entity->attribute_value;
                                }
                                $children[$j]->attributes = $attributes;
                                // quantity
                                $children[$j]->in_stock_import = ProductInStock::where("product_id", $children[$j]->id)->where("time", ">=", $timeStampFrom)->where("time", "<=", $timeStampTo)->where("quantity", ">", 0)->groupBy("time")->selectRaw("SUM(quantity) as quantity, time")->pluck("quantity", 'time')->toArray();
                                //dd(\DB::getQueryLog());
                                $children[$j]->in_stock_export = ProductInStock::where("product_id", $children[$j]->id)->where("time", ">=", $timeStampFrom)->where("time", "<=", $timeStampTo)->where("quantity", "<", 0)->groupBy("time")->selectRaw("SUM(quantity) as quantity, time")->pluck("quantity", 'time')->toArray();

                                $children[$j]->in_stock = $children[$j]->quantity - array_sum($children[$j]->in_stock_import) - array_sum($children[$j]->in_stock_export);
                            }

                            $productsHaveChildren[$i]->children = $children;
                        }
                        // get products no have children
                        $productsNoChild = Product::whereRaw($query)->whereIn('id', $productIds)->where('parent_id', 0)->where("quantity", $product_in_stock_operator, $product_in_stock)->whereNotIn("id", array_column($tmp->toArray(), "parent_id"))->orderBy('created_at', 'DESC')->select('id', 'model', 'name', 'price_ipo', 'price_original', 'weight', 'length', 'width', 'height', 'view', 'unit', 'quantity')->get();

                        for ($i = 0; $i < count($productsNoChild); $i++) {
                            $productsNoChild[$i]->in_stock_import = ProductInStock::where("product_id", $productsNoChild[$i]->id)->where("time", ">=", $timeStampFrom)->where("time", "<=", $timeStampTo)->where("quantity", ">", 0)->groupBy("time")->selectRaw("SUM(quantity) as quantity, time")->pluck("quantity", 'time')->toArray();
                            $productsNoChild[$i]->in_stock_export = ProductInStock::where("product_id", $productsNoChild[$i]->id)->where("time", ">=", $timeStampFrom)->where("time", "<=", $timeStampTo)->where("quantity", "<", 0)->groupBy("time")->selectRaw("SUM(quantity) as quantity, time")->pluck("quantity", 'time')->toArray();

                            $productsNoChild[$i]->in_stock = $productsNoChild[$i]->quantity - array_sum($productsNoChild[$i]->in_stock_export) - array_sum($productsNoChild[$i]->in_stock_import);
                        }

                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'timeStampFrom' => $timeStampFrom, 'timeStampTo' => $timeStampTo], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'timeStampFrom' => $timeStampFrom, 'timeStampTo' => $timeStampTo]])->render();
                        break;
                    case \App\Define\Report::ORDER_ALL:
                        $query .= " AND status <> '" . \App\Define\Order::STATUS_RECORDED . "'";
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        if ($request->order_code) $query .= " AND code = '" . $request->order_code . "'";
                        if ($request->payment_method) $query .= " AND payment_method = '" . $request->payment_method . "'";
                        if ($request->order_status) $query .= " AND status = '" . $request->order_status . "'";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['orders' => $orders], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['orders' => $orders]])->render();
                        break;
                    case \App\Define\Report::ORDER_CPN:
                        $query .= " AND status <> '" . \App\Define\Order::STATUS_RECORDED . "'";
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $query .= " AND (delivery_provider = '" . \App\Define\Order::DELIVERY_GHN . "' OR delivery_provider = '" . \App\Define\Order::DELIVERY_GHTK . "')";
                        if ($request->order_code) $query .= " AND code = '" . $request->order_code . "'";
                        if ($request->payment_method) $query .= " AND payment_method = '" . $request->payment_method . "'";
                        if ($request->order_status) $query .= " AND status = '" . $request->order_status . "'";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['orders' => $orders], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['orders' => $orders]])->render();
                        break;
                    case \App\Define\Report::ORDER_BANK_NL:
                        $query .= " AND status <> '" . \App\Define\Order::STATUS_RECORDED . "'";
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        if ($request->order_code) $query .= " AND code = '" . $request->order_code . "'";
                        $query .= (" AND payment_method = '" . \App\Define\Order::PAYMENT_METHOD_ONLINE . "'");
                        if ($request->order_status) $query .= " AND status = '" . $request->order_status . "'";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['orders' => $orders], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['orders' => $orders]])->render();
                        break;
                    case \App\Define\Report::ORDER_REVENUE:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $query  .= " AND (status = '" . \App\Define\Order::STATUS_COMPLETE . "' OR (status = '" . \App\Define\Order::STATUS_DELIVERY . "' AND delivery_status = '" . \App\Define\Order::DELIVERY_STATUS_DELIVERING . "'))";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['orders' => $orders], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['orders' => $orders]])->render();
                        break;
                    case \App\Define\Report::PRODUCT_PROMOTION:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $query  .= " AND (status = '" . \App\Define\Order::STATUS_COMPLETE . "' OR (status = '" . \App\Define\Order::STATUS_DELIVERY . "' AND delivery_status = '" . \App\Define\Order::DELIVERY_STATUS_DELIVERING . "'))";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->pluck('id')->toArray();
                        $orders = (empty($orders) ? [-1] : $orders);
                        $promotionProducts = \DB::table('order_details')
                            ->select(\DB::raw('*, sum(quantity) as quantity'))
                            ->whereIn('order_id', $orders)
                            ->whereNotNull('promotion_percent')
                            ->groupBy('product_id')
                            ->groupBy('promotion_percent')
                            ->groupBy('price_ipo')
                            ->groupBy('price_unit')
                            ->orderByRaw("FIELD(order_id, " . implode(',', $orders) . ")")
                            ->get()->toArray();
                        $uniqueProductIds = [];
                        foreach ($promotionProducts as $promotionProduct) {
                            $uniqueProductIds[$promotionProduct->product_id] = (isset($uniqueProductIds[$promotionProduct->product_id]) ? ($uniqueProductIds[$promotionProduct->product_id]++) : 1);
                        }
                        $product_model   = $request->product_model;
                        $product_name   = $request->product_name;
                        $query = "1=1";
                        if ($product_model) $query  .= " AND model = '" . $product_model . "'";
                        if ($product_name) $query  .= " AND name like '%" . $product_name . "%'";
                        // get all product id have child
                        $productsHaveChildren = $tmp = Product::whereRaw($query)->where("parent_id", "<>", 0)->whereIn('id', array_keys($uniqueProductIds))->select('parent_id')->distinct()->get();//, 'parent_id'
                        for ($i = 0; $i < count($productsHaveChildren); $i++) {
                            $product = Product::find($productsHaveChildren[$i]->parent_id);
                            $children = $product->children()->get();
                            $toDelete = [];
                            for($j = 0; $j < $children->count(); $j++) {
                                if (!isset($uniqueProductIds[$children[$j]->id])) {
                                    array_push($toDelete, $j);
                                    continue;
                                }
                                $entities   = ProductEntity::where('product_id', $children[$j]->id)->get();
                                $attributes = [];
                                foreach ($entities as $entity) {
                                    $attribute = Attribute::find($entity->attribute_id);
                                    if (is_null($attribute)) continue;
                                    $attributes[$attribute->name] = $entity->attribute_value;
                                }
                                $children[$j]->attributes = $attributes;
                                // $children[$j]->counter = $uniqueProductIds[$children[$j]->id];
                            }
                            foreach ($toDelete as $toDel) {
                                unset($children[$toDel]);
                            }
                            $productsHaveChildren[$i]->children = $children;
                        }
                        // get products no have children
                        $productsNoChild = Product::whereRaw($query)->whereIn('id', array_keys($uniqueProductIds))->where('parent_id', 0)->whereNotIn("id", array_column($tmp->toArray(), "parent_id"))->select('id', 'model', 'name', 'price_ipo', 'price_original', 'weight', 'length', 'width', 'height', 'view')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'promotionProducts' => $promotionProducts], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'promotionProducts' => $promotionProducts]])->render();
                        break;
                    case \App\Define\Report::PRODUCT_BEST_SELLER:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        // $query  .= " AND (status = '" . \App\Define\Order::STATUS_COMPLETE . "' OR (status = '" . \App\Define\Order::STATUS_DELIVERY . "' AND delivery_status = '" . \App\Define\Order::DELIVERY_STATUS_DELIVERING . "'))";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->pluck('id')->toArray();

                        $promotionProducts = \DB::table('order_details')
                            ->select(\DB::raw('product_id, sale_amount, sale_percent, price_ipo, price_unit, sum(quantity) as quantity'))//
                            ->whereIn('order_id', $orders)
                            ->whereNotNull('sale_id')
                            ->groupBy('product_id')
                            ->groupBy('sale_amount')
                            ->groupBy('sale_percent')
                            ->groupBy('price_ipo')
                            ->groupBy('price_unit')
                            ->orderByRaw("product_id, quantity DESC")
                            ->get()->toArray();
                        $details = OrderDetail::whereIn('order_id', $orders)->whereNotNull('sale_id')->select('sale_id', 'quantity', 'price_unit', 'price_original', 'price_ipo', 'sale_amount', 'sale_percent', 'product_id')->get()->toArray();
                        $uniqueProductIds = array_column($details, 'product_id', 'product_id');
                        $product_model   = $request->product_model;
                        $product_name   = $request->product_name;
                        $query = "1=1";
                        if ($product_model) $query  .= " AND model = '" . $product_model . "'";
                        if ($product_name) $query  .= " AND name like '%" . $product_name . "%'";
                        // get all product id have child
                        $productsHaveChildren = $tmp = Product::whereRaw($query)->where("parent_id", "<>", 0)->whereIn('id', ($uniqueProductIds))->select('parent_id')->distinct()->select('id', 'model', 'name', 'weight', 'length', 'width', 'height', 'view', 'parent_id')->get();//, 'parent_id'
                        for ($i = 0; $i < count($productsHaveChildren); $i++) {
                            $product = Product::find($productsHaveChildren[$i]->parent_id);
                            $children = $product->children()->get();
                            for($j = 0; $j < $children->count(); $j++) {
                                if (!isset($uniqueProductIds[$children[$j]->id])) continue;
                                $entities   = ProductEntity::where('product_id', $children[$j]->id)->get();
                                $attributes = [];
                                foreach ($entities as $entity) {
                                    $attribute = Attribute::find($entity->attribute_id);
                                    if (is_null($attribute)) continue;
                                    $attributes[$attribute->name] = $entity->attribute_value;
                                }
                                $children[$j]->attributes = $attributes;
                                // $children[$j]->counter = $uniqueProductIds[$children[$j]->id];
                            }
                            $productsHaveChildren[$i]->children = $children;
                        }
                        // get products no have children
                        $productsNoChild = Product::whereRaw($query)->whereIn('id', ($uniqueProductIds))->where('parent_id', 0)->whereNotIn("id", array_column($tmp->toArray(), "parent_id"))->select('id', 'model', 'name', 'weight', 'length', 'width', 'height', 'view')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'promotionProducts' => $promotionProducts], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'promotionProducts' => $promotionProducts]])->render();
                        break;
                    case \App\Define\Report::ORDER_PROFIT:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $query .= " AND (status = '" . \App\Define\Order::STATUS_COMPLETE . "' OR (status = '" . \App\Define\Order::STATUS_DELIVERY . "' AND delivery_status = '" . \App\Define\Order::DELIVERY_STATUS_DELIVERING . "'))";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['orders' => $orders], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['orders' => $orders]])->render();
                        break;
                    case \App\Define\Report::PRODUCT_PROFIT:
                        if (intval(floor(($timeStampTo-$timeStampFrom) / 86400)) > 31) {
                            throw new \Exception("Khoảng thời gian tối đa là 31 ngày.", 1);
                        }
                        $query  .= " AND (status = '" . \App\Define\Order::STATUS_COMPLETE . "' OR (status = '" . \App\Define\Order::STATUS_DELIVERY . "' AND delivery_status = '" . \App\Define\Order::DELIVERY_STATUS_DELIVERING . "'))";
                        $orders = Order::whereRaw($query)->orderBy('updated_at', 'DESC')->pluck('id')->toArray();
                        $promotionProducts = \DB::table('order_details')
                            ->select(\DB::raw('*, sum(quantity) as quantity'))
                            ->whereIn('order_id', $orders)
                            // ->whereNotNull('promotion_percent')
                            ->groupBy('product_id')
                            ->groupBy('promotion_percent')
                            ->groupBy('price_ipo')
                            ->groupBy('price_unit')
                            ->orderByRaw("quantity DESC")
                            ->get()->toArray();
                        $uniqueProductIds = [];
                        foreach ($promotionProducts as $promotionProduct) {
                            $uniqueProductIds[$promotionProduct->product_id] = (isset($uniqueProductIds[$promotionProduct->product_id]) ? ($uniqueProductIds[$promotionProduct->product_id]++) : 1);
                        }
                        $product_model   = $request->product_model;
                        $product_name   = $request->product_name;
                        $query = "1=1";
                        if ($product_model) $query  .= " AND model = '" . $product_model . "'";
                        if ($product_name) $query  .= " AND name like '%" . $product_name . "%'";
                        // get all product id have child
                        $productsHaveChildren = $tmp = Product::whereRaw($query)->where("parent_id", "<>", 0)->whereIn('id', array_keys($uniqueProductIds))->select('parent_id')->distinct()->get();//, 'parent_id'
                        for ($i = 0; $i < count($productsHaveChildren); $i++) {
                            $product = Product::find($productsHaveChildren[$i]->parent_id);
                            $children = $product->children()->get();
                            $toDelete = [];
                            for($j = 0; $j < $children->count(); $j++) {
                                if (!isset($uniqueProductIds[$children[$j]->id])) {
                                    array_push($toDelete, $j);
                                    continue;
                                }
                                $entities   = ProductEntity::where('product_id', $children[$j]->id)->get();
                                $attributes = [];
                                foreach ($entities as $entity) {
                                    $attribute = Attribute::find($entity->attribute_id);
                                    if (is_null($attribute)) continue;
                                    $attributes[$attribute->name] = $entity->attribute_value;
                                }
                                $children[$j]->attributes = $attributes;
                                // $children[$j]->counter = $uniqueProductIds[$children[$j]->id];
                            }
                            foreach ($toDelete as $toDel) {
                                unset($children[$toDel]);
                            }
                            $productsHaveChildren[$i]->ordered_children = $children;
                        }
                        // get products no have children
                        $productsNoChild = Product::whereRaw($query)->whereIn('id', array_keys($uniqueProductIds))->where('parent_id', 0)->whereNotIn("id", array_column($tmp->toArray(), "parent_id"))->select('id', 'model', 'name', 'price_ipo', 'price_original', 'weight', 'length', 'width', 'height', 'view')->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'promotionProducts' => $promotionProducts], 20);
                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['productsNoChild' => $productsNoChild, 'productsHaveChildren' => $productsHaveChildren, 'promotionProducts' => $promotionProducts]])->render();
                        break;
                    default: throw new \Exception("Bạn không có quyền truy nhập tài nguyên này.", 1);
                }

                $response['message']  = $renderView;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function store1(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $type = $request->input('type');
                $dateRange = $request->date_range;
                $query = "";
                $timeStampFrom = $timeStampTo = 0;
                $data = $request->all();

                if ($dateRange) $dateRange = explode(' - ', $dateRange);
                if (isset($dateRange[0]) && isset($dateRange[1])) {
                    $query = "created_at >= '" . date("Y-m-d 00:00:00", strtotime(str_replace('/', '-', ($dateRange[0] == '' ? '1/1/1970' : $dateRange[0]) ))) . "' AND updated_at <= '" . date("Y-m-d 23:59:59", strtotime(str_replace('/', '-', ($dateRange[1] == '' ? date("d/m/Y") : $dateRange[1]) ))) . "'";
                    $data['from_date'] = $dateRange[0];
                    $data['to_date'] = $dateRange[1];
                    $timeStampFrom = strtotime(str_replace('/', '-', ($dateRange[0] == '' ? '1/1/1970' : $dateRange[0]) ));
                    $timeStampTo = strtotime(str_replace('/', '-', ($dateRange[1] == '' ? date("d/m/Y") : $dateRange[1]) ));
                } else {
                    throw new \Exception("Khoảng thời gian lập báo cáo không hiệu lực.", 1);
                }
                $types = \App\Define\Report::getAllReportsForOption();
                $data['name'] = $types[$type];
                switch ($type) {
                    case \App\Define\Report::CONTAINER_ALL:
                        $customers = Customer::pluck('fullname', 'id')->toArray();
                        $containers = Container::whereRaw($query)->get();
                        \Cache::put('reports_info_' . $request->user()->id, $data, 20);
                        \Cache::put('reports_' . $request->user()->id . '_' . $type, ['containers' => $containers, 'customers' => $customers], 20);

                        $renderView = view("backend.reports.template.{$type}", ['type' => $type, 'info' => $data, 'data' => ['containers' => $containers, 'customers' => $customers]])->render();
                        break;
                    default: throw new \Exception("Bạn không có quyền truy nhập tài nguyên này.", 1);
                }

                $response['message']  = $renderView;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}