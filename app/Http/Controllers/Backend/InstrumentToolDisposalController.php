<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAsset;
use App\Models\FixedAssetDisposal;
use App\Models\Account;
use App\Models\CostItem;
use App\Models\Accounting;
use App\Models\InstrumentToolDisposal;
use App\Models\InstrumentToolDisposalDetail;
use App\Models\InstrumentTool;
use App\Models\InstrumentToolAllocation;
use App\Models\InstrumentToolAllocationCost;
use App\Models\FixedAssetAllocationCost;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class InstrumentToolDisposalController 
{
    public function index(Request $request){
      $query = "1=1"; 
      $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
      $from_date = $request->input('from_date'); 
      $to_date = $request->input('to_date'); 
      $status = $request->input('status'); 
      if($status) $query .= " AND (status like '" . $status . "' )" ;
      if($from_date && $to_date) {
          $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
          $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
          $query .= " AND voucher_date >= '" . $from_date . "' AND voucher_date <= '" .$to_date. "'";
      } else if( !$from_date && $to_date) {
          $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
          $query .= " AND  voucher_date <= '" .$to_date. "'";
      } else if ($from_date && !$to_date) {
          $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
          $query .= " AND  voucher_date >= '" .$from_date. "'";
      }
      $disposals = InstrumentToolDisposal::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->paginate($page_num);
      return view('backend.instrument-tool-disposal.index', compact('disposals'));
    }
    public function show(Request $request, $id){
        $disposal = InstrumentToolDisposal::find(intval($id));
        $date = explode("-", $disposal->voucher_date);
        $allocation_voucher = InstrumentToolAllocation::where('month', $date[1])->where('year', $date[0])->get();
        $instrument_tools = InstrumentTool::where('company_id', session('current_company'))->where('status', 1)->pluck('code', 'id')->toArray();   
        $details = InstrumentToolDisposalDetail::where('disposal_voucher_id', $id)->get();
        $instrument_tool = InstrumentTool::where('id', $disposal->instrument_tool_id)->first();
        if (is_null($disposal)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.instrument-tool-disposal.index');
        }
        return view('backend.instrument-tool-disposal.show', compact('disposal', 'instrument_tools', 'details', 'instrument_tool', 'accounts', 'cost_items', 'disposal_accounts', 'accumulated_depreciation'));
    }
    public function create(Request $request){
        
        $instrument_tools = InstrumentTool::where('company_id', session('current_company'))->where('status', 1)->pluck('code', 'id')->toArray();   
        // dd($instrument_tools);
        $accounts = Account::getActiveAccounts(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        $disposal_accounts = Account::getExpenseAccounts(session('current_company'));
        $latest_voucher_number = InstrumentToolDisposal::latest()->pluck('id')->first();
        $str = substr($latest_voucher_number, -7);
        $new_voucher_number = $str + 1;
        if(strlen($new_voucher_number) == 1){
           $voucher_number = "GGCCDC000000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 2){
           $voucher_number = "GGCCDC00000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 3){
           $voucher_number = "GGCCDC0000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 4){
           $voucher_number = "GGCCDC000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 5){
           $voucher_number = "GGCCDC00" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 6){
           $voucher_number = "GGCCDC0" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 7){
           $voucher_number = "GGCCDC" . $new_voucher_number;
        }
        return view('backend.instrument-tool-disposal.create', compact('instrument_tools', 'accounts', 'disposal_accounts', 'voucher_number', 'cost_items'));
    }
    public function store(Request $request){
      $data = $request->all();
    //   dd($data);
      $voucher_date = str_replace("/", "-", $request->voucher_date);
      $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
      $data['created_by'] = session('created_by');
      $data['company_id'] = session('current_company');
      $data['status'] = "DRAFT";
      $voucher = InstrumentToolDisposal::create($data);
      for ($i=0; $i < count($request->department_id) ; $i++) { 
         $detail = new InstrumentToolDisposalDetail();
         $detail->disposal_voucher_id = $voucher->id;
         $detail->department_id = $request->department_id[$i];
         $detail->residual_quantity = $request->residual_quantity[$i];
         $detail->disposal_quantity = $request->disposal_quantity[$i];
         $detail->amount = $request->residual_value[$i];
         $detail->save();
      }
    //   dd($detail);
      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');
      return redirect()->route('admin.instrument-tool-disposal.index');
    }
    public function edit(Request $request, $id){
        $disposal = InstrumentToolDisposal::find(intval($id));
        $date = explode("-", $disposal->voucher_date);
        $allocation_voucher = InstrumentToolAllocation::where('month', $date[1])->where('year', $date[0])->get();
        if(count($allocation_voucher) > 0){
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.instrument-tool-disposal.index');
        }
        else{
            $instrument_tools = InstrumentTool::where('company_id', session('current_company'))->where('status', 1)->pluck('code', 'id')->toArray();   
            $details = InstrumentToolDisposalDetail::where('disposal_voucher_id', $id)->get();
            $instrument_tool = InstrumentTool::where('id', $disposal->instrument_tool_id)->first();
            if (is_null($disposal)) {
                Session::flash('message', trans('system.have_an_error'));
                Session::flash('alert-class', 'danger');
                return redirect()->route('admin.instrument-tool-disposal.index');
            }
            return view('backend.instrument-tool-disposal.edit', compact('disposal', 'instrument_tools', 'details', 'instrument_tool', 'accounts', 'cost_items', 'disposal_accounts', 'accumulated_depreciation'));
        }
        
    }
    public function update(Request $request, $id){
        $disposal = InstrumentToolDisposal::find(intval($id));
        $data = $request->all();
        // dd($data);
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        if (is_null($disposal)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.instrument-tool-disposal.index');
        }
        $disposal->update($data);
        if($request->detail_id){
            for ($i=0; $i < count($request->detail_id) ; $i++) { 
                $detail = InstrumentToolDisposalDetail::find($request->detail_id[$i]);
                $detail->disposal_voucher_id = $id;
                $detail->department_id = $request->department_id;
                $detail->residual_quantity = $request->residual_quantity[$i];
                $detail->disposal_quantity = $request->disposal_quantity[$i];
                $detail->amount = $request->residual_value[$i];
                // dd($detail);
                $detail->update();
            }
        }
        else{
            InstrumentToolDisposalDetail::where('disposal_voucher_id', $id)->delete();
            for ($i=0; $i < count($request->department_id) ; $i++) { 
                $detail = new InstrumentToolDisposalDetail();
                $detail->disposal_voucher_id = $id;
                $detail->department_id = $request->department_id[$i];
                $detail->residual_quantity = $request->residual_quantity[$i];
                $detail->disposal_quantity = $request->disposal_quantity[$i];
                $detail->amount = $request->residual_value[$i];
                $detail->save();
             }
        }
        
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.instrument-tool-disposal.index');
    }
    public function destroy(Request $request, $id){
    
      $disposal = InstrumentToolDisposal::find($id);
      $detail = InstrumentToolDisposalDetail::where('disposal_voucher_id', $id);
      if (is_null($disposal)) {
          Session::flash('message', trans('system.have_an_error'));
          Session::flash('alert-class', 'danger');
          return back();
      }
      
      $disposal->delete();
      $detail->delete();

      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');
      return back();
  }
    public function getInfo(Request $request){
        if ($request->ajax()) {
            $instrument_tool = InstrumentTool::where('id', $request->instrument_tool_id)->with(['instrumentToolAllocation'=>function($query){$query->with('department');}])->first();
            $allocated_quantity = InstrumentToolDisposal::checkAllocatedQuantity($instrument_tool->id);
            foreach ($instrument_tool->instrumentToolAllocation as $key => $value) {
                foreach ($allocated_quantity as $key1 => $value1) {
                    if($value->department_id == $key1){
                        $value['allocated_quantity'] = $value1;
                    }
                }
            }
            // dd($value['allocated_quantity'] = $value1);
            $response = [
                'instrument_tool' => $instrument_tool,
                
            ]; 
            // dd($instrument_tool);
            return response($response);
        }
    }

}
