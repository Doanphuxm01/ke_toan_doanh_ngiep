<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Exports\DemoExport;
use App\Bank;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class DemoController 
{
    public function index(Request $request)
    {
        $banks = Bank::all();
        // dd(count($data['banks']));
       
        // for($i = 1; $i <= count($users); $i++){
        // foreach ($users as $key => $value) {
                      
        //             $a[] = "{" . '"recid"' . ":" . $i++ . "," . ltrim(json_encode($value), "{");
        //     }  
        //     $records = implode(",", $a);

        // }
        return view('backend.demo.demo1', compact('banks'));   
    }
    public function getData(){
        $banks = Bank::all();
        
        $records = [];
        foreach ($banks as $key => $value) {
            $record = [
                'id' => $value->id, 'name'=> $value->name
            ];
            $records[] = $record;
        }
        $data = [   
            'total' => -1,          
            'records' => $records
        ];    
        return response($data);
    }
    public function dataExport(){
        $banks = Bank::all();
        $abc = [
            [
                [
                    'text' => 'ID'
                ],
                [
                    'text' => 'Name'
                ]
            ]

        ];
        foreach ($banks as $key => $value) {
            $record = [
                [
                    'text' => $value->id
                ],
                [
                    'text' => $value->name
                ]
            ];
            $abc[] = $record;
        }
        $tableData = [[
            'sheetName' => 'Sheet1',
            'data' => $abc
        ]];
        $data = [   
            'option' => ['filename' => 'Export'],          
            'tableData' => $tableData
        ];    
        return response($data);
    }
    public function exportExcel(){
        return Excel::download(new DemoExport, 'Demo.xlsx'); 
    }
}
