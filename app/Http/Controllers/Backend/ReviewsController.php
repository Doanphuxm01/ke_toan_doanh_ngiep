<?php

namespace App\Http\Controllers\Backend;

use App\Review;
use App\Product;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ReviewsController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval($request->input('status', -1));
        $name = trim($request->input('name'));
        if ($name) $query .= " AND (name like '%" . $name . "%' OR sku like '%" . $name . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        $reviews = Review::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);

        return view('backend.reviews.index', compact('reviews'));
    }

    public function edit(Request $request, $id)
    {
        $review = Review::find(intval($id));
        if (is_null($review)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.reviews.index');
        }
        $customer = Customer::find($review->customer_id);
        $product = Product::find($review->product_id);
        if (is_null($customer) || is_null($product)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.reviews.index');
        }
        return view('backend.reviews.edit', compact('review', 'product', 'customer'));
    }

    public function update(Request $request, $id)
    {
        $status = intval($request->input('status', -1));
        $rules = [
            'comment'   => 'required|max:1024',
            'reply'     => 'max:1024',
        ];
        $validator = \Validator::make($data = $request->only(['comment', 'reply', 'status']), $rules);
        $validator->setAttributeNames(trans('reviews'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $review = Review::find(intval($id));
        if (is_null($review)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.reviews.index');
        }
        if ($review->reply <> $data['reply']) {
            $review->reply_by = auth()->guard('admin')->user()->id;
            $review->reply = $data['reply'];
        }
        $review->comment = $data['comment'];
        $review->status = $data['status'];
        $review->save();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.reviews.index');
    }

    public function publish(Request $request, $id)
    {
        $response = [ 'success' => 0, 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $request->merge(['status' => $request->input('status', 0)]);
                $review = Review::find(intval($id));
                if (is_null($review)) {
                    $statusCode = 404;
                    throw new \Exception(trans('system.no_record_found'));
                }
                $review->status = ($request->status == 'true' ? 1 : 0);
                $review->save();

                $response['success'] = 1;
                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function destroy($id)
    {
        $review = Review::find(intval($id));
        if (is_null($review)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $review->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
}
