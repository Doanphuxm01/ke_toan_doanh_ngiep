<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAssetAllocationInformation;
use App\Models\Department;
use App\Models\FixedAsset;
use App\Models\FixedAssetDepreciation;
use App\Models\Account;
use App\Models\Accounting;
use App\Models\AllocationCost;
use App\Models\ExpensesPrepaidAllocation;
use App\Models\ExpensesPrepaidAllocationCost;
use App\Models\Partner;
use App\Models\Staff;
use App\Models\ExpensesPrepaid;
use App\User;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class ExpensesPrepaidAllocationController {
    public function index(Request $request){
        $query = "1=1"; 
        $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date'); 
        $status = $request->input('status'); 
        if($status) $query .= " AND (status like '" . $status . "' )" ;
        if($from_date && $to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND voucher_date >= '" . $from_date . "' AND voucher_date <= '" .$to_date. "'";
        } else if( !$from_date && $to_date) {
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND  voucher_date <= '" .$to_date. "'";
        } else if ($from_date && !$to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $query .= " AND  voucher_date >= '" .$from_date. "'";
        }
        $vouchers = ExpensesPrepaidAllocation::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->paginate($page_num);
        return view('backend.expenses-prepaid-allocation.index', compact('vouchers'));
    }
    public function show(Request $request, $id){
        $voucher = ExpensesPrepaidAllocation::find(intval($id));
        $expenses_prepaid_id = ExpensesPrepaidAllocationCost::where('voucher_id', $id)->pluck('expenses_prepaid_id')->toArray();
        $expenses_prepaid = ExpensesPrepaid::whereIn('id', $expenses_prepaid_id)->get();
        $accounting = Accounting::where('expenses_prepaid_allocation_voucher_id', $id)->get();
        return view('backend.expenses-prepaid-allocation.show', compact('voucher', 'expenses_prepaid', 'accounting'));
    }
    public function create(Request $request){
        return view('backend.expenses-prepaid-allocation.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $data = $request->all();
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        $data['description'] = $request->voucher_description;
        $data['company_id'] = session('current_company');
        $data['created_by'] = session('created_by');
        $data['status'] = "DRAFT";
        $voucher = ExpensesPrepaidAllocation::create($data);
        for ($i=0; $i < count($request->e_p) ; $i++) { 
            $expenses_prepaid = ExpensesPrepaid::where('id', $request->e_p[$i])->first(); 
            $expenses_prepaid->residual_time = $expenses_prepaid->residual_time - 1;
            $expenses_prepaid->save();
        }
        for ($i=0; $i < count($request->e_p) ; $i++) { 
            $expenses_prepaid_allocation_cost = new ExpensesPrepaidAllocationCost();
            $expenses_prepaid_allocation_cost->voucher_id = $voucher->id;
            $expenses_prepaid_allocation_cost->expenses_prepaid_id = $request->e_p[$i];
            $expenses_prepaid_allocation_cost->department_id = $request->department_id[$i];
            $expenses_prepaid_allocation_cost->allocation_rate = $request->allocation_rate[$i];
            $expenses_prepaid_allocation_cost->money_amount = $request->money_amount[$i];
            $expenses_prepaid_allocation_cost->expense_account = $request->expense_account[$i];
            $expenses_prepaid_allocation_cost->cost_item = $request->cost_item[$i];
            $expenses_prepaid_allocation_cost->save();
        }
        for ($i=0; $i < count($request->debit_account) ; $i++) { 
            $accounting = new Accounting();
            $accounting->expenses_prepaid_allocation_voucher_id = $voucher->id;
            $accounting->description = $request->description[$i];
            $accounting->debit_account = $request->debit_account[$i];
            $accounting->credit_account = $request->credit_account[$i];
            $accounting->amount = $request->amount[$i];
            $accounting->debit_object_id = $request->debit_object_id[$i];
            $accounting->credit_object_id = $request->credit_object_id[$i];
            $accounting->debit_object_type = $request->debit_object_type[$i];
            $accounting->credit_object_type = $request->credit_object_type[$i];
            $accounting->cost_item = $request->cost_item[$i];
            $accounting->save();
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.expenses-prepaid-allocation.index');
    }
    public function edit(Request $request, $id){
        $voucher = ExpensesPrepaidAllocation::find(intval($id));
        $expenses_prepaid_id = ExpensesPrepaidAllocationCost::where('voucher_id', $id)->pluck('expenses_prepaid_id')->toArray();
        $expenses_prepaid = ExpensesPrepaid::whereIn('id', $expenses_prepaid_id)->get();
        $accounting = Accounting::where('expenses_prepaid_allocation_voucher_id', $id)->get();
        return view('backend.expenses-prepaid-allocation.edit', compact('voucher', 'expenses_prepaid', 'accounting'));
    }
    public function update(Request $request, $id){
        // dd($request->all());
        $voucher = ExpensesPrepaidAllocation::find(intval($id));
        $data = $request->all();
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        $data['description'] = $request->voucher_description;
        if (is_null($voucher)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.expenses-prepaid-allocation.index');
        }
        $voucher->update($data);
        for ($i=0; $i < count($request->e_p) ; $i++) { 
            $expenses_prepaid_allocation_cost = ExpensesPrepaidAllocationCost::find($request->allo_info_id[$i]); 
            $expenses_prepaid_allocation_cost->voucher_id = $voucher->id;
            $expenses_prepaid_allocation_cost->expenses_prepaid_id = $request->e_p[$i];
            $expenses_prepaid_allocation_cost->department_id = $request->department_id[$i];
            $expenses_prepaid_allocation_cost->allocation_rate = $request->allocation_rate[$i];
            $expenses_prepaid_allocation_cost->money_amount = $request->money_amount[$i];
            $expenses_prepaid_allocation_cost->expense_account = $request->expense_account[$i];
            $expenses_prepaid_allocation_cost->cost_item = $request->cost_item[$i];
            $expenses_prepaid_allocation_cost->update();
        }

        $accounting = Accounting::where('expenses_prepaid_allocation_voucher_id', $id)->delete();
        for ($i=0; $i < count($request->debit_account) ; $i++) { 
            $accounting = new Accounting();
            $accounting->expenses_prepaid_allocation_voucher_id = $voucher->id;
            $accounting->description = $request->description[$i];
            $accounting->debit_account = $request->debit_account[$i];
            $accounting->credit_account = $request->credit_account[$i];
            $accounting->amount = $request->amount[$i];
            $accounting->debit_object_id = $request->debit_object_id[$i];
            $accounting->credit_object_id = $request->credit_object_id[$i];
            $accounting->debit_object_type = $request->debit_object_type[$i];
            $accounting->credit_object_type = $request->credit_object_type[$i];
            $accounting->cost_item = $request->cost_item[$i];
            $accounting->save();
        }

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.expenses-prepaid-allocation.index');
    }
    public function destroy(Request $request, $id){
    
        $voucher = ExpensesPrepaidAllocation::find($id);
        $allocation_cost = ExpensesPrepaidAllocationCost::where('voucher_id', $id);
        $accounting = Accounting::where('expenses_prepaid_allocation_voucher_id', $id);
        $check = ExpensesPrepaidAllocation::where('company_id', session('current_company'))->where('month', '>', $voucher->month)->where('year', $voucher->year)->orWhere('year', '>', $voucher->year)->get();
        if (is_null($voucher)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        if (count($check) > 0) {
            Session::flash('message', trans('fixed_asset_depreciation.delete'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $allo_cost = ExpensesPrepaidAllocationCost::where('voucher_id', $id)->distinct()->get(['expenses_prepaid_id']);
        foreach ($allo_cost as $key => $value) {
            $expenses_prepaid = ExpensesPrepaid::where('id', $value->expenses_prepaid_id)->first();
            $expenses_prepaid->residual_time = $expenses_prepaid->residual_time + 1;
            // dd($fixed_asset->residual_use_time);
            $expenses_prepaid->update();
        }
        $voucher->delete();
        $allocation_cost->delete();
        $accounting->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
    public function toSave(Request $request, $id){
        $voucher = FixedAssetDepreciation::find(intval($id));
        if($voucher->status == "DRAFT" && $voucher->created_by == session('created_by')){
            $fixed_assets = FixedAsset::where('company_id', session('current_company'))->where('status',1)->get();
            $partners = Partner::where('company_id', session('current_company'))->where('status', 1)->selectRaw("id, CONCAT('Partner', ' - ', name) as name")->pluck('name')->toArray();
            $departments = Department::where('company_id', session('current_company'))->where('status', 1)->selectRaw("id, CONCAT('Department', ' - ', name) as name")->pluck('name')->toArray();
            $staffs = Staff::where('company_id', session('current_company'))->where('status', 1)->selectRaw("id, CONCAT('Staff', ' - ', name) as name")->pluck('name')->toArray();
            $objects = array_merge($partners, $departments, $staffs);
            $accounting = Accounting::where('depreciation_voucher_id', $id)->get();
            return view('backend.fixed-asset-depreciation.save', compact('voucher', 'fixed_assets', 'partners', 'departments', 'staffs', 'objects', 'accounting'));
        }
        else{
            return redirect()->route('admin.fixed-asset-depreciation.index');
        }
    }
    public function save(Request $request, $id){
        $voucher = FixedAssetDepreciation::find(intval($id));
        $voucher->status = "SAVE";
        $voucher->update();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.fixed-asset-depreciation.index');
    }
    public function getMonthYear(Request $request){
        if ($request->ajax()) {
            $check_created = ExpensesPrepaidAllocation::where('company_id', session('current_company'))->where('month', $request->month)->where('year', $request->year)->get();
            $check_expenses = ExpensesPrepaid::where('company_id', session('current_company'))->where('status', 1)->where('residual_time', '>', 0)->where('voucher_date', '<=', ($request->year . "-" . $request->month . "-31"))->get();
            $allocation = ExpensesPrepaidAllocation::where('company_id', session('current_company'))->get();
            $expenses_prepaid = ExpensesPrepaid::where('company_id', session('current_company'))->where('status',1)->where('residual_time', '>', 0)->where('voucher_date', '<=', ($request->year . "-" . $request->month . "-31"))->with(['expensesPrepaidAllocation'=>function($query){$query->with('department');}])->get();
            if(count($check_created) > 0){
                $response = [
                    'status' => 'created'
                ];
            }
            else if(count($check_expenses) == 0){
                $response = [
                    'status' => 'error'
                ];
            }
            else if(($request->year."-".$request->month) > date('Y-m')){
                $response = [
                    'status' => 'cannot'
                ];
            }
            else {
                $response = [
                    'month' => $request->month,
                    'year' => $request->year,
                    'expenses_prepaid' => $expenses_prepaid,
                    'status' => 'success',
                ]; 
 
            }
            return response($response);
        }
    }
    public function getDebitObject(Request $request){
        if ($request->ajax()) {
            if($request->type == 'DEPARTMENTS'){
                $departments = Department::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'departments' => $departments,
                    'type' => 'DEPARTMENTS'
                ]; 
            }
            else if($request->type == 'PARTNERS'){
                $partners = Partner::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'partners' => $partners,
                    'type' => 'PARTNERS'
                ]; 
            }
            else if($request->type == 'USERS'){
                $users = User::where('company_id', session('current_company'))->get();
                $response = [
                    'users' => $users,
                    'type' => 'USERS'
                ];
            }
            
            return response($response);
        }
    }
    public function getCreditObject(Request $request){
        if ($request->ajax()) {
            if($request->type == 'DEPARTMENTS'){
                $departments = Department::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'departments' => $departments,
                    'type' => 'DEPARTMENTS'
                ]; 
            }
            else if($request->type == 'PARTNERS'){
                $partners = Partner::where('company_id', session('current_company'))->where('status', 1)->get();
                $response = [
                    'partners' => $partners,
                    'type' => 'PARTNERS'
                ]; 
            }
            else if($request->type == 'USERS'){
                $users = User::where('company_id', session('current_company'))->get();
                $response = [
                    'users' => $users,
                    'type' => 'USERS'
                ];
            }
            
            return response($response);
        }
    }
}
