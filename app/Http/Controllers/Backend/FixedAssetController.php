<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAsset;
use App\Models\Account;
use App\Models\CostItem;
use App\Models\FixedAssetAllocationInformation;
use App\Models\FixedAssetAllocationCost;
use Illuminate\Support\Facades\Validator;
use App\Models\Department;
use App\Models\FixedAssetDepreciation;
use App\Models\VoucherCollection;
use App\Models\SaleCostItem;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class FixedAssetController 
{
    public function index(Request $request){
        $query = "1=1"; 
        $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
        $asset_name = $request->input('asset_name');
        $original_price = str_replace( ",","", $request->input('original_price'));
        $use_time = $request->input('use_time');
        $date_range = $request->input('date_range'); 
        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date'); 
        $status = intval($request->input('status', -1));
        if($status <> -1) $query .= " AND status = {$status}";
        if($asset_name) $query .= " AND (asset_name like '%" . $asset_name . "%' )" ;
        if($original_price) $query .= " AND (original_price like $original_price)" ;
        if($use_time) $query .= " AND (use_time like $use_time )" ;
        if($from_date && $to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND voucher_creation_date >= '" . $from_date . "' AND voucher_creation_date <= '" .$to_date. "'";
        } else if( !$from_date && $to_date) {
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND  voucher_creation_date <= '" .$to_date. "'";
        } else if ($from_date && !$to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $query .= " AND  voucher_creation_date >= '" .$from_date. "'";
        }
        if($request->input('use_department')){
            $fixed_assets = FixedAsset::where('id', "")->where('company_id', session('current_company'))->orderBy('voucher_creation_date', 'DESC')->orderBy('voucher_number', 'DESC')->paginate($page_num);
            $a = FixedAssetAllocationInformation::where('department_id', $request->input('use_department'))->get();
            foreach ($a as $key => $value) {
            $b = FixedAsset::whereRaw($query)->where('asset_code', $value->asset_code)->first();
            if($b != null){
                $fixed_assets->push($b);
            }
            }
        }
        else{
            $fixed_assets = FixedAsset::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_creation_date', 'DESC')->orderBy('voucher_number', 'DESC')->paginate($page_num);
        }
        return view('backend.fixed-assets.index', compact('fixed_assets'));
    }
    public function show(Request $request, $id)
    {
        $fixed_asset = FixedAsset::find(intval($id));
        $a = FixedAssetALlocationCost::where('asset_code', $fixed_asset->asset_code)->pluck('voucher_id')->toArray();
        $vouchers = FixedAssetDepreciation::whereIn('id', $a)->get();
        if (is_null($fixed_asset)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.fixed-assets.index');
        }
        return view('backend.fixed-assets.show', compact('fixed_asset', 'vouchers'));
    }
    public function create(){
        $accounts = Account::getOriginalPriceAccounts(session('current_company'));
        $expense_accounts = Account::getExpenseAccounts(session('current_company'));
        $departments = Department::getDepartments(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        $latest_voucher_number = FixedAsset::latest()->pluck('id')->first();
        $str = substr($latest_voucher_number, -7);
        $new_voucher_number = $str + 1;
        if(strlen($new_voucher_number) == 1){
           $voucher_number = "GT000000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 2){
           $voucher_number = "GT00000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 3){
           $voucher_number = "GT0000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 4){
           $voucher_number = "GT000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 5){
           $voucher_number = "GT00" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 6){
           $voucher_number = "GT0" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 7){
           $voucher_number = "GT" . $new_voucher_number;
        }
        return view('backend.fixed-assets.create', compact('accounts', 'departments', 'cost_items', 'expense_accounts', 'voucher_number'));
    }
    public function store(Request $request){
        $validator = Validator::make($data = $request->all(), FixedAsset::rules());
        $validator->setAttributeNames(trans('fixed_assets'));
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        $data['company_id'] = session('current_company');
        $data['original_price'] = str_replace(",", "", $request->original_price);
        $data['depreciation_value'] = str_replace(",", "", $request->depreciation_value);
        $data['accumulated_depreciation'] = str_replace(",", "", $request->accumulated_depreciation);
        $data['monthly_depreciation_value'] = str_replace(",", "", $request->monthly_depreciation_value);
        $data['yearly_depreciation_value'] = str_replace(",", "", $request->yearly_depreciation_value);
        $data['residual_value'] = str_replace(",", "", $request->residual_value);
        if($request->accumulated_depreciation != 0){
            $data['residual_use_time'] = round($request->use_time - ($request->accumulated_depreciation/str_replace(",", "", $request->monthly_depreciation_value)));
        }
        else{
            $data['residual_use_time'] = $request->use_time;
        }
        $data['created_by'] = session('created_by');
        $request->merge(['status' => intval($request->input('status', 0))]);
        if($request->accumulated_depreciation == ""){
            $data['accumulated_depreciation'] = "0";
        }
        else{
            $data['accumulated_depreciation'] = $request->accumulated_depreciation;
        }
        $voucher_creation_date = str_replace("/", "-", $request->voucher_creation_date);
        $data['voucher_creation_date'] = date('Y-m-d', strtotime($voucher_creation_date));
        $begin_use_date = str_replace("/", "-", $request->begin_use_date);
        $data['begin_use_date'] = date('Y-m-d', strtotime($begin_use_date));
        $fixed_asset = FixedAsset::create($data);
        for ($i=0; $i < count($request->use_department) ; $i++) { 
            $allocation_information = new FixedAssetAllocationInformation();
            $allocation_information->asset_code = $request->asset_code;
            $allocation_information->department_id = $request->use_department[$i];
            $allocation_information->allocation_rate = $request->allocation_rate[$i];
            $allocation_information->expense_account = $request->expense_account[$i];
            $allocation_information->cost_item = $request->cost_item[$i];
            $allocation_information->save();
        }
        for ($i=0; $i < count($request->accounting_id) ; $i++) { 
            $voucher_collection = new VoucherCollection();
            $voucher_collection->fixed_asset_id = $fixed_asset->id;
            $voucher_collection->accounting_id = $request->accounting_id[$i];
            $voucher_collection->save();
         }
        
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.fixed-assets.index');
    }

    public function edit(Request $request, $id){
        
        $fixed_asset = FixedAsset::find(intval($id));
        $voucher_collection = VoucherCollection::where('fixed_asset_id', $id)->get();
        $asset_code = FixedAsset::where('id', $id)->first()->asset_code;
        $use = FixedAssetAllocationInformation::where('asset_code', $asset_code )->selectRaw("id, department_id, allocation_rate, expense_account, cost_item")->get();
        $accounts = Account::getOriginalPriceAccounts(session('current_company'));
        $expense_accounts = Account::getExpenseAccounts(session('current_company'));
        $departments = Department::getDepartments(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        if (is_null($fixed_asset)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.fixed-assets.index');
        }
        else{
            return view('backend.fixed-assets.edit', compact('fixed_asset', 'accounts', 'cost_items', 'departments', 'expense_accounts', 'use', 'voucher_collection'));
        }
       
    }
    public function update(Request $request, $id)
    {   
        $fixed_asset = FixedAsset::find(intval($id));
        $request->merge(['status' => $request->input('status', 1)]);
        if (is_null($fixed_asset)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.fixed-assets.index');
        }
        
        $validator = \Validator::make($data = $request->all(), FixedAsset::rules(intval($id)));
        $validator->setAttributeNames(trans('fixed_assets'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $data['voucher_creation_date'] = date("Y-m-d");
        $data['company_id'] = session('current_company');
        $data['original_price'] = str_replace(",", "", $request->original_price);
        $data['depreciation_value'] = str_replace(",", "", $request->depreciation_value);
        $data['accumulated_depreciation'] = str_replace(",", "", $request->accumulated_depreciation);
        $data['monthly_depreciation_value'] = str_replace(",", "", $request->monthly_depreciation_value);
        $data['yearly_depreciation_value'] = str_replace(",", "", $request->yearly_depreciation_value);
        $data['residual_value'] = str_replace(",", "", $request->residual_value);
        if($request->accumulated_depreciation != 0){
            $data['residual_use_time'] = round($request->use_time - ($request->accumulated_depreciation/str_replace(",", "", $request->monthly_depreciation_value)));
        }
        else{
            $data['residual_use_time'] = $request->use_time;
        }
        $data['created_by'] = session('created_by');
        if($request->accumulated_depreciation == ""){
            $data['accumulated_depreciation'] = "0";
        }
        else{
            $data['accumulated_depreciation'] = $request->accumulated_depreciation;
        }
        $voucher_creation_date = str_replace("/", "-", $request->voucher_creation_date);
        $data['voucher_creation_date'] = date('Y-m-d', strtotime($voucher_creation_date));
        $begin_use_date = str_replace("/", "-", $request->begin_use_date);
        $data['begin_use_date'] = date('Y-m-d', strtotime($begin_use_date));

        if($request->voucher_collection_id != ''){
            VoucherCollection::whereNotIn("id", $request->voucher_collection_id)->where('fixed_asset_id', $id)->delete();
         }
         if($request->voucher_collection_id == ''){
            VoucherCollection::where('fixed_asset_id', $id)->delete();
         }
         for ($i=0; $i < count($request->accounting_id) ; $i++) { 
            $voucher_collection = VoucherCollection::find($request->voucher_collection_id[$i]); 
            if($voucher_collection == null){
               $new = new VoucherCollection();
               $new->fixed_asset_id = $fixed_asset->id;
               $new->accounting_id = $request->accounting_id[$i];
               $new->save();
            }
            else{
            
               $voucher_collection->accounting_id = $request->accounting_id[$i];
               $voucher_collection->update();
            }  
         }
        
        $fixed_asset->update($data);
        if($request->allocation_info_id != ''){
            $allo_del = FixedAssetAllocationInformation::whereNotIn("id", $request->allocation_info_id)->where('asset_code', $request->asset_code)->delete();
        }
        for ($i=0; $i < count($request->use_department) ; $i++) { 
            $allocation_information = FixedAssetAllocationInformation::find($request->allocation_info_id[$i]); 
            if($allocation_information == null){
                $new = new FixedAssetAllocationInformation();
                $new->asset_code = $request->asset_code;
                $new->department_id = $request->use_department[$i];
                $new->allocation_rate = $request->allocation_rate[$i];
                $new->expense_account = $request->expense_account[$i];
                $new->cost_item = $request->cost_item[$i];
                $new->save();
            }
            else{
                $allocation_information->department_id = $request->use_department[$i];
                $allocation_information->allocation_rate = $request->allocation_rate[$i];
                $allocation_information->expense_account = $request->expense_account[$i];
                $allocation_information->cost_item = $request->cost_item[$i];
                $allocation_information->update();
            }  
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.fixed-assets.index');
    }

    public function storeCostItem(Request $request){
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $validator = \Validator::make($data = $request->only(['new_cost_item', 'description', 'interpretation']), CostItem::rules());
                if($validator->fails()) throw new \Exception($validator->messages()->first());
                $data = $request->all();
                $data['cost_item'] = $request->new_cost_item;
                $data['status'] = 1;
                $data['created_by'] = session('created_by');
                $data['company_id'] = session('current_company');
                $cost_item = CostItem::create($data);
                $statusCode = 200;
                $response['message'] = trans('system.success');
                $response['cost_item'] = $cost_item->cost_item;
                $response['id'] = $cost_item->id;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function destroy(Request $request, $id)
    {
        $fixed_asset = FixedAsset::find($id);
        $allocation_information = FixedAssetAllocationInformation::where('asset_code', $fixed_asset->asset_code);
        if (is_null($fixed_asset)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        $fixed_asset->delete();
        $allocation_information->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
    }
}
