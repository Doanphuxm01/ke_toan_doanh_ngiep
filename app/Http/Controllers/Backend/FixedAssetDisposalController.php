<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAsset;
use App\Models\FixedAssetDisposal;
use App\Models\Account;
use App\Models\CostItem;
use App\Models\Accounting;
use App\Models\FixedAssetAllocationCost;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FixedAssetDisposalController 
{
    public function index(Request $request){
      $query = "1=1"; 
      $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
      $from_date = $request->input('from_date'); 
      $to_date = $request->input('to_date'); 
      $status = $request->input('status'); 
      if($status) $query .= " AND (status like '" . $status . "' )" ;
      if($from_date && $to_date) {
          $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
          $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
          $query .= " AND voucher_date >= '" . $from_date . "' AND voucher_date <= '" .$to_date. "'";
      } else if( !$from_date && $to_date) {
          $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
          $query .= " AND  voucher_date <= '" .$to_date. "'";
      } else if ($from_date && !$to_date) {
          $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
          $query .= " AND  voucher_date >= '" .$from_date. "'";
      }
      $disposals = FixedAssetDisposal::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->paginate($page_num);
      return view('backend.fixed-asset-disposal.index', compact('disposals'));
    }
    public function show(Request $request, $id){
        $disposal = FixedAssetDisposal::find(intval($id));
        $asset_info = FixedAsset::where('asset_code', $disposal->asset_code)->first();
        $accounting = Accounting::where('disposal_voucher_id', $id)->get();
        $allo_cost = FixedAssetAllocationCost::where('asset_code', $disposal->asset_code)->get();
        $accumulated_depreciation = 0;
        foreach ($allo_cost as $key => $value) {
            $accumulated_depreciation += $value->money_amount;
        }
        return view('backend.fixed-asset-disposal.show', compact('disposal', 'asset_info', 'accounting', 'accumulated_depreciation'));
    }
    public function create(){
        $asset_codes = [];
        $fixed_assets = FixedAsset::where('company_id', session('current_company'))->where('status', 1)->get();
        
        foreach ($fixed_assets as $fixed_asset) {  
            $asset_codes[$fixed_asset->asset_code] = $fixed_asset->asset_code;
        }
        $accounts = Account::getActiveAccounts(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        $disposal_accounts = Account::getExpenseAccounts(session('current_company'));
        $latest_voucher_number = FixedAssetDisposal::latest()->pluck('id')->first();
        $str = substr($latest_voucher_number, -7);
        $new_voucher_number = $str + 1;
        if(strlen($new_voucher_number) == 1){
           $voucher_number = "GGTSCĐ000000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 2){
           $voucher_number = "GGTSCĐ00000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 3){
           $voucher_number = "GGTSCĐ0000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 4){
           $voucher_number = "GGTSCĐ000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 5){
           $voucher_number = "GGTSCĐ00" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 6){
           $voucher_number = "GGTSCĐ0" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 7){
           $voucher_number = "GGTSCĐ" . $new_voucher_number;
        }
        return view('backend.fixed-asset-disposal.create', compact('asset_codes', 'accounts', 'disposal_accounts', 'voucher_number', 'cost_items'));
    }
    public function store(Request $request){
      $data = $request->all();
      // dd($data);
      $voucher_date = str_replace("/", "-", $request->voucher_date);
      $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
      $data['created_by'] = session('created_by');
      $data['company_id'] = session('current_company');
      $data['status'] = "DRAFT";
      $voucher = FixedAssetDisposal::create($data);
      for ($i=0; $i < count($request->description) ; $i++) { 
         $accounting = new Accounting();
         $accounting->disposal_voucher_id = $voucher->id;
         $accounting->description = $request->description[$i];
         $accounting->debit_account = $request->debit_account[$i];
         $accounting->credit_account = $request->credit_account[$i];
         $accounting->amount = $request->amount[$i];
         $accounting->debit_object_id = $request->debit_object_id[$i];
         $accounting->credit_object_id = $request->credit_object_id[$i];
         $accounting->debit_object_type = $request->debit_object_type[$i];
         $accounting->credit_object_type = $request->credit_object_type[$i];
         $accounting->cost_item = $request->cost_item[$i];
         $accounting->save();
      }
      $fixed_asset = FixedAsset::where('asset_code', $request->asset_code)->first();
      $fixed_asset->status = 0;
      $fixed_asset->update();
      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');
      return redirect()->route('admin.fixed-asset-disposal.index');
    }
    public function edit(Request $request, $id){
        $disposal = FixedAssetDisposal::find(intval($id));
        if (is_null($disposal)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.fixed-asset-disposal.index');
        }
        $asset_codes = [];
        $fixed_asset = FixedAsset::where('asset_code', $disposal->asset_code)->get();
        foreach ($fixed_asset as $abc) {  
            $asset_codes[$abc->asset_code] = $abc->asset_code;
        }
        $fixed_assets = FixedAsset::where('company_id', session('current_company'))->where('status', 1)->get();
        foreach ($fixed_assets as $fixed_asset) {  
            $asset_codes[$fixed_asset->asset_code] = $fixed_asset->asset_code;
        }
        $accounts = Account::getActiveAccounts(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        $disposal_accounts = Account::getExpenseAccounts(session('current_company'));
        
        $asset_info = FixedAsset::where('asset_code', $disposal->asset_code)->first();
        $accounting = Accounting::where('disposal_voucher_id', $id)->get();
        $allo_cost = FixedAssetAllocationCost::where('asset_code', $disposal->asset_code)->get();
        $accumulated_depreciation = 0;
        foreach ($allo_cost as $key => $value) {
            $accumulated_depreciation += $value->money_amount;
        }
        return view('backend.fixed-asset-disposal.edit', compact('disposal', 'asset_info', 'accounting', 'asset_codes', 'accounts', 'cost_items', 'disposal_accounts', 'accumulated_depreciation'));
    }
    public function update(Request $request, $id){
       
        $disposal = FixedAssetDisposal::find(intval($id));
        $data = $request->all();
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        if (is_null($disposal)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.fixed-asset-depreciation.index');
        }
        $disposal->update($data);
        $accounting = Accounting::where('disposal_voucher_id', $id)->delete();
        for ($i=0; $i < count($request->debit_account) ; $i++) { 
            $accounting = new Accounting();
            $accounting->disposal_voucher_id = $disposal->id;
            $accounting->description = $request->description[$i];
            $accounting->debit_account = $request->debit_account[$i];
            $accounting->credit_account = $request->credit_account[$i];
            $accounting->amount = $request->amount[$i];
            $accounting->debit_object_id = $request->debit_object_id[$i];
            $accounting->credit_object_id = $request->credit_object_id[$i];
            $accounting->debit_object_type = $request->debit_object_type[$i];
            $accounting->credit_object_type = $request->credit_object_type[$i];
            $accounting->cost_item = $request->cost_item[$i];
            $accounting->save();
        }
        $fixed_asset = FixedAsset::where('asset_code', $disposal->asset_code)->first();
        $fixed_asset->status = 0;
        $fixed_asset->update();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.fixed-asset-disposal.index');
    }
    public function destroy(Request $request, $id){
    
      $disposal = FixedAssetDisposal::find($id);
      $accounting = Accounting::where('disposal_voucher_id', $id);
      if (is_null($disposal)) {
          Session::flash('message', trans('system.have_an_error'));
          Session::flash('alert-class', 'danger');
          return back();
      }
      $fixed_asset = FixedAsset::where('asset_code', $disposal->asset_code)->first();
      $fixed_asset->status = "1";
      $fixed_asset->update();
      $disposal->delete();
      $accounting->delete();

      Session::flash('message', trans('system.success'));
      Session::flash('alert-class', 'success');
      return back();
  }
    public function getAssetInfo(Request $request){
        if ($request->ajax()) {
            $fixed_asset = FixedAsset::where('asset_code', $request->asset_code)->first();
            $allo_cost = FixedAssetAllocationCost::where('asset_code', $request->asset_code)->get();
            $accumulated_depreciation = 0;
            foreach ($allo_cost as $key => $value) {
                $accumulated_depreciation += $value->money_amount;
            }
            $response = [
                'fixed_asset' => $fixed_asset,
                'accumulated_depreciation' => $accumulated_depreciation,
            ]; 
            return response($response);
        }
    }

}
