<?php

namespace App\Http\Controllers\Backend;

use App\Elibs\eView;
use App\Models\Account;
use App\Models\BranchCode;
use App\Models\InputInvoice;
use App\Models\InvoicesCode;
use App\Models\Partner;
use App\Models\SaleCostItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class InputInvoiceController extends Controller
{
    protected $account;
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->_list();
        }
    }

    public function _list()
    {
        $query = "1=1";
        $page_num       = intval(request()->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval(request()->input('status', -1));
        $code           = request()->input('code');
        if($code) $query .= " AND (code like '%" . $code . "%' OR description like '%" . $code . "%' OR sale_account_code like '%" . $code . "%' OR cost_account_code like '%" . $code . "%' OR prov_account_code like '%" . $code . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        $saleCostItems = SaleCostItem::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        return view('backend.input-invoices.index', compact('saleCostItems', 'branchCodes'));
    }

    public function create()
    {
        $request = request();
        $tpl = [];
        $where = [];
        $where['status'] = 1;
        $saleCostItems = SaleCostItem::where($where)->get()->toArray();
        $tpl['saleCostItems'] = $saleCostItems;
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        $tpl['branchCodes'] = $branchCodes;
        $sci = SaleCostItem::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        $tpl['sci'] = $sci;
        return view('backend.input-invoices.create',$tpl);
    }
    public function show(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.accounting.invoices');
        }
        $currentCodes = [
            $saleCostItem->sale_account_code,
            $saleCostItem->cost_account_code,
            $saleCostItem->prov_account_code
        ];
        $currentCodes = implode("','", $currentCodes);
        $branchCodes = BranchCode::whereRaw("(status=1 OR code ='" . $saleCostItem->branch_code . "') AND company_id=" . session('current_company'))->pluck('code', 'code')->toArray();
        $accounts = Account::whereRaw("(follow=1 OR (follow=0 AND code IN('" . $currentCodes . "'))) AND company_id=" . session('current_company'))->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        return view('backend.input-invoices.show', compact('saleCostItem', 'branchCodes', 'accounts'));
    }

    public function edit(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.accounting.invoices');
        }
        $currentCodes = [
            $saleCostItem->sale_account_code,
            $saleCostItem->cost_account_code,
            $saleCostItem->prov_account_code
        ];
        $currentCodes = implode("','", $currentCodes);
        $branchCodes = BranchCode::whereRaw("(status=1 OR code ='" . $saleCostItem->branch_code . "') AND company_id=" . session('current_company'))->pluck('code', 'code')->toArray();
        $accounts = Account::whereRaw("(follow=1 OR (follow=0 AND code IN('" . $currentCodes . "'))) AND company_id=" . session('current_company'))->selectRaw("code, CONCAT(code, ' - ', name) as name")->pluck('name', 'code')->toArray();
        return view('backend.input-invoices.edit', compact('saleCostItem', 'branchCodes', 'accounts'));
    }

    public function store()
    {
        $user = auth()->user();
        $year_today = date('Y');
        $invoice_list = Request::capture()->input('obj', []);
        $obj = Request::capture()->input('obj', []);
        if(is_array($invoice_list['hoach_toan'])){
           unset($invoice_list['hoach_toan']);
        }
        dd($invoice_list['voucher_date'],convertDateToTimestamp($invoice_list['voucher_date']),convertTimestampToDate($invoice_list['voucher_date']));
        $year_user = date('Y', strtotime($invoice_list['voucher_date']));
        if($year_user == '1970'){
            return eView::getInstance()->getJsonError('xin lỗi quý khách số năm của Voucher Date hiện tại nhập vào đang khác so với năm nay '.$year_today.'');
        }
        $partner_id = Partner::whereId($invoice_list['partner_code'])->first();
        if(!isset($partner_id)){
            return eView::getInstance()->getJsonError('xin loi quy khach chung toi chua tim thay du lieu Partner');
        }
        if(empty($invoice_list['voucher_no'])){
            return eView::getInstance()->getJsonError('Dữ liệu Voucher No không được để chống');
        }
        if(empty($invoice_list['currency_rate'])){
            return eView::getInstance()->getJsonError('Dữ liệu Rate không được để chống');
        }
        if(empty($invoice_list['currency'])){
            return eView::getInstance()->getJsonError('Dữ liệu Currency không được để chống');
        }
        if(empty($invoice_list['description'])){
            return eView::getInstance()->getJsonError('Dữ liệu Description không được để chống');
        }
        $objToSave = [
            "partner_id" => $invoice_list['partner_code'],
            "voucher_date" => $invoice_list['voucher_date'],
            "voucher_no" => $invoice_list['partner_code'],
            "description" => $invoice_list['partner_code'],
            "currency" => $invoice_list['partner_code'],
            "invoice_no" => $invoice_list['partner_code'],
            "invoice_date" => $invoice_list['partner_code'],
            "due_payment" => $invoice_list['partner_code'],
            "currency_rate" => $invoice_list['partner_code'],
        ];

        dd($invoice_list);
        dd(eView::getInstance()->setMsgWarningAppendJs('xin loi quy khach chung toi chua tim thay du lieu Partner'));
        if(isset($partner_id)){
            return eView::getInstance()->setMsgWarning('xin loi quy khach chung toi chua tim thay du lieu Partner');
        }
        dd($invoid_list);

        dd($invoid_list);
        dd(InvoicesCode::insertGetId($invoid_list));
        $type_planning = [];
        foreach ($obj['hoach_toan'] as $_key => $nbs){
            dd($nbs);
            $type_planning[] = [
                'type' => $nbs['type'],
                'mawb' => $nbs['hawb'],
                'hawb' => $nbs['hawb']
            ];
        }
        dd($type_planning);
        return redirect()->route('admin.accounting.invoices');
    }


    public function update(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.accounting.invoices');
        }
        $request->merge(['status' => intval($request->input('status', 0))]);
        $validator = \Validator::make($data = $request->all(), SaleCostItem::rules(intval($id)));
        $validator->setAttributeNames(trans('input_invoices'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $saleCostItem->update($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.accounting.invoices');
    }

    public function destroy(Request $request, $id)
    {
        $saleCostItem = SaleCostItem::find(intval($id));
        if (is_null($saleCostItem)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.accounting.invoices');
        }
        // check cac rang buoc #
        $saleCostItem->deleted_by = $request->user()->id;
        $saleCostItem->save();
        $saleCostItem->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.accounting.invoices');
    }
    public function addRow(){
        $length = request('length');
        $tpl = [];
        if(!empty($length)){
            $count = $length;
            $where = [];
            $where['status'] = 1;
            $saleCostItems = SaleCostItem::where($where)->get()->toArray();
            $tpl['saleCostItems'] = $saleCostItems;
            $tpl['count'] = $count;
            $view = view("backend.input-invoices.include.after-data-2",$tpl)->render();
            return response()->json($view);
        }
    }
    public function append_thue(){
        $length = request('length');

        $tpl = [];
        if(!empty($length)){
            $count = $length;
            $where = [];
            $where['status'] = 1;
            $saleCostItems = SaleCostItem::where($where)->get()->toArray();
            $tpl['saleCostItems'] = $saleCostItems;
            $tpl['count'] = $count;
            $view = view("backend.input-invoices.include.after-data-thue",$tpl)->render();
            return response()->json($view);
        }
    }
}
