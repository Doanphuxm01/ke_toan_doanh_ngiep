<?php

namespace App\Http\Controllers\Backend;

use App\User;
use App\Product;
use App\Customer;
use App\AgencyPrice;
use App\AgencyPriceLog;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Import\AgencyPricesImport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AgencyPricesController extends Controller
{
    public function index(Request $request)
    {
        $query      = "1=1";
        $type       = trim($request->input('type'));
        $name       = trim($request->input('name'));
        $status     = intval($request->input('status', -1));
        $page_num   = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $time_range = explode(' - ', $request->input('date_range'));
        if (isset($time_range[0]) && isset($time_range[1])) {
            $query .= " AND updated_at >= '" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $time_range[0]))) . "' AND  updated_at <= '" . date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $time_range[1]))) . "'";
        }

        if ($status <> -1) {
            $query .= " AND status = {$status}";
        }
        $prices = AgencyPrice::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        return view('backend.agency-prices.index', compact('prices', 'types', 'categories', 'rawCategories'));
    }

    public function create(Request $request)
    {
        $product = [];
        if (old('product')) {
            $p = Product::find(old('product'));
            if (!is_null($p)) {
                $product = [$p->id => $p->sku . ' - ' . $p->name . ' - ' . \App\Helper\HString::currencyFormat($p->price_ipo) . 'đ'];
            }
        }

        return view('backend.agency-prices.create', compact('product'));
    }

    public function edit(Request $request, $id)
    {
        $price = AgencyPrice::find(intval($id));
        if (is_null($price)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.agency-prices.index');
        }
        $p = Product::find($price->product_id);
        if (is_null($p)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.agency-prices.index');
        }
        $product = [$p->id => $p->sku . ' - ' . $p->name . ' - ' . \App\Helper\HString::currencyFormat($p->price_ipo) . 'đ'];

        return view('backend.agency-prices.edit', compact('price', 'product'));
    }

    public function show(Request $request, $id)
    {
    }

    public function store(Request $request)
    {
        $request->merge(['status' => $request->input('status', 0)]);
        $data = $request->only(['greater_than_equal', 'status', 'product', 'less_than', 'price_ipo']);
        $rules = [
            'product'   => 'required|integer',
        ];
        $validator = \Validator::make($data, $rules);
        $validator->setAttributeNames(trans('agency_prices'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        $product = Product::where('status', 1)->where('id', $data['product'])->first();
        if (is_null($product)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        }
        if (!isset($data['greater_than_equal']) || !isset($data['less_than']) || !isset($data['price_ipo']) || count($data['greater_than_equal']) <> count($data['less_than']) || count($data['greater_than_equal']) <> count($data['price_ipo'])) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        }
        $rules = [
            'greater_than_equal'    => 'required|array',
            'greater_than_equal.*'  => 'required|integer|min:0|max:999',
            'less_than'    => 'required|array',
            'less_than.*'  => 'required|integer|min:0|max:999',
            'price_ipo'   => 'required|array',
            'price_ipo.*' => 'required|integer|min:0|max:99999999',
        ];
        $validator = \Validator::make($data, $rules);
        $validator->setAttributeNames(trans('agency_prices'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        // resort
        $l1 = implode('', $data['greater_than_equal']);
        sort($data['greater_than_equal']);
        $l2 = implode('', $data['greater_than_equal']);
        if ($l1 <> $l2) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', 'Các dòng cần sắp xếp theo thứ tự tăng dần từ trên xuống');
            return back()->withErrors($errors)->withInput();
        }
        $agencyPriceLists = [];
        for ($i=0; $i < count($data['greater_than_equal']); $i++) {
            if ($data['greater_than_equal'][$i] > $data['less_than'][$i]) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Khoảng tại dòng số ' . ($i+1) . ' không hợp lệ');
                return back()->withErrors($errors)->withInput();
            }
            if (isset($data['greater_than_equal'][$i+1]) && ($data['greater_than_equal'][$i+1] < $data['less_than'][$i])) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Dữ liệu nối tiếp dòng số ' . ($i+1) . ' và ' . ($i+2) . ' không hợp lệ');
                return back()->withErrors($errors)->withInput();
            }

            if ($data['price_ipo'][$i]%100) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Giá sỉ dòng số ' . ($i+1) . ' phải làm tròn trăm đồng');
                return back()->withErrors($errors)->withInput();
            }
            array_push($agencyPriceLists, [
                'less_than'         => $data['less_than'][$i],
                'greater_than_equal'=> $data['greater_than_equal'][$i],
                'price_ipo'         => $data['price_ipo'][$i],
                // 'status'        => $data['status'],
                'created_by'    => $request->user()->id,
                'created_at'    => date("Y-m-d H:i:s"),
                'updated_at'    => date("Y-m-d H:i:s"),
            ]);
        }
        $agencyPrice = AgencyPrice::create([
            'product_id'    => $product->id,
            'status'        => $data['status'],
            'created_by'    => $request->user()->id,
        ]);
        for ($i=0; $i < count($agencyPriceLists); $i++) {
            $agencyPriceLists[$i]['agency_price_id'] = $agencyPrice->id;
        }
        $agencyPrice->list()->insert($agencyPriceLists);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.agency-prices.index');
    }

    public function update(Request $request, $id)
    {
        $request->merge(['status' => $request->input('status', 0)]);
        $price = AgencyPrice::find(intval($id));
        if (is_null($price)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.agency-prices.index');
        }
        $data = $request->only(['greater_than_equal', 'status', 'less_than', 'price_ipo']);
        $product = Product::where('status', 1)->where('id', $price->product_id)->first();
        if (is_null($product)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        }
        if (!isset($data['greater_than_equal']) || !isset($data['less_than']) || !isset($data['price_ipo']) || count($data['greater_than_equal']) <> count($data['less_than']) || count($data['greater_than_equal']) <> count($data['price_ipo'])) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back()->withInput();
        }
        $rules = [
            'greater_than_equal'    => 'required|array',
            'greater_than_equal.*'  => 'required|integer|min:0|max:999',
            'less_than'    => 'required|array',
            'less_than.*'  => 'required|integer|min:0|max:999',
            'price_ipo'   => 'required|array',
            'price_ipo.*' => 'required|integer|min:0|max:99999999',
        ];
        $validator = \Validator::make($data, $rules);
        $validator->setAttributeNames(trans('agency_prices'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        // resort
        $l1 = implode('', $data['greater_than_equal']);
        sort($data['greater_than_equal']);
        $l2 = implode('', $data['greater_than_equal']);
        if ($l1 <> $l2) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', 'Các dòng cần sắp xếp theo thứ tự tăng dần từ trên xuống');
            return back()->withErrors($errors)->withInput();
        }
        $agencyPriceLists = [];
        $newData = "";
        $oldData = "";
        for ($i=0; $i < count($data['greater_than_equal']); $i++) {
            if ($data['greater_than_equal'][$i] > $data['less_than'][$i]) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Khoảng tại dòng số ' . ($i+1) . ' không hợp lệ');
                return back()->withErrors($errors)->withInput();
            }
            if (isset($data['greater_than_equal'][$i+1]) && ($data['greater_than_equal'][$i+1] < $data['less_than'][$i])) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Dữ liệu nối tiếp dòng số ' . ($i+1) . ' và ' . ($i+2) . ' không hợp lệ');
                return back()->withErrors($errors)->withInput();
            }

            if ($data['price_ipo'][$i]%100) {
                $errors = new \Illuminate\Support\MessageBag;
                $errors->add('editError', 'Giá sỉ dòng số ' . ($i+1) . ' phải làm tròn trăm đồng');
                return back()->withErrors($errors)->withInput();
            }
            array_push($agencyPriceLists, [
                'agency_price_id'   => $price->id,
                'less_than'         => $data['less_than'][$i],
                'greater_than_equal'=> $data['greater_than_equal'][$i],
                'price_ipo'         => $data['price_ipo'][$i],
                // 'status'        => $data['status'],
                'created_by'    => $request->user()->id,
                'created_at'    => date("Y-m-d H:i:s"),
                'updated_at'    => date("Y-m-d H:i:s"),
            ]);
            $newData .= ($data['greater_than_equal'][$i] . "-" . $data['less_than'][$i] . ":" . $data['price_ipo'][$i] . ".");
        }
        // kiem tra du lieu cu
        $list = $price->list()->get();
        foreach ($list as $l) {
            $oldData .= ($l->greater_than_equal . "-" . $l->less_than . ":" . $l->price_ipo . ".");
        }
        if ($newData <> $oldData) {
            AgencyPriceLog::create([
                'agency_price_id'=> $price->id,
                'data_new'  => $newData,
                'data_old'  => $oldData,
                'field'     => 'price_list',
                'action_by' => $request->user()->id,
                'action_at' => date("Y-m-d H:i:s"),
            ]);
        }
        $price->list()->delete();
        $price->list()->insert($agencyPriceLists);
        if ($data['status'] <> $price->status) {
            AgencyPriceLog::create([
                'agency_price_id'=> $price->id,
                'data_new'  => $data['status'],
                'data_old'  => $price->status,
                'field'     => 'status',
                'action_by' => $request->user()->id,
                'action_at' => date("Y-m-d H:i:s"),
            ]);
        }
        $price->update($data);

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.agency-prices.index');
    }

    public function createBulk(Request $request)
    {
        return view('backend.agency-prices.create_multi');
    }

    public function readBulk(Request $request)
    {
        ini_set('memory_limit', '4096M');
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $file = $request->file;
                switch ($file->getClientOriginalExtension()) {
                    case 'xlsx':
                        $data = \Excel::toArray(new AgencyPricesImport, $file);
                        if ($data) $data = $data[0];
                        if (count($data) == 0 || !isset($data[0][0]) || count($data[0]) < 4) {
                            throw new \Exception("Không được sửa dòng đầu tiên của file mẫu nhập liệu", 1);
                        }
                        if (!isset($data[1][0])) {
                            throw new \Exception("File tải lên không có dữ liệu", 1);
                        }
                        $response['message'] = view('backend.agency-prices.excel', compact('data'))->render();
                        break;
                    default:
                        throw new \Exception("Không hỗ trợ định dạng", 1);
                }
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function saveBulk(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $data = json_decode($request->data, 1);
                if (!is_array($data) && count($data) == 0) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $data = array_column($data, 'data', 'sku');
                // validate sku
                $products = Product::where('status', 1)->whereIn('sku', array_keys($data))->get()->keyBy('sku');
                if (count($products) <> count($data)) {
                    foreach ($data as $sku => $d) {
                        if (!isset($products[$sku])) {
                            $row = explode('.', $d[0]);
                            throw new \Exception("SKU `{$sku}` tại dòng " . $row[0] . " không tìm thấy", 1);
                        }
                    }
                }

                // validate row
                foreach ($data as $sku => $d) {
                    $to = 0;
                    foreach ($d as $r) {
                        $row = explode('.', $r);
                        if (count($row) <> 4) {
                            throw new \Exception("SKU `{$sku}` tại dòng " . $row[0] . " có dữ liệu nhập vào không đúng", 1);
                        }
                        $row[1] = intval($row[1]);
                        $row[2] = intval($row[2]);
                        $row[3] = intval($row[3]);
                        if ($row[1] < 1 || $row[1] > $row[2]) {
                            throw new \Exception("`Lớn hơn hoặc bằng` tại dòng " . $row[0] . " không đúng", 1);
                        }
                        if ($to && $to > $row[1]) {
                            throw new \Exception("SKU `{$sku}` sắp xếp các khoảng tăng dần, lỗi tại dòng " . $row[0] . "", 1);
                        }
                        if ($row[3]%100) {
                            throw new \Exception("SKU `{$sku}` có giá sỉ phải tròn trăm đồng, lỗi tại dòng " . $row[0] . "", 1);
                        }
                        $to = $row[2];
                        // if ($sku == '12152402041WH') {
                        //     dd($to);
                        // }
                    }
                }

                foreach ($data as $sku => $d) {
                    $agencyPrice = AgencyPrice::where('product_id', $products[$sku]->id)->first();
                    $oldData = "";
                    if (is_null($agencyPrice)) {
                        $agencyPrice = AgencyPrice::create([
                            'product_id'    => $products[$sku]->id,
                            'status'        => 1,
                            'created_by'    => $request->user()->id,
                        ]);
                    } else {
                        $list = $agencyPrice->list()->get();
                        foreach ($list as $l) {
                            $oldData .= ($l->greater_than_equal . "-" . $l->less_than . ":" . $l->price_ipo . ".");
                        }
                    }
                    $agencyPriceLists = [];
                    $newData = "";
                    foreach ($d as $r) {
                        $row = explode('.', $r);
                        array_push($agencyPriceLists, [
                            'agency_price_id'   => $agencyPrice->id,
                            'greater_than_equal'=> intval($row[1]),
                            'less_than'         => intval($row[2]),
                            'price_ipo'         => intval($row[3]),
                            // 'status'        => $data['status'],
                            'created_by'    => $request->user()->id,
                            'created_at'    => date("Y-m-d H:i:s"),
                            'updated_at'    => date("Y-m-d H:i:s"),
                        ]);
                        $newData .= (intval($row[1]) . "-" . intval($row[2]) . ":" . intval($row[3]) . ".");
                    }
                    if ($oldData) { // update
                        if ($oldData <> $newData) {
                            $agencyPrice->list()->delete();
                            $agencyPrice->list()->insert($agencyPriceLists);
                            AgencyPriceLog::create([
                                'agency_price_id'=> $agencyPrice->id,
                                'data_new'  => $newData,
                                'data_old'  => $oldData,
                                'field'     => 'price_list',
                                'action_by' => $request->user()->id,
                                'action_at' => date("Y-m-d H:i:s"),
                            ]);
                        } else {
                            // khong lam gi
                        }
                    } else { // them moi
                        $agencyPrice->list()->insert($agencyPriceLists);
                    }
                }
                $response['message'] = trans('system.success');
                Session::flash('message', $response['message']);
                Session::flash('alert-class', 'success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function download(Request $request)
    {
        $file= public_path() . "/assets/media/files/templates/price-list-sample.xlsx";
        $headers = [
            'Content-Type: application/xls',
        ];
        return response()->download($file, 'template-price-list.xlsx', $headers);
    }

    public function timeline(Request $request)
    {
        $response = ['message' => trans('system.have_an_error'), 'data' => ""];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $price = AgencyPrice::find(intval($request->id));
               
                if(is_null($price)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'], 1);
                }
                $product = $price->product()->first();
                
                if(is_null($product)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'], 1);
                }
                $timelines = AgencyPriceLog::where('agency_price_id', $price->id)->orderBy('id', 'DESC')->get();
               
                $response['message'] = trans('system.success');
                $users = User::where('activated', 1)->pluck('fullname', 'id')->toArray();
               
                $response['data'] = view('backend.agency-pri.ajaxTimeline', compact('price', 'timelines', 'users', 'product'))->render();
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
               
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
          
            return response()->json($response, $statusCode);
        }
    }

    public function destroy(Request $request, $id)
    {
        $price = AgencyPrice::find(intval($id));
        if (is_null($price)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.agency-prices.index');
        }
        $price->deleted_by = $request->user()->id;
        $price->save();
        $price->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.agency-prices.index');
    }
}
