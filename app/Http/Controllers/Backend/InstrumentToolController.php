<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\FixedAsset;
use App\Models\Account;
use App\Models\CostItem;
use App\Models\FixedAssetAllocationInformation;
use App\Models\FixedAssetAllocationCost;
use App\Models\InstrumentToolAllocationInformation;
use App\Models\InstrumentTool;
use Illuminate\Support\Facades\Validator;
use App\Models\Department;
use App\Models\Currency;
use App\Models\VoucherCollection;
use App\Models\SaleCostItem;
use App\Models\InstrumentToolAllocationCost;
use App\Models\InstrumentToolAllocation;
use App\Models\InstrumentToolType;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class InstrumentToolController 
{
    public function index(Request $request){
        $query = "1=1"; 
        $page_num       = intval($request->input('page_num',\App\Define\Constant::PAGE_NUM_20));
        $name = $request->input('name');
        $type_id = $request->input('type_id');
        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date'); 
        $status = intval($request->input('status', -1));
        if($status <> -1) $query .= " AND status = {$status}";
        if($name) $query .= " AND (name like '%" . $name . "%' )" ;
        if($type_id) $query .= " AND (type_id like $type_id)" ;
        if($from_date && $to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND voucher_date >= '" . $from_date . "' AND voucher_date <= '" .$to_date. "'";
        } else if( !$from_date && $to_date) {
            $to_date = date("Y-m-d", strtotime(str_replace('/', '-', $to_date)));
            $query .= " AND  voucher_date <= '" .$to_date. "'";
        } else if ($from_date && !$to_date) {
            $from_date = date("Y-m-d", strtotime(str_replace('/', '-', $from_date)));
            $query .= " AND  voucher_date >= '" .$from_date. "'";
        }
        
        $instrument_tools = InstrumentTool::whereRaw($query)->where('company_id', session('current_company'))->orderBy('voucher_date', 'DESC')->orderBy('voucher_no', 'DESC')->paginate($page_num);
        return view('backend.instrument-tools.index', compact('instrument_tools'));
    }
    public function show(Request $request, $id){
        $instrument_tool = InstrumentTool::find($id);
        $a = InstrumentToolAllocationCost::where('instrument_tool_id', $id)->pluck('voucher_id')->toArray();
        $vouchers = InstrumentToolAllocation::whereIn('id', $a)->get();
        return view('backend.instrument-tools.show', compact('instrument_tool', 'vouchers'));
     }
    public function create(){
        $currency = Currency::getCurrency();
        $allocation_accounts = Account::getAllocationAccounts(session('current_company'));
        $expense_accounts = Account::getExpenseAccounts(session('current_company'));
        $departments = Department::getDepartments(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        $types = InstrumentToolType::getActiveTypes(session('current_company'));
        $latest_voucher_number = InstrumentTool::latest()->pluck('id')->first();
         $str = substr($latest_voucher_number, -7);
        $new_voucher_number = $str + 1;
        if(strlen($new_voucher_number) == 1){
            $voucher_number = "TCCDC000000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 2){
            $voucher_number = "TCCDC00000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 3){
            $voucher_number = "TCCDC0000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 4){
            $voucher_number = "TCCDC000" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 5){
            $voucher_number = "TCCDC00" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 6){
            $voucher_number = "TCCDC0" . $new_voucher_number;
        }
        if(strlen($new_voucher_number) == 7){
            $voucher_number = "TCCDC" . $new_voucher_number;
        }
        return view('backend.instrument-tools.create', compact('allocation_accounts', 'departments', 'cost_items', 'expense_accounts', 'voucher_number', 'types', 'currency'));
    }
    
    public function store(Request $request){
    //    dd($request->all());
        $data = $request->all();
        if(!isset($request->status)){
           $data['status'] = '1';
        }
        $data['company_id'] = session('current_company');
        $data['created_by'] = session('created_by');
        $request->merge(['status' => intval($request->input('status', 1))]);
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        $data['residual_time'] = $request->period_number;
        $instrument_tool  = InstrumentTool::create($data);
        
        for ($i=0; $i < count($request->use_department) ; $i++) { 
           $instrument_tool_allocation = new InstrumentToolAllocationInformation();
           $instrument_tool_allocation->instrument_tool_id = $instrument_tool->id;
           $instrument_tool_allocation->department_id = $request->use_department[$i];
           $instrument_tool_allocation->quantity = $request->allocation_quantity[$i];
           $instrument_tool_allocation->allocation_rate = $request->allocation_rate[$i];
           $instrument_tool_allocation->expense_account = $request->expense_account[$i];
           $instrument_tool_allocation->cost_item = $request->cost_item[$i];
           $instrument_tool_allocation->save();
        }
        for ($i=0; $i < count($request->accounting_id) ; $i++) { 
           $voucher_collection = new VoucherCollection();
           $voucher_collection->instrument_tool_id = $instrument_tool->id;
           $voucher_collection->accounting_id = $request->accounting_id[$i];
           $voucher_collection->save();
        }
        
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.instrument-tools.index');
     }
    public function edit(Request $request, $id){
        $instrument_tool = InstrumentTool::find($id);
        $voucher_collection = VoucherCollection::where('instrument_tool_id', $id)->get();
        $allocation_accounts = Account::getAllocationAccounts(session('current_company'));
        $expense_accounts = Account::getExpenseAccounts(session('current_company'));
        $departments = Department::getDepartments(session('current_company'));
        $cost_items = CostItem::getActiveCostItem(session('current_company'));
        $types = InstrumentToolType::getActiveTypes(session('current_company'));
        $check = VoucherCollection::where('instrument_tool_id', $id)->get();
        $currency = Currency::getCurrency();
        $use = InstrumentToolAllocationInformation::where('instrument_tool_id', $id )->selectRaw("id, department_id, quantity, allocation_rate, expense_account, cost_item")->get();
        // dd($use);
        return view('backend.instrument-tools.edit', compact('instrument_tool', 'allocation_accounts', 'expense_accounts', 'departments', 'cost_items', 'voucher_collection', 'use', 'check', 'types', 'currency'));
    }
    public function update(Request $request, $id){
        // dd($request->all());
        $instrument_tool = InstrumentTool::find(intval($id));
        $data = $request->all();
        if (is_null($instrument_tool)) {
           Session::flash('message', trans('system.have_an_error'));
           Session::flash('alert-class', 'danger');
           return redirect()->route('admin.instrument-tools.index');
        }
        if(!isset($request->status)){
           $data['status'] = '1';
        }
        $request->merge(['status' => intval($request->input('status', 1))]);
        $voucher_date = str_replace("/", "-", $request->voucher_date);
        $data['voucher_date'] = date('Y-m-d', strtotime($voucher_date));
        $data['residual_time'] = $request->period_number;
        // dd($request->period_number);
        $instrument_tool->update($data);
        if($request->voucher_collection_id != ''){
           VoucherCollection::whereNotIn("id", $request->voucher_collection_id)->where('instrument_tool_id', $id)->delete();
        }
        if($request->voucher_collection_id == ''){
           VoucherCollection::where('instrument_tool_id', $id)->delete();
        }
        for ($i=0; $i < count($request->accounting_id) ; $i++) { 
           $voucher_collection = VoucherCollection::find($request->voucher_collection_id[$i]); 
           if($voucher_collection == null){
              $new = new VoucherCollection();
              $new->instrument_tool_id = $instrument_tool->id;
              $new->accounting_id = $request->accounting_id[$i];
              $new->save();
           }
           else{
              $voucher_collection->accounting_id = $request->accounting_id[$i];
              $voucher_collection->update();
           }  
        }
  
        if($request->allocation_info_id != ''){
           $allo_del = InstrumentToolAllocationInformation::whereNotIn("id", $request->allocation_info_id)->where('instrument_tool_id', $id)->delete();
        }
        for ($i=0; $i < count($request->use_department) ; $i++) { 
           $allocation_information = InstrumentToolAllocationInformation::find($request->allocation_info_id[$i]); 
           if($allocation_information == null){
              $new = new InstrumentToolAllocationInformation();
              $new->instrument_tool_id = $id;
              $new->department_id = $request->use_department[$i];
              $new->quantity = $request->allocation_quantity[$i];
              $new->allocation_rate = $request->allocation_rate[$i];
              $new->expense_account = $request->expense_account[$i];
              $new->cost_item = $request->cost_item[$i];
              $new->save();
           }
           else{
              $allocation_information->department_id = $request->use_department[$i];
              $allocation_information->quantity = $request->allocation_quantity[$i];
              $allocation_information->allocation_rate = $request->allocation_rate[$i];
              $allocation_information->expense_account = $request->expense_account[$i];
              $allocation_information->cost_item = $request->cost_item[$i];
              $allocation_information->update();
           }  
        }
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
  
        return redirect()->route('admin.instrument-tools.index');
     }
     public function destroy(Request $request, $id)
     {
        $instrument_tool = InstrumentTool::find($id);
        $instrument_tool_allocation_info = InstrumentToolAllocationInformation::where('instrument_tool_id', $instrument_tool->id);
        $voucher_collection = VoucherCollection::where('instrument_tool_id', $instrument_tool->id);
        if (is_null($instrument_tool)) {
           Session::flash('message', trans('system.have_an_error'));
           Session::flash('alert-class', 'danger');
           return back();
        }
        $instrument_tool->delete();
        $instrument_tool_allocation_info->delete();
        $voucher_collection->delete();
  
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return back();
     }
    public function storeType(Request $request){
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 400;
        if ($request->ajax()) {
            try {
                $validator = \Validator::make($data = $request->only(['type_code', 'type_name', 'type_description']), InstrumentToolType::rules());
                if($validator->fails()) throw new \Exception($validator->messages()->first());
                $data = $request->all();
                $data['status'] = '1';
                $request->merge(['status' => intval($request->input('status', 1))]);
                $data['code'] = $request->type_code;
                $data['name'] = $request->type_name;
                $data['description'] = $request->type_description;
                $data['created_by'] = session('created_by');
                $data['company_id'] = session('current_company');
                $type = InstrumentToolType::create($data);
                $statusCode = 200;
                $response['message'] = trans('system.success');
                $response['code'] = $type->code;
                $response['id'] = $type->id;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}
