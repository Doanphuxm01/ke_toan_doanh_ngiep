<?php

namespace App\Http\Controllers\Backend;

use App\Bank;
use App\Customer;
use App\CustomerWithdrawal;
use Illuminate\Http\Request;
use App\CustomerBankAccount;
use App\CustomerTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CustomerWithdrawalsController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval($request->input('status', -1));
        $customer       = intval($request->input('customer'));
        $source         = $request->input('source', -1);
        $fullname_email = $request->input('fullname_email');
        if($fullname_email) $query .= " AND (fullname like '%" . $fullname_email . "%' OR email like '%" . $fullname_email . "%' OR phone like '%" . $fullname_email . "%' OR tax_code like '%" . $fullname_email . "%' OR address like '%" . $fullname_email . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        if($source <> -1) $query .= " AND source = {$source}";

        $withdrawals = CustomerWithdrawal::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        if ($customer) {
            $customer = Customer::find(intval($request->input('customer')));
            if (!is_null($customer)) {
                $query .= " AND customer_id = {$customer->id}";
                $customer = [$customer->id => $customer->code . ' - ' . $customer->fullname];
            } else {
                $customer = [];
            }
        } else {
            $customer = [];
        }

        return view('backend.customer-withdrawals.index', compact('withdrawals', 'customer'));
    }

    public function create(Request $request)
    {
        if (!is_null(old('_token'))) {
            $media = Media::where('created_by', $request->user()->id)->where('status', -1)->where('type', \App\Define\Media::TYPE_COIN)->get();
            foreach ($media as $m) {
                $createdDay = date("Ymd", strtotime($m->created_at));
                if(\File::exists(public_path() . '/' . config('upload.customer-withdrawals') . $createdDay . '/' . $m->title)) {
                    \File::delete(public_path() . '/' . config('upload.customer-withdrawals') . $createdDay . '/' . $m->title);
                }
                $m->delete();
            }
        }

        $customerId = intval(old('customer'));
        $customer = [];
        if ($customerId) {
            $customer = Customer::where('id', $customerId)->where('level', \App\Define\Customer::LEVEL_PARTNER)->first();
            if (!is_null($customer)) $customer = [$customer->id => $customer->code . ' - ' . $customer->fullname];
        }

        return view('backend.customer-withdrawals.create', compact('customer'));
    }

    public function show(Request $request, $id)
    {
        $transaction = CustomerWithdrawal::find(intval($id));
        if (is_null($transaction)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customer-withdrawals.index');
        }
        $customer = Customer::find($transaction->customer_id);
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customer-withdrawals.index');
        }
        return view('backend.customer-withdrawals.show', compact('transaction', 'customer'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($data = $request->all(), CustomerWithdrawal::rules());
        $validator->setAttributeNames(trans('customer_withdrawals'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $customer = Customer::find($data['customer']);
        if (is_null($customer)) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', trans('system.have_an_error'));
            return back()->withErrors($errors)->withInput();
        }
        if ($customer->level <> \App\Define\Customer::LEVEL_PARTNER) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', "Tài khoản không phải Cộng tác viên");
            return back()->withErrors($errors)->withInput();
        }

        $data['source']     = \App\Define\CustomerWithdrawal::SOURCE_ADMIN;
        $data['type']       = \App\Define\CustomerWithdrawal::COIN_TYPE_REFILL;
        $data['created_by'] = $request->user()->id;
        $data['customer_id']= $customer->id;
        $trans = CustomerWithdrawal::create($data);

        // update media
        $media = Media::where('created_by', $request->user()->id)->where('status', -1)->where('type', \App\Define\Media::TYPE_COIN)->get();
        foreach ($media as $m) {
            $m->status = 1;
            $m->save();
            $trans->medias()->save($m);
        }

        // update total coin
        $customer->coin_available += $trans->amount;
        $customer->save();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.customer-withdrawals.index');
    }

    public function saveFile(Request $request)
    {
        $response = [ 'id' => 0, 'title' => '', 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                if (!$request->hasFile('file')) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $media  = [];
                $file  = $request->file;
                $rawName= $file->getClientOriginalName();
                $media['title'] = substr(str_slug(pathinfo($rawName, PATHINFO_FILENAME)), 0, 20) . '-' . date("His") . '.' . pathinfo($rawName, PATHINFO_EXTENSION);
                $path = config('upload.customer-withdrawals') .  date("Ymd") . '/';
                \File::makeDirectory($path, 0775, true, true);
                $file->move($path, $media['title']);
                $media['status']    = -1;
                $media['created_by']= $request->user()->id;
                $media['type']      = \App\Define\Media::TYPE_COIN;
                $media['souce']     = \App\Define\CustomerWithdrawal::SOURCE_ADMIN;
                $media['size']      = $this->formatBytes($file->getSize());
                $media = Media::create($media);
                $response['message']    = trans('system.success');
                $response['title']      = $media->title;
                $response['id']         = $media->id;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }

        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function deleteFile(Request $request)
    {
        $response = [ 'data' => 0, 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $media = Media::find(intval($request->id));
                if (is_null($media)) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $createdDay = date("Ymd", strtotime($media->created_at));
                if(\File::exists(public_path() . '/' . config('upload.customer-withdrawals') . $createdDay . '/' . $media->title)) {
                    \File::delete(public_path() . '/' . config('upload.customer-withdrawals') . $createdDay . '/' . $media->title);
                }
                $media->delete();

                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getImage(Request $request)
    {
        $response = [ 'data' => null, 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $media = [];
                $product = Product::find(intval($request->id));
                if (is_null($product)) {
                    $statusCode = 404;
                    throw new \Exception(trans('system.no_record_found', 1));
                }
                $tmp = $product->images()->select('id', 'title', 'size')->get();
                foreach ($tmp as $m) {
                    array_push($media, [
                        'title'     => $m->title,
                        'size'      => $m->size,
                        'id'        => $m->id,
                        'path'      => asset(config('upload.product') .  str_pad($product->id, 5, "0", STR_PAD_LEFT) . '/thumbs/' . $m->title),
                    ]);
                }

                $response['data']       = $media;
                $response['message']    = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }

        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    function formatBytes($bytes, $precision = 2) {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public function updateBulk(Request $request)
    {
        $response = ['path' => '', 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $ids = json_decode($request->input('ids'));
                if(empty($ids)) return response()->json(['error' => true, 'message' => trans('system.no_item_selected')]);
                switch ($request->input('action')) {
                    case 'excel':
                        $tmp = $withdrawals = CustomerWithdrawal::whereIn('id', $ids)->whereIn('status', [\App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING, \App\Define\Customer::WITHDRAWAL_STATUS_REQUESTED])->get();
                        if ($withdrawals->count() <> count($ids)) {
                            $statusCode = 400;
                            throw new \Exception(trans('system.have_an_error'), 1);
                        }

                        // lay customer id
                        $customerIds = array_column($tmp->toArray(), 'customer_id');
                        $customers = Customer::whereIn('id', $customerIds)->select('fullname', 'code', 'id')->get()->keyBy('id')->toArray();
                        $accounts = CustomerBankAccount::whereIn('customer_id', $customerIds)->where('is_default', 1)->select('customer_id', 'bank_name', 'bank_no', 'bank_branch', 'bank_id')->get()->keyBy('customer_id')->toArray();
                        foreach ($customerIds as $customerId) {
                            if (!isset($accounts[$customerId])) {
                                $statusCode = 400;
                                throw new \Exception("Tài khoản " . ($customers[$customerId]['code'] ?? "") . '-' . ($customers[$customerId]['fullname'] ?? "") . 'Chưa cập nhật tài khoản ngân hàng', 1);
                            }
                        }
                        $banks = Bank::pluck('name', 'id')->toArray();
                        $data = [];
                        foreach ($withdrawals as $withdrawal) {
                            $account = $accounts[$withdrawal->customer_id];
                            array_push($data, [
                                'fullname'  => $customers[$withdrawal->customer_id]['fullname'] ?? "",
                                'code'      => $customers[$withdrawal->customer_id]['code'] ?? "",
                                'bank'      => $banks[$account['bank_id']] ?? "",
                                'bank_name' => $account['bank_name'],
                                'bank_no'   => $account['bank_no'],
                                'bank_branch'=> $account['bank_branch'],
                                'amount'    => $withdrawal->amount,
                                'note'      => $withdrawal->note,
                            ]);
                        }
                        $fileName = 'Sube_Hoa-Hong_' . date('H.m.s_d.m.Y') . '.xlsx';
                        \Excel::store(new \App\Exports\WithdrawalsExport($data), $fileName, 'public');
                        // $response['path'] = \Storage::disk('local')->get($fileName);
                        $response['path'] = asset('storage/' . ($fileName));
                        // asset('storage/app/' . ($fileName));
                        $response['message'] = trans('system.success');
                        break;
                    case \App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING:
                        CustomerWithdrawal::whereIn('id', $ids)->update(['status' => \App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING]);
                        break;
                    case \App\Define\Customer::WITHDRAWAL_STATUS_REJECTED:
                        $withdrawals = CustomerWithdrawal::whereIn('id', $ids)->whereIn('status', [\App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING, \App\Define\Customer::WITHDRAWAL_STATUS_REQUESTED])->get();
                        foreach ($withdrawals as $withdrawal) {
                            $customer = Customer::find($withdrawal->customer_id);
                            $customer->coin_available   += $withdrawal->amount;
                            $customer->coin_holding     -= $withdrawal->amount;
                            $customer->save();
                            $withdrawal->status = \App\Define\Customer::WITHDRAWAL_STATUS_REJECTED;
                            $withdrawal->note = trim($request->input('note'));
                            $withdrawal->processed_by = $request->user()->id;
                            $withdrawal->save();
                        }
                        break;
                    case \App\Define\Customer::WITHDRAWAL_STATUS_SUCCESS:
                        $withdrawals = CustomerWithdrawal::whereIn('id', $ids)->whereIn('status', [\App\Define\Customer::WITHDRAWAL_STATUS_PROCESSING, \App\Define\Customer::WITHDRAWAL_STATUS_REQUESTED])->get();
                        foreach ($withdrawals as $withdrawal) {
                            $customer = Customer::find($withdrawal->customer_id);
                            // $customer->coin_available   += $withdrawal->amount;
                            $customer->coin_holding     -= $withdrawal->amount;
                            $customer->save();
                            $withdrawal->status = \App\Define\Customer::WITHDRAWAL_STATUS_SUCCESS;
                            $withdrawal->note = trim($request->input('note'));
                            $withdrawal->processed_by = $request->user()->id;
                            $withdrawal->save();
                            // update log sang tai khoan vi
                            CustomerTransaction::create([
                                'amount'        => -$withdrawal->amount,
                                // 'ref_order_id'  => '',
                                'type'          => \App\Define\CustomerTransaction::COIN_TYPE_COMMISSION,
                                // 'reason'        => 'Yêu cầu rút tiền',
                                'customer_id'   => $customer->id,
                                'created_by'    => $request->user()->id,
                                'note'          => trim($request->input('note')),
                                'source'        => \App\Define\CustomerTransaction::SOURCE_ADMIN,
                            ]);
                        }
                        break;
                    default:
                        $return['message']  = trans('system.no_action_selected');
                        return response()->json($return);
                }
                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}
