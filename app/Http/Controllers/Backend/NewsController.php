<?php

namespace App\Http\Controllers\Backend;

use App\News;
use App\NewsCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $query = '1=1';
        $title = $request->input('title');
        $status     = intval($request->input('status', -1));
        $featured   = intval($request->input('featured', -1));
        $category   = intval($request->input('category_id', -1));
        $date_range = $request->input('date_range');
        $page_num   = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));

        if($title) $query .= " AND title like '%" . $title . "%'";
        if($status <> -1) $query .= " AND status = {$status}";
        if($featured <> -1) $query .= " AND featured = {$featured}";
        if($category <> -1) {
            $children = NewsCategory::where('parent_id', $category)->select('id')->get();
            $children = implode(',', array_column($children->toArray(), 'id'));
            $category = ($children ? $category . ',' . $children : $category);
            $query .= " AND category_id IN({$category})";
        }
        if ($date_range) {
            $date_range = explode(' - ', $date_range);
            if (isset($date_range[0]) && isset($date_range[1])) {
                $query .= " AND created_at >= '" . date("Y-m-d 00:00:00", strtotime(str_replace('/', '-', ($date_range[0] == '' ? '1/1/2015' : $date_range[0]) ))) . "' AND updated_at <= '" . date("Y-m-d 23:59:59", strtotime(str_replace('/', '-', ($date_range[1] == '' ? date("d/m/Y") : $date_range[1]) ))) . "'";
            }
        }
        $news = News::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);
        $categories = [];
        $tmp = NewsCategory::where('parent_id', 0)->get();
        foreach ($tmp as $category) {
            $categories[$category->id] = $category->name;
            $children = NewsCategory::where('parent_id', $category->id)->get();
            foreach ($children as $child) {
                $categories[$child->id] = '-- ' . $child->name;
            }
        }

        return view('backend.news.index', compact('news', 'categories'));
    }

    public function create()
    {
        $categories = [];
        $tmp = NewsCategory::where('parent_id', 0)->where('status', 1)->get();
        foreach ($tmp as $category) {
            $categories[$category->id] = $category->name;
            $children = NewsCategory::where('parent_id', $category->id)->where('status', 1)->get();
            foreach ($children as $child) {
                $categories[$child->id] = '-- ' . $child->name;
            }
        }
        return view('backend.news.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->merge(['featured' => $request->input('featured', 0), 'status' => $request->input('status', 0)]);
        $validator = Validator::make($data = $request->only('category', 'title', 'image', 'seo_description', 'seo_keywords', 'content', 'summary', 'status', 'featured'), News::rules());

        $validator->setAttributeNames(trans('news'));
        if ($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        $data['created_by']   = \Auth::guard('admin')->user()->id;
        $data['category_id'] = $request->input('category');

        $image  = $request->file('image');
        $name   = str_slug($data['title']) . '_' . date("d_m_Y");
        $ext    = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
        $data['image'] = $name . '.' . $ext;
        $image->move(config('upload.news'), $data['image']);
        // origin
        $path = config('upload.news') . $data['image'];
        //resize
        ini_set('memory_limit','256M');
        $image = \Image::make($path);
        // if ($image->height() <> \App\Define\Constant::IMAGE_NEWS_HEIGHT || $image->width() <> \App\Define\Constant::IMAGE_NEWS_WIDTH) {
        //     Session::flash('message', "Ảnh bìa phải có kích thước là " . \App\Define\Constant::IMAGE_NEWS_WIDTH ."px x " . \App\Define\Constant::IMAGE_NEWS_HEIGHT . "px.");
        //     Session::flash('alert-class', 'danger');
        //     return back()->withInput();
        // }
        $image->save(config('upload.news') . $data['image']);
        $data['image'] = config('upload.news') . $data['image'];

        News::create($data);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.news.index');
    }

    public function show(Request $request, $id)
    {
        $news = News::find(intval($id));
        if ( is_null( $news ) ) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return Redirect::back();
        }

        $categories = [];
        $tmp = NewsCategory::where('parent_id', 0)->whereRaw("(status=1 OR id={$news->category_id})")->get();
        foreach ($tmp as $category) {
            $categories[$category->id] = $category->name;
            $children = NewsCategory::where('parent_id', $category->id)->whereRaw("(status=1 OR id={$news->category_id})")->get();
            foreach ($children as $child) {
                $categories[$child->id] = '-- ' . $child->name;
            }
        }

        return view('backend.news.show', ['news' => $news, 'categories' => $categories] );
    }

    public function edit(Request $request, $id)
    {
        $news = News::find(intval($id));
        if ( is_null( $news ) ) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }

        $categories = [];
        $tmp = NewsCategory::where('parent_id', 0)->whereRaw("(status=1 OR id={$news->category_id})")->get();
        foreach ($tmp as $category) {
            $categories[$category->id] = $category->name;
            $children = NewsCategory::where('parent_id', $category->id)->whereRaw("(status=1 OR id={$news->category_id})")->get();
            foreach ($children as $child) {
                $categories[$child->id] = '-- ' . $child->name;
            }
        }

        return view('backend.news.edit', compact( 'news', 'categories' ) );
    }

    public function update(Request $request, $id)
    {
        $id = intval($id);
        $request->merge(['featured' => $request->input('featured', 0), 'status' => $request->input('status', 0)]);
        $news = News::find($id);
        if (is_null($news)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }

        if ($request->hasFile('image'))
            $validator = Validator::make($data = $request->only('category', 'title', 'image', 'seo_description', 'seo_keywords', 'content', 'summary', 'status', 'featured'), News::rules($id));
        else
            $validator = Validator::make($data = $request->only('category', 'title', 'seo_description', 'seo_keywords', 'content', 'summary', 'status', 'featured'), News::rules($id));

        $validator->setAttributeNames(trans('news'));
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();

        $data['category_id'] = $request->input('category');

        if ($request->hasFile('image')) {
            if (\File::exists(public_path() . '/' . $news->image)) \File::delete(public_path() . '/' . $news->image);
            $image  = $request->file('image');
            $name   = str_slug($data['title']) . '_' . date("d_m_Y");
            $ext    = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
            $data['image'] = $name . '.' . $ext;
            $image->move(config('upload.news'), $data['image']);
            // origin
            $path = config('upload.news') . $data['image'];
            //resize
            ini_set('memory_limit','256M');
            $image = \Image::make($path);
            // if ($image->height() <> \App\Define\Constant::IMAGE_NEWS_HEIGHT || $image->width() <> \App\Define\Constant::IMAGE_NEWS_WIDTH) {
            //     Session::flash('message', "Ảnh bìa phải có kích thước là " . \App\Define\Constant::IMAGE_NEWS_WIDTH ."px x " . \App\Define\Constant::IMAGE_NEWS_HEIGHT . "px.");
            //     Session::flash('alert-class', 'danger');
            //     return back()->withInput();
            // }
            $image->save(config('upload.news') . $data['image']);

            $data['image'] = config('upload.news') . $data['image'];
        }

        $news->update($data);

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.news.index');
    }

    public function destroy(Request $request, $id)
    {
        $news = News::find($id);
        if (is_null($news)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return back();
        }
        if (\File::exists(public_path() . '/' . $news->image)) \File::delete(public_path() . '/' . $news->image);
        $news->delete();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.news.index');
    }

    public function updateBulk(Request $request)
    {
        if ($request->ajax()) {
            $return = [ 'error' => true, 'message' => trans('system.have_an_error') ];

            // if (!$request->user()->ability(['system', 'admin'], ['news.approve'])) {
            //     return response()->json($return, 401);
            // }

            $ids = json_decode($request->input('ids'));
            if(empty($ids)) return response()->json(['error' => true, 'message' => trans('system.no_item_selected')]);

            switch ($request->input('action')) {
                case 'delete':
                    foreach ($ids as $id) {
                        foreach ($ids as $id) {
                            $news = News::find($id);
                            if (\File::exists(public_path() . '/' . $news->image)) \File::delete(public_path() . '/' . $news->image);
                            $news->delete();
                        }
                    }
                    break;
                case 'active':
                    $return = ['error' => true, 'message' => trans('system.update') . ' ' . trans('system.success')];
                    foreach ($ids as $id) {
                        News::where('id', intval($id))->update(['status' => 1]);
                    }
                    break;
                case 'deactive':
                    $return = ['error' => true, 'message' => trans('system.update') . ' ' . trans('system.success')];
                    foreach ($ids as $id) {
                        News::where('id', intval($id))->update(['status' => 0]);
                    }
                    break;
                case 'category':
                    $return             = ['error' => true, 'message' => trans('system.update') . ' ' . trans('system.success')];
                    $category_id        = intval($request->input('category_id', 0));
                    $category           = NewsCategory::find($category_id);
                    if (is_null($category) || !$category->status) {
                        $return['message'] = 'Danh mục bạn chọn không cho phép thao tác này.';
                        return response()->json($return);
                    }

                    foreach ($ids as $id) {
                        News::where('id', intval($id))->update(['category_id' => $category_id]);
                    }
                    break;
                default:
                    $return['message']  = trans('system.no_action_selected');
                    return response()->json($return);
            }

            $return['error']    = false;
            $return['message']  = trans('system.success');
            Session::flash('message', $return['message']);
            Session::flash('alert-class', 'success');
            return response()->json($return);
        }
    }

    public function getById(Request $request)
    {

        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $newsId = intval($request->input("news_id"));
                if (!$newsId) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm lựa chọn của bạn.", 1);
                }
                $tmp        = [];
                $query      = "status = 1";
                $newsIds = $request->input("news_ids");
                $counter    = 1 ;
                if ($newsIds) {
                    $temp = explode(',', $newsIds);
                    foreach ($temp as $t) {
                        if (!is_numeric($t)) {
                            $statusCode = 400;
                            throw new \Exception("Dữ liệu không toàn vẹn.", 1);
                        }
                    }
                    $query .= " AND id NOT IN(" . $newsIds . ")";
                    $counter = count($temp) + 1;
                    $newsIds .= ",";
                }

                $news = News::whereRaw($query)->where('id', $newsId)->where('status', 1)->first();
                if (is_null($news)) {
                    $statusCode = 400;
                    throw new \Exception("Tin này đã được thêm vào trước đó.", 1);
                }
                $response['message'] = view('backend.products.ajaxRowNews', compact('news', 'counter'))->render();
                $response['newsIds'] = $newsIds . $news->id;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function searchByTerm(Request $request)
    {
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $tag = $request->input("tag");
                $tag = trim($tag['term']);
                if (!$tag) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm thấy từ khoá", 1);
                }
                $tmp        = [];


                $query = "(title like '%" . $tag . "%')";
                $news = News::where('status', 1)->whereRaw($query)->get();
                foreach ($news as $item) {
                    array_push($tmp, [
                        'id'    => $item->id,
                        'name'  => '[' . date('d/m/Y', strtotime($item->updated_at)) .'] ' . $item->title
                    ]);
                }

                $response['message'] = json_encode($tmp);
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getById1(Request $request)
    {
        $response = [ 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $type = $request->input("type");
                if (!in_array($type, \App\Define\Product::getSearchTypes())) {
                    $statusCode = 400;
                    throw new \Exception("Phân loại tìm theo không đúng.", 1);
                }
                $searchId = intval($request->input("search_id"));
                if (!$searchId) {
                    $statusCode = 400;
                    throw new \Exception("Không tìm lựa chọn của bạn.", 1);
                }
                $tmp        = [];
                $query      = "status = 1";
                $productIds = $request->input("product_ids");
                $counter    = 1 ;
                if ($productIds) {
                    $temp = explode(',', $productIds);
                    foreach ($temp as $t) {
                        if (!is_numeric($t)) {
                            $statusCode = 400;
                            throw new \Exception("Dữ liệu không toàn vẹn.", 1);
                        }
                    }
                    $query .= " AND id NOT IN(" . $productIds . ")";
                    $counter = count($temp) + 1;
                    $productIds .= ",";
                }

                if ($type == \App\Define\Product::SEARCH_TYPE_PRODUCT) {
                    $product   = Product::whereRaw($query)->where('parent_id', 0)->where('id', $searchId)->first();
                    if (is_null($product)) {
                        $statusCode = 400;
                        throw new \Exception("Dữ liệu đã bị thay đổi.", 1);
                    }
                    $tmp[$product->id] = [
                        'id'    => $product->id,
                        'name'  => $product->name,
                        'model' => $product->model,
                        'image' => asset($product->image)
                    ];
                } elseif ($type == \App\Define\Product::SEARCH_TYPE_CATEGORY) {
                    $category = ProductCategory::where("status", 1)->where("id", $searchId)->first();
                    if (is_null($category)) {
                        $statusCode = 400;
                        throw new \Exception("Dữ liệu đã bị thay đổi.", 1);
                    }
                    $categoryIds = $category->id;

                    $categories = ProductCategory::where("status", 1)->where("product_category_id", $category->id)->get();
                    foreach ($categories as $cat) {
                        if (!$cat->product_category_id)
                            break;

                        $categoryIds .= (',' . $cat->id);
                        // find grand-son or grand-daughter
                        $cates = ProductCategory::where("status", 1)->where("product_category_id", $cat->id)->select("id")->get();
                        if ($cates->count()) {
                            $categoryIds .= (',' . implode(',', array_column($cates->toArray(), 'id')));
                        }
                    }

                    $products = Product::whereRaw($query)->whereRaw("product_category_id IN(" . $categoryIds . ")")->get();
                    foreach ($products as $product) {
                        $tmp[$product->id] = [
                            'id'    => $product->id,
                            'name'  => $product->name,
                            'model' => $product->model,
                            'image' => asset($product->image)
                        ];
                    }
                } elseif ($type == \App\Define\Product::SEARCH_TYPE_BRAND) {
                    $brand = Brand::where("status", 1)->where("id", $searchId)->first();
                    if (is_null($brand)) {
                        $statusCode = 400;
                        throw new \Exception("Dữ liệu đã bị thay đổi.", 1);
                    }

                    $products = Product::whereRaw($query)->where("brand_id", $brand->id)->get();
                    foreach ($products as $product) {
                        $tmp[$product->id] = [
                            'id'    => $product->id,
                            'name'  => $product->name,
                            'model' => $product->model,
                            'image' => asset($product->image)
                        ];
                    }
                } elseif ($type == \App\Define\Product::SEARCH_TYPE_SUPPLIER) {
                    $supplier = ProductSupplier::where("status", 1)->where("id", $searchId)->first();
                    if (is_null($supplier)) {
                        $statusCode = 400;
                        throw new \Exception("Dữ liệu đã bị thay đổi.", 1);
                    }

                    $products = Product::whereRaw($query)->where("product_supplier_id", $supplier->id)->get();
                    foreach ($products as $product) {
                        $tmp[$product->id] = [
                            'id'    => $product->id,
                            'name'  => $product->name,
                            'model' => $product->model,
                            'image' => asset($product->image)
                        ];
                    }
                } else {
                    $statusCode = 400;
                    throw new \Exception("Phân loại tìm theo không đúng.", 1);
                }

                $response['message'] = view('backend.promotions.ajaxProducts', compact('tmp', 'counter'))->render();
                $response['productIds'] = $productIds . implode(',', array_keys($tmp));
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}