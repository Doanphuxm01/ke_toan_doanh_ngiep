<?php

namespace App\Http\Controllers\Backend;

use App\Models\Tax;
use App\Models\Job;
use App\Models\Partner;
use App\Models\Account;
use App\Models\Currency;
use App\Models\GroupUnit;
use App\Models\Accounting;
use App\Models\BranchCode;
use App\Models\SaleCostItem;
use Illuminate\Http\Request;
use App\Models\ServicePurchase;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ServicePurchasesController extends Controller
{
    public function index(Request $request)
    {
        $query = '1=1';
        $status = $request->input('status', -1);
        $page_num = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        if ($fromDate && $toDate) {
            $fromDate = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
            $toDate = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
            $query .= " AND created_at >= '" . $fromDate . "' AND created_at <= '" . $toDate . "'";
        } else if (!$fromDate && $toDate) {
            $toDate = date("Y-m-d", strtotime(str_replace('/', '-', $toDate)));
            $query .= " AND  created_at <= '" . $toDate . "'";
        } else if ($fromDate && !$toDate) {
            $fromDate = date("Y-m-d", strtotime(str_replace('/', '-', $fromDate)));
            $query .= " AND  created_at >= '" . $fromDate . "'";
        }
        $objectName = $request->input('vendor');
        if ($objectName) {
            $queryObjectName = '1=1';
            $queryObjectName .= " AND (name like '%" . $objectName . "%')";
            $id = Partner::whereRaw($queryObjectName)->where('company_id', session('current_company'))->pluck('id')->toArray();
            $id = implode(",", $id);
            if (!$id) {
                $id = '1=1';
            }
            $query .= " AND partner_id IN ($id)";
        }
        if ($status <> -1) $query .= " AND status = '$status'";
        $servicePurchase = ServicePurchase::whereRaw($query)->where('company_id', session('current_company'))->orderBy('id')->paginate($page_num);
        return view('backend.service-purchases.index', compact('servicePurchase'));
    }

    public function create()
    {
        \Session::forget('total_amount');
        \Session::forget('total_taxation');
        $branchCodes            = BranchCode::getBranchCode();
        $jobs                   = Job::getJob();
        $itemCode               = SaleCostItem::where('status', '1')->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        $accounts               = Account::getActiveLowestLevel(session('current_company'));
        $currency               = Currency::all()->pluck('name', 'id')->toArray();
        $group_unit             = GroupUnit::getGroupUnit();
        $latest_financial_paper = ServicePurchase::max('id');
        $new_financial_paper = $latest_financial_paper + 1;
        if (strlen($new_financial_paper) == 1) {
            $financial_paper = "MDV000000" . $new_financial_paper;
        }
        if (strlen($new_financial_paper) == 2) {
            $financial_paper = "MDV00000" . $new_financial_paper;
        }
        if (strlen($new_financial_paper) == 3) {
            $financial_paper = "MDV0000" . $new_financial_paper;
        }
        if (strlen($new_financial_paper) == 4) {
            $financial_paper = "MDV000" . $new_financial_paper;
        }
        if (strlen($new_financial_paper) == 5) {
            $financial_paper = "MDV00" . $new_financial_paper;
        }
        if (strlen($new_financial_paper) == 6) {
            $financial_paper = "MDV0" . $new_financial_paper;
        }
        if (strlen($new_financial_paper) == 7) {
            $financial_paper = "MDV" . $new_financial_paper;
        }
        return view('backend.service-purchases.create', compact('branchCodes', 'itemCode', 'jobs', 'accounts', 'currency', 'financial_paper', 'group_unit'));
    }

    public function store(Request $request)
    {   
        $data_service_purchase = $this->getDataServicePurchase($request);
        $service_purchase_id = 0;
        $validator = \Validator::make($data_service_purchase, ServicePurchase::rules($data_service_purchase['voucher_no']));
        $validator->setAttributeNames(trans('service_purchases'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        unset($data_service_purchase['vendor']);
        unset($data_service_purchase['vendor_name']);
        $data_service_purchase['created_by'] = $request->user()->id;
        $data_service_purchase['company_id'] = session('current_company');
        $data_accounting = $this->getDataAccounting($request);
        $data_tax        = $this->getDataTax($request);
        $data_service_purchase['voucher_date']     = date("Y-m-d", strtotime(str_replace('/', '-', $request->day_voucher)));
        $data_service_purchase['time_for_payment'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->time_for_payment)));
        $service_purchase_id = ServicePurchase::insertGetId($data_service_purchase);
        $data_tax = array_map(function ($value) use ($service_purchase_id) {
            $value['invoice_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $value['invoice_date'])));
            return array_merge($value, ['service_purchase_id' => $service_purchase_id]);
        }, $data_tax);

        $data_accounting = array_map(function ($value) use ($service_purchase_id) {
            return array_merge($value, ['service_purchase_id' => $service_purchase_id]);
        }, $data_accounting);
        Accounting::insert($data_accounting);
        Tax::insert($data_tax);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.service-purchases.index');
    }

    public function update(Request $request, $id)
    {
        $servicePurchase = ServicePurchase::find(intval($id));
        if (is_null($servicePurchase)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.partners.index');
        }
        $data_service_purchase = $this->getDataServicePurchase($request);
        $accounting = Accounting::where('service_purchase_id', $servicePurchase->id);
        $tax = Tax::where('service_purchase_id', $servicePurchase->id);
        $service_purchase_id = 0;
        $validator = \Validator::make($data_service_purchase, ServicePurchase::rules($data_service_purchase['voucher_no']));
        $validator->setAttributeNames(trans('service_purchases'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();
        unset($data_service_purchase['vendor']);
        unset($data_service_purchase['vendor_name']);
        $data_service_purchase['updated_by'] = $request->user()->id;
        $data_service_purchase['company_id'] = session('current_company');
        $accounting = ServicePurchase::find($id)->accounting->pluck('id')->toArray();
        if ($accounting) {
            $data_accounting = $this->getDataAccounting($request);
        }
        $tax = ServicePurchase::find($id)->Taxes->pluck('id')->toArray();
        $data_tax = [];
        if ($tax) {
            $data_tax = $this->getDataTax($request);
        }
        Accounting::destroy($accounting);
        Tax::destroy($tax);
        $data_service_purchase['voucher_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->day_voucher)));
        $data_service_purchase['time_for_payment'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->time_for_payment)));
        $servicePurchase->update($data_service_purchase);
        Accounting::insert($data_accounting);
        Tax::insert($data_tax);
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.service-purchases.index');
    }

    public function getInfoSaleCostItem(Request $request)
    {
        $response = ['message' => trans('system.have_an_error')];
        $statusCode = 200;
        if ($request->ajax()) {
            try {
                $saleCostItem = SaleCostItem::where('id', $request->id)->get();
                if (is_null($saleCostItem)) {
                    $statusCode = 400;
                    throw new \Exception($response['message'] . '1', 1);
                }
                $response['data'] = ['debit_account' => $saleCostItem[0]->cost_account_code, 'description' => $saleCostItem[0]->description, 'id' => $saleCostItem[0]->id];
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function edit(Request $request, $id)
    {
        $branchCodes         = BranchCode::getBranchCode();
        $partnerVendorCodes  = Partner::getAllPartner();
        $itemCode            = SaleCostItem::where('status', '1')->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        $jobs                = Job::getJob();
        $accounts            = Account::getActiveLowestLevel(session('current_company'));
        $currency            = Currency::all()->pluck('name', 'id')->toArray();
        $group_unit          = GroupUnit::getGroupUnit();
        $editServicePurchase = ServicePurchase::find($id)->load(accounting, taxes);
        return view('backend.service-purchases.edit', compact('editServicePurchase', 'branchCodes', 'itemCode', 'jobs', 'accounts', 'currency', 'partnerVendorCodes', 'group_unit'));
    }

    public function getDataTax(Request $request)
    {
        $data_tax = [];
        for ($i = 0; $i < count($request->tax_rate); $i++) {
            $data_tax_row['sale_code_item_id'] = $request->sale_code_item[$i];
            $data_tax_row['description']       = $request->description_tax[$i];
            $data_tax_row['tax_rate']          = $request->tax_rate[$i];
            $data_tax_row['taxation']          = $request->taxation[$i];
            $data_tax_row['credit_account']    = $request->credit_account[$i];
            $data_tax_row['exchange_tax']      = $request->exchange_tax[$i];
            $data_tax_row['unit_price']        = $request->unit_price[$i];
            $data_tax_row['exchange_unit']     = $request->exchange_unit_price[$i];
            $data_tax_row['account_tax']       = $request->account_tax[$i];
            $data_tax_row['group_unit_buy']    = $request->group_unit[$i];
            $data_tax_row['invoice_no']        = $request->invoice_no[$i];
            $data_tax_row['invoice_date']      = $request->date_invoice[$i];
            $data_tax_row['partner_id']        = intval($request->object_code_tax[$i]);
            $data_tax_row['partner_name']      = $request->object_name_tax[$i];
            $data_tax_row['tax_code']          = $request->tax_code[$i];
            $data_tax_row['explain_tax']       = $request->explain_tax[$i];
            $data_tax_row['created_by']        = $request->user()->id;
            $data_tax_row['company_id']        = session('current_company');
            $validator = \Validator::make($data_tax_row, Tax::rules());
            $validator->setAttributeNames(trans('taxes'));
            if ($validator->fails()) return back()->withErrors($validator)->withInput();
            array_push($data_tax, $data_tax_row);
        }
        return $data_tax;
    }

    public function getDataAccounting(Request $request)
    {
        $data_accounting = [];
        for ($i = 0; $i < count($request->amount); $i++) {
            $data_accounting_row['sale_code_item_id'] = $request->sale_code_item[$i];
            $data_accounting_row['description'] = $request->description[$i];
            $data_accounting_row['job_code_id'] = $request->job_code[$i];
            $data_accounting_row['debit_account'] = $request->debit_account[$i];
            $data_accounting_row['credit_account'] = $request->credit_account[$i];
            $data_accounting_row['quantity'] = $request->quantity[$i];
            $data_accounting_row['price_unit'] = $request->price_unit[$i];
            $data_accounting_row['amount'] = $request->amount[$i];
            $data_accounting_row['exchange'] = $request->exchange[$i];
            $data_accounting_row['branch_code_id'] = $request->branch_code[$i];
            $data_accounting_row['credit_object_id'] = intval($request->object_code[$i]);
            $data_accounting_row['bank_account'] = $request->bank_account[$i];
            $data_accounting_row['created_by'] = $request->user()->id;
            $data_accounting_row['company_id'] = session('current_company');
            $data_accounting_row['mawb'] = $request->mawb[$i];
            $data_accounting_row['hawb'] = $request->hawb[$i];
            $data_accounting_row['hbl'] = $request->hbl[$i];
            $data_accounting_row['obl'] = $request->obl[$i];
            $data_accounting_row['invoice'] = $request->invoice[$i];
            $validator = \Validator::make($data_accounting_row, Accounting::rules());
            $validator->setAttributeNames(trans('accounting'));
            if ($validator->fails()) return back()->withErrors($validator)->withInput();
            array_push($data_accounting, $data_accounting_row);
        }
        return $data_accounting;
    }

    public function getDataServicePurchase(Request $request)
    {
        $data_service_purchase['partner_id']   = $request->vendor;
        $data_service_purchase['desc']         = $request->details;
        $data_service_purchase['voucher_date'] = $request->day_voucher;
        $data_service_purchase['number_of_days_owed'] = $request->number_of_days_owed;
        $data_service_purchase['voucher_no'] = $request->financial_paper;
        $data_service_purchase['currency_id'] = $request->currency;
        $data_service_purchase['exchange_rate'] = $request->exchange_rate;
        $data_service_purchase['time_for_payment'] = $request->time_for_payment;
        $data_service_purchase['exchange_rate'] = $request->exchange_rate;
        $data_service_purchase['vendor'] = $request->vendor;
        $data_service_purchase['vendor_name'] = $request->vendor_name;
        $data_service_purchase['total_price_unit_accounting'] = $request->total_with_amount;
        $data_service_purchase['total_amount_accounting'] = $request->total_amount;
        $data_service_purchase['total_exchange_accounting'] = $request->total_exchange;
        $data_service_purchase['total_quantity_accounting'] = $request->total_quantity;
        $data_service_purchase['total_taxation'] = $request->total_taxation;
        $data_service_purchase['total_exchange_taxation'] = $request->total_exchange_tax;
        $data_service_purchase['total_amount'] = $request->total_exchange;
        $data_service_purchase['created_at'] = date("Y-m-d h:i:s");
        $data_service_purchase['vat_amount'] = $request->total_taxation;
        $data_service_purchase['total_payment'] = $request->total_exchange + $request->total_taxation;

        return  $data_service_purchase;
    }

    public function show(Request $request, $id)
    {
        $branchCodes = BranchCode::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        $vendor = Partner::where('status', '1')->where('company_id', session('current_company'))->where('is_vendor', '1')->pluck('code', 'id')->toArray();
        $partnerVendorCodes = Partner::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        $itemCode = SaleCostItem::where('status', '1')->where('company_id', session('current_company'))->pluck('code', 'id')->toArray();
        $jobs = Job::where('status', 1)->where('company_id', session('current_company'))->pluck('code', 'code')->toArray();
        $accounts = Account::getActiveLowestLevel(session('current_company'));
        $currency = Currency::all()->pluck('name', 'id')->toArray();
        $group_unit = GroupUnit::all()->pluck('id', 'id')->toArray();
        $servicePurchase = ServicePurchase::find(intval($id));
        $accounting = Accounting::where('service_purchase_id', intval($id))->get();

        $tax = Tax::where('service_purchase_id', intval($id))->get();
        return view('backend.service-purchases.show', compact('servicePurchase', 'accounting', 'tax', 'branchCodes', 'vendor', 'itemCode', 'jobs', 'accounts', 'currency', 'partnerVendorCodes', 'group_unit'));
    }

    public function save($id)
    {
        $servicePurchase = ServicePurchase::find(intval($id));
        $servicePurchase->status = 'SAVE';
        $servicePurchase->update();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.service-purchases.index');
    }

    public function approve($id)
    {
        $servicePurchase = ServicePurchase::find(intval($id));
        $servicePurchase->status = 'APPROVED';
        $servicePurchase->save();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.service-purchases.index');
    }

    public function cancel($id)
    {
        $servicePurchase = ServicePurchase::find(intval($id));
        $servicePurchase->status = 'DRAFT';
        $servicePurchase->update();
        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');
        return redirect()->route('admin.service-purchases.index');
    }


    public function destroy(Request $request, $id)
    {
        $servicePurchase = ServicePurchase::find(intval($id));
        if (is_null($servicePurchase)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.service-purchases.index');
        }
        try {
			DB::beginTransaction();
			$servicePurchase->accounting()->delete();
			$servicePurchase->taxes()->delete();
			$servicePurchase->delete();
			DB::commit();
			Session::flash('message', trans('system.success'));
			Session::flash('alert-class', 'success');
			return redirect()->route('admin.service-purchases.index');
		} catch (\Exception $e) {
			DB::rollBack();
			return back()->withErrors($e->getMessage());
		}
    }

    public function delete_all(Request $request)
    {
        if ($request->ajax()) {
            $return = ['error' => true, 'message' => trans('system.have_an_error')];
            $ids = json_decode($request->input('ids'));
            if (empty($ids)) return response()->json(['error' => true, 'message' => trans('system.no_item_selected')]);
            foreach ($ids as $id) {
                foreach ($ids as $id) {
                    $servicePurchase = ServicePurchase::find(intval($id));
                    $accounting = $servicePurchase->accounting->pluck('id')->toArray();
                    $tax = $servicePurchase->Taxes->pluck('id')->toArray();
                    $servicePurchase->delete();
                    Accounting::destroy($accounting);
                    Tax::destroy($tax);
                }
            }
            $return['error'] = false;
            $return['message'] = trans('system.success');
            Session::flash('message', $return['message']);
            Session::flash('alert-class', 'success');
            return response()->json($return);
        }
    }
}
