<?php

namespace App\Http\Controllers\Backend;

use App\Media;
use App\Customer;
use App\CustomerTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CustomerTransactionsController extends Controller
{
    public function index(Request $request)
    {
        $query = "1=1";
        $page_num       = intval($request->input('page_num', \App\Define\Constant::PAGE_NUM_20));
        $status         = intval($request->input('status', -1));
        $customer       = intval($request->input('customer'));
        $source         = $request->input('source', -1);
        $fullname_email = $request->input('fullname_email');
        if($fullname_email) $query .= " AND (fullname like '%" . $fullname_email . "%' OR email like '%" . $fullname_email . "%' OR phone like '%" . $fullname_email . "%' OR code like '%" . $fullname_email . "%' OR address like '%" . $fullname_email . "%')";
        if($status <> -1) $query .= " AND status = {$status}";
        if($source <> -1) $query .= " AND source = {$source}";
        if ($customer) {
            $customer = Customer::find(intval($request->input('customer')));
            if (!is_null($customer)) {
                $query .= " AND customer_id = {$customer->id}";
                $customer = [$customer->id => $customer->code . ' - ' . $customer->fullname];
            } else {
                $customer = [];
            }
        } else {
            $customer = [];
        }
        $transactions = CustomerTransaction::whereRaw($query)->orderBy('updated_at', 'DESC')->paginate($page_num);

        return view('backend.customer-transactions.index', compact('transactions', 'customer'));
    }

    public function create(Request $request)
    {
        if (!is_null(old('_token'))) {
            $media = Media::where('created_by', $request->user()->id)->where('status', -1)->where('type', \App\Define\Media::TYPE_COIN)->get();
            foreach ($media as $m) {
                $createdDay = date("Ymd", strtotime($m->created_at));
                if(\File::exists(public_path() . '/' . config('upload.customer-transactions') . $createdDay . '/' . $m->title)) {
                    \File::delete(public_path() . '/' . config('upload.customer-transactions') . $createdDay . '/' . $m->title);
                }
                $m->delete();
            }
        }

        $customerId = intval(old('customer'));
        $customer = [];
        if ($customerId) {
            $customer = Customer::where('id', $customerId)->whereIn('level', [\App\Define\Customer::LEVEL_PARTNER, \App\Define\Customer::LEVEL_DROPSHIP])->first();
            if (!is_null($customer)) $customer = [$customer->id => $customer->code . ' - ' . $customer->fullname];
        }

        return view('backend.customer-transactions.create', compact('customer'));
    }

    public function show(Request $request, $id)
    {
        $transaction = CustomerTransaction::find(intval($id));
        if (is_null($transaction)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customer-transactions.index');
        }
        $customer = Customer::find($transaction->customer_id);
        if (is_null($customer)) {
            Session::flash('message', trans('system.have_an_error'));
            Session::flash('alert-class', 'danger');
            return redirect()->route('admin.customer-transactions.index');
        }
        return view('backend.customer-transactions.show', compact('transaction', 'customer'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($data = $request->all(), CustomerTransaction::rules());
        $validator->setAttributeNames(trans('customer_transactions'));
        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $customer = Customer::find($data['customer']);
        if (is_null($customer)) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', trans('system.have_an_error'));
            return back()->withErrors($errors)->withInput();
        }
        if (!in_array($customer->level, [\App\Define\Customer::LEVEL_PARTNER, \App\Define\Customer::LEVEL_DROPSHIP])) {
            $errors = new \Illuminate\Support\MessageBag;
            $errors->add('editError', "Tài khoản không phải Cộng tác viên");
            return back()->withErrors($errors)->withInput();
        }

        $data['source']     = \App\Define\CustomerTransaction::SOURCE_ADMIN;
        $data['type']       = \App\Define\CustomerTransaction::COIN_TYPE_REFILL;
        $data['created_by'] = $request->user()->id;
        $data['customer_id']= $customer->id;
        $trans = CustomerTransaction::create($data);

        // update media
        $media = Media::where('created_by', $request->user()->id)->where('status', -1)->where('type', \App\Define\Media::TYPE_COIN)->get();
        foreach ($media as $m) {
            $m->status = 1;
            $m->save();
            $trans->medias()->save($m);
        }

        // update total coin
        $customer->coin_available += $trans->amount;
        $customer->save();

        Session::flash('message', trans('system.success'));
        Session::flash('alert-class', 'success');

        return redirect()->route('admin.customer-transactions.index');
    }

    public function saveFile(Request $request)
    {
        $response = [ 'id' => 0, 'title' => '', 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                if (!$request->hasFile('file')) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $media  = [];
                $file  = $request->file;
                $rawName= $file->getClientOriginalName();
                $media['title'] = substr(str_slug(pathinfo($rawName, PATHINFO_FILENAME)), 0, 20) . '-' . date("His") . '.' . pathinfo($rawName, PATHINFO_EXTENSION);
                $path = config('upload.customer-transactions') .  date("Ymd") . '/';
                \File::makeDirectory($path, 0775, true, true);
                $file->move($path, $media['title']);
                $media['status']    = -1;
                $media['created_by']= $request->user()->id;
                $media['type']      = \App\Define\Media::TYPE_COIN;
                $media['souce']     = \App\Define\CustomerTransaction::SOURCE_ADMIN;
                $media['size']      = $this->formatBytes($file->getSize());
                $media = Media::create($media);
                $response['message']    = trans('system.success');
                $response['title']      = $media->title;
                $response['id']         = $media->id;
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }

        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function deleteFile(Request $request)
    {
        $response = [ 'data' => 0, 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $media = Media::find(intval($request->id));
                if (is_null($media)) {
                    $statusCode = 400;
                    throw new \Exception(trans('system.have_an_error'), 1);
                }
                $createdDay = date("Ymd", strtotime($media->created_at));
                if(\File::exists(public_path() . '/' . config('upload.customer-transactions') . $createdDay . '/' . $media->title)) {
                    \File::delete(public_path() . '/' . config('upload.customer-transactions') . $createdDay . '/' . $media->title);
                }
                $media->delete();

                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    public function getImage(Request $request)
    {
        $response = [ 'data' => null, 'message' => trans('system.have_an_error') ];
        $statusCode = 200;
        if($request->ajax()) {
            try {
                $media = [];
                $product = Product::find(intval($request->id));
                if (is_null($product)) {
                    $statusCode = 404;
                    throw new \Exception(trans('system.no_record_found', 1));
                }
                $tmp = $product->images()->select('id', 'title', 'size')->get();
                foreach ($tmp as $m) {
                    array_push($media, [
                        'title'     => $m->title,
                        'size'      => $m->size,
                        'id'        => $m->id,
                        'path'      => asset(config('upload.product') .  str_pad($product->id, 5, "0", STR_PAD_LEFT) . '/thumbs/' . $m->title),
                    ]);
                }

                $response['data']       = $media;
                $response['message']    = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }

        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }

    function formatBytes($bytes, $precision = 2) {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public function reviewAtm(Request $request)
    {
        $response = ['message' => trans('system.have_an_error') ];
        $statusCode = 400;
        if($request->ajax()) {
            try {
                $action = trim($request->action);
                if (!in_array($action, ['reject', 'approve'])) {
                    throw new \Exception("Bạn cần Duyệt hoặc Từ chối", 1);
                }
                $ids = json_decode($request->input('ids'), 1);
                $trans = CustomerTransaction::whereNull("bank_code")->where('status', 0)->whereIn('id', $ids)->get();
                if ($trans->count() <> count($ids)) {
                    throw new \Exception("Dữ liệu không chính xác, vui lòng F5 lại trình duyệt", 1);
                }
                foreach ($trans as $tran) {
                    $customer = Customer::find($tran->customer_id);
                    if (is_null($customer)) continue;
                    if ($action == 'reject') {
                        $tran->status = -1;
                        $tran->note = "Từ chối bởi " . $request->user()->fullname;
                        $tran->save();
                    } else {
                        $tran->status = 1;
                        $tran->note = "Duyệt bởi " . $request->user()->fullname;
                        $tran->save();

                        $customer->coin_available += $tran->amount;
                        $customer->save();
                    }
                }

                Session::flash('message', trans('system.success'));
                Session::flash('alert-class', 'success');
                $statusCode = 200;
                $response['message'] = trans('system.success');
            } catch (\Exception $e) {
                if ($statusCode == 200) $statusCode = 500;
                $response['message'] = $e->getMessage();
            } finally {
                return response()->json($response, $statusCode);
            }
        } else {
            $statusCode = 405;
            return response()->json($response, $statusCode);
        }
    }
}
