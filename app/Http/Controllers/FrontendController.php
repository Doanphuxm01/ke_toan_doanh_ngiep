<?php

namespace App\Http\Controllers;

use App\Product;
use App\StaticPage;
use App\ProductCategory;
use Jenssegers\Agent\Agent;
use App\BannerAdvertisement;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class FrontendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $banners;
    protected $productCategories;
    protected $isMobileOrTablet = 0;
    protected $isTablet = 0;
    protected $agent = null;

    public function __construct()
    {
    	$this->middleware(function ($request, $next) {
            $viewedProducts = session('viewed_products', []);
            $lastProduct    = [];
            if (count($viewedProducts) < 15) {
                $lastProduct = Product::getFeaturedProducts();
                $lastProduct = array_slice($lastProduct, 0, 15 - count($viewedProducts));
            }
            $viewedProducts = array_reverse($viewedProducts) + $lastProduct;

    		$this->productCategories = ProductCategory::getCategories();
    		if (!Session::has('isMobileOrTablet')) {
                $agent = new Agent();
                if ($agent->isMobile() || $agent->isTablet()) {
                    session(['isMobileOrTablet' => 1]);
                } else {
                    session(['isMobileOrTablet' => 0]);
                }

                if ($agent->isTablet()) {
                    session(['isTablet' => 1]);
                } else {
                    session(['isTablet' => 0]);
                }
                session(['agent' => $agent]);
            }
            $this->isMobileOrTablet = Session::get('isMobileOrTablet');
            $this->isTablet = Session::get('isTablet');
            $this->agent = Session::get('agent');

            view()->share('staticPages', StaticPage::getByAllWihoutGroup());
            view()->share('viewedProducts', $viewedProducts);
            view()->share('productCategories', $this->productCategories);
	    	view()->share('banners', BannerAdvertisement::getBannerAdvertisements());
            return $next($request);
        });
    }

}
