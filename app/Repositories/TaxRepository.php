<?php


namespace App\Repositories;

use App\Models\Tax;

class TaxRepository
{
	protected $taxModel;

	public function __construct()
	{
		$this->taxModel = new Tax;
	}

	public function save(array $input, $id = null)
	{
		return $this->taxModel->updateOrCreate(
			[
				'id' => $id,
			],
			[
				'accounting_invoice_id' => $input['accounting_invoice_id'],
				'description' 		=> $input['desc'],
				'tax_rate' 			=> $input['tax_rate'],
				'taxation' 			=> $input['taxation'],
				'exchange_tax' 		=> $input['exchange_tax'],
				'unit_price' 		=> $input['unit_price'],
				'exchange_unit' 	=> $input['exchange_unit'],
				'account_tax' 		=> $input['account_tax'],
				'group_unit_buy' 	=> $input['group_unit'],
				'invoice_no' 		=> $input['invoice_no'],
				'invoice_date' 		=> $input['invoice_date'],
				'partner_id' 		=> $input['partner_id'],
				'partner_name' 		=> $input['partner_name'],
				'tax_code' 			=> $input['tax_code'],
				'company_id'		=> session('current_company'),
			]
		);
	}
}