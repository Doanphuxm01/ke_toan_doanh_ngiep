<?php


namespace App\Repositories;

use App\Models\Accounting;

class AccountingRepository
{
	protected $accountingModel;

	public function __construct()
	{
		$this->accountingModel = new Accounting;
	}

	public function save(array $input, $id = null)
	{
		return $this->accountingModel->updateOrCreate(
			[
				'id' => $id,
			],
			[
				'accounting_invoice_id' => $input['accounting_invoice_id'],
				'description' 			=> $input['desc'],
				'job_code_id' 			=> $input['job_code_id'],
				'debit_account' 		=> $input['debit_account'],
				'credit_account' 		=> $input['credit_account'],
				'amount' 				=> $input['amount'],
				'exchange' 				=> $input['exchange'],
				'branch_code_id' 		=> $input['branch_code_id'],
				'debit_object_id' 		=> $input['debit_object_id'],
				'credit_object_id' 		=> $input['credit_object_id'],
				'cost_item'				=> $input['cost_item'],
				'company_id'			=> session('current_company'),
			]
		);
	}
}