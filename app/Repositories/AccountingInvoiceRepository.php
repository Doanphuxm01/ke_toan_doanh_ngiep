<?php


namespace App\Repositories;

use App\Models\AccountingInvoice;

class AccountingInvoiceRepository
{
	protected $accountingInvoice;

	public function __construct()
	{
		$this->accountingInvoice = new AccountingInvoice;
	}

	public function save(array $input, $id = null)
	{
		return $this->accountingInvoice->updateOrCreate(
			[
				'id' => $id,
			],
			[
				'desc'			=> $input['desc'],
				'voucher_no' 	=> $input['voucher_no'],
				'voucher_date' 	=> $input['voucher_date'],
				'payment_term' 	=> $input['payment_term'],
				'exchange_rate'	=> $input['exchange_rate'],
				'currency' 		=> $input['currency'],
				'company_id'	=> session('current_company'),
				'total_amount'	=> $input['total_amount'],
				'status'		=> 1,
				'created_by'	=> $input['created_by'] ?? null,
				'updated_by'	=> $input['updated_by'] ?? null,
			]
		);
	}

	public function findById($id)
	{
		return $this->accountingInvoice->find($id);
	}
}