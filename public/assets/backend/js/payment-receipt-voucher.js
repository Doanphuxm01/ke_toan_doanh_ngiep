$('.date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
});
var date_input = $('input[name="date_invoice[]"]');
date_input.datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
})

$(document).on('keyup', 'input[name="amount[]"] ,input[name="exchange_rate"]', function() {
    var total_with_amount = 0;
    var total_exchange = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    $("input[name='amount[]']").each(function() {
        var with_amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var tr = $(this).closest('tr').find('input[name="exchange[]"]');
        tr.val(with_amount * exchange_rate).inputmask();
        total_with_amount += with_amount;
    })
    $("#total_with_amount").val(total_with_amount.toFixed(2)).inputmask();
    $("input[name='exchange[]']").each(function() {
        var exchange = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange += exchange;
    })
    $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
});

$(document).on('keyup', 'input[name="taxation[]"], input[name="exchange_rate"]', function() {
    caculateTax();
})

$(document).on('change', 'select[name="tax_rate[]"]', function() {
    caculateTax()
})


$(document).on('keyup', 'input[name="exchange[]"]', function() {
    caculate('input[name="exchange[]"]', "#total_exchange");
})

$(document).on('keyup', 'input[name="exchange_tax[]"]', function() {
    console.log('asdasd');
    caculate('input[name="exchange_tax[]"]', "#total_exchange_tax");
})

$(document).on('keyup', 'input[name="unit_price[]"]', function() {
    caculate('input[name="unit_price[]"]', "#total_service_value")
})

$(document).on('keyup', 'input[name="exchange_unit_price[]"]', function() {
    caculate('input[name="exchange_unit_price[]"]', "#total_exchange_service");
})

function caculate(name_type, id_sum) {
    var total = 0;
    $(name_type).each(function() {
        var value = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total += Number(value);
    })
    $(id_sum).val(Number(total).toFixed(2)).inputmask();
}

function caculateTax() {
    var tax_rate = 0;
    var total_taxation = 0;
    var total_exchange_tax = 0;
    var total_service_value = 0;
    var total_exchange_service = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    var amount_servive = (parseFloat($("#total_exchange").text().replace(/,/g, '')) || 0);

    $("input[name='taxation[]']").each(function() {
        var taxation = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var tr = $(this).closest('tr').find('input[name="exchange_tax[]"]');
        tr.val(taxation * exchange_rate).inputmask();
        tr = tr.closest('tr').find('select[name="tax_rate[]"]');
        var tax_rate = (parseFloat(tr.val()) || 0);
        tr = tr.closest('tr').find('input[name="unit_price[]"]');
        tr.val(taxation / (tax_rate)).inputmask();
        total_taxation += taxation;
    })
    $("#total_taxation").val(total_taxation.toFixed(2)).inputmask();

    $("input[name='exchange_tax[]']").each(function() {
        var exchange_tax = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange_tax += exchange_tax;
    })
    $("#total_exchange_tax").val(total_exchange_tax.toFixed(2)).inputmask();

    $("input[name='unit_price[]']").each(function() {
        var unit_price = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var tr = $(this).closest('tr').find('input[name="exchange_unit_price[]"]');
        tr.val((unit_price * exchange_rate).toFixed(2)).inputmask();
        total_service_value += unit_price;
    })
    $("#total_service_value").val(total_service_value.toFixed(2)).inputmask();

    $("input[name='exchange_unit_price[]']").each(function() {
        var exchange_service = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange_service += exchange_service;
    })
    var amount_total = total_taxation + amount_servive;
    $("#total_exchange_service").val(total_exchange_service.toFixed(2)).inputmask();
}

function setInfoObjectOriginal(repo) {
    if (typeof repo) {
        $('input[name="beneficiary"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="object_name"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="address"]').val(repo.params.originalSelect2Event.data.address);
        $('input[name="details"]').val(trans + repo.params.originalSelect2Event.data.name);
    }
}

function setInfoObjectAccounting(repo, element) {
    if (typeof repo) {
        $(element).closest('tr').find('input[name="object_name_accounting[]"]').val(repo.params.originalSelect2Event.data.name);
    }
}