jQuery(document).ready(function(a) {
    var b = a(document.body),
        c = !1,
        d = !1;
    b.on("keydown", function(a) {
        var b = a.keyCode ? a.keyCode : a.which;
        16 == b && (c = !0)
    }), b.on("keyup", function(a) {
        var b = a.keyCode ? a.keyCode : a.which;
        16 == b && (c = !1)
    }), b.on("mousedown", function(b) {
        d = !1, 1 != a(b.target).is('[class*="select2"]') && (d = !0)
    }), b.on("select2:opening", function(b) {
        d = !1, a(b.target).attr("data-s2open", 1)
    }), b.on("select2:closing", function(b) {
        a(b.target).removeAttr("data-s2open")
    }), b.on("select2:close", function(b) {
        var e = a(b.target);
        e.removeAttr("data-s2open");
        var f = e.closest("form-u"),
            g = f.has("[data-s2open]").length;
        if (0 == g && 0 == d) {
            var h = f.find(":input:enabled:not([readonly], input:hidden, button:hidden, textarea:hidden)").not(function() {
                    return a(this).parent().is(":hidden")
                }),
                i = null;
            if (a.each(h, function(b) {
                    var d = a(this);
                    if (d.attr("id") == e.attr("id")) return i = c ? h.eq(b - 1) : h.eq(b + 1), !1
                }), null !== i) {
                var j = i.siblings(".select2").length > 0;
                j ? i.select2("open") : i.focus()
            }
        }
    }), b.on("focus", ".select2", function(b) {
        var c = a(this).siblings("select");
        0 == c.is("[disabled]") && 0 == c.is("[data-s2open]") && a(this).has(".select2-selection--single").length > 0 && (c.attr("data-s2open", 1), c.select2("open"))
    })
});

function enterTab() {
    $(document).on('keydown', 'input', function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if (key == 13) {
            e.preventDefault();
            var html = $(this).closest('td').next().find('select');
            if (html.is("select")) {
                $(this).closest('td').next().find('select').select2('open');
            } else {
                $(this).closest('td').next().find('input').focus();
            }
        }
    });
    $(document).on('keydown', ':tabbable', function(e) {
        if (e.key === "Enter") {
            e.preventDefault();
            var $canfocus = $(':tabbable:visible');
            var index = $canfocus.index(document.activeElement) + 1;
            var isOriginalEvent = e.originalEvent
            var isSingleSelect = $(this).find(".select2-selection--single").length > 0
            if (isOriginalEvent && isSingleSelect) {
                $(this).siblings('select:enabled').select2('open');
            }
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });
}

function select2Tab() {
    $(document).on('focus', '.select2', function(e) {
        if (e.originalEvent) {
            var s2element = $(this).siblings('select');
            s2element.select2('open');
            // Set focus back to select2 element on closing.
            s2element.on('select2:closing', function(e) {
                s2element.select2('focus');
            });
        }
    });
    $(".select2").select2().on("select2:open", function(e) {
        var position = $(this).closest('td');
        if (position.index() > 4) {
            $('.table-responsive').scrollLeft($(this).position().left - 100);
        }
    });

    $(".select2").select2().on("select2:close", function(e) {
        var html = $(this).closest('td').next().find('select');
        if (html.is("select")) {} else {
            $(this).closest('td').next().find('input').focus();
        }
    });

    $(".select2").select2().on("select2:selecting", function(e) {
        var html = $(this).closest('td').next().find('select');
        if (html.is("select")) {
            $(this).closest('td').next().find('select').select2('open');
        } else {
            $(this).closest('td').next().find('input').focus();
        }
    });
}