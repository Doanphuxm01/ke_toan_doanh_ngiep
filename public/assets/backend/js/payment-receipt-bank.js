$(document).on('keyup', 'input[name="amount_accounting[]"] ,input[name="exchange_rate"]', function() {
    var total_with_amount = 0;
    var total_exchange = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    $("input[name='amount_accounting[]']").each(function() {
        var with_amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var tr = $(this).closest('tr').find('input[name="exchange_accounting[]"]');
        tr.val(with_amount * exchange_rate).inputmask();
        total_with_amount += with_amount;

    })
    $("#total_amount_accounting").val(total_with_amount.toFixed(2)).inputmask();
    $("input[name='exchange_accounting[]']").each(function() {
        var exchange = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange += exchange;
    })

    $("#total_exchange_accounting").val(total_exchange.toFixed(2)).inputmask();
});

$('.date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
});
var date_input = $('input[name="date_invoice[]"]');
date_input.datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
})

function getInfoAllPartner(repo) {
    if (typeof repo) {
        $('input[name="details"]').val(trans + repo.params.originalSelect2Event.data.name);
        $('input[name="object_name"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="object_name_tax[]"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="address"]').val(repo.params.originalSelect2Event.data.address);
        $('input[name="tax_code[]"]').attr('value', repo.params.originalSelect2Event.data.tax_code);
        $("select[name='object_code_tax[]']").append($("<option selected></option>").attr("value", repo.params.originalSelect2Event.data.id).text(repo.params.originalSelect2Event.data.code));
    }
}