function totalAInput(el) {
    let total = 0
    el.each(function (e) {
        let temp = formatInputMask($(this).val())
        total += temp
    })
    return total
}

function setExchangeAndTotal(el, tagExchange) {
    let total = 0
    let total2 = 0
    let rate = formatInputMask($('input.exchange-rate').val())
    el.each(function () {
        let temp = formatInputMask($(this).val())
        total += temp
        $(this).parent().parent().find(tagExchange).val(temp * rate)
        total2 += temp * rate
    })
    return [total, total2]
}

function totalOnChange(el) {
    let total = 0
    let tag = 'input' + '[name="' + el.attr('name') + '"]'
    console.log('tag', tag)
    $(document).on('keyup', tag, function () {
        total = totalAInput($(el))
    })
    return total
}

function formatInputMask(number) {
    return parseFloat(number.toString().replace(/,/g, '') || 0)
}

function formatInputMask2Digits(number) {
    return number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

$(".voucher-date").datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true,
    language: "vi",
}).datepicker("update", new Date());

function clearRow($el) {
    $el.find('select').each(function () {
        $(this).val('')
    })
    $el.find('input').each(function () {
        $(this).val('')
    })
}

function setPriceUnitFromTaxRate($elTaxRate) {
    $elTaxRate.each(function () {
        let taxRate = $(this).val()
        let taxation = $(this).parent().parent().find('.taxation').val()
        if (taxRate == 10) {
            let priceUnit = parseFloat(taxation) / (parseFloat(taxRate) / 100)
            $(this).parent().parent().find('.unit-price').val(formatInputMask2Digits(priceUnit))
        }
        if (!taxRate || taxRate == 0) {
            $(this).parent().parent().find('.unit-price').val(0)
        }
    })
}

async function setTotal($elTotalAmount, $elTotalExchange, $elAmount, elExchange, $elTaxRate = null) {
    if ($elTaxRate) await setPriceUnitFromTaxRate($elTaxRate)
    let total = await setExchangeAndTotal($elAmount, elExchange)
    if (total) $elTotalAmount.val(formatInputMask2Digits(total[0]))
    // let totalExchange = totalAInput($('input[name^="exchange"]'))
    $elTotalExchange.val(formatInputMask2Digits(total[1]))
}

function autoDesc() {
    $('input[name^="desc_accounting"]').val($('.desc').val())
    $('.desc').on('keyup', function () {
        let desc = this.value
        $('input[name^="desc_accounting"]').val(desc)
    })
}

function cloneRow($tag, classRemove, title) {
    var tr = $tag.closest('tr');
    tr.find("select").select2('destroy');
    var tr1 = tr.clone();
    tr.find("select").each(function() {
        var name = $(this).attr('name');
        tr1.find("select[name='"+ name + "']").val($(this).select2().val());
    });
    if (!tr1.find('a').hasClass(classRemove)) {
        tr1.find('.clone-row').parent('td').append(`<a href="javascript:void(0);" title="${title}" tabindex="-1" class="btn btn-xs btn-default remove-row ${classRemove}"><i class="text-danger fas fa-minus"></i></a>`);
    }
    tr1.find('input[type="hidden"]').val('')
    $tag.closest('tr').after(tr1);
    $(".select2").select2().on("select2:close", function (e) {$tag.closest('td').next().find('input').focus();});
    callInputMaskDecimal()
    callSelect2()

}
