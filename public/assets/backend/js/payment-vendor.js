$('.date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
});
$(document).ready(function() {
    var date_input = $('input[name="date_invoice[]"]');
    date_input.datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true,
    })
})

var total_amount = 0;
$(document).on('click', 'input:checkbox[name="checkboxindex[]"]', function() {
    var total_exchange = 0
    exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    if ($(this).is(':checked')) {
        var amount = $(this).closest('tr').find('input[name="amount[]"]').val()
        $(this).closest('tr').find('input[name="exchange[]"]').val(exchange_rate * Number(amount.replace(/,/g, "")));
        exchange = $(this).closest('tr').find('input[name="exchange[]"]').val();
        total_amount += Number(amount.replace(/,/g, ""))
        total_exchange += Number(exchange.replace(/,/g, ""))
        $('#total_amount').val(total_amount);
        $('input[name="with_amount"]').val(total_amount);
        $('#total_exchange').val(total_exchange);
    } else {
        var amount = $(this).closest('tr').find('input[name="amount[]"]').val()
        var exchange = $(this).closest('tr').find('input[name="exchange[]"]').val()
        total_amount -= Number(amount.replace(/,/g, ""))
        total_exchange -= Number(exchange.replace(/,/g, ""))
        $('#total_amount').val(total_amount);
        $('input[name="with_amount"]').val(total_amount);
        $('#total_exchange').val(total_exchange);
    }
});

$(document).on('keyup', 'input[name="exchange_rate"]', function() {
    exchange_rate = (parseFloat($(this).val().replace(/,/g, '')) || 0);
    ex = $('table.plane').find($('input[name="exchange[]"]'));
    total_exchange = 0;
    ex.each(function() {
        var amount = (parseFloat($(this).closest('tr').find('input[name="amount[]"]').val().replace(/,/g, '')) || 0);
        exchange = Number(amount) * Number(exchange_rate);
        $(this).val(exchange);
        total_exchange += Number(exchange);
        $('#total_exchange').val(total_exchange);
    })
});

function getInfoAllVendor(repo) {
    if (typeof repo) {
        var data = repo.params.originalSelect2Event;
        if (data.hasOwnProperty('data')) {
            console.log(repo.params.originalSelect2Event.data.name);
            $('input[name="vendor_name"]').val(repo.params.originalSelect2Event.data.name);
        }
    }
}