function caculateDate(number_days, elementStart, elementFinal) {
    $('input[name="number_of_days_owed"], input[name="day_voucher"]').change(function() {

        var date = $('input[name="day_voucher"]').datepicker('getDate');
        if (date !== null) {
            var date = new Date(date);
        }
        days = (parseInt($("input[name='number_of_days_owed']").val(), 10) || 0);
        if (!isNaN(date.getTime())) {
            date.setDate(date.getDate() + days);
            $("input[name='time_for_payment']").val(date.toInputFormat()).prop('readonly', true);
        } else {
            alert("Invalid Date");
        }

    })
    Date.prototype.toInputFormat = function() {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString();
        var dd = this.getDate().toString();
        return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy;
    };
}