function callInputMaskDecimal() {
    $(".currency").inputmask('currency', {
        'alias': 'decimal',
        'groupSeparator': ',',
        'autoGroup': true,
        'min': 0,
        'max': 99999999999999999999.99,
        // 'removeMaskOnSubmit': true,
        autoUnmask: true
    });
}

function callDatePicker() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true,
        language: "vi",
    });
}

function callSelect2() {
    $(".select2").select2({
        width: '100%',
        dropdownAutoWidth : true
    });
}

function callICheck() {
    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue'
    });
}