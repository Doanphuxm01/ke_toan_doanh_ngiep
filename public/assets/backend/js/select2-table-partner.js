var firstEmptySelect = true;

function renderTableS2(element, url, class_css, templateResult, set_info) {
    $(element).select2({
        minimumInputLength: 0,
        ajax: {
            url: url,
            dataType: 'json',
            type: "GET",
            delay: 1000,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function(data, params) {
                data = Object.values(data)[1];
                params.page = params.page || 1;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 5) < data.total,
                    }
                };
            },
            cache: true
        },
        templateResult: templateResult,
        templateSelection: function(repo) {
            return repo.text;
        },
        dropdownCssClass: class_css,
    }).on('select2:close', function(repo) {
        if (typeof set_info) {
            set_info(repo, this);
        }
    })
}

function s2FormatResultPartner(partners) {
    var $container;
    if (!partners.id) {
        firstEmptySelect = true;
        return partners.text;
    }
    if (firstEmptySelect) {
        firstEmptySelect = false;
        $container = $(
            '<div class="row table-select2">' +
            '<div class="col-xs-3"><b>Code</b></div>' +
            '<div class="col-xs-3"><b>Name</b></div>' +
            '<div class="col-xs-3"><b>Tax Code</b></div>' +
            '</div>' +

            '<div class="row">' +
            '<div class="col-xs-3">' + partners.text + '</div>' +
            '<div class="col-xs-3">' + partners.name + '</div>' +
            '<div class="col-xs-3">' + partners.tax_code + '</div>' +
            '</div>'
        );
    } else {
        $container = $('<div class="row table-select2">' +
            '<div class="col-xs-3">' + partners.text + '</div>' +
            '<div class="col-xs-3">' + partners.name + '</div>' +
            '<div class="col-xs-3">' + partners.tax_code + '</div>' +
            '</div>');
    }

    return $container
}

function getInfoAllCustomer(repo) {
    if (typeof repo) {
        $('input[name="details"]').val(trans + repo.params.originalSelect2Event.data.name);
        $('input[name="customer_name"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="object_name_tax[]"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="customer_tax_code"]').val(repo.params.originalSelect2Event.data.tax_code);
        $('input[name="tax_code[]"]').attr('value', repo.params.originalSelect2Event.data.tax_code);
        $("select[name='object_code_tax[]']").empty().append($("<option></option>").attr("value", repo.params.originalSelect2Event.data.id).text(repo.params.originalSelect2Event.data.code));
    }
}

function getInfoTax(repo, element) {
    if (typeof repo) {
        $(element).closest('tr').find('input[name="object_name_tax[]"]').val(repo.params.originalSelect2Event.data.name);
        $(element).closest('tr').find('input[name="tax_code[]"]').val(repo.params.originalSelect2Event.data.tax_code);
    }
}

function s2FormatResultAccount(bankAccount) {
    var $container;
    if (!bankAccount.id) {
        firstEmptySelect = true;
        return bankAccount.text;
    }
    if (firstEmptySelect) {
        firstEmptySelect = false;
        $container = $(
            '<div class="row">' +
            '<div class="col-xs-3"><b>account number</b></div>' +
            '<div class="col-xs-3"><b>Name</b></div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-xs-3">' + bankAccount.text + '</div>' +
            '<div class="col-xs-3">' + bankAccount.bank + '</div>' +
            '</div>'
        );
    } else {
        $container = $('<div class="row">' +
            '<div class="col-xs-3">' + bankAccount.text + '</div>' +
            '<div class="col-xs-3">' + bankAccount.bank + '</div>' +
            '</div>');
    }
    return $container
}

function getInfoAccount(repo) {
    if (typeof repo.params.originalSelect2Event.data) {
        $('input[name="payment_account_name"]').val(repo.params.originalSelect2Event.data.bank);
    }
}