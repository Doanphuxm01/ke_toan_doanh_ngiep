function getInfoAllInput(repo) {
    if (typeof repo) {
        var data = repo.params.originalSelect2Event;
        if (data.hasOwnProperty('data')) {
            $('input[name="details"]').val(trans + repo.params.originalSelect2Event.data.name);
            $('input[name="obj[vendor_name]"]').val(repo.params.originalSelect2Event.data.name);
            $('input[name="object_name_tax[]"]').val(repo.params.originalSelect2Event.data.name);
            $('input[name="tax_code_service"]').val(repo.params.originalSelect2Event.data.tax_code);
            $('input[name="obj[address]"]').val(repo.params.originalSelect2Event.data.address);
            $('input[name="obj[tax_code]"]').attr('value', repo.params.originalSelect2Event.data.tax_code);
            $("select[name='object_code_tax[]']").empty().append($("<option></option>").attr("value", repo.params.originalSelect2Event.data.id).text(repo.params.originalSelect2Event.data.text));
        }
    }
}

function DomCallBack(element, url, class_css, templateResult, set_info) {
    $(element).select2({
        minimumInputLength: 0,
        ajax: {
            url: url,
            dataType: 'json',
            type: "GET",
            delay: 1000,
            data: function(params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function(data, params) {
                data = Object.values(data)[1];
                params.page = params.page || 1;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 5) < data.total,
                    }
                };
            },
            cache: true
        },
        templateResult: templateResult,
        templateSelection: function(repo) {
            return repo.text;
        },
        dropdownCssClass: class_css,
    }).on('select2:close', function(repo) {
        if (typeof set_info) {
            set_info(repo, this);
        }
    })
}

// function ToltalExChange() {
//     $(document).ready(function() {
//         $('input[name="obj[unit_price][]"],input[name="obj[amount][]"],input[name="obj[currency_rate][]"]').keyup(function() {
//             var form = $(this).closest('form#mainFormInput');
//             var rate = (parseFloat(form.find('input[name="obj[currency_rate][]"]').val().replace(/,/g, '')) || 0);
//             var Money = parseInt(form.find('input[name="obj[into_money][]"]').val() || 0);
//             form.find('input[name="obj[exchange][]"]').val(rate * Money);
//         })
//     });
// }

// function TolTalMoney() {
//     $(document).ready(function() {
//         $('input[name="obj[unit_price][]"],input[name="obj[amount][]"]').keyup(function() {
//             var set = $(this).closest('tr');
//             var unit_price = (parseFloat(set.find('input[name="obj[unit_price][]"]').val().replace(/,/g, '')) || 0);
//             var amount = parseInt(set.find('input[name="obj[amount][]"]').val() || 0);
//             set.find('input[name="obj[into_money][]"]').val(unit_price * amount);
//         })
//     });
// }

// function ToltalExChange() {
//     $('input[name="obj[unit_price][]"],input[name="obj[amount][]"],input[name="obj[currency_rate][]"],input[name="obj[into_money][]"]').keyup(function() {
//         var form = $(this).closest('form#mainFormInput');
//         var rate = (parseFloat($('input[name="obj[currency_rate][]"]').val().replace(/,/g, '')) || 0);
//         $('input[name="obj[exchange][]"]').each(function() {
//             // var money = $(this).closest('tr').find('input[name="obj[into_money][]"]').val();
//             var money = (parseFloat($(this).closest('tr').find('input[name="obj[into_money][]"]').val().replace(/,/g, '')) || 0);
//             // money = parseFloat(money || 0);
//             // console.log($(this).closest('tr').find('input[name="obj[into_money][]"]').val());
//             // console.log(rate);
//             $(this).closest('tr').find('input[name="obj[exchange][]"]').val(rate * money);
//         })
//     })
// }
//
function ToltalExChange() {
    $('.unit_price,.amount,.currency_rate,.int_money').on('keyup', function() {
        var rate_val = $('.currency_rate').val();
        var rate = parseFloat(rate_val.replace(/,/g, ''))|| 0;
        if(rate > 0){
            var form = $(this).closest('tr');
            var id_money = form.find('input.int_money').attr('id');
            var money_val = $('#'+ id_money +'').val();
            if(typeof(money_val)  !== "undefined"){
                var money = parseFloat(money_val.replace(/,/g, '')|| 0)
                // console.log({
                //     'id' : money_val,
                //     'so_tien' : money,
                //     '-----':'------',
                //     'Rate' : rate,
                //     'tong_tien' : rate * money
                //
                // });
                // parseFloat(money || 0);
                var exchange = form.find('input.exchange').attr('id');
                $('#'+exchange+'').val(rate * money);
            }else{
                console.log('anh  có thể chờ em từ sáng đến tối !! nhưng không thể chờ em Undefined Anh  !!');
                return;
            }
        }else{
            return alert('Xin lỗi Quý Khách số Rate phải lớn hơn số 0 để hệ thống có thể chạy');
        }
    })
}

// $(document).ready(function() {
//     $('.tbodys').keyup(function () {
//         callExchane();
//     });
//
// })
// function ToltalExChange() {
//     $(document).ready(function() {
//         $('input[name="obj[currency_rate][]"],input[name="obj[into_money][]"],input[name="obj[exchange][]"]').keyup(function() {
//             callExchane();
//         })
//     });
// }
// function callExchane() {
//     var rate = (parseFloat($('input[name="obj[currency_rate][]"]').val().replace(/,/g, '')) || 0);
//     $('input[name="obj[exchange][]"]').each(function() {
//         var money = (parseFloat($(this).closest('tr').find('input[name="obj[into_money][]"]').val().replace(/,/g, '')) || 0);
//         $(this).closest('tr').find('input[name="obj[exchange][]"]').val(rate * money);
//     })
// }
function TolTalMoney() {
    $('.unit_price,.amount').on('keyup', function() {
        var set        = $(this).closest('tr');
        var unit_price = set.find('input.unit_price').attr('id');
        var amount     = set.find('input.amount').attr('id');
        var int_money  = set.find('input.int_money').attr('id');
        var unit_price = (parseFloat($('#'+unit_price+'').val().replace(/,/g, '')) || 0);
        var amount     = parseInt($('#'+amount+'').val() || 0);
        $('#'+int_money+'').val(unit_price * amount);
    })
}