$(document).on('keyup', 'input[name="quantity[]"] , input[name="price_unit[]"]', function() {
    var total_with_amount = 0;
    var total_amount = 0;
    var total_quantity = 0;
    var total_exchange = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    $("input[name='quantity[]']").each(function() {
        var tr = $(this).closest('tr');
        var quantity = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var with_amount = (parseFloat(tr.find('input[name="price_unit[]"]').val().replace(/,/g, '')) || 0);
        var td_total_amount = tr.find('input[name="amount[]"]');
        var td_exchange = tr.find('input[name="exchange[]"]');
        td_total_amount.val(with_amount * quantity).inputmask();
        total_with_amount += with_amount;
        total_quantity += quantity;
        total_amount += with_amount * quantity;
        td_exchange.val(with_amount * quantity * exchange_rate).inputmask()
        total_exchange += Number(with_amount * quantity * exchange_rate);
    })

    $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    $("#total_quantity").val(total_quantity.toFixed(2)).inputmask();
    $("#total_with_amount").val(total_with_amount.toFixed(2)).inputmask();
    $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
    $("#total_amount").val(total_amount.toFixed(2)).inputmask();
})

$(document).on('keyup', 'input[name="amount[]"]', function() {
    var total_with_amount = 0;
    var sum_total_amount = 0;
    var total_quantity = 0;
    var total_exchange = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    $('input[name="amount[]"]').each(function() {
        var total_amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var tr = $(this).closest('tr');
        var td_quantity = tr.find('input[name="quantity[]"]');
        var td_with_amount = tr.find('input[name="price_unit[]"]');
        var td_exchange = tr.find('input[name="exchange[]"]');
        var quantity = (parseFloat(String(td_quantity.val()).replace(/,/g, '')) || 0);
        var with_amount = total_amount / quantity;
        if (Object.is(with_amount, NaN) || Object.is(with_amount, Infinity)) {
            with_amount = 0;
        }
        td_with_amount.val(with_amount.toFixed(2));
        total_with_amount += with_amount;
        total_quantity += quantity;
        sum_total_amount += total_amount;
        td_exchange.val((with_amount * quantity * exchange_rate).toFixed(2)).inputmask();
        total_exchange += with_amount * quantity * exchange_rate;

        $('#amount_service_final').html(String(sum_total_amount.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        $("#total_quantity").val(total_quantity.toFixed(2)).inputmask();
        $("#total_with_amount").val(total_with_amount.toFixed(2)).inputmask();
        $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
        $("#total_amount").val(sum_total_amount.toFixed(2)).inputmask();

    })

})
$(document).on('keyup', 'input[name="exchange_rate"]', function() {
    var total_exchange = 0
    var total_amount = 0
    var exchange_rate = (parseFloat($(this).val().replace(/,/g, '')) || 0);
    $('input[name="amount[]"]').each(function() {
        var tr = $(this).closest('tr').find('input[name="exchange[]"]');
        var amount = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        tr.val(amount * exchange_rate);
        total_exchange += amount * exchange_rate;
        total_amount += amount;
        $("#total_exchange").val(total_exchange.toFixed(2)).inputmask();
    });
    $('input[name="taxation[]"]').each(function() {
        var td_exchange_tax = $(this).closest('tr').find('input[name="exchange_tax[]"]');
        var taxation = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        td_exchange_tax.val(taxation * exchange_rate);
        total_exchange += taxation * exchange_rate;
    });
    $("#total_exchange_tax").val(total_exchange.toFixed(2)).inputmask();
    $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
})

$(document).on('change', 'select[name="tax_rate[]"]', function() {
    var tax_rate = 0;
    var total_taxation = 0;
    var total_exchange_tax = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    var total_amount = (parseFloat($("#total_amount").val().replace(/,/g, '')) || 0);
    $("input[name='taxation[]']").each(function() {
        var tr = $(this).closest('tr');
        var indexTrTax = $(this).closest('tr').index();
        var amount_accounting = (parseFloat($('#table_accounting tr:eq(' + indexTrTax + ')').find('input[name="amount[]"]').val().replace(/,/g, '')) || 0);
        var td_tax_rate = tr.find('select[name="tax_rate[]"]');
        var tax_rate = (parseFloat(td_tax_rate.val().replace(/,/g, '')) || 0);
        $(this).val((amount_accounting * tax_rate) / 100);
        var taxation = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        var td_exchange_tax = tr.find('input[name="exchange_tax[]"]');
        td_exchange_tax.val(Number(taxation) * Number(exchange_rate)).inputmask();
        total_taxation += Number(taxation);
    })
    $("#total_taxation").val(total_taxation.toFixed(2)).inputmask();
    $("input[name='exchange_tax[]']").each(function() {
        var exchange_tax = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange_tax += exchange_tax;
    })
    $("#total_exchange_tax").val(total_exchange_tax.toFixed(2)).inputmask();
    console.log("total_amount" + total_amount);
    var amount_total = total_taxation + total_amount;

    $('#amount_service_final').html(total_amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    $('#amount_vat').html(total_taxation.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    $('#amount_total').html(amount_total.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
})

$(document).on('keyup', 'input[name="exchange_tax[]"]', function() {
    var total_exchange_tax = 0;
    $("input[name='exchange_tax[]']").each(function() {
        var exchange_tax = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange_tax += exchange_tax;
    })
    $("#total_exchange_tax").val(total_exchange_tax.toFixed(2)).inputmask();
})

$(document).on('keyup', 'input[name="exchange[]"]', function() {
    var total_exchange = 0;
    $("input[name='exchange[]']").each(function() {
        var exchange = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        total_exchange += Number(exchange);
    })
    $("#total_exchange").val(Number(total_exchange).toFixed(2)).inputmask();
})

$(document).on('keyup', 'input[name="taxation[]"]', function() {
    var totalTaxation = 0;
    var totalExchangeTax = 0;
    var exchange_rate = (parseFloat($('input[name="exchange_rate"]').val().replace(/,/g, '')) || 0);
    $('input[name="taxation[]"]').each(function(e) {
        tr = $(this).closest('tr');
        taxation = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        totalTaxation += Number(taxation);
        td_exchange_tax = tr.find('input[name="exchange_tax[]"]');
        td_exchange_tax.val(Number(exchange_rate) * Number(taxation)).inputmask()
    });
    $('input[name="exchange_tax[]"]').each(function(e) {
        exchange_tax = (parseFloat($(this).val().replace(/,/g, '')) || 0);
        totalExchangeTax += Number(exchange_tax);
    });
    $('#total_taxation').val(totalTaxation.toFixed(2)).inputmask();
    $('#total_exchange_tax').val(totalExchangeTax.toFixed(2)).inputmask();
    amount_servive = (parseFloat($("#total_amount").val().replace(/,/g, '')) || 0);
    amount_total = amount_servive + totalTaxation;
    $('#amount_service_final').html(String(amount_servive.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    $('#amount_vat').html(String(totalTaxation.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    $('#amount_total').html(String(amount_total.toFixed(2)).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
})

$('.date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
});
$('input[name="day_voucher"]').change(function(e) {
    e.preventDefault();
    $('input[name="date_vouchers"]').val($('input[name="settlement_date"]').val());
});

$('input[name="date_invoice[]"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    autoclose: true,
})
$('input[name="number_of_days_owed"]').inputmask({ 'alias': 'decimal', 'autoGroup': true, 'min': 0, 'max': 99999999999999999999.99, 'removeMaskOnSubmit': true });
$('input[name="number_of_days_owed"], input[name="day_voucher"]').change(function() {

    var date = $('input[name="day_voucher"]').datepicker('getDate');
    if (date !== null) {
        var date = new Date(date);
    }

    days = (parseInt($("input[name='number_of_days_owed']").val(), 10) || 0);
    if (!isNaN(date.getTime())) {
        date.setDate(date.getDate() + days);
        $("input[name='time_for_payment']").val(date.toInputFormat()).prop('readonly', true);
    } else {
        alert("Invalid Date");
    }

})
Date.prototype.toInputFormat = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    var dd = this.getDate().toString();
    return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy;
};

$(document).on('change', 'input[name="day_voucher"]', function(param) {
    var day_voucher = $('input[name="day_voucher"]').val();
    $('input[name="date_invoice[]"]').val(day_voucher);
})

$(document).on('change', "select[name='type']", function(event) {
    var type = $(this).val();
    if (type == typeAir) {
        $('.for_mawb').html(mawb);
        $('.for_hawb').html(hawb);
        $("input[name='hawb[]']").prop('disabled', false);
    } else if (type == typeSea) {
        $('.for_mawb').html(hbl);
        $('.for_hawb').html(obl);
        $("input[name='hawb[]']").prop('disabled', false);
    } else if (type == typeDom) {
        $('.for_mawb').html(invoice);
        $('.for_hawb').html('&nbsp;');
        $("input[name='hawb[]']").prop('disabled', true);
    } else if (type == typeOther) {
        $('.for_mawb').html(statistical_code);
        $('.for_hawb').html('&nbsp;');
        $("input[name='hawb[]']").prop('disabled', true);
    } else {
        toastr.error("");
        return false;
    }
});

function getInfoAllVendor(repo) {
    if (typeof repo) {
        var data = repo.params.originalSelect2Event;
        if (data.hasOwnProperty('data')) {
            $('input[name="details"]').val(trans + repo.params.originalSelect2Event.data.name);
            $('input[name="vendor_name"]').val(repo.params.originalSelect2Event.data.name);
            $('input[name="object_name_tax[]"]').val(repo.params.originalSelect2Event.data.name);
            $('input[name="tax_code_service"]').val(repo.params.originalSelect2Event.data.tax_code);
            $('input[name="tax_code[]"]').attr('value', repo.params.originalSelect2Event.data.tax_code);
            $("select[name='object_code_tax[]']").empty().append($("<option></option>").attr("value", repo.params.originalSelect2Event.data.id).text(repo.params.originalSelect2Event.data.text));
        }
    }
}

function getInfoAllPartner(repo) {
    if (typeof repo) {
        $('input[name="details"]').val(trans + repo.params.originalSelect2Event.data.name);
        $('input[name="object_name"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="object_name_tax[]"]').val(repo.params.originalSelect2Event.data.name);
        $('input[name="address"]').val(repo.params.originalSelect2Event.data.address);
        $('input[name="tax_code[]"]').attr('value', repo.params.originalSelect2Event.data.tax_code);
        $("select[name='object_code_tax[]']").append($("<option selected></option>").attr("value", repo.params.originalSelect2Event.data.id).text(repo.params.originalSelect2Event.data.code));
    }
}